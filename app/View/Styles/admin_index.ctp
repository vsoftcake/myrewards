<style>
#content
{
	border-left:1px solid #cccccc;
	border-right:1px solid #cccccc;
	border-bottom:1px solid #cccccc;
	margin-left:20px !important;
	padding-left:18px !important;
	width:913px;
}
.rounded-corners 
{
	-moz-border-radius: 10px 10px 0px 0px; /* Firefox */
	-webkit-border-radius: 10px 10px 0px 0px; /* Safari, Chrome */
	border-radius: 10px 10px 0px 0px; /* CSS3 */
}
.current
{
    font-family: 'Droid Sans',sans-serif;
    font-size: 12px;
   	padding:5px 8px !important;   
    background-color: #0066CC;
    border: 1px solid #DFDFDF;
    color:#ffffff;
}
.cpagination1
{
	/*padding:1px 2px 1px 2px;*/
	font-family: 'Droid Sans',sans-serif;
    font-size: 12px;
	text-decoration:none !important;
	background-color: #FFFFFF;
	padding:3px 8px;
	 border: 1px solid #0066CC;
	 color:#808080 !important;
}
.admin_button
{
	height: 24px;
	padding-top: 2px;
	padding-bottom: 5px;
	border-radius: 5px 5px 5px 5px;
	background-color: #0066cc;
	border: 1px solid #cccccc;
	color: #FFFFFF; 
	cursor: pointer;
	font-size:13px;
}
</style>
<table style="margin-left:-2px;">
	<tr>
		<td>
            &nbsp;&nbsp;<img style="width:40px;height:40px;" src="/files/admin_home_icons/styles.png"  alt="Styles" title="Styles" />&nbsp;&nbsp;
        </td>
        <td> 
            <h2><?php echo 'Styles';?></h2>
        </td>
    </tr>        
</table>
<table>
	<?php if($this->Session->read('user.User.type') != 'Client Admin' && $this->Session->read('user.User.type') != 'Country Admin'): ?>
	<tr>
		<td style="vertical-align:top;">
			<div class="actions" style="padding-bottom:20px">
				<table style="width:925px;background-color:#cccccc;height:75px;margin-left:-2px;border:1px solid #969090;">
					<tr>
						<td style="width: 700px; padding-top: 5px;">
						<form action="<?php echo $this->Html->url('/admin/styles/'); ?>" method="get" id="SearchForm">
						<table>
							<tr>
								<td style="padding-left:10px;"><strong>Find Styles</strong></td>
								<td style="padding-left:5px;">
									<?php echo $this->Form->input('Style.search', array('div'=>false, 'label'=>false)); ?>
									<script type="text/javascript">
									$(function() {
										if ($('#StyleSearch').val() == '') {
											$('#StyleSearch').val('ID or name');
											$('#StyleSearch').css( "color", "#aaaaaa" );
										}
										$( "#StyleSearch" ).focus(function() {
											if ($(this).val() == 'ID or name') {
												$(this).val('');
												$(this).css( "color", "#000" );
											}
										});
										$( "#SearchForm" ).submit(function() {
											if ($("#StyleSearch").val() == 'ID or name') {
												$("#StyleSearch").val('');
												$("#StyleSearch").css( "color", "#000" );
											}
										});
									});
									</script>
								</td>
								<td></td>
								<td style="padding-left:10px;"><button class="admin_button">Search</button></td>
							</tr>
						</table>
						</form>
						</td>
						<td style="padding-top: 5px;">
							<ul>
						    	<li><button class ="admin_button" onclick="location.href='<?php echo $this->Html->url('edit'); ?>';">Create New Style</button></li>
							</ul>
						</td>
					</tr>
				</table>
			</div>
		</td> 
	</tr>
	<?php endif; ?>
	<tr>
		<td>
			<div class="list">
			<table style="width:925px;margin-left:-2px;">
			<tr style="height:30px;background-color:#cccccc;">
				<th style="height:30px;background-color:#cccccc;text-align:left;padding-left:5px;border-left:1px solid #969090;border-bottom:1px solid #969090;border-top:1px solid #969090;"><?php echo $this->Paginator->sort('name');?></th>
				<th style="height:30px;background-color:#cccccc;text-align:left;padding-left:5px;border-bottom:1px solid #969090;border-top:1px solid #969090;">Corners</th>
				<th style="height:30px;background-color:#cccccc;text-align:left;padding-left:7px;border-bottom:1px solid #969090;border-top:1px solid #969090;">Style</th>
				<th class="actions" style="height:30px;background-color:#cccccc;text-align:left;padding-left:4px;border-right:1px solid #969090;border-bottom:1px solid #969090;border-top:1px solid #969090;"><?php echo "Actions";?></th>
			</tr>
			<?php
			$i = 0;
			foreach ($styles as $style):
				$class = null;
				if ($i++ % 2 == 0) {
					$class = ' class="altrow"';
				}
			?>
				<tr<?php echo $class;?>>
					<td width="400" style="padding-left:5px;">
						<?php echo $style['Style']['name'] ?>
					</td>
					<td width="220">
						<table style="border: 0;">
							<tr>
								<td style="padding:0;border:0;" >
									<div class="rounded-corners" style="width: 50px;height: 20px;background-color: #<?php echo $style['Style']['tab_background_color']; ?>;">
										&nbsp;&nbsp;&nbsp;
									</div>
								</td>
							</tr>
						</table>
					</td>
					<td width="220">
						&nbsp;
						<span style="background-color: #<?php echo $style['Style']['button_border_color']; ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
						&nbsp;
						<span style="background-color: #<?php echo $style['Style']['h1_color']; ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
						&nbsp;
					</td>
					<td class="actions" width="100">
						<?php echo $this->Html->link(__('Edit', true), array('action'=>'edit', $style['Style']['id'])); ?> 
						<?php if($this->Session->read('user.User.type') != 'Client Admin' && $this->Session->read('user.User.type') != 'Country Admin'): ?>
						|
						<?php echo $this->Html->link(__('Delete', true), array('action'=>'delete', $style['Style']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $style['Style']['id'])); ?>
						<?php endif; ?>
					</td>
				</tr>
			<?php endforeach; ?>
			</table>
			</div>
			
			<div class="paging" style="padding-top:15px;float:right;">
				<table style="border:none !important;">
					<tr>
						<td style="border:none !important;" valign="top">
							<?php echo $this->Paginator->first('<<', array('title'=>'first','class'=>'disabled cpagination1'));?>
							<?php echo $this->Paginator->prev('<', array('title'=>'prev','class'=>'disabled cpagination1'));?>
							<?php echo $this->Paginator->numbers(array('separator' => ' ','class'=>'cpagination1'));?>
							<?php echo $this->Paginator->next('>', array('title'=>'next','class'=>'disabled cpagination1'));?>
							<?php echo $this->Paginator->last('>>', array('title'=>'last','class'=>'disabled cpagination1'));?>
						</td>
						<td style="border:none !important;padding-top:1px;padding-left:3px">
						<select name="page" id="selectpage" style="margin-top:-5px;">
							<?php for ($i = 1; $i <= $this->Paginator->counter(array('format' => '%pages%')); $i++):?>
							<option value="<?php echo $i;?>"<?php echo ($this->Paginator->current($params['page']) == $i) ? ' selected="selected"' : ''?>><?php echo $i ?></option>
							<?php endfor ?>
						</select>
						<script>
						$("#selectpage").change(function() {
							var url = window.location.href;
							var n = url.indexOf("/?");
							var extra = "";
							if(n>0){extra = url.substring(n,url.length);}
							window.location.href = '/admin/styles/index/page:'+$(this).val()+extra;
						});
						</script>
						</td>
					</tr>
				</table>
			</div>
		</td>
	</tr>
</table>