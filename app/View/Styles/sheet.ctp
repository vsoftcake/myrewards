/* base elements */
html, body {
		background-color: #<?php echo $style['Style']['body_background_color']; ?>;
		font-family: <?php echo $style['Style']['font_style'];?>;
}
h1 {
	<?php echo ($style['Style']['h1_color'] != '')? 'color: #'. $style['Style']['h1_color']. ';': ""; ?>
	<?php echo ($style['Style']['h1_size'] != '')? 'size: '. $style['Style']['h1_size']. ';': ""; ?>
	margin-bottom: 0px;
	padding-bottom: 0px;
}

h2 {
	<?php echo ($style['Style']['h2_color'] != '')? 'color: #'. $style['Style']['h2_color']. ';': ""; ?>
	<?php echo ($style['Style']['h2_size'] != '')? 'size: '. $style['Style']['h2_size']. ';': ""; ?>
	margin: 0px;
	padding: 0px;
}

h3 {
	<?php echo ($style['Style']['h3_color'] != '')? 'color: #'. $style['Style']['h3_color']. ';': ""; ?>
	<?php echo ($style['Style']['h3_size'] != '')? 'size: '. $style['Style']['h3_size']. ';': ""; ?>
	margin-bottom: 0px;
	padding-bottom: 0px;
}

h4 {
	<?php echo ($style['Style']['h4_color'] != '')? 'color: #'. $style['Style']['h4_color']. ';': ""; ?>
	<?php echo ($style['Style']['h4_size'] != '')? 'size: '. $style['Style']['h4_size']. ';': ""; ?>
	margin: 0px;
	padding: 0px;
}

.style1.a{

<?php if ($this->Session->read('client.Client.id') == '1115'){echo 'color:#F60909'; }?>

}

h5 {
	<?php echo ($style['Style']['h5_color'] != '')? 'color: #'. $style['Style']['h5_color']. ';': ""; ?>
	<?php echo ($style['Style']['h5_size'] != '')? 'size: '. $style['Style']['h5_size']. ';': ""; ?>
	margin-bottom: 0px;
	padding-bottom: 0px;
}

a, a:visited, a:link, a:active {
	<?php echo ($style['Style']['link_color'] != '')? 'color: #'. $style['Style']['link_color']. ';': ""; ?>
}
.link_color
{
	<?php echo ($style['Style']['link_color'] != '')? 'color: #'. $style['Style']['link_color']. '!important;': ""; ?>
}
a:hover {
	<?php echo ($style['Style']['link_hover_color'] != '')? 'color: #'. $style['Style']['link_hover_color']. ';': ""; ?>
}

/* header */
#banner_container {
	background-color: #<?php echo $this->Session->read('client.Client.banner_background_color'); ?>;
}

#header td {
	/*border-bottom-color: #<?php echo $style['Style']['nav_border_color']; ?>;*/
}

button {
	border-color: #<?php echo $style['Style']['button_border_color']; ?>;
	background-color: #<?php echo $style['Style']['button_background_color']; ?>;
	color: #<?php echo $style['Style']['button_color']; ?>;
}

input {
	color: #<?php if ($this->Session->read('client.Client.id') == '968'){ echo '000000'; } else { echo $style['Style']['button_background_color']; }?>;
}

a, .list_bullet {
	color: #<?php echo $style['Style']['link_color']; ?>;
}

#container {
	background-color: #<?php echo $style['Style']['nav_background_color']; ?>;
}

#content {
	<?php echo ($style['Style']['nav_border_color'] != ''?  'border-color: #'.  $style['Style']['nav_border_color']. ';': ''); ?>
}

/* left nav*/
#left_nav, #nav_browser a, #nav_browser .list_bullet {
	color: #<?php echo $style['Style']['nav_color']; ?>;
	background-color: #<?php echo $style['Style']['nav_background_color']; ?>;
	<?php echo ($style['Style']['nav_border_color'] != ''?  'border-color: #'.  $style['Style']['nav_border_color']. ';': ''); ?>
}

#nav_links li.root div.root a {
	color: #<?php echo $style['Style']['tab_color']; ?>;
}

#nav_links div.children a {
    color: #<?php echo $style['Style']['nav_color']; ?>;
}

#nav_links li.root {
	background-color: #<?php echo $style['Style']['button_background_color']; ?>;
	<?php echo ($style['Style']['nav_border_color'] != ''?  'border-color: #'.  $style['Style']['nav_border_color']. ';': ''); ?>
}

#nav_links li {
	/*background-color: #<?php echo $style['Style']['nav_background_color']; ?>;*/
}

#nav_links hr {
	<?php echo ($style['Style']['nav_border_color'] != ''?  'background-color: #'.  $style['Style']['nav_border_color']. ';': ''); ?>
	<?php echo ($style['Style']['nav_border_color'] != ''?  'color: #'.  $style['Style']['nav_border_color']. ';': ''); ?>
}

/* modules */
.module div.tl {
	background:  url('<?php echo  $this->Html->url('/files/styles/tl_image/'. $this->Session->read('client.Style.tl_image')); ?>') no-repeat top left;
}


.module div.tr {
	background:  url('<?php echo  $this->Html->url('/files/styles/tr_image/'. $this->Session->read('client.Style.tr_image')); ?>') no-repeat top right;
}

.module div.t {
	border-top-color: #<?php echo $style['Style']['border_color']; ?>;
	border-left-color: #<?php echo $style['Style']['border_color']; ?>;
	border-right-color: #<?php echo $style['Style']['border_color']; ?>;
	/*background-color: #<?php echo $style['Style']['tab_background_color']; ?>;
	background: #<?php echo $style['Style']['tab_background_color']; if($style['Style']['background_shade'] == 'glassy') {?> url('/files/newimg/box_glass_bg.png') repeat-x 0 -9px <?php }  elseif($style['Style']['background_shade'] == 'ttb') {?> url('/files/newimg/ttb.png')<?php } elseif($style['Style']['background_shade'] == 'btt') {?> url('/files/newimg/btt.png')<?php } elseif($style['Style']['background_shade'] == 'normal') { }?> ;
	*/
	color: #<?php echo $style['Style']['tab_color']; ?>;
}
.stay_touch
{
	color: #<?php echo $style['Style']['tab_color']; ?>;
}
.module div.content {
	border-left-color: #<?php echo $style['Style']['border_color']; ?>;
	border-right-color: #<?php echo $style['Style']['border_color']; ?>;
	background-color: #<?php echo $style['Style']['tab_content_background_color']; ?>;
}

.module div.bl {
	background: url('<?php echo  $this->Html->url('/files/styles/bl_image/'. $this->Session->read('client.Style.bl_image')); ?>') no-repeat top left;
}

.module div.br {
	background: url('<?php echo  $this->Html->url('/files/styles/br_image/'. $this->Session->read('client.Style.br_image')); ?>') no-repeat top right;
}

.module div.b {
	border-bottom-color: #<?php echo $style['Style']['border_color']; ?>;
	border-left-color: #<?php echo $style['Style']['border_color']; ?>;
	border-right-color: #<?php echo $style['Style']['border_color']; ?>;
	background-color: #<?php echo $style['Style']['tab_content_background_color']; ?>;
}

.module a {
	color: #<?php echo $style['Style']['tab_link_color']; ?>;
}

.module a:hover {
	color: #<?php echo $style['Style']['tab_link_hover_color']; ?>;
}

.module_box div.t {
	color: #<?php echo $style['Style']['tab_color']; ?>;
}

#footer, #footer a {
	color: #<?php echo $style['Style']['footer_color']; ?>;
	/*border-bottom-color: #<?php echo $style['Style']['footer_color']; ?>;*/
	background-color: #<?php echo $style['Style']['footer_background_color']; ?>;
	font-family: <?php echo $style['Style']['font_style'];?>;
}

/* offers and special offers */
.special_offer {
	border-top-color: #<?php echo $style['Style']['border_color']; ?>;
	border-top-color: #<?php echo $style['Style']['tab_background_color']; ?>;
}
.whats_new {
	border-top-color: #<?php echo $style['Style']['border_color']; ?>;
	border-top-color: #<?php echo $style['Style']['tab_background_color']; ?>;
}

<?php 
/* moved into voucher page, using Voucher Model
.voucher_content {
	border-left: 2px solid #<?php echo $style['Style']['voucher_border_color']; ?>;
	border-right: 2px solid #<?php echo $style['Style']['voucher_border_color']; ?>;
	border-bottom: 1px solid transparent;
	border-top: 1px solid transparent;
	background: url('<?php echo  $this->Html->url('/images/style/voucher_background'); ?>') no-repeat top left;
}
*/
?>

.dots {
	border-bottom-width: 1px;
	border-bottom-style: dotted;
	border-bottom-color: #<?php echo $style['Style']['h1_color']; ?>;
}
.list_content {
	list-style-type: disc;
	margin-top: 0px;
	margin-right: 15px;
	margin-bottom: 5px;
	margin-left: 15px;
	color: #<?php echo $style['Style']['h1_color']; ?>;
}
.list_text {
	color: #000000;
}
.pic_right {
	margin-bottom: 6px;
	margin-left: 6px;
}
.pic_left {
	margin-right: 6px;
	margin-bottom: 6px;
}

.button_search_module
{
	color:#<?php echo $style['Style']['button_color']; ?>;
	border:#<?php echo $style['Style']['button_border_color']; ?> 1px solid;
	background:#<?php echo $style['Style']['button_background_color']; if($style['Style']['background_shade'] == 'glassy') {?> url('/files/newimg/box_glass_bg.png') repeat-x 0 -10px <?php }  elseif($style['Style']['background_shade'] == 'ttb') {?> url('/files/newimg/ttb.png')<?php } elseif($style['Style']['background_shade'] == 'btt') {?> url('/files/newimg/btt.png')<?php } elseif($style['Style']['background_shade'] == 'normal') { }?> ;
}

.search_input,.select_input,.select_input_exact, .select_input_state, .search_input1, .keyword_search_input, .first_time_input, .first_time_select, .comment_input, .quick_search, .map_input1, .invite_family_textbox, .campaign_input
{
	border:1px solid #<?php echo $style['Style']['border_color']; ?>;
	padding-top:1px;
}
.comment_button
{
	border-color: #<?php echo $style['Style']['button_border_color']; ?>;
	background-color: #<?php echo $style['Style']['button_background_color']; ?>;
	color: #<?php echo $style['Style']['button_color']; ?>;
}
.map_input
{
	border:1px solid #<?php echo $style['Style']['border_color']; ?>;
}
/*module2*/
.module2 div.t2 
{
	color: #<?php echo $style['Style']['tab_color']; ?>;
	border-top-color: #<?php echo $style['Style']['border_color']; ?>;
	border-left-color: #<?php echo $style['Style']['border_color']; ?>;
	border-bottom-color: #<?php echo $style['Style']['border_color']; ?>;
	background: #<?php echo $style['Style']['tab_background_color']; if($style['Style']['background_shade'] == 'glassy') {?> url('/files/newimg/box_glass_bg.png') repeat-x <?php }  elseif($style['Style']['background_shade'] == 'ttb') {?> url('/files/newimg/ttb.png')<?php } elseif($style['Style']['background_shade'] == 'btt') {?> url('/files/newimg/btt.png')<?php } elseif($style['Style']['background_shade'] == 'normal') { }?> ;
}
	
.module2 div.t3 
{
	border-left-color: #<?php echo $style['Style']['border_color']; ?>;
	border-right-color: #<?php echo $style['Style']['border_color']; ?>;
	/*background-color: #<?php echo $style['Style']['tab_content_background_color']; ?>;*/
	border-top-color: #<?php echo $style['Style']['border_color']; ?>;
	border-bottom-color: #<?php echo $style['Style']['border_color']; ?>;
}
.tab_text
{
	<?php echo ($style['Style']['h3_color'] != '')? 'color: #'. $style['Style']['h3_color']. ';': ""; ?>
}
.search_content
{
	/*border-left-color: #<?php echo $style['Style']['border_color']; ?>;
	border-right-color: #<?php echo $style['Style']['border_color']; ?>;*/
	background-color: #<?php echo $style['Style']['tab_content_background_color']; ?>;
	border-top-color: #<?php echo $style['Style']['border_color']; ?>;
	border-bottom-color: #<?php echo $style['Style']['border_color']; ?>;
}
.homepage_box
{
	border-left-color: #<?php echo $style['Style']['border_color']; ?>;
	border-right-color: #<?php echo $style['Style']['border_color']; ?>;
	background-color: #<?php echo $style['Style']['tab_content_background_color']; ?>;
	border-bottom-color: #<?php echo $style['Style']['border_color']; ?>;
}
#MB_content
{
	/*background-color: #<?php echo $style['Style']['tab_content_background_color']; ?> !important;*/
}
.search_results, .how_it_works
{
	border-left-color: #<?php echo $style['Style']['border_color']; ?>;
	border-right-color: #<?php echo $style['Style']['border_color']; ?>;
	border-top-color: #<?php echo $style['Style']['border_color']; ?>;
	border-bottom-color: #<?php echo $style['Style']['border_color']; ?>;
}
.search_printcoupon
{
	border-color: #<?php echo $style['Style']['border_color']; ?>;
	/*border-left-color: #<?php echo $style['Style']['border_color']; ?>;
	border-right-color: #<?php echo $style['Style']['border_color']; ?>;
	border-top-color: #<?php echo $style['Style']['border_color']; ?>;*/
}
.menu_color a
{
	color: #<?php echo $style['Style']['tab_menu_color']; ?> !important;
}
/*.pagination
{
	color: #<?php echo $style['Style']['tab_color']; ?> !important;
	background-color: #<?php echo $style['Style']['link_color']; ?>;
	border-color : #<?php echo $style['Style']['button_background_color']; ?>;
}
.current
{
	color:  #<?php echo $style['Style']['tab_color']; ?> !important;
	background-color: #<?php echo $style['Style']['button_background_color']; ?>;
	border-color : #<?php echo $style['Style']['button_background_color']; ?>;
} */
.pagination
{
	color: #<?php echo $style['Style']['button_background_color']; ?> !important;
	background-color: #<?php echo $style['Style']['button_color']; ?>;
	border-color : #<?php echo $style['Style']['button_border_color']; ?>;
}
.current
{
	color:  #<?php echo $style['Style']['button_color']; ?> !important;
	background-color: #<?php echo $style['Style']['button_background_color']; ?>;
	border-color : #<?php echo $style['Style']['button_border_color']; ?>;
} 
.map_border
{
	border-color: #<?php echo $style['Style']['border_color']; ?>;
}

#menu li ul 
{
	background-color: #<?php echo $style['Style']['tab_background_color'];?> !important;
}
.module div.tm {
	color: #<?php echo $style['Style']['tab_menu_color']; ?>;
	background: #<?php echo $style['Style']['tab_background_color']; if($style['Style']['background_shade'] == 'glassy') {?> url('/files/newimg/box_glass_bg.png') repeat-x 0 -4px <?php }  elseif($style['Style']['background_shade'] == 'ttb') {?> url('/files/newimg/ttb.png')<?php } elseif($style['Style']['background_shade'] == 'btt') {?> url('/files/newimg/btt.png')<?php } elseif($style['Style']['background_shade'] == 'normal') { }?> ;
}

.module3 div.t {
	border-top-color: #<?php echo $style['Style']['border_color']; ?>;
	border-left-color: #<?php echo $style['Style']['border_color']; ?>;
	border-right-color: #<?php echo $style['Style']['border_color']; ?>;
/*	background-color: #<?php echo $style['Style']['tab_background_color']; ?>;
	background: #<?php echo $style['Style']['tab_background_color']; if($style['Style']['background_shade'] == 'glassy') {?> url('/files/newimg/box_glass_bg.png') repeat-x 0 -9px <?php }  elseif($style['Style']['background_shade'] == 'ttb') {?> url('/files/newimg/ttb.png')<?php } elseif($style['Style']['background_shade'] == 'btt') {?> url('/files/newimg/btt.png')<?php } elseif($style['Style']['background_shade'] == 'normal') { }?> ;*/
	color: #<?php echo $style['Style']['tab_color']; ?>;
}

.module3 div.content {
	border-left-color: #<?php echo $style['Style']['border_color']; ?>;
	border-right-color: #<?php echo $style['Style']['border_color']; ?>;
	background-color: #<?php echo $style['Style']['tab_content_background_color']; ?>;
}

.module3 div.b {
	border-bottom-color: #<?php echo $style['Style']['border_color']; ?>;
	border-left-color: #<?php echo $style['Style']['border_color']; ?>;
	border-right-color: #<?php echo $style['Style']['border_color']; ?>;
	background-color: #<?php echo $style['Style']['tab_content_background_color']; ?>;
}

.module3 a {
	color: #<?php echo $style['Style']['tab_link_color']; ?>;
}

.module3 a:hover {
	color: #<?php echo $style['Style']['tab_link_hover_color']; ?>;
}

.search_content_background
{
	background-color: #<?php echo $style['Style']['tab_content_background_color']; ?>;
	
	/*	<?php echo ($style['Style']['tab_content_background_color'] != '')? 'border-left-color: #'. $style['Style']['border_color']. ';': ""; ?>
	<?php if ($style['Style']['tab_content_background_color'] != 'FFFFFF'){?>
		border-left-color: #<?php echo $style['Style']['border_color'];?>;
		border-right-color: #<?php echo $style['Style']['border_color'];?>;
		border-top-color: #<?php echo $style['Style']['border_color'];?>;
		border-bottom-color: #<?php echo $style['Style']['border_color'];?>;
	<?php } elseif($style['Style']['tab_content_background_color'] == 'FFFFFF') { ?>
		border-left-color: #ffffff;
		border-right-color:  #ffffff;
		border-top-color:  #ffffff;
		border-bottom-color:  #ffffff;
	<?php }?>*/
}

.header_color
{
	background-color: #<?php echo $style['Style']['header_background_color']; ?>;
}
.header_tab_color
{
	background-color: #<?php echo $style['Style']['header_tab_color']; ?>;
}
.wishlist
{
	color: #<?php echo $style['Style']['tab_link_color']; ?>;
}
.left_column_border
{
	border-right-color: #<?php echo $style['Style']['border_color'];?>;
}
.tab_font_style
{
	font-family: <?php echo $style['Style']['tab_font_style'];?>;
}
#menu a {
	font-family: <?php echo $style['Style']['menu_font_style'];?>;
}
#popup-content 
{
	<?php if($style['Style']['quick_view_border_color'] =='') : ?>
			border: 10px solid #e61873; 
		<?php else : ?>
			border: 10px solid  #<?php echo $style['Style']['quick_view_border_color']; ?>;
	<?php endif; ?>
}
#MB_window
{
	<?php if($style['Style']['quick_view_border_color'] =='') : ?>
		border: 10px solid #E61873; 
	<?php else : ?>
		border: 10px solid  #<?php echo $style['Style']['quick_view_border_color']; ?>;
	<?php endif; ?>

}
/*div.acwrap1, .results, .result_separator
{
	border-color: #<?php echo $style['Style']['border_color']; ?> !important;
}*/

.boxTitle
{
	background-color:#<?php echo $style['Style']['tab_background_color']; ?> ;
	color:#<?php echo $style['Style']['tab_menu_color']; ?> ;
}

.options, .optionsCtry, .optionsCat
{
	border-color: #<?php echo $style['Style']['border_color']; ?>;
}	
.results_background
{	
	<?php if($style['Style']['results_background'] =='') : ?>
		background-color: #<?php echo $style['Style']['tab_content_background_color']; ?>;
	<?php else : ?>
		background-color: #<?php echo $style['Style']['results_background']; ?>;
	<?php endif; ?>
	border-color: #<?php echo $style['Style']['border_color']; ?>;
}
.filter_background
{
	background-color:#<?php echo $style['Style']['filter_background']; ?> ;
}

.header_tab_text_color, #header_top a
{
	<?php echo ($style['Style']['header_tab_text_color'] != '')? 'color: #'. $style['Style']['header_tab_text_color']. ';': "color:#333"; ?>
}