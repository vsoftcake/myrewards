<style type="text/css">
#content
{
	border-left:1px solid #cccccc;
	border-right:1px solid #cccccc;
	border-bottom:1px solid #cccccc;
	margin-left:20px !important;
	padding-left:18px !important;
	width:913px;
}
div.modalPopupTransparent 
{
    position:fixed;
    top:0%;
    left:0%;
    width:100%;
    height:100%;
    background-color:#F0F0F0;
    z-index:1001;
    -moz-opacity:0.9;
    opacity:.80;
    filter:alpha(opacity=80);
}
div.modalPopupWindow 
{
    position:absolute;
   	top:100px;
   	border:10px solid white;
   	width:1000px;
    z-index:1002;
}
.fontstyle
{
	padding:5px;
	font-size:15px;
	font-family:arial;
}
.rounded-corners_bottom
{
	-moz-border-radius: 0px 0px 10px 10px; /* Firefox */
	-webkit-border-radius: 0px 0px 10px 10px; /* Safari, Chrome */
	border-radius: 0px 0px 10px 10px; /* CSS3 */
}
.rounded-corners_button
{
	-moz-border-radius: 5px 5px 5px 5px; /* Firefox */
	-webkit-border-radius: 5px 5px 5px 5px; /* Safari, Chrome */
	border-radius: 5px 5px 5px 5px; /* CSS3 */
}
.rounded-corners_search_left
{
	-moz-border-radius: 10px 0px 0px 10px; /* Firefox */
	-webkit-border-radius: 10px 0px 0px 10px; /* Safari, Chrome */
	border-radius: 10px 0px 0px 10px; /* CSS3 */
}
.rounded-corners_search_right
{
	-moz-border-radius: 10px 10px 10px 10px; /* Firefox */
	-webkit-border-radius: 10px 10px 10px 10px; /* Safari, Chrome */
	border-radius: 10px 10px 10px 10px; /* CSS3 */
}
.offer_ribbon
{
	background-image: url("/files/newimg/special_offer2.png");
    background-repeat: no-repeat;
    color: #FFFFFF;
    display: inherit;
    font-size: 11px;
    height: 80px;
    margin-left: -35px;
    line-height: 13px;
	position: relative;
    text-align: left;
    margin-top: -147px;
    width: 79px;
}
.special_offer
{
	height:250px;
	width:220px;
	float:left;
	padding:3px;
	background-image:url('/files/newimg/result_bg_home.png');
	background-repeat:no-repeat;
}
.highlight 
{
    color:#e41c24;
}
.favourites
{
	float: left; 
	background-image: url('/files/newimg/logo_bg.png'); 
	height: 135px; 
	width: 132px; 
	margin-top: 5px; 
	text-align: center;
}
#menup li:hover ul, #menup li.over ul 
{
	display:block !important;
}
#menup li:hover ul ul, #menup li.over ul ul 
{
	display:none !important;
}
#menup a {
	display:block;
	text-decoration:none;
	padding:0 10px;
	height:20px;
}
#menup a:hover {
	color:#fff;
}
#menup ul a {
	border-right:none;
	border-right:1px solid #fff;
	border-bottom:1px solid #fff;
	border-left:1px solid #fff;
}
#menup li ul {
	position:absolute; 
	margin-left:0em;
	display:none;
	line-height: 15px;
	margin-left: -2px;
	z-index:1001;
}
#menup ul li ul {
	margin-top:-3em;
	margin-left:7em;
}
#menup li {
	float:left;
	position:relative;
	/*line-height: 1.5em;*/
	width:auto;
}
#menup li ul li
{
	margin-top:8px;
	line-height: 1.3em;
	white-space: nowrap;
	padding-left:10px;
	float:none;
}
#menup li ul li a
{
	font-size:12px;
}
.menup_color a
{
	padding-left:10px;
	padding-right:10px;
	font-size:15px;
	margin-top:-8px;
	font-weight:bold;
}
.rounded-corners 
{
	-moz-border-radius: 10px 10px 0px 0px; /* Firefox */
	-webkit-border-radius: 10px 10px 0px 0px; /* Safari, Chrome */
	border-radius: 10px 10px 0px 0px; /* CSS3 */
}
.tr 
{
	height:50px;
}
.admin_button
{
	height: 26px;
	padding-bottom: 2px;
	border-radius: 5px 5px 5px 5px;
	background-color: #0066cc;
	border: 1px solid #cccccc;
	color: #FFFFFF; 
	cursor: pointer;
	font-size:13px;
}
</style>
<?php echo $this->Html->script('jscolor'); ?>
<script type="text/javascript">
function openModalWindow() 
{
	$("#modalWindow").dialog({width: 985, modal: true, "position": "top"});
}

function getBrowserWindowSize() {
    var winW = 630, winH = 460;

    if (parseInt(navigator.appVersion)>3) {
        if (navigator.appName=="Netscape") {
            winW = window.innerWidth;
            winH = window.innerHeight;
        }
        if (navigator.appName.indexOf("Microsoft")!=-1) {
            winW = document.body.offsetWidth;
            winH = document.body.offsetHeight;
        }
    }

    var rval = {
        width: winW,
        height: winH
    };

    return rval;
}

function getdata()
{
	var tab_text_color= document.getElementById('tab_text_color').value;
	var tab_menu_text_color= document.getElementById('tab_menu_text_color').value;
	var tab_background_color= document.getElementById('tab_background_color').value;
	var tab_content_background_color= document.getElementById('tab_content_background_color').value;
	var tab_link_color= document.getElementById('tab_link_color').value;
	var border_color= document.getElementById('border_color').value;
	var button_text_color= document.getElementById('button_text_color').value;
	var button_border_color= document.getElementById('button_border_color').value;
	var button_background_color= document.getElementById('button_background_color').value;
	var h3_color = document.getElementById('h3_color').value;
	var link_color = document.getElementById('link_color').value;
	var body_background_color = document.getElementById('body_background_color').value;
	var footer_text_color = document.getElementById('footer_text_color').value;	
	var footer_background_color = document.getElementById('footer_background_color').value;
	var menu_font_style = document.getElementById('menu_font_style').value;
	var tab_font_style = document.getElementById('tab_font_style').value;
	var font_style = document.getElementById('font_style').value;	
		
	if((!document.getElementById('StyleBackgroundShadeTtb').checked==true) && (!document.getElementById('StyleBackgroundShadeBtp').checked==true) && (!document.getElementById('StyleBackgroundShadeGlassy').checked==true) && (!document.getElementById('StyleBackgroundShadeNone').checked==true))
	{
		var background = "";
	}
	if(document.getElementById('StyleBackgroundShadeTtb').checked==true)
	{
		var background = "url(/files/newimg/ttb.png)";
	}
	else if(document.getElementById('StyleBackgroundShadeBtp').checked==true)
	{
		var background = "url(/files/newimg/btt.png)";
	}
	else if(document.getElementById('StyleBackgroundShadeGlassy').checked==true)
	{
		var background = "url(/files/newimg/box_glass_bg.png) 0 -5px";
	}
	else if(document.getElementById('StyleBackgroundShadeNone').checked==true)
	{
		var background = "";
	}

	var disp = "''";
			
	disp = <?php echo $this->element('preview_page');?>;
	$('#modalWindow').html(disp);
	openModalWindow();
}

function searchResultsPreview()
{   
	var tab_text_color= document.getElementById('tab_text_color').value;
	var tab_menu_text_color= document.getElementById('tab_menu_text_color').value;
	var tab_background_color= document.getElementById('tab_background_color').value;
	var tab_content_background_color= document.getElementById('tab_content_background_color').value;
	var tab_link_color= document.getElementById('tab_link_color').value;
	var border_color= document.getElementById('border_color').value;
	var button_text_color= document.getElementById('button_text_color').value;
	var button_border_color= document.getElementById('button_border_color').value;
	var button_background_color= document.getElementById('button_background_color').value;
	var h3_color = document.getElementById('h3_color').value;
	var link_color = document.getElementById('link_color').value;
	var body_background_color = document.getElementById('body_background_color').value;
	var footer_text_color = document.getElementById('footer_text_color').value;	
	var footer_background_color = document.getElementById('footer_background_color').value;
	var menu_font_style = document.getElementById('menu_font_style').value;
	var tab_font_style = document.getElementById('tab_font_style').value;
	var font_style = document.getElementById('font_style').value;
		
	if((!document.getElementById('StyleBackgroundShadeTtb').checked==true) && (!document.getElementById('StyleBackgroundShadeBtp').checked==true) && (!document.getElementById('StyleBackgroundShadeGlassy').checked==true) && (!document.getElementById('StyleBackgroundShadeNone').checked==true))
	{
		var background = "";
	}
	if(document.getElementById('StyleBackgroundShadeTtb').checked==true)
	{
		var background = "url(/files/newimg/ttb.png)";
	}
	else if(document.getElementById('StyleBackgroundShadeBtp').checked==true)
	{
		var background = "url(/files/newimg/btt.png)";
	}
	else if(document.getElementById('StyleBackgroundShadeGlassy').checked==true)
	{
		var background = "url(/files/newimg/box_glass_bg.png) 0 -5px";
	}
	else if(document.getElementById('StyleBackgroundShadeNone').checked==true)
	{
		var background = "";
	}

	var disp ="''";
			
	disp = <?php echo $this->element('preview_search_result_page');?>;
	$('#modalWindow').html(disp);
	openModalWindow();
}

function moreInfoPreview()
{
	var tab_text_color= document.getElementById('tab_text_color').value;
	var tab_menu_text_color= document.getElementById('tab_menu_text_color').value;
	var tab_background_color= document.getElementById('tab_background_color').value;
	var tab_content_background_color= document.getElementById('tab_content_background_color').value;
	var tab_link_color= document.getElementById('tab_link_color').value;
	var border_color= document.getElementById('border_color').value;
	var button_text_color= document.getElementById('button_text_color').value;
	var button_border_color= document.getElementById('button_border_color').value;
	var button_background_color= document.getElementById('button_background_color').value;
	var h3_color = document.getElementById('h3_color').value;
	var link_color = document.getElementById('link_color').value;
	var body_background_color = document.getElementById('body_background_color').value;
	var footer_text_color = document.getElementById('footer_text_color').value;	
	var footer_background_color = document.getElementById('footer_background_color').value;
	var menu_font_style = document.getElementById('menu_font_style').value;
	var tab_font_style = document.getElementById('tab_font_style').value;
	var font_style = document.getElementById('font_style').value;
		
	if((!document.getElementById('StyleBackgroundShadeTtb').checked==true) && (!document.getElementById('StyleBackgroundShadeBtp').checked==true) && (!document.getElementById('StyleBackgroundShadeGlassy').checked==true) && (!document.getElementById('StyleBackgroundShadeNone').checked==true))
	{
		var background = "";
	}
	if(document.getElementById('StyleBackgroundShadeTtb').checked==true)
	{
		var background = "url(/files/newimg/ttb.png)";
	}
	else if(document.getElementById('StyleBackgroundShadeBtp').checked==true)
	{
		var background = "url(/files/newimg/btt.png)";
	}
	else if(document.getElementById('StyleBackgroundShadeGlassy').checked==true)
	{
		var background = "url(/files/newimg/box_glass_bg.png) 0 -5px";
	}
	else if(document.getElementById('StyleBackgroundShadeNone').checked==true)
	{
		var background = "";
	}

	var disp ="''";
			
	disp = <?php echo $this->element('preview_more_info');?>;
	$('#modalWindow').html(disp);
	openModalWindow();
}

document.write('<style type="text/css">.tabber{display:none;}<\/style>');
function confirmation(name,id)
{
	var answer = confirm("Are you sure you want to delete style "+name)
	if (answer)
	{
		location.href="/admin/styles/delete/"+id;
	}
}
</script>
<form action="" method="post" enctype="multipart/form-data">
	<div style="width:953px;margin-left:-14px;">
		<div style="width:949px;height:50px;">
			<div style=" height: 40px; float:left; width: 610px;padding-left:37px;">
		       	<div style=" height: 40px; float:left;padding-top:5px;">
		        	<img title="Styles" alt="Styles" src="/files/admin_home_icons/styles.png" style="width:40px;height:40px;">
		        </div>
		        <div style=" height: 40px;float:left;padding-left:10px;">
		        	<h2>Edit Style <?php if($this->Form->value('Style.name')!=''): ?>- <?php echo $this->Form->value('Style.name') ?> <?php endif;?></h2>
		        </div> 
			</div>
	        <div class="actions" style="height: 40px; float:left;  width: 300px;">
				<ul style="padding-top:20px;margin-left:-28px;">
					<li style="display:inline;"><?php echo $this->Form->button('Cancel', array('onclick' => "location.href='".$this->Session->read('redirect')."'",
					'class'=>'admin_button','style'=>'width:60px;', 'type'=>'button'));  ?></li>
					<?php if($this->Form->value('Style.id')!=''): $name=$this->Form->value('Style.name');$id=$this->Form->value('Style.id');?>
					<li style="display:inline;"><?php echo $this->Form->button('Delete Style', array('onclick' => "confirmation('$name',$id)",'style'=>'width:100px;','class'=>'admin_button','type'=>'button'));  ?></li>
		        		<li style="display:inline;">
						<button class="admin_button" style="width:50px" type="submit" name="copy" >Copy</button>
					</li>
		        	<?php endif;?>
		        	<li style="display:inline;"><?php echo $this->Form->button('Submit', array('class'=>'admin_button','style'=>'width:60px;','type'=>'submit'));  ?></li>
		        </ul>
		        <div id="modalBackgroundDiv" class="modalPopupTransparent" style="display: none;"> </div>
				<div id="modalWindow" class="modalPopupWindow" style="display: none; "></div>
			</div>
		</div>

		<?php echo $this->Form->input('Style.id', array('type' => 'hidden')); ?>
		
		<div class="tabber" id="mytab1">
			<div class="tabbertab"  style="height:250px">
	  			<h2>General Information</h2>
				<h4>Style General Information</h4>
	     		<div style="background-color:#EFEFEF;border:1px solid #cccccc;padding:10px;width:790px;min-height:175px;overflow:hidden">
					<table>
						<tr  class="tr">
							<td class="bt" style="width:140px"><strong>Name</strong></td>
							<td class="bt"><?php echo $this->Form->input('Style.name', array('div'=>false, 'label'=>false)); ?></td>
	                        <td class="bl"><?php echo $this->Form->error('Style.name', array('required' => 'Name required', 'unique' => 'Name already in use'), array('required' => 'Name required')); ?></td>
						</tr>
						<tr class="tr">
							<td style="white-space:nowrap;padding-right:25px;"><strong>Body background color</strong></td>
							<td>
								<?php echo $this->Form->input('Style.body_background_color', array('div'=>false, 'label'=>false, 'class' => 'color', 'id' =>'body_background_color' )); ?>
								<span style="background-color:#<?php echo $this->Form->value('Style.body_background_color'); ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
							</td>
						</tr>
						<tr class="tr">
							<td><strong>Body font style</strong></td>
							<td>
								<?php echo $this->Form->input('Style.font_style', array('div'=>false, 'label'=>false, 'type'=>'select','id' =>'font_style','options'=>$font_styles,'empty'=>'Please Select Font' )); ?>
								<br/>
								<sub>Font style for content</sub>
							</td>
						</tr>
					</table>
			 	</div>

		       	<div align="center" style="padding-top:10px">
					<button type="button" class="admin_button" onclick="getdata()">Home Preview</button>
					<button type="button" class="admin_button" onclick="searchResultsPreview()">Results Preview</button>
					<button type="button" class="admin_button" onclick="moreInfoPreview()">View Preview</button>
					<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(1)">Next</button>
		       	</div>
			</div>
			
			<div class="tabbertab"  style="min-height:250px;overflow:hidden;">
	  			<h2>Menu, Header And Footer</h2>
			 	<h4>Menu, Header And Footer</h4>
     			<div style="background-color:#EFEFEF;border:1px solid #cccccc;padding:10px;width:790px;min-height:160px;overflow:hidden;padding-top:20px">
					<div style="float:left;padding-right:25px;">	
						<table>	
							<tr class="tr">
								<td style="padding-right: 5px; padding-bottom: 24px;"><strong>Menu text colour</strong></td>
								<td>
									<?php echo $this->Form->input('Style.tab_menu_color', array('div'=>false, 'label'=>false, 'class' => 'color', 'id' =>'tab_menu_text_color' )); ?>
									<span style="background-color:#<?php echo $this->Form->value('Style.tab_menu_color'); ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
									<br/>
									<sub>Text color for Menu heading(Eg: Dashboard, <br/>How It Works, Savings Calculator,..etc )</sub>
								</td>
								<td class="bl"></td>
							</tr>
			     			<tr class="tr">
								<td style="padding-right: 5px; padding-bottom: 15px;"><strong>Header background colour</strong></td>
								<td>
									<?php echo $this->Form->input('Style.header_background_color', array('div'=>false, 'label'=>false, 'class' => 'color', 'id' =>'header_background_color' )); ?>
									<span style="background-color:#<?php echo $this->Form->value('Style.header_background_color'); ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
									<br/><sub>Background color for top header</sub>
								</td>
								<td class="bl"></td>
							</tr>
							<tr class="tr">
								<td style="padding-right: 5px; padding-bottom: 15px;"><strong>Footer Background color</strong></td>
								<td>
									<?php echo $this->Form->input('Style.footer_background_color', array('div'=>false, 'label'=>false, 'class' => 'color', 'id' =>'footer_background_color' )); ?>
									<span style="background-color:#<?php echo $this->Form->value('Style.footer_background_color'); ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
									<br/>
									<sub>background color for footer</sub>
								</td>
								<td class="bl"></td>
							</tr>
							<tr class="tr">
								<td style="padding-right: 5px; padding-bottom: 15px;"><strong>Footer text color</strong></td>
								<td>
									<?php echo $this->Form->input('Style.footer_color', array('div'=>false, 'label'=>false, 'class' => 'color', 'id' =>'footer_text_color' )); ?>
									<span style="background-color:#<?php echo $this->Form->value('Style.footer_color'); ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
									<br/>
									<sub>Text color for footer</sub>
								</td>
								<td class="bl"></td>
							</tr>
							
			     	  	</table>
					</div>
				     	 
					<div>
			     		<table>		
			     			<tr class="tr">
								<td style="padding-right: 5px; padding-bottom: 24px;"><strong>Menu font style</strong></td>
								<td>
									<?php echo $this->Form->input('Style.menu_font_style', array('div'=>false, 'label'=>false, 'type'=>'select','id' =>'menu_font_style','options'=>$font_styles ,'empty'=>'Please Select Font','style'=>'width:215px')); ?>
									<br/>
									<sub>Font style for menu(Eg: Dashboard, How It Works,<br>Savings Calculator,..etc )</sub>
								</td>
								<td class="bl"></td>
							</tr>
			     			<tr class="tr">
								<td style="padding-right: 5px; padding-bottom: 20px; padding-top: 2px;"><strong>Header tab colour</strong></td>
								<td>
									<?php echo $this->Form->input('Style.header_tab_color', array('div'=>false, 'label'=>false, 'class' => 'color', 'id' =>'header_tab_color' )); ?>
									<span style="background-color:#<?php echo $this->Form->value('Style.header_tab_color'); ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
									<br/><sub>Background color for top header menu <br>(Eg: My Account, Welcome back...., Admin)</sub>
								</td>
								<td class="bl"></td>
							</tr>
							<tr class="tr">
								<td style="padding-right: 5px; padding-bottom: 20px; padding-top: 2px;"><strong>Header tab text colour</strong></td>
								<td>
									<?php echo $this->Form->input('Style.header_tab_text_color', array('div'=>false, 'label'=>false, 'class' => 'color', 'id' =>'header_tab_text_color' )); ?>
									<span style="background-color:#<?php echo $this->Form->value('Style.header_tab_text_color'); ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
									<br/><sub>Text color for top header menu <br>(Eg: My Account, Welcome back...., Admin)</sub>
								</td>
								<td class="bl"></td>
							</tr>
						</table>	
					</div>
      		    </div>
		       	<div align="center" style="padding-top:15px">
		       		<button type="button" class="admin_button" onclick="getdata()">Home Preview</button>
					<button type="button" class="admin_button" onclick="searchResultsPreview()">Results Preview</button>
					<button type="button" class="admin_button" onclick="moreInfoPreview()">View Preview</button>
					<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(0)">Previous</button>
					<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(2)">Next</button>
		       	</div>
			</div>
			
			<div class="tabbertab">
			  	<h2>Buttons And Borders</h2>
			  	<h4>Buttons And Borders</h4>
                <div style="background-color:#EFEFEF;border:1px solid #cccccc;width:800px;padding:5px;height:175px;">
					<div style="padding:5px;float:left;width:380px">
	                	<table>	
							<tr class="tr">
								<td><strong>Border colour</strong></td>
								<td>
									<?php echo $this->Form->input('Style.border_color', array('div'=>false, 'label'=>false, 'class' => 'color', 'id' =>'border_color' )); ?>
									<span style="background-color:#<?php echo $this->Form->value('Style.border_color'); ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
								</td>
								<td class="bl"></td>
							</tr>
							<tr class="tr">
								<td><strong>Button text colour</strong></td>
								<td>
									<?php echo $this->Form->input('Style.button_color', array('div'=>false, 'label'=>false, 'class' => 'color', 'id' =>'button_text_color' )); ?>
									<span style="background-color:#<?php echo $this->Form->value('Style.button_color'); ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
								</td>
								<td class="bl"></td>
							</tr>
							<tr class="tr">
								<td><strong>Quick view border colour</strong></td>
								<td>
									<?php echo $this->Form->input('Style.quick_view_border_color', array('div'=>false, 'label'=>false, 'class' => 'color', 'id' =>'quick_view_border_color' )); ?>
									<span style="background-color:#<?php echo $this->Form->value('Style.quick_view_border_color'); ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><br/>
								</td>
								<td class="bl"></td>
					       </tr>
						</table>
			 		</div>
			 		<div style="padding:5px;">
		 		        <table>	
							<tr class="tr">
								<td><strong>Button background colour</strong></td>
								<td>
									<?php echo $this->Form->input('Style.button_background_color', array('div'=>false, 'label'=>false, 'class' => 'color', 'id' =>'button_background_color' )); ?>
									<span style="background-color:#<?php echo $this->Form->value('Style.button_background_color'); ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
								</td>
								<td class="bl"></td>
							</tr>
							<tr class="tr">
								<td><strong>Button border colour</strong></td>
								<td>
									<?php echo $this->Form->input('Style.button_border_color', array('div'=>false, 'label'=>false, 'class' => 'color', 'id' =>'button_border_color' )); ?>
									<span style="background-color:#<?php echo $this->Form->value('Style.button_border_color'); ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
								</td>
								<td class="bl"></td>
							</tr>
						</table>   
			 		</div>
			   	</div>
			 	
			 	<div align="center" style="padding-top:10px">
			 		<button type="button" class="admin_button" onclick="getdata()">Home Preview</button>
					<button type="button" class="admin_button" onclick="searchResultsPreview()">Results Preview</button>
					<button type="button" class="admin_button" onclick="moreInfoPreview()">View Preview</button>
					<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(1)">Previous</button>
					<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(3)">Next</button>
				</div>
			</div>
			 
			<div class="tabbertab">
				<h2>Tabs And Links</h2>
				<h4>Tabs And Links</h4>
				<div style="min-height: 300px;overflow:hidden;float:left; width: 770px;padding:20px;background-color:#EFEFEF;border:1px solid #cccccc;">
					<div style="padding-right:15px;float:left;width:385px;">
						<table>
							<tr class="tr">
								<td style="padding-right: 5px; padding-bottom: 25px;"><strong>Link colour</strong></td>
								<td>
									<?php echo $this->Form->input('Style.link_color', array('div'=>false, 'label'=>false, 'class' => 'color', 'id' =>'link_color' )); ?>
									<span style="background-color:#<?php echo $this->Form->value('Style.link_color'); ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
									<br/><sub>Text color for link (Eg: Product title link color in the <br/>product box, pagination box)</sub>
								</td>
								<td class="bl"></td>
							</tr>
							<tr><td style="height:5px;"></td></tr>
							<tr class="tr">
								<td style="padding-right: 5px; padding-bottom: 20px;"><strong>Tab text colour</strong></td>
								<td>
									<?php echo $this->Form->input('Style.tab_color', array('div'=>false, 'label'=>false, 'class' => 'color', 'id' =>'tab_text_color' )); ?>
									<span style="background-color:#<?php echo $this->Form->value('Style.tab_color'); ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
									<br/>
									<sub>Text color for box heading(Eg: Hot Offer!, <br/>Our Favourites )</sub>
								</td>
								<td class="bl"></td>
							</tr>
							<tr><td style="height:5px;"></td></tr>
							<tr class="tr">
								<td style="padding-right: 5px;"><strong>Tab content background <br>colour</strong></td>
								<td>
									<?php echo $this->Form->input('Style.tab_content_background_color', array('div'=>false, 'label'=>false, 'class' => 'color', 'id' =>'tab_content_background_color' )); ?>
									<span style="background-color:#<?php echo $this->Form->value('Style.tab_content_background_color'); ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
									<br/><sub>Background color for content box</sub>
								</td>
								<td class="bl"></td>
							</tr>
							<tr><td style="height:5px;"></td></tr>
							<tr class="tr">
								<td><strong>Tab header &amp; navigation <br>background colour</strong></td>
								<td> 
								   	<?php echo $this->Form->input('Style.tab_background_color', array('div'=>false, 'label'=>false, 'class' => 'color', 'id' =>'tab_background_color' )); ?>
								   	<span style="background-color:#<?php echo $this->Form->value('Style.tab_background_color'); ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
								   	<br/>
								   	<sub>Background color for menu and different options <br>for shade effects</sub>
								</td>
								<td class="bl"></td>
							</tr>
							<tr class="tr">
								<td>&nbsp;</td>
								<td>
									<?php echo $this->Form->input('Style.background_shade', array('type' => 'radio','options' => array('none'=>'Normal', 'glassy'=>'Glassy','ttb'=>'Top 2 Bottom', 'btp'=>'Bottom 2 Top'),'class'=>'input_radio','separator'=>'<br/>','legend'=>false));?>			
								</td>
								<td class="bl"></td>
							</tr>
						</table>
					</div>
					<div>
						<table>
							<tr class="tr">
								<td style="padding-right: 5px; padding-bottom: 32px;"><strong>Link hover colour</strong></td>
								<td>
									<?php echo $this->Form->input('Style.link_hover_color', array('div'=>false, 'label'=>false, 'class' => 'color', 'id' =>'link_hover_color' )); ?>
									<span style="background-color:#<?php echo $this->Form->value('Style.link_hover_color'); ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
									<br/><sub>Text color for link hover or mouseover <br>(Eg: Product title <br> link color in the product box, pagination box)</sub>
								</td>
								<td class="bl"></td>
							</tr>
							<tr><td style="height:5px;"></td></tr>
							<tr class="tr">
								<td style="padding-right: 5px; padding-bottom: 20px;"><strong>Tab link colour</strong></td>
								<td>
									<?php echo $this->Form->input('Style.tab_link_color', array('div'=>false, 'label'=>false, 'class' => 'color', 'id' =>'tab_link_color' )); ?>
									<span style="background-color:#<?php echo $this->Form->value('Style.tab_link_color'); ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
									<br/><sub>Text color for link in the content</sub>
								</td>
								<td class="bl"></td>
							</tr>
							<tr><td style="height:5px;"></td></tr>
							<tr class="tr">
								<td style="padding-right: 5px; padding-bottom: 12px;"><strong>Tab link hover colour</strong></td>
								<td>
									<?php echo $this->Form->input('Style.tab_link_hover_color', array('div'=>false, 'label'=>false, 'class' => 'color', 'id' =>'tab_link_hover_color' )); ?>
									<span style="background-color:#<?php echo $this->Form->value('Style.tab_link_hover_color'); ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
									<br/><sub>Text color for link hover or mouseover</sub>
								</td>
								<td class="bl"></td>
							</tr>
							<tr><td style="height:5px;"></td></tr>
							<tr class="tr">
								<td style="padding-right: 5px; padding-bottom: 25px;"><strong>Tab font style</strong></td>
								<td>
									<?php echo $this->Form->input('Style.tab_font_style', array('div'=>false, 'label'=>false, 'type'=>'select','id' =>'tab_font_style','options'=>$font_styles,'empty'=>'Please Select Font','style'=>'width:215px')); ?>
									<br/>
									<sub>Font style for box heading(Eg: Hot Offer!, <br>Our Favourites )</sub>
								</td>
								<td class="bl"></td>
							</tr>
						</table>
					</div>
				</div>
		 		
		 		<div align="center" style="padding-top:15px">
					<button type="button" class="admin_button" onclick="getdata()">Home Preview</button>
					<button type="button" class="admin_button" onclick="searchResultsPreview()">Results Preview</button>
					<button type="button" class="admin_button" onclick="moreInfoPreview()">View Preview</button>
					<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(2)">Previous</button>
					<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(4)">Next</button>
				</div>
			</div>
			 
			<div class="tabbertab">
				<h2>H Style</h2>
			  	<h4>H Style</h4>
			  	<div style="background-color:#EFEFEF;border:1px solid #cccccc;width:770px;height:250px;padding:20px;">
					<div style="padding:5px;float:left;width:370px">
	                	<table style="width:100%" >
							<tr class="tr">
								<td><strong>H1 colour</strong></td>
								<td>
									<?php echo $this->Form->input('Style.h1_color', array('div'=>false, 'label'=>false, 'class' => 'color', 'id' =>'h1_color' )); ?>
									<span style="background-color:#<?php echo $this->Form->value('Style.h1_color'); ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><br/>
								</td>
								<td class="bl"></td>
							</tr>
							<tr class="tr">
								<td><strong>H2 colour</strong></td>
								<td>
									<?php echo $this->Form->input('Style.h2_color', array('div'=>false, 'label'=>false, 'class' => 'color', 'id' =>'h2_color' )); ?>
									<span style="background-color:#<?php echo $this->Form->value('Style.h2_color'); ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><br/>
								</td>
								<td class="bl"></td>
							</tr>
							<tr class="tr">
								<td><strong>H3 colour</strong></td>
								<td>
									<?php echo $this->Form->input('Style.h3_color', array('div'=>false, 'label'=>false, 'class' => 'color', 'id' =>'h3_color' )); ?>
									<span style="background-color:#<?php echo $this->Form->value('Style.h3_color'); ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><br/>
								</td>
								<td class="bl"></td>
							</tr>
							<tr class="tr">
								<td><strong>H4 colour</strong></td>
								<td>
									<?php echo $this->Form->input('Style.h4_color', array('div'=>false, 'label'=>false, 'class' => 'color', 'id' =>'h4_color' )); ?>
									<span style="background-color:#<?php echo $this->Form->value('Style.h4_color'); ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><br/>
								</td>
								<td class="bl"></td>
							</tr>
							<tr class="tr">
								<td><strong>H5 colour</strong></td>
								<td>
									<?php echo $this->Form->input('Style.h5_color', array('div'=>false, 'label'=>false, 'class' => 'color', 'id' =>'h5_color' )); ?>
									<span style="background-color:#<?php echo $this->Form->value('Style.h5_color'); ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><br/>
								</td>
								<td class="bl"></td>
							</tr>
						</table>
					</div>
					<div style="padding:5px;float:left;width:370px">
					    <table style="width:100%">
					    	<tr class="tr">
								<td><strong>H1 size</strong></td>
								<td>
									<?php echo $this->Form->input('Style.h1_size', array('div'=>false, 'label'=>false)); ?>
								</td>
								<td class="bl"></td>
							</tr>
							<tr class="tr">
								<td><strong>H2 size</strong></td>
								<td>
									<?php echo $this->Form->input('Style.h2_size', array('div'=>false, 'label'=>false)); ?>
								</td>
								<td class="bl"></td>
							</tr>
							<tr class="tr">
								<td><strong>H3 size</strong></td>
								<td>
									<?php echo $this->Form->input('Style.h3_size', array('div'=>false, 'label'=>false)); ?>
								</td>
								<td class="bl"></td>
							</tr>
							<tr class="tr">
								<td><strong>H4 size</strong></td>
								<td>
									<?php echo $this->Form->input('Style.h4_size', array('div'=>false, 'label'=>false)); ?>
								</td>
								<td class="bl"></td>
							</tr>
							<tr class="tr">
								<td><strong>H5 size</strong></td>
								<td>
									<?php echo $this->Form->input('Style.h5_size', array('div'=>false, 'label'=>false)); ?>
								</td>
								<td class="bl"></td>
							</tr>
						</table>
					</div>
				</div>		
				<div align="center" style="padding-top:10px">
					<button type="button" class="admin_button" onclick="getdata()">Home Preview</button>
					<button type="button" class="admin_button" onclick="searchResultsPreview()">Results Preview</button>
					<button type="button" class="admin_button" onclick="moreInfoPreview()">View Preview</button>
					<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(3)">Previous</button>
					<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(5)">Next</button>
				</div>
			</div>
			 	
			<div class="tabbertab">
	 			<h4>Navigation</h4>
	 			<div style="background-color:#EFEFEF;border:1px solid #cccccc;width:770px;height:150px;padding:20px;">
	 				<table>
	 					<tr class="tr">
							<td><strong>Navigation background colour</strong></td>
							<td>
								<?php echo $this->Form->input('Style.nav_background_color', array('div'=>false, 'label'=>false, 'class' => 'color', 'id' =>'nav_background_color' )); ?>
								<span style="background-color:#<?php echo $this->Form->value('Style.nav_background_color'); ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
							</td>
							<td class="bl"></td>
						</tr>
						<tr class="tr">
							<td><strong>Navigation border colour</strong></td>
							<td>
								<?php echo $this->Form->input('Style.nav_border_color', array('div'=>false, 'label'=>false, 'class' => 'color', 'id' =>'nav_border_color' )); ?>
								<span style="background-color:#<?php echo $this->Form->value('Style.nav_border_color'); ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
							</td>
							<td class="bl"></td>
						</tr>
						<tr class="tr">
							<td><strong>Navigation colour</strong></td>
							<td>
								<?php echo $this->Form->input('Style.nav_color', array('div'=>false, 'label'=>false, 'class' => 'color', 'id' =>'nav_color' )); ?>
								<span style="background-color:#<?php echo $this->Form->value('Style.nav_color'); ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
							</td>
							<td class="bl"></td>
						</tr>
					</table>		
	 			</div>	
				<div align="center" style="padding-top:10px">
					<button type="button" class="admin_button" onclick="getdata()">Home Preview</button>
					<button type="button" class="admin_button" onclick="searchResultsPreview()">Results Preview</button>
					<button type="button" class="admin_button" onclick="moreInfoPreview()">View Preview</button>
					<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(4)">Previous</button>
					<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(6)">Next</button>
				</div>
			</div>			 	
			 
			<div class="tabbertab">
	 			<h4>Keyword Search</h4>
	 			<div style="background-color:#EFEFEF;border:1px solid #cccccc;width:770px;height:120px;padding:20px;">
	 				<table>
	 					<tr class="tr">
							<td><strong>Keyword search results background colour</strong></td>
							<td>
								<?php echo $this->Form->input('Style.results_background', array('div'=>false, 'label'=>false, 'class' => 'color', 'id' =>'results_background' )); ?>
								<span style="background-color:#<?php echo $this->Form->value('Style.results_background'); ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><br/>
								<sub>Background color for keyword search results</sub>
							</td>
							<td class="bl"></td>
						</tr>
						<tr class="tr">
							<td><strong>Keyword search filter background colour</strong></td>
							<td>
								<?php echo $this->Form->input('Style.filter_background', array('div'=>false, 'label'=>false, 'class' => 'color', 'id' =>'filter_background' )); ?>
								<span style="background-color:#<?php echo $this->Form->value('Style.filter_background'); ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><br/>
								<sub>Background color for keyword search filter</sub>
							</td>
							<td class="bl"></td>
						</tr>
	 				</table>
	 			</div>
	 			<div align="center" style="padding-top:10px">
					<button type="button" class="admin_button" onclick="getdata()">Home Preview</button>
					<button type="button" class="admin_button" onclick="searchResultsPreview()">Results Preview</button>
					<button type="button" class="admin_button" onclick="moreInfoPreview()">View Preview</button>
					<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(5)">Previous</button>
					<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(7)">Next</button>
				</div>
	 		</div>
	 		
			<div class="tabbertab">
				<h2>Log</h2>
				<div style="background-color:#EFEFEF;border:1px solid #cccccc;width:770px;height:120px;padding:20px;">
					<div class="edit_new">
						<table style="width:85%">
					    	<tr>
								<td class="bt" valign="top"><strong>Notes</strong><br/><sub>for change log</sub></td>
								<td class="bt br">
									<?php echo $this->Form->input('Log.notes',array('rows' => '4', 'cols' => '40', 'div'=>false, 'label'=>false)); ?>
								</td>
							</tr>
							<tr>
								<td class="bb bt"></td>
								<td class="bb br bt" style="padding-top:10px;padding-left:50px">
									<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(6)">Previous</button>
								</td>
							</tr>
				     	</table>
						<?php echo $this->Form->input('Style.id', array('type' => 'hidden')); ?>
					</div>
				</div>
				<br /><br />
				<div style="border:1px solid #cccccc;width:770px;min-height:105px;overflow:hidden;padding:20px;">   
					<div class="list" id="log_list">
						<h4>Change Log</h4>
						<div id="Log" style="margin:0;">Retrieving changes...</div>
						<script type="text/javascript">
						$(function() {
							$.ajax({
								url: "/admin/logs/ajax_list_new/Style/<?php echo $this->Form->value('Style.id');?>",
								type: "GET",
								success: function (data) {
									$('#Log').html(data);
								}
							});
						});</script>
					</div> 
			 	</div>
			</div>
		</div>
	</div>	
</form>