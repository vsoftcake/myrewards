<div id="gogleMap" height="280px;">
		 <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3&sensor=true"></script>
		<?php
			$ip = $_SERVER['REMOTE_ADDR'];
			$i=0;
			$user_country=(unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip='.$ip)));
			foreach($google_map as $google)
			{
				foreach($google as $id=>$detail)
			   	{  
			   		$i=1;
			   		$pid = $detail['Product']['id']; 
			   		$mid = $detail['MerchantAddress']['id'];
		   	    	$productname=strip_tags($detail['Product']['name']);
		   	    	if(strlen($productname)>25)
		   	    		$productname = substr($productname,0,25).'...';
		   	    	else
		   	    		$productname = $productname;
		   	    		
		   	    	$highlight=strip_tags($detail['Product']['highlight']);
		   	    	
		   	    	if(strlen($highlight)>25)
		   	    		$highlight = substr($highlight,0,25).'...';
		   	    	else
		   	    		$highlight = $highlight;
		   	    	$data=strip_tags($detail['Product']['details']);
		   	    	if(strlen($data)>75)
		   	    		$data = substr($data,0,75).'...';
		   	    	else
		   	    		$data = $data;
		   	      $data = preg_replace("/[^a-zA-Z0-9s]/", " ", $data);
		  			$map_infowindow ="<div><a style='font-weight:bold;font-size:13px;text-decoration:none;' title='' href='/products/view/$pid/$mid'>".$productname."</a></div><div style='color:red;'>".$highlight."</div><div  style='width:225px;border-width:1px; border-style:dashed; border-color:#cccccc; margin-top:3px;'>".addslashes($data)."</div>";
				  		
				  	$points[$id]=array('Detail' => array('title' => '', 'html' => $map_infowindow, 'latitude' => $detail['MerchantAddress']['latitude'], 'longitude' => $detail['MerchantAddress']['longitude'] ));
			   
			   	}
			} 
			$default = array('type'=>'0','zoom'=>14,'lat'=>$user_country['geoplugin_latitude'],'long'=>$user_country['geoplugin_longitude']);
		   	
		   	echo $this->GoogleMap->map_and_markers($points,$default, $style = 'width:344px; height: 270px');
		?> 
		
  <div style='font-size:15px;color:red;margin-top:5px;position:relative;height:20px;'> 
    <?php if($i==0) echo "<strong>No Offers found</strong>" ?>
  </div>
</div>