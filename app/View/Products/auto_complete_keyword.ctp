<?php
$merList = array();$pList = array();$result = array();
foreach($products as $product):
	$key = $product['name'];
	$merchant = $product['merchant'];
	if(strtolower($merchant) == strtolower($key)) :
		if(!in_array($key,$pList)):
			array_push($result, array("id"=>$key, "label"=>$key, "value"=>strip_tags($key)));
			array_push($pList, $key);
		endif;
	else :
		if(!in_array($key,$pList)):
			array_push($result, array("id"=>$key, "label"=>$key, "value"=>strip_tags($key)));
			array_push($pList, $key);
		endif;
		if(!in_array($merchant,$merList)):
			array_push($result, array("id"=>$merchant, "label"=>$merchant, "value"=>strip_tags($merchant)));
			array_push($merList, $merchant);
		endif;
	endif;
endforeach;
echo json_encode($result); ?>