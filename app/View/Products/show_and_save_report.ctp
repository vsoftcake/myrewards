	<?php echo $this->element('module2', array('module2' => 'Keyword_search', 'title' => 'Search'));?>
	<?php echo $this->element('message'); ?>
	<a class="no_print" href="javascript:window.history.back()" id="back" style="display:none;"><img src="/files/newimg/back.png" alt="Back" title="Back"/></a>
	<?php if ($page['ClientPage']['h1'] != '') : ?>
		<h1><?php echo $page['ClientPage']['h1']; ?></h1>
	<?php endif; ?>
	<?php if ($page['ClientPage']['h2'] != '') : ?>
		<h2><?php echo $page['ClientPage']['h2']; ?></h2>
	<?php endif; ?>
	<?php if ($page['ClientPage']['h3'] != '') : ?>
		<h3><?php echo $page['ClientPage']['h3']; ?></h3>
	<?php endif; ?>
	<?php if ($page['ClientPage']['content'] != '') : ?>
		<?php echo $page['ClientPage']['content']; ?>
	<?php endif; ?>
<h1><?php echo $state; ?></h1>
<table style="width:800px">
	<tr>
		<?php foreach($categories as $column) : ?>
		<td style="vertical-align:top;padding-right:10px;width:50%">
			<table>
			<?php foreach($column as $category => $products) : ?>
				<tr>
					<td colspan="2"><h3><?php echo $category; ?></h3></td>
				</tr>
				<?php foreach($products as $product) : ?>
					<tr>
						<td class="dots" colspan="2">&nbsp;</td>
					</tr>
					<tr>
						<td style="vertical-align:top;padding: 0 5px 10px 1px" rowspan="2">
							<h5><?php echo ($product['MerchantAddress']['name'] != '')? $product['MerchantAddress']['name']: $product['Product']['name'] ; ?></h5>
							<br/>
							<?php echo $product['MerchantAddress']['address1']; ?>
							<?php echo $product['MerchantAddress']['address2']; ?>
							<?php echo $product['MerchantAddress']['suburb']; ?>
							<br/>
							<?php echo ($product['MerchantAddress']['phone'] != '')? 'Tel '. $product['MerchantAddress']['phone']: ''; ?>
						</td>
						<td style="vertical-align:top;padding: 4px 5px 10px 1px">
							<b><?php echo $product['Product']['highlight']; ?></b>
						</td>
					</tr>
					<tr>
						<td style="vertical-align:top;padding: 0 5px 10px 1px">
							<?php echo $product['Product']['details']; ?>
						</td>
					</tr>
					
				<?php endforeach; ?>
			<?php endforeach; ?>
			</table>
		</td>
		<?php endforeach; ?>
	</tr>
</table>
<span class="no_print"><br/><br/><button type="submit" class="button_search_module rounded_corners_button" onclick="window.print(); return false;">Print</button></span>