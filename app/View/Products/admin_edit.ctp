<?php echo $this->element('tinymce');?>
<?php echo $this->Html->script('jscolor'); ?>
<style type="text/css">
	#content
	{
		border-left:1px solid #cccccc;
		border-right:1px solid #cccccc;
		border-bottom:1px solid #cccccc;
		margin-left:20px !important;
		padding-left:18px !important;
		width:913px;
	}
	.rounded-corners 
	{
		-moz-border-radius: 10px 10px 0px 0px; /* Firefox */
		-webkit-border-radius: 10px 10px 0px 0px; /* Safari, Chrome */
		border-radius: 10px 10px 0px 0px; /* CSS3 */
	}
	.tr 
	{
		height:35px;
	}
	.tr1
	{
		height:85px;
	}
	.admin_button
	{
		height: 26px;
		padding-bottom: 3px;
		border-radius: 5px 5px 5px 5px;
		background-color: #0066cc;
		border: 1px solid #cccccc;
		color: #FFFFFF; 
		cursor: pointer;
		font-size:13px;
	}
</style>

<script type="text/javascript">
	document.write('<style type="text/css">.tabber{display:none;}<\/style>');
	function confirmation(name,id)
    {
		var answer = confirm("Are you sure you want to delete product '<?php echo $this->Form->value('Product.name') ?>'")
		if (answer)
		{
			location.href="/admin/products/delete/<?php echo $this->Form->value('Product.id') ?>";
		}
    }
    function loadClientsList(program)
    	{
    		var pid = program.value;
    		if(program.checked)
    		{
    			$.ajax({
    				url: '/admin/products/load_clients/'+pid,
    				type: "GET",
    				success: function (data) {
    					$('#clients').append(data);
    				}
    			});
    		}
    		else
    		{
    			$("#ic"+pid).remove();
    		}
    	}
    
    	function selectAll(pid)
    	{
    		var aChecked = document.getElementById('checkall'+pid).checked;
    		var aId = "ic"+pid;
    		var collection = document.getElementById(aId).getElementsByTagName('INPUT');
    	    	for (var x=0; x<collection.length; x++) {
    		if (collection[x].type.toUpperCase()=='CHECKBOX')
    		    collection[x].checked = aChecked;
    	    }
	}
function enableClientProgram(){
	var val =$('input[name="data[Product][product_type]"]:checked').val();
	if(val == "-1"){
		$("#productPrograms :input").attr("disabled", true);
		$("#clients :input").attr("disabled", true);
	}else{
		$("#productPrograms :input").attr("disabled", false);
		$("#clients :input").attr("disabled", false);
	}
}
</script>
<form action="<?php echo $this->Html->url('/admin/products/edit/'. $this->Html->value('Product.id')); ?>" method="post" id="edit_form" enctype="multipart/form-data">
<div style="width:953px;margin-left:-14px;">
	<div id="message"></div>
	<div style="width:949px;height:60px;">
		<div style=" height: 40px; float:left; width: 570px;padding-left:37px;">
	       	<div style=" height: 40px; float:left;padding-top:5px;">
	        	<img title="Products" alt="Products" src="/files/admin_home_icons/products.png" style="width:40px;height:40px;">
	        </div>
	        <div style=" height: 40px;float:left;padding-left:10px;">
	        	<?php 
				if(strlen($this->Form->value('Product.name'))>30) :
					$prodName = wordwrap($this->Form->value('Product.name'), 30, "<br/>");
				else : 
					$prodName = $this->Form->value('Product.name');
				endif;
			?>
	        	<h2>Edit Product <?php if($this->Form->value('Product.name')!=''): ?>- <?php echo $prodName; ?> <?php endif;?></h2>
	        </div> 
		</div>
        <div class="actions" style=" height: 40px; float:left;  width: 340px;">
			<ul style="padding:20px;margin-left:-28px;">
				<li style="display:inline;"><?php echo $this->Form->button('Cancel', array('onclick' => "location.href='".$this->Session->read('redirect')."'",
				'class'=>'admin_button','style'=>'width:60px;', 'type'=>'button'));  ?></li>
				<?php if($this->Form->value('Product.id')!=''):
				$conname=$this->Form->value('Product.name');$conid=$this->Form->value('Product.id'); ?>
					<li style="display:inline;"><?php echo $this->Form->button('Delete Product', array('onclick' => "confirmation('$conname','$conid')",'style'=>'width:110px;','class'=>'admin_button','type'=>'button'));  ?></li>
					<li style="display:inline;">
						<button class="admin_button" style="width:50px" type="submit" name="copy">Copy</button>
					</li>
		        <?php endif;?>
		        	<li style="display:inline;">
		        		<button class="admin_button" type="submit">Submit</button>
		        	</li>
	        	</ul>
		</div>
	</div>

	
		<div class="tabber" id="mytab1">

			<div class="tabbertab"  style="min-height:615px;overflow:hidden">
	  			<h2>General Information</h2>
	    			<h4>Product General Information</h4>
				<div style="background-color:#EFEFEF;border:1px solid #cccccc;padding:10px;width:790px;min-height:475px;overflow:hidden">
					<div style="padding:20px;">
						<table>
							<tr class="tr">
								<td class="bt" valign="top"><strong>Merchants</strong></td>
								<td class="bt">
									<?php if(empty($mer_id)) :
										echo $this->Form->input('Product.merchant_id',array('type'=>'select','label'=>false,'options'=>$merchantslist,'empty'=>' ','style'=>'width:350px')); 
									else : 
									 	echo $this->Form->input('Product.merchant_id',array('default'=>$mer_id, 'type'=>'select','label'=>false,'options'=>$merchantslist,'empty'=>' ','style'=>'width:350px'));
									endif;
										$options = array('url' => 'update_merchantaddresses','update' => 'MerchantAddressMerchantAddress');
							           	echo $this->Ajax->observeField('ProductMerchantId', $options);?> *
							           	<br/><sub>Select Merchant for the product</sub>
								</td>
								<td class="bl"><?php echo $this->Form->error('Product.merchant_id',array('required' => 'Merchant required'), array('required' => 'Merchant required')); ?></td>
							</tr>
							<tr class="tr">
								<td class="bt" style="padding-right:33px"><strong>ID</strong></td>
								<td class="bt"><?php echo $this->Form->value('Product.id'); ?></td>
								<td class="bl"></td>
							</tr>
							<tr class="tr">
								<td style="padding-right:33px"><strong>Name</strong></td>
								<td><?php echo $this->Form->input('Product.name', array('div'=>false, 'label'=>false)); ?></td>
								<td class="bl"><?php // echo $this->Form->error('Product.name', array('required' => 'Name required'), array('required' => 'Name required')); ?></td>
							</tr>
							<tr class="tr">
								<td style="padding-right:29px;padding-top:10px;vertical-align:top"><strong>Details</strong></td>
								<td style="padding-top:10px"><?php echo $this->Form->input('Product.details', array('div'=>false, 'label'=>false)); ?></td>
							</tr>
							<tr class="tr">
								<td style="padding-right:29px;padding-top:10px;vertical-align:top"><strong>Text</strong></td>
								<td style="padding-top:10px"><?php echo $this->Form->input('Product.text', array('div'=>false, 'label'=>false)); ?></td>
							</tr>
							<tr class="tr">
								<td style="padding-top:10px;vertical-align:top;"><strong>Keyword</strong></td>
								<td style="padding-top:10px"><?php echo $this->Form->input('Product.keyword', array('div'=>false, 'label'=>false)); ?></td>
							</tr>
							<tr class="tr">
								<td style="padding-right:5px;padding-top:10px;vertical-align:top;"><strong>Terms & Conditions</strong></td>
								<td style="padding-top:10px"><?php echo $this->Form->input('Product.terms_and_conditions', array('div'=>false, 'label'=>false, 'style'=>'width:400px !important;height:40px;')); ?></td>
							</tr>
						</table>
					</div>
				</div>
	            <br/>
		       	<div>
					<table>
						<tr class="tr">
							<td style="width:315px;"></td>  
							<td>
								<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(1)">Next</button>
							</td>
						</tr>
					</table>
	       	   	</div>
	 		</div>
     		
			<div class="tabbertab"  style="min-height:480px;overflow:hidden;">
				<h2>Eligibility</h2>
				<h4>Eligibility </h4>
		    	<div style="background-color:#EFEFEF;border:1px solid #cccccc;padding:10px;width:790px;min-height:356px;overflow:hidden">
					<div style="padding:20px">	
						<table>
							<tr>
								<td colspan="2" style="padding-bottom:10px"><strong>Choose Product Type</strong><br/>
								<?php echo $this->Form->input('Product.product_type', array('type' => 'radio','style'=>'margin-left:60px','options' => array('-1'=>'Generic', '0'=>'Exclusive'),'class'=>'input_radio','separator'=>'','legend'=>false,'default'=>'-1', 'onClick'=>'enableClientProgram()'));?>
								<sub style="margin-left:60px">Generic - available to all clients with selected category</sub>
								<sub style="margin-left:60px">Exclusive - available to selected clients only</sub>

								</td>
							</tr>
							<tr class="tr1">
								<td valign="top"><strong>Categories</strong></td>
								<td valign="top">
									<?php echo $this->Form->input('Category.Category', array('multiple'=>'multiple', 'div'=>false, 'label'=>false, 'options'=>$categories)); ?>
								</td>
							</tr>
						</table>
						<table>
							<tr class="tr1">
								<td valign="top" style="width:200px">
									<table>
										<tr>
											<td valign="top" style="width:70px"><strong>Included programs</strong></td>
											<td valign="top">
												<div style="width:300px;min-height:100px">
													<div id="productPrograms" style="float:left;height:350px;width:295px;background-color:#fff;overflow:auto;border:1px solid #ccc;margin-left:10px;">
														<?php 
															$check = "checked= 'checked'";
															$pTypeDisable = $this->request->data['Product']['product_type']==0?"":"disabled='disabled'";
														?>
														<?php foreach ($programs as $pval=>$pname):?>
															<input type="checkbox" id="ProgramProgram" <?php if(in_array($pval,$programsList)) echo $check; ?> <?php echo $pTypeDisable?> value="<?php echo $pval;?>" onclick="loadClientsList(this)" name="data[Program][Program][]" class="checkbox">
															&nbsp;&nbsp;<?php echo $pname ?><br>
														<?php endforeach; ?>
													</div>
												</div>
											</td>
										</tr>
									</table>
								</td>
								<td style="padding-left:18px" valign="top">
									<table>
										<tr>
											<td valign="top" style="width:70px"><strong>Included Clients</strong></td>
											<td valign="top">
												<div style="width: 250px; overflow: auto; height: 350px;background-color:#fff;border:1px solid #ccc;" id="clients">
													<?php $pid=0;?>
													<?php foreach ($clients as $client):?>
													<?php if($pid != $client['Client']['program_id']){?>
														<?php if($pid>0){?></div><?php }?>
													<div style="width:245px;background-color:#fff;overflow:auto;border:1px solid #ccc;padding-bottom:5px;" id="ic<?php echo $client['Client']['program_id'] ?>">
													<strong><?php echo $client['Program']['name']; ?></strong><br>

													<input type="checkbox" class="checkbox" id="checkall<?php echo $client['Client']['program_id'] ?>" <?php echo $pTypeDisable?> onclick="selectAll('<?php echo $client['Client']['program_id'] ?>');"><span style="color:red"><b>Select All Clients</b></span><br>
													<?php }?>
													<input type="checkbox" name="data[Client][Client][]" value="<?php echo $client['Client']['id'];?>" <?php echo $pTypeDisable?> <?php if(in_array($client['Client']['id'], $clientsList)) : echo $check; endif;?> class="checkbox"/>
													&nbsp;&nbsp;<?php echo $client['Client']['name'] ?><br>
													<?php $pid = $client['Client']['program_id'];
													endforeach?>
													</div>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<table>		
							<tr class="tr1">
								<td valign="top" style="width:50px"><strong>Included Addresses</strong></td>
								<td valign="top">
									<?php echo $this->Form->input('MerchantAddress.MerchantAddress', array('multiple'=>'multiple', 'div'=>false, 'label'=>false, 'options'=>$merchant_addresses)); ?>
									<br/>
									<sub>Select Merchant from the list to load Addresses</sub>
								</td>
							</tr>
						</table>
					</div>
				</div>
				<br/>
				<div>
					<table>
						<tr class="tr">
							<td style="width:315px;"></td>  
							<td class="bb br" style="">
								<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(0)">Previous</button>
								<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(2)">Next</button>
							</td>
						</tr>
					</table>
	  			</div>	 	
			</div>
		
			<div class="tabbertab"  style="min-height:360px;overflow:hidden">
				<h2>Offer</h2>
				<h4>Offer</h4>
				<div style="background-color:#EFEFEF;border:1px solid #cccccc;padding:10px;width:790px;min-height:265px;overflow:hidden">
					  <table>
					   		<tr>
					      		<td style="width: 385px;" valign="top">		
									<table>
										<tr class="tr">
											<td style="padding-bottom:10px;width:125px;"><strong>Offer</strong></td>
											<td><?php echo $this->Form->input('Product.offer', array('div'=>false, 'label'=>false)); ?></td>
										</tr>
										<tr class="tr">
											<td style="width:125px;padding-bottom:10px;"><strong>Highlight</strong></td>
											<td><?php echo $this->Form->input('Product.highlight', array('div'=>false, 'label'=>false)); ?></td>
										</tr>
										<tr class="tr">
											<td style="padding-bottom:70px"><strong>Web link 1</strong></td>
											<td>
												Link<br/><?php echo $this->Form->input('Product.link1', array('div'=>false, 'label'=>false)); ?><br/>
												Text<br/><?php echo $this->Form->input('Product.link1_text', array('div'=>false, 'label'=>false)); ?>
											</td>
										</tr>
										<tr class="tr">
											<td style="padding-bottom:70px"><strong>Web link 2</strong></td>
											<td>
												Link<br/><?php echo $this->Form->input('Product.link2', array('div'=>false, 'label'=>false)); ?><br/>
												Text<br/><?php echo $this->Form->input('Product.link2_text', array('div'=>false, 'label'=>false)); ?>
											</td>
										</tr>
									</table>
								</td>
								<td valign="top" style="width:400px;">	 
									<table>
										<tr>
											<td>
												<div class="tr">
													<?php echo $this->Form->checkbox('Product.show_and_save', array('div'=>false, 'label'=>false)); ?>
													<strong>Show & Save</strong>
												</div>
												<div class="tr">
													<?php echo $this->Form->checkbox('Product.book_coupon', array('div'=>false, 'label'=>false)); ?>
													<strong>Book Coupon</strong>
												</div>
											</td>
											<td>
												<div class="tr">
													<?php echo $this->Form->checkbox('Product.mobile_reward', array('div'=>false, 'label'=>false)); ?>
													<strong>Mobile Reward</strong>
												</div>
												<div class="tr">
													<?php echo $this->Form->checkbox('Product.phone_benefit', array('div'=>false, 'label'=>false)); ?>
													<strong>Phone Benefit</strong>
												</div>
											</td>
										</tr>
										<tr class="tr1">
											<td colspan="2" valign="top">
												<span>
													<?php echo $this->Form->checkbox('Product.web_coupon', array('div'=>false, 'label'=>false)); ?>
													<strong>Web Coupon</strong>
												</span>
												<span style="padding-left:50px;">
													<?php echo $this->Form->input('Product.coupon_expiry', array('style'=>'width:3em', 'default'=>30, 'div'=>false, 'label'=>false)); ?>   Print Expires
													<br/><sub>Number of days given above will be added to the present date for the print coupon</sub>
												</span>
												<br>
												<?php echo $this->Form->checkbox('Product.online_purchase', array('div'=>false, 'label'=>false));?>
												<strong>Online Purchase</strong>
								                 Price $<?php echo $this->Form->input('Product.price', array('style'=>'width:4em', 'div'=>false, 'label'=>false, 'type'=>'text')); ?>
												Qty <?php echo $this->Form->input('Product.quantity', array('style'=>'width:2em', 'div'=>false, 'label'=>false, 'type'=>'text')); ?>
											</td>
										</tr>
										<tr class="tr">	
											<td colspan="2">
												<?php echo $this->Form->checkbox('Product.favourites', array('div'=>false, 'label'=>false)); ?>
												<strong>Favourites</strong>
												<br/>
												<sub>If check the products - merchant logo will be displayed in favourites box</sub>
											</td>
										</tr>
										<tr class="tr">
											<script>
												function showDiv()
												{
													if (document.getElementById('hotoffer').checked==true)
														document.getElementById('ProductImage1File').style.display='inline';
													else
														document.getElementById('ProductImage1File').style.display='none';
												}
												function removeCheck()
												{
													if (document.getElementById('imgRem').checked==true)
														document.getElementById('hotoffer').checked=false;
												}
											</script>
											<td colspan="2">
												<?php echo $this->Form->input('Product.hotoffer',array('div'=>false, 'label'=>false,'type'=>'checkbox','id'=>'hotoffer', 'onclick'=>"showDiv()")); ?> 
												<strong>Deal of the Day</strong>
												<?php 
													if($this->Form->value('Product.hotoffer')==1) :?>
														<img width="100" height="100" src="<?php echo $this->Html->url(HOT_OFFER_IMAGE_WWW_PATH. $this->Form->value('Product.id'). '.'. $this->Form->value('Product.hotoffer_extension'). '?rand='. time()); ?>" alt="" /><br/>
														<a href="javascript:void(0)" onclick="this.style.display='none'; $('ProductImage1File').style.display = 'inline'; return false;">Change</a><br>
														<?php echo $this->Form->file('ProductImage1.file',array('style' => 'display:none;'));?> 
														Remove 
														<?php echo $this->Form->input('ProductImage1Delete.file',array('div'=>false, 'label'=>false,'type'=>'checkbox','id'=>'imgRem', 'onclick'=>"removeCheck()")); ?> 
														<?php //echo $this->Form->checkbox('ProductImage1Delete.file'); ?>
													<?php else :
														echo $this->Form->file('ProductImage1.file',array('style' => 'display:none;')); 
													endif;	
												?><br/>
												<sub>* The product will be displayed in Hotoffer box and if check must upload an image<br/>
												For better appearance upload image of width 530px and height 430px.</sub>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
				</div>
			  	<br/>
		      	<div>
					<table>
						<tr class="tr">
							<td style="width:315px;"></td>  
							<td>
								<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(1)">Previous</button>
								<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(3)">Next</button>
							</td>
						</tr>
					</table>
	       	  	</div>   	  	
			</div>
		
			<div class="tabbertab"  style="height:360px">
				<h2>Special Offer</h2>
				<h4>Special Offer</h4>
			   	<div style="background-color:#EFEFEF;border:1px solid #cccccc;padding:10px;width:790px;min-height:265px;overflow:hidden">
			    	<table>
						<tr class="tr">
							<td style="width:150px"><strong>Special Offer</strong></td>
							<td><?php echo $this->Form->checkbox('Product.special_offer', array('div'=>false, 'label'=>false)); ?></td>
						</tr>
						<tr class="tr">
							<td><strong>Special Offer headline</strong></td>
							<td>
								<?php echo $this->Form->input('Product.special_offer_headline', array('div'=>false, 'label'=>false)); ?>
							</td>
						</tr>
						<tr class="tr">
							<td valign="top"><strong>Special Offer body</strong></td>
							<td>
								<?php echo $this->Form->input('Product.special_offer_body', array('div'=>false, 'label'=>false)); ?>
							</td>
						</tr>
					</table>
		     	</div>
		      	<br/>
		      	<div>
					<table>
						<tr class="tr">
							<td style="width:315px;"></td>  
							<td>
								<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(2)">Previous</button>
								<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(4)">Next</button>
							</td>
						</tr>
					</table>
	       	  	</div> 
			</div>
		
			<div class="tabbertab"  style="min-height:340px;overflow:hidden;">
				<h2>Availability And Image</h2>
				<h4>Availability And Image </h4>
		    	<div style="background-color:#EFEFEF;border:1px solid #cccccc;padding:10px;width:790px;min-height:225px;overflow:hidden">		
				    <table width="100%" style="padding:30px;">
				    	<tr>
				        	<td style="padding-right:38px;" valign="top">		
								<table>	
									<tr class="tr">
										<td><strong>Offer Start</strong></td>
										<td><?php
											$this->request->data["Product"]["offer_start"]=$this->request->data["Product"]["offer_start"]=="0000-00-00"?"":$this->request->data["Product"]["offer_start"];
											echo $this->Form->day('Product.offer_start', array('empty' => ''));
											echo $this->Form->month('Product.offer_start', array('empty' => ""));
											echo $this->Form->year('Product.offer_start', date('Y'), date('Y')+10, array('empty' => ""));?>
										</td>
									</tr>
									<tr class="tr">
										<td><strong>Expires</strong></td>
										<td><?php
											$this->request->data["Product"]["expires"]=$this->request->data["Product"]["expires"]=="0000-00-00"?"":$this->request->data["Product"]["expires"];
											echo $this->Form->day('Product.expires', array('empty' => ''));
											echo $this->Form->month('Product.expires', array('empty' => ""));
											echo $this->Form->year('Product.expires', date('Y'), date('Y')+10, array('empty' => ""));?>
										</td>
									</tr>
									<tr class="tr">
										<td style="padding-right:10px;"><strong>Availability</strong></td>
										<td>
											<table style="border-left:0;" id="TableAvailability">
												<tr>
													<td><?php echo $this->Form->checkbox('Product.available_national', array('onClick' => 'ToggleStates(this)')); ?> National<br/>
													</td>
													<td><?php echo $this->Form->checkbox('Product.available_international', array('onClick' => 'ToggleStates1(this)')); ?> InterNational<br/>
													</td>
													<!--<td><?php echo $this->Form->checkbox('Product.available_act'); ?> ACT</td>
													<td><?php echo $this->Form->checkbox('Product.available_nsw'); ?> NSW</td>-->
												</tr>
												<!--<tr>
													<td><?php echo $this->Form->checkbox('Product.available_nt'); ?> NT</td>
													<td><?php echo $this->Form->checkbox('Product.available_qld'); ?> QLD</td>
													<td><?php echo $this->Form->checkbox('Product.available_sa'); ?> SA</td>
												</tr>
												<tr>
													<td><?php echo $this->Form->checkbox('Product.available_tas'); ?> TAS</td>
													<td><?php echo $this->Form->checkbox('Product.available_vic'); ?> VIC</td>
													<td><?php echo $this->Form->checkbox('Product.available_wa'); ?> WA</td>
												</tr>-->
											</table>
										</td>
									</tr>
									<tr>
										<td class="tr"><strong>Renewed</strong></td>
										<td>
											<?php echo $this->Form->checkbox('Product.renewed');?>
							                Year Until
											<?php echo $this->Form->year('Product.yearuntil', date('Y'), date("Y")+10, array('style'=>'width:auto', 'div'=>false, 'label'=>false)); ?>
										</td>
									</tr>
									<tr class="tr">
										<td><strong>Active</strong></td>
										<td><?php echo $this->Form->checkbox('Product.active'); ?></td><td class="bl"></td>
										
									</tr>
									<tr class="tr">
										<td><strong>Cancelled</strong></td>
										<td><?php echo $this->Form->checkbox('Product.cancelled'); ?></td><td class="bl"></td>
									</tr>
									<tr class="tr">
										<td><strong>Search weight</strong></td>
										<td>
											<?php echo $this->Form->input('Product.search_weight', array('div'=>false, 'label'=>false, 'type'=>'select', 'options'=>$search_weights)); ?>
											<sub>Higher values appear first in search results</sub>
										</td>
									</tr>
								</table>
							</td>
							<td valign="top">
							    <table>	
							    	<tr style="height:60px;">
										<td style="padding-right:10px;padding-top:8px;" valign="top"><strong>Display image</strong></td>
										<td style="padding-top:8px;" valign="top">
											<?php echo $this->Form->input('Product.display_image', array('div'=>false, 'label'=>false, 'type'=>'select', 'options'=>$display_images)); ?>
											<br/><sub>Image to display on product list pages</sub>	
										</td>
									</tr>
							    	<tr style="height:60px;">
										<td valign="top"><strong>Image</strong></td>
										<td>
											<img src="<?php echo $this->Html->url(PRODUCT_IMAGE_WWW_PATH. $this->Form->value('Product.id'). '.'. $this->Form->value('Product.image_extension')); ?>" alt="" /><br/>
											<?php echo $this->Form->file('ProductImage.file'); ?><br/>
											Remove <?php echo $this->Form->checkbox('ImageDelete.file'); ?>
										</td>
									</tr>
									<tr class="tr">
										<td><strong>Print coupon limit count</strong></td>
										<?php 
											if($this->Form->value('Product.used_count')=='') 
												$defaultCount = -1;
											else 
												$defaultCount = $this->Form->value('Product.used_count');
										?>
										<td><?php echo $this->Form->input('Product.used_count', array('default'=>$defaultCount, 'div'=>false, 'label'=>false)); ?></td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
		     		<?php echo $this->Form->input('Product.id', array('type' => 'hidden')); ?>
		    	</div>
		     	<br/>
		      	<div>
					<table>
						<tr class="tr">
							<td style="width:315px;"></td>  
							<td>
								<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(3)">Previous</button>
								<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(5)">Next</button>
							</td>
						</tr>
					</table>
	       	  	</div>
	    	</div>
    		
    		<div class="tabbertab"  style="min-height:250px;overflow:hidden;">
	  			<h2>App</h2>
	    		<h4>App Text</h4>
	    		<div style="background-color:#EFEFEF;border:1px solid #cccccc;padding:10px;width:790px;min-height:180px;overflow:hidden">
					<div style="padding:20px">	
						<table> 				
							<tr class="tr">
								<td><strong>App Text</strong></td>
								<td><?php echo $this->Form->input('Product.app_text',array('style'=>'width:450px;height:40px;', 'div'=>false, 'label'=>false)); ?></td>
							</tr>
							<tr class="tr">
								<td><strong>App Coupon</strong></td>
								<td><?php echo $this->Form->input('Product.app_coupon', array('div'=>false, 'label'=>false)); ?></td>
							</tr>
							<tr class="tr">
								<td><strong>App Image</strong></td>
								<td>
									<img width="100px" height="100px" src="<?php echo $this->Html->url(APP_IMAGE_WWW_PATH. $this->Form->value('Product.id'). '.'. $this->Form->value('Product.app_image_extension')); ?>" alt="" /><br/>
									<?php echo $this->Form->file('ProductAppImage.file'); ?><br/>
									Remove <?php echo $this->Form->checkbox('ImageAppDelete.file'); ?>
								</td>
							</tr>
						</table>
				  	</div>
			    </div>
			    <br/>
			    <div>
					<table>
						<tr class="tr">
							<td style="width:315px;"></td>  
							<td class="bb br" style="">
								<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(4)">Previous</button>
								<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(6)">Next</button>
							</td>	
						</tr>
					</table>
				</div>
	 		</div>
	 		
	 		<div class="tabbertab" style="min-height:108px;overflow:hidden">
				<h2>Comments</h2>
				<h4>Comments</h4>
				<div style="border:1px solid #cccccc;padding:10px;width:790px;min-height:110px;overflow:hidden">
					<div class="list" id="comment_list" style="float:none !important;">
						<div class="edit_new">
					    	<div id="Comments" style="margin:0;">Retrieving comments...</div>
					    	<script type="text/javascript">
							$.ajax({
								url: "/admin/merchants/ajax_comments/<?php echo $mer_id?>",
								type: "GET",
								success: function (data) {
									$('#Comments').html(data);
								}
							});
				        	</script>
						</div>
					</div>	
				</div>
				<div align="center" style="padding-top:10px">
					<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(5)">Previous</button>
					<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(7)">Next</button>
				</div>
			</div>
     		
	    	<div class="tabbertab" style="min-height:108px;overflow:hidden">
				<h2>Change Log</h2>
				<h4>Change Log </h4>
				<div style="border:1px solid #cccccc;padding:10px;width:790px;min-height:110px;overflow:hidden">
					<div class="list" id="log_list" style="width:800px">
						<h3>Change Log</h3>
						<div id="Log" style="margin:0;">Retrieving changes...</div>
						<script type="text/javascript">
						$(function() {
							$.ajax({
								url: "/admin/logs/ajax_list_new/<?php echo Inflector::singularize($this->name);?>/<?php echo $this->Form->value(Inflector::singularize($this->name). '.id');?>",
								type: "GET",
								success: function (data) {
									$('#Log').html(data);
								}
							});
						});</script>
					</div>
				</div>
				<div align="center" style="padding-top:10px">
					<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(6)">Previous</button>
				</div>
			</div>
		</div>
	</div>
</form>

<script type="text/javascript">

	ToggleStates = function (National)
	{
	    var rows = document.getElementById('TableAvailability').getElementsByTagName('input');
	    for ( var i = 0; i < rows.length; i++ ) {
		if ( rows[i] && rows[i].type == 'checkbox' && rows[i] != National ) 
		{
			if(National.checked == true)
			{
			    rows[i].checked = true;
			}
			else
			{
			   rows[i].checked = false;
			}
		}
	    }

	    return true;
	}
	Togglestates1 = function (InterNational)
	{
	    var rows = document.getElementById('TableAvailability').getElementsByTagName('input');
	    for ( var i = 0; i < rows.length; i++ ) {
		if ( rows[i] && rows[i].type == 'checkbox' && rows[i] != InterNational ) 
		{
			if(InterNational.checked == true)
			{
			    rows[i].checked = true;
			}
			else
			{
			   rows[i].checked = false;
			}
		}
	    }

	    return true;
	}
	
	function toggleSpellchecker()
	{
		var val = navigator.userAgent.toLowerCase();
		if(val.indexOf("msie") < 0)
		{
			tinyMCE.execCommand ('mceFocus', false, 'ProductSpecialOfferBody');
			tinyMCE.execCommand('mceSpellCheck',false,'ProductSpecialOfferBody');

			tinyMCE.execCommand ('mceFocus', false, 'ProductDetails');
			tinyMCE.execCommand('mceSpellCheck',false,'ProductDetails');

			tinyMCE.execCommand ('mceFocus', false, 'ProductText');
			tinyMCE.execCommand('mceSpellCheck',false,'ProductText');

			tinyMCE.execCommand ('mceFocus', false, 'ProductKeyword');
			tinyMCE.execCommand('mceSpellCheck',false,'ProductKeyword');

			tinyMCE.execCommand ('mceFocus', false, 'ProductTermsAndConditions');
			tinyMCE.execCommand('mceSpellCheck',false,'ProductTermsAndConditions');
		}
	}
	 
</script>