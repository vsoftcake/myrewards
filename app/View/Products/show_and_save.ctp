	<?php echo $this->element('module2', array('module2' => 'Keyword_search', 'title' => 'Search'));?>
	<?php echo $this->element('message'); ?>
	<a href="javascript:window.history.back()" id="back" style="display:none;"><img src="/files/newimg/back.png" alt="Back" title="Back"/></a>
	<?php if ($page['ClientPage']['h1'] != '') : ?>
		<h1><?php echo $page['ClientPage']['h1']; ?></h1>
	<?php endif; ?>
	<?php if ($page['ClientPage']['h2'] != '') : ?>
		<h2><?php echo $page['ClientPage']['h2']; ?></h2>
	<?php endif; ?>
	<?php if ($page['ClientPage']['h3'] != '') : ?>
		<h3><?php echo $page['ClientPage']['h3']; ?></h3>
	<?php endif; ?>
	<?php if ($page['ClientPage']['content'] != '') : ?>
		<?php echo $page['ClientPage']['content']; ?>
	<?php endif; ?>
	<br/>
<form action="<?php echo $this->Html->url('/products/show_and_save_report'); ?>" method="post">
<table>
	<tr>
		<td><b>Country</b>&nbsp;</td>
		<td>
		<?php echo $this->Form->input('Product.country', array('div'=>false, 'label'=>false, 'type'=>'select', 'options'=>$countries, 'selected'=>$this->Session->read('user.User.country'))); 
		$options = array('url' => 'easyfuel_states','update' => 'ProductState');
		echo $this->Ajax->observeField('ProductCountry', $options);?>
		</td>
	</tr>
	<tr><td>&nbsp;</td></tr>
	<tr>
		<td><b>State</b>&nbsp;</td>
		<td><?php echo $this->Form->input('Product.state', array('div'=>false, 'label'=>false, 'type'=>'select', 'options'=>$states, 'selected'=>$this->Session->read('user.User.state'))); ?></td>
	</tr>
	<tr><td>&nbsp;</td></tr>
	<tr>
		<td></td>
		<td><button type="submit" class="button_search_module rounded_corners_button">Submit</button></td>
	</tr>
</table>
</form>
<?php if (isset($new_products)) : ?>
	<div class="module" style="width: 66%">
		<div class="tl">
			<div class="tr">
				<div class="t">
					&nbsp;
				</div>
			</div>
		</div>
		<div class="content">
			<?php echo $this->element('special_offers', array('special_offers' => $new_products)); ?>
		</div>
		<div class="bl">
			<div class="br">
				<div class="b">&nbsp;</div>
			</div>
		</div>
	</div>
<?php endif; ?>
<?php if ($page['ClientPage']['special_offers'] == '1') : ?>
	<div class="module" style="width: 66%">
		<div class="tl">
			<div class="tr">
				<div class="t">
					&nbsp;
				</div>
			</div>
		</div>
		<div class="content">
			<?php echo $this->element('special_offers'); ?>
		</div>
		<div class="bl">
			<div class="br">
				<div class="b">&nbsp;</div>
			</div>
		</div>
	</div>
<?php endif; ?>
<?php if (isset($page['ClientPage']['whats_new_module']) && $page['ClientPage']['whats_new_module'] == '1') : ?>
	<div class="module" style="width: 66%">
		<div class="tl">
			<div class="tr">
				<div class="t">
					&nbsp;
				</div>
			</div>
		</div>
		<div class="content">
			<?php echo $this->element('special_offers', array('special_offers' => $whats_new_offers)); ?>
		</div>
		<div class="bl">
			<div class="br">
				<div class="b">&nbsp;</div>
			</div>
		</div>
	</div>
<?php endif; ?>