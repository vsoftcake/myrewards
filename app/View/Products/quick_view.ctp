<style>p{clear:both;}#MB_window{overflow:visible !important}</style>

			<div style="float:left;padding-bottom:15px;">
				<img style="margin-bottom:5px;left:-12px;top:25px;position:absolute" src="/files/newimg/rm_lightbox.png"/>
				<p style="font-size:15px;font-weight:bold;width:180px;padding-left:20px;padding-top:65px;">
					<?php echo $product['Product']['name']; ?>
				</p>
				<?php 
						$logo="no_logo";
						if ($product['Product']['display_image'] == 'Product Image') {
							$filepath = PRODUCT_IMAGE_PATH;
							$filename = $product['Product']['id']. '.'. $product['Product']['image_extension'];
						} else {
							$filepath = MERCHANT_LOGO_PATH;
							$filename = $product['Merchant']['id']. '.'. $product['Merchant']['logo_extension'];
						}
				
						if (!file_exists($filepath. $filename)) {
				
							//	try for a merchant image
							$filepath = MERCHANT_IMAGE_PATH;
							$filename = $product['Merchant']['id']. '.'. $product['Merchant']['image_extension'];
							if (!file_exists($filepath. $filename)) {
								//	try for a merchant logo
								$filepath = MERCHANT_LOGO_PATH;
								if ($logo != 'no_logo') {	//	if logo is not meant to appear, set a dummy name so it will not be found
									$filename = 'no_file';
								} else {
				
									$filename = $product['Merchant']['id']. '.'. $product['Merchant']['logo_extension'];
								}
								if (!file_exists($filepath. $filename)) {
									//	try for a client image
									$filepath = FILES_PATH. 'default_client_product_image/';
									$filename = $this->Session->read('client.Client.id'). '.'. $this->Session->read('client.Client.default_client_product_image_extension');
									if (!file_exists($filepath. $filename)) {
										//	try for a program image
										$filepath = FILES_PATH. 'default_product_image/';
										$filename = $this->Session->read('client.Program.id'). '.'. $this->Session->read('client.Program.default_product_image_extension');
										if (!file_exists($filepath. $filename)) {
											//	use the default no image image
											$filepath = WWW_ROOT. 'img/';
											$filename = 'no_image.gif';
										}
									}
								}
							}
						}
						$path = substr($filepath,strpos($filepath,'webroot')+8);
						$path_prod = $path.$filename;
					  	
						$a=$this->ImageResize->getResizedDimensions($path_prod, 180, 140);
						
					?>		
				<img align="center" style="margin-left:20px;" width="<?php echo $a['width'];?>" height="<?php echo $a['height'];?>"  src="<?php echo $this->Html->url('/'. $path_prod);?>" alt="" />
			</div>
			
			<div style="padding-left:200px;padding-top:20px;">
				<div style="min-height:100px;overflow:hidden;"  id="quickview_data">
					<?php if (trim($product['Product']['special_offer']) == '1' && $product['Product']['special_offer_headline']!='' && $product['Product']['special_offer_body']!='') : ?>
						<?php if(trim($product['Product']['special_offer_headline'])!='') :?>
							<p style="font-size:16px;font-weight:bold;color:#ee2a7b"><b><?php echo $product['Product']['special_offer_headline']; ?></b></p>
						<?php endif;?>
						<?php if(trim($product['Product']['special_offer_body'])!='') :?>
							<p><?php  echo $product['Product']['special_offer_body']; ?></p>
						<?php endif;?>
					<?php else : ?>
							<p style="font-size:16px;font-weight:bold;color:#ee2a7b"><b><?php echo $product['Product']['highlight']; ?></b></p>
							<p><?php  echo $product['Product']['details']; ?></p>
						<?php endif; ?>
					</div>
					<div style="min-height:50px;overflow:hidden">
						<?php if (trim($product['address']) == 'multiple') : ?>
							<p><a href="<?php echo $this->Html->url('/products/view/'. $product['Product']['id']. '/all'); ?>">View locations</a></p>
						<?php elseif (trim($product['address']) != 'none') : ?>
							<ul>
								<?php if (trim($product['MerchantAddress']['address1']) != '0' && trim($product['MerchantAddress']['address1']) != '') : ?>
									<li>
										<?php echo $product['MerchantAddress']['address1']; ?>
									</li>
								<?php endif; ?>
								<?php if (trim($product['MerchantAddress']['address2']) != '0' && trim( $product['MerchantAddress']['address2']) != '') : ?>
									<li>
										<?php echo $product['MerchantAddress']['address2']; ?>
									</li>
								<?php endif; ?>
								<?php if (trim($product['MerchantAddress']['suburb']) != '0' && trim($product['MerchantAddress']['suburb']) != '') : ?>
									<li>
										<?php echo $product['MerchantAddress']['suburb']; ?>
									</li>
								<?php endif; ?>
								<?php if (trim($product['MerchantAddress']['postcode']) != '0' && trim($product['MerchantAddress']['postcode']) != '') : ?>
									<li>
										<?php if (trim($product['MerchantAddress']['postcode']) != '0') { echo ' '. $product['MerchantAddress']['postcode']; } ?>
									</li>
								<?php endif; ?>
								<?php if (trim(strip_tags($product['MerchantAddress']['phone'])) != '0' && trim(strip_tags($product['MerchantAddress']['phone'])) != '') : ?>
									<li>
										Tel: <?php echo $product['MerchantAddress']['phone']; ?>
									</li>
								<?php endif; ?>
							</ul>
						<?php endif; ?>
					</div>
					
					<div style="min-height:20px;overflow:hidden">
					
						<?php if  ($product['Product']['phone_benefit'] == '1') : ?>
						<img style="padding:5px;" src="<?php echo $this->Html->url('/files/styles/icon_images/phone.png'); ?>" alt="phone" title="Phone Benefit" />
						<?php endif; ?>
						<?php if  ($product['Product']['link1'] != '') : ?>
							<a href="<?php echo $product['Product']['link1']; ?>" target="_blank"><img style="padding:5px;" src="<?php echo $this->Html->url('/files/styles/icon_images/link.png'); ?>" alt="web link" title="Click to go to the merchant web site" /></a>
						<?php endif; ?>
						<?php if  (($product['Product']['show_and_save'] == '1') && ($this->Session->read('dashboardidinsession')!=3)) : ?>
							<img style="padding:5px;" src="<?php echo $this->Html->url('/files/styles/icon_images/card.png'); ?>" alt="show and save" title="Present you mobile phone coupon or membership card" />
						<?php endif; ?>
						<?php if  (($product['Product']['web_coupon'] == '1') && ($product['Product']['used_count'] > 0 || $product['Product']['used_count'] == -1) && ($usedcount>0 || $usedcount== -1 ) && ($this->Session->read('dashboardidinsession')!=3)) : ?>
							<a href="<?php echo $this->Html->url('/products/voucher/'. $product['Product']['id']. '/'. $product['MerchantAddress']['id']); ?>" ><img style="padding:5px;" src="<?php echo $this->Html->url('/files/styles/icon_images/print.png'); ?>" alt="print" title="Print" /></a>
						<?php endif; ?>
						<?php if  ((count($cartproduct)>0) && ($this->Session->read('dashboardidinsession')!=3)) : ?>
							<?php $this->Html->image('/files/styles/icon_images/shop.png', array('style'=>'padding:5px 20px;cursor:pointer;', 'escape'=>false, 'onclick'=>"addcartpopup(".$product['Product']['id'].")"));?>
						<?php endif; ?>
						<?php if  (($product['Product']['web_coupon'] == '1') && ($product['Product']['used_count'] > 0 || $product['Product']['used_count'] == -1) && ($usedcount>0 || $usedcount== -1 ) && ($this->Session->read('dashboardidinsession')!=3)) : ?>
							<a href="<?php echo $this->Html->url('/products/voucher/'. $product['Product']['id']. '/'. $product['MerchantAddress']['id']); ?>">
								<img style="padding:5px;" src="<?php echo $this->Html->url('/files/styles/icon_images/voucher.png'); ?>" alt="web coupon" title="Web Coupon" />
							</a>
						<?php endif; ?>
						<?php if  (strtotime($product['Product']['created']) > strtotime('-2 month')) : ?>
							<img style="padding:5px;" src="<?php echo $this->Html->url('/files/styles/icon_images/new.png'); ?>" alt="new" title="New"/>
						<?php endif; ?>
						<?php if  (($product['Product']['web_coupon'] == '1') && ($product['Product']['used_count'] > 0 || $product['Product']['used_count'] == -1) && ($usedcount>0 || $usedcount== -1 ) && ($this->Session->read('dashboardidinsession')!=3)) : ?>
							<a href="javascript:void(0)" id="wlink" onclick="showWish(<?php echo $product['Product']['id'].','.$this->Session->read('user.User.id');?>)">
								<img style="padding:5px;" src="<?php echo $this->Html->url('/files/styles/icon_images/wishlist.png'); ?>" alt="Add to Favourites" title="Add to Favourites"/>
							</a>							
						<?php endif;?>
						<?php if  ($this->Session->read('dashboardidinsession') == '3' && $productpoints['0']['products_points']['points']>0): ?>
							<a href="javascript:void(0)" onclick="showRedeem(<?php echo $product['Product']['id'].','.$this->Session->read('user.User.id');?>)">
								<?php echo $this->Html->image('/files/styles/icon_images/redeem.png', array('style'=>'padding:5px 20px;cursor:pointer;','alt' => 'Redeem','title'=>'Redeem', 'escape'=>false, 'onclick'=>"redeempoints(".$product['Product']['id'].")"));?>
							</a>							
						<?php endif;?>
						<a href="<?php echo $this->Html->url('/products/view/'. $product['Product']['id']);?>"><img style="padding:5px;" src="<?php echo $this->Html->url('/files/newimg/more_info.png'); ?>" alt="More" title="More information available" /></a>
						<?php if($product['Product']['web_coupon'] == '1' && ($product['Product']['used_count'] == 0 )):?>
							<p><b><font color="red">PRINT LIMIT EXCEEDED</font></b></p>
						<?php endif; ?>
					</div>
			</div> 
			
	<style type="text/css">
		.ui-dialog{
			z-index: 9999;
		}
	</style>		
			
	
<script type="text/javascript">
 var searchTerm = '<?php echo $keyword; ?>'; //$(this).val();
 
        // remove any old highlighted terms
        $('#modalboxpopup').removeHighlight();
		
	console.log($('#quickview_data').html());
        // disable highlighting if emptys
     if (searchTerm) {	
	            // highlight the new term
            $('#modalboxpopup').highlight(searchTerm);
     }

 </script>
<style type="text/css">
	.highlight {
		color: #01b7ff;
	}
</style>				
			