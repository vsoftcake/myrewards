<?php
$result = array();
foreach($code as $codes) :
	$key = $codes['Postcode']['suburb']." - ". $codes['Postcode']['postcode'];
	array_push($result, array("id"=>$key, "label"=>$key, "value"=>strip_tags($key)));
endforeach;
echo json_encode($result);?>