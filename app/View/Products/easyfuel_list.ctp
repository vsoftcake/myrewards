	<?php echo $this->element('module2', array('module2' => 'Keyword_search', 'title' => 'Search'));?>
	<?php echo $this->element('message'); ?>
	<a class="no_print" href="javascript:window.history.back()" id="back" style="display:none;"><img src="/files/newimg/back.png" alt="Back" title="Back"/></a>
	<?php if ($page['ClientPage']['h1'] != '') : ?>
		<h1><?php echo $page['ClientPage']['h1']; ?></h1>
	<?php endif; ?>
	<?php if ($page['ClientPage']['h2'] != '') : ?>
		<h2><?php echo $page['ClientPage']['h2']; ?></h2>
	<?php endif; ?>
	<?php if ($page['ClientPage']['h3'] != '') : ?>
		<h3><?php echo $page['ClientPage']['h3']; ?></h3>
	<?php endif; ?>
	<?php if ($page['ClientPage']['content'] != '') : ?>
		<?php echo $page['ClientPage']['content']; ?>
	<?php endif; ?>
<h1>Fuel Outlets <?php echo $state; ?></h1>
<table>
	<tr>
		<?php foreach($fuel_suburbs as $column) : ?>
		<td style="vertical-align:top;padding-right:10px">
			<table>
			<?php foreach($column as $suburb => $products) : ?>
				<tr>
					<td><h5><?php echo $suburb; ?></h5></td>
					<td></td>
				</tr>
				<?php foreach($products as $product) : ?>
					<tr>
						<td style="vertical-align:top;"><?php echo $product['MerchantAddress']['name']; ?></td>
						<td style="vertical-align:top;"><?php echo $product['MerchantAddress']['address1']; ?></td>
					</tr>
				<?php endforeach; ?>
			<?php endforeach; ?>
			</table>
		</td>
		<?php endforeach; ?>
	</tr>
</table>
<br/><br/>
<h1 style="page-break-before: always">Retail Outlets <?php echo $state; ?></h1>
<table>
	<tr>
		<?php foreach($retail_suburbs as $column) : ?>
		<td style="vertical-align:top;padding-right:10px">
			<table>
			<?php foreach($column as $suburb => $products) : ?>
				<tr>
					<td><h5><?php echo $suburb; ?></h5></td>
					<td></td>
				</tr>
				<?php foreach($products as $product) : ?>
					<tr>
						<td style="vertical-align:top;"><?php echo $product['MerchantAddress']['name']; ?></td>
						<td style="vertical-align:top;"><?php echo $product['MerchantAddress']['address1']; ?></td>
					</tr>
				<?php endforeach; ?>
			<?php endforeach; ?>
			</table>
		</td>
		<?php endforeach; ?>
	</tr>
</table>
<span class="no_print"><br/><br/><button type="submit" class="button_search_module rounded_corners_button" onclick="window.print(); return false;">Print</button></span>