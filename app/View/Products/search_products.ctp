<script>
	function clearAll(id)
	{
		if(id==1)
		{
			var url = "/products/search_products/?key=<?php echo urlencode($keyword);?><?php if(!empty($cat)): echo "&cat=".urlencode($cat);endif;?><?php if(!empty($merchant)): echo "&merchant=".urlencode($merchant);endif;?>";
		}
		else if(id==2)
		{
			var url = "/products/search_products/?key=<?php echo urlencode($keyword);?><?php if(!empty($merchant)): echo "&merchant=".urlencode($merchant);endif;?><?php if(!empty($ctry)): echo "&ctry=".urlencode($ctry);endif;?><?php if(!empty($state)): echo "&state=".urlencode($state);endif;?>";
		}	
		else if(id==3)
		{
			var url = "/products/search_products/?key=<?php echo urlencode($keyword);?><?php if(!empty($cat)): echo "&cat=".urlencode($cat);endif;?><?php if(!empty($ctry)): echo "&ctry=".urlencode($ctry);endif;?><?php if(!empty($state)): echo "&state=".urlencode($state);endif;?>";
		}
		else if(id==4)
		{
			var url = "/products/search_products/?key=<?php echo urlencode($keyword);?><?php if(!empty($cat)): echo "&cat=".urlencode($cat);endif;?><?php if(!empty($ctry)): echo "&ctry=".urlencode($ctry);endif;?><?php if(!empty($merchant)): echo "&merchant=".urlencode($merchant);endif;?>";
		}
		else if(id==5)
		{
			var url = "/products/search_products/?key=<?php echo urlencode($keyword);?><?php if(!empty($cat)): echo "&cat=".urlencode($cat);endif;?><?php if(!empty($ctry)): echo "&ctry=".urlencode($ctry);endif;?><?php if(!empty($state)): echo "&state=".urlencode($state);endif;?><?php if(!empty($merchant)): echo "&merchant=".urlencode($merchant);endif;?>";
		}
		document.location=url;
	}
</script>
<?php echo $this->element('module2', array('module2' => 'Keyword_search', 'title' => 'Search'));?>
	<?php if (isset($products)) : ?>
		<?php if (!empty($products)) : ?>
			<table class="results results_background" style="width: 952px;">
				<tr>
					<td style="padding-bottom: 12px;padding-left:12px;">
						<h3>Your search for "<?php echo $keyword;?>" (<?php echo $count;?> product/s found)</h3>
					</td>
				</tr>
				<tr><td>
					<div style="float:left;">
						<div style="padding:15px 0px 10px 10px;width:180px;margin-top:30px;margin-left:5px" class="filter_background">
							<div class="box">
								<div class="boxTitle rounded_corners_button">
									Countries
									<?php if(!empty($ctry)): ?>
										<a style="margin-left:60px;font-size:12px;color:#cecece;font-weight:normal;" href="#clear" onclick="clearAll(1)">clear</a>
									<?php endif;?>
								</div>
								<div class="optionsCtry">
									<ul>
										<?php foreach($countries as $key=>$val) : ?>
											<?php if(!empty($key)): ?>
												<li <?php if($key==$ctry): echo "class='selected'";endif;?>>
													<a href="/products/search_products/?key=<?php echo urlencode($keyword)?>&ctry=<?php echo urlencode($key);?><?php if(!empty($cat)): echo "&cat=".urlencode($cat);endif;?><?php if(!empty($merchant)): echo "&merchant=".urlencode($merchant);endif;?>">
														<?php echo $key."&nbsp;(".$val.")<br>"; ?>
													</a>
												</li>
											<?php endif; ?>
										<?php endforeach; ?>
									</ul>
								</div>
								
								<?php if(!empty($ctry) && isset($states)) : ?>
									<br/>
									<div class="boxTitle rounded_corners_button">
										States
										<?php if(!empty($state)): ?>
											<a style="margin-left:80px;font-size:12px;color:#cecece;font-weight:normal;" href="#clear" onclick="clearAll(4)">clear</a>
										<?php endif;?>
									</div>
									<div class="optionsCtry">
										<ul>
											<?php foreach($states as $key=>$val) : ?>
												<?php if(!empty($key)): ?>
													<li <?php if($key==$state): echo "class='selected'";endif;?>>
														<a href="/products/search_products/?key=<?php echo urlencode($keyword)?>&state=<?php echo urlencode($key);?><?php if(!empty($ctry)): echo "&ctry=".urlencode($ctry);endif;?><?php if(!empty($cat)): echo "&cat=".urlencode($cat);endif;?><?php if(!empty($merchant)): echo "&merchant=".urlencode($merchant);endif;?>">
															<?php echo $key."&nbsp;(".$val.")<br>"; ?>
														</a>
													</li>
												<?php endif; ?>
											<?php endforeach; ?>
										</ul>
									</div>
								<?php endif; ?>
								
								<?php if(!empty($state) && isset($suburbs)) : ?>
									<br/>
									<div class="boxTitle rounded_corners_button">
										Suburbs
										<?php if(!empty($suburb)): ?>
											<a style="margin-left:65px;font-size:12px;color:#cecece;font-weight:normal;" href="#clear" onclick="clearAll(5)">clear</a>
										<?php endif;?>
									</div>
									<div class="options">
										<ul>
											<?php foreach($suburbs as $key=>$val) : ?>
												<?php if(!empty($key)): ?>
													<li <?php if($key==$suburb): echo "class='selected'";endif;?>>
														<a href="/products/search_products/?key=<?php echo urlencode($keyword)?>&suburb=<?php echo urlencode($key);?><?php if(!empty($ctry)): echo "&ctry=".urlencode($ctry);endif;?><?php if(!empty($state)): echo "&state=".urlencode($state);endif;?><?php if(!empty($cat)): echo "&cat=".urlencode($cat);endif;?><?php if(!empty($merchant)): echo "&merchant=".urlencode($merchant);endif;?>">
															<?php echo $key."&nbsp;(".$val.")<br>"; ?>
														</a>
													</li>
												<?php endif; ?>
											<?php endforeach; ?>
										</ul>
									</div>
								<?php endif; ?>
								<br/>
								
								<div class="boxTitle rounded_corners_button">
									Categories
									<?php if(!empty($cat)): ?>
										<a style="margin-left:60px;font-size:12px;color:#cecece;font-weight:normal;" href="#clear" onclick="clearAll(2)">clear</a>
									<?php endif;?>
								</div>
								<div class="optionsCat">
									<ul>
										<?php foreach($catCounts as $key=>$val) : ?>
											<li <?php if($key==$cat): echo "class='selected'";endif;?>>
												<a href="/products/search_products/?key=<?php echo urlencode($keyword)?>&cat=<?php echo urlencode($key);?><?php if(!empty($merchant)): echo "&merchant=".urlencode($merchant);endif;?><?php if(!empty($ctry)): echo "&ctry=".urlencode($ctry);endif;?><?php if(!empty($state)): echo "&state=".urlencode($state);endif;?>">
													<?php echo $key."&nbsp;(".$val.")<br>"; ?>
												</a>
											</li>
										<?php endforeach; ?>
									</ul>
								</div>
								<br/>		
								<div class="boxTitle rounded_corners_button">
									Merchants
									<?php if(!empty($merchant)): ?>
										<a style="margin-left:60px;font-size:12px;color:#cecece;font-weight:normal;" href="#clear" onclick="clearAll(3)">clear</a>
									<?php endif;?>
								</div>
								<div class="options">
									<ul>
										<?php foreach($merchants as $key=>$val) : ?>
											<li <?php if($key==$merchant): echo "class='selected'";endif;?>>
												<a href="/products/search_products/?key=<?php echo urlencode($keyword)?><?php if(!empty($cat)): echo "&cat=".urlencode($cat);endif;?>&merchant=<?php echo urlencode($key);?><?php if(!empty($ctry)): echo "&ctry=".urlencode($ctry);endif;?><?php if(!empty($state)): echo "&state=".urlencode($state);endif;?>">
													<?php echo $key."&nbsp;(".$val.")<br>"; ?>
												</a>
											</li>
										<?php endforeach; ?>
									</ul>
								</div>
							</div>
						</div>
						<?php if(isset($spells)): ?>
						<div style="margin:15px 0px 5px 6px;min-height:80px;overflow:hidden;border:1px solid #ccc;">
							<div style="width:180px;">
								<h4 style="padding-left:8px;padding-top:10px;padding-bottom:5px;">You can also try</h4>
									<?php foreach($spells as $spell):?>
										<span style="float:left;line-height:20px;height:20px;padding-left:8px;font-size:15px">
											<a href="/products/search_products/?key=<?php echo urlencode($spell['word']);?>">
												<?php echo $spell['word'];?>
											</a>
										</span>
									<?php endforeach; ?>
								</ul>
							</div>
						</div>	
						<?php endif;?>
						<div align="center" style="margin-top:20px;margin-bottom:10px;">
							<?php foreach($bannerlist as $list):?>
								<?php 
									$filepath = SKYSCRAPER_IMAGE_PATH;
									$filename = $list['Skyscraper']['id']. '.'. $list['Skyscraper']['scraper_extension'];
		
									$path = substr($filepath,strpos($filepath,'webroot')+8);
									$path_prod = $path.$filename;
		
									$a=$this->ImageResize->getResizedDimensions($path_prod, 120, 600);
								?>
								
								<a href="<?php echo $list['Skyscraper']['link_url'];?>">
									<img width="<?php echo $a['width'];?>" height="<?php echo $a['height'];?>" src="<?php echo $this->Html->url('/'. $path_prod); ?>" alt="" />
								</a>
							<?php endforeach;?>
						</div>
					</div>
					<div style="float:left" id="table_list">
						<table>
							<tr>
								<td style="padding-bottom:10px;">
									<div style="float:left;padding-left:10px;">
										<script>
											function sortByOption(sort)
											{
												var sortOption = sort.value;
												var url = "/products/search_products/?key=<?php echo urlencode($keyword);?>&sort="+sortOption+"<?php if(!empty($cat)): echo "&cat=".urlencode($cat);endif;?><?php if(!empty($merchant)): echo "&merchant=".urlencode($merchant);endif;?><?php if(!empty($ctry)): echo "&ctry=".urlencode($ctry);endif;?><?php if(!empty($state)): echo "&state=".urlencode($state);endif;?>";
												document.location=url;
											}
										</script>
										
											Order by 
											<?php $options = array("default"=> "Default", "maCountry" => "Country", "category" => "Category", "merchant" => "Merchants"); ?>
											<?php echo $this->Form->input('sort', array('name'=>'sortoption','type'=>'select', 'label'=>false, 'div'=>false,'options'=>$options,'default'=>$sort,'onChange'=>"sortByOption(this)"));?>
										</div>
										<div align="right" style="padding-right:16px;">
											<?php 	
												$pages = ceil($count / $perpage);
												$lpm1 = $pages - 1;	
												$prev = $page1 - 1;							
												$next = $page1 + 1;
												$adjacents = 3;
											?>
										
											<?php if($pages > 1) :?>
												<?php if($page1>1 && $pages>1) : ?>
													<a class="pagination" href="/products/search_products/?page=1&key=<?php echo urlencode($keyword);?><?php if(!empty($cat)): echo "&cat=".urlencode($cat);endif;?><?php if(!empty($merchant)): echo "&merchant=".urlencode($merchant);endif;?><?php if(!empty($ctry)): echo "&ctry=".urlencode($ctry);endif;?><?php if(!empty($state)): echo "&state=".urlencode($state);endif;?>"><<</a>
													<a class="pagination" href="/products/search_products/?page=<?php echo $prev;?>&key=<?php echo urlencode($keyword);?><?php if(!empty($cat)): echo "&cat=".urlencode($cat);endif;?><?php if(!empty($merchant)): echo "&merchant=".urlencode($merchant);endif;?><?php if(!empty($ctry)): echo "&ctry=".urlencode($ctry);endif;?><?php if(!empty($state)): echo "&state=".urlencode($state);endif;?>"><</a>
												<?php endif; ?>
										
												<?php if ($pages < 7 + ($adjacents * 2)) :?>	
													<?php for ($counter = 1; $counter <= $pages; $counter++){ ?>
														<?php if ($counter == $page1): ?>
															<span class="current"><?php echo $counter;?></span>
														<?php else: ?>
															<a class="pagination" href="/products/search_products/?page=<?php echo $counter;?>&key=<?php echo urlencode($keyword);?><?php if(!empty($cat)): echo "&cat=".urlencode($cat);endif;?><?php if(!empty($merchant)): echo "&merchant=".urlencode($merchant);endif;?><?php if(!empty($ctry)): echo "&ctry=".urlencode($ctry);endif;?><?php if(!empty($state)): echo "&state=".urlencode($state);endif;?>"><?php echo $counter;?></a>					
														<?php endif; ?>
													<?php }?>
										
												<?php elseif($pages > 5 + ($adjacents * 2)) : ?>
													<?php if($page1 < 1 + ($adjacents * 2)) : ?>
														<?php for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++){ ?>
															<?php if ($counter == $page1) : ?>
																<span class="current"><?php echo $counter;?></span>
															<?php else : ?>
																<a class="pagination" href="/products/search_products/?page=<?php echo $counter;?>&key=<?php echo urlencode($keyword);?><?php if(!empty($cat)): echo "&cat=".urlencode($cat);endif;?><?php if(!empty($merchant)): echo "&merchant=".urlencode($merchant);endif;?><?php if(!empty($ctry)): echo "&ctry=".urlencode($ctry);endif;?><?php if(!empty($state)): echo "&state=".urlencode($state);endif;?>"><?php echo $counter;?></a>	
															<?php endif; ?>			
														<?php }	?>
														<a class="pagination" href="/products/search_products/?page=<?php echo $next;?>&key=<?php echo urlencode($keyword);?><?php if(!empty($cat)): echo "&cat=".urlencode($cat);endif;?><?php if(!empty($merchant)): echo "&merchant=".urlencode($merchant);endif;?><?php if(!empty($ctry)): echo "&ctry=".urlencode($ctry);endif;?><?php if(!empty($state)): echo "&state=".urlencode($state);endif;?>">></a>
														<a class="pagination" href="/products/search_products/?page=<?php echo $pages;?>&key=<?php echo urlencode($keyword);?><?php if(!empty($cat)): echo "&cat=".urlencode($cat);endif;?><?php if(!empty($merchant)): echo "&merchant=".urlencode($merchant);endif;?><?php if(!empty($ctry)): echo "&ctry=".urlencode($ctry);endif;?><?php if(!empty($state)): echo "&state=".urlencode($state);endif;?>">>></a>						
													<?php elseif($pages - ($adjacents * 2) > $page1 && $page1 > ($adjacents * 2)) :	?>
															<?php for ($counter = $page1 - $adjacents; $counter <= $page1 + $adjacents; $counter++)	{?>
																<?php if ($counter == $page1) :?>
																	<span class="current"><?php echo $counter;?></span>
																<?php else :?>
																	<a class="pagination" href="/products/search_products/?page=<?php echo $counter;?>&key=<?php echo urlencode($keyword);?><?php if(!empty($cat)): echo "&cat=".urlencode($cat);endif;?><?php if(!empty($merchant)): echo "&merchant=".urlencode($merchant);endif;?><?php if(!empty($ctry)): echo "&ctry=".urlencode($ctry);endif;?><?php if(!empty($state)): echo "&state=".urlencode($state);endif;?>"><?php echo $counter;?></a>
																<?php endif;?>
															<?php }?>
															<a class="pagination" href="/products/search_products/?page=<?php echo $next;?>&key=<?php echo urlencode($keyword);?><?php if(!empty($cat)): echo "&cat=".urlencode($cat);endif;?><?php if(!empty($merchant)): echo "&merchant=".urlencode($merchant);endif;?><?php if(!empty($ctry)): echo "&ctry=".urlencode($ctry);endif;?><?php if(!empty($state)): echo "&state=".urlencode($state);endif;?>">></a>
															<a class="pagination" href="/products/search_products/?page=<?php echo $pages;?>&key=<?php echo urlencode($keyword);?><?php if(!empty($cat)): echo "&cat=".urlencode($cat);endif;?><?php if(!empty($merchant)): echo "&merchant=".urlencode($merchant);endif;?><?php if(!empty($ctry)): echo "&ctry=".urlencode($ctry);endif;?><?php if(!empty($state)): echo "&state=".urlencode($state);endif;?>">>></a>
													<?php else :?>
														<a class="pagination" href="/products/search_products/?page=1&key=<?php echo urlencode($keyword);?><?php if(!empty($cat)): echo "&cat=".urlencode($cat);endif;?><?php if(!empty($merchant)): echo "&merchant=".urlencode($merchant);endif;?><?php if(!empty($ctry)): echo "&ctry=".urlencode($ctry);endif;?><?php if(!empty($state)): echo "&state=".urlencode($state);endif;?>">1</a>
														<a class="pagination" href="/products/search_products/?page=2&key=<?php echo urlencode($keyword);?><?php if(!empty($cat)): echo "&cat=".urlencode($cat);endif;?><?php if(!empty($merchant)): echo "&merchant=".urlencode($merchant);endif;?><?php if(!empty($ctry)): echo "&ctry=".urlencode($ctry);endif;?><?php if(!empty($state)): echo "&state=".urlencode($state);endif;?>">2</a>
														<?php echo "...";?>
														<?php for ($counter = $pages - (1 + ($adjacents * 1)); $counter <= $pages; $counter++)	{?>
															<?php if ($counter == $page1) :?>
																<span class="current"><?php echo $counter;?></span>
															<?php else :?>
																<a class="pagination" href="/products/search_products/?page=<?php echo $counter;?>&key=<?php echo urlencode($keyword);?><?php if(!empty($cat)): echo "&cat=".urlencode($cat);endif;?><?php if(!empty($merchant)): echo "&merchant=".urlencode($merchant);endif;?><?php if(!empty($ctry)): echo "&ctry=".urlencode($ctry);endif;?><?php if(!empty($state)): echo "&state=".urlencode($state);endif;?>"><?php echo $counter;?></a>
															<?php endif;?>		
														<?php }?>
													<?php endif;?>
												<?php endif; ?>	
											<?php endif;?>
										</div>
								</td>
							</tr>
							<tr>
								<td colspan=2" valign="top">
									<table>
										<tr>
											<td valign="top" style="width:745px;padding-left:8px;">
												<div >
													<?php foreach ($products as $product): ?>
														<?php echo $this->element('search_result_products', array('product' => $product)); ?>
													<?php endforeach; ?>
												</div>	
											</td>
										</tr>
									</table>	
								</td>
							</tr>
							<tr>
								<td>
									<div align="right" style="padding-right:16px;padding-bottom:10px;">
										<?php 	
											$pages = ceil($count / $perpage);
											$lpm1 = $pages - 1;	
											$prev = $page1 - 1;							
											$next = $page1 + 1;
											$adjacents = 3;
										?>
									
										<?php if($pages > 1) :?>
											<?php if($page1>1 && $pages>1) : ?>
												<a class="pagination" href="/products/search_products/?page=1&key=<?php echo urlencode($keyword);?><?php if(!empty($cat)): echo "&cat=".urlencode($cat);endif;?><?php if(!empty($merchant)): echo "&merchant=".urlencode($merchant);endif;?><?php if(!empty($ctry)): echo "&ctry=".urlencode($ctry);endif;?><?php if(!empty($state)): echo "&state=".urlencode($state);endif;?>"><<</a>
												<a class="pagination" href="/products/search_products/?page=<?php echo $prev;?>&key=<?php echo urlencode($keyword);?><?php if(!empty($cat)): echo "&cat=".urlencode($cat);endif;?><?php if(!empty($merchant)): echo "&merchant=".urlencode($merchant);endif;?><?php if(!empty($ctry)): echo "&ctry=".urlencode($ctry);endif;?><?php if(!empty($state)): echo "&state=".urlencode($state);endif;?>"><</a>
											<?php endif; ?>
									
											<?php if ($pages < 7 + ($adjacents * 2)) :?>	
												<?php for ($counter = 1; $counter <= $pages; $counter++){ ?>
													<?php if ($counter == $page1): ?>
														<span class="current"><?php echo $counter;?></span>
													<?php else: ?>
														<a class="pagination" href="/products/search_products/?page=<?php echo $counter;?>&key=<?php echo urlencode($keyword);?><?php if(!empty($cat)): echo "&cat=".urlencode($cat);endif;?><?php if(!empty($merchant)): echo "&merchant=".urlencode($merchant);endif;?><?php if(!empty($ctry)): echo "&ctry=".urlencode($ctry);endif;?><?php if(!empty($state)): echo "&state=".urlencode($state);endif;?>"><?php echo $counter;?></a>					
													<?php endif; ?>
												<?php }?>
									
											<?php elseif($pages > 5 + ($adjacents * 2)) : ?>
												<?php if($page1 < 1 + ($adjacents * 2)) : ?>
													<?php for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++){ ?>
														<?php if ($counter == $page1) : ?>
															<span class="current"><?php echo $counter;?></span>
														<?php else : ?>
															<a class="pagination" href="/products/search_products/?page=<?php echo $counter;?>&key=<?php echo urlencode($keyword);?><?php if(!empty($cat)): echo "&cat=".urlencode($cat);endif;?><?php if(!empty($merchant)): echo "&merchant=".urlencode($merchant);endif;?><?php if(!empty($ctry)): echo "&ctry=".urlencode($ctry);endif;?><?php if(!empty($state)): echo "&state=".urlencode($state);endif;?>"><?php echo $counter;?></a>	
														<?php endif; ?>			
													<?php }	?>
													<a class="pagination" href="/products/search_products/?page=<?php echo $next;?>&key=<?php echo urlencode($keyword);?><?php if(!empty($cat)): echo "&cat=".urlencode($cat);endif;?><?php if(!empty($merchant)): echo "&merchant=".urlencode($merchant);endif;?><?php if(!empty($ctry)): echo "&ctry=".urlencode($ctry);endif;?><?php if(!empty($state)): echo "&state=".urlencode($state);endif;?>">></a>
													<a class="pagination" href="/products/search_products/?page=<?php echo $pages;?>&key=<?php echo urlencode($keyword);?><?php if(!empty($cat)): echo "&cat=".urlencode($cat);endif;?><?php if(!empty($merchant)): echo "&merchant=".urlencode($merchant);endif;?><?php if(!empty($ctry)): echo "&ctry=".urlencode($ctry);endif;?><?php if(!empty($state)): echo "&state=".urlencode($state);endif;?>">>></a>						
												<?php elseif($pages - ($adjacents * 2) > $page1 && $page1 > ($adjacents * 2)) :	?>
														<?php for ($counter = $page1 - $adjacents; $counter <= $page1 + $adjacents; $counter++)	{?>
															<?php if ($counter == $page1) :?>
																<span class="current"><?php echo $counter;?></span>
															<?php else :?>
																<a class="pagination" href="/products/search_products/?page=<?php echo $counter;?>&key=<?php echo urlencode($keyword);?><?php if(!empty($cat)): echo "&cat=".urlencode($cat);endif;?><?php if(!empty($merchant)): echo "&merchant=".urlencode($merchant);endif;?><?php if(!empty($ctry)): echo "&ctry=".urlencode($ctry);endif;?><?php if(!empty($state)): echo "&state=".urlencode($state);endif;?>"><?php echo $counter;?></a>
															<?php endif;?>
														<?php }?>
														<a class="pagination" href="/products/search_products/?page=<?php echo $next;?>&key=<?php echo urlencode($keyword);?><?php if(!empty($cat)): echo "&cat=".urlencode($cat);endif;?><?php if(!empty($merchant)): echo "&merchant=".urlencode($merchant);endif;?><?php if(!empty($ctry)): echo "&ctry=".urlencode($ctry);endif;?><?php if(!empty($state)): echo "&state=".urlencode($state);endif;?>">></a>
														<a class="pagination" href="/products/search_products/?page=<?php echo $pages;?>&key=<?php echo urlencode($keyword);?><?php if(!empty($cat)): echo "&cat=".urlencode($cat);endif;?><?php if(!empty($merchant)): echo "&merchant=".urlencode($merchant);endif;?><?php if(!empty($ctry)): echo "&ctry=".urlencode($ctry);endif;?><?php if(!empty($state)): echo "&state=".urlencode($state);endif;?>">>></a>
												<?php else :?>
													<a class="pagination" href="/products/search_products/?page=1&key=<?php echo urlencode($keyword);?><?php if(!empty($cat)): echo "&cat=".urlencode($cat);endif;?><?php if(!empty($merchant)): echo "&merchant=".urlencode($merchant);endif;?><?php if(!empty($ctry)): echo "&ctry=".urlencode($ctry);endif;?><?php if(!empty($state)): echo "&state=".urlencode($state);endif;?>">1</a>
													<a class="pagination" href="/products/search_products/?page=2&key=<?php echo urlencode($keyword);?><?php if(!empty($cat)): echo "&cat=".urlencode($cat);endif;?><?php if(!empty($merchant)): echo "&merchant=".urlencode($merchant);endif;?><?php if(!empty($ctry)): echo "&ctry=".urlencode($ctry);endif;?><?php if(!empty($state)): echo "&state=".urlencode($state);endif;?>">2</a>
													<?php echo "...";?>
													<?php for ($counter = $pages - (1 + ($adjacents * 1)); $counter <= $pages; $counter++)	{?>
														<?php if ($counter == $page1) :?>
															<span class="current"><?php echo $counter;?></span>
														<?php else :?>
															<a class="pagination" href="/products/search_products/?page=<?php echo $counter;?>&key=<?php echo urlencode($keyword);?><?php if(!empty($cat)): echo "&cat=".urlencode($cat);endif;?><?php if(!empty($merchant)): echo "&merchant=".urlencode($merchant);endif;?><?php if(!empty($ctry)): echo "&ctry=".urlencode($ctry);endif;?><?php if(!empty($state)): echo "&state=".urlencode($state);endif;?>"><?php echo $counter;?></a>
														<?php endif;?>		
													<?php }?>
												<?php endif;?>
											<?php endif; ?>	
										<?php endif;?>
									</div>
								</td>
							</tr>
						</table>
					</div>
					</td>
				</tr>
			</table>
		<?php endif;?>
	<?php else : ?>
		<div class="results results_background" style="width: 950px;height:700px;" align="center">
			<img src="/files/newimg/confused_girl_computer.png" border="none"/><br/>
			<h3>Your search for "<?php echo $keyword;?>" (<?php echo $count;?> product/s found)</h3>
			<p style="padding-left:150px;padding-top:20px;">
				<h2>Sorry, we couldn't locate any offers using your selected search criteria or keywords.<br/>However don't give up, try searching again with other keywords, or phrases that will broaden the search parameters.</h3>
			</p>
		</div>
	<?php endif; ?>
	
	
<!--string highlight script-->	
<script type="text/javascript">
 var searchTerm = '<?php echo $keyword; ?>';
        $('#table_list').removeHighlight();     
     if (searchTerm) {	
            $('#table_list').highlight(searchTerm);
			     }
</script>
<style type="text/css">
	.highlight_string {
	background-color: #FFFFCC
	}
</style>
<!--end string highlight script-->	
	