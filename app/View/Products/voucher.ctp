<?php echo $this->element('message'); ?>
<?php if ($page['ClientPage']['h1'] != '') : ?>
	<h1><?php echo $page['ClientPage']['h1']; ?></h1>
<?php endif; ?>
<?php if ($page['ClientPage']['h2'] != '') : ?>
	<h2><?php echo $page['ClientPage']['h2']; ?></h2>
<?php endif; ?>
<?php if ($page['ClientPage']['h3'] != '') : ?>
	<h3><?php echo $page['ClientPage']['h3']; ?></h3>
<?php endif; ?>
<?php if ($page['ClientPage']['content'] != '') : ?>
	<?php echo $page['ClientPage']['content']; ?>
<?php endif; ?>
<?php if (!empty($product)) : ?>
	<?php echo ($this->element('voucher')); ?>
<?php endif; ?>