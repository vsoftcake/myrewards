	<?php echo $this->element('message'); ?>
	<?php if ($page['ClientPage']['h1'] != '') : ?>
		<h1><?php echo $page['ClientPage']['h1']; ?></h1>
	<?php endif; ?>
	<?php if ($page['ClientPage']['h2'] != '') : ?>
		<h2><?php echo $page['ClientPage']['h2']; ?></h2>
	<?php endif; ?>
	<?php if ($page['ClientPage']['h3'] != '') : ?>
		<h3><?php echo $page['ClientPage']['h3']; ?></h3>
	<?php endif; ?>
	<?php if ($page['ClientPage']['content'] != '') : ?>
		<?php echo $page['ClientPage']['content']; ?>
	<?php endif; ?>
	<?php if  ($type == null) : ?>
	<div class="module" style="width:99%;">
		<div class="tl">
			<div class="tr">
				<div class="t">
					Search offers in your area
				</div>
			</div>
		</div>
		<div class="content" style="height:320px;">
		<form action="<?php echo $this->Html->url('/products/search/location/'); ?>" method="get">
			<div style="width:33%;float:left;">
				<div style="margin-right: 18px;">
					<h3>1. Postcode, Suburb or State</h3>
						<p>Enter your postcode or suburb, or select your state from the map below.</p>
						<?php echo $this->Form->input('Product.search'); ?><br/><br/>
						<img src="<?php echo $this->Html->url('/files/styles/map_image/'. $this->Session->read('client.Style.map_image')); ?>" alt="Map search" usemap="#map" />
						<map name="map" id="map">
							<area shape="poly" coords="57,19,65,24,71,26,72,119,65,125,62,130,45,133,35,139,19,138,22,128,5,94,1,66,15,54,33,48,45,28" href="<?php echo $this->Html->url('/products/search/location/?data%5BProduct%5D%5Bsearch%5D=wa'); ?>" alt="WA" />
							<area shape="poly" coords="83,13,96,9,114,9,109,23,122,34,122,82,73,82,72,28" href="<?php echo $this->Html->url('/products/search/location/?data%5BProduct%5D%5Bsearch%5D=nt'); ?>" alt="NT" />
							<area shape="poly" coords="132,145,128,136,122,121,110,121,92,114,72,119,72,82,134,82,133,144" href="<?php echo $this->Html->url('/products/search/location/?data%5BProduct%5D%5Bsearch%5D=sa'); ?>" alt="SA" />
							<area shape="poly" coords="122,34,134,39,140,27,144,5,150,2,154,25,161,30,165,47,181,63,194,80,196,100,187,102,179,100,136,99,136,81,123,81,121,34" href="<?php echo $this->Html->url('/products/search/location/?data%5BProduct%5D%5Bsearch%5D=qld'); ?>" alt="QLD" />
							<area shape="poly" coords="135,100,184,102,194,102,191,117,186,127,178,149,167,139,153,136,145,126,134,124,136,101" href="<?php echo $this->Html->url('/products/search/location/?data%5BProduct%5D%5Bsearch%5D=nsw'); ?>" alt="NSW ACT" />
							<area shape="poly" coords="135,148,152,158,159,155,164,159,177,151,168,140,152,137,143,127,135,126" href="<?php echo $this->Html->url('/products/search/location/?data%5BProduct%5D%5Bsearch%5D=vic'); ?>" alt="VIC" />
							<area shape="poly" coords="151,166,159,169,166,167,165,177,158,184,151,178,150,166" href="<?php echo $this->Html->url('/products/search/location/?data%5BProduct%5D%5Bsearch%5D=tas'); ?>" alt="TAS" />
						</map>
				</div>
			</div>
			<div style="width:33%;float:left;">
				<div style="margin-right: 20px;">
					<h3>2. Select Category</h3>
					<p>Select a category from the menu below to search for offers in that category.</p>
						<?php echo $this->Form->input('Product.category_id', array('div'=>false, 'label'=>false, 'type'=>'select', 'options'=>$this->Session->read('client.Categories'))); ?><br/><br/>
						<button type="submit">Search &gt;&gt;</button>
				</div>
			</div>
		</form>
			<div style="width:33%;float:left;">
				<div style="margin-right: 20px;">
					<h3>Or Keyword  Search</h3>
					<p>Use the search below to search for offers by keywords.	</p>
					<form action="<?php echo $this->Html->url('/products/search/keyword/'); ?>" method="get">
						<?php echo $this->Form->input('Product.search'); ?><br/><br/>
						<button type="submit">Search &gt;&gt;</button>
					</form>
				</div>
			</div>
		</div>
		<div class="bl">
			<div class="br">
				<div class="b">&nbsp;
				</div>
			</div>
		</div>
	</div>
	<?php else : ?>
	<div style="width: 33%;float:right;">
		<h3 style="margin: 1px 0 5px 15px;"><i><?php 
			if ($this->Form->value('Product.category_id') != '') {
				$categories = $this->Session->read('client.Categories');
				echo $categories[$this->Form->value('Product.category_id')]; 
			} else {
				echo $this->Form->value('Product.search'); 
			}
		?></i> search results</h3>
		<?php if (!empty($products)) : ?>
			<?php echo $this->element('products'); ?>
		<?php endif; ?>
	</div>
	<div style="width:66%;">
		<?php if ($type == 'keyword') : ?>
		<div class="module">
			<div class="tl">
				<div class="tr">
					<div class="t">
						Search benefits in your Area
					</div>
				</div>
			</div>
			<div class="content">	
				<div>
					<h3>Keyword search results</h3>
					<form action="<?php echo $this->Html->url('/products/search/keyword/'); ?>" method="get">
						<?php echo $this->Form->input('Product/search'); ?><br/><br/>
						<button type="submit">Search &gt;&gt;</button>
					</form>
					&nbsp;
				</div>
			</div>
			<div class="bl">
				<div class="br">
					<div class="b">&nbsp;
					</div>
				</div>
			</div>
		</div>
		<?php elseif ($type == 'location') : ?>
		<div class="module">
			<div class="tl">
				<div class="tr">
					<div class="t">
						Search benefits in your Area
					</div>
				</div>
			</div>
			<div class="content">
				<h3>Area search results</h3>
				<form action="<?php echo $this->Html->url('/products/search/location/'); ?>" method="get">
						<?php echo $this->Form->input('Product.search'); ?><br/>
						<?php echo $this->Form->input('Product.category_id', array('div'=>false, 'label'=>false, 'type'=>'select', 'options'=>$this->Session->read('client.Categories'))); ?><br/><br/>
					<button type="submit">Search &gt;&gt;</button>
				</form>
				&nbsp;
			</div>
			<div class="bl">
				<div class="br">
					<div class="b">&nbsp;
					</div>
				</div>
			</div>
		</div>
		<?php endif; ?>
		
		<?php if (isset($special_offers)) : ?>
		<h1 style="margin:  0 0 5px 0">Special Offers</h1>
		<div class="module">
			<div class="tl">
				<div class="tr">
					<div class="t">
						Search results
					</div>
				</div>
			</div>
			<div class="content">
				<?php if (!empty($special_offers)) : ?>
					<?php echo $this->element('special_offers'); ?>
				<?php elseif (isset($special_offers)) : ?>
					<br/>No offers found
				<?php endif; ?>
			</div>
			<div class="bl">
				<div class="br">
					<div class="b">&nbsp;</div>
				</div>
			</div>
		</div>
		<?php endif; ?>
	</div>
	<?php endif; ?>
	