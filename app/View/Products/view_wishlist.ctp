<?php echo $this->element('favourites_keyword_search');?>

<script type="text/javascript">
function confirmPrint(ucount)
{
	if(ucount==-1)
	{
		window.print(); 
		return false;  
	}
	else if(ucount>0)
	{	
		check=confirm('Do you want to proceed to print?');
		if(check)
		{
			window.print(); 
	  	}
	  	else
	  	{
	  		window.location = '/products/view_wishlist';
	  	}
	}
	return false;
}

function checkClick()
{
	var s = document.getElementById('check').value;
	if(s=="Quick Search")
	{
		document.getElementById('check').value='';
		document.myform.submit();
	}
	else
	{
		document.myform.submit();
	}
}
function checkFocus()
{
	var f = document.getElementById('check').value;
	if(f=='Quick Search')
		document.getElementById('check').value='';
}

function checkblur()
{
	var x=document.getElementById("check").value;
	if(x=='' || x==null)
		document.getElementById("check").value='Quick Search';
}
</script>

<div id="wishdiv" class="search_content rounded-corners_bottom  map_border" style="width:950px;">
<?php echo $this->element('message'); ?>
	
	<!--<div style="padding-right: 10px; padding-top: 5px; margin-left: 680px;width:300px" class="no_print">
		<form action="<?php echo $this->Html->url('/products/search2'); ?>" method="get" name="myform">
			<div style="float:left;padding-top:14px;padding-right:8px;">
				<img width="25px" src="/files/newimg/magnifier.png"/>
			</div>
			<div align="center" style="position: relative; width: 150px; height: 40px; top: 12px; float: left;z-index:1">
				<?php echo $this->Form->input('Product.keywords',array('class' => 'quick_search rounded_corners_button','id'=>'check','value'=>'Quick Search','onfocus'=>'checkFocus()','onblur'=>'checkblur()','style'=>'color:#C8D6D6;font-style:italic;')); ?>
			</div>
			<div style="top:15px;position:relative">
				<button type="submit" class="quick-search_button_style rounded_corners_button" onclick="checkClick()">Go !</button>
			</div>
		</form>
	</div>-->
	
	<div style="margin-top: 10px; margin-bottom: 10px; width: 815px; margin-left: 65px;" class="no_print" >
		<div style="float:left;width:600px;margin-top:5px;">
			<h3 style="margin:0px !important;" >My Favourites</h3>
		</div>
		<div style="float:left; padding-left:30px;width:100px;margin-top:10px;">
			<?php if (isset($wish_list)) : ?>
			<?php if (!empty($wish_list)) : ?>
				<a style="text-align:right" onclick="return confirm('Are you sure you want to clear your favorites');" href="/products/clear_wishlist">
					Clear Favourites
				</a>
			<?php endif;?>
			<?php endif;?>
		</div>
		<div  align="right">
			<a class="link_color" style="text-decoration:none;font-weight:normal;" href="javascript:window.history.back()">
				<img src="/files/newimg/back.png" alt="Back" title="Back"/>
			</a>
		</div>
	</div>
	
	
	<?php if (isset($wish_list)) : ?>
		<?php if (!empty($wish_list)) : ?>
		
			<div id="wishbackground" class="rounded_corners_map map_border" style="width: 800px; margin-left: 60px; padding: 10px;background-color:#ffffff;">
			<div>
				<span class="no_print">
					<div align="right">
						<!--<button style="height: 30px; font-size: 15px; width: 150px;" class="rounded_corners_button" type="button" onclick="confirmPrint(<?php echo $usedcount;?>)">Print Coupons</button>-->
						<a href="javascript:void(0)" onclick="confirmPrint(<?php echo $usedcount;?>)">
							<img src="<?php echo $this->Html->url('/files/styles/icon_images/print.png'); ?>" alt="print" title="Print Favourites" />
						</a>
					</div>
				</span>
			</div>	
			
			
			
			<?php $i = 1; ?>
			<?php foreach ($wish_list as $list): ?>
					
		    <div style="position: relative;"> 
		    	<a onclick="return confirm('Are you sure you want to delete product from favourites');" href="<?php echo $this->Html->url('/products/remove_wishlist/'. $list['Product']['id']. '/'. $this->Session->read('user.User.id')); ?>" id="removeWish"> 
		    		<img src="/files/newimg/rem_wish.png" style="position: relative; left: 765px; top: 35px;"/> 
		      	</a> 
		      	<img style="margin-top:-5px;" src="/files/newimg/Online_coupons4_<?php echo $i;?>.jpg" height="267" width="800" alt="" border="0"> 
		      	<div style="position: absolute; top: 0; left: 0;height:275px;"> 
		        	<table height="310">
		          		<tr> 
		            		<td> 
		            			<div style=" width: 400px; padding-left: 30px; padding-right: 50px;"> 
		                			<div style="float:left;"> 
								<div style="min-height:80px;overflow:hidden;">
									<?php $display_image = $list['Product']['display_image']; ?>
		                  														
									<?php 
										if($display_image == 'Merchant Logo') :
											$filepath = MERCHANT_LOGO_PATH;
											$filename = $list['Merchant']['id']. '.'. $list['Merchant']['logo_extension'];
											if (!file_exists($filepath. $filename)) 
											{
												$filepath = PRODUCT_IMAGE_PATH;
												$filename = $list['Product']['id']. '.'. $list['Product']['image_extension'];
												if (!file_exists($filepath. $filename)) 
												{
													$filename = 'transparent.gif';
													$filepath = '../img';
												}
											}
											$path = substr($filepath,strpos($filepath,'webroot')+8);
											$path_prod = $path.$filename;
										elseif($display_image == 'Product Image') : 
											$filepath = PRODUCT_IMAGE_PATH;
											$filename = $list['Product']['id']. '.'. $list['Product']['image_extension'];
											if (!file_exists($filepath. $filename)) 
											{
												$filepath = MERCHANT_LOGO_PATH;
												$filename = $list['Merchant']['id']. '.'. $list['Merchant']['logo_extension'];
												if (!file_exists($filepath. $filename)) 
												{
													$filename = 'transparent.gif';
													$filepath = '../img';
												}
											}
											$path = substr($filepath,strpos($filepath,'webroot')+8);
											$path_prod = $path.$filename;
										endif; 
									?>
					
									<?php $a=$this->ImageResize->getResizedDimensions($path_prod, 100, 100);?>
									<img width="<?php echo $a['width'];?>" height="<?php echo $a['height'];?>" src="<?php echo $this->Html->url('/'. $path_prod); ?>" alt="" /><br/>
				
										</div>                 
										<!--<br/>-->
		                 				<div style="width:150px">
		 									<strong>
		 									<?php echo (isset($list['MerchantAddress']['name']) && $list['MerchantAddress']['name'] != ''? $list['MerchantAddress']['name']: $list['Product']['name']); ?></strong> 
							             	<br/>
							                <?php if($list[0]['count']>1):?>
							                	<?php echo "Multiple Locations";?> 
							                <?php else :?>
							                	<?php if(trim($list['MerchantAddress']['address1']) != ''):?>
							                		<?php echo $list['MerchantAddress']['address1']; ?> 
							                	<?php endif;?>
							                	<?php if(trim($list['MerchantAddress']['address1']) != '' && trim($list['MerchantAddress']['address2']) != ''):?>
							               			<?php echo ", ";?> 
							               	 	<?php endif;?>
							                	<?php if(trim($list['MerchantAddress']['address2']) != ''):?>
							                		<?php echo $list['MerchantAddress']['address2']."<br/>"; ?> 
							                	<?php endif;?>
							                	<?php if(trim($list['MerchantAddress']['suburb']) != ''):?>
							                		<?php echo $list['MerchantAddress']['suburb']; ?> 
							                	<?php endif;?>
							                	<?php if (trim($list['MerchantAddress']['state']) != '' || (trim($list['MerchantAddress']['postcode']) != '' && trim($list['MerchantAddress']['postcode']) != '0')) : ?>
							                	<?php if(trim($list['MerchantAddress']['state']) != ''):?>
								                <?php echo $list['MerchantAddress']['state']; ?> 
								                <?php endif;?>
								                <?php if(trim($list['MerchantAddress']['state']) != '' && trim($list['MerchantAddress']['postcode']) != ''):?>
								                <?php echo ", ";?> 
								                <?php endif;?>
								                <?php if(trim($list['MerchantAddress']['postcode']) != ''):?>
								                <?php echo $list['MerchantAddress']['postcode']; ?> 
								                <?php endif;?>
								                <?php endif; ?>
								                <?php echo (trim(strip_tags($list['MerchantAddress']['phone'])) != ''? '<br/>Tel: '. $list['MerchantAddress']['phone']: ''); ?> 
								                <?php endif;?>
										</div>
		              				</div>
		
		                			
		                  		 <div style="margin-left:160px;padding-top:10px;"> 
                           
                                                <h3 style="margin:0px !important;text-align:center;"><?php echo strip_tags($list['Product']['highlight']); ?></h3> 
                                              
                                                 </div>
                                              <div id="text2" style="padding-top:10px;" title="<?php echo strip_tags($list['Product']['offer']); ?>" >   
                                               <?php  if (strlen($list['Product']['offer']) > '180') { ?>
                                               <?php $offer = strip_tags(substr($list['Product']['offer'],0,180)); ?>
	                                       <p style="text-align:center;"><?php echo $offer.'...' ; ?></p>
                                               <?php } else { ?>
                                               <p style="text-align:center;"><?php echo strip_tags($list['Product']['offer']); } ?></p>
                                               <script type="text/javascript">
                                                   document.getElementById('text2').title="<?php echo strip_tags($list['Product']['offer']); ?>"
                                               </script>
	                                       </div>
		                			
				              	</div>
							</td>
				            <td style="padding-top:65px;" valign="top"> 
				            	<div> 
				                	<strong> Member Name:</strong> 
				                	<?php echo $this->Session->read('user.User.first_name'). ' '. $this->Session->read('user.User.last_name');?><br/>
				                	<strong>Membership Number:</strong> <?php echo $this->Session->read('user.User.username') ?><br/>
				                	<strong>Offer ID:</strong> <?php echo $list['Product']['id']; ?><br/>
				                	<strong>Client:</strong> <?php echo $this->Session->read('client.Client.name'); ?><br/>
				              	</div>
				              
				              	<div style="width: 285px; padding-top: 5px;"> 
				                	<span style="font-size:10px"><?php echo $list['Product']['terms_and_conditions']; ?></span>
				                </div>
							</td>
		          		</tr>
		        	</table>
		      	</div>
		    </div>
					
		    <div style="position:relative;bottom:36px;float:right;margin-right:35px;color:#ffffff;"> 
		      Expires: <?php echo date("d-M-Y", strtotime('+ '.$list['Product']['coupon_expiry'].' days')); ?> 
		    </div>	
			<?php $i++;?>
			<?php endforeach;?>
			</div>
		<?php else : ?>
			<p style="padding-left:40px;font-size:15px;"><?php echo "Your Favourites is empty";?></p>
		<?php endif;?>
	<?php endif;?>
	
	<?php if (isset($wish_list)) : ?>
		<?php if (!empty($wish_list)) : ?>	
			
			<div id="wishPaging" class="paging" style="text-align:right;margin:1em 1.5em 0.5em;">
				<?php echo $this->Paginator->first('<<', array('title'=>'first','class'=>'disabled pagination'));?>
				<?php echo $this->Paginator->prev('<', array('title'=>'prev','class'=>'disabled pagination'));?>
				<?php echo $this->Paginator->numbers(array('separator' => ' ','class'=>'pagination'));?>
				<?php echo $this->Paginator->next('>', array('title'=>'next','class'=>'disabled pagination'));?>
				<?php echo $this->Paginator->last('>>', array('title'=>'last','class'=>'disabled pagination'));?>
			</div>
		<?php endif;?>
	<?php endif;?>
</div>