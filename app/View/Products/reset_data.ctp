<div id="s_content" style="display:block;" class="search_content">
<form action="<?php echo $this->Html->url('/products/search2'); ?>" method="get">
	<table style="width:950px;" border="0">
		
		<tr><td>
		<div style="position: relative; float: right; right: 1px; top: 3px;">
				<img src="/files/newimg/icon_close.png" onclick="SUDEffect('s_content')" style="cursor:pointer"/>
			</div>
			<div style="width:230px;float:left;padding-right:10px;padding-left:5px">
				<table style="width:100%">
					<tr style="height:32px;">
						<td style="width:80px;">Country</td>
						<td>
							<?php echo $this->Form->input('Product.country_id', array('type'=>'select', 'options'=>$this->Session->read('client.Countries'),'selected'=>$this->Session->read('client.Client.country'), 'div'=>false, 'label'=>false,'class'=>'select_input'));
							echo $this->Ajax->observeField('ProductCountryId', array('url' => '/products/update_select','update' => 'ProductSearchStateId'));
							?>
						</td>
					</tr>
					<tr style="height:32px;">
						<td style="width:80px;">Category</td>
						<td>
							<?php echo $this->Form->input('Product.category_id', array('type'=>'select', 'options'=>$categorylist, 'div'=>false, 'label'=>false,'empty'=>'Select category','class'=>'select_input'));
							echo $this->Ajax->observeField('ProductCountryId', array('url' => '/products/update_cat','update' => 'ProductCategoryId'));?>
						</td>
					</tr>
				</table>
			</div>
			<div style="width:230px;float:left;padding-right:10px;">
				<table style="width:100%">
					<?php 
						$cStyle="display:none"; 
						$ctry = $this->params['named']['country_id'];
						if(empty($ctry)):
							if ($this->Session->read('client.Client.country') != 'Hong Kong') :
							 $cStyle="inline";
							endif;
						elseif($ctry !='Hong Kong') :
							$cStyle="inline";
						endif;
					?>
					<tr style="height:32px;<?php echo $cStyle;?>" id="stId">
						<td style="width:70px;">State
						<script language="javascript">
						$(function() {
							$( "#ProductCountryId" ).change(function() {
								if($(this).val()=='Hong Kong')
									$('#stId').hide();
								else
									$('#stId').show();
								if($(this).val()!='Australia') {
									$('#pId').hide();
									$('#rId').hide();
								}else{
									$('#pId').show();
									$('#rId').show();
								}
							});
						});
						</script></td>
						<td>
							<?php echo $this->Form->input('Product.search_state_id', array('type'=>'select', 'options'=>$statelist,'empty'=>'Select State', 'div'=>false, 'label'=>false,'class'=>'select_input'));?>
						</td>
					</tr>
					<tr style="height:32px;">
						<td>Keyword/s</td>
						<?php $title="Keywords refer to search terms that help you find exactly what you are looking for. For example if you want to find a pizza shop in an area then use the keyword 'pizza'. Keep refining the keywords until  you find exactly what you want."; ?>
	                   	<td> 
		                    <div title="<?php  echo $title; ?>">
		                    	<?php echo $this->Form->input('Product.keywords',array('class'=>'search_input', 'div'=>false, 'label'=>false));  ?> 
		                    </div>
	                   </td>
					</tr>
				</table>
			</div>
			<div style="width:280px;float:left;padding-right:10px;">
				<table style="width:100%">
					<?php 
						$pStyle="display:none"; 
						$ptry = $this->params['named']['country_id'];
						if(empty($ptry)):
							if ($this->Session->read('client.Client.country') == 'Australia') :
							 $pStyle="";
							endif;
						elseif($ptry =='Australia') :
							$pStyle="";
						endif;
					
					?>
					<tr style="height:32px;<?php echo $pStyle;?>" id="pId">
						<td style="width:150px;">Suburb / Postcode </td>
						<td>
							<?php echo $this->Form->input('Product.location',array('id'=>'autocomplete', 'class'=>'autocomplete search_input','style'=>'font-size:11px !important;', 'div'=>false, 'label'=>false));?>	
							<script type="text/javascript">
							$( "#autocomplete" ).autocomplete({
								source: "/products/get_postcodes/",
								minLength: 4
							});
							</script> 	
						</td>
					</tr>
					<?php 
						$rStyle="display:none"; 
						$rtry = $this->params['named']['country_id'];
						if(empty($rtry)):
							if ($this->Session->read('client.Client.country') == 'Australia') :
							 $rStyle="";
							endif;
						elseif($rtry =='Australia') :
							$rStyle="";
						endif;
					?>
					<tr style="height:32px;<?php echo $rStyle;?>" id="rId">
						<td>Radius Search</td>
						<td>
							<?php echo $this->Form->input('Product.distance', array('type'=>'select', 'options'=>array(""=> "", "3" => "3 km", "5" => "5 km", "10" => "10 km", "15" => "15 km", "20" => "20 km", "25" => "25 km", "30" => "30 km"), 'div'=>false, 'label'=>false)); ?> approx
						</td>
					</tr>
				</table>
			</div>
			<div style="float:left;">
				<table style="width:100%">
					<tr style="height:20px;">
						<td colspan="2">
							<button type="submit" class="button_style rounded_corners_button">Go !</button>
							<button type="button" style="padding:3px;margin-left:10px" class="rounded_corners_button" onclick="resetdata()">Clear</button>
						</td>
					</tr>
				</table>
			</div></td>
		</tr>
	</table>
	</form>
</div>