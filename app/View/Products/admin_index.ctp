<style>
	.current
	{
	    font-family: 'Droid Sans',sans-serif;
	    font-size: 12px;
	   	padding:5px 8px !important;   
	    background-color: #0066CC;
	    border: 1px solid #DFDFDF;
	    color:#ffffff;
	}
	.cpagination1
	{
		/*padding:1px 2px 1px 2px;*/
		font-family: 'Droid Sans',sans-serif;
	    font-size: 12px;
		text-decoration:none !important;
		background-color: #FFFFFF;
		padding:3px 8px;
		border: 1px solid #0066CC;
		color:#808080 !important;
	}
	#content
	{
		border-left:1px solid #cccccc;
		border-right:1px solid #cccccc;
		border-bottom:1px solid #cccccc;
		margin-left:20px !important;
		padding-left:15px !important;
		width:916px;
	}
	.admin_button
	{
		height: 24px;
		padding-top: 2px;
		padding-bottom: 5px;
		border-radius: 5px 5px 5px 5px;
		background-color: #0066cc;
		border: 1px solid #cccccc;
		color: #FFFFFF; 
		cursor: pointer;
		font-size:13px;
	}
</style>
<table>
	<tr>
    	<td>
			<table>
				<tr>
					<td>
			            &nbsp;&nbsp;<img style="width:40px;height:40px;" src="/files/admin_home_icons/products.png"  alt="Products" title="Products" />&nbsp;&nbsp;
			        </td>
			        <td> 
			            <h2><?php echo "Products";?></h2>
			        </td>
			    </tr>        
			</table> 
    	</td>
  	</tr>
	<tr>
    	<td>
			<div class="actions" style="padding-bottom:20px;padding-top:10px">
				<table style="width:920px;background-color:#cccccc;height:75px;border:1px solid #969090;">
					<tr>
						<td style="width: 700px;">
							<form action="<?php echo $this->Html->url('/admin/products/'); ?>" method="get" id="SearchForm">
								<table>
									<tr>
		<td><b>Status</b></td><td ><?php echo $this->Form->input('Product.active', array ('type' => 'select','label'=>'','options'=>array('1'=>'active','0'=>'inactive'))); ?></td>											
										<td style="padding-left:10px;"><strong>Find Products</strong></td>
										
										<td>
											<?php echo $this->Form->input('Product.search', array('div'=>false, 'label'=>false)); ?>
											<script type="text/javascript">
											$(function() {
												if ($('#ProductSearch').val() == '') {
													$('#ProductSearch').val('');
													$('#ProductSearch').css( "color", "#aaaaaa" );
												}
												$( "#ProductSearch" ).focus(function() {
													if ($(this).val() == '') {
														$(this).val('');
														$(this).css( "color", "#000" );
													}
												});
											});
											</script>
										</td>
										<td><button class="admin_button">Search</button></td>
									</tr>
								</table>
							</form>
						</td>
						<td>
							<button class ="admin_button" onclick="location.href='<?php echo $this->Html->url('edit'); ?>';">Create New Product</button>
						</td>
					</tr>
				</table>
			</div>
     	</td>
   	</tr>
</table>
<div class="list">
	<table cellpadding="0" cellspacing="0" width="920">
		<tr height="30">
			<th style="width:100px;background-color:#cccccc;text-align:left;padding-left:5px;border-left:1px solid #969090;border-bottom:1px solid #969090;border-top:1px solid #969090;"><?php echo $this->Paginator->sort('id');?></th>
			<th style="width:700px;background-color:#cccccc;text-align:left;padding-left:5px;border-bottom:1px solid #969090;border-top:1px solid #969090;"><?php echo $this->Paginator->sort('name');?></th>
			<th class="actions" style="background-color:#cccccc;text-align:left;padding-left:4px;border-right:1px solid #969090;border-bottom:1px solid #969090;border-top:1px solid #969090;"><?php echo "Actions";?></th>
		</tr>
		<?php
		$i = 0;
		foreach ($products as $product):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td>
				<?php echo $product['Product']['id'] ?>
			</td>
			<td>
				<?php echo $product['Product']['name'] ?>
			</td>
			<td class="actions">
				<?php echo $this->Html->link(__('Edit', true), array('action'=>'edit', $product['Product']['id'])); ?> |
				<?php echo $this->Html->link(__('Delete', true), array('action'=>'delete', $product['Product']['id']), null, sprintf(__('Are you sure you want to delete product \'%s\'?', true), $product['Product']['name'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
	<div class="paging" style="padding-top:7px;float:right">
		<table style="border:none !important;">
			<tr>
				<td style="border:none !important;" valign="top">
					<?php echo $this->Paginator->first('<<', array('title'=>'first','class'=>'disabled cpagination1'));?>
					<?php echo $this->Paginator->prev('<', array('title'=>'prev','class'=>'disabled cpagination1'));?>
					<?php echo $this->Paginator->numbers(array('separator' => ' ','class'=>'cpagination1'));?>
					<?php echo $this->Paginator->next('>', array('title'=>'next','class'=>'disabled cpagination1'));?>
					<?php echo $this->Paginator->last('>>', array('title'=>'last','class'=>'disabled cpagination1'));?>
				</td>
				<td style="border:none !important;padding-top:3px;">
				<select name="page" id="selectpage" style="margin-top:-5px;">
					<?php for ($i = 1; $i <= $this->Paginator->counter(array('format' => '%pages%')); $i++):?>
					<option value="<?php echo $i;?>"<?php echo ($this->Paginator->current($params['page']) == $i) ? ' selected="selected"' : ''?>><?php echo $i ?></option>
					<?php endfor ?>
			 	</select>
				<script>
				$("#selectpage").change(function() {
					var url = window.location.href;
					var n = url.indexOf("/?");
					var extra = "";
					if(n>0){extra = url.substring(n,url.length);}
					window.location.href = '/admin/products/index/page:'+$(this).val()+extra;
				});
				</script>
				</td>
			</tr>
		</table>
	</div>
</div>

