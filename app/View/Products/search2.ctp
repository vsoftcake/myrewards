<script type="text/javascript">
function resetdata()
{
	document.getElementById("s_content").style.display="none";
	$.ajax({
		url: "/products/reset_data",
		type: "GET",
		success: function (data) {
			$('#searchDiv').html(data);
		}
	});
}
function SlideEffect(element){
	$( "#"+element ).toggle( "slow" );
}
$(function() {
	$( "#ProductCountryId" ).change(function() {
		if($(this).val()=='Hong Kong')
			$('#stId').hide();
		else
			$('#stId').show();
		if($(this).val()!='Australia') {
			$('#pId').hide();
			$('#rId').hide();
		}else{
			$('#pId').show();
			$('#rId').show();
		}
	});
});
function showWish(pid,uid)
{
	$.ajax({
		url: '/products/wishlist/'+pid+'/'+uid+'/'+Math.random(10),
		type: "GET",
		success: function (data) {
			alert(data);
		}
	});
}
</script>
	<?php
		if (isset($paginator)) {
			$this->Paginator->options(array('url' => array_merge($this->params['pass'], $this->params['named'])));
		}
	?>
	<?php echo $this->element('message'); ?>
	<!--<?php if ($page['ClientPage']['h1'] != '') : ?>
		<h1><?php echo $page['ClientPage']['h1']; ?></h1>
	<?php endif; ?>
	<?php if ($page['ClientPage']['h2'] != '') : ?>
		<h2><?php echo $page['ClientPage']['h2']; ?></h2>
	<?php endif; ?>
	<?php if ($page['ClientPage']['h3'] != '') : ?>
		<h3><?php echo $page['ClientPage']['h3']; ?></h3>
	<?php endif; ?>
	<?php if ($page['ClientPage']['content'] != '') : ?>
		<?php echo $page['ClientPage']['content']; ?>
	<?php endif; ?>-->

	<?php if  ($type == null) : ?>
	<div class="module" style="width:99%;">
		<div class="tl">
			<div class="tr">
				<div class="t">
					Search offers in your area
				</div>
			</div>
		</div>
		<div class="content" style="height:320px;">
		<form action="<?php echo $this->Html->url('/products/search2/location/'); ?>" method="get">
			<div style="width:33%;float:left;">
				<div style="margin-right: 18px;">
					<h3>1. Postcode, Suburb or State</h3>
						<?php if  ($this->Session->read('client.Client.country') == 'Australia') :  ?>
						<p>Enter your postcode or suburb, or select your state from the map below.</p>
						<?php echo $this->Form->input('Product.search',array('class'=>'search_input', 'div'=>false, 'label'=>false)); ?><br/><br/>
						<img src="<?php echo $this->Html->url('/files/styles/map_image/'. $this->Session->read('client.Style.map_image')); ?>" alt="Map search" usemap="#map" />
						<map name="map" id="map">
							<area shape="poly" coords="57,19,65,24,71,26,72,119,65,125,62,130,45,133,35,139,19,138,22,128,5,94,1,66,15,54,33,48,45,28" href="<?php echo $this->Html->url('/products/search2/location/?data%5BProduct%5D%5Bsearch%5D=wa'); ?>" alt="WA" />
							<area shape="poly" coords="83,13,96,9,114,9,109,23,122,34,122,82,73,82,72,28" href="<?php echo $this->Html->url('/products/search2/location/?data%5BProduct%5D%5Bsearch%5D=nt'); ?>" alt="NT" />
							<area shape="poly" coords="132,145,128,136,122,121,110,121,92,114,72,119,72,82,134,82,133,144" href="<?php echo $this->Html->url('/products/search2/location/?data%5BProduct%5D%5Bsearch%5D=sa'); ?>" alt="SA" />
							<area shape="poly" coords="122,34,134,39,140,27,144,5,150,2,154,25,161,30,165,47,181,63,194,80,196,100,187,102,179,100,136,99,136,81,123,81,121,34" href="<?php echo $this->Html->url('/products/search2/location/?data%5BProduct%5D%5Bsearch%5D=qld'); ?>" alt="QLD" />
							<area shape="poly" coords="135,100,184,102,194,102,191,117,186,127,178,149,167,139,153,136,145,126,134,124,136,101" href="<?php echo $this->Html->url('/products/search2/location/?data%5BProduct%5D%5Bsearch%5D=nsw'); ?>" alt="NSW ACT" />
							<area shape="poly" coords="135,148,152,158,159,155,164,159,177,151,168,140,152,137,143,127,135,126" href="<?php echo $this->Html->url('/products/search2/location/?data%5BProduct%5D%5Bsearch%5D=vic'); ?>" alt="VIC" />
							<area shape="poly" coords="151,166,159,169,166,167,165,177,158,184,151,178,150,166" href="<?php echo $this->Html->url('/products/search2/location/?data%5BProduct%5D%5Bsearch%5D=tas'); ?>" alt="TAS" />
						</map>
						<?php endif; ?>

				</div>
			</div>
			<div style="width:33%;float:left;">
				<div style="margin-right: 20px;">
					<h3>2. Select Category</h3>
					<p>Select a category from the menu below to search for offers in that category.</p>
						<?php echo $this->Form->input('Product.category_id', array('type'=>'select', 'options'=>$this->Session->read('client.Categories'), 'div'=>false, 'label'=>false)); ?><br/><br/>
						<button type="submit" class="button_search_module rounded_corners_button">Search</button>
				</div>
			</div>
		</form>
			<div style="width:33%;float:left;">
				<div style="margin-right: 20px;">
					<h3>Or Keyword  Search</h3>
					<p>Use the search below to search for offers by keywords.	</p>
					<form action="<?php echo $this->Html->url('/products/search2/keyword/'); ?>" method="get">
						<?php echo $this->Form->input('Product.search',array('class'=>'search_input', 'div'=>false, 'label'=>false)); ?><br/><br/>
						Category <?php echo $this->Form->input('Product.category_id', array('type'=>'select', 'options'=>$this->Session->read('client.Categories'), 'div'=>false, 'label'=>false, 'empty' => 'All categories')); ?><br/><br/>
						<button type="submit" class="button_search_module rounded_corners_button">Search</button>
					</form>
				</div>
			</div>
		</div>
		<div class="bl">
			<div class="br">
				<div class="b">&nbsp;
				</div>
			</div>
		</div>
	</div>
	<?php else : ?>
	<div class="search_content search_results rounded-corners_bottom">
		<table>
			<tr>
				<td style="vertical-align:top;">
					<table style="width: 950px">
						<tr>
							<td style="vertical-align: top" colspan="2">
								<?php if ($type == 'keyword') : ?>
									<div id="searchDiv">
									<div id="s_content" style="padding-left:20px;display:block;" class="search_content">
									<div style="float: right; position: relative; padding-top: 3px;">
										<img src="/files/newimg/icon_close.png" onclick="SlideEffect('s_content')" style="cursor:pointer"/>
									</div>
										<?php 
											$url_array = array_merge($this->params['pass'], $this->params['named']);
											unset($url_array['page']);
										?>
										<form action="<?php echo $this->Html->url($url_array); ?>" method="get">
											<table style="width:900px;" border="0">
											<tr><td>
												<div style="width:230px;float:left;padding-right:10px;padding-left:3px;">
													<table style="width:100%">
														<tr style="height:32px;">
															<td style="width:80px;">Country</td>
															<td>
																<?php echo $this->Form->input('Product.country_id', array('type'=>'select', 'div'=>false, 'label'=>false, 'options'=>$this->Session->read('client.Countries'),'class'=>'select_input'));
																echo $this->Ajax->observeField('ProductCountryId', array('url' => '/products/update_select','update' => 'ProductSearchStateId'));
																echo $this->Ajax->observeField('ProductCountryId', array('url' => '/products/update_cat','update' => 'ProductCategoryId'));
																?>
															</td>
														</tr>
														<tr style="height:32px;">
															<td style="width:80px;">Category</td>
															<td>
																<?php echo $this->Form->input('Product.category_id', array('type'=>'select', 'div'=>false, 'label'=>false, 'options'=>$categorylist,'empty'=>'Select category','class'=>'select_input'));?>
															</td>
														</tr>
													</table>
												</div>
												<div style="width:230px;float:left;padding-right:4px;">
													<table style="width:100%">
														<?php 
															$cStyle="display:none"; 
															$ctry = $this->params['named']['country_id'];
															if(empty($ctry)):
																if ($this->Session->read('client.Client.country') != 'Hong Kong') :
																	$cStyle="";
																endif;
															elseif($ctry !='Hong Kong') :
																$cStyle="";
															endif;
														?>
														<tr style="height:32px;<?php echo $cStyle;?>" id="stId">
															<td style="width:70px;">State</td>
															<td>
																<?php echo $this->Form->input('Product.search_state_id', array('type'=>'select', 'div'=>false, 'label'=>false, 'options'=>$statelist,'empty'=>'Select State','class'=>'select_input'));?>
															</td>
														</tr>
														<tr style="height:32px;">
															<td>Keyword/s</td>
															<?php $title="Keywords refer to search terms that help you find exactly what you are looking for. For example if you want to find a pizza shop in an area then use the keyword 'pizza'. Keep refining the keywords until  you find exactly what you want."; ?>
										                   	<td> 
											                    <div title="<?php  echo $title; ?>">
											                    	<?php echo $this->Form->input('Product.keywords',array('class'=>'search_input', 'div'=>false, 'label'=>false));  ?> 
											                    </div>
										                   </td>
														</tr>
													</table>
												</div>
												<div style="width:280px;float:left;padding-right:10px;">
													<table style="width:100%">
														<?php 
															$pStyle="display:none"; 
															$ptry = $this->params['named']['country_id'];
															if(empty($ptry)):
																if ($this->Session->read('client.Client.country') == 'Australia') :
																 $pStyle="";
																endif;
															elseif($ptry =='Australia') :
																$pStyle="";
															endif;
														
														?>
														<tr style="height:32px;<?php echo $pStyle;?>" id="pId">
															<td style="width:150px;">Suburb / Postcode </td>
															<td>
																<?php echo $this->Form->input('Product.location',array('id'=>'autocomplete', 'class'=>'autocomplete search_input','style'=>'font-size:11px !important;', 'div'=>false, 'label'=>false));?>	
																<script type="text/javascript">
																$( "#autocomplete" ).autocomplete({
																	source: "/products/get_postcodes/",
																	minLength: 4
																}); 
																</script> 	
															</td>
														</tr>
														<?php 
															$rStyle="display:none"; 
															$rtry = $this->params['named']['country_id'];
															if(empty($rtry)):
																if ($this->Session->read('client.Client.country') == 'Australia') :
																 $rStyle="";
																endif;
															elseif($rtry =='Australia') :
																$rStyle="";
															endif;
														?>
<?php  
if($this->Session->read('client.Client.id')!=1618){ ?>
														<tr style="height:32px;<?php echo $rStyle;?>" id="rId">
															<td>Radius Search</td>
															<td>
																<?php echo $this->Form->input('Product.distance', array('type'=>'select', 'options'=>array(""=> "", "3" => "3 km", "5" => "5 km", "10" => "10 km", "15" => "15 km", "20" => "20 km", "25" => "25 km", "30" => "30 km"), 'div'=>false, 'label'=>false)); ?> approx
															</td>
														</tr>
<?php } ?>
													</table>
												</div>
												<div style="float:left;">
													<table style="width:100%">
														<tr style="height:20px;">
															<td colspan="2">
																<button type="submit" class="button_style rounded_corners_button">Go !</button>
																<button type="button" style="padding:3px;margin-left:10px" class="rounded_corners_button" onclick="resetdata()">Clear</button>
															</td>
														</tr>
													</table>
												</div></td>
											</tr>
											
										</table>
										</form>
									</div></div>
									<?php if (isset($products)) : ?>
										<?php if (!empty($products)) : ?>
											<div class="paging" style="float:left;margin:1em 1em 0em;">
												<?php $url_array = array_merge($this->params['pass'], $this->params['named']);
													unset($url_array['page']);
												?>
												<form action="<?php echo $this->Html->url($url_array); ?>" name="form" method="get" style="float:left;">
													Order by <?php echo $this->Form->input('Product.sort', array('type'=>'select', 'options'=>$sorts, 'div'=>false, 'label'=>false, 'onChange'=>"form.submit();")); ?>
												</form>
											</div>
											
											<div class="paging" style="line-height: 35px; float: right; margin: 0.5em 0.5em 1em;">
												<?php echo $this->Paginator->first('<<', array('title'=>'first','class'=>'disabled pagination'));?>
												<?php echo $this->Paginator->prev('<', array('title'=>'prev','class'=>'disabled pagination'));?>
												<?php echo $this->Paginator->numbers(array('separator' => ' ','class'=>'pagination'));?>
												<?php echo $this->Paginator->next('>', array('title'=>'next','class'=>'disabled pagination'));?>
												<?php echo $this->Paginator->last('>>', array('title'=>'last','class'=>'disabled pagination'));?>
												&nbsp;
												<select name="page" id="selectpage" style="margin-top:-5px;">
													<?php for ($i = 1; $i <= $this->Paginator->counter(array('format' => '%pages%')); $i++):?>
													<option value="<?php echo $i;?>"<?php echo ($this->Paginator->current($params['page']) == $i) ? ' selected="selected"' : ''?>><?php echo $i ?></option>
													<?php endfor ?>
												</select>
												<script>
												$("#selectpage").change(function() {
													var url = window.location.href;
													var n = url.indexOf("/?");
													var extra = "";
													if(n>0){extra = url.substring(n,url.length);}
													window.location.href = '/products/search2/page:'+$(this).val()+extra;
												});
												</script>
											</div><br/>
										<?php endif; ?>
									<?php endif; ?>
								
								<?php elseif ($type == 'location') : ?>
									<h1 style="margin:0;">Area search results</h1>
									<h2 style="font-weight:normal">
										<?php echo $this->Paginator->params['paging']['ProductList2']['count']; ?> results
										<?php if ($this->Form->value('Product.search') != '') : ?>
											for <b><i><?php echo $this->Form->value('Product.search'); ?></i></b>
										<?php endif; ?>
										<?php if ($this->Form->value('Product.category_id') != ''):
											$categories = $this->Session->read('client.Categories'); ?>
											in <b><i><?php echo $categories[$this->Form->value('Product.category_id')]; ?></i></b>
										<?php else : ?>
											in <b><i>all categories</i></b>
										<?php endif; ?>
									</h2>
									<br/>
									<table style="width:100%;">
										<tr>
											<td>
												<form action="<?php echo $this->Html->url('/products/search2/location/'); ?>" method="get">
													Search again for <?php echo $this->Form->input('Product.search',array('class'=>'search_input', 'div'=>false, 'label'=>false)); ?>
													in <?php echo $this->Form->input('Product.category_id', array('type'=>'select', 'options'=>$this->Session->read('client.Categories'), 'div'=>false, 'label'=>false, 'empty'=>'All Categories')); ?>
													<button type="submit" class="button_search_module rounded_corners_button">Search</button>
												</form>
											</td>
											<td style="text-align:right">
												<?php $url_array = array_merge($this->params['pass'], $this->params['named']);
													unset($url_array['page']);
												?>
												<form action="<?php echo $this->Html->url($url_array); ?>" method="get" style="float:right;">
													Order by <?php echo $this->Form->input('Product.sort', array('type'=>'select', 'options'=>$sorts, 'div'=>false, 'label'=>false)); ?>
													<button type="submit" class="button_search_module rounded_corners_button">Sort</button>
												</form>
											</td>
										</tr>
									</table>
									<!--<div class="paging" style="text-align:right;margin:1em 0 0.5em;">
										<?php echo $this->Paginator->counter(array('format' => __('Page %page% of %pages%', true)));	?> <?php echo $this->Paginator->prev('<< Prev', array(), null, array('class'=>'disabled'));?>
										<?php echo $this->Paginator->numbers();?>
										<?php echo $this->Paginator->next('Next >>', array(), null, array('class'=>'disabled'));?>
									</div>-->
									<div class="paging" style="text-align:right;margin:1em 0 0.5em;">
										<?php echo $this->Paginator->prev('<<', array(), null, array('class'=>'disabled pagination'));?>
										<?php echo $this->Paginator->numbers(array('separator' => ' ','class'=>'pagination'));?>
										<?php echo $this->Paginator->next('>>', array(), null, array('class'=>'disabled pagination'));?>
									</div>
								
								<?php elseif ($type == 'category') : ?>
									<h1 style="margin:0;">Category search results</h1>
									<?php if ($category['Category']['details'] != '') : ?>
										<br/><h2 style="margin:0;"><?php echo $category['Category']['details']; ?></h2>
									<?php endif; ?>
									<h2 style="font-weight:normal">
										<?php echo $this->Paginator->params['paging']['ProductList2']['count']; ?> results
										<?php if ($this->Form->value('Product.search') != '') : ?>
											for <b><i><?php echo $this->Form->value('Product.search'); ?></i></b>
										<?php endif; ?>
										<?php if ($this->Form->value('Product.category_id') != ''): ?>
											in <b><i><?php echo $category['Category']['name']; ?></i></b>
										<?php else : ?>
											in <b><i>all categories</i></b>
										<?php endif; ?>
									</h2>
									<br/>
									<table style="width:100%;">
										<tr>
											<td>
												<form action="<?php echo $this->Html->url('/products/search2/category/'); ?>" method="get">
													Search again in <?php echo $this->Form->input('Product.category_id', array('type'=>'select', 'options'=>$this->Session->read('client.Categories'), 'div'=>false, 'label'=>false, 'empty'=>'All categories')); ?>
													<button type="submit" class="button_search_module rounded_corners_button">Search</button>
												</form>
											</td>
											<td style="text-align:right">
												<?php $url_array = array_merge($this->params['pass'], $this->params['named']);
													unset($url_array['page']);
												?>
												<form action="<?php echo $this->Html->url($url_array); ?>" method="get" style="float:right;">
													Order by <?php echo $this->Form->input('Product.sort', array('type'=>'select', 'options'=>$sorts, 'div'=>false, 'label'=>false)); ?>
													<button type="submit" class="button_search_module rounded_corners_button">Sort</button>
												</form>
											</td>
										</tr>
									</table>
									<!--<div class="paging" style="text-align:right;margin:1em 0 0.5em;">
										<?php echo $this->Paginator->counter(array('format' => __('Page %page% of %pages%', true)));	?> <?php echo $this->Paginator->prev('<< Prev', array(), null, array('class'=>'disabled'));?>
										<?php echo $this->Paginator->numbers();?>
										<?php echo $this->Paginator->next('Next >>', array(), null, array('class'=>'disabled'));?>
									</div>-->
									<div class="paging" style="text-align:right;margin:1em 0 0.5em;">
										<?php echo $this->Paginator->prev('<<', array(), null, array('class'=>'disabled pagination'));?>
										<?php echo $this->Paginator->numbers(array('separator' => ' ','class'=>'pagination'));?>
										<?php echo $this->Paginator->next('>>', array(), null, array('class'=>'disabled pagination'));?>
									</div>
								<?php endif; ?>
							</td>
						</tr>
						<tr>
							<td valign="top">
								<div style="float:left;width:770px">
									<?php if (isset($products)) : ?>
										<?php if (!empty($products)) : ?>
											<div style="padding-left:8px;">
												<?php foreach ($products as $product): ?>
													<?php echo $this->element('search_result', array('product' => $product)); ?>
												<?php endforeach; ?>
											</div>
										<?php else : ?>
											<a class="link_color" style="font-weight:normal;margin-left:25px;display:none" id="back" href="javascript:window.history.back()"><img src="/files/newimg/back.png" alt="Back" title="Back"/></a><br/>
											<div style="padding-left:0px;" align="center">
												<img src="/files/newimg/confused_girl_computer.png" border="none"/><br/>
												<p style="padding-left:150px;font-size:20px;font-size:bold;font-family: 'Droid Sans', sans-serif">
													Sorry, we couldn't locate any offers using your selected search criteria or keywords.<br/>However don't give up, try searching again with other keywords, or phrases that will broaden the search parameters.
												</p>
											</div>
										<?php endif; ?>
									<?php endif; ?>
								</div>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td>
					<?php if (isset($products)) : ?>
						<?php if (!empty($products)) : ?>
							<div class="paging" style="text-align: right; margin-right: 60px; line-height: 25px;">
								<?php echo $this->Paginator->first('<<', array('title'=>'first','class'=>'disabled pagination'));?>
								<?php echo $this->Paginator->prev('<', array('title'=>'prev','class'=>'disabled pagination'));?>
								<?php echo $this->Paginator->numbers(array('separator' => ' ','class'=>'pagination'));?>
								<?php echo $this->Paginator->next('>', array('title'=>'next','class'=>'disabled pagination'));?>
								<?php echo $this->Paginator->last('>>', array('title'=>'last','class'=>'disabled pagination'));?>
							</div>
							  <div style="float: right; margin-top: -20px; margin-right: 5px;margin-left:3px;">
							   <form style="float:right;margin-top:-2px;" action="<?php echo $this->Html->url($url_array).$_REQUEST[paginate]; ?>"  name="paginate" method="get">
							 		<select name="page" onchange="paginate.submit();" >
							 	 	<?php for ($i = 1; $i <= $this->Paginator->counter(array('format' => '%pages%')); $i++):?>
							 	 	   	<option value="<?php echo $i;?>"<?php echo ($this->Paginator->current($params['page']) == $i) ? ' selected="selected"' : ''?>><?php echo $i ?></option>
							 	 	  <?php endfor ?>
								 </select>
								 
							  </form>
							</div>
						<?php endif; ?>
					<?php endif; ?>
				</td>
			</tr>
			<tr><td style="height:10px;">&nbsp;&nbsp;</td></tr>
		</table>
	</div>
	<?php endif; ?>