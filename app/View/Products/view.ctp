<script type="text/javascript">
function showWish(pid,uid)
{
	$.ajax({
		url: '/products/wishlist/'+pid+'/'+uid,
		type: "GET",
		success: function (data) {
			$('#divout').html(data);
		}
	});
}
function showRedeem(pid,uid)
{
	$.ajax({
		url: '/products/redeemlist/'+pid+'/'+uid+'/'+Math.random(10),
		type: "GET",
		success: function (data) {
			$('#divout1').html(data);
		}
	});
}
function addcomments(pid) {
	$("#modalboxpopup").html("").load("/comments/add_comments/"+pid).dialog({width: 730, modal: true, "position": "top"});
}
</script>
<?php echo $this->element('view_keyword_search_module', array('title' => 'Search'));?>
	<?php if ($page['ClientPage']['h1'] != '') : ?>
		<h1><?php echo $page['ClientPage']['h1']; ?></h1>
	<?php endif; ?>
	<?php if ($page['ClientPage']['h2'] != '') : ?>
		<h2><?php echo $page['ClientPage']['h2']; ?></h2>
	<?php endif; ?>
	<?php if ($page['ClientPage']['h3'] != '') : ?>
		<h3><?php echo $page['ClientPage']['h3']; ?></h3>
	<?php endif; ?>
	<?php if ($page['ClientPage']['content'] != '') : ?>
		<?php echo $page['ClientPage']['content']; ?>
	<?php endif; ?>
	<?php echo $product_message; ?>
	<table width="952px">
		<tr>
			<td style="width:767px;">
				<?php foreach ($products as $product) : ?>
				<div class="module" style="float:left;width:760px;">
						<div class="content tab_padding" style="padding:6px 12px 0 ! important;white-space:normal !important;">
							<div style="padding:5px;float:left">
								<?php $backimg = $this->Session->read('client.Program.id')==7?"/files/newimg/wellplan_back.png":"/files/newimg/back.png";?>
								<a class="link_color" style="font-weight:normal;display:none" id="back"  href="javascript:window.history.back()"><img src=<?php echo $backimg;?> alt="Back" title="Back"/></a>
							</div>
							<div class="rounded_corners_map map_border" style="padding:10px;background-color:#ffffff;">
								<div id="divout" style="font-weight: bold;" class="wishlist"></div>
								<table width="100%">
									<tr>
										<td>
											<div style="width:565px;" id="table_list">
												<div style="font-size:16px;font-weight:bold;padding-bottom:8px"><?php echo $product['Product']['name']; ?></div>
												<table width="100%">
													<tr>
														<td width="100"><?php $offerhighimg = $this->Session->read('client.Program.id')==7?"/files/newimg/wellplan_offer_highlight.png":"/files/newimg/offer_highlight.png";?>
															<img style="margin-bottom:5px;left:-25px;position:relative" src="<?php echo $offerhighimg;?>"/>
														</td>
														<td>	
															<h3 style="margin-bottom:15px !important;"><?php echo $product['Product']['highlight']; ?></h3>
														</td>
													</tr>
												</table>
												<div>
													<?php  echo $product['Product']['details']; ?>
												</div>
												<p style="font-size:15px;color:#C0C0C0;font-style:italic;">
												
												<?php $i = 0; ?>
													<?php foreach($categories_list as $clist) :?>
													
													<?php  echo $clist['Category']['name']; $i++; ?>
													<?php if($i>0 && $i<sizeof($categories_list)): echo ", " ;  endif; ?>													
													<?php endforeach;?>
													
													<?php if (!($product['location_count'] > 1)) : ?>
													<?php if (sizeof($categories_list)>1 && ($product['MerchantAddress']['suburb'] != '' || $product['MerchantAddress']['state'] != '')) : ?>
													
													<?php echo ", ";endif;?>
													<?php if ($product['MerchantAddress']['suburb'] != '0' && $product['MerchantAddress']['suburb'] != '') : ?>
															<?php echo $product['MerchantAddress']['suburb'].' '; ?>
													<?php endif; ?>
													<?php if ($product['MerchantAddress']['state'] != '0' && $product['MerchantAddress']['state'] != '') : ?>
															<?php echo $state; ?>
													<?php endif; ?>
													<?php endif; ?>
												</p>
												<p><?php  echo $product['Product']['text']; ?></p>
												
												<?php if (!($product['location_count'] > 1)) : ?>
												<p style="font-size:13px;font-style:italic;">
													<?php if ($product['MerchantAddress']['address1'] != '0' && trim($product['MerchantAddress']['address1']) != '') : ?>
															<?php echo $product['MerchantAddress']['address1'].', '; ?>
													<?php endif; ?>
													<?php if ($product['MerchantAddress']['address2'] != '0' && trim($product['MerchantAddress']['address2']) != '') : ?>
															<?php echo $product['MerchantAddress']['address2'].', '; ?>
													<?php endif; ?>
													<?php if ($product['MerchantAddress']['suburb'] != '0' && trim($product['MerchantAddress']['suburb']) != '') : ?>
															<?php echo $product['MerchantAddress']['suburb'].' '; ?>
													<?php endif; ?>
													<?php if ($product['MerchantAddress']['state'] != '0' && trim($product['MerchantAddress']['state']) != '') : ?>
															<?php echo $state.' '; ?>
													<?php endif; ?>
													<?php if ($product['MerchantAddress']['postcode'] != '0' && trim($product['MerchantAddress']['postcode']) != '') : ?>
															<?php if ($product['MerchantAddress']['postcode'] != '0') 
																{ echo $product['MerchantAddress']['postcode']; 
																} 
															?>
													<?php endif; ?>
													<?php if (trim($product['MerchantAddress']['state']) != '' || trim($product['MerchantAddress']['postcode']) != '' || trim($product['MerchantAddress']['suburb']) != '') : ?>
														<?php echo ", ";?>
													<?php endif;?>
													<?php if ($product['MerchantAddress']['country'] != '0' && trim($product['MerchantAddress']['country']) != '') : ?>
															<?php echo $product['MerchantAddress']['country']; ?>
													<?php endif; ?>
												</p>
												<?php endif;?>
												<?php if ($product['Product']['available_international'] == 1 && ($product['Product']['available_national'] != 1 || $product['Product']['available_national'] == 1)) : ?>
													<h3>INTERNATIONAL OFFER</h3>
												<?php elseif ($product['Product']['available_international'] !=1 && $product['Product']['available_national'] == 1) : ?>
													<h3>NATIONAL OFFER</h3>
												<?php endif;?>
												<?php if ($product['MerchantAddress']['phone'] != '0' && trim($product['MerchantAddress']['phone']) != '') : ?>
												<table>
													<tr>
														<td valign="top">
															<img style="padding-right:10px" src="/files/newimg/phone_view.png"/>
														</td>
														<td valign="middle">
																<span><b><?php echo $product['MerchantAddress']['phone']; ?></b></span>
														</td>
													</tr>
												</table>
												<?php endif;?>
												
												<?php if ($product['Product']['link1'] != '' || $product['Product']['link2'] != '') : ?>
												<table>
													<tr>
														<td valign="top">
															<img style="padding-right:10px" src="/files/newimg/web.png"/>
														</td>
														<td valign="middle">
															<a href="<?php echo htmlentities($product['Product']['link1']); ?>" target="_blank">
																<?php if($product['Product']['link1_text'] !='' || !empty($product['Product']['link1_text'])) :?>
																	<?php echo $product['Product']['link1_text'];?>
																<?php else : ?>
																	<?php echo $product['Product']['link1']; ?>
																<?php endif ;?>
															</a><br/>
															<a href="<?php echo htmlentities($product['Product']['link2']); ?>" target="_blank">
																<?php if($product['Product']['link2_text'] !='' || !empty($product['Product']['link2_text'])) :?>
																	<?php echo $product['Product']['link2_text'];?>
																<?php else : ?>
																	<?php echo $product['Product']['link2']; ?>
																<?php endif ;?>
															</a>
														</td>
													</tr>
												</table>
												<?php endif;?>
												<?php if(isset($product['merchant_addresses'])) : ?>
													<br/><b>Locations111</b><br/>
													<?php foreach ($product['merchant_addresses'] as $address) : ?>
														<a href="<?php echo $this->Html->url('/products/view/'. $product['Product']['id']. '/'. $address['MerchantAddress']['id']); ?>"><?php echo $address['MerchantAddress']['suburb']; ?></a>	<br/>
													<?php endforeach; ?>
												<?php elseif ($product['location_count'] > 1) : ?>
													<br/>
													<div id="locations_<?php echo $product['Product']['id']; ?>"><a href="javascript:void(0)" onclick="return displayLocations('<?php echo $product['Product']['id']; ?>');">Locations</a></div>
													<script type="text/javascript">
														function displayLocations(id) {
															$('#locations_' + id).html('Retreiving locations');
															$.ajax({
																url: "/products/ajax_addresses/<?php echo $product['Product']['id'];?>/<?php echo $product['Merchant']['id'];?>/<?php echo $product['MerchantAddress']['id'];?>",
																type: "GET",
																success: function (data) {
																	$('#locations_' + id).html(data);
																}
															});
															return false;
														}
													</script>
												<?php endif; ?>
											</div>
										</td>
										<!--<td style="vertical-align:top;text-align:center;width:150px;">
											
											<?php 
												//Merchant logo resize
												$filepath_merchant = MERCHANT_LOGO_PATH;
												$filename_merchant = $product['Merchant']['id']. '.'. $product['Merchant']['logo_extension'];

												//Product image resize              
												$filepath_product = PRODUCT_IMAGE_PATH;
												$filename_product = $product['Product']['id']. '.'. $product['Product']['image_extension'];

											?>
											<?php if((!file_exists($filepath_merchant. $filename_merchant)) && (!file_exists($filepath_product. $filename_product))) : 
												$filepath = CATEGORY_LOGO_PATH;
												$filename = $categories_list[0]['Category']['id']. '.'. $categories_list[0]['Category']['logo_extension'];

												$path = substr($filepath,strpos($filepath,'webroot')+8);
												$path_cat = $path.$filename;

												$cat=$this->ImageResize->getResizedDimensions($path_cat, 150, 150);
											?>
												<img width="<?php echo $cat['width'];?>" height="<?php echo $cat['height'];?>" src="<?php echo $this->Html->url('/'. $path_cat);?>" alt="" /><br/><br/>

											<?php else : ?>

												<?php if (file_exists($filepath_product. $filename_product)) :
													$path = substr($filepath_product,strpos($filepath_product,'webroot')+8);
													$path_prod = $path.$filename_product;
													$a=$this->ImageResize->getResizedDimensions($path_prod, 150, 150);
												?>
													<img width="<?php echo $a['width'];?>" height="<?php echo $a['height'];?>" src="<?php echo $this->Html->url('/'. $path_prod);?>" alt="" />
													<br/><br/>
												<?php endif; ?>

												<?php if (file_exists($filepath_merchant. $filename_merchant)) :
													$path = substr($filepath_merchant,strpos($filepath_merchant,'webroot')+8);
													$path_mer = $path.$filename_merchant;
													$mer=$this->ImageResize->getResizedDimensions($path_mer, 150, 150);
												?>
													<img width="<?php echo $mer['width'];?>" height="<?php echo $mer['height'];?>" src="<?php echo $this->Html->url('/'. $path_mer);?>" alt="" /><br/><br/>
												<?php endif;?>

											<?php endif; ?>
										</td>-->
										<td style="vertical-align:top;text-align:center;width:150px;">

											<?php 
											//Merchant logo resize
											$filepath_merchant = MERCHANT_LOGO_PATH;
											$filename_merchant = $product['Merchant']['id']. '.'. $product['Merchant']['logo_extension'];

											if (!file_exists($filepath_merchant. $filename_merchant)) 
											{
												$filename_merchant = 'transparent.gif';
												$path = 'img/';
											}else{
												$path = substr($filepath_merchant,strpos($filepath_merchant,'webroot')+8);
											}
											
											$path_mer = $path.$filename_merchant;
											$mer=$this->ImageResize->getResizedDimensions($path_mer, 150, 150);?>
											<img width="<?php echo $mer['width'];?>" height="<?php echo $mer['height'];?>" src="<?php echo $this->Html->url('/'. $path_mer);?>" alt="" /><br/><br/>

											<?php
											//Product image resize              
											$filepath_product = PRODUCT_IMAGE_PATH;
											$filename_product = $product['Product']['id']. '.'. $product['Product']['image_extension'];

											if (!file_exists($filepath_product. $filename_product)) 
											{
												$filename_product = 'transparent.gif';
												$path = 'img/';
											}else{
												$path = substr($filepath_product,strpos($filepath_product,'webroot')+8);
											}

											$path_prod = $path.$filename_product;
											$a=$this->ImageResize->getResizedDimensions($path_prod, 150, 150);?>
											<img width="<?php echo $a['width'];?>" height="<?php echo $a['height'];?>" src="<?php echo $this->Html->url('/'. $path_prod);?>" alt="" />
										</td>
									</tr>
									<tr>
										<?php if  ($product['Product']['quantity'] != '0') : ?>
										<td align="">
										<a href="<?php echo 'http://www.perksatwork.com.au/index.html?user='. $this->Session->read('user.User.username'); ?>" target="_blank"> <img src="http://www.myrewards.com.au/images/shop-now-button.gif" style='padding-right:120px;cursor:pointer;float:right;width:150px;'></a>&nbsp;&nbsp;
										</td>
										<?php else : ?>
										<td align="">
											<?php if  ((count($cartproduct)>0) && ($this->Session->read('dashboardidinsession')!=3)) : ?>
														<?php echo $this->Html->link($this->Html->image('/images/shop-now-button.gif', array('style'=>'padding-right:120px;cursor:pointer;float:right;width:150px;')),array('controller'=>'carts','action' => 'add_cart/'.$product['Product']['id']),array('escape'=>false, 'onclick' => "Modalbox.show(this.href, {title: this.title, width: 730}); return false;"));?>
											<?php endif; ?>
										</td>
										<?php endif; ?>
									</tr>

									<tr>
										<td colspan="2">
											<hr style="border:1px dashed #cccccc;">
											<div style="float:left;">
												<?php if  ($product['Product']['phone_benefit'] == '1') : ?>
													<img style="padding:5px 25px;border-right:1px solid #cccccc" src="<?php echo $this->Html->url('/files/styles/icon_images/phone.png'); ?>" alt="phone" title="Make bookings, reservations or purchases over the phone" /> 
												<?php endif; ?>
												<?php if  ($product['Product']['link1'] != '') : ?>
													<a href="<?php echo $product['Product']['link1']; ?>" target="_blank">
														<img style="padding:5px 25px;border-right:1px solid #cccccc" src="<?php echo $this->Html->url('/files/styles/icon_images/link.png'); ?>" alt="web link" title="Click to go to the merchant web site" />
													</a>
												<?php endif; ?>
												<?php if  ($product['Product']['show_and_save'] == '1') : ?>
													<img style="padding:5px 25px;border-right:1px solid #cccccc" src="<?php echo $this->Html->url('/files/styles/icon_images/card.png'); ?>" alt="show and save" title="Present you mobile phone coupon or membership card" />
												<?php endif; ?>
												<?php if  ($product['Product']['web_coupon'] == '1' && ($product['Product']['used_count'] > 0 || $product['Product']['used_count'] == -1) && ($usedcount>0 || $usedcount== -1 ) && ($this->Session->read('dashboardidinsession')!=3)) : ?>
													<a href="<?php echo $this->Html->url('/products/voucher/'. $product['Product']['id']. '/'. $product['MerchantAddress']['id']); ?>" >
														<img style="padding:5px 20px;border-right:1px solid #cccccc" src="<?php echo $this->Html->url('/files/styles/icon_images/print.png'); ?>" alt="print" title="Print your web coupon" />
													</a>
				 								<?php endif; ?>
												
												<?php if  ($product['Product']['web_coupon'] == '1' && ($product['Product']['used_count'] > 0 || $product['Product']['used_count'] == -1) && ($usedcount>0 || $usedcount== -1 ) && ($this->Session->read('dashboardidinsession')!=3)) : ?>
													<a href="<?php echo $this->Html->url('/products/voucher/'. $product['Product']['id']. '/'. $product['MerchantAddress']['id']); ?>">
														<img style="padding:5px 20px;border-right:1px solid #cccccc" src="<?php echo $this->Html->url('/files/styles/icon_images/voucher.png'); ?>" alt="web coupon" title="Print a web coupon to present to merchant " />
													</a>
												<?php endif; ?>
												<?php if  (($this->Session->read('dashboardidinsession') == '3') && ($productpoints['ProductsPoint']['points']>0 )): ?>
													<a href="javascript:void(0)" onclick="showRedeem(<?php echo $product['Product']['id'].','.$this->Session->read('user.User.id');?>)">
														<?php echo $this->Html->image('/files/styles/icon_images/redeem.png', array('style'=>'padding:5px 20px;border-right:1px solid #cccccc;cursor:pointer;','alt' => 'Redeem','title'=>'Redeem your points for this item', 'escape'=>false, 'onclick'=>"redeempoints(".$product['Product']['id'].")"));?>
													</a>							
												<?php endif;?>
												<?php if  ((count($cartproduct)>0) && ($this->Session->read('dashboardidinsession')!=3)) : ?>
														<?php echo $this->Html->image('/files/styles/icon_images/shop.png', array('style'=>'padding:5px 20px;border-right:1px solid #cccccc;cursor:pointer;', 'escape'=>false,'title'=>'Purchase in our online store', 'onclick'=>"addcartpopup(".$product['Product']['id'].")"));?>
												<?php endif; ?>
												<?php if  (($product['Product']['web_coupon'] == '1') && ($product['Product']['used_count'] > 0 || $product['Product']['used_count'] == -1) && ($usedcount>0 || $usedcount== -1 )) : ?>
													<a href="javascript:void(0)" onclick="showWish(<?php echo $product['Product']['id'].','.$this->Session->read('user.User.id');?>)">
														<img style="padding:5px;" src="<?php echo $this->Html->url('/files/styles/icon_images/wishlist.png'); ?>" alt="Add to Favourites" title="Add to your favorite web coupons list"/>
													</a>							
												<?php endif;?>
												<?php if  (strtotime($product['Product']['created']) > strtotime('-2 month')) : ?>
													<img style="padding:5px 25px;border-right:1px solid #cccccc" src="<?php echo $this->Html->url('/files/styles/icon_images/new.png'); ?>" alt="new" title="This offer is new or has been modified "/>
												<?php endif; ?>
												<?php if($product['Product']['web_coupon'] == '1' && ($product['Product']['used_count'] == 0  || $usedcount ==0 )):?>
													<b><font color="red">PRINT LIMIT EXCEEDED</font></b>
												<?php endif; ?>
											</div>
										</td>
									</tr>
								</table>
							</div>
							<br/>
							<div class="rounded_corners_map map_border" style="padding:5px;background-color:#ffffff;">
								<?php $addressfinal=str_replace("+"," ",$addressfinal); ?>
								
								<?php	$mapOptions= array(
										'width'=>'',				//Width of the map
										'height'=>'280px',				//Height of the map
										'zoom'=>11,						//Zoom
										'type'=>'ROADMAP', 				//Type of map (ROADMAP, SATELLITE, HYBRID or TERRAIN)
										'latitude'=>$latitude,	//Default latitude if the browser doesn't support localization or you don't want localization
										'longitude'=>$longitude,	//Default longitude if the browser doesn't support localization or you don't want localization
										//'localize'=>true				//Boolean to localize your position or not
										
									); ?>
								<?php	echo $this->GoogleMapV3->map($mapOptions); ?>
								<?php  echo $this->GoogleMapV3->addMarker(array('latitude'=>$latitude,'longitude'=>$longitude,'localize'=>true,				//Boolean to localize your position or not
										'marker'=>true,					
										'infoWindow'=>true,				
										'windowText'=>$addressfinal)); ?>
							</div>
							<br/>
							<div class="rounded_corners_map map_border" style="padding:15px;background-color:#ffffff;">
								<div style="width:700px;text-align:right">
									<?php //echo $this->Html->link('Add Comments',array('onclick'=>"addcomments(".$product['Product']['id'].")"));?> 
			<a  onclick="addcomments(<?php echo $product['Product']['id']; ?>)"   style="cursor: pointer;" > Add Comments </a> 
		
								</div>
								
								<?php if(isset($comments)): ?>
									<?php if(!empty($comments)): ?>
										<div style="height:150px;overflow:auto;">
									   			<ul>
									   				<?php foreach($comments as $comment) :?>
										       		<li>
										       			<table style="border-bottom:1px dotted #868D8E;width:680px;">
															<tr>
											  					<td>
											     					<strong><?php echo $comment['Comment']['name']; ?></strong> 
											  					</td>
										       				</tr>
										       				<tr>
														   		<td style="padding-left:20px;">by
														     		<span style="color:green;">
														     			<i>
														     				<?php echo $users['User']['first_name']. ' '. $users['User']['last_name']; ?>
														     			</i>
														     		</span>
														   		</td>
										  					</tr>
										  					<tr>
											   					<td style="padding-left:20px;">
											    					<?php echo  $comment['Comment']['comments']; ?>
											   					</td>
										 					</tr>
									       				</table>
										       		</li>
										       		<?php endforeach; ?>
									       		</ul>
									       	
										</div>
									<?php else : ?>   
                                		<div style="color:blue">Be the first to comment on this Product</div>
									<?php endif;?>
								<?php endif;?>
							</div>
							<br/>
							
						</div>
						<div class="b rounded-corners_bottom">&nbsp;</div>
					</div>
					<?php endforeach; ?>
				</td>
				<td style="width:5px;">&nbsp;</td>
				<?php  if (isset($_SERVER['HTTP_USER_AGENT']) && (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 7.0') !== false)) : ?>
					<td valign="top" style="width:186px;">
				<?php else : ?>
					<td valign="top" style="width:180px;">
				<?php endif; ?>
					<div class="module">
						<div class="content" style="padding:13px 12px 0 ! important;">
							<div align="center">
								<?php foreach($bannerlist as $list):?>
									<?php 
									$filepath = SKYSCRAPER_IMAGE_PATH;
									$filename = $list['Skyscraper']['id']. '.'. $list['Skyscraper']['scraper_extension'];

									$path = substr($filepath,strpos($filepath,'webroot')+8);
									$path_prod = $path.$filename;

									$a=$this->ImageResize->getResizedDimensions($path_prod, 120, 600);?>
									
									<a href="<?php echo $list['Skyscraper']['link_url'];?>">
										<img width="<?php echo $a['width'];?>" height="<?php echo $a['height'];?>" src="<?php echo $this->Html->url('/'. $path_prod); ?>" alt="" />
									</a>
								<?php endforeach;?>
							</div>
						</div>
						<div class="b rounded-corners_bottom">&nbsp;</div>
					</div>
				</td>
				</tr>
				</table>


<!--string highlight script-->	
<script type="text/javascript">
 var searchTerm = '<?php echo $keyword; ?>'; 
 
        $('#table_list').removeHighlight(); 
     if (searchTerm) {
       $('#table_list').highlight(searchTerm);
     }
 </script>

<style type="text/css">
	.highlight_string {
	background-color: #FFFFCC
	}
</style>
<!--string highlight script end-->