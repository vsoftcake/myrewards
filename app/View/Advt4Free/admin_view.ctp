<style>
	#content
	{
		border-left:1px solid #cccccc;
		border-right:1px solid #cccccc;
		border-bottom:1px solid #cccccc;
		margin-left:20px !important;
		padding-left:15px !important;
		width:916px;
	}
</style>
<h2>View</h2>
<?php if($edit['Advt4Free']['merchant_approved'] == 'NO') : ?>
	<a href="/admin/advt_4_free/merchants_not_approved">Back</a><br/><br/>
<?php else : ?>
	<a href="/admin/advt_4_free">Back</a><br/><br/>
<?php endif;?>
<div style="width:50%">
	<table>
		<?php if($edit['Advt4Free']['merchant_approved'] == 'NO') : ?>
		<tr>
			<td style="width:170px"><strong>Merchant Created Date:</strong></td>
			<td><?php echo $edit['Advt4Free']['modified']; ?></td>
		</tr>
		<?php else: ?>
		<tr>
			<td style="width:170px"><strong>Merchant Approved Date : </strong></td>
			<td><?php echo $edit['Advt4Free']['merchant_modified']; ?></td>
		</tr>
		<tr>
			<td style="width:170px"><strong>Admin Approved Date : </strong></td>
			<td><?php echo $edit['Advt4Free']['admin_modified']; ?></td>
		</tr>
		<?php endif;?>
		<tr><td><br><br></td></tr>
	</table>
	<table width="100%">
		<tr>
			<td colspan="2"><strong>Supplier Contact Details - Postal Address</strong><br><br></td>
		</tr>
		<tr>
			<td width="150px">Company Name:</td>
			<td><?php echo $edit['Advt4Free']['company_name']; ?></td>
		</tr>
		<tr>
			<td>Contact Name:</td>
			<td><?php echo $edit['Advt4Free']['contact_name']; ?></td>
		</tr>
		<tr>
			<td>Position:</td>
			<td><?php echo $edit['Advt4Free']['position']; ?></td>
		</tr>
		<tr>
			<td>Postal Address:</td>
			<td><?php echo $edit['Advt4Free']['postal_address']; ?></td>
		</tr>
		<tr>
			<td>Suburb / City:</td>
			<td><?php echo $edit['Advt4Free']['suburb']; ?></td>
		</tr>
		<tr>
			<td>P/code:</td>
			<td><?php echo $edit['Advt4Free']['p_code']; ?></td>
		</tr>
		<tr>
			<td>State:</td>
			<td><?php echo $edit['Advt4Free']['state']; ?></td>
		</tr>
		<tr>
			<td>Country:</td>
			<td><?php echo $edit['Advt4Free']['country']; ?></td>
		</tr>
		<tr>
			<td>Phone:</td>
			<td><?php echo $edit['Advt4Free']['phone']; ?></td>
		</tr>
		<tr>
			<td>Mobile:</td>
			<td><?php echo $edit['Advt4Free']['mobile']; ?></td>
		</tr>
		<tr>
			<td>Email:</td>
			<td><?php echo $edit['Advt4Free']['email']; ?></td>
		</tr>
		<tr>
			<td>Web:</td>
			<td><?php echo $edit['Advt4Free']['web']; ?></td>
		</tr>
		<tr>
			<td colspan="2"><br><br><strong>Supplier Trading Address</strong><br><br></td>
		</tr>
		<tr>
			<td>Trading Name:</td>
			<td><?php echo $edit['Advt4Free']['trading_name']; ?></td>
		</tr>
		<tr>
			<td>Trading Address:</td>
			<td><?php echo $edit['Advt4Free']['trading_address']; ?></td>
		</tr>
		<tr>
			<td>Suburb:</td>
			<td><?php echo $edit['Advt4Free']['suburb2']; ?></td>
		</tr>
		<tr>
			<td>State:</td>
			<td><?php echo $edit['Advt4Free']['state2']; ?></td>
		</tr>
		
		<tr>
			<td>P/code:</td>
			<td><?php echo $edit['Advt4Free']['p_code2']; ?></td>
		</tr>
		<tr>
			<td>Trading Phone:</td>
			<td><?php echo $edit['Advt4Free']['trading_phone']; ?></td>
		</tr>
		<tr>
			<td>Email:</td>
			<td><?php echo $edit['Advt4Free']['email2']; ?></td>
		</tr>
		<tr>
			<td>Web:</td>
			<td><?php echo $edit['Advt4Free']['web2']; ?></td>
		</tr>
		<tr>
			<td><br><br><strong>Business Category</strong><br><br></td>
		</tr>
		<tr>
			<td>Category:</td>
			<td><?php echo $edit['Advt4Free']['category']; ?></td>
		</tr>
		<tr>
			<td valign="top">promote businesses:</td>
			<td><?php echo $edit['Advt4Free']['business_category']; ?></td>
		</tr>
		<tr>
			<td>Web:</td>
			<td><?php echo $edit['Advt4Free']['web3']; ?></td>
		</tr>
		<tr>
			<td>Promo code:</td>
			<td><?php echo $edit['Advt4Free']['promo_code']; ?></td>
		</tr>
		
		<tr>
			<td><br><br><strong>Promotional offer</strong><br><br></td>
		</tr>
		
		<tr>
			<td>Buy one get one free</td>
			<td><?php echo $edit['Advt4Free']['discount_offer']; ?></td>
		</tr>
		<tr>
			<td>Offer Details:</td>
			<td><?php echo $edit['Advt4Free']['offer_details']; ?></td>
		</tr>
		<tr>
			<td>% off</td>
			<td><?php echo $edit['Advt4Free']['discount_offer2']; ?></td>
		</tr>
		<tr>
			<td>% off</td>
			<td><?php echo $edit['Advt4Free']['discount_offer3']; ?></td>
		</tr>
		
		<tr>
			<td>Offer Details</td>
			<td><?php echo $edit['Advt4Free']['offer_details2']; ?></td>
		</tr>
		
		<tr>
			<td colspan="2"><br><br><strong>Promotional text about your business</strong><br><br></td>
		</tr>
		<tr>
			<td>Promotional text about business:</td>
			<td><?php echo $edit['Advt4Free']['about_business']; ?></td>
		</tr>
		<?php if(!empty($edit['Advt4Free']['logo_extension'])) : ?>
		<tr>
			<td>Logo</td>
			<td>
				<img src="/advt4free/logos/<?php echo $edit['Advt4Free']['company_name'].'.'.$edit['Advt4Free']['logo_extension'];?>"  height='200' width='200' alt="" />
			</td>
		</tr>
		<?php endif; ?>
		<tr>
			<td>Terms & Conditions:</td>
			<td><?php echo $edit['Advt4Free']['terms_conditions']; ?></td>
		</tr>
	</table>
	
	<table>
		<tr><td><br><br></td></tr>
		<?php if($edit['Advt4Free']['merchant_approved'] == 'NO') : ?>
		<tr>
		    <td style="width:100px"><?php echo $this->Html->link(__('Back', true), array('action'=>'index')); ?>&nbsp;&nbsp;</td>
		    <td style="width:100px"><a href="http://www.myrewards.com.au/advt4free/merchant_form.php?edit=<?php echo $edit['Advt4Free']['id'];?>" target="_blank">Edit</a>&nbsp;&nbsp;</td>
		    <td style="width:150px"><?php echo $this->Html->link(__('Merchant Approve', true), array('action'=>'merchant_approve', $edit['Advt4Free']['id'])); ?>&nbsp;&nbsp;</td>
		    <td style="width:100px"><?php echo $this->Html->link(__('Delete', true), array('action'=>'delete', $edit['Advt4Free']['id'])); ?>&nbsp;&nbsp;</td>
		</tr>
		<?php else: ?>
		<?php if($edit['Advt4Free']['approved'] == 0) : ?>
		<tr>
		    <td style="width:100px">
			<?php echo $this->Html->link(__('Back', true), array('action'=>'index')); ?>&nbsp;&nbsp;
		    </td>
		    <td style="width:100px">
			<?php echo $this->Html->link(__('Approve', true), array('action'=>'approve', $edit['Advt4Free']['id'])); ?>&nbsp;&nbsp;
		    </td>
		    <td style="width:100px" colspan="2">
		    	<?php echo $this->Html->link(__('Reject', true), array('action'=>'reject', $edit['Advt4Free']['id'])); ?>&nbsp;&nbsp;
		    </td>
		</tr>
		<?php endif;endif; ?>
		<tr><td><br><br></td></tr>
	</table>
	
</div>
