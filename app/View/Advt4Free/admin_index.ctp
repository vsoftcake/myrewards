<style>
	#content
	{
		border-left:1px solid #cccccc;
		border-right:1px solid #cccccc;
		border-bottom:1px solid #cccccc;
		margin-left:20px !important;
		padding-left:15px !important;
		width:916px;
	}
	.admin_button
	{
		height: 24px;
		padding-top: 2px;
		padding-bottom: 5px;
		border-radius: 5px 5px 5px 5px;
		background-color: #0066cc;
		border: 1px solid #cccccc;
		color: #FFFFFF; 
		cursor: pointer;
		font-size:13px;
	}
	.current
	{
	    font-family: 'Droid Sans',sans-serif;
	    font-size: 12px;
	   	padding:5px 8px !important;   
	    background-color: #0066CC;
	    border: 1px solid #DFDFDF;
	    color:#ffffff;
	}
	.cpagination1
	{
		/*padding:1px 2px 1px 2px;*/
		font-family: 'Droid Sans',sans-serif;
	    font-size: 12px;
		text-decoration:none !important;
		background-color: #FFFFFF;
		padding:3px 8px;
		border: 1px solid #0066CC;
		color:#808080 !important;
	}
</style>
<table style="margin-left:-2px;margin-top:10px;">
	<tr>
		<td>
            &nbsp;&nbsp;<img style="width:40px;height:40px;" src="/files/admin_home_icons/advt.png"  alt="Invite my family" title="Invite my family" />&nbsp;&nbsp;
        </td>
        <td> 
            <h2><?php echo "Advt 4 Free";?></h2>
        </td>
        <td width="400"></td>
		<td>
		<h3><?php echo $this->Html->link('Advt4Free Merchants Not Approved', array('action'=>'merchants_not_approved')); ?></h3>
        </td>
    </tr>        
</table>
<?php $this->Paginator->options(array('url' => array_merge($this->params['pass'], $this->params['named']))); ?>
<div id="advt4free">
	<table style="width: 920px; background-color: rgb(204, 204, 204); height: 65px; margin-top:1px; border: 1px solid rgb(150, 144, 144);">
		<tr>
			<td style="border: none ! important; padding-left: 10px;width:250px">
				<strong>Country</strong> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<?php echo $this->Form->input('Advt4Free.country',array('type'=>'select','label'=>false,'options'=>$countries,'empty'=>'Select Country','style'=>'width:150px'));
				$options = array('url' => 'update_states','update' => 'Advt4FreeState');
				echo $this->Ajax->observeField('Advt4FreeCountry', $options);
				?>
			</td>
			<td style="border: none ! important;padding-left:20px;width:250px">
				<strong>State</strong> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<?php echo $this->Form->input('Advt4Free.state',array('type'=>'select','label'=>false,'options'=>$states,'empty'=>'Select State','style'=>'width:180px'));?>
			</td>
			<td style="border: none ! important; padding-right: 15px;">
				<button type="submit" onclick="search_merchants()" class="admin_button"  style="margin-top:2px">Search</button>
			</td>
		</tr>
	</table>
	<div class="list" style="width:920px;padding-top:20px" >
		<?php if(!empty($advt4free)){?>
		<table cellpadding="0" cellspacing="0" style="width:920px">
			<tr height="30">
				<th style="width:125px;background-color:#cccccc;text-align:left;padding-left:5px;border-left:1px solid #969090;border-bottom:1px solid #969090;border-top:1px solid #969090;"><strong>Company <br/>Name</strong></th>
				<th style="width:125px;background-color:#cccccc;text-align:left;padding-left:1px;border-bottom:1px solid #969090;border-top:1px solid #969090;"><strong>Contact Name</strong></th>
				<th style="width:125px;background-color:#cccccc;text-align:left;padding-left:1px;border-bottom:1px solid #969090;border-top:1px solid #969090;"><strong>Country</strong></th>
				<th style="width:125px;background-color:#cccccc;text-align:left;padding-left:1px;border-bottom:1px solid #969090;border-top:1px solid #969090;"><strong>State</strong></th>
				<th style="width:125px;background-color:#cccccc;text-align:left;padding-left:1px;border-bottom:1px solid #969090;border-top:1px solid #969090;"><strong>Web</strong></th>
				<th style="width:125px;background-color:#cccccc;text-align:left;padding-left:1px;border-bottom:1px solid #969090;border-top:1px solid #969090;"><strong>Email</strong></th>
				<th class="actions" style="background-color:#cccccc;text-align:left;padding-left:4px;border-right:1px solid #969090;border-bottom:1px solid #969090;border-top:1px solid #969090;"><strong>Actions</strong></th>
			</tr>
			<?php 
			$i = 0;
			foreach($advt4free as $adv):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
			?>
	
			<tr<?php echo $class;?>>
				<td style="width:550px">
					<?php echo $adv['Advt4Free']['company_name']; ?>
				</td>
				<td style="width:550px">
					<?php echo $adv['Advt4Free']['contact_name'] ?>
				</td>
				<td style="width:300px">
					<?php echo $adv['Advt4Free']['country'] ?>
				</td>
				<td style="width:300px">
					<?php echo $adv['Advt4Free']['state'] ?>
				</td>
				<td style="width:300px">
					<?php echo $adv['Advt4Free']['web'] ?>
				</td>
				
				<td style="width:300px">
					<?php echo $adv['Advt4Free']['email'] ?>
				</td>
				
				<?php if($adv['Advt4Free']['approved']==0): ?>
				<td class="actions" style="width:1000px">
				    <?php echo $this->Html->link(__('View', true), array('action'=>'view', $adv['Advt4Free']['id'])); ?>&nbsp;&nbsp;
					<?php echo $this->Html->link(__('Approve', true), array('action'=>'approve', $adv['Advt4Free']['id'])); ?>&nbsp;&nbsp;
					<?php echo $this->Html->link(__('Reject', true), array('action'=>'reject', $adv['Advt4Free']['id'])); ?>&nbsp;&nbsp;
					<?php echo $this->Html->link(__('Delete', true), array('action'=>'delete', $adv['Advt4Free']['id']), null, sprintf(__('Are you sure you want to delete ?', true), $adv['Advt4Free']['company_name'])); ?>
				</td>
				<?php endif; ?>
				
				<?php if($adv['Advt4Free']['approved']==1): ?>
				<td class="actions" style="width:1000px">
					<?php echo $this->Html->link(__('View', true), array('action'=>'view', $adv['Advt4Free']['id'])); ?>&nbsp;&nbsp;
					<?php echo "<font color='red'><strong>APPROVED</strong></font>"; ?>
				</td>
				<?php endif; ?>
				
				<?php if($adv['Advt4Free']['approved']==2): ?>
				<td class="actions" style="width:1000px">
					<?php echo $this->Html->link(__('View', true), array('action'=>'view', $adv['Advt4Free']['id'])); ?>&nbsp;&nbsp;
					<?php echo "<font color='red'><strong>REJECTED</strong></font>"; ?>
					&nbsp;&nbsp;&nbsp;&nbsp;
					<?php echo $this->Html->link(__('Delete', true), array('action'=>'delete', $adv['Advt4Free']['id']), null, sprintf(__('Are you sure you want to delete ?', true), $adv['Advt4Free']['company_name'])); ?>
				</td>
				<?php endif; ?>
			</tr>
		<?php endforeach; ?>
		</table>
		<?php }else{?>
			<div><h3>Merchants Not Available</h3></div>
		<?php }?>
	</div>
	<?php if(!empty($advt4free)) : ?>
		<div class="paging" style="width:920px">
			<table style="border:none !important;padding-top:15px;float:right;">
				<tr>
					<td style="border:none !important;" valign="top">
						<?php echo $this->Paginator->first('<<', array('title'=>'first','class'=>'disabled cpagination1'));?>
						<?php echo $this->Paginator->prev('<', array('title'=>'prev','class'=>'disabled cpagination1'));?>
						<?php echo $this->Paginator->numbers(array('separator' => ' ','class'=>'cpagination1'));?>
						<?php echo $this->Paginator->next('>', array('title'=>'next','class'=>'disabled cpagination1'));?>
						<?php echo $this->Paginator->last('>>', array('title'=>'last','class'=>'disabled cpagination1'));?>
					</td>
					<td style="border:none !important;padding-left:5px;padding-top:1px;">
					<select name="page" id="selectpage" style="margin-top:-5px;">
						<?php for ($i = 1; $i <= $this->Paginator->counter(array('format' => '%pages%')); $i++):?>
						<option value="<?php echo $i;?>"<?php echo ($this->Paginator->current($params['page']) == $i) ? ' selected="selected"' : ''?>><?php echo $i ?></option>
						<?php endfor ?>
					</select>
					<script>
					$("#selectpage").change(function() {
						var url = window.location.href;
						var n = url.indexOf("/?");
						var extra = "";
						if(n>0){extra = url.substring(n,url.length);}
						window.location.href = '/admin/advt_4_free/index/page:'+$(this).val()+extra;
					});
					</script>
					</td>
				</tr>
			</table>
		</div>
	<?php endif;?>
</div>
<script>
	function search_merchants()
	{
		country = document.getElementById("Advt4FreeCountry").value;	
		state = document.getElementById("Advt4FreeState").value;
		$.ajax({
			url: '/admin/advt_4_free/search/ctry='+country+'/st='+state,
			type: "GET",
			success: function (data) {
				$('#advt4free').html(data);
			}
		});
	}
</script>