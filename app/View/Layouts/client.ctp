<?php header('Content-Type: text/html; charset=UTF-8'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>
		<?php echo $title_for_layout;?>
	</title>

	<?php echo $html->charset();?>

	<link rel="icon" href="<?php echo $this->webroot;?>favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="<?php echo $this->webroot;?>favicon.ico" type="image/x-icon" />
	<?php echo $html->css('default');?>
	<?php echo $html->css('admin');?>
	<?php echo $scripts_for_layout;?>
	
	<?php echo $javascript->link('prototype'); ?>
	<?php echo $javascript->link('form'); ?>
	
</head>
<body>
	<div id="container">
		<div id="header">
			<table>
				<tr>
					<td><a href="<?php echo  $html->url('/client/'); ?>"><h1>Administration</h1></a></td>
					<td id="header_right">
						<?php if  ($session->check('user')) : ?>
							<b>Welcome back <?php echo $session->read('user.User.first_name'). ' '. $session->read('user.User.last_name'); ?></b><br/>
							<a href="<?php echo $html->url('/'); ?>">Public Website</a> | <a href="<?php echo $html->url('/users/logout/'); ?>">Sign Out</a><br/><br/>
						<?php endif; ?>
					</td>
				</tr>
			</table>
			<a href="<?php echo $html->url('/client/merchants/'); ?>">Merchants</a> |
			<a href="<?php echo $html->url('/client/users/'); ?>">Users</a> |
			<a href="<?php echo $html->url('/client/reports/'); ?>">Reports</a>
		</div>
		<div id="message">
		<?php
				if ($session->check('Message.flash')):
						$session->flash();
				endif;
			?>
		</div>
		<div id="content">
			<?php echo $content_for_layout;?>
		</div>
		<div id="footer">
		</div>
	</div>
	<?php echo $cakeDebug?>
</body>
</html>
