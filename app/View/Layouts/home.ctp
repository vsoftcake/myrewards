<?php
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>
		<?php echo $title_for_layout;?>
	</title>
	<meta name="verify-v1" content="w3v5+W56MejdqfKVtUIBwEhNYcBYU+o72589lETT4rM=" />
	<meta name="description" content="<?php echo htmlentities($page['ClientPage']['meta_description']); ?>" />
	<meta name="keywords" content="<?php echo htmlentities($page['ClientPage']['meta_keywords']); ?>" />
	<meta name="robots" content="<?php echo htmlentities($page['ClientPage']['meta_robots']); ?>" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<?php echo $html->charset();?>
	
	<link rel="icon" href="<?php echo $this->webroot;?>favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="<?php echo $this->webroot;?>favicon.ico" type="image/x-icon" />
	<?php echo $html->css('default');?>
	<?php echo $html->css('/popup/css/default_theme'); ?>
	<?php echo $html->css('carousel');?>
	<?php echo $html->css('modalbox');?>
	<?php echo $html->css('auto_complete');?>
	<?php echo $html->css('tooltip');?>
	<link rel="stylesheet" type="text/css" href="<?php echo $html->url('/styles/sheet/'. $session->read('dashboard_style_id')); ?>" />
	<link href="http://fonts.googleapis.com/css?family=Droid+Sans" rel="stylesheet" type="text/css">
	<?php echo $scripts_for_layout;?>
	
	<?php echo $javascript->link('prototype'); ?>
	
	<?php echo $javascript->link('scriptaculous'); ?>
	<?php echo $javascript->link('effects'); ?>
	<?php echo $javascript->link('carousel'); ?>
	<?php echo $javascript->link('modalbox'); ?>
	<?php echo $javascript->link('form'); ?>
	<?php echo $javascript->link('tooltip'); ?>
	<?php echo $javascript->link('mouseover'); ?>
	
	<script>
	function start() {
		var str = <?php echo $session->read('user.User.id');?>;
		if (window.XMLHttpRequest)
		{// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp=new XMLHttpRequest();
		}
		else
		{// code for IE6, IE5
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}

		xmlhttp.onreadystatechange=function()
		{
			if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				if(xmlhttp.responseText != '')
					Modalbox.show(xmlhttp.responseText, {title: '', width: 650, height: 400}); return false;
			}
		}
		xmlhttp.open("GET","/campaigns/checkwinner/"+str,true);
		xmlhttp.send();
	}
	onload = start;
</script>

</head>
<?php if(($session->read('client.Client.client_background_images_left_extension')=='') && ($session->read('client.Client.client_background_images_right_extension')=='')) : ?>
<body class="body_background">
<?php else: ?>
<body>
<?php endif;?>

<?php
foreach($session->read('client.ClientBanner') as $key => $value) {
if($session->read('dashboardidinsession')==$value['dashboard_id'])
{
	if($session->read('dashboardidinsession')==1)
	{
		$left_ext = $value['client_background_images_left_extension'];
		$right_ext = $value['client_background_images_right_extension'];
		$left_link = $value['left_link'];
		$right_link = $value['right_link'];
		$pext = "";
		$filepath_left = CLIENT_BACKGROUND_IMAGE_LEFT_PATH;
		$filepath_right = CLIENT_BACKGROUND_IMAGE_RIGHT_PATH;
	}
	if($session->read('dashboardidinsession')==2)
	{
		$left_ext = $value['client_background_images_left_extension'];
		$right_ext = $value['client_background_images_right_extension'];
		$left_link = $value['left_link'];
		$right_link = $value['right_link'];
		$pext = "_2";
		$filepath_left = CLIENT_BACKGROUND_IMAGE_LEFT_2_PATH;
		$filepath_right = CLIENT_BACKGROUND_IMAGE_RIGHT_2_PATH;
	}
	if($session->read('dashboardidinsession')==3)
	{
		$left_ext = $value['client_background_images_left_extension'];
		$right_ext = $value['client_background_images_right_extension'];
		$left_link = $value['left_link'];
		$right_link = $value['right_link'];
		$pext = "_3";
		$filepath_left = CLIENT_BACKGROUND_IMAGE_LEFT_3_PATH;
		$filepath_right = CLIENT_BACKGROUND_IMAGE_RIGHT_3_PATH;
	}

?>

	<div style=" width:1348px;margin:0 auto; height:auto">
		<div id="popups" style="position:relative;z-index: 1004;padding:0px;height:0px;"></div>
	
		<?php echo $this->element('header_top'); ?>
		
		<?php  if (isset($_SERVER['HTTP_USER_AGENT']) && (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 7.0') == false)) : ?>
		
			<script language="javascript">
				Event.observe(window, 'scroll', function(){
				var height = 50;
				var scroll = (document.all ? document.documentElement.scrollTop : window.pageYOffset);
				if ( scroll > height) 
				{
					 $('leftimage').setStyle({
					  top: '0px'
					  }); 
				} 
				else if ( scroll < height) 
				{
					$('leftimage').setStyle({
					top: '50px'
					});
				}
				});
			</script>


			<?php 
				if($left_ext!=''): 
				$filename = $session->read('client.Client.id'). '.'. $left_ext;
				if (file_exists($filepath_left. $filename)) :			
			?>
				<div id="leftimage" style="position: fixed; top: 50px; float:left;width:170px;" >
					<?php 
						$lurl = '';
						if (false === strpos($left_link, '://')) 
						{
							$lurl = 'http://'.$left_link;
						}
						else
						{
							$lurl = $left_link;
						}
					?>
					<?php if($left_link!='') : ?>
						<a href="<?php echo $lurl;?>" target="_blank">
					<?php endif;?>
						<img width="171px" src="/files/client_background_images_left<?php echo $pext;?>/<?php echo $session->read('client.Client.id').'.'.$left_ext; ?>" />
					<?php if($left_link!='') : ?>
						</a>
					<?php endif;?>
				</div>
				<?php endif;?>
			<?php endif;?>
			
			
			
			<!-- right image -->
						
			<script language="javascript">
			Event.observe(window, 'scroll', function() {
			 var height = 50;
			 var scroll = (document.all ? document.documentElement.scrollTop : window.pageYOffset);
			 if ( scroll > height) 
			{
				 $('rightimage').setStyle({
				  top: '0px'
				  }); 
			} 
			else if ( scroll < height) {

				$('rightimage').setStyle({
				  top: '50px'
				  });
			}

			});
			</script>

			<?php 
				if($right_ext!=''): 
				$filename = $session->read('client.Client.id'). '.'. $right_ext;
				if (file_exists($filepath_right. $filename)) :
			?>
				<div id="rightimage" style="position: fixed; top: 50px;width:170px;margin-left:1177px;" >
					<?php 
						$rurl = '';
						if (false === strpos($right_link, '://')) 
						{
						    $rurl = 'http://'.$right_link;
						}
						else
						{
							$rurl = $right_link;
						}
					?>
					<?php if($right_link!='') : ?>
						<a href="<?php echo $rurl;?>" target="_blank">
					<?php endif;?>
						<img width="171px" src="/files/client_background_images_right<?php echo $pext;?>/<?php echo $session->read('client.Client.id').'.'.$right_ext; ?>" />

					<?php if($right_link!='') : ?>
						</a>
					<?php endif;?>
				</div>
				<?php endif;?>
			<?php endif;?>
		
		<?php endif;?>
		
		<div style="position:relative;z-index:1003;margin-left: 171px;margin-right: 171px;">
			<div id="header">
				<?php echo $this->element('header'); ?>
			</div>
			<div id="container">
				<div id="home_content">
					<?php
						if ($session->check('Message.flash')):
								$session->flash();
						endif;
					?>
					<?php echo $content_for_layout;?>
				</div>
			</div>
			<div id="footer">
				<!--<?php if($search_allowed){ ?><a href="<?php echo $html->url('/products/search/'); ?>">Search</a><?php $start++; } ?>
				<?php if($news_allowed){ if($start >0) echo " | "; $start ++; ?><a href="<?php echo $html->url('/display/news/'); ?>">News</a><?php } ?>
				<?php if($special_allowed){  if($start >0) echo " | "; $start ++; ?><a href="<?php echo $html->url('/display/special_offers/'); ?>">Special Offers</a><?php } ?>
				<?php if($customer_allowed){  if($start >0) echo " | "; $start ++; ?><a href="<?php echo $html->url('/display/customer_service/'); ?>">Customer Service</a><?php } ?>
				<?php if($supplier_allowed){  if($start >0) echo " | "; $start ++; ?><a href="<?php echo $html->url('/display/supplier/'); ?>">Supplier</a><?php } ?>
				-->
				<!--<div style="float: left; width: 400px; padding-left: 30px;">
					<div style="float:left;width:100px">
						<a class="footer_links" href="/">DASHBOARD</a><br/>
						<a class="footer_links" href="/display/whats_new">WHAT'S NEW</a>
					</div>
					<div style="float:left;width:120px">
						<a class="footer_links" href="/display/how_it_works">HOW IT WORKS</a><br/>
						<a class="footer_links" href="/display/my_membership">MY MEMBERSHIP</a>
						</div>
					<div style="float:left;width:120px;padding-left:10px">	
						<a class="footer_links" href="/display/customer_service">CUSTOMER SERVICE</a><br/>
						<a class="footer_links" href="/display/savings_calculator">SAVINGS CALCULATOR</a>
					</div>
				</div>
				-->
				<div style="padding-left:20px;height:120px;">
					<table width="986px">
						<tr>
							<td valign="top" >
								<?php if($session->check('user')) : ?>
									<ul id="menuf">
										<li>
										<a style="text-transform:uppercase;" href="<?php if($session->read('dashboardidinsession')==1){echo $html->url('/myrewards');}if($session->read('dashboardidinsession')==2){echo $html->url('/send_a_friend');}if($session->read('dashboardidinsession')==3){echo $html->url('/mypoints');}?>">
										<?php if(($session->read('client.Client.program_id') == 21) || ($session->read('client.Client.program_id') == 7)) : ?>
												Home
											<?php else : ?>
												<b>Dashboard</b>
											<?php endif; ?>		
										</a>
										</li>
									</ul>
								<?php endif;?>
								<?php
									if($session->read('dashboardidinsession')==1)
									{
										echo $tree->show_footer_menu($session->read('client1'),1);
									}
									if($session->read('dashboardidinsession')==2)
									{
										echo $tree->show_footer_menu($session->read('client2'),2);
									}
									if($session->read('dashboardidinsession')==3)
									{
										echo $tree->show_footer_menu($session->read('client3'),3);
									}  
								 ?>
							</td>
							<td style="text-align:center;width:230px;padding-top: 35px;">
								<?php
									$filename = $session->read('client.Client.id'). '.'. $session->read('client.Client.client_logo_extension');
									$filepath = 'files/client_logo/';
									if (!file_exists('files/client_logo/'. $filename)) 
									{
										$filename = 'blank.png';
										$filepath = '/';
									}

									$path_prod = $filepath.$filename;

									$a=$imageResize->getResizedDimensions($path_prod, 200, 120);?>

									<img width="<?php echo $a['width'];?>" height="<?php echo $a['height'];?>" src="<?php echo $html->url('/'. $path_prod);?>" alt="" />
							</td>
						</tr>
					</table>
				</div>
				<br/>
				<div style="padding-left:30px;padding-bottom:8px;">
					<?php if($session->read('client.Client.copyright_disabled')==0): ?> 
						<?php if($session->read('client.Client.copyright_text')!=''): ?>
							<?php echo $session->read('client.Client.copyright_text'); ?> <br/>
						<?php else : ?>
							&copy; My Rewards International  Ltd 2013 All Rights Reserved. <br/>
						<?php endif; ?> 
					<?php endif; ?>
					
					<?php //if($session->read('client.Client.program_id')!='21') : ?>
				 	<?php if($session->read('client.Client.facebook_disabled')==0 || $session->read('client.Client.twitter_disabled')==0): ?> 
						Follow us on &nbsp;

						<?php if($session->read('client.Client.facebook_disabled')==0) : ?>
							<?php 
								$facebook_link = ''; 
								if($session->read('client.Client.facebook_link')!= '') :
									$facebook_link = $session->read('client.Client.facebook_link');
								else :
									$facebook_link = "http://www.facebook.com/pages/My-Rewards-International/133544976702355?v=wall";
								endif;
							?>

							<a style="text-decoration:none;border:none !important;" target="_blank" href="<?php echo $facebook_link; ?>">
								<img style="vertical-align:middle" width="20px" src="/files/social_icons/fb.png" title="facebook"/>
							</a>
							&nbsp;&nbsp;
						<?php endif; ?>
						
						<?php if($session->read('client.Client.twitter_disabled')==0) : ?>
							<?php 
								$twitter_link='';
								if($session->read('client.Client.twitter_link')!= '') :
									$twitter_link = $session->read('client.Client.twitter_link');
								else :
									$twitter_link = "http://twitter.com/#!/MyRewardsIntl";
								endif;
							?>
							<a style="text-decoration:none;border:none !important;" target="_blank" href="<?php echo $twitter_link;?>">
								<img style="vertical-align:middle" width="20px" src="/files/social_icons/twitter.png" title="twitter"/>
							</a>
						<?php endif; ?>
					<?php endif; ?>
				</div>
			</div>
		</div>
	<?php echo $cakeDebug?>
<script type="text/javascript">
	var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
	document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
	try {
	var pageTracker = _gat._getTracker("UA-64808-8");
	pageTracker._trackPageview();
	} catch(err) {}
</script>

<!-- Piwik Analytics Tracking --> 
<script type="text/javascript">
var pkBaseURL = (("https:" == document.location.protocol) ? "https://wstats.infozeal.net/" : "http://wstats.infozeal.net/");
document.write(unescape("%3Cscript src='" + pkBaseURL + "piwik.js' type='text/javascript'%3E%3C/script%3E"));
</script><script type="text/javascript">
try {
var piwikTracker = Piwik.getTracker(pkBaseURL + "piwik.php", 3);
piwikTracker.setDocumentTitle(document.domain + "/" + document.title);
piwikTracker.trackPageView();
piwikTracker.enableLinkTracking();
} catch( err ) {}
</script><noscript><p><img src="http://wstats.infozeal.net/piwik.php?idsite=3" style="border:0" alt="" /></p></noscript>
<!-- End Piwik Analytics Tracking Code -->

	<?php  if (isset($_SERVER['HTTP_USER_AGENT']) && (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 7.0') == false)) : ?>
		
	<?php endif; ?>
	</div>
	<?php }}?>
</body>
</html>
