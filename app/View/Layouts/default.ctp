<?php
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
?>
<html>
<head>
	<title><?php echo $title_for_layout;?></title>
	<meta name="description" content="<?php echo htmlentities($page['ClientPage']['meta_description']); ?>" />
	<meta name="keywords" content="<?php echo htmlentities($page['ClientPage']['meta_keywords']); ?>" />
	<meta name="robots" content="<?php echo htmlentities($page['ClientPage']['meta_robots']); ?>" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<?php echo $this->Html->charset();?>
	<link rel="icon" href="<?php echo $this->webroot;?>favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="<?php echo $this->webroot;?>favicon.ico" type="image/x-icon" />
	
	<?php echo $this->Html->css('default');?>
	<?php $styleId = $this->Session->read('dashboard_style_id')==''?$this->Session->read('client.Client.style_id'):$this->Session->read('dashboard_style_id');?>
	<link rel="stylesheet" type="text/css" href="<?php echo $this->Html->url('/styles/sheet/'. $styleId); ?>" />
	<link href="http://fonts.googleapis.com/css?family=Droid+Sans" rel="stylesheet" type="text/css">
	<?php 
	echo $this->Html->css('print', null, array('media' => 'print'));
	echo $this->Html->css('style');
	echo $this->Html->css('style1');
	echo $this->Html->css('campaign');
	echo $this->Html->css('jquery-ui-1.10.4.custom.min');
	echo $this->Html->script('jquery-1.10.2');  
	echo $this->Html->script('jquery-ui-1.10.4.custom.min');
	echo $this->Html->script('highlight_string');  
	echo $this->Html->script('default');
	echo $this->Html->script('domnews');
	
	echo $this->fetch('css');
	echo $this->fetch('script');?>
	
	<script type="text/javascript">
	function load()
	{
		if(!document.getElementById || !document.createTextNode){return;}
		initDOMnews();
		if(document.getElementById("back")!=null)
		{
			if (history.length>1) 
			{
			   document.getElementById("back").style.display="block";
			}
			else
			{
				 document.getElementById("back").style.display="none";
			}
		}
	}
	</script>
</head>
<?php if(($this->Session->read('client.Client.client_background_images_left_extension')=='') && ($this->Session->read('client.Client.client_background_images_right_extension')=='')) : ?>
<body class="body_background" onload="load()">
<?php else: ?>
<body onload="load()">
<?php endif;?>

<?php
foreach($this->Session->read('client.ClientBanner') as $key => $value) {
if($this->Session->read('dashboardidinsession')==$value['dashboard_id'])
{
	if($this->Session->read('dashboardidinsession')==1)
	{
		$left_ext = $value['client_background_images_left_extension'];
		$right_ext = $value['client_background_images_right_extension'];
		$left_link = $value['left_link'];
		$right_link = $value['right_link'];
		$pext = "";
		$filepath_left = CLIENT_BACKGROUND_IMAGE_LEFT_PATH;
		$filepath_right = CLIENT_BACKGROUND_IMAGE_RIGHT_PATH;
	}
	if($this->Session->read('dashboardidinsession')==2)
	{
		$left_ext = $value['client_background_images_left_extension'];
		$right_ext = $value['client_background_images_right_extension'];
		$left_link = $value['left_link'];
		$right_link = $value['right_link'];
		$pext = "_2";
		$filepath_left = CLIENT_BACKGROUND_IMAGE_LEFT_2_PATH;
		$filepath_right = CLIENT_BACKGROUND_IMAGE_RIGHT_2_PATH;
	}
	if($this->Session->read('dashboardidinsession')==3)
	{
		$left_ext = $value['client_background_images_left_extension'];
		$right_ext = $value['client_background_images_right_extension'];
		$left_link = $value['left_link'];
		$right_link = $value['right_link'];
		$pext = "_3";
		$filepath_left = CLIENT_BACKGROUND_IMAGE_LEFT_3_PATH;
		$filepath_right = CLIENT_BACKGROUND_IMAGE_RIGHT_3_PATH;
	}
?>

<div style=" width:1348px;margin:0 auto; height:auto">
	<div id="popups" style="position:relative;z-index: 1004;padding:0px;height:0px;"></div>
	<?php echo $this->element('header_top'); ?>
	<?php  if (isset($_SERVER['HTTP_USER_AGENT']) && (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 7.0') == false)) : ?>
		<script>
		$(window).scroll(function(){
			var height = 50;
			var scroll = (document.all ? document.documentElement.scrollTop : window.pageYOffset);
			if ( scroll > height) {
				$('#leftimage').css( "top", "0px" );
			} 
			else if ( scroll < height) {
				$('#leftimage').css( "top", "50px" );
			}
		});
		</script>
	
		<?php 
		if($left_ext!=''): 
			$filename = $this->Session->read('client.Client.id'). '.'. $left_ext;
		if (file_exists($filepath_left. $filename)) :
		?>
		<div class="no_print" id="leftimage" style="position: fixed; top: 50px; float:left;width:170px;" >
			<?php 
				$lurl = '';
				if (false === strpos($left_link, '://')) 
				{
					$lurl = 'http://'.$left_link;
				}
				else
				{
					$lurl = $left_link;
				}
			?>
			<?php if($left_link!='') : ?>
				<a href="<?php echo $lurl;?>" target="_blank">
			<?php endif;?>
				<img width="171px" src="/files/client_background_images_left<?php echo $pext;?>/<?php echo $this->Session->read('client.Client.id').'.'.$left_ext; ?>"/>
			<?php if($left_link!='') : ?>
				</a>
			<?php endif;?>
		</div>
		<?php endif;?>
		<?php endif;?>
	
		<script>
		$(window).scroll(function(){
			var height = 50;
			var scroll = (document.all ? document.documentElement.scrollTop : window.pageYOffset);
			if ( scroll > height) {
				$('#rightimage').css( "top", "0px" );
			} 
			else if ( scroll < height) {
				$('#rightimage').css( "top", "50px" );
			}
		});
		</script>
		<?php 
		if($right_ext!=''): 
			$filename = $this->Session->read('client.Client.id'). '.'. $right_ext;
		if (file_exists($filepath_right. $filename)) :
		?>
		<div class="no_print" id="rightimage" style="position:fixed;top:50px; margin-left: 1177px;z-index:1002;width:170px;" >
			<?php 
				$rurl = '';
				if (false === strpos($right_link, '://')) 
				{
				    $rurl = 'http://'.$right_link;
				}
				else
				{
					$rurl = $right_link;
				}
			?>
			<?php if($right_link!='') : ?>
				<a href="<?php echo $rurl;?>" target="_blank">
			<?php endif;?>
				<img width="171px" src="/files/client_background_images_right<?php echo $pext;?>/<?php echo $this->Session->read('client.Client.id').'.'.$right_ext; ?>"/>
			<?php if($right_link!='') : ?>
				</a>
			<?php endif;?>
		</div>
		<?php endif;?>
		<?php endif;?>
	<?php endif;?>
	
	<div style="z-index:1003;margin-left: 171px;margin-right: 171px;">
		<div id="header">
			<?php echo $this->element('header'); ?>
		</div>
		<div id="container">
			<div id="content">
				<?php echo $this->fetch('content'); ?>
			</div>
		</div>
		<div id="footer">
			<div style="padding-left:20px;height:120px;">
				<table style="width:986px">
					<tr>
						<td valign="top" >
							<?php if($this->Session->check('user')) : ?>
							<ul id="menuf">
								<li>
								<a style="text-transform:uppercase;" href="<?php echo $this->Html->url('/'); ?>">
									<?php if(($this->Session->read('client.Client.program_id') == 21) || ($this->Session->read('client.Client.program_id') == 7)) : ?>
										<b>Home</b>
									<?php else : ?>
										<b>Dashboard</b>
									<?php endif; ?>
								</a>
								</li>
							</ul>
							<?php endif;?>
							<?php 
							if($this->Session->read('dashboardidinsession')==1)
							{
								echo $this->Tree->show_footer_menu($this->Session->read('client1'),1);
							}
							if($this->Session->read('dashboardidinsession')==2)
							{
								echo $this->Tree->show_footer_menu($this->Session->read('client2'),2);
							}
							if($this->Session->read('dashboardidinsession')==3)
							{
								echo $this->Tree->show_footer_menu($this->Session->read('client3'),3);
							}
							?>
						</td>
						<td style="text-align:center;width:230px;padding-top:35px;">
							<?php
							$filename = $this->Session->read('client.Client.id'). '.'. $this->Session->read('client.Client.client_logo_extension');
							$filepath = 'files/client_logo/';
							if (!file_exists('files/client_logo/'. $filename)) 
							{
								$filename = 'blank.png';
								$filepath = '/';
							}

							$path_prod = $filepath.$filename;

							$a=$this->ImageResize->getResizedDimensions($path_prod, 200, 120);?>

							<img width="<?php echo $a['width'];?>" height="<?php echo $a['height'];?>" src="<?php echo $this->Html->url('/'. $path_prod);?>" alt="" />
						</td>
					</tr>
				</table>
			</div>
			<br/>
			<div style="padding-left:30px;padding-bottom:8px;">
				<?php if($this->Session->read('client.Client.copyright_disabled')==0): ?> 
					<?php if($this->Session->read('client.Client.copyright_text')!=''): ?>
						<?php echo $this->Session->read('client.Client.copyright_text'); ?> <br/>
					<?php else : ?>
						&copy; My Rewards International  Ltd 2013 All Rights Reserved. <br/>
					<?php endif; ?> 
				<?php endif; ?>

				<?php if($this->Session->read('client.Client.facebook_disabled')==0 || $this->Session->read('client.Client.twitter_disabled')==0): ?> 
					Follow us on &nbsp;

					<?php if($this->Session->read('client.Client.facebook_disabled')==0) : ?>
						<?php 
							$facebook_link = ''; 
							if($this->Session->read('client.Client.facebook_link')!= '') :
								$facebook_link = $this->Session->read('client.Client.facebook_link');
							else :
								$facebook_link = "http://www.facebook.com/pages/My-Rewards-International/133544976702355?v=wall";
							endif;
						?>

						<a style="text-decoration:none;border:none !important;" target="_blank" href="<?php echo $facebook_link; ?>">
							<img style="vertical-align:middle" width="20px" src="/files/social_icons/fb.png" title="facebook"/>
						</a>
						&nbsp;&nbsp;
					<?php endif; ?>

					<?php if($this->Session->read('client.Client.twitter_disabled')==0) : ?>
						<?php 
							$twitter_link='';
							if($this->Session->read('client.Client.twitter_link')!= '') :
								$twitter_link = $this->Session->read('client.Client.twitter_link');
							else :
								$twitter_link = "http://twitter.com/#!/MyRewardsIntl";
							endif;
						?>
						<a style="text-decoration:none;border:none !important;" target="_blank" href="<?php echo $twitter_link;?>">
							<img style="vertical-align:middle" width="20px" src="/files/social_icons/twitter.png" title="twitter"/>
						</a>
					<?php endif; ?>
				<?php endif; ?>
			</div>
		</div>
	</div>
	<div id="modalboxpopup"></div>
	<div style="position:absolute"><?php echo $this->element('sql_dump'); ?></div>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
try {
	var pageTracker = _gat._getTracker("UA-64808-8");
	pageTracker._trackPageview();
} catch(err) {}
</script>
</div>	
<?php } } ?>
</body>
</html>