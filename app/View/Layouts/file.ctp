<?php

// send the right headers
$extension = substr(strrchr($filename, '.'), 1);
switch ($extension) {
	case "pdf": $ctype="application/pdf";
	break;
	case "zip": $ctype="application/zip";
	break;
	case "doc": $ctype="application/msword";
	break;
	case "xls": $ctype="application/vnd.ms-excel";
	break;
	
	case "ppt": $ctype="application/vnd.ms-powerpoint";
	break;
	case "gif": $ctype="image/gif";
	break;
	case "png": $ctype="image/png";
	break;
	case "image/pic": $ctype="image/pict";
	break;
	case "jpe": case "jpeg":case "jpg": $ctype="image/jpg";
	break;
	case "htm": case "html": $ctype="text/html"; 
	break;
	case "swf": $ctype="application/x-shockwave-flash";
	break;
	case "xml": $ctype="application/xml";
	break;
	case "psd": $ctype="application/photoshop";
	break;
	case "txt": $ctype="text/plain";
	break;
	case "mov": $ctype="video/quicktime";
	break;
	case "mpg": $ctype="video/mpeg";
	break;
	case "avi": $ctype="video/avi";
	break;
	case "divx": $ctype="video/divx";
	break;
	case "aif": $ctype="audio/aiff";
	break;
	case "mp3": $ctype="audio/mpeg3";
	break;
	case "wav": $ctype="audio/wav";
	break;
	
	default: $ctype="application/force-download";
}

if (is_file($filepath. $filename)) {
	header('Content-Type: '. $ctype);
	header('Content-Length: '.filesize($filepath. $filename));
	header('Content-Disposition: inline; filename="'. $filename. '"'. "\r\n");

	//	send the file
	readfile($filepath. $filename);
} else  {
	header("HTTP/1.0 404 Not Found");
	?>
	<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">
	<html><head>
	<title>404 Not Found</title>
	</head><body>
	<h1>Not Found</h1>
	<p>The requested URL was not found on this server.</p>
	<?php echo $filepath. $filename; ?>
	</body></html>
	<?php
} ?>