<?php header('Content-Type: text/html; charset=UTF-8'); ?>
<html>
<head>
	<title><?php echo $title_for_layout;?></title>

	<?php echo $this->Html->charset();?>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<link rel="icon" href="<?php echo $this->webroot;?>favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="<?php echo $this->webroot;?>favicon.ico" type="image/x-icon" />
	<link href="http://fonts.googleapis.com/css?family=Droid+Sans" rel="stylesheet" type="text/css">
	<?php 
	echo $this->Html->css('default');
	echo $this->Html->css('admin');
	echo $this->Html->css('tabber');
	echo $this->Html->css('campaign');
	echo $this->Html->css('jquery-ui-1.10.4.custom.min');
	
	echo $this->Html->script('jquery-1.10.2');
	echo $this->Html->script('jquery-ui-1.10.4.custom.min');
	echo $this->Html->script('default');
	echo $this->Html->script('highlight');
	echo $this->Html->script('tabber');
	echo $this->Html->script('tiny_mce/tiny_mce');
	
	echo $this->fetch('css');
	echo $this->fetch('script');?>
	
	<?php  ?>
	<style>
	.rounded-corners 
	{
		-moz-border-radius: 10px 10px 0px 0px; /* Firefox */
		-webkit-border-radius: 10px 10px 0px 0px; /* Safari, Chrome */
		border-radius: 10px 10px 0px 0px; /* CSS3 */
	}
	#main_menu {  float: left;background:none repeat scroll 0 0 #565454;line-height: 8px; padding: 10px 2px 5px 6px; width: 947px; }
	#main_menu li 
	{
		float:left;
		padding-top:5px;
		padding-bottom:5px;
		padding-right:1px;
	}
	#main_menu li a 
	{
		color:#fff;
		font-size:12px;
		padding:0px;
		text-decoration:none !important; 
	}
	#main_menu li a:hover
	{
		background-color:#0066cc;
		padding-top:5px;
		padding-bottom:6px;
		-webkit-user-select: none;
		-moz-user-select: none;
		user-select: none;
		-moz-border-radius-topleft: 4px;
		-moz-border-radius-topright: 4px;
		-moz-border-radius-bottomright: 0px;
		-moz-border-radius-bottomleft: 0px;
		border-top-left-radius:4px;
		border-top-right-radius: 4px;
		border-bottom-right-radius: 0px;
		border-bottom-left-radius: 0px; 
		line-height:8px;
	}
	#main_menu li .last { }
	#main_menu li .last a { padding-right:0;}
	#main_menu li .currentli 
	{
		background-color:#0066cc;
		padding-top:5px;
		padding-bottom:6px;
		-webkit-user-select: none;
		-moz-user-select: none;
		user-select: none;
		-moz-border-radius-topleft: 4px;
		-moz-border-radius-topright: 4px;
		-moz-border-radius-bottomright: 0px;
		-moz-border-radius-bottomleft: 0px;
		border-top-left-radius:4px;
		border-top-right-radius: 4px;
		border-bottom-right-radius: 0px;
		border-bottom-left-radius: 0px;
	}
	#main_menu li .li_seperator {
		color:#ffffff;padding-left:1px;padding-right:1px;
	}		 
	</style>
</head>
<body>
	<form><input type="text" name="dummy" style="position:absolute; top:-30px; height:1px; width:1px"></form>
	<div id="container" style="margin-top:0px !important;">
		<div id="header">
			<table>
				<tr>
					<td><h1>Administration</h1></td>
					<td id="header_right">
						<?php if  ($this->Session->check('user')) : ?>
							<b><span style="padding-left:50px;">Welcome back <?php echo $this->Session->read('user.User.first_name'). ' '. $this->Session->read('user.User.last_name'); ?></span></b><br/>
							<a target="_blank" href="<?php echo $this->Html->url('/livehelp/admin'); ?>">Admin Live Help</a> | <a href="<?php echo $this->Html->url('/admin'); ?>">Admin</a> | <a href="<?php echo $this->Html->url('/'); ?>">Public Website</a> | <a href="<?php echo $this->Html->url('/users/logout/'); ?>">Sign Out</a><br/><br/>
						<?php endif; ?>
					</td>
				</tr>
			</table>
			<table>
				<tr id="banner_container">
					<td align="right" style="border-left:1px solid #DFDFDF;border-right:1px solid #DFDFDF;padding:0;<?php
						if ($this->Session->read('client.Client.banner_background_image_left_extension') != '') {
							echo "background-image:  url('". $this->Html->url('/files/banner_background_image_left/'. $this->Session->read('client.Client.id'). '.'. $this->Session->read('client.Client.banner_background_image_left_extension')). "');";
						}
						
						echo 'background-repeat: no-repeat;height:100px';
						?>">
						<?php if($this->Session->read('client.Client.banner_background_image_right_extension') !=''){ ?>
						<img src="<?php echo $this->Html->url('/files/banner_background_image_right/'. $this->Session->read('client.Client.id'). '.'. $this->Session->read('client.Client.banner_background_image_right_extension')); ?>" alt=""  border="0" />
						<?php } ?>
					</td>
				</tr>
			</table>
			<br/>
			<div style="padding-left: 20px;">
				<div id="main_menu"  class="rounded-corners">
					<ul id="client_menu">
						<li>
							<a  href="<?php echo $this->Html->url('/admin/styles/'); ?>">Styles</a><span class="li_seperator">|</span>  
							</li>
							<?php if($this->Session->read('user.User.type') == 'Administrator'): ?>
							<li>
								<a href="<?php echo $this->Html->url('/admin/programs/'); ?>">Programs</a><span class="li_seperator">|</span> 
							</li>
							<?php endif ?>
							<li>
								<a href="<?php echo $this->Html->url('/admin/clients/'); ?>">Clients</a><span class="li_seperator">|</span> 
							</li>
							<?php if($this->Session->read('user.User.type') == 'Administrator'): ?>
							<li>
								<a href="<?php echo $this->Html->url('/admin/domains/'); ?>">Domains</a><span class="li_seperator">|</span> 
							</li>
							<li>
								<a href="<?php echo $this->Html->url('/admin/categories/'); ?>">Categories</a><span class="li_seperator">|</span>  
							</li>
							<li>
								<a href="<?php echo $this->Html->url('/admin/merchants/'); ?>">Merchants</a><span class="li_seperator">|</span> 
							</li>
							<li>
								<a href="<?php echo $this->Html->url('/admin/products/'); ?>">Products</a><span class="li_seperator">|</span> 
							</li>
							<?php endif; ?>
							<li>
								<a href="<?php echo $this->Html->url('/admin/users/'); ?>">Users</a><span class="li_seperator">|</span> 
							</li>
							<?php if($this->Session->read('user.User.type') == 'Administrator'): ?>
							<li>
								<a href="<?php echo $this->Html->url('/admin/reports/'); ?>">Reports</a><span class="li_seperator">|</span> 
							</li>
							<li>
								<a href="<?php echo $this->Html->url('/admin/newsletters/'); ?>">Newsletters</a><span class="li_seperator">|</span> 
							</li>
							<li>	
								<a href="<?php echo $this->Html->url('/admin/comments/'); ?>">Comments</a><span class="li_seperator">|</span> 				
							</li>
							<?php endif; ?>
							<li>
								<a href="<?php echo $this->Html->url('/admin/skyscrapers/'); ?>">SkyScraper</a>
							</li>
							<?php if($this->Session->read('user.User.type') == 'Administrator'): ?>
							<li>
								<span class="li_seperator">|</span><a href="<?php echo $this->Html->url('/admin/user_nominates/'); ?>">InviteMyFamily</a><span class="li_seperator">|</span> 
							</li>
						
							<li>
								<a  class="last" href="<?php echo $this->Html->url('/admin/advt_4_free/'); ?>">Advt4Free</a><span class="li_seperator">|</span>
							</li>
							<li>
								<a  class="last" href="<?php echo $this->Html->url('/admin/campaigns/'); ?>">Competitions</a>
							</li>
							
							<li>
								<a  class="last" href="<?php echo $this->Html->url('/admin/carts/'); ?>">Shopping Cart</a><span class="li_seperator">|</span>
							</li>
							<li>
								<a  class="last" href="<?php echo $this->Html->url('/admin/saf/'); ?>">SAF Products </a><span class="li_seperator">|</span>
							</li>
							<li>
								<a  class="last" href="<?php echo $this->Html->url('/admin/points/'); ?>">Points</a><span class="li_seperator">|</span>
							</li>
							<li>
								<a  class="last" href="<?php echo $this->Html->url('/admin/safs_invitation/'); ?>">Send A Friend Invitation</a><span class="li_seperator">|</span>
							</li>
							<li>
								<a  class="last" href="<?php echo $this->Html->url('/admin/safs_invitation/saf_logs/'); ?>">Send A Friend Log</a><span class="li_seperator">|</span>
							</li>
							<li>
								<a  class="last" href="<?php echo $this->Html->url('/admin/auto_logins/'); ?>">Auto Login Details</a>
							</li>
							<?php endif;?>
					</ul>
			
					<script type="text/javascript">setPage()</script>
				</div>
			</div>
		</div>
		
		<div id="content">
			<?php if ($this->Session->check('Message.flash')): ?>
				<div id="message">
					<?php echo $this->Session->flash(); ?>
				</div>
			<?php endif;?>
		
			<?php echo $content_for_layout;?>
		</div>
		<br/>
		
	</div>
	<?php if  ($this->Session->read('client.Client.footer_links_disabled') != '1') : 
			$pages = $this->Session->read('client.Page');
				$start =0;
			  $search_allowed =0;
			  $news_allowed =0;
			  $special_allowed =0;
			  $customer_allowed =0;
			  $supplier_allowed =0;
				foreach($pages as $page){
					switch($page['Page']['name']){
						case '/products/search': $search_allowed = 1;break;
						case '/display/news': $news_allowed = 1;break;
						case '/display/special_offers': $special_allowed = 1;break;
						case '/display/customer_service': $customer_allowed = 1;break;
						case '/display/supplier': $supplier_allowed = 1;break;
					}
				}
		?> 
	<div id="footer">
		<?php if($search_allowed){ ?><a href="<?php echo $this->Html->url('/products/search/'); ?>">Search</a><?php $start++; } ?>
		<?php if($news_allowed){ if($start >0) echo " | "; $start ++; ?><a href="<?php echo $this->Html->url('/display/news/'); ?>">News</a><?php } ?>
		<?php if($special_allowed){  if($start >0) echo " | "; $start ++; ?><a href="<?php echo $this->Html->url('/display/special_offers/'); ?>">Special Offers</a><?php } ?>
		<?php if($customer_allowed){  if($start >0) echo " | "; $start ++; ?><a href="<?php echo $this->Html->url('/display/customer_service/'); ?>">Customer Service</a><?php } ?>
		<?php if($supplier_allowed){  if($start >0) echo " | "; $start ++; ?><a href="<?php echo $this->Html->url('/display/supplier/'); ?>">Supplier</a><?php } ?>
		<br/>
		<div style="margin-top:8px;margin-bottom:8px">&copy; My Rewards International Ltd 2014 All Rights Reserved.</div>
	</div>
	<?php endif; ?>
	<div style="position:absolute"><?php echo $this->element('sql_dump'); ?></div>
</body>
</html>