<html>
<head>
	<title>
		<?php echo $title_for_layout;?>
	</title>

	<?php echo $this->Html->charset();?>
	<style type="text/css">
		body {
			font-family: <?php echo $newsletter['Newsletter']['font']; ?>;
			font-size: 12px;
		}

		a.more:visited {
			color: #<?php echo $client['Style']['button_color']; ?>;
		}

		a, a:visited, a:link, a:active {
			<?php echo ($client['Style']['link_color'] != '')? 'color: #'. $client['Style']['link_color']. ';': ""; ?>
		}

		a:hover {
			<?php echo ($client['Style']['link_hover_color'] != '')? 'color: #'. $client['Style']['link_hover_color']. ';': ""; ?>
		}

		h1{
			font-size: 20px;
		}
		h2 {
			font-size: 18px;
		}
		h3 {
			font-size: 16px;
		}
		h4 {
			font-size: 14px;
		}
		h5 {
			font-size: 12px;
			margin: 4px 0;
		}


	</style>
</head>
<body>
	<?php echo $content_for_layout;?>
</body>
</html>
