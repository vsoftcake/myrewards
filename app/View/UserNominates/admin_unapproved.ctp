<style>
	.current
	{
	    font-family: 'Droid Sans',sans-serif;
	    font-size: 12px;
	   	padding:5px 8px !important;   
	    background-color: #0066CC;
	    border: 1px solid #DFDFDF;
	    color:#ffffff;
	}
	.cpagination1
	{
		/*padding:1px 2px 1px 2px;*/
		font-family: 'Droid Sans',sans-serif;
	    font-size: 12px;
		text-decoration:none !important;
		background-color: #FFFFFF;
		padding:3px 8px;
		border: 1px solid #0066CC;
		color:#808080 !important;
	}
	#content
	{
		border-left:1px solid #cccccc;
		border-right:1px solid #cccccc;
		border-bottom:1px solid #cccccc;
		margin-left:20px !important;
		padding-left:15px !important;
		width:916px;
	}
	.admin_button
	{
		height: 24px;
		padding-top: 2px;
		padding-bottom: 5px;
		border-radius: 5px 5px 5px 5px;
		background-color: #0066cc;
		border: 1px solid #cccccc;
		color: #FFFFFF; 
		cursor: pointer;
		font-size:13px;
	}
</style>
<script>
	function deleteApprovals(id)
	{
		var url = '/admin/user_nominates/delete_selected/';
		var list="";
		var length=document.nominateForm.elements.length;
		for(var i=0;i<length;i++)
		{
			if(document.nominateForm.elements[i].name=="data[UserNominate][check]")
			{
				if(document.nominateForm.elements[i].checked)
				{
					if(list.length>0)
						list=list+","+document.nominateForm.elements[i].value;
					else
						list=list+document.nominateForm.elements[i].value;
				}
			}
		}
		if(list=="")
			alert("Please select users to delete");	
		else
			document.location=url+list;
	}
	
	function bulkApprovals()
	{
		var url = '/admin/user_nominates/bulk_approvals/';
		var list="";
		var length=document.nominateForm.elements.length;
		for(var i=0;i<length;i++)
		{
			if(document.nominateForm.elements[i].name=="data[UserNominate][check]")
			{
				if(document.nominateForm.elements[i].checked)
				{
					if(list.length>0)
						list=list+","+document.nominateForm.elements[i].value;
					else
						list=list+document.nominateForm.elements[i].value;
				}
			}
		}
		if(list=="")
			alert("Please select users to approve");	
		else
			document.location=url+list;
	}
</script>
<table style="margin-left:-2px;margin-top:10px;">
	<tr>
		<td>
            &nbsp;&nbsp;<img style="width:40px;height:40px;" src="/files/admin_home_icons/imf.png"  alt="Invite my family" title="Invite my family" />&nbsp;&nbsp;
        </td>
        <td style="width:500px"> 
            <h2><?php echo "Unapproved Friends";?></h2>
        </td>
        <td style="margin-left:100px">
        	<h3>
				<a href="/admin/user_nominates/">Back</a>
			</h3>
        </td>
    </tr>        
</table>
<br/>
<form name="nominateForm" method="post">
	<div class="list" style="width:920px;">
		<table cellpadding="0" cellspacing="0">
		<tr height="30">
			<th style="width:125px;background-color:#cccccc;text-align:left;padding-left:5px;border-left:1px solid #969090;border-bottom:1px solid #969090;border-top:1px solid #969090;"></th>
			<th style="width:125px;background-color:#cccccc;text-align:left;padding-left:5px;border-bottom:1px solid #969090;border-top:1px solid #969090;">User ID</th>
			<th style="width:125px;background-color:#cccccc;text-align:left;padding-left:1px;border-bottom:1px solid #969090;border-top:1px solid #969090;">Name</th>
			<th style="width:125px;background-color:#cccccc;text-align:left;padding-left:1px;border-bottom:1px solid #969090;border-top:1px solid #969090;">Country</th>
			<th style="width:125px;background-color:#cccccc;text-align:left;padding-left:1px;border-bottom:1px solid #969090;border-top:1px solid #969090;">Client</th>
			<th style="width:125px;background-color:#cccccc;text-align:left;padding-left:1px;border-bottom:1px solid #969090;border-top:1px solid #969090;">First Name</th>
			<th style="width:125px;background-color:#cccccc;text-align:left;padding-left:1px;border-bottom:1px solid #969090;border-top:1px solid #969090;">Last Name</th>
			<th style="width:125px;background-color:#cccccc;text-align:left;padding-left:1px;border-bottom:1px solid #969090;border-top:1px solid #969090;">Relationship</th>
			<th style="width:125px;background-color:#cccccc;text-align:left;padding-left:1px;border-bottom:1px solid #969090;border-top:1px solid #969090;">Email</th>
			<th class="actions" style="background-color:#cccccc;text-align:left;padding-left:4px;border-right:1px solid #969090;border-bottom:1px solid #969090;border-top:1px solid #969090;">Actions</th>
		</tr>
		<?php 
		$i = 0;
		foreach($nominate as $nom):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
		?>
	
		<tr<?php echo $class;?>>
			<td><?php echo $this->Form->checkbox('UserNominate.check',array('value'=>$nom['UserNominate']['id'])); ?></td>
			<td style="width:100px"><?php echo $nom['UserNominate']['user_id'] ?></td>
			<td style="width:450px"><?php echo $nom['UserNominate']['clientname']; ?></td>
			<td style="width:100px"><?php echo $nom['User']['country'] ?></td>
			<td style="width:200px"><?php echo $nom['Client']['name'] ?></td>
			<td style="width:450px"><?php echo $nom['UserNominate']['firstname']; ?></td>
			<td style="width:450px"><?php echo $nom['UserNominate']['lastname'] ?></td>
			<td style="width:300px"><?php echo $nom['UserNominate']['relationship'] ?></td>
			<td style="width:300px"><?php echo $nom['UserNominate']['email'] ?></td>
			
			<?php if($nom['UserNominate']['approved']==0): ?>
			<td class="actions" style="width:550px">
				<?php echo $this->Html->link(__('Approve', true), array('action'=>'approve', $nom['UserNominate']['id'])); ?>
				<?php echo $this->Html->link(__('Delete', true), array('action'=>'delete', $nom['UserNominate']['id']), null, sprintf(__('Are you sure you want to delete User \'%s\'?', true), $nom['UserNominate']['firstname'])); ?>
			</td>
			<?php endif; ?>
			<?php if($nom['UserNominate']['approved']==1): ?>
			<td class="actions" style="width:550px">
			<?php echo "APPROVED"; ?>
			</td>
			<?php endif; ?>
		</tr>
		<?php endforeach; ?>
		<tr>
			<td colspan="10">
				<div align="center">
					<button type="button" class="admin_button rounded_corners_button" onclick="deleteApprovals()">Delete</button>
					<button type="button" style="margin-left:15px;" class="admin_button rounded_corners_button" onclick="bulkApprovals()">Approve</button>
	        	</div>
	        </td>
		</tr>
		</table>
	</div>
	
	<div class="paging" style="padding-top:15px;float:right;">
		<table style="border:none !important;">
			<tr>
				<td style="border:none !important;padding-right: 5px;">
					<?php echo $this->Paginator->first('<<', array('title'=>'first','class'=>'disabled cpagination1'));?>
					<?php echo $this->Paginator->prev('<', array('title'=>'prev','class'=>'disabled cpagination1'));?>
					<?php echo $this->Paginator->numbers(array('separator' => ' ','class'=>'cpagination1'));?>
					<?php echo $this->Paginator->next('>', array('title'=>'next','class'=>'disabled cpagination1'));?>
					<?php echo $this->Paginator->last('>>', array('title'=>'last','class'=>'disabled cpagination1'));?>
				</td>
			</tr>
			<tr>
				<td style="border:none !important;">
					<p><?php echo $this->Paginator->counter(array('format' => __('Page %page% of %pages%', true)));	?></p>
				 </td>
			</tr>
		</table>
	</div>
</form>