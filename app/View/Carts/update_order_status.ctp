<style>
#content
{
	border-left:1px solid #cccccc;
	border-right:1px solid #cccccc;
	border-bottom:1px solid #cccccc;
	margin-left:20px !important;
	padding-left:18px !important;
	width:913px;
}
.admin_button
{
	height: 26px;
	padding-bottom: 2px;
	border-radius: 5px 5px 5px 5px;
	background-color: #0066cc;
	border: 1px solid #cccccc;
	color: #FFFFFF; 
	cursor: pointer;
	font-size:13px;
	width:90px;
}
</style>
<div style="padding-left:30px" class="rounded-corners_bottom">
		
		<div class="module">
			
			<table style="margin-left:-2px;margin-top: 10px;">
			    <tr>
				<td style="width:800px">
					<div style="float:left">
						<div style="padding-top:10px;">
							<span style="color: #E2008A;margin: 0;font-size: 18px;" >Update Order Details </span><br><br>
						</div>
					</div>
					<div style="float: right; padding-top: 15px;">
						<a class="link_color" style="font-weight:normal;margin-left:25px;" id="back" href="javascript:window.history.back()">
							<button class="admin_button rounded_corners_button">Back</button>
						</a><br/>
			
					</div>
				</td>
			    </tr>               
			</table>
			<br>
			<div>
				
					<form name="CartUpdateOrderStatus" id="CartUpdateOrderStatus" action="/carts/update_order_status/<?php echo $id;?>" class="standard" method="post">
					<table>
						<input type="hidden" name="productid" id="productid" value="<?php echo $productid; ?>" >
						<input type="hidden" name="cartid" id="cartid" value="<?php echo $id;?>" >
						<tr>
							<td>Shipment Name:</td>
							<td><?php echo $this->Form->input('CartItem/courier_name', array("label"=>false, "div"=>false)); ?><br />
						</tr>
						
						<tr>
							<td>Consignment Number: </td>
							<td><?php echo $this->Form->input('CartItem/courier_number', array("label"=>false, "div"=>false)); ?><br />
						</tr>
						
						<tr>
							<td>Track your Shipment: </td>
							<td><?php echo $this->Form->input('CartItem/tracking_link', array("label"=>false, "div"=>false)); ?><br />
						</tr>
						<tr>
							<td>Estimated Date: </td>
							<td><?php echo $this->Form->input('CartItem/completed_date', array("label"=>false, "div"=>false)); ?><br />
						</tr>
						<tr>
							<td>Modify Status:</td>
							<td><?php echo $this->Form->input('CartItem/cart_status', array("label"=>false, "div"=>false, "options"=>array("Shipped"=>"Shipped", "Cancelled"=>"Cancelled"))); ?></td>
						</tr>
						<tr>
							<td>Comments: </td>
							<td><?php echo $this->Form->textarea('CartItem/comments', array("label"=>false, "div"=>false)); ?><br />
						</tr>
						<tr>
							<td colspan=4 align="center">
							<button type="submit" class="admin_button" >Submit</button>
							</td>
						</tr>
					</table>
				</form>
				<?php //echo $this->Form->end();?>
			</div>
		</div>
		
	</div>
