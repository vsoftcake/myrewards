<style>
.cont-right {
    float: right;
    overflow: hidden;
    width: 245px;
    margin-top: 25px;
    padding-right: 10px;
}
.checkout-cart-box {
    margin-bottom: 8px;
    padding: 2px;
}
.content-right-common, .checkout-cart-box {
    background: none repeat scroll 0 0 #C0C2C3;
    border-radius: 8px 8px 8px 8px;
    margin-top: 0;
    padding: 3px;
}
.checkout-cart-box-head {
    overflow: hidden;
}
.checkout-mycart {
    background: url("/files/styles/icon_images/shop.png") no-repeat scroll 0 1px transparent;
    color: #383838;
    float: left;
    font-weight: bold;
    height: 47px;
    margin-left: 4px;
    overflow: hidden;
    padding-left: 12px;
}
#cartQty {
    color: #FFFFFF;
    display: block;
    float: left;
    font-size: 15px;
   
}
.ch-mycart-text {
    display: block;
    float: left;
    font-size: 14px;
     padding: 9px 0 0 30px;
}
.ch-mycart-edit {
    display: block;
    float: left;
    font-size: 14px;
     padding: 9px 0 0 80px;
}
.checkout-cart-con {
    background: url("/img/cart-head-bg.gif") repeat-x scroll center top #FFFFFF;
    border-radius: 0 0 7px 7px;
    clear: both;
    font-size: 11px;
}
#mycartSummaryfill {
    background: url("/img/cart-bg.gif") repeat-y scroll 148px top transparent;
    overflow: hidden;
}
.ch-cart-items {
    padding-left: 6px;
    width: 140px;
}
.ch-cart-qt {
    text-align: center;
    width: 27px;
}
.ch-crt-price {
    padding-right: 6px;
    text-align: right;
    width: 54px;
}
.checkout-cart-head {
    float: left;
    padding: 3px;
}
.checkout-cart-list-outer {
    border-bottom: 1px dotted #B2B2B2;
    clear: both;
    overflow: hidden;
    padding: 3px 0 4px;
}
.ch-pay-outer {
    background: none repeat scroll 0 0 #E6E6E6;
    border-radius: 0 0 6px 6px;
    border-top: 1px solid #DADADA;
    font-size: 12px;
    font-weight: bold;
    overflow: hidden; 
    padding: 2px 0;
}

.ch-pay-box, .ch-shipcrg-box {
    float: left;
    padding: 6px 0 6px 8px;
    width: 140px;
}
.ch-pay-price, .ch-shipcrg-price {
    float: right;
    padding: 6px 6px 8px 0;
    text-align: right;
    width: 80px;
}
.ch-pay-box, .ch-shipcrg-box {
    float: left;
    padding: 6px 0 6px 8px;
    width: 140px;
}
.ch-pay-price, .ch-shipcrg-price {
    float: right;
    padding: 6px 6px 8px 0;
    text-align: right;
    width: 80px;
}
.content-right-common, .checkout-cart-box {
    background: none repeat scroll 0 0 #C0C2C3;
    border-radius: 8px 8px 8px 8px;
    margin-top: 0;
    padding: 3px;
}
.content-right-common .title {
    color: #272727;
    display: block;
    font-size: 16px;
    font-weight: bold;
    padding: 9px;
}
.payment-faq {
    background: none repeat scroll 0 0 #FFFFFF;
    border-radius: 0 0 6px 6px;
    color: #000000;
    font-size: 12px;
    margin: 0;
    padding: 5px 0 0 5px;
}
.payment-faq .qfaq {
    color: #505050;
    font-size: 12px;
    font-weight: bold;
    margin: 0;
    padding: 5px 0 5px 5px;
}
.payment-faq .afaq {
    color: #505050;
    font-size: 12px;
    padding: 0 0 5px 5px;
}
.payment-faq .afaq ul {
    color: #5A5959;
    display: block;
    font-size: 12px;
    list-style: disc outside none;
    margin: 0 0 0 16px;
    padding: 0 3px;
}
.payment-faq .afaq ul, .payment-faq .afaq ul li {
    list-style: disc outside none !important;
}
.online_request_border
	{
		border:1px solid;
	}
.online_td
{
	font-size:13px;
}
.tr
{
	height:35px;
}
.online_input
{
	width:200px;
	height:22px;
}
.online_request_border
{
	border:1px solid;
}
.tabs-outer {
    margin: 0 10px 25px;
    overflow: hidden;
    width: 703px;
}
.tab-active {
    color: #17B0DC;
    font-size: 13px;
    font-weight: bold;
}
.tabs-order {
    background: url("/files/styles/icon_images/shop.png") no-repeat scroll center center transparent;
    height: 36px;
    margin-top:20px;
}
.shipping {
    background: url("/files/styles/icon_images/shipping.jpg") no-repeat scroll center center transparent;
    height: 36px;
    margin-top:20px;
}
.payment {
    background: url("/files/styles/icon_images/pay.jpg") no-repeat scroll center center transparent;
    height: 36px;
    margin-top:20px;
}
.order {
    background: url("/files/styles/icon_images/summary.jpg") no-repeat scroll center center transparent;
    height: 36px;
    margin-top:20px;
}
.tabs-cont {
    color: #666666;
    float: left;
    font-size: 12px;
    font-weight: normal;
    width: 150px;
}

.address {
    float: left;
    font-size: 12px;
    padding: 10px;
}
.customer-info {
    float: left;
    overflow: hidden;
    padding-bottom: 25px;
    width: 380px;
}
</style>
<script>
	function checkValidation()
	{
	   	username = document.getElementById('CartAddressUsername').value;
		email = document.getElementById('CartAddressEmail').value;
		mobile = document.getElementById('CartAddressMobile').value;
		address1 = document.getElementById('CartAddressAddress1').value;
		//address2 = document.getElementById('CartAddressAddress2').value;
		country = document.getElementById('CartAddressCountry').value;
		state = document.getElementById('CartAddressState').value;
		city = document.getElementById('CartAddressCity').value;
		zipcode = document.getElementById('CartAddressZipcode').value;
		
		if (username == null || username =='')
   		{
			alert("Please Enter Username");
			return false;
		}
		var atpos=email.indexOf("@");
		var dotpos=email.lastIndexOf(".");
		if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length)
	  	{
  			alert("Please Enter A Valid Email");
  			return false;
  		}
  		
  		if (mobile == null || mobile =='')
   		{
			alert("Please Enter Mobile Number");
			return false;
		}
    	
		if (country == null || country =='')
			{
			alert("Please Select Country");
			return false;
		}
        
	       /* if (country != 'Hong Kong' && (state == null || state ==''))
			{
			alert("Please select state");
			return false;
		} */

		if (address1 == ''|| address1=="")
		{
			alert("Please Enter Address Line1");
			return false;
		}

		if (city == ''|| city=="")
		{
			alert("Please Enter City");
			return false;
		}

		if (zipcode == ''|| zipcode=="")
		{
			alert("Please Enter Zip Code");
			return false;
		}

	    return true;	
	}
	
	
	
	
function address(id) {
	
	
	$.ajax({
    type:"POST",
    url:"/carts/address_details/"+id,
    success:function(data){
		
	var obj = $.parseJSON(data);
   // alert(obj['username']);
	
	
	document.getElementById('CartAddressId').value = obj['id'];
			    document.getElementById('CartAddressUsername').value = obj['username'];
				document.getElementById('CartAddressEmail').value = obj['email'];
				document.getElementById('CartAddressMobile').value = obj['mobile'];
				document.getElementById('CartAddressAddress1').value = obj['address1'];
				document.getElementById('CartAddressAddress2').value = obj['address2'];
				document.getElementById('CartAddressCountry').value = obj['country'];
				document.getElementById('CartAddressCity').value = obj['city'];
				document.getElementById('CartAddressZipcode').value = obj['zipcode'];

				$.ajax({
					url: '/carts/update_online_request_state/'+obj['country'],
					type: "JSON",
					success: function (data) {
						$('#CartAddressState').val(obj['state']);
					}
				});
	
		
		
			
             }
    });
	
			
		
		
		
		document.getElementById('CartAddressStatus').value = '';
	}
	
	
	
	
	
	function addnewaddress()
	{
		document.getElementById('CartAddressId').value = '';
		document.getElementById('CartAddressMobile').value = '';
		document.getElementById('CartAddressAddress1').value = '';
		document.getElementById('CartAddressAddress2').value = '';
		document.getElementById('CartAddressCountry').value = '';
		document.getElementById('CartAddressCity').value = '';
		document.getElementById('CartAddressZipcode').value = '';
		document.getElementById('CartAddressState').value = '';
		document.getElementById('CartAddressStatus').value = 'new';
	}
function update_email()
{
	var email = document.getElementById('CartAddressEmail').value;
	var atpos=email.indexOf("@");
	var dotpos=email.lastIndexOf(".");
	if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length)
	{
		alert("Please enter a valid email");
		return false;
	}
	else
	{
		$.ajax({
			url: '/users/update_email/'+email,
			type: "GET",
			success: function (data) {
				$('#emailupdate').html(data);
			}
		});
	}
}
</script>
<?php echo $this->element('module2', array('module2' => 'Keyword_search', 'title' => 'Search'));?>
<div class="online_request_border"  style="width:950px;margin-bottom:0px !important;min-height:10px;overflow:hidden; padding-bottom: 20px;">
	<div style="background: none repeat scroll 0 0 #FFFFFF;border: 1px solid #AEAEAE;border-radius: 10px 10px 10px 10px;box-shadow: 0 0 2px #828282;float: left;overflow: hidden;margin-top:25px;margin-left:10px;width: 670px;">
			
		<div class="tabs-outer">
			<div class="tabs-cont">
				<a href="/carts/show_cart">
					<div class="tabs-order"></div>
					<div align="center" class="tab-active">Your Cart</div>
				</a>
			</div>
	
			<div class="tabs-cont">
				<div class="shipping"></div>
				<div align="center" class="tab-active">Shipping Details</div>
			</div>
		
			<div style="" id="step3" class="tabs-cont order-summary-tab tab-disabled">
				<div class="order"></div>
				<div align="center">Order Summary</div>
			</div>
		
			<div id="step4" class="tabs-cont payment-tab tab-disabled">
				<div class="payment"></div>
				<div align="center">Payment</div>
			</div>
		</div>
		
		<div style="padding-left:10px">
			<div id="emailupdate" style="color:#17B0DC;font-size:13px;font-weight:bold"></div>
		<?php if(!empty($user_address)) { ?>			
			<span class="tab-active">Select one of previous shipping addresses OR Enter a shipping address</span>
			<?php } else { ?>
			<span class="tab-active">Enter A Shipping Address</span>
			<?php } ?>
			
		</div>
		
		<div  style="margin-top:10px;">
		<div style="border-right: 2px solid #C0C2C1;float: left;width: 250px;">
			<div class="address"><button type="submit" class="quick-search_button_style rounded_corners_button" style="width:150px" onclick="addnewaddress()" >Add New Address</button></div>
		    	<?php foreach($user_address as $details): ?>
				<div class="address">
					<strong><?php echo $details['CartAddress']['username'] ?></strong><br>
					<?php echo $details['CartAddress']['member_name'] ?> <br>
					<?php echo $details['CartAddress']['address1'] ?> <br>
					<?php echo $details['CartAddress']['city'] ?> <br>
					<?php echo $details['CartAddress']['state'].' - '.$details['CartAddress']['zipcode'] ?> <br>
					<?php echo $details['CartAddress']['country'] ?> <br>
					
					Phone: <span><?php echo $details['CartAddress']['mobile'] ?> <br></span><br><br>
					<button type="submit" class="quick-search_button_style rounded_corners_button" style="width:150px" onclick="address(<?php echo $details['CartAddress']['id']?>)" >Ship to this address</button>
				</div>
			<?php endforeach; ?>
			</div>
		</div>
		
		<form action="<?php echo $this->Html->url('/carts/order_summary/'.$cart[0]['Cart']['id']); ?>" method="post" onSubmit="return checkValidation();">
		<div class="customer-info">	
			<table style="width:100%;margin-left:20px">
				<?php echo $this->Form->input( 'CartAddress.id', array( 'type' => 'hidden','value'=>$address[0]['CartAddress']['id']) );  ?>
				<?php echo $this->Form->input( 'CartAddress.cartid', array( 'type' => 'hidden','value'=>$cart[0]['Cart']['id']) );  ?>
				<?php 
				if ($address[0]['CartAddress']['id'] == ''){
					echo $this->Form->input( 'CartAddress.status', array( 'type' => 'hidden','value'=>'new')); 
				} else {
					echo $this->Form->input( 'CartAddress.status', array( 'type' => 'hidden','value'=>'')); 
				}
				?>
			
				<tr class="tr">
					<td class="online_td">
						<strong>User name</strong>
					</td>
					<td>
						<?php echo $this->Form->input('CartAddress.username', array('class'=>'online_input','disabled'=>true, 'value'=>$this->Session->read('user.User.username'), 'div'=>false, 'label'=>false)); ?>
						<span style="vertical-align: top;color:red"> * </span>
					</td>
				</tr>
				<tr class="tr">
					<td class="online_td">
						<strong>Member name</strong>
					</td>
					<td>
						<?php echo $this->Form->input('CartAddress.username', array('class'=>'online_input','disabled'=>true, 'value'=>$this->Session->read('user.User.first_name').' '.$this->Session->read('user.User.last_name'), 'div'=>false, 'label'=>false)); ?>
					</td>
				</tr>
				<tr class="tr">
					<td class="online_td">
						<strong>Email</strong>
					</td>
					<td>
						<?php echo $this->Form->input('CartAddress.email', array('class'=>'online_input','value'=>$this->Session->read('user.User.email'), 'div'=>false, 'label'=>false)); ?>
						<span style="vertical-align: top;color:red"> * </span>
					</td>
				</tr>
				<tr>
					<td class="online_td"></td><td><a href="javascript:void(0);" onclick="update_email()" style="color:#17B0DC"><strong>Update Email</strong></a></td>
				</tr>
				<tr class="tr">
					<td class="online_td">
						<strong>Mobile</strong>
					</td>
					<td>
						<?php echo $this->Form->input('CartAddress.mobile', array('class'=>'online_input','value'=>$address[0]['CartAddress']['mobile'], 'div'=>false, 'label'=>false)); ?>
						<span style="vertical-align: top;color:red"> * </span>
					</td>
				</tr>
				<tr class="tr">
					<td class="online_td">
						<strong>Address1</strong>
					</td>
					<td>
						<?php echo $this->Form->input('CartAddress.address1', array('class'=>'online_input','value'=>$address[0]['CartAddress']['address1'], 'div'=>false, 'label'=>false)); ?>
						<span style="vertical-align: top;color:red"> * </span>
					</td>
				</tr>
				<tr class="tr">
					<td class="online_td">
						<strong>Address2</strong>
					</td>
					<td>
						<?php echo $this->Form->input('CartAddress.address2', array('class'=>'online_input','value'=>$address[0]['CartAddress']['address2'], 'div'=>false, 'label'=>false)); ?>
						<!--<span style="vertical-align: top;color:red"> * </span>-->
					</td>
				</tr>
				<tr class="tr">
					<td class="online_td">
						<strong>Country</strong>
					</td>
					<td>
						<?php echo $this->Form->input('CartAddress.country', array('type'=>'select','options'=>$countries,'default'=>$address[0]['CartAddress']['country'], 'empty'=> 'Select Country','style'=>'height: 25px !important;width:214px !important',  'class'=>'online_input', 'div'=>false, 'label'=>false)); ?>
						<?php 
							$options = array('url' => '/carts/update_online_request_state','update' => 'CartAddressState');
				           	echo $this->Ajax->observeField('CartAddressCountry', $options);
			         	?>
			         	<span style="vertical-align: top;color:red"> * </span>
					</td>
				</tr>
				<tr class="tr">
					<td class="online_td">
						<strong>State</strong>
					</td>
					<td>
						<?php echo $this->Form->input('CartAddress.state', array('type'=>'select','options'=>$states,'default'=>$address[0]['CartAddress']['state'], 'empty'=> 'Select State','style'=>'height: 25px !important;width:214px !important', 'class'=>'online_input', 'div'=>false, 'label'=>false)); ?>
							<script language="javascript">
							$( "#CartOrderAddressCountry" ).change(function() {
								if($(this).val()!='Hong Kong') {
									$('#star').show();
								}else{
									$('#star').hide();
								}
							});
							</script>
					</td>
				</tr>
				<tr class="tr">
					<td class="online_td">
						<strong>City</strong>
					</td>
					<td>
						<?php echo $this->Form->input('CartAddress.city', array('class'=>'online_input','value'=>$address[0]['CartAddress']['city'], 'div'=>false, 'label'=>false)); ?>
						<span style="vertical-align: top;color:red"> * </span>
					</td>
				</tr>
				<tr class="tr">
					<td class="online_td">
						<strong>Postcode</strong>
					</td>
					<td>
						<?php echo $this->Form->input('CartAddress.zipcode', array('class'=>'online_input','value'=>$address[0]['CartAddress']['zipcode'], 'div'=>false, 'label'=>false)); ?>
						<span style="vertical-align: top;color:red"> * </span>
					</td>
				</tr>
				
				<tr class="tr">
					<td class="online_td">&nbsp;</td>
					<td>
						<?php echo $this->Form->submit('Continue', array('name'=>'data[ProductsCart][online]', 'class'=>'button_search_module rounded_corners_button','style'=>'width:120px !important;cursor:pointer'));?>
					</td>
				</tr>
				
				</table>
			</form>
		</div>
	</div>
	
	<div class="cont-right">
		<div id="myCartSummary" class="checkout-cart-box">
			<div class="checkout-cart-box-head">
				<div class="checkout-mycart">
					<span id="cartQty" style="background-color:#01B7FF;border-radius: 20px 20px 20px 20px;text-align: center;width: 20px;"><?php echo count($cart); ?></span> 
					<span class="ch-mycart-text">My Cart</span> <span class="ch-mycart-edit"><a href="/carts/show_cart/" >Edit </a></span>
				</div>	
			</div>
			
			<div class="checkout-cart-con">
				<div id="mycartSummaryfill">
					<div class="checkout-cart-head ch-cart-items">
					<strong>Item(s)</strong>
					</div>
					<div class="checkout-cart-head ch-cart-qt"><strong>Qty</strong></div>
					<div class="checkout-cart-head ch-crt-price"><strong>Price</strong></div>
					<?php $grandtotal=0; $shipping_count = 0;?>
					<?php foreach($cart as $details): ?>
						<div class="checkout-cart-list-outer">
							<div class="checkout-cart-head ch-cart-items"><?php echo $details['Product']['name']; ?></div>
							<div class="checkout-cart-head ch-cart-qt"><?php echo $details['CartItem']['product_qty']; ?></div>
							<?php if ($details['CartProduct']['product_type'] == 'Points') { ?>
								<div class="checkout-cart-head ch-crt-price"><?php echo $pointstotal= ($details['CartItem']['product_qty'] * $details['CartProduct']['product_points']).' '.P; ?></div>
							<?php 
			         		} 
							else 
							{ 
								if($details['CartProduct']['product_shipping_type']==1) { ?>
									<div class="checkout-cart-head ch-crt-price">
									<?php $total= (($details['CartItem']['product_qty'] * $details['CartProduct']['product_price'])+$details['CartProduct']['product_shipping_cost']); ?>
									<?php echo "$ ".number_format($total,2);?>
									</div>
								<?php }
								if($details['CartProduct']['product_shipping_type']==2) { ?>
									<div class="checkout-cart-head ch-crt-price">
										<?php $total= (($details['CartItem']['product_qty'] * ($details['CartProduct']['product_price']+$details['CartProduct']['product_shipping_cost']))); ?>
										<?php echo "$ ".number_format($total,2);?>
									</div>
								<?php } 
								if($details['CartProduct']['product_shipping_type']==3) { ?>
									<div class="checkout-cart-head ch-crt-price">
									<?php 
										if($shipping_count != $details['CartProduct']['product_shipping_type'])
											$total= (($details['CartItem']['product_qty'] * $details['CartProduct']['product_price'])+$max_group_ship_cost); 
										else
											$total= ($details['CartItem']['product_qty'] * $details['CartProduct']['product_price']); 
										
										echo "$ ".number_format($total,2);?>
									</div>
								<?php }
								if($details['CartProduct']['product_shipping_type']==4)
								{?>
									<div class="checkout-cart-head ch-crt-price">
									<?php 
										if($shipping_count != $details['CartProduct']['product_shipping_type']):
											$total= ($details['CartItem']['product_qty'] * $details['CartProduct']['product_price']) + $counts[0]['CartItem']['smart_shipping_cost']; 
										else :
											$total= $details['CartItem']['product_qty'] * $details['CartProduct']['product_price'];
										endif;
									?>
									<?php echo "$ ".number_format($total,2);?>
									</div>
								<?php }
							}?>
						</div>

					<?php  $grandtotal += $total; $grandpoints+=$pointstotal; $shipping_count = $details['CartProduct']['product_shipping_type'];?>
					<?php endforeach; ?>
					
					<?php if($details['CartItem']['product_type'] =='Pay') :?>
					<div class="ch-pay-outer">
						<div class="ch-pay-box">Payable Amount</div>
						<div class="ch-pay-price">
							<?php 
								//if(!empty($counts[0]['CartItem']['smart_shipping_cost']))
								//$grandtotal+= intval($counts[0]['CartItem']['smart_shipping_cost']);
							 	echo "$ ".number_format($grandtotal,2); 
							 ?>
						</div>
					</div>
					<?php endif; ?>

					<?php if($details['CartItem']['product_type'] =='Points') :?>
						<div class="ch-pay-outer">
							<div class="ch-pay-box">Redeemable Points</div>
							<div class="ch-pay-price"><?php echo $grandpoints."P"; ?></div>
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
		<div class="content-right-common">
			<?php if($this->Session->read('dashboardidinsession') == 1) { ?>
				<label style="color: #272727;display: block;font-size: 16px;font-weight: bold;padding: 9px;">Payment and Shipping</label>
				<div style="background: none repeat scroll 0 0 #FFFFFF;border-radius: 0 0 6px 6px;color: #000000;font-size: 12px;margin: 0;padding: 5px;">
						<div style=" color: #505050;font-size: 12px;padding: 5px;">
							<ul>
								<li>
									<div style=" color: #505050;font-size: 12px;font-weight: bold;margin: 0;padding: 5px 0 5px 5px;">How can I pay?</div>
								</li>
								<li>
									<div style=" color: #505050;font-size: 12px;font-weight: bold;margin: 0;padding: 5px 0 5px 5px;">You can pay with Visa or MasterCard credit cards.</div>
								</li>
								<li>
									<div style="padding: 5px 0 5px 5px">
										We use the PayPal payment system to take and process your payments which ensures that your payments are within a world class payment gateway. All the information is processed using SSL data encryption (secure encrypted connection), which protects the information from being viewed by anyone.
									</div>
								</li>
							</ul>
						</div>
						<div>
							<ul>
								<li>
									<div style=" color: #505050;font-size: 12px;font-weight: bold;margin: 0;padding: 5px 0 5px 5px;">How does the postage and handling work?</div>
								</li>
								<li>
									<div style="padding: 5px 0 5px 5px;">The level and type of postage and handling is determined by the product or service that you purchase. Most products will attract smart shipping rates. </div>
								</li>
								<li style="padding-left:10px">
									<div style=" color: #505050;font-size: 12px;font-weight: bold;margin: 0;padding: 5px 0 5px 5px;">Ordinary mail- Not recommended</div>
								
									<div style="padding:5px 0 5px 5px">Ordinary mail is the cheapest option however does not have any insurance and we are unable to track it through the postal system .If you choose this method and your order is lost, the supplier will not be responsible and no refund will apply.</div>
								
									<div style=" color: #505050;font-size: 12px;font-weight: bold;margin: 0;padding: 5px 0 5px 5px;">Registered Mail - Recommended </div>
								
									<div style="padding:5px 0 5px 5px">We recommend that you choose Registered mail for all orders as it gives a level of protection and we can track your order.</div>
								</li>
								<li>
									<div style=" color: #505050;font-size: 12px;font-weight: bold;margin: 0;padding: 5px 0 5px 5px;">
										Variable postage and handling rates:
									</div>
								</li>
								<li>	
									<div style="padding:5px 0 5px 5px">
										Variable Postage and Handling rates will apply to certain products. These rates will be shown in cart and the product as you make the order 
									</div>
								</li>
								<li>
									<div style=" color: #505050;font-size: 12px;font-weight: bold;margin: 0;padding: 5px 0 5px 5px;">
										Shopping cart: 
									</div>
								</li>
								<li>
									<div style="padding:0px 0 5px 5px">
										If you are unsure of what you have saved to the cart you can change or edit your order within the cart as you proceed through the order process. 
										It's easy.<br><br>For full terms and conditions of purchase please <a href="/display/terms_purchase" target="_blank">click here</a>.
									</div>
								</li>
							</ul>
						</div>
		  			</div>
					<div style="display: none" id="tracking-pixel"></div>
				<?php }?>
				<?php if($this->Session->read('dashboardidinsession') == 3) { ?>
			<div style="background: none repeat scroll 0 0 #FFFFFF;border-radius: 0 0 6px 6px;color: #000000;font-size: 12px;margin: 0;padding: 5px;">
				<div style=" color: #505050;font-size: 12px;padding: 5px;">
					<ul>
						<li>
							<div style=" color: #505050;font-size: 12px;font-weight: bold;margin: 0;padding: 5px 0 5px 5px;">Congratulations on redeeming your points ! </div>
						</li>
						<li>
							<div style="padding: 5px 0 5px 5px">
								The items you have selected will be processed in the next 48 hours. Once you finalise your order , 
								you will receive an order confirmation email.   So sure that your email is correct in the order details.  
								If this doesn't arrive please check your spam box. Product delivery times depend upon our suppliers, 
								and generally items will be delivered within 21 working days. Deliver fees do not apply to items in the redemption 
								menu unless otherwise specified in your points shopping cart. 
							</div>
						</li>
					</ul>
				</div>
				<div>
					<ul>
						<li>
							<div style=" color: #505050;font-size: 12px;font-weight: bold;margin: 0;padding: 5px 0 5px 5px;">Check your points shopping cart as you go ...</div>
						</li>
						<li>
							<div style="padding: 5px 0 5px 5px;">If you are unsure of what you have purchased or saved to your  cart you can change or edit your order within the cart as you proceed through the order process. It's easy! </div>
						</li>
						<li>
							<div style=" color: #505050;font-size: 12px;font-weight: bold;margin: 0;padding: 5px 0 5px 5px;">
								Needs some help ?
							</div>
						</li>
						<li>	
							<div style="padding:5px 0 5px 5px">
								Feel free to send us an email to support@therewardsteam.com.au or chat live to a consultant via the Support Chat Box . ( If during operating hours )
								<p>For full terms and conditions of the points redemption purchases and program see "Terms and Conditions" in this web site . For specific product details  refer to the product explanations supplied in this web site for that product.</p> 
							</div>
						</li>
					</ul>
				</div>
			</div>
		<?php }?>
			</div>
		</div>
	 </div>
</div>