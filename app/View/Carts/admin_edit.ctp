<?php echo $this->element('tinymce');?>
<?php echo $this->Html->script('jscolor'); ?>
<style type="text/css">
	#content
	{
		border-left:1px solid #cccccc;
		border-right:1px solid #cccccc;
		border-bottom:1px solid #cccccc;
		margin-left:20px !important;
		padding-left:18px !important;
		width:913px;
	}
	.rounded-corners 
	{
		-moz-border-radius: 10px 10px 0px 0px; /* Firefox */
		-webkit-border-radius: 10px 10px 0px 0px; /* Safari, Chrome */
		border-radius: 10px 10px 0px 0px; /* CSS3 */
	}
	.tr 
	{
		height:35px;
	}
	.tr1
	{
		height:85px;
	}
	.admin_button
	{
		height: 26px;
		padding-bottom: 3px;
		border-radius: 5px 5px 5px 5px;
		background-color: #0066cc;
		border: 1px solid #cccccc;
		color: #FFFFFF; 
		cursor: pointer;
		font-size:13px;
	}
</style>

<table style="margin-left:-2px;margin-top: 10px;">
	<tr>
		<td>
           <img style="width:40px;height:40px;" src="/files/admin_home_icons/products.png" alt="Products" title="Products">&nbsp;&nbsp;
        </td>
        <td> 
            <h2><?php echo "Add Product";?></h2>
        </td>
    </tr>        
</table>

<form action="<?php echo $this->Html->url('/admin/carts/edit/'); ?>" method="post" >		
	<div style="width:949px;">
		
		<div class="tabber" id="mytab1">
			<div class="tabbertab"  style="min-height:300px;overflow:hidden">
	  			<h2><?php echo $this->data['CartProduct']['id']>0?"Edit":"Add";?> Product</h2>
	    		<h4><?php echo $this->data['CartProduct']['id']>0?"Edit":"Add";?> Product</h4>
				<div style="background-color:#EFEFEF;border:1px solid #cccccc;padding:10px;width:790px;overflow:hidden">
					<div style="padding:20px;">
						<table>
							<tr>
								<td style="width: 385px;" valign="top"><?php echo $this->Form->input('CartProduct.id',array('type'=>'hidden')); ?>
										<table>
											<tr class="tr">
												<td style="padding-bottom:10px;width:200px;"><strong>Product Id</strong></td>
												<td><?php echo $this->Form->input('CartProduct.product_id', array('div'=>false, 'label'=>false, 'type'=>'text')); ?>
												<script type="text/javascript">
												$(function() {
													$( "#CartProductProductId" ).autocomplete({
														source: "/admin/carts/xml_products",
														minLength: 2
													});
												});
												</script></td>
											</tr>
											<tr class="tr">
												<td style="width:125px;padding-bottom:10px;"><strong>Price $</strong></td>
												<td><?php echo $this->Form->input('CartProduct.product_price', array('div'=>false, 'label'=>false, "type"=>"text")); ?></td>
											</tr>
											<tr class="tr">
												<td style="width:125px;padding-bottom:10px;"><strong>Qty</strong></td>
												<td>
													<?php echo $this->Form->input('CartProduct.product_quantity', array('div'=>false, 'label'=>false, "type"=>"text")); ?><br/>
												</td>
											</tr>
											<tr class="tr">
												<td style="width:125px;padding-bottom:10px;"><strong>Shipping Cost</strong></td>
												<td>
													<?php if($this->Form->value('CartProduct.product_shipping_type') == 4): ?>
													<?php echo $this->Form->input('CartProduct.product_shipping_cost',array('disabled' => true, 'div'=>false, 'label'=>false, "type"=>"text")); ?><br/>
													<?php else:?>
													<?php echo $this->Form->input('CartProduct.product_shipping_cost', array('div'=>false, 'label'=>false, "type"=>"text")); ?><br/>
													<?php endif;?>
												</td>
											</tr>
											<tr class="tr">
												<td style="width:125px;padding-bottom:10px;"><strong>Shipping Type</strong></td>
												<td>
												    <?php echo $this->Form->input('CartProduct.product_shipping_type',array('type'=>'select','options'=>$stype,'empty'=>false, 'div'=>false, 'label'=>false,'onchange'=>'getShipping(this)'));?>
												</td>
											</tr>
											<tr class="tr">
												<td style="width:125px;padding-bottom:10px;"><strong>SKU Code</strong></td>
												<td><?php echo $this->Form->input('CartProduct.product_sku',array('div'=>false, 'label'=>false, "type"=>"text"));?></td>
											</tr>
											<tr class="tr">
												<td style="width:125px;padding-bottom:10px;"><strong>Cost Ex GST</strong></td>
												<td><?php echo $this->Form->input('CartProduct.product_exgst_cost',array('div'=>false, 'label'=>false, "type"=>"text"));?></td>
											</tr>
											<tr class="tr">
												<td style="width:125px;padding-bottom:10px;"><strong>Cost Inc GST</strong></td>
												<td><?php echo $this->Form->input('CartProduct.product_incgst_cost',array('div'=>false, 'label'=>false, "type"=>"text"));?></td>
											</tr>
											<tr class="tr">
												<td style="width:125px;padding-bottom:10px;"><strong>Active</strong></td>
												<td>
													<?php echo $this->Form->checkbox('CartProduct.active', array('div'=>false, 'label'=>false)); ?><br/>
												</td>
											</tr>
											<tr><td><br><br></td>
											</tr>
											<tr>
												<td colspan="2" valign="center">
													<?php echo $this->Form->button('Cancel', array('onclick' => "location.href='/admin/carts'",'class'=>'admin_button','style'=>'width:60px;', 'type'=>'button'));  ?>
													<button class="admin_button" type="submit" onclick="toggleSpellchecker()">Submit</button>
												</td>
											</tr>
										</table>
								</td>
							</tr>
						</table>
					</div>
				</div>
	 		</div>
	</div>
</form>
<script>
function getShipping(sid)
{
	var shipping_value = $(sid).val();
	if(shipping_value == 4)
	{
		$("#CartProductProductShippingCost").val("");
		$("#CartProductProductShippingCost").attr('disabled','disabled');
	}
	else
	{
		$("#CartProductProductShippingCost").removeAttr('disabled');
	}
}
</script>