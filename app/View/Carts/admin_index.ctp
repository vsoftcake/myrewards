<style type="text/css">
#content
{
	border-left:1px solid #cccccc;
	border-right:1px solid #cccccc;
	border-bottom:1px solid #cccccc;
	margin-left:20px !important;
	padding-left:18px !important;
	width:913px;
}
.rounded-corners 
{
	-moz-border-radius: 10px 10px 0px 0px; /* Firefox */
	-webkit-border-radius: 10px 10px 0px 0px; /* Safari, Chrome */
	border-radius: 10px 10px 0px 0px; /* CSS3 */
}
.tr 
{
	height:45px;
}
.tr1 
{
	height:35px;
}
.admin_button
{
	height: 26px;
	padding-bottom: 2px;
	border-radius: 5px 5px 5px 5px;
	background-color: #0066cc;
	border: 1px solid #cccccc;
	color: #FFFFFF; 
	cursor: pointer;
	font-size:13px;
}
</style>
<script>
function update_order_status(id)
	{
	    var status= document.getElementById("status"+id).value;
		var url = '/carts/update_order_status/'+id+'/'+status;
		new Ajax.Request(url, {
			method: 'get',
			onComplete:function(transport) {
				window.location.reload();
			}
		});
	}
function update_shipping_status(id,productid)
{
	var answer = confirm("Are you sure you want to complete the order")
	if (answer)
	{
	   // var modify= document.getElementById("modify"+id).value;
		var url = '/carts/update_shipping_status/'+id+'/'+productid+'/';
		new Ajax.Request(url, {
				method: 'get',
			onComplete:function(transport) {
				window.location.reload();
			}
		});
	}
}
function send_shipping_status_mail(orderid)
{
	var answer = confirm("Are you sure you want to send status mail");
	if (answer)
	{
	
		var url = '/carts/send_shipping_status_mail/'+orderid;
		new Ajax.Request(url, {
			method: 'get',
			onComplete:function(transport) {
				window.location.reload();
			}
		});
	}
}
</script>

<table style="margin-left:-2px;margin-top: 10px;">
	<tr>
		<td>
           <img style="width:40px;height:40px;" src="/files/styles/icon_images/shop.png""  alt="Shopping Cart" title="Shopping Cart" />&nbsp;&nbsp;
        </td>
        <td> 
            <h2><?php echo "Shopping Cart";?></h2>
        </td>
    </tr>        
</table>
				
<div class="tabber" id="mytab1">
		<div class="tabbertab" style="min-height:210px;overflow:hidden">
	  			<h2>Manage Inventory</h2>
	    		<h4>All Products</h4>	
    			<div style="background-color:#EFEFEF;border:1px solid #cccccc;width:768px;min-height:40px;overflow:hidden;padding:20px">	 
					<div>
						<div class="actions" style="padding-bottom:20px;padding-top:10px">
							<table style="width:760px;background-color:#cccccc;height:75px;border:1px solid #969090;">
								<tr>
									<td>
										<form action="<?php echo $this->Html->url('/admin/carts/'); ?>" method="get" id="SearchForm">
											<table>
												<tr>
													<td style="padding-left:10px;"><strong>Find Products</strong></td>
													<td>
														<?php echo $this->Form->input('Product.search', array('div'=>false, 'label'=>false)); ?>
														<script type="text/javascript">
														$(function() {
															if ($('#ProductSearch').val() == '') {
																$('#ProductSearch').val('ID or name');
																$('#ProductSearch').css( "color", "#aaaaaa" );
															}
															$( "#ProductSearch" ).focus(function() {
																if ($(this).val() == 'ID or name') {
																	$(this).val('');
																	$(this).css( "color", "#000" );
																}
															});
															$( "#SearchForm" ).submit(function() {
																if ($("#ProductSearch").val() == 'ID or name') {
																	$("#ProductSearch").val('');
																	$("#ProductSearch").css( "color", "#000" );
																}
															});
														});
														</script>
													</td>
													<td><button class="admin_button">Search</button></td>
												</tr>
											</table>
										</form>
									</td>
									<td>
										<button class ="admin_button" onclick="location.href='<?php echo $this->Html->url('edit'); ?>';">Add New Product</button>
									</td>
								</tr>
							</table>
						</div>
					</div>
					
					<div style="background-color:#EFEFEF;min-height:40px;overflow:hidden;">	 
					<div class="list" style="width: 99%;">
						<table cellpadding="0" cellspacing="0" width="100%">
							<tr height="30">
								<th style="width:50px;background-color:#cccccc;text-align:left;padding-left:5px;border-left:1px solid #969090;border-bottom:1px solid #969090;border-top:1px solid #969090;">Id</th>
								<th style="width:400px;background-color:#cccccc;text-align:left;padding-left:5px;border-bottom:1px solid #969090;border-top:1px solid #969090;">Name</th>
								<th style="width:100px;background-color:#cccccc;text-align:left;padding-left:5px;border-bottom:1px solid #969090;border-top:1px solid #969090;">Price</th>
								<th style="width:100px;background-color:#cccccc;text-align:left;padding-left:5px;border-bottom:1px solid #969090;border-top:1px solid #969090;">S.Cost</th>
								<th class="actions" style="background-color:#cccccc;text-align:left;padding-left:4px;border-right:1px solid #969090;border-bottom:1px solid #969090;border-top:1px solid #969090;">Actions</th>
							</tr>
							<?php if (empty($searchproducts)) { ?>
							<?php
							$i = 0;
							foreach ($products as $product):
								$class = null;
								if ($i++ % 2 == 0) {
									$class = 'style="background-color: white;"';
								}
							?>
							<?php //if ($product['CartProduct']['product_type'] == 'Pay') { ?>
							    <tr<?php echo $class;?>>
									<td>
										<?php echo $product['Product']['id']; ?>
									</td>
									<td>
										<?php echo $product['Product']['name']; ?>
									</td>
									<td>
										<?php echo "$ ".$product['CartProduct']['product_price']; ?>
									</td>
									<td>
										<?php echo "$ ".$product['CartProduct']['product_shipping_cost']; ?>
									</td>
									<td class="actions">
										<?php echo $this->Html->link(__('Edit', true), array('action'=>'edit', $product['CartProduct']['id'])); ?> |
										<?php echo $this->Html->link(__('Delete', true), array('action'=>'delete', $product['CartProduct']['id']), null, sprintf(__('Are you sure you want to delete product \'%s\'?', true), $product['Product']['name'])); ?>
									</td>
								</tr>
								<?php //} ?>
							
						<?php endforeach; ?>
						<tr class=".tr1"><td></td></tr>
						<tr><td colspan="5">
						<!-- pagination -->
						<div class="paging" style="padding-top:7px;float:right">
							<table style="border:none !important;">
								<tr>
									<td style="border:none !important;" valign="top">
										<?php echo $this->Paginator->first('<<', array('title'=>'first','class'=>'disabled cpagination1'));?>
										<?php echo $this->Paginator->prev('<', array('title'=>'prev','class'=>'disabled cpagination1'));?>
										<?php echo $this->Paginator->numbers(array('separator' => ' ','class'=>'cpagination1'));?>
										<?php echo $this->Paginator->next('>', array('title'=>'next','class'=>'disabled cpagination1'));?>
										<?php echo $this->Paginator->last('>>', array('title'=>'last','class'=>'disabled cpagination1'));?>
									</td>
									<td style="border:none !important;padding-top:3px;">
									<select name="page" id="selectpage" style="margin-top:-5px;">
										<?php for ($i = 1; $i <= $this->Paginator->counter(array('format' => '%pages%')); $i++):?>
										<option value="<?php echo $i;?>"<?php echo ($this->Paginator->current($params['page']) == $i) ? ' selected="selected"' : ''?>><?php echo $i ?></option>
										<?php endfor ?>
									</select>
									<script>
									$("#selectpage").change(function() {
										var url = window.location.href;
										var n = url.indexOf("/?");
										var extra = "";
										if(n>0){extra = url.substring(n,url.length);}
										window.location.href = '/admin/carts/index/page:'+$(this).val()+extra;
									});
									</script>
									</td>
								</tr>
							</table>
						</div>
						</td></tr>
						
						<?php } else {
							foreach($searchproducts as $searchproduct) {?>
							<tr>
								<td>
									<?php echo $searchproduct['Product']['id'] ?>
								</td>
								<td>
									<?php echo $searchproduct['Product']['name'] ?>
								</td>
								<td>
									<?php echo "$ ".$searchproduct['CartProduct']['product_price']; ?>
								</td>
								<td>
									<?php echo "$ ".$searchproduct['CartProduct']['product_shipping_cost']; ?>
								</td>
								<td class="actions">
									<?php echo $this->Html->link(__('Edit', true), array('action'=>'edit', $searchproduct['CartProduct']['id'])); ?> |
									<?php echo $this->Html->link(__('Delete', true), array('action'=>'delete', $searchproduct['CartProduct']['id']), null, sprintf(__('Are you sure you want to delete product \'%s\'?', true), $searchproducts['Product']['name'])); ?>
								</td>
							</tr>
							
						<?php }} ?>
							<tr>
								<td>
									<!-- pagination -->
									<div class="paging" style="padding-top:7px;float:right">
										<table style="border:none !important;">
											<tr>
												<td style="border:none !important;" valign="top">
													<?php echo $this->Paginator->first('<<', array('title'=>'first','class'=>'disabled cpagination1'));?>
													<?php echo $this->Paginator->prev('<', array('title'=>'prev','class'=>'disabled cpagination1'));?>
													<?php echo $this->Paginator->numbers(array('separator' => ' ','class'=>'cpagination1'));?>
													<?php echo $this->Paginator->next('>', array('title'=>'next','class'=>'disabled cpagination1'));?>
													<?php echo $this->Paginator->last('>>', array('title'=>'last','class'=>'disabled cpagination1'));?>
												</td>
												<td style="border:none !important;padding-top:3px;">
												<select name="page" id="selectpage" style="margin-top:-5px;">
													<?php for ($i = 1; $i <= $this->Paginator->counter(array('format' => '%pages%')); $i++):?>
													<option value="<?php echo $i;?>"<?php echo ($this->Paginator->current($params['page']) == $i) ? ' selected="selected"' : ''?>><?php echo $i ?></option>
													<?php endfor ?>
												</select>
												<script>
												$("#selectpage").change(function() {
													var url = window.location.href;
													var n = url.indexOf("/?");
													var extra = "";
													if(n>0){extra = url.substring(n,url.length);}
													window.location.href = '/admin/carts/index/page:'+$(this).val()+extra;
												});
												</script>
												</td>
											</tr>
										</table>
									</div>
								</td>
							</tr>
						</table>
						
						
	  
						</div>
					</div>
			
				</div>
		</div>
		
		<div class="tabbertab" style="min-height:210px;overflow:hidden">
	  			<h2>Manage Orders</h2>
	  			<div class="tabber" id="mytab1">
					<div class="tabbertab" style="min-height:210px;overflow:hidden">
				  			<h2>New Orders</h2>
				    		<h4>New Orders</h4>		 
								<div style="background-color:#EFEFEF;border:1px solid #cccccc;min-height:120px;overflow:hidden;width:90%">
									<div class="list" style="overflow: hidden; float: left; width: 100%;">
										<table cellpadding="0" cellspacing="0" width="100%">
											<tr height="30">
												<th style="background-color:#cccccc;text-align:left;padding-left:5px;border-left:1px solid #969090;border-bottom:1px solid #969090;border-top:1px solid #969090;"><?php echo 'Order Id';?></th>
												<th style="background-color:#cccccc;text-align:left;padding-left:5px;border-bottom:1px solid #969090;border-top:1px solid #969090;">User Id</th>
												<th style="background-color:#cccccc;text-align:left;padding-left:5px;border-bottom:1px solid #969090;border-top:1px solid #969090;">Product Id</th>
												<th style="background-color:#cccccc;text-align:left;padding-left:5px;border-bottom:1px solid #969090;border-top:1px solid #969090;">Product Name</th>
												<th style="background-color:#cccccc;text-align:left;padding-left:5px;border-bottom:1px solid #969090;border-top:1px solid #969090;">Order Date</th>
												<th class="actions" style="background-color:#cccccc;text-align:left;padding-left:4px;border-right:1px solid #969090;border-bottom:1px solid #969090;border-top:1px solid #969090;"><?php echo "Actions";?></th>
											</tr>
											<?php
											$i = 0;
											foreach ($carts as $cart):
												$class = null;
												if ($i++ % 2 == 0) {
													$class = ' style="background-color: white;"';
												}
											?>
											<tr<?php echo $class;?>>
												<td >
													<?php echo $cart['CartItem']['order_id'] ?>
												</td>
												<td >
													<?php echo $cart['Cart']['user_id'] ?>
												</td>
												<td >
													<?php echo $cart['CartItem']['product_id'] ?>
												</td>
												<td >
													<?php echo $cart['Product']['name'] ?>
												</td>
												<td >
												   <?php echo $cart['CartItem']['created'] ?>
												</td>
												<td class="actions">
													<a href="/carts/update_order_status/<?php echo $cart['CartItem']['id'];?>/<?php echo $cart['CartItem']['product_id'];?>">Update</a> | &nbsp;
													<a href="/carts/order_show/<?php echo $cart['CartItem']['id'];?>/<?php echo $cart['CartItem']['product_id'];?>">View</a> | &nbsp;
													<a href="/carts/delete/<?php echo $cart['CartItem']['id'];?>" onclick="if (confirm('Are you sure you want to delete?')) { return true; } return false;">Delete</a>
												</td>
											</tr>
										<?php endforeach; ?>
										</table>
									</div> 
								</div>
					</div>
				
				<div class="tabbertab" style="min-height:210px;overflow:hidden">
			  			<h2>Shipped Orders</h2>
			  			<h4>Shipped Orders</h4>
			    			<div style="background-color:#EFEFEF;border:1px solid #cccccc;min-height:120px;overflow:hidden;width:90%">
							<div class="list" style="overflow: hidden; float: left; width: 100%; min-height: 205px;">
								<table cellpadding="0" cellspacing="0" width="100%">
									<tr height="30">
										<th style="background-color:#cccccc;text-align:left;padding-left:5px;border-left:1px solid #969090;border-bottom:1px solid #969090;border-top:1px solid #969090;"><?php echo 'Order Id';?></th>
										<th style="background-color:#cccccc;text-align:left;padding-left:5px;border-bottom:1px solid #969090;border-top:1px solid #969090;"><?php echo'User Id';?></th>
										<th style="background-color:#cccccc;text-align:left;padding-left:5px;border-bottom:1px solid #969090;border-top:1px solid #969090;"><?php echo 'Product Id';?></th>
										<th style="background-color:#cccccc;text-align:left;padding-left:5px;border-bottom:1px solid #969090;border-top:1px solid #969090;"><?php echo 'Product Name';?></th>
										<th style="background-color:#cccccc;text-align:left;padding-left:5px;border-bottom:1px solid #969090;border-top:1px solid #969090;">Shipped Date</th>
										<th style="background-color:#cccccc;text-align:left;padding-left:5px;border-bottom:1px solid #969090;border-top:1px solid #969090;">Change Status</th>
										<th class="actions" style="background-color:#cccccc;text-align:left;padding-left:4px;border-right:1px solid #969090;border-bottom:1px solid #969090;border-top:1px solid #969090;"><?php echo "Actions";?></th>
									</tr>
									<?php
									$i = 0;
									foreach ($shipped_cart as $shipped_cart):
										$class = null;
										if ($i++ % 2 == 0) {
											$class = ' style="background-color: white;"';
										}
									?>
									<tr<?php echo $class;?>>
										<td >
											<?php echo $shipped_cart['CartItem']['order_id'] ?>
										</td >
										<td>
											<?php echo $shipped_cart['Cart']['user_id'] ?>
										</td >
										<td >
											<?php echo $shipped_cart['CartItem']['product_id'] ?>
										</td>
										<td >
											<?php echo $shipped_cart['Product']['name'] ?>
										</td>
										<td >
											<?php echo $shipped_cart['CartItem']['shipping_date'] ?>
										</td>
										<td >
										   
										<a href="#update" onclick="update_shipping_status(<?php echo $shipped_cart['CartItem']['id'];?>,<?php echo $shipped_cart['CartItem']['product_id'];?>)">Completed</a>
										</td>
										<td class="actions">
											<a href="/carts/order_show/<?php echo $shipped_cart['CartItem']['id'];?>/<?php echo $shipped_cart['CartItem']['product_id'];?>">View</a> &nbsp;&nbsp;
										</td>
									</tr>
								<?php endforeach; ?>
								</table>
							</div>
					   	</div>
				</div>
				
				<div class="tabbertab" style="min-height:210px;overflow:hidden">
					<h2>Send Mail For Shipped Orders</h2>
					<h4>Send Mail For Shipped Orders</h4>
					<div style="background-color:#EFEFEF;border:1px solid #cccccc;min-height:120px;overflow:hidden;width:90%">
						<div class="list" style="overflow: hidden; float: left; width: 100%; min-height: 205px;">
							<table cellpadding="0" cellspacing="0" width="100%">
								<tr height="30">
									<th style="background-color:#cccccc;text-align:left;padding-left:5px;border-left:1px solid #969090;border-bottom:1px solid #969090;border-top:1px solid #969090;"><?php echo 'Order Id';?></th>
									<th style="background-color:#cccccc;text-align:left;padding-left:4px;border-right:1px solid #969090;border-bottom:1px solid #969090;border-top:1px solid #969090;"></th>
								</tr>

								<?php
								foreach ($total_count as $shipped_cart):
									foreach($shipped_total_count as $cart):
									//echo "shipped--orderid=".$cart['CartItem']['order_id']."--count=".$cart[0]['count1']."####Total--orderid=".$shipped_cart['CartItem']['order_id']."--count=".$shipped_cart[0][count2]."<br/><br/>";
									if($cart['CartItem']['order_id'] == $shipped_cart['CartItem']['order_id'] && $cart[0]['count1'] == $shipped_cart[0][count2])
									{
										$class = null;
										if ($i++ % 2 == 0) {
											$class = ' style="background-color: white;"';
										}
									?>
									<tr<?php echo $class;?>>
									<td >
										<?php echo $shipped_cart['CartItem']['order_id'] ?>
									</td >

									<td class="actions">
										<a href="javascript:void(0);" onclick="send_shipping_status_mail(<?php echo $shipped_cart['CartItem']['order_id'];?>)">Send mail for order <?php echo $shipped_cart['CartItem']['order_id'];?></a>
									</td>
								</tr>
								<?php }endforeach;endforeach; ?>
							</table>
						</div>
					</div>
				</div>
				
				<div class="tabbertab" style="min-height:210px;overflow:hidden">
	  			<h2>Completed Orders</h2>
	    		<h4>Completed Orders</h4>	
    			<div style="background-color:#EFEFEF;border:1px solid #cccccc;width:768px;min-height:40px;overflow:hidden;width:90%">	 
					<div class="list" style="overflow: hidden; float: left; width: 100%;">
					<table cellpadding="0" cellspacing="0" width="100%">
							<tr height="30">
								<th style="background-color:#cccccc;text-align:left;padding-left:5px;border-left:1px solid #969090;border-bottom:1px solid #969090;border-top:1px solid #969090;"><?php echo 'Order Id';?></th>
								<th style="background-color:#cccccc;text-align:left;padding-left:5px;border-bottom:1px solid #969090;border-top:1px solid #969090;"><?php echo'User Id';?></th>
								<th style="background-color:#cccccc;text-align:left;padding-left:5px;border-bottom:1px solid #969090;border-top:1px solid #969090;"><?php echo 'Product Id';?></th>
								<th style="background-color:#cccccc;text-align:left;padding-left:5px;border-bottom:1px solid #969090;border-top:1px solid #969090;"><?php echo 'Product Name';?></th>
								<th style="background-color:#cccccc;text-align:left;padding-left:5px;border-bottom:1px solid #969090;border-top:1px solid #969090;"><?php echo 'Status';?></th>
								<th style="background-color:#cccccc;text-align:left;padding-left:5px;border-bottom:1px solid #969090;border-top:1px solid #969090;"><?php echo 'Completed Date';?></th>
								<th class="actions" style="background-color:#cccccc;text-align:left;padding-left:4px;border-right:1px solid #969090;border-bottom:1px solid #969090;border-top:1px solid #969090;"><?php echo "Actions";?></th>
							</tr>
							<?php
							$i = 0;
							foreach ($complted_cart as $complted_cart):
								$class = null;
								if ($i++ % 2 == 0) {
									$class = ' style="background-color: white;"';
								}
							?>
							<tr<?php echo $class;?>>
								<td >
									<?php echo $complted_cart['CartItem']['order_id'] ?>
								</td >
								<td>
									<?php echo $complted_cart['Cart']['user_id'] ?>
								</td >
								<td >
									<?php echo $complted_cart['CartItem']['product_id'] ?>
								</td>
								<td >
									<?php echo $complted_cart['Product']['name'] ?>
								</td>
								<td >
									<?php echo $complted_cart['CartItem']['cart_status'] ?>
								</td>
								<td>
									<?php echo $complted_cart['CartItem']['completed_date'] ?>
								</td>
								<td class="actions">
									<a href="/carts/order_show/<?php echo $complted_cart['CartItem']['id'];?>/<?php echo $complted_cart['CartItem']['product_id'];?>">View</a> &nbsp;&nbsp;
								</td>
							</tr>
						<?php endforeach; ?>
						</table>
						</div>
				</div>
			</div>
				
		</div>
	    		
		</div>
</div>