<style>
.cart-info table {
    border-collapse: collapse;
    border-left: 1px solid #DDDDDD;
    border-right: 1px solid #DDDDDD;
    border-top: 1px solid #DDDDDD;
    margin-bottom: 15px;
    margin:10px 10px 10px 10px;
    width:97%;
}
.cart-info .headrow {
    background-color: #F7F7F7;
    border-bottom: 1px solid #DDDDDD;
    font-weight: bold;
    height:30px;
}
.cart-info .row {
    border-bottom: 1px solid #DDDDDD;
    font-weight: bold;
}
.cart-info td {
    padding: 4px;
}
.online_request_border
	{
		border:1px solid;
	}
.order-shipping-address-outer {
    background: none repeat scroll 0 0 #E7E5E5;
    border-top: 1px dotted #252525;
    margin: 0 1px;
    overflow: hidden;
    padding: 10px;
}
.order-shipping-address-edit {
    color: #242424;
    float: left;
    font-size: 11px;
    overflow: hidden;
    width: 445px;
}
.customer-information {
    float: left;
    width: 229px;
}
.customer-heading-text {
    color: #242424;
    font-size: 12px;
    font-weight: bold;
    padding-bottom: 10px;
   /* text-align: center; */
}
.order-shipping-address-edit {
    color: #242424;
    font-size: 11px;
}

.customer-address {
    border-left: 1px solid #ABABAB;
    float: left;
    min-height: 98px;
    padding: 0 0 0 10px;
    width: 205px;
}
.tabs-outer {
    margin: 0 10px 25px;
    overflow: hidden;
    width: 703px;
}
.tab-active {
    color: #17B0DC;
    font-size: 13px;
    font-weight: bold;
}
.tabs-order {
    background: url("/files/styles/icon_images/shop.png") no-repeat scroll center center transparent;
    height: 36px;
    margin-top:20px;
}
.shipping {
    background: url("/files/styles/icon_images/shipping.jpg") no-repeat scroll center center transparent;
    height: 36px;
    margin-top:20px;
}
.payment {
    background: url("/files/styles/icon_images/pay.jpg") no-repeat scroll center center transparent;
    height: 36px;
    margin-top:20px;
}
.order {
    background: url("/files/styles/icon_images/summary.jpg") no-repeat scroll center center transparent;
    height: 36px;
    margin-top:20px;
}
.tabs-cont {
    color: #666666;
    float: left;
    font-size: 12px;
    font-weight: normal;
    width: 150px;
}

.address {
    float: left;
    font-size: 12px;
    padding: 10px;
}
.customer-info {
    float: left;
    overflow: hidden;
    padding-bottom: 25px;
    width: 380px;
}
</style>
<script>
function checkValidation()
	{
	    cardnumber = document.getElementById('cardnumber').value;
	   	cvv = document.getElementById('cvv').value;
		
		if (cardnumber == null || cardnumber =='')
   		{
    		alert("Please enter cardnumber");
    		return false;
    	}
		
  		if (cvv == null || cvv =='')
   		{
    		alert("Please enter cvv");
    		return false;
    	}
    return true;	
	}
function PaymentMode(rad)
{
	var radss=document.getElementsByName(rad.name);
	document.getElementById('paypal').style.display=(radss[0].checked)?'block':'none';
	//document.getElementById('credit').style.display=(radss[1].checked)?'block':'none';
	document.getElementById('points').style.display=(radss[1].checked)?'block':'none';
}
</script>
<?php echo $this->element('module2', array('module2' => 'Keyword_search', 'title' => 'Search'));?>
<div class="online_request_border"  style="width:950px;margin-bottom:0px !important;min-height:10px;overflow:hidden;">
	<div style="background: none repeat scroll 0 0 #FFFFFF;border: 1px solid #AEAEAE;border-radius: 10px 10px 10px 10px;box-shadow: 0 0 2px #828282;float: left;overflow: hidden;margin-bottom: 20px;margin-top:25px;margin-left:10px;width:650px;">

		<div class="tabs-outer">
			<div class="tabs-cont">
				<a href="/carts/show_cart">
					<div class="tabs-order"></div>
					<div align="center" class="tab-active">Order Details</div>
				</a>
			</div>
			
			<div class="tabs-cont">
			<a href="/carts/check_out">
				<div class="shipping"></div>
				<div align="center" class="tab-active">Shipping Details</div>
			</a>
			</div>
		
			<div class="tabs-cont">
				<form action="<?php echo $this->Html->url('/carts/order_summary/'.$cartlist[0]['Cart']['id']); ?>" method="post" name="myform">
					<?php echo $this->Form->input( 'CartAddress.status', array( 'type' => 'hidden','value'=>'order','label'=>false) );  ?>
					<a href="javascript:voide(0)" onclick="document.myform.submit()">
						<div class="order"></div>
						<div align="center" class="tab-active">Order Summary</div>
					</a>
				</form>
			</div>
		
			<div class="tabs-cont">
				<div class="payment"></div>
				<div align="center"   class="tab-active">Payment</div>
			</div>
		</div>
				
				<table>
					<tr>
					 <?php if($this->Session->read('dashboardidinsession')==3) :?>
					 <td style="padding:10px;">
						<input name="selection" style="width:10px;" type="radio" value="points" checked="checked"/><strong>Pay with Points</strong>
					 </td>
					<?php endif; ?>
					</tr>
				</table>

				<?php if($this->Session->read('dashboardidinsession')==1) :?>
				<?php 
					if($this->Session->read('paypaldetails.PaypalDetail.paypal_business_name') == ''):
						$business_name = 'joanne@myrewards.com.au';
					else:
						$business_name = $this->Session->read('paypaldetails.PaypalDetail.paypal_business_name');
					endif;
					
					if($this->Session->read('paypaldetails.PaypalDetail.paypal_currency_code') == ''):
						$currency_code = 'AUD';
					else:
						$currency_code = $this->Session->read('paypaldetails.PaypalDetail.paypal_currency_code');
					endif;
				?>
				<div id="paypal" style="display:block;padding:10px">
					<form action="https://www.paypal.com/cgi-bin/webscr" method="post" name="frmPaypal" id="frmPaypal">
					    <input type="hidden" name="business" value="<?php echo $business_name;?>">
					    <input type="hidden" name="cmd" value="_cart">
					    <input type="hidden" name="upload" value="1">
					    <input type="hidden" name="custom" value="<?php echo $cartid;?>">
					    <input type="hidden" name="address_override" value="1">
					    <input type="hidden" name="currency_code" value="<?php echo $currency_code;?>">
					    <?php
							$i=1;$shipping_count=0;
							if(count($cartlist)>1 )
							{  
								foreach($cartlist as $details){
									$qty = $details['CartItem']['product_qty'];
									$price = $details['CartProduct']['product_price'];
									if($details['CartProduct']['product_shipping_type']==1) {
										$total = (($qty * $price) + $details['CartProduct']['product_shipping_cost']);
									}
									if($details['CartProduct']['product_shipping_type']==2) {
										$total = ($qty * ($price + $details['CartProduct']['product_shipping_cost']));
									}
									if($details['CartProduct']['product_shipping_type']==3)
									{
										if($shipping_count != $details['CartProduct']['product_shipping_type'])
											$total = (($qty * $price) + $max_group_ship_cost);
										else
											$total = ($qty * $price);
									}
									if($details['CartProduct']['product_shipping_type']==4) {
										$total = $qty * $price;
									}
									$grandtotal += $total;
									echo'<input type="hidden" name="item_name_'.$i.'" value="'.$details['Product']['name'].'">
									<input type="hidden" name="item_number_'.$i.'" value="'.$details['CartProduct']['product_id'].'">
									<input type="hidden" name="quantity_'.$i.'" value="'.$qty.'">
									<input type="hidden" name="amount_'.$i.'" value="'.$price.'">';
									if (($details['CartProduct']['product_shipping_type']==1))
									{
										echo '<input type="hidden" name="shipping_'.$i.'" value="'.$details['CartProduct']['product_shipping_cost'].'">';
									}
									if (($details['CartProduct']['product_shipping_type']==2))
									{
										echo '<input type="hidden" name="shipping_'.$i.'" value="'.($details['CartProduct']['product_shipping_cost']*$details['CartItem']['product_qty']).'">';
									}	
									if (($details['CartProduct']['product_shipping_type']==3))
									{
										if($shipping_count != $details['CartProduct']['product_shipping_type'])
											echo '<input type="hidden" name="shipping_'.$i.'" value="'.$max_group_ship_cost.'">';
										else
											echo '<input type="hidden" name="shipping_'.$i.'" value="">';

									}
									if (($details['CartProduct']['product_shipping_type']==4))
									{
										if($shipping_count != $details['CartProduct']['product_shipping_type'])
											echo '<input type="hidden" name="shipping_'.$i.'" value="'.$counts[0]['CartItem']['smart_shipping_cost'].'">';
										else
											echo '<input type="hidden" name="shipping_'.$i.'" value="0">';
									}
									$i++;$shipping_count = $details['CartProduct']['product_shipping_type'];
								}
							}
							else
							{
								echo'<input type="hidden" name="item_name_1" value="'.$cartlist[0]['Product']['name'].'">
								<input type="hidden" name="item_number_1" value="'.$cartlist[0]['CartProduct']['product_id'].'">
								<input type="hidden" name="quantity_1" value="'.$cartlist[0]['CartItem']['product_qty'].'">
								<input type="hidden" name="amount_1" value="'.$cartlist[0]['CartProduct']['product_price'].'">';
								if ($cartlist[0]['CartProduct']['product_shipping_type']==1)
								{
									echo '<input type="hidden" name="shipping_1" value="'.($cartlist[0]['CartProduct']['product_shipping_cost']).'">';
								}
								if ($cartlist[0]['CartProduct']['product_shipping_type']==2)
								{
									echo '<input type="hidden" name="shipping_1" value="'.($cartlist[0]['CartProduct']['product_shipping_cost']*$cartlist[0]['CartItem']['product_qty']).'">';
								}
								if ($cartlist[0]['CartProduct']['product_shipping_type']==3)
								{
									echo '<input type="hidden" name="shipping_1" value="'.($cartlist[0]['CartProduct']['product_shipping_cost']).'">';
								}
								if ($cartlist[0]['CartProduct']['product_shipping_type']==4)
								{
									echo '<input type="hidden" name="shipping_1" value="'.$counts[0]['CartItem']['smart_shipping_cost'].'">';
								}
							}
						?>
						<div style="float:left;padding-left:10px">
							<div style="padding: 0 0 2px 0;" class="customer-heading-text heading-text-left">Shipping Address</div>
							<div style="width: 160px;" class="address-detail">
								<?php echo $useraddress[0]['CartAddress']['username'] ?><br>
								<?php echo $useraddress[0]['CartAddress']['member_name'] ?><br>
								<?php echo $useraddress[0]['CartAddress']['address1'] ?><br>
								<?php echo $useraddress[0]['CartAddress']['city'] ?><br>
								<?php echo $useraddress[0]['CartAddress']['state'] ?><br>
								<?php echo $useraddress[0]['CartAddress']['country'] ?><br>
								<?php echo $useraddress[0]['CartAddress']['zipcode'] ?><br>
							</div>
						</div>
						<?php if(!empty($grandtotal)){
							if(!empty($counts[0]['CartItem']['smart_shipping_cost']))
								$grandtotal += $counts[0]['CartItem']['smart_shipping_cost'];
						?>
				      	<div id="grandtotal" align="right" style="margin-right:20px;"><?php echo "Grand Total: $". number_format($grandtotal,2); ?></div><?php }?>
						<input type="hidden" name="notify_url" value="<?php echo "http://".$this->Session->read('client.Domain.name')."/carts/invoice/".$this->Session->read('user.User.id')?>" />
						<input type="hidden" name="return" value="<?php echo "http://".$this->Session->read('client.Domain.name')."/carts/success/".$cartid."/".$this->Session->read('user.User.id')?>" />
						<div align="right" style="margin:10px 20px 0 0;">
						<button type="submit" class="quick-search_button_style rounded_corners_button" style="width:100px" >Pay Now</button>
						</div>
					</form>
				</div>
				<?php endif; ?>
				
				<div id="credit" style="display:none;min-height:50px;overflow:hidden">
					<form id="formidExisting" action="/carts/success/<?php echo $cartid; ?>" method="post" onSubmit="return checkValidation();">
						<table style="width:635px;margin:10px 0px 0px 20px">
							<tr class="tr">
								<td style="width:250;vertical-align:top; padding:10px;"><strong>Credit Card Number<span class="mandatory">*</span></strong></td>
								<td style="vertical-align:top; padding:10px;"><input type="text" class="input" name="cardnumber" id="cardnumber" value="" /></td>
							</tr>
							<tr class="tr">
									<td style="vertical-align:top; padding:10px;"><strong>Expiry Date<span class="mandatory">*</span></strong></td>
									<td style="vertical-align:top; padding:10px;">
									<select name="expyear" class="input" id="expyear">
									<?php for($i=0;$i<10;$i++){
									    $year = date('Y')+$i;
										echo '<option value="'.$year.'">'.$year.'</option>';
									} ?>
									</select>&nbsp;
									-&nbsp;<select name="expmonth" class="input" id="expmonth" onchange="monthvalid()">
										<option value="01">January</option>
										<option value="02">February</option>
										<option value="03">March</option>
										<option value="04">April</option>
										<option value="05">May</option>
										<option value="06">June</option>
										<option value="07">July</option>
										<option value="08">August</option>
										<option value="09">September</option>
										<option value="10">October</option>
										<option value="11">November</option>
										<option value="12">December</option>
									</select>
									</td>
							</tr>
							<tr class="tr">
								<td style="width:250;vertical-align:top; padding:10px;"><strong>CVV Code<span class="mandatory">*</span></strong></td>
								<td style="vertical-align:top; padding:10px;"><input type="text" class="input" name="cvv" id="cvv" value="" maxlength=3 /></td>
							</tr>
							<tr><td><input type="hidden" name="cartid" value="<?php echo $cartid; ?>"><input type="hidden" name="amount" value="<?php echo $total; ?>"></td></tr>
						</table>
							<div align="right" style="margin:10px 20px 0 0;">
								<button type="submit" class="quick-search_button_style rounded_corners_button" style="width:100px" >Pay Credit</button><br><br>
							</div>
						</form>
				</div>
				
				<?php if($this->Session->read('dashboardidinsession')==3) :?>
				<div id="points" style="display:block;padding:10px">
				   <?php  if($pointslist['0']['0']['total']>0) { ?>
				   <?php if( ($total_user_points >= $pointslist['0']['0']['total']) ) { ?>
				   	<div style="padding:0px 0px 0px 20px;font-weight:bold;">
						<?php echo $pointslist['0']['0']['total']. " points will be redeemed from your available points ".$total_user_points ;?>
					</div>
					<div align="right" style="margin:10px 20px 0 0;">
						<form action="/carts/redeem_points/" method="post">
						
							<input type="hidden" name="cartid" value="<?php echo $cartid;?>">
							<input type="hidden" name="points" value="<?php echo $pointslist['0']['0']['total']; ?>"> 
							<button type="submit" class="quick-search_button_style rounded_corners_button" style="width:150px" > Redeem Order</button><br><br>
						</form>
					</div>
					<?php } else { 
					echo "<span style='padding-left:18px'><strong>You have no suficiant points to buy this order.</strong></span>"; } ?>
					<?php } else { 
					echo "<span style='padding-left:18px'><strong>There are no points products in your cart.</strong></span>"; } ?>
					 
				</div>
				<?php endif; ?>
				
	</div>
	
	<div style="float: right;overflow: hidden;width: 275px;margin-top: 25px;padding-right: 5px;">
	<div style="background: none repeat scroll 0 0 #C0C2C3;border-radius: 8px 8px 8px 8px;margin-top: 0;padding: 3px;">
		<?php if($this->Session->read('dashboardidinsession') == 1) { ?>
			<label style="color: #272727;display: block;font-size: 16px;font-weight: bold;padding: 9px;">Payment and Shipping</label>
			<div style="background: none repeat scroll 0 0 #FFFFFF;border-radius: 0 0 6px 6px;color: #000000;font-size: 12px;margin: 0;padding: 5px;">
				<div style=" color: #505050;font-size: 12px;padding: 5px;">
					<ul>
						<li>
							<div style=" color: #505050;font-size: 12px;font-weight: bold;margin: 0;padding: 5px 0 5px 5px;">How can I pay?</div>
						</li>
						<li>
							<div style=" color: #505050;font-size: 12px;font-weight: bold;margin: 0;padding: 5px 0 5px 5px;">You can pay with Visa or MasterCard credit cards.</div>
						</li>
						<li>
							<div style="padding: 5px 0 5px 5px">
								We use the PayPal payment system to take and process your payments which ensures that your payments are within a world class payment gateway. All the information is processed using SSL data encryption (secure encrypted connection), which protects the information from being viewed by anyone.
							</div>
						</li>
					</ul>
				</div>
				<div>
					<ul>
						<li>
							<div style=" color: #505050;font-size: 12px;font-weight: bold;margin: 0;padding: 5px 0 5px 5px;">How does the postage and handling work?</div>
						</li>
						<li>
							<div style="padding: 5px 0 5px 5px;">The level and type of postage and handling is determined by the product or service that you purchase. Most products will attract smart shipping rates. </div>
						</li>
						<li style="padding-left:15px">
							<div style=" color: #505050;font-size: 12px;font-weight: bold;margin: 0;padding: 5px 0 5px 5px;">Ordinary mail- Not recommended:-</div>
						
							<div style="padding:5px 0 5px 5px">Ordinary mail is the cheapest option however does not have any insurance and we are unable to track it through the postal system .If you choose this method and your order is lost, the supplier will not be responsible and no refund will apply.</div>
						
							<div style=" color: #505050;font-size: 12px;font-weight: bold;margin: 0;padding: 5px 0 5px 5px;">Registered Mail - Recommended </div>
						
							<div style="padding:5px 0 5px 5px">We recommend that you choose Registered mail for all orders as it gives a level of protection and we can track your order.</div>
						</li>
						<li>
							<div style=" color: #505050;font-size: 12px;font-weight: bold;margin: 0;padding: 5px 0 5px 5px;">
								Variable postage and handling rates:
							</div>
						</li>
						<li>	
							<div style="padding:5px 0 5px 5px">
								Variable Postage and Handling rates will apply to certain products. These rates will be shown in cart and the product as you make the order 
							</div>
						</li>
						<li>
							<div style=" color: #505050;font-size: 12px;font-weight: bold;margin: 0;padding: 5px 0 5px 5px;">
								Shopping cart: 
							</div>
						</li>
						<li>
							<div style="padding:0px 0 5px 5px">
								If you are unsure of what you have saved to the cart you can change or edit your order within the cart as you proceed through the order process. 
								It's easy.<br><br>For full terms and conditions of purchase please <a href="/display/terms_purchase" target="_blank">click here</a>.
							</div>
						</li>
					</ul>
				</div>
			</div>
			<div style="display: none" id="tracking-pixel"></div>
		<?php }?>
		<?php if($this->Session->read('dashboardidinsession') == 3) { ?>
			<div style="background: none repeat scroll 0 0 #FFFFFF;border-radius: 0 0 6px 6px;color: #000000;font-size: 12px;margin: 0;padding: 5px;">
				<div style=" color: #505050;font-size: 12px;padding: 5px;">
					<ul>
						<li>
							<div style=" color: #505050;font-size: 12px;font-weight: bold;margin: 0;padding: 5px 0 5px 5px;">Congratulations on redeeming your points ! </div>
						</li>
						<li>
							<div style="padding: 5px 0 5px 5px">
								The items you have selected will be processed in the next 48 hours. Once you finalise your order , 
								you will receive an order confirmation email.   So sure that your email is correct in the order details.  
								If this doesn't arrive please check your spam box. Product delivery times depend upon our suppliers, 
								and generally items will be delivered within 21 working days. Deliver fees do not apply to items in the redemption 
								menu unless otherwise specified in your points shopping cart. 
							</div>
						</li>
					</ul>
				</div>
				<div>
					<ul>
						<li>
							<div style=" color: #505050;font-size: 12px;font-weight: bold;margin: 0;padding: 5px 0 5px 5px;">Check your points shopping cart as you go ...</div>
						</li>
						<li>
							<div style="padding: 5px 0 5px 5px;">If you are unsure of what you have purchased or saved to your  cart you can change or edit your order within the cart as you proceed through the order process. It's easy! </div>
						</li>
						<li>
							<div style=" color: #505050;font-size: 12px;font-weight: bold;margin: 0;padding: 5px 0 5px 5px;">
								Needs some help ?
							</div>
						</li>
						<li>	
							<div style="padding:5px 0 5px 5px">
								Feel free to send us an email to support@therewardsteam.com.au or chat live to a consultant via the Support Chat Box . ( If during operating hours )
								<p>For full terms and conditions of the points redemption purchases and program see "Terms and Conditions" in this web site . For specific product details  refer to the product explanations supplied in this web site for that product.</p> 
							</div>
						</li>
					</ul>
				</div>
			</div>
		<?php }?>
	</div>
</div>
</div>
