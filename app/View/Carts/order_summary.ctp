<style>
.cart-info table {
    border-collapse: collapse;
    border-left: 1px solid #DDDDDD;
    border-right: 1px solid #DDDDDD;
    border-top: 1px solid #DDDDDD;
    margin-bottom: 15px;
    margin:10px 10px 10px 10px;
    width:97%;
}

.cart-info .headrow {
    background-color: #F7F7F7;
    border-bottom: 1px solid #DDDDDD;
    font-weight: bold;
    height:30px;
}
.cart-info .row {
    border-bottom: 1px solid #DDDDDD;
    font-weight: bold;
}
.cart-info td {
    padding: 4px;
}
.online_request_border
	{
		border:1px solid;
	}
.order-shipping-address-outer {
    background: none repeat scroll 0 0 #E7E5E5;
    border-top: 1px dotted #252525;
    margin: 0 1px;
    overflow: hidden;
    padding: 10px;
}
.order-shipping-address-edit {
    color: #242424;
    float: left;
    font-size: 11px;
    overflow: hidden;
    width: 445px;
}
.customer-information {
    float: left;
    width: 229px;
}
.customer-heading-text {
    color: #242424;
    font-size: 12px;
    font-weight: bold;
    padding-bottom: 10px;
   /* text-align: center; */
}
.order-shipping-address-edit {
    color: #242424;
    font-size: 11px;
}

.customer-address {
    border-left: 1px solid #ABABAB;
    float: left;
    min-height: 98px;
    padding: 0 0 0 10px;
    width: 205px;
}
.tabs-outer {
    margin: 0 10px 25px;
    overflow: hidden;
    width: 703px;
}
.tab-active {
    color: #17B0DC;
    font-size: 13px;
    font-weight: bold;
}
.tabs-order {
    background: url("/files/styles/icon_images/shop.png") no-repeat scroll center center transparent;
    height: 36px;
    margin-top:20px;
}
.shipping {
    background: url("/files/styles/icon_images/shipping.jpg") no-repeat scroll center center transparent;
    height: 36px;
    margin-top:20px;
}
.payment {
    background: url("/files/styles/icon_images/pay.jpg") no-repeat scroll center center transparent;
    height: 36px;
    margin-top:20px;
}
.order {
    background: url("/files/styles/icon_images/summary.jpg") no-repeat scroll center center transparent;
    height: 36px;
    margin-top:20px;
}
.tabs-cont {
    color: #666666;
    float: left;
    font-size: 12px;
    font-weight: normal;
    width: 150px;
}

.address {
    float: left;
    font-size: 12px;
    padding: 10px;
}
.customer-info {
    float: left;
    overflow: hidden;
    padding-bottom: 25px;
    width: 380px;
}
</style>
<script>
	function update_cart(cartid,stock,id,cartitemid,value,type,max_group_ship_cost)
	{
	
		if(parseInt(stock)>=parseInt(value) || stock==-1){
			$.ajax({
				url: '/carts/update_cart/'+cartid+'/'+id+'/'+cartitemid+'/'+value+'/'+type+'/'+max_group_ship_cost,
				type: "GET",
				success: function (data) {
					$('#updatecart'+id).html(data);
					set_grand_total(max_group_ship_cost);
				}
			});
		}
		else
		{
			alert('We have '+stock+' products only');
		}
		
	}
	
	function set_grand_total(max_group_ship_cost)
	{
		$.ajax({
			url: '/carts/grand_total'+'/'+max_group_ship_cost,
			type: "GET",
			success: function (data) {
				$('#grandtotal').html(data);
			}
		});
	}
	
	function update_cart_points(cartid,stock,id,cartitemid,value)
	{
		if(stock>=value || stock==-1){
			$.ajax({
				url: '/carts/update_cart_points/'+cartid+'/'+id+'/'+cartitemid+'/'+value,
				type: "GET",
				success: function (data) {
					$('#updatecartpoints'+id).html(data);
					set_grand_points();
				}
			});
		}
		else
		{		
			alert('We have '+stock+' products only');
		}
		
	}
	
	function set_grand_points()
	{
		$.ajax({
			url: '/carts/grand_total_points/',
			type: "GET",
			success: function (data) {
				$('#grandpoints').html(data);
			}
		});
	}
	
	function remove_cart(cartid,productid,cartitemid)
	{
		var answer = confirm("Are you sure you want to remove from cart");
		if (answer)
		{
			var url = "/carts/remove_cart/"+cartid+"/"+productid+"/"+cartitemid;
			window.location = url;
		}
	}
	function smart_shipping(cost, max_group_ship_cost)
	{
		var shipcost = cost.value;
		if(shipcost=='')
		{
			alert("Please select your preferred Smart Shipping rate");
		}
		else
		{
			$.ajax({
				url: '/carts/smart_grand_total'+'/'+shipcost+'/'+max_group_ship_cost,
				type: "GET",
				success: function (data) {
					$('#grandtotal').html(data);
				}
			});
		}
	}
	function check_smart_shipping()
	{
		if (document.getElementById('CartItemSmartShippingCost') == null) 
		{
			document.location="/carts/proceed_payment";
		}
		else
		{ 
			var shipcost = document.getElementById('CartItemSmartShippingCost').value;
			if(shipcost == '')
			{
				alert("Please select your preferred Smart Shipping rate");
			}
			else
			{
				document.location="/carts/proceed_payment";
			} 
		}
	}
</script>
<?php echo $this->element('module2', array('module2' => 'Keyword_search', 'title' => 'Search'));?>
<div class="online_request_border"  style="width:950px;margin-bottom:0px !important;min-height:10px;overflow:hidden; padding-bottom: 20px;">
	<div style="background: none repeat scroll 0 0 #FFFFFF;border: 1px solid #AEAEAE;border-radius: 10px 10px 10px 10px;box-shadow: 0 0 2px #828282;float: left;overflow: hidden;margin-top:25px;margin-left:10px;width:650px;">

		<div class="tabs-outer">
			<div class="tabs-cont">
				<a href="/carts/show_cart">
					<div class="tabs-order"></div>
					<div align="center" class="tab-active">Your Cart</div>
				</a>
			</div>
			
			<div class="tabs-cont">
			<a href="/carts/check_out">
				<div class="shipping"></div>
				<div align="center" class="tab-active">Shipping Details</div>
			</a>
			</div>
		
			<div style="" id="step3" class="tabs-cont order-summary-tab tab-disabled">
				<form action="<?php echo $this->Html->url('/carts/order_summary/'.$useraddress[0]['Cart']['id']); ?>" method="post" name="myform">
					<?php echo $this->Form->input( 'CartAddress.status', array( 'type' => 'hidden','value'=>'order','label'=>false) );  ?>
					<a href="javascript:voide(0)" onclick="document.myform.submit()">
						<div class="order"></div>
						<div align="center"  class="tab-active">Order Summary</div>
					</a>
				</form>
			</div>
		
			<div id="step4" class="tabs-cont payment-tab tab-disabled">
				<div class="payment"></div>
				<div align="center">Payment</div>
			</div>
		</div>
			
			<?php if(isset($cartDetails)) : ?>
				<?php if(!empty($cartDetails)) : ?>
					<div class="cart-info">
						<table>
							<tr class="headrow">
					          	<th style="padding: 2px 0px 2px 10px; text-align: left; width: 260px;">Product</th>
					            <th style="padding: 2px 0px 2px 4px; text-align: left; width: 70px;">Price/Unit</th>
					            <th style="padding: 2px 0px 2px 4px; text-align: left; width: 100px;">Quantity</th>
					            <th style="padding: 2px 0px 2px 4px; text-align: left; width: 100px;">Shipping Cost</th>
					            <th style="padding: 2px 0px 2px 4px; text-align: left; width: 55px;">Total</th>
					            <th style="padding: 2px 0px 2px 4px; text-align: left; width: 55px;">Action</th>
				          	</tr>
						</table>
						<table>
				          	<?php $grandtotal=0; $shipping_count = 0;$i=1;?>
				          	<?php foreach($cartDetails as $details): ?>
				          	<?php 
			            		if ($details['CartProduct']['product_type'] == 'Pay')
			            		{
			            			if($shipping_count != $details['CartProduct']['product_shipping_type'])
			            			{?>
			            				<tr style="height:15px" class="headrow">
			            					<td colspan='6'>
			            						<?php 
			            							foreach($shippingCategory as $category)
			            							{
			            								if($category['ShippingCategory']['id'] == $details['CartProduct']['product_shipping_type'])
			            									echo "<strong>".strtoupper($category['ShippingCategory']['name'])."</strong>";
			            							}
			            						?>
			            					</td>
			            				</tr>
			            			<?php }
			            		}
			            	?>
				            <tr class="row">
				            	<td valign="top" style="width:240px">  
				            		<input type="hidden" name="productId" id="productId" value="<?php echo $details['CartProduct']['product_id'];?>"/>
				            		<div style="width:110px;float:left;padding:5px;min-height:100px;overflow:hidden;">
			        					<?php 
			        						$filepath = PRODUCT_IMAGE_PATH;
											$filename = $details['CartProduct']['product_id']. '.'. $details['Product']['image_extension'];
											$path = $this->Html->url(PRODUCT_IMAGE_WWW_PATH. $details['CartProduct']['product_id']. '.'. $details['Product']['image_extension']); 
											$path_prod = substr($path,strpos($path)+1);
									 	
											if (file_exists($filepath. $filename)) :?>
												<?php $img=$this->ImageResize->getResizedDimensions($path_prod, 100, 100);?>
												<a href="/products/view/<?php echo $details['CartProduct']['product_id'];?>">
													<img width="<?php echo $img['width'];?>" height="<?php echo $img['height'];?>"  src="<?php echo $this->Html->url('/'. $path_prod);?>" alt="" />
												</a>
										<?php endif; ?>
										<?php $filepath_merchant = MERCHANT_LOGO_PATH; 
									     	$path = substr($filepath_merchant,strpos($filepath_merchant,'webroot')+8);
											$path_mer = $path.$filename;
											if ((file_exists($filepath_merchant. $filename)) && (! file_exists($filepath. $filename)) ):?>
											<?php $img=$this->ImageResize->getResizedDimensions($path_mer, 100, 100);?>
											<a href="/products/view/<?php echo $details['CartProduct']['product_id'];?>">
												<img width="<?php echo $img['width'];?>" height="<?php echo $img['height'];?>"  src="<?php echo $this->Html->url('/'. $path_mer);?>" alt="" />
											</a>
									     <?php endif; ?>
				            		 </div>
		        					<div style="padding-top:2px;font-weight:normal">
		        						<h4>
		        							<a href="/products/view/<?php echo $details['CartProduct']['product_id'];?>">
		        								<?php echo $details['Product']['name'];?>
		        							</a>
		        						</h4>
		        						<?php echo $details['Product']['highlight'];?>
		        					</div>
				              	</td>
				              	
				              	<?php if ($details['CartProduct']['product_type'] == 'Points') { ?>
				              	<td valign="top" style="padding-top:5px">
				              		<?php echo $details['CartProduct']['product_points'].' '.P;?>
				              		<td valign="top" style="padding-top:5px">
				            		<?php echo $this->Form->input('ProductsCart.quantity',array('div'=>false,'label'=>false,'id'=>'ProductsCartQuantity'.$details['CartProduct']['product_id'],'class'=>'search_input','style'=>'width:30px','onKeyup' =>'update_cart_points('.$details['Cart']['id'].','.$details['CartProduct']['product_quantity'].','.$details['CartProduct']['product_id'].','.$details['CartItem']['id'].',value)','value'=>$details['CartItem']['product_qty']));?>
					           		<?php if($details['CartProduct']['product_quantity'] != -1):?><p><strong>In Stock : </strong> <?php echo $details['CartProduct']['product_quantity'];?></p><?php endif;?>
									
					           	</td>
					           	<td valign="top" style="padding-top:5px">
				              		<?php echo "Free"; ?>
				              	</td>
					          	<td valign="top" style="padding-top:5px">
					          		<?php 
				          				$qty = $details['CartItem']['product_qty'];
				          				$price = $details['CartProduct']['product_points'];
										$pointstotal = ($qty *$price);
									?>     
					          		<div id="updatecartpoints<?php echo $details['CartProduct']['product_id'];?>">
					          			<?php echo $pointstotal." P" ; ?>
					          		</div>
					           	</td>
				              	<td valign="top" style="padding-top:5px">
					            	<a href="#remove">
					            		<img title="Remove" alt="Remove" src="/files/newimg/remove.png" style="cursor:pointer" onclick="remove_cart(<?php echo $details['Cart']['id'].','.$details['CartItem']['product_id'].','.$details['CartItem']['id'];?>)">
					            	</a>
					           	</td>
				              	<?php } else {?>
				              	<td valign="top" style="padding-top:5px;width:70px">
				              		<?php echo '$'.$details['CartProduct']['product_price'];?>
				              	</td>
				              	<td valign="top" style="padding-top:5px;width:75px">
				            		<?php echo $this->Form->input('ProductsCart.quantity',array('div'=>false,'label'=>false,'id'=>'ProductsCartQuantity'.$details['CartProduct']['product_id'],'class'=>'search_input','style'=>'width:30px','onKeyup' =>'update_cart('.$details['Cart']['id'].','.$details['CartProduct']['product_quantity'].','.$details['CartProduct']['product_id'].','.$details['CartItem']['id'].',value,'.$details['CartProduct']['product_shipping_type'].',\''.$max_group_ship_cost.'\')','value'=>$details['CartItem']['product_qty']));?>
					           		
									<?php if($details['CartProduct']['product_quantity'] != -1):?><p><strong>In Stock : </strong> <?php echo $details['CartProduct']['product_quantity'];?></p><?php endif;?>
									
					           	</td>
					           	<td valign="top" style="padding-top:5px;width:100px;">
					           		<?php if($details['CartProduct']['product_shipping_type']==1) { $itemtype = 'Total'; } 
					           		      if($details['CartProduct']['product_shipping_type']==2) { $itemtype = 'Per Item'; } 
					           		      if($details['CartProduct']['product_shipping_type']==3)
					           		      {
					           		      	if($shipping_count != $details['CartProduct']['product_shipping_type'])
					           		      		$itemtype = 'Total';
					           		      	else
					           		      		$itemtype = '';
					           		      }
					           		      if($details['CartProduct']['product_shipping_type']==4)
					           		      {
					           		      	$itemtype = 'N/A';
					           		      }
					           		?>
					           		<?php 
				              			if($itemtype=='' || empty($itemtype)){
				              				echo 'Free';
				              			}else{ 
				              				if($details['CartProduct']['product_shipping_type']==3)
				              					echo "$ ". $max_group_ship_cost.'/ '.$itemtype;
				              				elseif($details['CartProduct']['product_shipping_type']==4)
				              					echo $itemtype;
				              				else
				              					echo "$ ". $details['CartProduct']['product_shipping_cost'].'/ '.$itemtype;
				              			}
				              		?>
				              	</td>
					          	<td valign="top" style="padding-top:5px;width:55px">
					          		<?php 
				          				$qty = $details['CartItem']['product_qty'];
				          				$price = $details['CartProduct']['product_price'];
				          				if($details['CartProduct']['product_shipping_type']==1) {
											$total = (($qty * $price) + $details['CartProduct']['product_shipping_cost']);
										}
										if($details['CartProduct']['product_shipping_type']==2) {
											$total = ($qty * ($price + $details['CartProduct']['product_shipping_cost']));
										}
										if($details['CartProduct']['product_shipping_type']==3)
										{
											if($shipping_count != $details['CartProduct']['product_shipping_type'])
												$total = (($qty * $price) + $max_group_ship_cost);
											else
												$total = ($qty * $price);
										}	
										if($details['CartProduct']['product_shipping_type']==4) {
											$total = $qty * $price;
										}
									?>     
					          		<div id="updatecart<?php echo $details['CartProduct']['product_id'];?>">
					          			<?php echo "$ ".number_format($total,2); ?>
					          		</div>
					           	</td>
					           	<td valign="top" style="padding-top:5px">
					            	<a href="#remove">
					            		<img title="Remove" alt="Remove" src="/files/newimg/remove.png" style="cursor:pointer" onclick="remove_cart(<?php echo $details['Cart']['id'].','.$details['CartItem']['product_id'].','.$details['CartItem']['id'];?>)">
					            	</a>
					           	</td>
							</tr>
							
							<?php if($i== $counts[0][0]['count']){?>
		              		<tr class="row" style="height:50px">
		              			<td colspan="6" style="text-align:right;padding-right:30px">
									Please select your preferred Smart Shipping rate:
									<?php
										if(empty($max_group_ship_cost))
											$max_group_ship_cost = 0;
										$stype = array('1.25'=>'$1.25 - Ordinary mail - Not recommended ','4.50'=>'$4.50 - Registered mail - Recommended');
										echo $this->Form->input('CartItem.smart_shipping_cost',array('type'=>'select','options'=>$stype,'empty'=>'','div'=>false,'default'=> $counts[0]['CartItem']['smart_shipping_cost'], 'onchange'=>'smart_shipping(this,'.$max_group_ship_cost.')'));
									?>
								</td>	
							</tr>
							<?php }?>
							<?php  $i++; } ?>
							
							<?php $grandtotal += $total; $grandpoints+=$pointstotal;$scount++;$shipping_count = $details['CartProduct']['product_shipping_type'];?>
							<?php endforeach; ?>
				      	</table>
				      	
				      	<?php if($grandtotal !=0) : ?>
				      	<?php 
			      			if(!empty($counts[0]['CartItem']['smart_shipping_cost']))
			      				$grandtotal+= $counts[0]['CartItem']['smart_shipping_cost'];
			      		?>
				      	<div id="grandtotal" align="right" style="margin-right:20px;">
			          			<?php echo "Grand Total: $". number_format($grandtotal,2); ?>
			        	</div>
			        	<?php endif; ?>
											
					<?php if($grandpoints !=0) : ?>
						<div id="grandpoints" align="right" style="margin-right:20px;">
							<?php echo "<strong>Grand Points :</strong>".$grandpoints."P"; ?>
						</div>
			        	<?php endif; ?>
			        	<div class="order-shipping-address-outer">
							<div class="order-shipping-address-edit">
								<div class="customer-information">
									<div style="padding: 0 0 2px 0;" class="customer-heading-text heading-text-left">Customer Information</div>
									<div id="order-summary-customer-information"><?php echo $useraddress[0]['CartAddress']['username']  ?> <br><?php echo $useraddress[0]['CartAddress']['email'] ?><br><? echo $useraddress[0]['CartAddress']['mobile'] ?></div>
								</div>
								<div class="customer-address">
									<div style="padding: 0 0 2px 0;" class="customer-heading-text heading-text-left">Shipping Address</div>
										<div style="width: 160px;" class="address-detail">
											<?php echo $useraddress[0]['CartAddress']['username'] ?><br>
											<?php echo $useraddress[0]['CartAddress']['member_name'] ?><br>
											<?php echo $useraddress[0]['CartAddress']['address1'] ?><br>
											<?php echo $useraddress[0]['CartAddress']['city'] ?><br>
											<?php echo $useraddress[0]['CartAddress']['state'] ?><br>
											<?php echo $useraddress[0]['CartAddress']['country'] ?><br>
											<?php echo $useraddress[0]['CartAddress']['zipcode'] ?><br>
										</div>
								</div>
							</div>
							
							<div class="order-summary-continue">
							    <!-- <a href="/carts/proceed_payment"> -->
								<button type="submit" class="quick-search_button_style rounded_corners_button" style="width:150px" onclick="check_smart_shipping()">Proceed to payment</button>
								<!-- </a> -->
							</div>
						</div>
					    
				    </div>
				    
				<?php else: ?>
					<p style="padding-left:15px;font-size:15px;"><?php echo "Your cart is empty";?></p>
				<?php endif; ?>
			<?php endif; ?>
	</div>
	
	<div style="float: right;overflow: hidden;width: 275px;margin-top: 25px;padding-right: 5px;">
	<div style="background: none repeat scroll 0 0 #C0C2C3;border-radius: 8px 8px 8px 8px;margin-top: 0;padding: 3px;">
		<?php if($this->Session->read('dashboardidinsession') == 1) { ?>
			<label style="color: #272727;display: block;font-size: 16px;font-weight: bold;padding: 9px;">Payment and Shipping</label>
			<div style="background: none repeat scroll 0 0 #FFFFFF;border-radius: 0 0 6px 6px;color: #000000;font-size: 12px;margin: 0;padding: 5px;">
				<div style=" color: #505050;font-size: 12px;padding: 5px;">
					<ul>
						<li>
							<div style=" color: #505050;font-size: 12px;font-weight: bold;margin: 0;padding: 5px 0 5px 5px;">How can I pay?</div>
						</li>
						<li>
							<div style=" color: #505050;font-size: 12px;font-weight: bold;margin: 0;padding: 5px 0 5px 5px;">You can pay with Visa or MasterCard credit cards.</div>
						</li>
						<li>
							<div style="padding: 5px 0 5px 5px">
								We use the PayPal payment system to take and process your payments which ensures that your payments are within a world class payment gateway. All the information is processed using SSL data encryption (secure encrypted connection), which protects the information from being viewed by anyone.
							</div>
						</li>
					</ul>
				</div>
				<div>
					<ul>
						<li>
							<div style=" color: #505050;font-size: 12px;font-weight: bold;margin: 0;padding: 5px 0 5px 5px;">How does the postage and handling work?</div>
						</li>
						<li>
							<div style="padding: 5px 0 5px 5px;">The level and type of postage and handling is determined by the product or service that you purchase. Most products will attract smart shipping rates. </div>
						</li>
						<li style="padding-left:15px">
							<div style=" color: #505050;font-size: 12px;font-weight: bold;margin: 0;padding: 5px 0 5px 5px;">Ordinary mail- Not recommended:-</div>
						
							<div style="padding:5px 0 5px 5px">Ordinary mail is the cheapest option however does not have any insurance and we are unable to track it through the postal system .If you choose this method and your order is lost, the supplier will not be responsible and no refund will apply.</div>
						
							<div style=" color: #505050;font-size: 12px;font-weight: bold;margin: 0;padding: 5px 0 5px 5px;">Registered Mail - Recommended </div>
						
							<div style="padding:5px 0 5px 5px">We recommend that you choose Registered mail for all orders as it gives a level of protection and we can track your order.</div>
						</li>
						<li>
							<div style=" color: #505050;font-size: 12px;font-weight: bold;margin: 0;padding: 5px 0 5px 5px;">
								Variable postage and handling rates:
							</div>
						</li>
						<li>	
							<div style="padding:5px 0 5px 5px">
								Variable Postage and Handling rates will apply to certain products. These rates will be shown in cart and the product as you make the order 
							</div>
						</li>
						<li>
							<div style=" color: #505050;font-size: 12px;font-weight: bold;margin: 0;padding: 5px 0 5px 5px;">
								Shopping cart: 
							</div>
						</li>
						<li>
							<div style="padding:0px 0 5px 5px">
								If you are unsure of what you have saved to the cart you can change or edit your order within the cart as you proceed through the order process. 
								It's easy.<br><br>For full terms and conditions of purchase please <a href="/display/terms_purchase" target="_blank">click here</a>.
							</div>
						</li>
					</ul>
				</div>
			</div>
			<div style="display: none" id="tracking-pixel"></div>
		<?php }?>
		<?php if($this->Session->read('dashboardidinsession') == 3) { ?>
			<div style="background: none repeat scroll 0 0 #FFFFFF;border-radius: 0 0 6px 6px;color: #000000;font-size: 12px;margin: 0;padding: 5px;">
				<div style=" color: #505050;font-size: 12px;padding: 5px;">
					<ul>
						<li>
							<div style=" color: #505050;font-size: 12px;font-weight: bold;margin: 0;padding: 5px 0 5px 5px;">Congratulations on redeeming your points ! </div>
						</li>
						<li>
							<div style="padding: 5px 0 5px 5px">
								The items you have selected will be processed in the next 48 hours. Once you finalise your order , 
								you will receive an order confirmation email.   So sure that your email is correct in the order details.  
								If this doesn't arrive please check your spam box. Product delivery times depend upon our suppliers, 
								and generally items will be delivered within 21 working days. Deliver fees do not apply to items in the redemption 
								menu unless otherwise specified in your points shopping cart. 
							</div>
						</li>
					</ul>
				</div>
				<div>
					<ul>
						<li>
							<div style=" color: #505050;font-size: 12px;font-weight: bold;margin: 0;padding: 5px 0 5px 5px;">Check your points shopping cart as you go ...</div>
						</li>
						<li>
							<div style="padding: 5px 0 5px 5px;">If you are unsure of what you have purchased or saved to your  cart you can change or edit your order within the cart as you proceed through the order process. It's easy! </div>
						</li>
						<li>
							<div style=" color: #505050;font-size: 12px;font-weight: bold;margin: 0;padding: 5px 0 5px 5px;">
								Needs some help ?
							</div>
						</li>
						<li>	
							<div style="padding:5px 0 5px 5px">
								Feel free to send us an email to support@therewardsteam.com.au or chat live to a consultant via the Support Chat Box . ( If during operating hours )
								<p>For full terms and conditions of the points redemption purchases and program see "Terms and Conditions" in this web site . For specific product details  refer to the product explanations supplied in this web site for that product.</p> 
							</div>
						</li>
					</ul>
				</div>
			</div>
		<?php }?>
	</div>
</div>
</div>