<style>
.head-lable {
    border-radius: 6px 6px 0 0;
    color: #2D2E2E;
    font-size: 18px;
    font-weight: bold;
    height: 38px;
    margin-bottom: 15px;
    padding: 8px 0 0 15px;
    text-shadow: 0 1px #FFFFFF;
}
.online_request_border
{
	border:1px solid;
}
</style>
<?php echo $this->element('module2', array('module2' => 'Keyword_search', 'title' => 'Search'));?>

<div class="online_request_border"  style="width:950px;margin-bottom:0px !important;min-height:10px;overflow:hidden;">
	<div style="padding:20px;">
			
		<table>
			<tr>
				<td>
		           <img style="width:40px;height:40px;" src="/files/styles/icon_images/summary.jpg""  alt="My Orders" title="My Orders" />&nbsp;&nbsp;
		        </td>
		        <td> 
		            <h2><?php echo "My Orders";?></h2>
		        </td>
		    </tr>        
		</table>
		
			<div style="background-color:#EFEFEF;border:1px solid #cccccc;margin-top: 10px;overflow:hidden;">
					<table cellpadding="0" cellspacing="0" width="100%">
						<tr height="30">
							<th style="background-color:#cccccc;text-align:left;padding-left:5px;border-left:1px solid #969090;border-bottom:1px solid #969090;border-top:1px solid #969090;">S.No.</th>
							<th style="background-color:#cccccc; padding-left:5px;border-bottom:1px solid #969090;border-top:1px solid #969090;">Order Id</th>
							<th style="background-color:#cccccc; padding-left:5px;border-bottom:1px solid #969090;border-top:1px solid #969090;">Purchase Date</th>
							<th style="background-color:#cccccc; text-align:left;padding-left:5px;border-bottom:1px solid #969090;border-top:1px solid #969090;">Order Items</th>
							<th style="background-color:#cccccc; padding-left:5px;border-bottom:1px solid #969090;border-top:1px solid #969090;">Qty/Unit Price</th>
							<th style="background-color:#cccccc; padding-left:5px;border-bottom:1px solid #969090;border-top:1px solid #969090;">Shipping Cost</th>
							<th style="background-color:#cccccc; padding-left:5px;border-bottom:1px solid #969090;border-top:1px solid #969090;">Total Amount</th>
							<th style="background-color:#cccccc; padding-left:5px;border-bottom:1px solid #969090;border-top:1px solid #969090;">Order Status</th>
							<th class="actions" style="background-color:#cccccc;padding-left:4px;border-right:1px solid #969090;border-bottom:1px solid #969090;border-top:1px solid #969090;">Invoice</th>
						</tr>
					    <?php if(count($cartlist) >0 ) { ?>
						<?php
						$i = 0;$orderId = 0;
						foreach ($cartlist as $details):
							
							$class = null;
							if ($i++ % 2 == 0) {
								$class = ' style="background-color: white;"';
							}
						?>
						<tr<?php echo $class;?>>
							<td align="center"><strong><?php echo $i;?></strong></td>
						    <td align="center"><a onclick="Modalbox.show(this.href, {title: this.title, width: 730}); return false;" href="/carts/cart_show_order/<?php echo $details['CartItem']['id'];?>/<?php echo $details['CartItem']['product_id'];?>"><?php echo $details['CartItem']['order_id'];?></a></td>
						    <td align="center"><?php echo $details['CartItem']['order_date'];?></td>
						    <td><?php echo $details['Product']['name'];?></td>
						     <?php if($details['CartItem']['product_type']=='Points') { ?>
						     <td align="center"><?php echo $details['CartItem']['product_qty'].'/ '. $details['CartProduct']['product_points'].' '.P ;?></td>
						     <td align="center"> --- </td>
						     <td align="center"><?php echo ($details['CartProduct']['product_points']*$details['CartItem']['product_qty']).' '.P; ?></td>
						     <?php } else { ?>
						     
						    <td align="center"><?php echo $details['CartItem']['product_qty'].'/ $ '.$details['CartProduct']['product_price'] ;?></td>
						    <td align="center">
						    	<?php
						    	if($details['CartProduct']['product_shipping_type']==3)
						    	{
							    	if($orderId !== $details['CartItem']['order_id'])
								    	echo "$ ".$details['CartProduct']['product_shipping_cost'];
							    	else
							    		echo "Free";
						    	}else{
						    		echo "$ ".$details['CartProduct']['product_shipping_cost'];
						    	}
						    	?>
						    </td>
						    <?php 
		          				$qty = $details['CartItem']['product_qty'];
		          				$price = $details['CartProduct']['product_price'];
		          				if($details['CartProduct']['product_shipping_type']==1) 
		          				{
									$total = (($qty * $price) +$details['CartProduct']['product_shipping_cost']);
								}
								if($details['CartProduct']['product_shipping_type']==2) 
								{
									$total = $qty * ($price+$details['CartProduct']['product_shipping_cost']);
								}	
								if($details['CartProduct']['product_shipping_type']==3)
								{
									if($orderId !== $details['CartItem']['order_id'])
										$total = ($qty * $price)+$details['CartProduct']['product_shipping_cost'];
									else
										$total = $qty * $price;
									$orderId = $details['CartItem']['order_id'];
								}
								if($details['CartProduct']['product_shipping_type']==4)
								{
									$total = ($qty * $price);
								}
							?>   
						    <td align="center"><?php echo "$ ".$total;?></td>
						    <?php } ?>
						    <td align="center"><?php echo $details['CartItem']['cart_status'];?></td>
						    <td align="center"><a onclick="Modalbox.show(this.href, {title: this.title, width: 730}); return false;" href="/carts/order_invoice/<?php echo $details['CartItem']['cart_id'];?>/<?php echo $details['CartItem']['order_id'];?>"><strong>Invoice</strong></a></td>
						</tr>
					   <?php endforeach; ?>
					   <?php } else {  echo " <tr><td colspan='8'><strong>You have no orders.</strong></td></tr>"; } ?>
					</table>
			</div>               
		</div>
</div>