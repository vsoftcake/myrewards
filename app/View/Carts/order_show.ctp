<style>
.cart-info table {
    border-collapse: collapse;
    border-left: 1px solid #DDDDDD;
    border-right: 1px solid #DDDDDD;
    border-top: 1px solid #DDDDDD;
    margin-bottom: 15px;
    margin:10px 10px 10px 10px;
    width:97%;
}

.cart-info .headrow {
    background-color: #F7F7F7;
    border-bottom: 1px solid #DDDDDD;
    font-weight: bold;
    height:30px;
}
.cart-info .row {
    border-bottom: 1px solid #DDDDDD;
    font-weight: bold;
}
.cart-info td {
    padding: 4px;
}
.order-shipping-address-outer {
    background: none repeat scroll 0 0 #E7E5E5;
    border-top: 1px dotted #252525;
    margin: 0 1px;
    overflow: hidden;
    padding: 10px 0px 10px 20px;
}
.order-shipping-address-edit {
    color: #242424;
    float: left;
    font-size: 11px;
    overflow: hidden;
    width: 445px;
}
.customer-information {
    float: left;
    width: 200px;
}
.customer-heading-text {
    color: #242424;
    font-size: 12px;
    font-weight: bold;
    padding-bottom: 10px;
   /* text-align: center; */
}
.order-shipping-address-edit {
    color: #242424;
    font-size: 11px;
}

.customer-address {
    border-left: 1px solid #ABABAB;
    float: left;
    min-height: 98px;
    padding: 0 0 0 10px;
    width: 200px;
}
/*.address {
    float: left;
    font-size: 12px;
    padding: 10px;
}*/
.customer-info {
    float: left;
    overflow: hidden;
    padding-bottom: 25px;
    width: 380px;
}

#content
{
	border-left:1px solid #cccccc;
	border-right:1px solid #cccccc;
	border-bottom:1px solid #cccccc;
	margin-left:20px !important;
	padding-left:18px !important;
	width:913px;
}
.admin_button
{
	height: 26px;
	padding-bottom: 2px;
	border-radius: 5px 5px 5px 5px;
	background-color: #0066cc;
	border: 1px solid #cccccc;
	color: #FFFFFF; 
	cursor: pointer;
	font-size:13px;
	width:90px;
}
</style>
<div style="padding:10px">

<table style="margin-left:-2px;margin-top: 10px;">
    <tr>
	<td style="width:800px">
		<div style="float:left">
			<table>
				<tr>
					<td>
					   <img style="width:40px;height:40px;" src="/files/styles/icon_images/summary.jpg""  alt="Order Summary" title="Order Summary" />&nbsp;&nbsp;
					</td>
					<td> 
					    <h2><?php echo "Product Summary";?></h2>
					</td>
			    </tr>
			</table>
		</div>
		<div style="float: right; padding-top: 15px;">
			<a class="link_color" style="font-weight:normal;margin-left:25px;" id="back" href="javascript:window.history.back()">
				<button class="admin_button rounded_corners_button">Back</button>
			</a><br/>

		</div>
	</td>
    </tr>               
</table>

</div>
	<div style="background: none repeat scroll 0 0 #FFFFFF;border: 1px solid #AEAEAE;border-radius: 10px 10px 10px 10px;box-shadow: 0 0 2px #828282;float: left;overflow: hidden;margin-left:10px;margin-top:10px;width:97%;">
			
			<?php if(isset($cartlist)) : ?>
				<?php if(!empty($cartlist)) : ?>
					<div class="cart-info">
						<table>
							<tr class="headrow">
					          	<th style="padding: 2px 0px 2px 10px; text-align: left; width: 100px;">Product Name</th>
					          	<th style="padding: 2px 0px 2px 10px; text-align: left; width: 50px;">Order Id</th>
					            <th style="padding: 2px 0px 2px 4px; text-align: left; width: 70px;">Unit Price</th>
					            <th style="padding: 2px 0px 2px 4px; text-align: left; width: 60px;">Quantity</th>
					            <th style="padding: 2px 0px 2px 4px; text-align: left; width: 60px;">Shipping Cost</th>
					            <th style="padding: 2px 0px 2px 4px; text-align: left; width: 55px;">Total</th>
					            <th style="padding: 2px 0px 2px 4px; text-align: left; width: 25px;">Status</th>
					            <th style="padding: 2px 0px 2px 4px; text-align: left; width: 25px;">Updated</th>
				          	</tr>
				          	<?php foreach($cartlist as $details): ?>
				            <tr class="row">
				            	
				            	<td valign="top"> 
	        						<h4>
	        							<a href="/products/view/<?php echo $details['CartProduct']['product_id'];?>">
	        								<?php echo $details['Product']['name'];?>
	        							</a>
	        						</h4>
				              	</td>
				              	<td valign="top"> 
	        						<?php echo $details['CartItem']['order_id'];?>
				              	</td>
				              	<?php if( $cartlist[0]['CartItem']['product_type'] == 'Points' ) { ?>
				              	
				              	<td valign="top" style="padding-top:5px">
				            		<?php echo $details['CartProduct']['product_points'].' '.P; ?>
					           	</td>
					           	<td valign="top" style="padding-top:5px"><?php echo $details['CartItem']['product_qty'];?></td>
								<td valign="top" style="padding-top:5px">---</td>
						    	<td valign="top" style="padding-top:5px"><?php echo ($details['CartItem']['product_qty'] * $details['CartProduct']['product_points']).' '.P ;?></td>
						    	
						    	<?php } else { ?>
								<td valign="top" style="padding-top:5px">
				            		<?php echo "$".$details['CartProduct']['product_price']; ?>
					           	</td>
					           	<td valign="top" style="padding-top:5px"><?php echo $details['CartItem']['product_qty'];?></td>
								<?php 
			          				$qty = $details['CartItem']['product_qty'];
			          				$price = $details['CartProduct']['product_price'];
			          				if($details['CartProduct']['product_shipping_type']==1) {
										$total = (($qty * $price) + $details['CartProduct']['product_shipping_cost']);
									}
									if($details['CartProduct']['product_shipping_type']==2) {
										$total = ($qty * ($price + $details['CartProduct']['product_shipping_cost']));
									}
									if($details['CartProduct']['product_shipping_type']==3) 
									{
										if($shipping_cost == 'Free')
											$total = $qty * $price;
										else 
											$total = (($qty * $price) + $shipping_cost);
									}
									if($details['CartProduct']['product_shipping_type']==4)
									{
										$total = $qty * $price;
									}
								?>  
									
								<td valign="top" style="padding-top:5px">
									<?php 
										if($details['CartProduct']['product_shipping_type']==3)
										{
											echo $shipping_cost;
										}
										elseif($details['CartProduct']['product_shipping_type']==4)
										{
											echo "$ 0.00";
										}
										else
										{
											echo "$ ".$details['CartProduct']['product_shipping_cost'];
										}
									?>
									
								</td>
						    	<td valign="top" style="padding-top:5px"><?php echo "$ ".$total;?></td>
						    	
						    	<?php } ?>
					           	<td valign="top" style="padding-top:5px"> <?php echo $details['CartItem']['cart_status'];?></td>
					           	<td valign="top" style="padding-top:5px"> 
								<?php 
									if($details['CartItem']['cart_status'] == 'Pending' || $details['CartItem']['cart_status'] == 'Paid') : 
										echo $details['CartItem']['modified'];
									elseif($details['CartItem']['cart_status'] == 'Shipped') :
										echo $details['CartItem']['shipping_date'];
									elseif($details['CartItem']['cart_status'] == 'Completed') :
										echo $details['CartItem']['completed_date'];
									endif;
								?>
					           	</td>
							</tr>
							<?php endforeach; ?>
				      	</table>
				      	
			        	<div class="order-shipping-address-outer">
							<div class="order-shipping-address-edit">
								<div class="customer-information">
									<div style="padding: 0 0 2px 0;" class="customer-heading-text heading-text-left">Customer Information</div>
									<div id="order-summary-customer-information"><? echo $cartlist[0]['CartAddress']['username']  ?> <br><? echo $cartlist[0]['CartAddress']['email'] ?><br><? echo $cartlist[0]['CartAddress']['mobile'] ?></div>
								</div>
								<div class="customer-address">
									<div style="padding: 0 0 2px 0;" class="customer-heading-text heading-text-left">Shipping Address</div>
										<div style="width: 160px;" class="address-detail">
											<? echo $cartlist[0]['CartAddress']['member_name'] ?><br>
											<? echo $cartlist[0]['CartAddress']['address1'] ?><br>
											<? echo $cartlist[0]['CartAddress']['city'] ?><br>
											<? echo $cartlist[0]['CartAddress']['state'] ?><br>
											<? echo $cartlist[0]['CartAddress']['country'] ?><br>
											<? echo $cartlist[0]['CartAddress']['zipcode'] ?><br>
										</div>
								   </div>
							  </div>
							  <?php if(!empty($cartlist[0]['CartItem']['courier_name'])) { ?>
							  <div class="customer-address">
									<div style="padding: 0 0 2px 0;" class="customer-heading-text heading-text-left">Shipping Details</div>
										<div class="address-detail">
											<? echo "<strong>Shipment Name: </strong>". $cartlist[0]['CartItem']['courier_name'] ?><br>
											<? echo "<strong>Consignment Number: </strong>".$cartlist[0]['CartItem']['courier_number'] ?><br>
											<? echo '<strong>Track your Shipment: </strong><a href="'.$cartlist[0]['CartItem']['tracking_link'].'"> Link </a>'; ?><br>
											<? echo "<strong>Estimated Date: </strong>".$cartlist[0]['CartItem']['completed_date'] ?><br>
										</div>
								   </div>
							  </div>
							  <?php } ?>
							</div>  
						</div>
				    </div>
				<?php else: ?>
					<p style="padding-left:15px;font-size:15px;"><?php echo "Your cart is empty";?></p>
				<?php endif; ?>
			<?php endif; ?>
	</div>
</div>
