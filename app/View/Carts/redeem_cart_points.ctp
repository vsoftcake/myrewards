<style>	
.cart-info table {
    border-collapse: collapse;
    border-left: 1px solid #DDDDDD;
    border-right: 1px solid #DDDDDD;
    border-top: 1px solid #DDDDDD;
    margin-bottom: 15px;
    margin:10px 10px 10px 10px;
    width:100%;
}

.cart-info .headrow {
    background-color: #F7F7F7;
    border-bottom: 1px solid #DDDDDD;
    font-weight: bold;
    height:30px;
}
.cart-info .row {
    border-bottom: 1px solid #DDDDDD;
    font-weight: bold;
}
.cart-info td {
    padding: 4px;
}
</style>
<script>
	function update_cart(id,value)
	{
		$.ajax({
			url: '/carts/update_cart/'+id+'/'+value,
			type: "GET",
			success: function (data) {
				$('#updatecart'+id).html(data);
			}
		});
	}
	function online_request()
	{
		var url = '/products/online_request/';
		window.location = url;
	}
	function clear_cart()
	{
		var answer = confirm("Are you sure you want to clear cart");
		if (answer)
		{
			var url = "/products/clear_cart/";
			window.location = url;
		}
	}
	function remove_cart(id)
	{
		var answer = confirm("Are you sure you want to remove from cart");
		if (answer)
		{
			var url = "/carts/remove_cart/"+id;
			window.location = url;
		}
	}
	var cmt = "";
	var uid = 0;
	function showOrders(id)
	{
		if(cmt=="")
		{
			document.getElementById('order'+id).style.display='block';
			cmt = id;
		}
		else if((cmt!="" || id != "") && cmt!=id )
		{
			document.getElementById('order'+cmt).style.display="none";
			document.getElementById('order'+id).style.display="block";
			cmt = id;
		}
		else if(cmt==id)
		{
			if(uid==0)
			{
				document.getElementById('order'+cmt).style.display="none";
				uid = 1;			
			}
			else if(uid==1)
			{
				document.getElementById('order'+cmt).style.display="block";
				uid = 0;
			}
		}
	}
</script>

<div class="online_request_border"  style="width:100%;margin-bottom:0px !important;min-height:10px;overflow:hidden;">

	<?php if(isset($cartDetails)) : ?>
		<?php if(!empty($cartDetails)) : ?>
		<div>
			<div style="float:left; padding:20px 0px 10px 15px; width:70px;">
				<?php echo $this->Html->image('/files/styles/icon_images/shop.png'); ?>
			</div>
			<div  style="padding:20px 0px 10px 15px;">
				<h2>My Cart</h2>
			</div>
		</div>
		<?php endif; ?>
	<?php endif; ?>
		
	<div style=" width:98%;float:left;">
			<?php //print_r($cartDetails); ?>
			<?php if(isset($cartDetails)) : ?>
				<?php if(!empty($cartDetails)) : ?>
					<div class="cart-info">
					<table>
							<tr class="headrow">
					          	<th style="padding: 2px 0px 2px 4px; text-align: left; width: 180px;">Product</td>
					            <th style="padding: 2px 0px 2px 4px; text-align: left; width: 70px;">Price/Unit</td>
					            <th style="padding: 2px 0px 2px 4px; text-align: left; width: 100px;">Quantity</td>
					            <th style="padding: 2px 0px 2px 4px; text-align: left; width: 70px;">Shipping Cost</td>
					            <th style="padding: 2px 0px 2px 4px; text-align: left; width: 55px;">Total</td>
				          	</tr>
				       </table>
				        <div style="height: 230px; overflow-x: hidden;overflow-y: auto;">
						<table>
						<tbody>
							<tr >
					          	<th style="padding: 2px 0px 2px 10px; text-align: left; width: 200px;"></td>
					            <th style="padding: 2px 0px 2px 4px; text-align: left; width: 70px;"></td>
					            <th style="padding: 2px 0px 2px 4px; text-align: left; width: 100px;"></td>
					            <th style="padding: 2px 0px 2px 4px; text-align: left; width: 70px;"></td>
					            <th style="padding: 2px 0px 2px 4px; text-align: left; width: 55px;"></td>
				          	</tr>
							
				          	<?php foreach($cartDetails as $details): ?>
				            <tr class="row">
				            	<td valign="top">  
				            		<input type="hidden" name="productId" id="productId" value="<?php echo $details['CartProduct']['product_id'];?>"/>
				            		<div style="width:110px;float:left;padding:5px;min-height:100px;overflow:hidden;">
			        					<?php 
			        						$filepath = PRODUCT_IMAGE_PATH;
											$filename = $details['CartProduct']['product_id']. '.'. $details['Product']['image_extension'];
											$path = $this->Html->url(PRODUCT_IMAGE_WWW_PATH. $details['CartProduct']['product_id']. '.'. $details['Product']['image_extension']); 
											$path_prod = substr($path,strpos($path)+1);
									 	
											if (file_exists($filepath. $filename)) :?>
												<?php $img=$this->ImageResize->getResizedDimensions($path_prod, 100, 100);?>
												<a href="/products/view/<?php echo $details['CartProduct']['product_id'];?>">
													<img width="<?php echo $img['width'];?>" height="<?php echo $img['height'];?>"  src="<?php echo $this->Html->url('/'. $path_prod);?>" alt="" />
												</a>
											<?php endif; ?>
				            			</div>
			        					<div style="padding-top:2px;font-weight:normal">
			        						<h4>
			        							<a href="/products/view/<?php echo $details['CartProduct']['product_id'];?>">
			        								<?php echo $details['Product']['name'];?>
			        							</a>
			        						</h4>
			        						<?php echo $details['Product']['highlight'];?>
			        					</div>
				              	</td>
				              	<?php if ($details['CartItem']['product_type'] == 'Points') { ?>
				              	<td valign="top" style="padding-top:5px">
				              		<?php echo $details['CartProduct']['product_points'].' '.P ;?>
				              	</td>
				              	<td valign="top" style="padding-top:5px">
								    <?php echo $details['CartItem']['product_qty'];?>
									<?php if($details['CartProduct']['product_quantity']!=-1):?><p><strong>In Stock : </strong> <?php echo $details['CartProduct']['product_quantity'];?></p><?php endif;?>
									
					           	</td>
					           	<td valign="top" style="padding-top:5px">
				              		<?php echo "---";?>
				              	</td>
					          	<td valign="top" style="padding-top:5px">
					          		<?php 
				          				$qty = $details['CartItem']['product_qty'];
				          				$price = $details['CartProduct']['product_points'];
				          				 
										$totalpoints = ($qty * $price);	
									?>     
					          		
					          		<?php echo $totalpoints.' '.P; ?>
					           	</td>
				              	<?php } else { ?>
				              	<td valign="top" style="padding-top:5px">
				              		<?php echo '$'.$details['CartProduct']['product_price'];?>
				              	</td>
				              	<td valign="top" style="padding-top:5px">
								    <?php echo $details['CartItem']['product_qty'];?>
									<?php if($details['CartProduct']['product_quantity']!=-1):?><p><strong>In Stock : </strong> <?php echo $details['CartProduct']['product_quantity'];?></p><?php endif;?>
									
					           	</td>
					           	<td valign="top" style="padding-top:5px">
				              		<?php echo "$ ".$details['CartProduct']['product_shipping_cost'];?>
				              	</td>
					          	<td valign="top" style="padding-top:5px">
					          		<?php 
				          				$qty = $details['CartItem']['product_qty'];
				          				$price = $details['CartProduct']['product_price'];
				          				 
										$total = ($qty * $price)+$details['CartProduct']['product_shipping_cost'];	
									?>     
					          		<div id="updatecart<?php echo $details['CartProduct']['product_id'];?>">
					          			<?php echo "$ ". number_format($total,2); ?>
					          		</div>
					           	</td>
				              	<?php } ?>
							</tr>
							
							<?php endforeach; ?>
							</tbody>
				      	</table>
				      </div>
				    <div style="background: none repeat scroll 0 0 #E7E5E5;color: #3E3E3E;font-size: 13px;overflow: hidden;">
				      	<div style="color: #3E3E3E;font-weight: bold;float: left;padding: 10px;">
					      	Cart Summary (<span class="cart-text"><?php echo count($cartDetails); ?> Item(s)</span>)<br>
					      	<span>(You can update your cart please click on checkout)</span></div>
				  	</div>
				    </div>
				    
				    <div style="background: none repeat scroll 0 0 #F2F1F1;color: #5F5F5F;font-size: 11px;overflow: hidden;padding: 9px 12px;">
						<div class="payment-button-outer">
							<div style="float: right;">
								<button type="submit" class="quick-search_button_style rounded_corners_button" style="width:100px" onclick="window.location.href='/carts/show_cart'">Check Out!</button>
							</div>
							<div style="float: right;padding-right: 10px;">
								<?php if($this->Session->read('dashboardidinsession')==3) :?>
									<button type="submit" class="quick-search_button_style rounded_corners_button" style="width:180px" onclick="window.location.href='/products/search2'">Add more item(s) to cart</button>
								<?php else: ?>
									<button type="submit" class="quick-search_button_style rounded_corners_button" style="width:180px" onclick="window.location.href='/products/shop_now'">Add more item(s) to cart</button>
								<?php endif; ?>								
							</div>
						</div>
				  </div>
				  
				<?php else: ?>
					<p style="padding-left:15px;font-size:15px;"><?php echo "Your cart is empty";?></p>
				<?php endif; ?>
			<?php endif; ?>
	</div>
	
</div>