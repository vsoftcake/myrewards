<style>
.head-lable {
    border-radius: 6px 6px 0 0;
    color: #2D2E2E;
    font-size: 18px;
    font-weight: bold;
    height: 38px;
    margin-bottom: 15px;
    padding: 8px 0 0 15px;
    text-shadow: 0 1px #FFFFFF;
}
#MB_window
{
	top: 4% !important;
}
</style>
<style>#MB_window{overflow:scroll !important}</style>
<?php //echo $this->element('module2', array('module2' => 'Keyword_search', 'title' => 'Search'));?>
<div class="online_request_border"  style="margin-bottom:0px !important;min-height:10px;overflow:hidden;">
	<div>
		<div class="head-lable" style="height:25px !important;padding:5px 0 2px 15px">Invoice</div>
		<div style="font-size:12px;">
			<div>
				<table cellspacing="0" cellpadding="0" border="0" style="border:1px #ebebeb solid;font:normal 12px Segoe UI,arial;background:#fff;margin: -20px auto 0;color:#272727">
					<tbody>
						<tr>
							<td style="border-bottom:2px solid #ccc">
								<!--<a target="_blank" href="http://www.myrewards.com.au">
									<img border="0" width="100px" height="30px" src="http://www.myrewards.com.au/app/webroot/advt4free/images/logo.png">-->
									<img border="0" width="120px" height="60px" src="<?php echo $this->Html->url('/files/client_logo/'. $this->Session->read('client.Client.id'). '.'. $this->Session->read('client.Client.client_logo_extension')); ?>">
								<!--</a>-->
							</td>
						</tr>
						<tr>
							<td style="padding-left:15px;padding-right:1px">
								<table width="100%" cellspacing="0" cellpadding="0" border="0">
									<tr>
										<td width="58%" valign="bottom" style="font-weight:bold;font-size:14px;font-family:Segoe UI,arial;color:#292929">Dear <?php echo $cartlist[0]['CartAddress']['username']; ?></td>
										<td width="42%" align="right" style="font-size:12px;color:#787878;padding-top:5px;font-family:Segoe UI,arial"> Order Placed 	on : <?php echo $cartlist[0]['CartItem']['order_date']; ?></td>
									</tr>
									<tr>
										<td width="58%" valign="bottom" style="font-weight:bold;font-size:14px;font-family:Segoe UI,arial;color:#292929" align="left">
											<span style="font-family:Segoe UI,arial;font-size:12px;font-weight:bold">Ordered Items:</span>
										</td>
										<td width="42%" align="right" style="font-size:12px;color:#787878;padding-top:5px;font-family:Segoe UI,arial"> Order Number : <?php echo $cartlist[0]['CartItem']['order_id']; ?></td>
									</tr>
								</table>
								<table width="100%" cellspacing="0" cellpadding="0" border="1" style="border-collapse:collapse;text-align:center">
									<tr bgcolor="#f0eded">
										<th style="padding:1px 5px;font-size:12px;font-family:Segoe UI,arial;width:30px">SNo.</th>
										<th style="padding:1px 5px;font-size:12px;font-family:Segoe UI,arial;text-align:left;width:175px">Item(s) name</th>
										<th style="padding:1px 5px;font-size:12px;font-family:Segoe UI,arial;width:50px">ProductId</th>
										<th style="padding:1px 5px;font-size:12px;font-family:Segoe UI,arial;width:30px">Qty</th>
										<th style="padding:1px 5px;font-size:12px;font-family:Segoe UI,arial;width:65px">Status</th>
										<th style="padding:1px 5px;font-size:12px;font-family:Segoe UI,arial;width:60px">Discount</th>
										<th style="padding:1px 5px;font-size:12px;font-family:Segoe UI,arial;width:65px">Unit Price/points</th>
										<th style="padding:1px 5px;font-size:12px;font-family:Segoe UI,arial;width:70px">Total Price/Points</th>
									</tr>
										<?php 
										$i = 1;
										foreach($cartlist as $list) :?>
									<tr>
										<td style="padding:8px 5px;font-size:12px;font-family:Segoe UI,arial"><?php echo $i;?></td>
										<td style="padding:8px 5px;font-size:12px;font-family:Segoe UI,arial;text-align:left"><?php echo $list['Product']['name']; ?></td>
										<td style="padding:8px 5px;font-size:12px;font-family:Segoe UI,arial;text-align:left"><?php echo $list['CartProduct']['product_id']; ?></td>
										<td style="padding:8px 5px;font-size:12px;font-family:Segoe UI,arial"><?php echo $list['CartItem']['product_qty']; ?></td>
										<td style="padding:8px 5px;font-size:12px;font-family:Segoe UI,arial"><?php echo $list['CartItem']['cart_status']; ?></td>
										<td style="padding:8px 5px;font-size:12px;font-family:Segoe UI,arial">---</td>
										<?php if( $list['CartItem']['product_type'] == 'Points' ) { ?>
										<td style="padding:8px 5px;font-size:12px;font-family:Segoe UI,arial"><?php echo $list['CartProduct']['product_points'].' '.P; ?></td>
										<?php } else { ?>
										<td style="padding:8px 5px;font-size:12px;font-family:Segoe UI,arial"><?php echo "$ ".$list['CartProduct']['product_price']; ?></td>
						    			<?php } ?>
						    			<?php if( $list['CartItem']['product_type'] == 'Points' ) { ?>
										<td style="padding:8px 5px;font-size:12px;font-family:Segoe UI,arial">
											<?php echo $list['CartProduct']['product_points']*$list['CartItem']['product_qty'].' '.P; ?>
										</td>
										<?php } else { ?>
										<td style="padding:8px 5px;font-size:12px;font-family:Segoe UI,arial">
											<?php echo "$ ".$list['CartProduct']['product_price']*$list['CartItem']['product_qty']; ?>
										</td>
						    			<?php } ?>
									</tr>
									<?php $i++; endforeach; ?>
									<tr><td></td></tr>
									<tr bgcolor="#fafafa">
										<td colspan="8">
											<table width="100%" cellspacing="0" cellpadding="0" border="0">
												<tbody>
													<tr>
														<td style="width:289px">
															<table width="100%" cellspacing="0" cellpadding="0" border="0">
																<tr>
																	<td style="border-right:1px dotted #dbd9d9;text-align: left; padding-left: 20px;">
																		<p style="font-family:Segoe UI,arial;font-size:12px;line-height:13px"><strong>Shipping Information</strong>: <br>
																		<strong><?php echo $cartlist[0]['CartAddress']['username']; ?></strong><br>
																		<?php echo $cartlist[0]['CartAddress']['address1']; ?><br>
																		<?php echo $cartlist[0]['CartAddress']['city']." - ".$cartlist[0]['CartAddress']['zipcode']; ?><br>
																		<?php echo $cartlist[0]['CartAddress']['state'];
																			if(!empty($cartlist[0]['CartAddress']['state'])) echo ", ";
																			echo $cartlist[0]['CartAddress']['country']; ?><br>
																		<?php echo "Contact Number:".$cartlist[0]['CartAddress']['mobile']; ?></p>
																	</td>
																</tr>
															</table>
														</td>
														<td valign="top" align="right" style="padding-top:10px;width:267px">
									   				 		<?php if( $cartlist[0]['CartItem']['product_type'] == 'Points' ) { ?>
															    <table width="100%" cellspacing="0" cellpadding="0" border="0">
																	<tbody>
																		<tr>
																			<td style="font-size:14px;font-family:Segoe UI,arial;font-weight:bold;text-align:right">Total Points :</td>
																			<td style="width:60px;font-size:14px;font-family:Segoe UI,arial;font-weight:bold;text-align:right;padding-right:20px">
																				<!--<strong><?php echo ($cartlist[0]['CartItem']['product_qty']* $cartlist[0]['CartProduct']['product_points']).' '.P; ?></strong>-->
																				<?php 
																					$total_points = 0;
																					foreach($cartlist as $list) :
																						$points = $list['CartItem']['product_qty']* $list['CartProduct']['product_points'];
																						$total_points+=$points;
																					 endforeach; ?>
																				<strong><?php echo $total_points.' '.P; ?></strong>
																			</td>
																		</tr>
																		<tr>
																			<td style="font-size:14px;font-family:Segoe UI,arial;font-weight:bold;text-align:right">Shipping :</td>
																			<td style="width:60px;font-size:14px;font-family:Segoe UI,arial;font-weight:bold;text-align:right;padding-right:15px">
																			   Free
																			</td>
																		</tr>
																		<tr>
																			<td style="font-size:14px;font-family:Segoe UI,arial;font-weight:bold;text-align:right">Total Points Redeemed :</td>
																			<td style="font-size:14px;font-family:Segoe UI,arial;font-weight:bold;text-align:right;padding-right:15px">
																				<?php 
																					$total_points = 0;
																					foreach($cartlist as $list) :
																						$points = $list['CartItem']['product_qty']* $list['CartProduct']['product_points'];
																						$total_points+=$points;
																					 endforeach; ?>
																				<?php echo $total_points.' '.P; ?>
																				<?php //echo ($cartlist[0]['CartItem']['product_qty']* $cartlist[0]['CartProduct']['product_points']).' '.P; ?>
																			</td>
																		</tr>
																	</tbody>
																</table>
										    				<?php } else { ?>
																<table width="100%" cellspacing="0" cellpadding="0" border="0">
																	<tr>
																		<td style="font-size:14px;font-family:Segoe UI,arial;font-weight:bold;text-align:right">Total Amount :</td>
																		<td style="width:60px;font-size:14px;font-family:Segoe UI,arial;font-weight:bold;text-align:right;padding-right:20px">
																			<?php 
																				$total_amt = 0;
																				foreach($cartlist as $list) :
																				$amt = $list['CartItem']['product_qty']* $list['CartProduct']['product_price'];
																				$total_amt+=$amt;
																				endforeach;
																			?>
																			<strong>
																				<?php echo "$ ".$total_amt;?>
																			</strong>
																		</td>
																	</tr>
																	<tr>
																	   	<td style="font-size:14px;font-family:Segoe UI,arial;font-weight:bold;text-align:right">Shipping Amount :</td>
																		<td style="width:60px;font-size:14px;font-family:Segoe UI,arial;font-weight:bold;text-align:right;padding-right:15px">
																		    <?php $shipping_amt = 0; 
																		    	foreach($cartlist as $list){?>
																		    	<?php 
																		    		if($list['CartProduct']['product_shipping_type']==1) { ?>
																					<?php 
																						$samt1 = $list['CartProduct']['product_shipping_cost'];
																					?>
																					<?php }?>
																					<?php if($list['CartProduct']['product_shipping_type']==2) { ?>
																					<?php 
																						$samt2 = $list['CartItem']['product_qty']* $list['CartProduct']['product_shipping_cost'];
																					?>
																					<?php } ?>
																					<?php $samt3 = $max_group_ship_cost;
																					$samt4 = $counts[0]['CartItem']['smart_shipping_cost'];
																					}?>
																		    <strong>
																		    <?php
																		    $shipping_amt = $samt1 + $samt2 + $samt3+$samt4; echo "$ ".$shipping_amt;?>
																		    </strong>
																		</td>
																	</tr>
																	<tr>
																		<td style="font-size:14px;font-family:Segoe UI,arial;font-weight:bold;text-align:right">Amount Paid :</td>
																		<td style="font-size:14px;font-family:Segoe UI,arial;font-weight:bold;text-align:right;padding-right:15px">
																			<?php $total = $total_amt+$shipping_amt;  echo "$ ".$total;?>
																		</td>
																	</tr>
																</table>
															<?php } ?>
														</td>
													</tr>
												</tbody>
							    			</table>
										</td>
						    		</tr>
						      	</table>
							</td>
						</tr>
						<tr>
							<td style="padding-left:15px;padding-right:15px;font-size:13px">
								<p>Best Regards, <br>
								<strong><?php echo $this->Session->read('client.Client.name');?> Team</strong></p><br>
							</td>
							<td>
								<a onclick="window.print();" href="javascript:void(0)">
									<img title="Print" alt="print" src="/files/styles/icon_images/print.png">
								</a>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>                
	</div>
</div>