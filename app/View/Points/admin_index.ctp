<style type="text/css">
#content
{
	border-left:1px solid #cccccc;
	border-right:1px solid #cccccc;
	border-bottom:1px solid #cccccc;
	margin-left:20px !important;
	padding-left:18px !important;
	width:913px;
}
.rounded-corners 
{
	-moz-border-radius: 10px 10px 0px 0px; /* Firefox */
	-webkit-border-radius: 10px 10px 0px 0px; /* Safari, Chrome */
	border-radius: 10px 10px 0px 0px; /* CSS3 */
}
.current
{
    font-family: 'Droid Sans',sans-serif;
    font-size: 12px;
   	padding:5px 8px !important;   
    background-color: #0066CC;
    border: 1px solid #DFDFDF;
    color:#ffffff;
}
.cpagination1
{
	/*padding:1px 2px 1px 2px;*/
	font-family: 'Droid Sans',sans-serif;
    font-size: 12px;
	text-decoration:none !important;
	background-color: #FFFFFF;
	padding:3px 8px;
	border: 1px solid #0066CC;
	color:#808080 !important;
}
.admin_button
{
	height: 26px;
	padding-bottom: 2px;
	border-radius: 5px 5px 5px 5px;
	background-color: #0066cc;
	border: 1px solid #cccccc;
	color: #FFFFFF; 
	cursor: pointer;
	font-size:13px;
}
</style>
<script>
function update_order_status(id)
	{
	    var status= document.getElementById("status"+id).value;
		var url = '/carts/update_order_status/'+id+'/'+status;
		new Ajax.Request(url, {
			method: 'get',
			onComplete:function(transport) {
				window.location.reload();
			}
		});
	}
function update_shipping_status(id,productid)
{
	var answer = confirm("Are you sure you want to complete the order")
	if (answer)
	{
	   // var modify= document.getElementById("modify"+id).value;
		var url = '/carts/update_shipping_status/'+id+'/'+productid+'/';
		new Ajax.Request(url, {
			method: 'get',
			onComplete:function(transport) {
				window.location.reload();
			}
		});
	}
		
}
function send_shipping_status_mail(orderid)
{
	var answer = confirm("Are you sure you want to send status mail");
	
	if (answer)
	{
		var url = '/carts/send_shipping_status_mail/'+orderid;
		new Ajax.Request(url, {
			method: 'post',
			onComplete:function(response) {
				window.location.reload();
			}
		});
	}
}
</script>
<table style="margin-left:-2px;margin-top: 10px;">
	<tr>
		<td>
           		<img style="width:40px;height:40px;" src="/files/styles/icon_images/points.png" alt="Update Points" title="Update Points" />
       		 </td>
        <td> 
            <h2><?php echo "Update Points Products";?></h2>
        </td>
    </tr>        
</table>
				
<div class="tabber" id="mytab1">
		<div class="tabbertab" style="min-height:210px;overflow:hidden">
	  			<h2>Products List</h2>
	    		<h4>All Products</h4>	
    			<div style="border:1px solid #cccccc;width:768px;min-height:40px;overflow:hidden;padding:20px">	 
					<div>
						<div class="actions" style="padding-bottom:20px;padding-top:10px">
							<table style="width:760px;background-color:#cccccc;height:75px;border:1px solid #969090;">
								<tr>
									<td>
										<form action="<?php echo $this->Html->url('/admin/points/'); ?>" method="get" id="SearchForm" >
											<table>
												<tr>
													<td style="padding-left:10px;"><strong>Find Products</strong></td>
													<td>
														<?php echo $this->Form->input('Product.search', array('div'=>false, 'label'=>false)); ?>
														<script type="text/javascript">
														$(function() {
															if ($('#ProductSearch').val() == '') {
																$('#ProductSearch').val('ID or name');
																$('#ProductSearch').css( "color", "#aaaaaa" );
															}
															$( "#ProductSearch" ).focus(function() {
																if ($(this).val() == 'ID or name') {
																	$(this).val('');
																	$(this).css( "color", "#000" );
																}
															});
															$( "#SearchForm" ).submit(function() {
																if ($("#ProductSearch").val() == 'ID or name') {
																	$("#ProductSearch").val('');
																	$("#ProductSearch").css( "color", "#000" );
																}
															});
														});
														</script>
													</td>
													<td><button class="admin_button">Search</button></td>
												</tr>
											</table>
										</form>
									</td>
									<td>
										<button class ="admin_button" onclick="location.href='<?php echo $this->Html->url('edit'); ?>';">Add New Product</button>
									</td>
								</tr>
							</table>
						</div>
					</div>
					<!-- index data -->
					<div style="min-height:40px;overflow:hidden;">	 
							<div class="list" style="width: 99%;">
								<table cellpadding="0" cellspacing="0" width="100%">
									<tr height="30">
										<th style="width:100px;background-color:#cccccc;text-align:left;padding-left:5px;border-left:1px solid #969090;border-bottom:1px solid #969090;border-top:1px solid #969090;">id</th>
										<th style="width:560px;background-color:#cccccc;text-align:left;padding-left:5px;border-bottom:1px solid #969090;border-top:1px solid #969090;">name</th>
										<th class="actions" style="background-color:#cccccc;text-align:left;padding-left:4px;border-right:1px solid #969090;border-bottom:1px solid #969090;border-top:1px solid #969090;">Actions</th>
									</tr>
									<?php if (empty($searchproducts)) { ?>
									<?php
									$i = 1;
									foreach ($products as $product):
										$class = null;
										if ($i++ % 2 == 0) {
											$class = ' class="altrow"';
										}
									?>
										<tr<?php echo $class;?>>
										<td>
											<?php echo $product['Product']['id'] ?>
										</td>
										<td>
											<?php echo $product['Product']['name'] ?>
										</td>
										<td class="actions">
											<?php echo $this->Html->link(__('Edit', true), array('action'=>'edit', $product['ProductsPoint']['id'])); ?> |
											<?php echo $this->Html->link(__('Delete', true), array('action'=>'delete', $product['ProductsPoint']['id']), null, sprintf(__('Are you sure you want to delete product \'%s\'?', true), $product['Product']['name'])); ?>
										</td>
									</tr>
								<?php endforeach; ?>
								<?php } else { ?>
								
									<tr>
										<td>
											<?php echo $searchproducts[0]['Product']['id'] ?>
										</td>
										<td>
											<?php echo $searchproducts[0]['Product']['name'] ?>
										</td>
										<td class="actions">
											<?php echo $this->Html->link(__('Edit', true), array('action'=>'edit', $searchproducts[0]['ProductsPoint']['id'])); ?> |
											<?php echo $this->Html->link(__('Delete', true), array('action'=>'delete', $searchproducts[0]['ProductsPoint']['id']), null, sprintf(__('Are you sure you want to delete product \'%s\'?', true), $searchproducts['Product']['name'])); ?>
										</td>
									</tr>
									
								<?php } ?>
								</table>
								
								<!-- pagination -->
								<div class="paging" style="padding-top:7px;float:right">
								<table style="border:none !important;">
									<tr>
										<td style="border:none !important;" valign="top">
											<?php echo $this->Paginator->first('<<', array('title'=>'first','class'=>'disabled cpagination1'));?>
											<?php echo $this->Paginator->prev('<', array('title'=>'prev','class'=>'disabled cpagination1'));?>
											<?php echo $this->Paginator->numbers(array('separator' => ' ','class'=>'cpagination1'));?>
											<?php echo $this->Paginator->next('>', array('title'=>'next','class'=>'disabled cpagination1'));?>
											<?php echo $this->Paginator->last('>>', array('title'=>'last','class'=>'disabled cpagination1'));?>
										</td>
										<td style="border:none !important;padding-top:3px;">
										<select name="page" id="selectpage" style="margin-top:-5px;">
											<?php for ($i = 1; $i <= $this->Paginator->counter(array('format' => '%pages%')); $i++):?>
											<option value="<?php echo $i;?>"<?php echo ($this->Paginator->current($params['page']) == $i) ? ' selected="selected"' : ''?>><?php echo $i ?></option>
											<?php endfor ?>
										</select>
										<script>
										$("#selectpage").change(function() {
											var url = window.location.href;
											var n = url.indexOf("/?");
											var extra = "";
											if(n>0){extra = url.substring(n,url.length);}
											window.location.href = '/admin/points/index/page:'+$(this).val()+extra;
										});
										</script>
										</td>
									</tr>
								</table>
							</div>
							<!-- End pagination -->
	  					</div>
					</div>
					<!-- End index data -->
				</div>
			</div>
			<!-- start of second tab data -->
			<div class="tabbertab"  style="height:340px">
			<h2>Add points to Clients</h2>
	    	<h4>Add points to Clients</h4>
				<div style="background-color:#EFEFEF;border:1px solid #cccccc;padding:10px;width:790px;min-height:290px;overflow:hidden">
					<div style="padding:20px">
						<form action="<?php echo $this->Html->url('/admin/points/updatepoints/'); ?>" method="post" id="SearchForm">	
							<table>
								<tr><td><?php echo $count; ?></td></tr>
								<tr>
									<td style="width:150px"><strong>Enter the Points</strong></td>
									<td ><input type="text" name="client_points"></td>
								</tr>
								<tr>
									<td valign="top">
										<strong>Included programs</strong>
									</td>
									<td valign="top">
										<?php 
											echo $this->Form->input('Program.Program', array('multiple' => 'multiple', 'div'=>false, 'label'=>false));
										 ?>
										<input type="hidden" name="data[Program][Program][]" value="" />
										<?php 
											$options = array('url' => 'update_program_clients','update' => 'ClientClient');
					          			 	echo $this->Ajax->observeField('ProgramProgram', $options);
					          			 ?>
									</td>
								</tr>
								<tr>
									<td valign="top">
										<strong>Included clients</strong>
									</td>
									<td valign="top">
										<?php
											 echo $this->Form->input('Client.Client', array('multiple' => 'multiple', 'div'=>false, 'label'=>false)); 
										?>
										<input type="hidden" name="data[Client][Client][]" value="" />
										<br/>
										<sub>Select program to load clients</sub>
									</td>
								</tr>
						</table>
						<div style="float:right;width:450px;margin-top:30px">
							<button class="admin_button" type="submit" >Submit</button>
						</div>		
				</form>		
				</div>
			</div>
		</div>
		<!-- End of second tab data -->
		
		<!-- start of second tab data -->
			<div class="tabbertab"  style="min-height: 340px; overflow: hidden;">
	  			<h2>Manage Orders</h2>
	  			<div class="tabber" id="mytab1" style="margin-left: -40px; width: 816px;">
					<div class="tabbertab" style="min-height:210px;overflow:hidden">
				  			<h2>New Orders</h2>
				    		<h4>New Orders</h4>		 
								<div style="background-color:#EFEFEF;border:1px solid #cccccc;min-height:120px;overflow:hidden;width:752px;">
									<div class="list" style="overflow: hidden; float: left; width: 100%;">
										<table cellpadding="0" cellspacing="0" width="100%">
											<tr height="30">
												<th style="background-color:#cccccc;text-align:left;padding-left:5px;border-left:1px solid #969090;border-bottom:1px solid #969090;border-top:1px solid #969090;"><?php echo 'Order Id';?></th>
												<th style="background-color:#cccccc;text-align:left;padding-left:5px;border-bottom:1px solid #969090;border-top:1px solid #969090;">User Id</th>
												<th style="background-color:#cccccc;text-align:left;padding-left:5px;border-bottom:1px solid #969090;border-top:1px solid #969090;">Product Id</th>
												<th style="background-color:#cccccc;text-align:left;padding-left:5px;border-bottom:1px solid #969090;border-top:1px solid #969090;">Product Name</th>
												<th style="background-color:#cccccc;text-align:left;padding-left:5px;border-bottom:1px solid #969090;border-top:1px solid #969090;">Order Date</th>
												<th class="actions" style="background-color:#cccccc;text-align:left;padding-left:4px;border-right:1px solid #969090;border-bottom:1px solid #969090;border-top:1px solid #969090;"><?php echo "Actions";?></th>
											</tr>
											<?php
											$i = 0;
											foreach ($carts as $cart):
												$class = null;
												if ($i++ % 2 == 0) {
													$class = ' style="background-color: white;"';
												}
											?>
											<tr<?php echo $class;?>>
												<td >
													<?php echo $cart['CartItem']['order_id'] ?>
												</td>
												<td >
													<?php echo $cart['Cart']['user_id'] ?>
												</td>
												<td >
													<?php echo $cart['CartItem']['product_id'] ?>
												</td>
												<td >
													<?php echo $cart['Product']['name'] ?>
												</td>
												<td >
												   <?php echo $cart['CartItem']['created'] ?>
												</td>
												<td class="actions">
													<a onclick="Modalbox.show(this.href, {title: this.title, width: 730,height:300}); return false;" href="/carts/update_order_status/<?php echo $cart['CartItem']['id'];?>/<?php echo $cart['CartItem']['product_id'];?>">Update</a> |
													<a onclick="Modalbox.show(this.href, {title: this.title, width: 730}); return false;" href="/carts/order_show/<?php echo $cart['CartItem']['id'];?>/<?php echo $cart['CartItem']['product_id'];?>">View</a>
												</td>
											</tr>
										<?php endforeach; ?>
										</table>
									</div> 
								</div>
					</div>
				
				<div class="tabbertab" style="min-height:210px;overflow:hidden">
			  			<h2>Shipped Orders</h2>
			  			<h4>Shipped Orders</h4>
			    			<div style="background-color:#EFEFEF;border:1px solid #cccccc;min-height:120px;overflow:hidden;width:90%">
							<div class="list" style="overflow: hidden; float: left; width: 100%; min-height: 205px;">
								<table cellpadding="0" cellspacing="0" width="100%">
									<tr height="30">
										<th style="background-color:#cccccc;text-align:left;padding-left:5px;border-left:1px solid #969090;border-bottom:1px solid #969090;border-top:1px solid #969090;"><?php echo 'Order Id';?></th>
										<th style="background-color:#cccccc;text-align:left;padding-left:5px;border-bottom:1px solid #969090;border-top:1px solid #969090;"><?php echo'User Id';?></th>
										<th style="background-color:#cccccc;text-align:left;padding-left:5px;border-bottom:1px solid #969090;border-top:1px solid #969090;"><?php echo 'Product Id';?></th>
										<th style="background-color:#cccccc;text-align:left;padding-left:5px;border-bottom:1px solid #969090;border-top:1px solid #969090;"><?php echo 'Product Name';?></th>
										<th style="background-color:#cccccc;text-align:left;padding-left:5px;border-bottom:1px solid #969090;border-top:1px solid #969090;">Shipped Date</th>
										<th style="background-color:#cccccc;text-align:left;padding-left:5px;border-bottom:1px solid #969090;border-top:1px solid #969090;">Change Status</th>
										<th class="actions" style="background-color:#cccccc;text-align:left;padding-left:4px;border-right:1px solid #969090;border-bottom:1px solid #969090;border-top:1px solid #969090;"><?php echo "Actions";?></th>
									</tr>
									<?php
									$i = 0;
									foreach ($shipped_cart as $shipped_cart):
										$class = null;
										if ($i++ % 2 == 0) {
											$class = ' style="background-color: white;"';
										}
									?>
									<tr<?php echo $class;?>>
										<td >
											<?php echo $shipped_cart['CartItem']['order_id'] ?>
										</td >
										<td>
											<?php echo $shipped_cart['Cart']['user_id'] ?>
										</td >
										<td >
											<?php echo $shipped_cart['CartItem']['product_id'] ?>
										</td>
										<td >
											<?php echo $shipped_cart['Product']['name'] ?>
										</td>
										<td >
											<?php echo $shipped_cart['CartItem']['shipping_date'] ?>
										</td>
										<td >
										   
										<a href="#update" onclick="update_shipping_status(<?php echo $shipped_cart['CartItem']['id'];?>,<?php echo $shipped_cart['CartItem']['product_id'];?>)">Completed</a>
										</td>
										<td class="actions">
											<a onclick="Modalbox.show(this.href, {title: this.title, width: 730}); return false;" href="/carts/order_show/<?php echo $shipped_cart['CartItem']['id'];?>/<?php echo $shipped_cart['CartItem']['product_id'];?>">View</a>
										</td>
									</tr>
								<?php endforeach; ?>
								</table>
							</div>
					   	</div>
				</div>
				
				<div class="tabbertab" style="min-height:210px;overflow:hidden">
					<h2>Send Mail For Shipped Orders</h2>
					<h4>Send Mail For Shipped Orders</h4>
					<div style="background-color:#EFEFEF;border:1px solid #cccccc;min-height:120px;overflow:hidden;width:90%">
						<div class="list" style="overflow: hidden; float: left; width: 100%; min-height: 205px;">
							<table cellpadding="0" cellspacing="0" width="100%">
								<tr height="30">
									<th style="background-color:#cccccc;text-align:left;padding-left:5px;border-left:1px solid #969090;border-bottom:1px solid #969090;border-top:1px solid #969090;"><?php echo 'Order Id';?></th>
									<th style="background-color:#cccccc;text-align:left;padding-left:4px;border-right:1px solid #969090;border-bottom:1px solid #969090;border-top:1px solid #969090;"></th>
								</tr>

								<?php
								foreach ($total_count as $shipped_cart):
									foreach($shipped_total_count as $cart):
									if($cart['CartItem']['order_id'] == $shipped_cart['CartItem']['order_id'] && $cart[0]['count1'] == $shipped_cart[0][count2])
									{
										$class = null;
										if ($i++ % 2 == 0) {
											$class = ' style="background-color: white;"';
										}
									?>
									<tr<?php echo $class;?>>
									<td >
										<?php echo $shipped_cart['CartItem']['order_id'] ?>
									</td >

									<td class="actions">
										<a href="javascript:void(0);" onclick="send_shipping_status_mail(<?php echo $shipped_cart['CartItem']['order_id'];?>)">Send mail for order <?php echo $shipped_cart['CartItem']['order_id'];?></a>
									</td>
								</tr>
								<?php }endforeach;endforeach; ?>
							</table>
						</div>
					</div>
				</div>
					
				<div class="tabbertab" style="min-height:210px;overflow:hidden">
	  			<h2>Completed Orders</h2>
	    		<h4>Completed Orders</h4>	
    			<div style="background-color:#EFEFEF;border:1px solid #cccccc;width:768px;min-height:40px;overflow:hidden;width:90%">	 
					<div class="list" style="overflow: hidden; float: left; width: 100%;">
					<table cellpadding="0" cellspacing="0" width="100%">
							<tr height="30">
								<th style="background-color:#cccccc;text-align:left;padding-left:5px;border-left:1px solid #969090;border-bottom:1px solid #969090;border-top:1px solid #969090;"><?php echo 'Order Id';?></th>
								<th style="background-color:#cccccc;text-align:left;padding-left:5px;border-bottom:1px solid #969090;border-top:1px solid #969090;"><?php echo'User Id';?></th>
								<th style="background-color:#cccccc;text-align:left;padding-left:5px;border-bottom:1px solid #969090;border-top:1px solid #969090;"><?php echo 'Product Id';?></th>
								<th style="background-color:#cccccc;text-align:left;padding-left:5px;border-bottom:1px solid #969090;border-top:1px solid #969090;"><?php echo 'Product Name';?></th>
								<th style="background-color:#cccccc;text-align:left;padding-left:5px;border-bottom:1px solid #969090;border-top:1px solid #969090;"><?php echo 'Status';?></th>
								<th style="background-color:#cccccc;text-align:left;padding-left:5px;border-bottom:1px solid #969090;border-top:1px solid #969090;"><?php echo 'Completed Date';?></th>
								<th class="actions" style="background-color:#cccccc;text-align:left;padding-left:4px;border-right:1px solid #969090;border-bottom:1px solid #969090;border-top:1px solid #969090;"><?php echo "Actions";?></th>
							</tr>
							<?php
							$i = 0;
							foreach ($complted_cart as $complted_cart):
								$class = null;
								if ($i++ % 2 == 0) {
									$class = ' style="background-color: white;"';
								}
							?>
							<tr<?php echo $class;?>>
								<td >
									<?php echo $complted_cart['CartItem']['order_id'] ?>
								</td >
								<td>
									<?php echo $complted_cart['Cart']['user_id'] ?>
								</td >
								<td >
									<?php echo $complted_cart['CartItem']['product_id'] ?>
								</td>
								<td >
									<?php echo $complted_cart['Product']['name'] ?>
								</td>
								<td >
									<?php echo $complted_cart['CartItem']['cart_status'] ?>
								</td>
								<td>
									<?php echo $complted_cart['CartItem']['completed_date'] ?>
								</td>
								<td class="actions">
									<a onclick="Modalbox.show(this.href, {title: this.title, width: 730}); return false;" href="/carts/order_show/<?php echo $complted_cart['CartItem']['id'];?>/<?php echo $complted_cart['CartItem']['product_id'];?>">View</a>
								</td>
							</tr>
						<?php endforeach; ?>
						</table>
						</div>
				</div>
			</div>
			</div>
		</div>
		
		<!-- start of third tab data-->
			<div class="tabbertab"  style="min-height:340px">
			<h2>Log</h2>
	    	<h4>Log</h4>
				<div style="min-height:40px;overflow:hidden;">	 
							<div class="list" style="width: 99%;">
								<table cellpadding="0" cellspacing="0" width="100%">
									<tr height="30">
				       			<th style="text-align:left;background-color:#cccccc;">User Name</th>
				       			<th style="text-align:left;background-color:#cccccc;">Activity</th>
				       			<th style="text-align:left;background-color:#cccccc;">Points</th>
				       			<th style="text-align:left;background-color:#cccccc;">Activity Date</th>
				       		</tr>
								   	 <?php
								   	 	$i=1;
										foreach ($redeem_history as $gmp)
										{
										$class = null;
										if ($i++ % 2 == 0)
										{
											$class = ' class="altrow"';
										}
									?>
										<tr<?php echo $class;?>>
							      		 			<td  ><?php echo $gmp['User']['username'];?></td>
													<td  ><?php echo $gmp['RedeemTracker']['activity'];?></td>
													<td ><?php echo $gmp['RedeemTracker']['points'];?></td>
													<td ><?php echo $gmp['RedeemTracker']['activity_date'];?></td>
							  			   </tr>
							 	 <?php
							  			}
							  	 ?>
			</table>
			<!-- pagination start-->
				<div class="paging" style="padding-top:7px;float:right">
					<table style="border:none !important;">
						<tr>
							<td style="border:none !important;" valign="top">
								<?php echo $this->Paginator->first('<<', array('title'=>'first','class'=>'disabled cpagination1'));?>
								<?php echo $this->Paginator->prev('<', array('title'=>'prev','class'=>'disabled cpagination1'));?>
								<?php echo $this->Paginator->numbers(array('separator' => ' ','class'=>'cpagination1'));?>
								<?php echo $this->Paginator->next('>', array('title'=>'next','class'=>'disabled cpagination1'));?>
								<?php echo $this->Paginator->last('>>', array('title'=>'last','class'=>'disabled cpagination1'));?>
							</td>
							<td style="border:none !important;padding-top:3px;">
							<select name="page" id="selectpage" style="margin-top:-5px;">
								<?php for ($i = 1; $i <= $this->Paginator->counter(array('format' => '%pages%')); $i++):?>
								<option value="<?php echo $i;?>"<?php echo ($this->Paginator->current($params['page']) == $i) ? ' selected="selected"' : ''?>><?php echo $i ?></option>
								<?php endfor ?>
							</select>
							<script>
							$("#selectpage").change(function() {
								var url = window.location.href;
								var n = url.indexOf("/?");
								var extra = "";
								if(n>0){extra = url.substring(n,url.length);}
								window.location.href = '/admin/points/index/page:'+$(this).val()+extra;
							});
							</script>
							</td>
						</tr>
					</table>
				</div>
				</div>
				<!-- pagination End here-->
			</div>
		</div>
		<!-- End of third tab data-->
</div>