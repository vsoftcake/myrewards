<?php
$result = array();
foreach($products as $product) :
	$key = $product['Product']['id'];
	array_push($result, array("id"=>$product['Product']['product_type'], "label"=>strip_tags($key.' - '.$product['Product']['name']), "value"=>$key));
endforeach;
echo json_encode($result);?>