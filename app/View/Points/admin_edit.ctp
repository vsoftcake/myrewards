<?php echo $this->element('tinymce');?>
<?php echo $this->Html->script('jscolor'); ?>
<style type="text/css">
	#content
	{
		border-left:1px solid #cccccc;
		border-right:1px solid #cccccc;
		border-bottom:1px solid #cccccc;
		margin-left:20px !important;
		padding-left:18px !important;
		width:913px;
	}
	.rounded-corners 
	{
		-moz-border-radius: 10px 10px 0px 0px; /* Firefox */
		-webkit-border-radius: 10px 10px 0px 0px; /* Safari, Chrome */
		border-radius: 10px 10px 0px 0px; /* CSS3 */
	}
	.tr 
	{
		height:35px;
	}
	.tr1
	{
		height:85px;
	}
	.admin_button
	{
		height: 26px;
		padding-bottom: 3px;
		border-radius: 5px 5px 5px 5px;
		background-color: #0066cc;
		border: 1px solid #cccccc;
		color: #FFFFFF; 
		cursor: pointer;
		font-size:13px;
	}
</style>
<script>
	function getClients(pid)
	{
		$.ajax({
			url: '/admin/points/get_product_clients/'+pid,
			type: "GET",
			success: function (data) {
				$('#clients').html(data);
			}
		});
	}
	function getClientsGo()
	{
		pid = document.getElementById('autocomplete').value;
		pos = pid.indexOf("-");
		if(pos== -1)
			productId=pid;
		else
			productId=pid.substr(0,pos)	;
		$.ajax({
			url: '/admin/points/get_product_clients/'+productId,
			type: "GET",
			success: function (data) {
				$('#clients').html(data);
			}
		});
	}
	function loadClientsList(program)
	{
		var pid = program.value;
		if(program.checked)
		{
			$.ajax({
				url: '/admin/points/load_clients/'+pid,
				type: "GET",
				success: function (data) {
					$('#clientslist').append(data);
				}
			});
		}
		else
		{
			$("#ic"+pid).remove();
		}
	}

	function selectAll(pid)
	{
		var aChecked = document.getElementById('checkall'+pid).checked;
		var aId = "ic"+pid;
		var collection = document.getElementById(aId).getElementsByTagName('INPUT');
	    	for (var x=0; x<collection.length; x++) {
		if (collection[x].type.toUpperCase()=='CHECKBOX')
		    collection[x].checked = aChecked;
	    }
}
</script>

<table style="margin-left:-2px;margin-top: 10px;">
	<tr>
		<td>
           <img style="width:40px;height:40px;" src="/files/admin_home_icons/products.png" alt="Products" title="Products">&nbsp;&nbsp;
        </td>
        <td> 
            <h2><?php echo "Add Product";?></h2>
        </td>
    </tr>        
</table>

<form action="<?php echo $this->Html->url('/admin/points/edit/'); ?>" method="post" enctype="multipart/form-data">		
	<div style="width:949px;">
		<div class="tabber" id="mytab1">
			<div class="tabbertab"  style="min-height:300px;overflow:hidden">
	  			<h2><?php echo $this->data['ProductsPoint']['id']>0?"Edit":"Add";?> Product</h2>
	    		<h4><?php echo $this->data['ProductsPoint']['id']>0?"Edit":"Add";?> Product</h4>
				<div style="background-color:#EFEFEF;border:1px solid #cccccc;padding:10px;width:790px;overflow:hidden">
					<div style="padding:20px;">
						<table>
							<tr class="tr">
									<td style="padding-bottom:10px;width:100px;"><strong>Product Id</strong></td>
									<td>
										<?php 
											if(empty($this->data['ProductsPoint']['product_id'])):
												echo $this->Form->input('ProductsPoint.product_id',array('id'=>'autocomplete', 'class'=>'autocomplete', 'div'=>false, 'label'=>false, 'type'=>'text'));
												echo $this->Form->button('Go!', array('onclick' => "getClientsGo()",'type'=>'button','class'=>'admin_button','style'=>'width:60px;'));	
											?> 
										<?php 
											else :
												echo $this->Form->input('ProductsPoint.product_id', array('div'=>false, 'label'=>false, 'type'=>'text'));
											endif;
											echo $this->Form->input('ProductsPoint.product_type', array('type'=>'hidden'));
										?>
									<script type="text/javascript">
									$(function() {
										$( "#autocomplete" ).autocomplete({
											source: '/admin/points/products/', minLength: 4,
											select: function( event, ui ) {$("#ProductsPointProductType").val(ui.item.id);if(ui.item.id==0){getClients(ui.item.value)};}
										});
									}); 
									</script>
									</td>
							</tr>
						</table>
										
						<table>
							<?php if(!empty($this->data['ProductsPoint']['product_id']) && $this->data['ProductsPoint']['product_type']==0): ?>
							<tr class="tr1">
								<td valign="top">
									<table>
										<tr>
											<td valign="top" style="width:90px"><strong>Programs</strong></td>
											<td valign="top">
												<div style="width:300px;min-height:100px">
													<div style="float:left;height:350px;width:295px;background-color:#fff;overflow:auto;border:1px solid #ccc;margin-left:10px;">
														<?php 
															$check = "checked= 'checked'";
														?>
														<?php foreach ($programsList as $pval=>$pname):?>
															<input type="checkbox" id="ProgramProgram" <?php if(in_array($pval,$programs)) echo $check; ?> value="<?php echo $pval;?>" onclick="loadClientsList(this)" name="data[Program][Program][]" class="checkbox">
															&nbsp;&nbsp;<?php echo $pname ?><br>
														<?php endforeach; ?>
													</div>
												</div>
											</td>
										</tr>
									</table>
								</td>
								<td style="padding-left:18px" valign="top">
									<table>
										<tr>
											<td valign="top" style="width:70px"><strong>Clients</strong></td>
											<td valign="top">
												<div style="width: 250px; overflow: auto; height: 350px;background-color:#fff;border:1px solid #ccc;" id="clientslist">
													<?php $pid=0;?>
													<?php foreach ($allclients as $client):?>
													<?php if($pid != $client['Client']['program_id']){?>
														<?php if($pid>0){?></div><?php }?>
													<div style="width:245px;background-color:#fff;overflow:auto;border:1px solid #ccc;padding-bottom:5px;" id="ic<?php echo $client['Client']['program_id'] ?>">
													<strong><?php echo $client['Program']['name']; ?></strong><br>

													<input type="checkbox" class="checkbox" id="checkall<?php echo $client['Client']['program_id'] ?>" onclick="selectAll('<?php echo $client['Client']['program_id'] ?>');"><span style="color:red"><b>Select All Clients</b></span><br>
													<?php }?>
													<input type="checkbox" name="data[Client][Client][]" value="<?php echo $client['Client']['id'];?>" <?php if(in_array($client['Client']['id'], $clients)) : echo $check; endif;?> class="checkbox"/>
													&nbsp;&nbsp;<?php echo $client['Client']['name'] ?><br>
													<?php $pid = $client['Client']['program_id'];
													endforeach?>
													</div>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<?php else : ?>
							<tr>
								<td id="clients" colspan="2">

								</td>
							</tr>
						<?php endif; ?>
						</table>
									
						<table>
							<tr class="tr">
									<td style="padding-bottom:10px;width:100px;"><strong>Redeem Value</strong></td>
									<td><?php echo $this->Form->input('ProductsPoint.points', array('div'=>false, 'label'=>false, 'type'=>'text')); ?></td>
							</tr>
							<tr class="tr">
								<td style="padding-bottom:10px;"><strong>Qty</strong></td>
								<td>
									<?php echo $this->Form->input('CartProduct.product_quantity',array('value'=>$productid['0']['CartProduct']['product_quantity'], 'div'=>false, 'label'=>false, 'type'=>'text')); ?><br/>
								</td>
							</tr>
							<tr class="tr">
								<td style="padding-bottom:10px;"><strong>Active</strong></td>
								<td>
									<?php echo $this->Form->checkbox('ProductsPoint.active'); ?><br/>
								</td>
							</tr>
							<tr class="tr">
									<td><strong>Hot Offer Image</strong></td>
									<td style="width:373px"><?php echo $this->Form->file('ProductsPoint.file'); ?>
									<br>
									Remove 
									<?php echo $this->Form->input('ProductsPointImage1Delete.file',array('type'=>'checkbox', 'div'=>false, 'label'=>false)); ?>
									</td>
							</tr>
							<tr>
								<td></td>
								<td class="bl">
										<?php 
											$filepath = WWW_ROOT.'/files/hot_offer_img_points/';
											$filename = $this->Form->value('ProductsPoint.product_id'). '.'. $this->Form->value('ProductsPoint.logo_extension');
											if (file_exists($filepath. $filename)) :
											$path = $this->Html->url('/files/hot_offer_img_points/'. $this->Form->value('ProductsPoint.product_id'). '.'. $this->Form->value('ProductsPoint.logo_extension')); 
											$path_prod = substr($path,strpos($path)+1);
											$img=$this->ImageResize->getResizedDimensions($path_prod, 200, 100);
										?>
										<img width="<?php echo $img['width'];?>" height="<?php echo $img['height'];?>" src="<?php echo $this->Html->url(FILES_WWW_PATH. 'hot_offer_img_points/'. $this->Form->value('ProductsPoint.product_id'). '.'. $this->Form->value('ProductsPoint.logo_extension')); ?>" alt="" />
										<?php endif;?>
									</td>
							</tr>
							
							<tr>
								<td><p></p></td>
							</tr>
							<tr>
								<td colspan="2" style="padding-left: 200px; padding-top: 20px;">
									<?php echo $this->Form->button('Cancel', array('onclick' => "location.href='/admin/points'",'class'=>'admin_button','style'=>'width:60px;','type'=>'button'));  ?>
									<button class="admin_button" type="submit" onclick="toggleSpellchecker()">Submit</button>
								</td>
							</tr>
						</table>
					</div>
				</div>
	 		</div>
	</div>
</form>