<style type="text/css">
	.rounded-corners 
	{
		-moz-border-radius: 10px 10px 0px 0px; /* Firefox */
		-webkit-border-radius: 10px 10px 0px 0px; /* Safari, Chrome */
		border-radius: 10px 10px 0px 0px; /* CSS3 */
	}
	.tr 
	{
		height:45px;
	}
	.admin_button
	{
		height: 26px;
		padding-bottom: 2px;
		border-radius: 5px 5px 5px 5px;
		background-color: #0066cc;
		border: 1px solid #cccccc;
		color: #FFFFFF; 
		cursor: pointer;
		font-size:13px;
	}
</style>
<?php echo $this->element('tinymce');?>

<div style="width:953px;border-left:1px solid #cccccc;border-right:1px solid #cccccc;border-bottom:1px solid #cccccc;margin-left:-14px;">
	<div style="width:949px;height:60px;">
		<div style=" height: 40px; float:left; width: 608px;padding-left:37px;">
	       	<div style=" height: 40px; float:left;padding-top:5px;">
	        	<img title="Clients" alt="Clients" src="/files/admin_home_icons/clients.png" style="width:40px;height:40px;">
	        </div>
	        <div style=" height: 40px;float:left;padding-left:10px;">
	        	<h2>Client <?php if($client['Client']['name']!=''): ?> - <?php echo $client['Client']['name']; ?> <?php endif;?></h2>
	        	<h4>Edit ClientPage <?php if($this->Form->value('ClientPage.page_title')!=''): ?> - <?php echo $this->Form->value('ClientPage.page_title') ?> <?php endif;?></h4>
	        </div> 
		</div>
        <div class="actions" style=" height: 40px; float:left;  width: 300px;">
			<ul style="padding:20px;">
				<li style="display:inline;"><?php echo $this->Form->button('Cancel', array('onclick' => "location.href='/admin/clients/edit/".$client['Client']['id']."'",'class'=>'admin_button','style'=>'width:60px;'));  ?></li>
	        </ul>
		</div>
	</div>

	<form action="<?php echo $this->Html->url('/admin/client_pages/edit/'. $this->Html->value('ClientPage.id'). '/'. $this->Html->value('ProgramPage.program_id'). '/'. $this->Html->value('ProgramPage.page_id').'/'.$dashboard_id); ?>" method="post">
		<div class="tabber" id="mytab1">
			<div class="tabbertab"  style="min-height:100px;overflow:hidden">
	  			<h2>General Information</h2>
	    		<h4>ClientPage Information</h4>
	 			<div style="background-color:#EFEFEF;border:1px solid #cccccc;padding:10px;width:790px;min-height:100px;overflow:hidden;font-weight:bold">
	 				The following field replacements are available:<br/>
					[[client_name]] replaced by the name of the client<br/>
					[[client_email]] replaced by the clients email address<br/>
					[[program_name]] replaced by the name of the program<br/>
					e.g. "Welcome to [[client_name]] online" will result in "Welcome to My Rewards online"
	 			</div>
	 			
	 			<div align="center" style="padding-top:10px;">
					<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(1)">Next</button>
		       	</div>
	     	</div>
	     	
	     	<div class="tabbertab"  style="min-height:400px;overflow:hidden">
	  			<h2>Page Details</h2>
	    		<h4>Page Details</h4>
	 			<div style="background-color:#EFEFEF;border:1px solid #cccccc;padding:10px;width:790px;min-height:475px;overflow:hidden">
	 				<table>
						<tr class="tr">
							<td></td>
							<td><strong>Use Default setting?</strong></td>
							<td></td>
						</tr>
						<tr class="tr">
							<td><strong>Enabled</strong></td>
							<td><?php echo $this->Form->checkbox('ClientPage.enabled_override', array('onclick' => "updateOverride(this, 'ClientPageEnabled')")); ?></td>
							
							<td>
							<?php if($this->Html->value('ClientPage.page_id')!=60 && $this->Html->value('ClientPage.page_id')!=61 && $this->Html->value('ClientPage.page_id')!=63 && $this->Html->value('ClientPage.page_id')!=73):?>
								<?php 
								($this->Form->value('ClientPage.enabled_override') == 1)? $options = array('disabled' => 'disabled'): $options = array();
								echo $this->Form->checkbox('ClientPage.enabled', $options); 
								?>
							<?php endif; ?>
							</td>
						</tr>
						<tr>
							<td><strong>URL</strong><br/><sub>Displayed in the browser address bar</sub></td>
							<td></td>
							<td>
								<?php echo $this->Form->value('Page.name'); ?>
							</td>
						</tr>
						<tr class="tr">
							<td><strong>Page title</strong><br/><sub>Displayed in the browser title bar</sub></td>
							<td><?php echo $this->Form->checkbox('ClientPage.page_title_override', array('onclick' => "updateOverride(this, 'ClientPagePageTitle')")); ?></td>
							<td>
								<?php
									($this->Form->value('ClientPage.page_title_override') == 1)? $options = array('disabled' => 'disabled'): $options = array();
									echo $this->Form->input('ClientPage.page_title', array('div'=>false, 'label'=>false)+$options); 
								?>
							</td>
						</tr>
						<tr class="tr">
							<td><strong>Meta Description</strong></td>
							<td><?php echo $this->Form->checkbox('ClientPage.meta_description_override', array('onclick' => "updateOverride(this, 'ClientPageMetaDescription')")); ?></td>
							<td>
								<?php
									($this->Form->value('ClientPage.meta_description_override') == 1)? $options = array('disabled' => 'disabled'): $options = array();
									echo $this->Form->input('ClientPage.meta_description', array('div'=>false, 'label'=>false)+$options); 
								?>
							</td>
						</tr>
						<tr class="tr">
							<td><strong>Meta Keywords</strong></td>
							<td><?php echo $this->Form->checkbox('ClientPage.meta_keywords_override', array('onclick' => "updateOverride(this, 'ClientPageMetaKeywords')")); ?></td>
							<td>
								<?php
									($this->Form->value('ClientPage.meta_keywords_override') == 1)? $options = array('disabled' => 'disabled'): $options = array();
									echo $this->Form->input('ClientPage.meta_keywords', array('div'=>false, 'label'=>false)+$options); 
								?>
							</td>
						</tr>
						<tr class="tr">
							<td><strong>Meta Robots</strong></td>
							<td><?php echo $this->Form->checkbox('ClientPage.meta_robots_override', array('onclick' => "updateOverride(this, 'ClientPageMetaRobots')")); ?></td>
							<td>
								<?php
									($this->Form->value('ClientPage.meta_robots_override') == 1)? $options = array('disabled' => 'disabled'): $options = array();
									echo $this->Form->input('ClientPage.meta_robots', array('div'=>false, 'label'=>false)+$options); 
								?>
							</td>
						</tr>
						<tr class="tr">
							<td><strong>Navigation Menu Title</strong></td>
							<td><?php echo $this->Form->checkbox('ClientPage.title_override', array('onclick' => "updateOverride(this, 'ClientPageTitle')")); ?></td>
							<td>
								<?php
									($this->Form->value('ClientPage.title_override') == 1)? $options = array('disabled' => 'disabled'): $options = array();
									echo $this->Form->input('ClientPage.title', array('div'=>false, 'label'=>false)+$options); 
								?>
							</td>
						</tr>
						<?php if($this->Html->value('ClientPage.page_id')!=62):?>
						<tr class="tr">
							<td><strong>Heading 1</strong></td>
							<td><?php echo $this->Form->checkbox('ClientPage.h1_override', array('onclick' => "updateOverride(this, 'ClientPageH1')")); ?></td>
							<td>
								<?php
									($this->Form->value('ClientPage.h1_override') == 1)? $options = array('disabled' => 'disabled'): $options = array();
									echo $this->Form->input('ClientPage.h1', array('div'=>false, 'label'=>false)+$options); 
								?>
							</td>
						</tr>
						<?php endif;?>
						<tr class="tr">
							<td><strong>Heading 2</strong></td>
							<td><?php echo $this->Form->checkbox('ClientPage.h2_override', array('onclick' => "updateOverride(this, 'ClientPageH2')")); ?></td>
							<td>
								<?php
									($this->Form->value('ClientPage.h2_override') == 1)? $options = array('disabled' => 'disabled'): $options = array();
									echo $this->Form->input('ClientPage.h2', array('div'=>false, 'label'=>false)+$options); 
								?>
							</td>
						</tr>
						<?php if($this->Html->value('ClientPage.page_id')!=62):?>
						<tr class="tr">
							<td><strong>Heading 3</strong></td>
							<td><?php echo $this->Form->checkbox('ClientPage.h3_override', array('onclick' => "updateOverride(this, 'ClientPageH3')")); ?></td>
							<td>
								<?php
									($this->Form->value('ClientPage.h3_override') == 1)? $options = array('disabled' => 'disabled'): $options = array();
									echo $this->Form->input('ClientPage.h3', array('div'=>false, 'label'=>false)+$options); 
								?>
							</td>
						</tr>
						<tr class="tr">
							<td><strong>Display special offers?</strong></td>
							<td><?php echo $this->Form->checkbox('ClientPage.special_offers_override', array('onclick' => "updateOverride(this, 'ClientPageSpecialOffers')")); ?></td>
							<td>
								<?php
									($this->Form->value('ClientPage.special_offers_override') == 1)? $options = array('disabled' => 'disabled'): $options = array();
									echo $this->Form->checkbox('ClientPage.special_offers', $options); 
								?>
							</td>
						</tr>
						<tr class="tr">
							<td><strong>Display News?</strong></td>
							<td><?php echo $this->Form->checkbox('ClientPage.whats_new_module_override', array('onclick' => "updateOverride(this, 'ClientPageWhatsNewModule')")); ?></td>
							<td>
								<?php
									($this->Form->value('ClientPage.whats_new_module_override') == 1)? $options = array('disabled' => 'disabled'): $options = array();
									echo $this->Form->checkbox('ClientPage.whats_new_module', $options); 
								?>
							</td>
						</tr>
						<?php endif;?>
					</table>
	 			</div>
	 			
	 			<div align="center" style="padding-top:10px;">
					<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(0)">Previous</button>
					<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(2)">Next</button>
		       	</div>
	 		</div>
	 		
	 		<div class="tabbertab"  style="min-height:200px;overflow:hidden">
	  			<h2>Content</h2>
	    			<h4>Content</h4>
	 			<div style="background-color:#EFEFEF;border:1px solid #cccccc;padding:10px;width:790px;min-height:200px;overflow:hidden">
	 				<table style="width:100%">
	 					<tr class="tr">
							<td><strong>Content</strong></td>
							<td><?php echo $this->Form->checkbox('ClientPage.content_override', array('onclick' => "updateOverride(this, 'ClientPageContent')")); ?></td>
							
							<td>
								<?php
									($this->Form->value('ClientPage.content_override') == 1)? $options = array('disabled' => 'disabled'): $options = array();
									echo $this->Form->input('ClientPage.content', array('div'=>false, 'label'=>false)+$options); 
								?>
								<?php if($this->Html->value('ClientPage.page_id')==73): ?>
									<br/>
									<span style="color:red">For better appearance place image with width 350px and height 430px</span>
								<?php endif; ?>
							</td>
						</tr>
	 				</table>
	 			</div>
	 			
	 			<div align="center" style="padding-top:10px;">
					<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(1)">Previous</button>
					<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(3)">Next</button>
					<button type="submit" class="admin_button" onclick="toggleSpellchecker()">Submit</button>
		       	</div>
	 		</div>
	 		
	 		<div class="tabbertab"  style="min-height:200px;overflow:hidden">
	  			<h2>Change Log</h2>
	    		<h4>Change Log</h4>
	 			<div style="background-color:#EFEFEF;border:1px solid #cccccc;padding:10px;width:790px;min-height:200px;overflow:hidden">
	 				<div id="Log" style="margin:0;">Retrieving changes...</div>
					<script type="text/javascript">
					$(function() {
						$.ajax({
							url: "/admin/logs/ajax_list_new/<?php echo Inflector::singularize($this->name);?>/<?php echo $this->Form->value(Inflector::singularize($this->name). '.id');?>",
							type: "GET",
							success: function (data) {
								$('#Log').html(data);
							}
						});
					});</script>
	 			</div>
	 			
	 			<div align="center" style="padding-top:10px;">
					<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(2)">Previous</button>
		       	</div>
	 		</div>
	 		
		</div>	
	    <?php echo $this->Form->input('ClientPage.id', array('type' => 'hidden')); ?>
		<?php echo $this->Form->input('ClientPage.client_id', array('type' => 'hidden')); ?>
		<?php echo $this->Form->input('ClientPage.page_id', array('type' => 'hidden')); ?>
		<?php echo $this->Form->input('ClientPage.dashboard_id', array('type' => 'hidden','value'=>$dashboard_id)); ?>
	</form>
</div>

<script type="text/javascript">

	function updateOverride(checkbox, input) {
		if ($(checkbox).prop("checked") == true) {
			$("#"+input).attr("disabled", true);
		} else {
			$("#"+input).attr("disabled", false);
		}
	}
	
	function toggleSpellchecker()
	{
		var val = navigator.userAgent.toLowerCase();
		if(val.indexOf("msie") < 0)
		{
			tinyMCE.execCommand('mceFocus', false, 'ClientPageContent');
			tinyMCE.execCommand('mceSpellCheck', false, 'ClientPageContent');
		}
	}

</script>