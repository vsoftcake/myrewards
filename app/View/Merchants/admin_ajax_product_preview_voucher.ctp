<table style="width: 100%">
	<tr>
		<td>Voucher: <?php echo $this->Form->input('Voucher.id', array('onchange' => 'previewVoucherFor(this.value);', 'div'=>false,'label'=>false, 'type'=>'select', 'options'=>$vouchers, 'selected'=>$voucher['id'])); ?></td>
		<td><a href="javascript:void(0)" onclick="return false;">close</a></td>
	</tr>
</table>
<br/>

<?php echo $this->element('merchant_voucher'); ?>
<script type="text/javascript">

$( "#VoucherId" ).click(function() {
	Event.stop(event);
});

previewVoucherFor = function(voucher_id) {
	previewVoucher(voucher_id);
	previewVoucherDisplay();
}

//	center the div
f_clientWidth = function(){
	return f_filterResults (
		window.innerWidth ? window.innerWidth : 0,
		document.documentElement ? document.documentElement.clientWidth : 0,
		document.body ? document.body.clientWidth : 0
	);
}
f_clientHeight = function()  {
	return f_filterResults (
		window.innerHeight ? window.innerHeight : 0,
		document.documentElement ? document.documentElement.clientHeight : 0,
		document.body ? document.body.clientHeight : 0
	);
}
f_scrollLeft = function() {
	return f_filterResults (
		window.pageXOffset ? window.pageXOffset : 0,
		document.documentElement ? document.documentElement.scrollLeft : 0,
		document.body ? document.body.scrollLeft : 0
	);
}
f_scrollTop = function() {
	return f_filterResults (
		window.pageYOffset ? window.pageYOffset : 0,
		document.documentElement ? document.documentElement.scrollTop : 0,
		document.body ? document.body.scrollTop : 0
	);
}
f_filterResults = function(n_win, n_docel, n_body) {
	var n_result = n_win ? n_win : 0;
	if (n_docel && (!n_result || (n_result > n_docel)))
		n_result = n_docel;
	return n_body && (!n_result || (n_result > n_body)) ? n_body : n_result;
}

//$('preview_center').style.top = ((f_clientHeight()- $('preview_center').offsetHeight) /2) + 'px';
var top = ((f_clientHeight() - $('preview_center').offsetHeight) /2) + f_scrollTop();
if (top > 0) {
	$('preview_center').style.marginTop = top + 'px';
} else {
	$('preview_center').style.marginTop = f_scrollTop() + 'px';
}

previewVoucherDisplay();

</script>