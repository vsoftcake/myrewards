<script type="text/javascript">
$('ProductMessage').innerHTML = 'Product deleted';

function reloadProducts() {
	$('ProductMessage').style.display = 'none';
	$('Products').innerHTML = 'Retreving products...';
	$('product_list').style.display = 'block';
	new Ajax.Updater(window.parent.$('Products'), '<?php 
		if ($this->Session->check('Merchants.search_url.Product')) {
			$url = $this->Session->read('Merchants.search_url.Product');
		} else {
			$url = $this->Html->url('/admin/merchants/ajax_products/'. $this->Form->value('Product.merchant_id'));
		}
		echo $url; ?>', {evalScripts: true });
}

setTimeout(reloadProducts, 1000);

</script>

