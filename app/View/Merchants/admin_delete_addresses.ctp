<style>
	#content
	{
		border-left:1px solid #cccccc;
		border-right:1px solid #cccccc;
		border-bottom:1px solid #cccccc;
		margin-left:20px !important;
		padding-left:15px !important;
		width:916px;
	}
</style>
<?php if ($step == '1') : ?>
	<h2>Delete Merchant Addresses</h2>
	
	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link('Cancel', '/admin/merchants'); ?></li>
		</ul>
	</div>
	
	<div class="edit">
		<p>
			File must be in csv format with the following column:<br/>
			<ul>
				<li>Merchant Address name (required) </li>
			</ul>
		</p>
		<p>
			<a href="<?php echo $this->Html->url('/admin/merchants/example_address_delete'); ?>">Download example</a>
		</p>
		<form action="<?php echo $this->Html->url('/admin/merchants/delete_addresses/'.$id.'/2'); ?>" method="post" enctype="multipart/form-data">
			<table>
				<tr>
					<td class="bt">CSV file</td>
					<td class="bt"><?php echo $this->Form->file('Merchant.addresses'); ?></td>
					<td class="bl"><?php if (isset($upload_error)) { echo $upload_error; } ?></td>
				</tr>
				<tr>
					<td class="bb"></td>
					<td class="bb br"><button type="submit">Submit</button></td>
				</tr>
			</table>
		</form>
	</div>
<?php elseif ($step == '2') : ?>
	<h2>Confirm delete</h2>
	
	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link('Cancel', '/admin/merchants'); ?></li>
		</ul>
	</div>
	
	<br/>
	
	<?php echo $uploaded_addresses; ?> Merchant addresses uploaded.<br/>
	<?php echo $delete_merchants; ?> Merchant addresses will be deleted.
	
	<div style="clear:both">
		<p><a href="<?php echo $this->Html->url('/admin/merchants/delete_addresses/'.$id.'/3'); ?>">Confirm</a></p>
	</div>
<?php elseif ($step == '3') : ?>
	<h2>delete complete</h2>
	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link('Done', '/admin/merchants'); ?></li>
		</ul>
	</div>
	<?php echo $address_count; ?> merchant addresses deleted.
<?php endif; ?>