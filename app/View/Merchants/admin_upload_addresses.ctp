<style>
	#content
	{
		border-left:1px solid #cccccc;
		border-right:1px solid #cccccc;
		border-bottom:1px solid #cccccc;
		margin-left:20px !important;
		padding-left:15px !important;
		width:916px;
	}
</style>
<?php if ($step == '1') : ?>
<h2>Upload merchant addresses</h2>
<div class="actions">
	<ul>
		<li><?php echo $this->Html->link('Cancel', '/admin/merchants/edit/'.$id); ?></li>
	</ul>
</div>
<div class="edit">
<p>
	File must be in csv format with the following columns, column name must be in the first row, leave empty if not required:<br/>
	<ul>
		<li>primary - if the address is the primary address (only one address can be primary) </li>
		<li>name</li>
		<li>address1</li>
		<li>address2</li>
		<li>suburb</li>
		<li>city</li>
		<li>state - Australian state, one of ACT, NSW, NT, QLD, SA, TAS, VIC, WA</li>
		<li>region - NZ region, one of Auckland, Bay of Islands, Central N Island, Central S Island, Christchurch, Dunedin, Lower N Island, Lower S Island, Nationwide, Nelson, Q'town/Wanaka, Rotorua/Taupo, Upper N Island, Upper S Island, Wellington</li>
		<li>country - one of Australia, New Zealand (default Australia if empty)</li>
		<li>postcode</li>
		<li>phone</li>
	</ul>
</p>
<p><a href="<?php echo $this->Html->url('/admin/merchants/example_merchant_addresses_import'); ?>">Download example</a></p>
<form action="<?php echo $this->Html->url('/admin/merchants/upload_addresses/'.$id.'/2'); ?>" method="post" enctype="multipart/form-data">
	<table>
		<tr>
			<td class="bt">CSV file</td>
			<td class="bt">
				<?php echo $this->Form->file('Merchant.addresses'); ?>
				<br/>
				Max file size: <?php echo (ini_get('post_max_size') < ini_get('upload_max_filesize'))? ini_get('post_max_size'): ini_get('upload_max_filesize'); ?>
			</td>
			<td class="bl"><?php if (isset($upload_error)) { echo $upload_error; } ?></td>
		</tr>
		<tr>
			<td class="bb"></td>
			<td class="bb br"><button type="submit">Submit</button></td>
		</tr>
	</table>
	</form>
</div>
<?php elseif ($step == '2') : ?>
<h2>Confirm import</h2>
<div class="actions">
	<ul>
		<li><?php echo $this->Html->link('Cancel', '/admin/merchants/edit/'.$id); ?></li>
	</ul>
</div>
	<br/>
	<?php echo $uploaded_addresses; ?> addresses sent, <?php echo $good_addresses; ?> addresses will be imported.
	<?php if (!empty($upload_errors)) : ?>
		<h5>The following rows contain errors:</h5>
		<div class="edit">
		<table>
			<tr>
				<td class="bt">Row</td><td class="bt br">Error</td>
			</tr>
		<?php foreach ($upload_errors as $error) : ?>
			<tr><td><?php echo $error['row']; ?></td><td class="br"><?php echo $error['error']; ?></td></tr>
		<?php endforeach; ?>
			<tr>
				<td class="bb"></td><td class="bb br"></td>
			</tr>
		</table>
		</div>
	<?php endif; ?>
<div style="clear:both">
	<br/>
	<p><a href="<?php echo $this->Html->url('/admin/merchants/upload_addresses/'.$id.'/3'); ?>">Confirm</a> or</p>
	<p><a href="javascript:void(0)" onclick=" $('reupload').style.display = 'block'; return false;">Re-upload</a></p>
	<div class="edit" style="display:none;" id="reupload">
	<form action="<?php echo $this->Html->url('/admin/merchants/upload_addresses/'.$id.'/2'); ?>" method="post" enctype="multipart/form-data">
		<table>
			<tr>
				<td class="bt">CSV file</td>
				<td class="bt">
					<?php echo $this->Form->file('Merchant.addresses'); ?><br/>
					Max file size: <?php echo (ini_get('post_max_size') < ini_get('upload_max_filesize'))? ini_get('post_max_size'): ini_get('upload_max_filesize'); ?>
				</td>
				<td class="bl"><?php if (isset($upload_error)) { echo $upload_error; } ?></td>
			</tr>
			<tr>
				<td class="bb"></td>
				<td class="bb br"><button type="submit">Submit</button></td>
			</tr>
		</table>
		</form>
	</div>
</div>

<?php elseif ($step == '3') : ?>
<h2>Import complete</h2>
<div class="actions">
	<ul>
		<li><?php echo $this->Html->link('Back to merchant', '/admin/merchants/edit/'.$id); ?></li>
	</ul>
</div>
<?php echo $address_count; ?> addresses imported.
<?php endif; ?>