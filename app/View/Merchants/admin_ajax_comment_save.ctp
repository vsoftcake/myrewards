<?php echo str_replace("'", "\'",$this->Form->error('MerchantComment.description', array('required' => 'Description required','name'=>'data[MerchantComment][description]'))); ?>

<script type="text/javascript">
<?php if (isset($error)) : ?>
	
	$('message').innerHTML = '<div id="flashMessage" class="message">Please correct the errors below.</div>';
	$('NewMerchantComment').style.display = 'table-row';
	$('NewMerchantComment2').style.display = 'table-row';
	$('NewMerchantCommentSaving').style.display = 'none';

<?php else : ?>
	$('#message').html('<div id="flashMessage" class="message">Comment saved.</div>');
	$('#NewMerchantCommentSaving').hide();
	$('#Comments').html('Retrieving comments...');
	$.ajax({
		url: '/admin/merchants/ajax_comments/<?php echo $id;?>',
		type: "GET",
		success: function (data) {
		
			$('#Comments').html(data);
		}
	});
<?php endif; ?>
</script> 