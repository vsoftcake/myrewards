<?php $this->Paginator->options(
		array(
			'update' => '#Products',
			'evalScripts' => true,
			'url' => $this->params['pass'],
			'model'=>'Product',
			'sort' => 'Product.name'));
			
	?>
	<table cellpadding="0" cellspacing="0" width="760px;">
	<tr style="text-align:left;">
		<th><?php echo $this->Paginator->sort('id', 'Product.id');?></th>
		<th><?php echo $this->Paginator->sort('Name', 'Product.name');?></th>
		<th class="actions"><?php echo "Actions";?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($products as $product):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
		<tr<?php echo $class;?>>
			<td>
				<?php echo $product['Product']['id'] ?>
			</td>
			<td>
				<?php echo $product['Product']['name'] ?>
			</td>
			<td class="actions">
				<!--<a href="javascript:void(0)" onclick="return editProduct(<?php echo $product['Product']['id']; ?>);">Edit</a>-->
				<?php echo $this->Html->link(__('edit',true),array('controller'=>'products','action'=>'edit',$product['Product']['id']),array('target'=>'_blank')); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
	
	<div class="paging" style="width:760px">
		<table style="border:none !important;padding-top:15px;float:right;">
			<tr>
				<td style="border:none !important;" valign="top">
					<?php echo $this->Paginator->first('<<', array('title'=>'first','class'=>'disabled cpagination1'));?>
					<?php echo $this->Paginator->prev('<', array('title'=>'prev','class'=>'disabled cpagination1'));?>
					<?php echo $this->Paginator->numbers(array('separator' => ' ','class'=>'cpagination1'));?>
					<?php echo $this->Paginator->next('>', array('title'=>'next','class'=>'disabled cpagination1'));?>
					<?php echo $this->Paginator->last('>>', array('title'=>'last','class'=>'disabled cpagination1'));?>
				</td>
				<td style="border:none !important;padding-left:5px;padding-top:1px;">
					<p><?php echo $this->Paginator->counter(array('format' => __('Page %page% of %pages%', true)));	echo $this->Js->writeBuffer();?></p>
			  	</td>
			</tr>
		</table>
	</div>

<script type="text/javascript">

	editProduct = function(product_id) {
		$('Product').innerHTML = 'Retreiving product...';
		$('product_list').style.display = 'none';
		$('Product').style.display = 'block';
		new Ajax.Updater('Product', '<?php echo $this->Html->url('/admin/merchants/ajax_product/'); ?>' + product_id, {evalScripts: true }); 
		return false;
	}

</script>