<?php if ($step == '1') : ?>
<h2>Upload merchant products</h2>
<div class="actions">
	<ul>
		<li><?php echo $this->Html->link('Cancel', '/admin/merchants/edit/'.$id); ?></li>
	</ul>
</div>
<div class="edit">
<form action="<?php echo $this->Html->url('/admin/merchants/upload_products/'.$id.'/2'); ?>" method="post" enctype="multipart/form-data">
	<table>
		<tr>
			<td class="bt">CSV file</td>
			<td class="bt">
				<?php echo $this->Form->file('Merchant.products'); ?>
				<br/>
				Max file size: <?php echo (ini_get('post_max_size') < ini_get('upload_max_filesize'))? ini_get('post_max_size'): ini_get('upload_max_filesize'); ?>
			</td>
			<td class="bl"><?php if (isset($upload_error)) { echo $upload_error; } ?></td>
		</tr>
		<tr>
			<td class="bb"></td>
			<td class="bb br"><button type="submit">Submit</button></td>
		</tr>
	</table>
</form>
<p>
	File must be in csv format with the following columns, column name must be in the first row, leave empty if not required:<br/>
	<ul>
	<li>name</li>
	<li>details</li>
	<li>highlight</li>
	<li>text</li>
	<li>keyword</li>
	<li>link1</li>
	<li>link1_text</li>
	<li>link2</li>
	<li>link2_text</li>
	<li>active</li>
	<li>special_offer</li>
	<li>special_offer_headline</li>
	<li>special_offer_body</li>
	<li>type</li>
	<li>price</li>
	<li>quantity</li>
	<li>expires</li>
	<li>changed_expire</li>
	<li>show_and_save</li>
	<li>book_coupon</li>
	<li>web_coupon</li>
	<li>mobile_reward</li>
	<li>phone_benefit</li>
	<li>online_purchase</li>
	<li>available_national</li>
	<li>available_act</li>
	<li>available_nsw</li>
	<li>available_nt</li>
	<li>available_qld</li>
	<li>available_sa</li>
	<li>available_tas</li>
	<li>available_vic</li>
	<li>available_wa</li>
	<li>category - category name the product is to be entered into, this will make the product available to all clients of that category</li>

	</ul>
</p>
<p><a href="<?php echo $this->Html->url('/admin/merchants/example_products_import'); ?>">Download example</a></p>
</div>
<?php elseif ($step == '2') : ?>
<h2>Confirm import</h2>
<div class="actions">
	<ul>
		<li><?php echo $this->Html->link('Cancel', '/admin/merchants/edit/'.$id); ?></li>
	</ul>
</div>
	<br/>
	<?php echo $uploaded_products; ?> products sent, <?php echo $good_products; ?> products will be imported.
	<?php if (!empty($upload_errors)) : ?>
		<h5>The following rows contain errors:</h5>
		<div class="edit">
		<table>
			<tr>
				<td class="bt">Row</td><td class="bt br">Error</td>
			</tr>
		<?php foreach ($upload_errors as $error) : ?>
			<tr><td><?php echo $error['row']; ?></td><td class="br"><?php echo $error['error']; ?></td></tr>
		<?php endforeach; ?>
			<tr>
				<td class="bb"></td><td class="bb br"></td>
			</tr>
		</table>
		</div>
	<?php endif; ?>
<div style="clear:both">
	<br/>
	<p><a href="<?php echo $this->Html->url('/admin/merchants/upload_products/'.$id.'/3'); ?>">Confirm</a> or</p>
	<p><a href="javascript:void(0)" onclick=" $('reupload').style.display = 'block'; return false;">Re-upload</a></p>
	<div class="edit" style="display:none;" id="reupload">
	<form action="<?php echo $this->Html->url('/admin/merchants/upload_products/'.$id.'/2'); ?>" method="post" enctype="multipart/form-data">
		<table>
			<tr>
				<td class="bt">CSV file</td>
				<td class="bt">
					<?php echo $this->Form->file('Merchant.products'); ?>
					<br/>
					Max file size: <?php echo (ini_get('post_max_size') < ini_get('upload_max_filesize'))? ini_get('post_max_size'): ini_get('upload_max_filesize'); ?>
				</td>
				<td class="bl"><?php if (isset($upload_error)) { echo $upload_error; } ?></td>
			</tr>
			<tr>
				<td class="bb"></td>
				<td class="bb br"><button type="submit">Submit</button></td>
			</tr>
		</table>
		</form>
	</div>
</div>

<?php elseif ($step == '3') : ?>
<h2>Import complete</h2>
<div class="actions">
	<ul>
		<li><?php echo $this->Html->link('Back to merchant', '/admin/merchants/edit/'.$id); ?></li>
	</ul>
</div>
<?php echo $product_count; ?> products imported.
<?php endif; ?>