<div class="list">
	<?php if (empty($merchants)) : ?>
		No Merchants found
	<?php else : ?>
		<table cellpadding="0" cellspacing="0">
		<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('name');?></th>
			<th class="actions"><?php echo "Actions";?></th>
		</tr>
		<?php
		$i = 0;
		foreach ($merchants as $merchant):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
			<tr<?php echo $class;?>>
				<td>
					<?php echo $merchant['Merchant']['id'] ?>
				</td>
				<td>
					<?php echo $merchant['Merchant']['name'] ?>
				</td>
				<td class="actions">
					<a href="javascript:void(0)" onclick="return updateMerchant('<?php echo $merchant['Merchant']['id']; ?>');">Select</a>
				</td>
			</tr>
		<?php endforeach; ?>
		</table>
			<div class="paging" id="paging">
				<?php echo $this->Paginator->prev('<< Previous', array(), null, array('class'=>'disabled'));?>
				<?php echo $this->Paginator->numbers();?>
				<?php echo $this->Paginator->next('Next >>', array(), null, array('class'=>'disabled', 'onclick' => "return false;"));?>
			</div>
			<script type="text/javascript">
				var elements = $('paging').getElementsBySelector('a');
				
				elements.each(function(element) {
					element.onclick = function(e) {
						
						new Ajax.Updater('merchant_search_results', this, {
						  evalScripts: true
						});
						
						return false;
					}
				});
				
				
			</script>
		<p><?php echo $this->Paginator->counter(array('format' => __('Page %page% of %pages%', true)));	?></p>
	<?php endif; ?>
</div>