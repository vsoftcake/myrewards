	<table>
		<tr>
			<td class="bt">Name</td>
			<td class="bt"><?php echo $this->Form->input('Merchant.name'); ?></td>
			<td class="bl"><?php echo $this->Form->error('Merchant.name', array('required' => 'Name required'), array('required' => 'Name required')); ?></td>
		</tr>
		<tr>
			<td>Contact</td>
			<td><?php echo $this->Form->input('Merchant.contact'); ?></td>
			<td class="bl"></td>
		</tr>
		<tr>
			<td>Address</td>
			<td><?php echo $this->Form->input('Merchant.address1'); ?><br/><?php echo $this->Form->input('Merchant.address2'); ?></td>
			<td class="bl"></td>
		</tr>
		<tr>
			<td>Suburb</td>
			<td><?php echo $this->Form->input('Merchant.suburb'); ?></td>
			<td class="bl"></td>
		</tr>
		<tr>
			<td>State</td>
			<td><?php echo $this->Form->input('Merchant.state', array('div'=>false,'label'=>false, 'type'=>'select', 'options'=>$states)); ?></td>
			<td class="bl"></td>
		</tr>
		<tr>
			<td>Postcode</td>
			<td><?php echo $this->Form->input('Merchant.postcode'); ?></td>
			<td class="bl"></td>
		</tr>
		<tr>
			<td>Phone</td>
			<td><?php echo $this->Form->input('Merchant.phone'); ?></td>
			<td class="bl"></td>
		</tr>
	</table>
	<!--<iframe src="<?php echo $this->Html->url('/admin/merchants/upload_addresses/'. $this->Html->value('Merchant.id')); ?>" height="1000" width="800" frameborder="0" id="upload_merchant_addresses" scrolling="no"></iframe>-->
	<?php echo $this->Form->input('Merchant.id', array('type' => 'hidden')); ?>
	<input type="hidden" name="data[Product][merchant_id]" value="<?php echo $this->Html->value('Merchant.id'); ?>" />
	<h4>Other products of this merchant</h4>
	<div class="list">
		<table cellpadding="0" cellspacing="0">
		<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('name');?></th>
			<th class="actions"><?php echo "Actions";?></th>
		</tr>
		<?php
		$i = 0;
		foreach ($related_products as $product):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
			<tr<?php echo $class;?>>
				<td>
					<?php echo $product['Product']['id'] ?>
				</td>
				<td>
					<?php echo $product['Product']['name'] ?>
				</td>
				<td class="actions">
					<a href="<?php echo $this->Html->url('/admin/products/edit/'. $product['Product']['id']); ?>">Edit</a>
				</td>
			</tr>
		<?php endforeach; ?>
		</table>
		<div class="paging">
			<?php echo $this->Paginator->prev('<< Previous', array(), null, array('class'=>'disabled'));?>
			<?php echo $this->Paginator->numbers();?>
			<?php echo $this->Paginator->next('Next >>', array(), null, array('class'=>'disabled'));?>
		</div>
		<p><?php echo $this->Paginator->counter(array('format' => __('Page %page% of %pages%', true)));	?></p>
	</div>