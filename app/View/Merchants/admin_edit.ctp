<style type="text/css">
	.rounded-corners 
	{
		-moz-border-radius: 10px 10px 0px 0px; /* Firefox */
		-webkit-border-radius: 10px 10px 0px 0px; /* Safari, Chrome */
		border-radius: 10px 10px 0px 0px; /* CSS3 */
	}
	.tr 
	{
		height:45px;
	}
	.tr1 
	{
		height:35px;
	}
	.admin_button
	{
		height: 26px;
		padding-bottom: 2px;
		border-radius: 5px 5px 5px 5px;
		background-color: #0066cc;
		border: 1px solid #cccccc;
		color: #FFFFFF; 
		cursor: pointer;
		font-size:13px;
	}
</style>
<script type="text/javascript">
	document.write('<style type="text/css">.tabber{display:none;}<\/style>');
	function confirmation(name,id)
    {
		var answer = confirm("Are you sure you want to delete merchant '<?php echo $this->Form->value('Merchant.name') ?>'")
		if (answer)
		{
			location.href="/admin/merchants/delete/<?php echo $this->Form->value('Merchant.id') ?>";
		}
    }
</script>
<form action="<?php echo $this->Html->url('/admin/merchants/edit/'. $this->Html->value('Merchant.id')); ?>" method="post" id="edit_form"  enctype="multipart/form-data">
<div style="width:953px;border-left:1px solid #cccccc;border-right:1px solid #cccccc;border-bottom:1px solid #cccccc;margin-left:-14px;">
		<div id="message"></div>
		<div style="width:949px;height:50px;">
			<div style=" height: 40px; float:left; width: 608px;padding-left:37px;">
		       	<div style=" height: 40px; float:left;padding-top:5px;">
		        	<img title="Merchants" alt="Merchants" src="/files/admin_home_icons/merchants.png" style="width:40px;height:40px;">
		        </div>
		        <div style=" height: 40px;float:left;padding-left:10px;">
		        	<h2>Edit Merchant <?php if($this->Form->value('Merchant.name')!='') : ?>- <?php echo $this->Form->value('Merchant.name') ?><?php endif; ?></h2>
		        </div> 
			</div>
	        <div class="actions" style="height:40px;float:left;width: 300px;">
				<ul style="padding:20px;margin-left:-28px;">
					<li style="display:inline;"><?php echo $this->Form->button('Cancel', array('onclick' => "location.href='/admin/merchants/'",
					'class'=>'admin_button','style'=>'width:60px;', 'type'=>'button'));  ?></li>
					<?php if($this->Form->value('Merchant.id')!='') :
					$conname=$this->Form->value('Merchant.name');$conid=$this->Form->value('Merchant.id'); ?>
					<li style="display:inline;"><?php echo $this->Form->button('Delete Merchant', array('onclick' => "confirmation('$conname','$conid')",'style'=>'width:110px;','class'=>'admin_button','type'=>'button'));  ?></li>
		        	<?php endif;?>
		        	<li style="display:inline;"><?php echo $this->Form->button('Submit', array('class'=>'admin_button','style'=>'width:60px;','type'=>'submit'));  ?></li>
		        </ul>
			</div>
		</div>
	
		<div class="tabber" id="mytab1">
			<?php echo $this->Form->input('Merchant.id', array('type' => 'hidden')); ?>
			<div class="tabbertab"  style="min-height:220px;overflow:hidden">
				<h2>General Information</h2>
				<h4>Merchant General Information</h4>
				<div style="background-color:#EFEFEF;border:1px solid #cccccc;width:768px;min-height:400px;overflow:hidden;padding:20px">
					<table>
						<tr>
							<td valign="top" style="width:340px;padding-right:30px;">
								<table>
									<tr class="tr">
										<td style="width:200px"><strong>Name</strong></td>
										<td><?php echo $this->Form->input('Merchant.name', array('div'=>false, 'label'=>false)); ?></td>
									</tr>
									<tr class="tr">
										<td><strong>Contact title</strong></td>
										<td colspan="2"><?php echo $this->Form->input('Merchant.contact_title', array('div'=>false, 'label'=>false)); ?></td>
									</tr>
									<tr class="tr">
										<td><strong>Contact first name</strong></td>
										<td colspan="2"><?php echo $this->Form->input('Merchant.contact_first_name', array('div'=>false, 'label'=>false)); ?></td>
									</tr>
									<tr class="tr">
										<td><strong>Countries</strong></td>
										<td colspan="2">
											<?php 
												echo $this->Form->input('Merchant.mail_country', array('div'=>false, 'label'=>false, 'type'=>'select', 'options'=>$mail_countries)); 
												$options = array('url' => 'update_state','update' => 'MerchantMailState');
									           	echo $this->Ajax->observeField('MerchantMailCountry', $options);
								         	?>
										</td>
									</tr>
									<tr class="tr">
										<td><strong>Phone</strong></td>
										<td colspan="2"><?php echo $this->Form->input('Merchant.phone', array('div'=>false, 'label'=>false)); ?></td>
									</tr>
									<tr class="tr">
										<td><strong>Address</strong></td>
										<td colspan="2"><?php echo $this->Form->input('Merchant.mail_address1', array('div'=>false, 'label'=>false)); ?><br/><?php echo $this->Form->input('Merchant.mail_address2', array('div'=>false, 'label'=>false)); ?></td>
									</tr>
									<tr class="tr">
										<td><strong>Postcode</strong></td>
										<td colspan="2"><?php echo $this->Form->input('Merchant.mail_postcode', array('div'=>false, 'label'=>false)); ?></td>
									</tr>
									<tr class="tr">
										<td><strong>Logo</strong></td>
										<td colspan="2">
											<img src="<?php echo $this->Html->url(MERCHANT_LOGO_WWW_PATH. $this->Form->value('Merchant.id'). '.'. $this->Form->value('Merchant.logo_extension'). '?rand='. time()); ?>" alt="" /><br/>
											<a href="javascript:void(0)" onclick="$(this).hide(); $('#MerchantLogoFile').show(); return false;">Change</a>
											<?php echo $this->Form->file('MerchantLogo.file', array('style' => 'display:none;')); ?><br/>
											Remove <?php echo $this->Form->checkbox('MerchantLogoImageDelete.file'); ?>
										</td>
									</tr>
								</table>
							</td>
							<td valign="top">
								<table>
									<tr class="tr">
										<td style="width:150px"><strong>Email</strong></td>
										<td colspan="2"><?php echo $this->Form->input('Merchant.email', array('div'=>false, 'label'=>false)); ?></td>
									</tr>
									<tr class="tr">
										<td><strong>Contact position</strong></td>
										<td colspan="2"><?php echo $this->Form->input('Merchant.contact_position', array('div'=>false, 'label'=>false)); ?></td>
									</tr>
									<tr class="tr">
										<td><strong>Contact last name</strong></td>
										<td colspan="2"><?php echo $this->Form->input('Merchant.contact_last_name', array('div'=>false, 'label'=>false)); ?></td>
									</tr>
									<tr class="tr">
										<td><strong>State</strong></td>
										<td colspan="2"><?php echo $this->Form->input('Merchant.mail_state', array('div'=>false, 'label'=>false, 'type'=>'select', 'options'=>$statelist, 'empty'=>'Select State')); ?></td>
									</tr>
									<tr class="tr">
										<td><strong>Mobile phone</strong></td>
										<td><?php echo $this->Form->input('Merchant.mobile', array('div'=>false, 'label'=>false)); ?></td>
										<td class="bl"></td>
									</tr>
									<tr class="tr">
										<td><strong>Suburb</strong></td>
										<td colspan="2"><?php echo $this->Form->input('Merchant.mail_suburb', array('div'=>false, 'label'=>false)); ?></td>
									</tr>
									<tr class="tr">
										<td><strong>Fax</strong></td>
										<td colspan="2"><?php echo $this->Form->input('Merchant.fax', array('div'=>false, 'label'=>false)); ?></td>
									</tr>
									<tr class="tr">
										<td><strong>Search weight</strong></td>
										<td colspan="2">
											<?php echo $this->Form->input('Merchant.search_weight', array('div'=>false, 'label'=>false, 'type'=>'select', 'options'=>$search_weights)); ?>
											<sub>Higher values appear first in search results</sub>
										</td>
									</tr>
									<!--<tr class="tr">
										<td><strong>Image</strong></td>
										<td class="br">
											<img src="<?php echo $this->Html->url(MERCHANT_IMAGE_WWW_PATH. $this->Form->value('Merchant.id'). '.'. $this->Form->value('Merchant.image_extension'). '?rand='. time()); ?>" alt="" /><br/>
											<a href="javascript:void(0)" onclick="this.style.display='none'; $('MerchantImageFile').style.display = 'inline'; return false;">Change</a>
											<?php echo $this->Form->file('MerchantImage.file', array('style' => 'display:none;')); ?><br/>
											Remove <?php echo $this->Form->checkbox('MerchantImageDelete.file'); ?>
											
										</td>
									</tr>-->
								</table>
							</td>
						</tr>
					</table>
				</div>
				<div align="center" style="padding-top:10px;">
					<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(1)">Next</button>
		       	</div>
			</div>
			
			<?php if ($this->Html->value('Merchant.id')) : ?>
				<div class="tabbertab"  style="min-height:220px;overflow:hidden;padding-left:15px !important;">
					<h2>Merchant Addresses</h2>
					<div style="border:1px solid #cccccc;width:840px;min-height:40px;overflow:hidden;">
						<div class="list" id="address_list" style="width:840px">
							<div id="AddresssLinks" style="padding:10px">
								<a href="javascript:void(0)" onclick="return addAddress();" id="NewAddressLink">create new...</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<?php echo $this->Html->link(__('upload multiple...', true), array('action'=>'upload_addresses', $this->Form->value('Merchant.id')), null, null); ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<?php echo $this->Html->link(__('delete multiple...', true), array('action'=>'delete_addresses', $this->Form->value('Merchant.id')), null, null); ?>
							</div>
							<div id="Addresses" style="margin:0;">Retrieving addresss...</div>
							<script type="text/javascript">
							$(function() {
								$.ajax({
									url: "/admin/merchants/ajax_addresses/<?php echo $this->Form->value('Merchant.id');?>",
									type: "GET",
									success: function (data) {
										$('#Addresses').html(data);
									}
								});
							});</script>
						</div>
					</div>
					<div align="center" style="padding-top:10px;">
						<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(0)">Previous</button>
						<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(2)">Next</button>
			       	</div>
				</div>
			<?php endif;?>
			
			<?php if ($this->Html->value('Merchant.id')) : ?>
				<div class="tabbertab"  style="min-height:220px;overflow:hidden">
					<h2>Products</h2>
					<div style="border:1px solid #cccccc;width:768px;min-height:40px;overflow:hidden;padding:20px">
						<div class="list" id="product_list">
							<div id="Products" style="margin:0;">Retrieving products...</div>
							<script type="text/javascript">
							$(function() {
								$.ajax({
									url: "/admin/merchants/ajax_products/<?php echo $this->Form->value('Merchant.id');?>",
									type: "GET",
									success: function (data) {
										$('#Products').html(data);
									}
								});
							});</script>
						</div>
					</div>
					<div align="center" style="padding-top:10px;">
						<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(1)">Previous</button>
						<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(3)">Next</button>
			       			<?php echo $this->Form->button('Add Product', array('class'=>'admin_button', 'type'=>'button', 'style'=>'width:110px;padding:0 !important;', 'onclick' => "location.href='".$this->Html->url('/admin/products/edit/merchant_id:'. $this->Html->value('Merchant.id'))."'"));?>
			       		</div>
				</div>
			<?php endif;?>
			
			<?php if ($this->Html->value('Merchant.id')) : ?>
				<div class="tabbertab"  style="min-height:220px;overflow:hidden">
					<h2>Comments</h2>
					<h4>Comments</h4>
					<div style="border:1px solid #cccccc;width:768px;min-height:40px;overflow:hidden;padding:20px">
						<div class="list" id="comment_list">
							<div id="Comments" style="margin:0;">Retrieving comments...</div>
							<script type="text/javascript">
							$(function() {
								$.ajax({
									url: "/admin/merchants/ajax_comments/<?php echo $this->Form->value('Merchant.id');?>",
									type: "GET",
									success: function (data) {
										$('#Comments').html(data);
									}
								});
							});
							
							
							</script>
						</div>
					</div>
					<div align="center" style="padding-top:10px;">
						<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(2)">Previous</button>
						<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(4)">Next</button>
			       	</div>
				</div>
			<?php endif; ?>
			
			<div class="tabbertab"  style="min-height:220px;overflow:hidden">
				<h2>Change Log</h2>
				<h4>Change Log</h4>
				<div style="border:1px solid #cccccc;width:768px;min-height:40px;overflow:hidden;padding:20px">
					<div class="list" id="log_list" style="clear:both">
						<div id="Log" style="margin:0;">Retrieving changes...</div>
						<script type="text/javascript">
						$(function() {
							$.ajax({
								url: "/admin/logs/ajax_list_new/<?php echo Inflector::singularize($this->name);?>/<?php echo $this->Form->value(Inflector::singularize($this->name). '.id');?>",
								type: "GET",
								success: function (data) {
									$('#Log').html(data);
								}
							});
						});</script>
					</div>
				</div>
				<div align="center" style="padding-top:10px;">
					<?php if ($this->Html->value('Merchant.id')) : ?>
						<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(3)">Previous</button>
					<?php else: ?>
						<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(0)">Previous</button>
					<?php endif;?>
		       	</div>
			</div>
		</div>
	</div>
</form>