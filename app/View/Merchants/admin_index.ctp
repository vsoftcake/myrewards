<style>
	#content
	{
		border-left:1px solid #cccccc;
		border-right:1px solid #cccccc;
		border-bottom:1px solid #cccccc;
		margin-left:20px !important;
		padding-left:18px !important;
		width:913px;
	}
	.rounded-corners 
	{
		-moz-border-radius: 10px 10px 0px 0px; /* Firefox */
		-webkit-border-radius: 10px 10px 0px 0px; /* Safari, Chrome */
		border-radius: 10px 10px 0px 0px; /* CSS3 */
	}
	.current
	{
	    font-family: 'Droid Sans',sans-serif;
	    font-size: 12px;
	   	padding:5px 8px !important;   
	    background-color: #0066CC;
	    border: 1px solid #DFDFDF;
	    color:#ffffff;
	}
	.cpagination1
	{
		/*padding:1px 2px 1px 2px;*/
		font-family: 'Droid Sans',sans-serif;
	    font-size: 12px;
		text-decoration:none !important;
		background-color: #FFFFFF;
		padding:3px 8px;
		border: 1px solid #0066CC;
		color:#808080 !important;
	}
	.admin_button
	{
		height: 26px;
		padding-top: 2px;
		padding-bottom: 2px;
		border-radius: 5px 5px 5px 5px;
		background-color: #0066cc;
		border: 1px solid #cccccc;
		color: #FFFFFF; 
		cursor: pointer;
		font-size:13px;
	}
</style>

<table>
	<tr>
    	<td>
			<table style="margin-left:-2px;margin-top:10px;">
				<tr>
					<td>
			            &nbsp;&nbsp;<img style="width:40px;height:40px;" src="/files/admin_home_icons/merchants.png"  alt="Merchants" title="Merchants" />&nbsp;&nbsp;
			        </td>
			        <td> 
			            <h2><?php echo "Merchants";?></h2>
			        </td>
			        <td width="500"></td>
			        <td>
			        	<h3><?php echo $this->Html->link('Merchants With No Geocodes', array('action'=>'no_geo_codes')); ?></h3>
			        </td>
			    </tr>        
			</table>
		</td>
  	</tr>
	<tr>
    	<td>
			<div class="actions" style="padding-bottom:20px;padding-top:10px">
				<table style="width:920px;background-color:#cccccc;height:75px;border:1px solid #969090;">
					<tr>
						<td>
							<form action="<?php echo $this->Html->url('/admin/merchants/'); ?>" method="get" id="SearchForm">
								<table>
									<tr>
										<td style="padding-left:10px;">Find Merchants</td>
										<td>
											<?php echo $this->Form->input('Merchant.search', array('div'=>false, 'label'=>false)); ?>
											<script type="text/javascript">
											$(function() {
												if ($('#MerchantSearch').val() == '') {
													$('#MerchantSearch').val('ID or name');
													$('#MerchantSearch').css( "color", "#aaaaaa" );
												}
												$( "#MerchantSearch" ).focus(function() {
													if ($(this).val() == 'ID or name') {
														$(this).val('');
														$(this).css( "color", "#000" );
													}
												});
												$( "#SearchForm" ).submit(function() {
													if ($("#MerchantSearch").val() == 'ID or name') {
														$("#MerchantSearch").val('');
														$("#MerchantSearch").css( "color", "#000" );
													}
												});
											});
											</script>
										</td>
										<td></td>
										<td><button class="admin_button">Search</button></td>
									</tr>
								</table>
							</form>
						</td>
						<td style="padding-left: 120px;">
							<button class ="admin_button" onclick="location.href='<?php echo $this->Html->url('edit'); ?>';">Create New Merchant</button>
							<button class ="admin_button" onclick="location.href='<?php echo $this->Html->url('upload_merchants'); ?>';">Upload multiple merchants</button>
						</td>
					<!--<ul>
						<li><?php echo $this->Html->link('Create new merchant', array('action'=>'edit')); ?></li>
						<li><?php echo $this->Html->link('Upload multiple merchants ', array('action'=>'upload_merchants', $this->Form->value('1')), null, null); ?></li>
					</ul>-->
					</tr>
				</table>
			</div>
		</td> 
	</tr>
</table>
<br />
<table>
  <tr>
    <td>
<div class="list">
	<table cellpadding="0" cellspacing="0" width="920">
	<tr height="30">
		<th style="background-color:#cccccc;text-align:left;padding-left:5px;border-left:1px solid #969090;border-bottom:1px solid #969090;border-top:1px solid #969090;"><?php echo $this->Paginator->sort('id');?></th>
		<th style="background-color:#cccccc;text-align:left;padding-left:1px;border-bottom:1px solid #969090;border-top:1px solid #969090;"><?php echo $this->Paginator->sort('name');?></th>
		<th class="actions" style="background-color:#cccccc;text-align:left;padding-left:4px;border-right:1px solid #969090;border-bottom:1px solid #969090;border-top:1px solid #969090;"><?php echo "Actions";?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($merchants as $merchant):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
		<tr<?php echo $class;?>>
			<td>
				<?php echo $merchant['Merchant']['id'] ?>
			</td>
			<td>
				<?php echo $merchant['Merchant']['name'] ?>
			</td>
			<td class="actions">
				<?php echo $this->Html->link(__('Edit', true), array('action'=>'edit', $merchant['Merchant']['id'])); ?> | 
				<?php echo $this->Html->link(__('Delete', true), array('action'=>'delete', $merchant['Merchant']['id']), null, sprintf(__('Are you sure you want to delete merchant \'%s\'?', true), $merchant['Merchant']['name'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
	<div class="paging" style="padding-top:7px;float:right">
		<table style="border:none !important;">
			<tr>
				<td style="border:none !important;" valign="top">
					<?php echo $this->Paginator->first('<<', array('title'=>'first','class'=>'disabled cpagination1'));?>
					<?php echo $this->Paginator->prev('<', array('title'=>'prev','class'=>'disabled cpagination1'));?>
					<?php echo $this->Paginator->numbers(array('separator' => ' ','class'=>'cpagination1'));?>
					<?php echo $this->Paginator->next('>', array('title'=>'next','class'=>'disabled cpagination1'));?>
					<?php echo $this->Paginator->last('>>', array('title'=>'last','class'=>'disabled cpagination1'));?>
				</td>
				<td style="border:none !important;padding-top:3px;">
				<select name="page" id="selectpage" style="margin-top:-5px;">
					<?php for ($i = 1; $i <= $this->Paginator->counter(array('format' => '%pages%')); $i++):?>
					<option value="<?php echo $i;?>"<?php echo ($this->Paginator->current($params['page']) == $i) ? ' selected="selected"' : ''?>><?php echo $i ?></option>
					<?php endfor ?>
				</select>
				<script>
				$("#selectpage").change(function() {
					var url = window.location.href;
					var n = url.indexOf("/?");
					var extra = "";
					if(n>0){extra = url.substring(n,url.length);}
					window.location.href = '/admin/merchants/index/page:'+$(this).val()+extra;
				});
				</script>
				</td>
			</tr>
		</table>
	</div>
</div>
</td>


</tr>
</table>
