<?php echo($this->Html->script("tinymce/jscripts/tiny_mce/tiny_mce.js"));?>
<script language="javascript" type="text/javascript">
<?php if($preset = "basic")
{
    $options = '
    mode : "textareas",
    theme : "advanced",
    
    plugins : "autolink,lists,pagebreak,style,layer,table,advhr,advimage,advlink,fullscreen,advlist,autosave", 
    theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,fontselect,fontsizeselect,|,bullist,numlist",
    theme_advanced_buttons2 : "outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,forecolor,backcolor,|,hr,fullscreen,spellchecker",
    theme_advanced_buttons3 : "",
  
    theme_advanced_toolbar_location : "top",
    theme_advanced_toolbar_align : "left",
    theme_advanced_path_location : "bottom",
    theme_advanced_resizing : true,
    extended_valid_elements : "a[name|href|target|title|onclick],img[class|src|border=0|alt|title|hspace|vspace|width|height|align|onmouseover|onmouseout|name],hr[class|width|size|noshade],font[face|size|color|style],span[class|align|style]",
    content_css : "/css/'.$this->layout.'.css"    
    ';
}
?>

tinyMCE.init({<?php echo($options); ?>});
</script>

<?php if (isset($status) && $status = 'update_complete') : ?>
<?php echo $this->Html->script('prototype'); ?>
<script type="text/javascript">
window.parent.$('ProductMessage').innerHTML = 'Product saved';
window.parent.$('message').innerHTML = '';
window.parent.clearTimeout(window.parent.productSubmitTimeout);
function reloadProducts() {
	window.parent.$('ProductMessage').style.display = 'none';
	window.parent.$('Products').innerHTML = 'Retreving products...';
	window.parent.$('product_list').style.display = 'block';
	new Ajax.Updater(window.parent.$('Products'), '<?php
		if ($this->Session->check('Merchants.search_url.Product')) {
			$url = $this->Session->read('Merchants.search_url.Product');
		} else {
			$url = $this->Html->url('/admin/merchants/ajax_products/'. $this->Form->value('Product.merchant_id'));
		}
		echo $url; ?>', {evalScripts: true });
}

setTimeout(reloadProducts, 1000);

</script>

<?php elseif (isset($status) && $status = 'update_error') : ?>
Update error.

<?php endif; ?>



<?php if (isset($product)) : ?>

<h3>Edit Product</h3>

<div class="actions">
	<ul>
		<li><a href="javascript:void(0)" onclick="$('Product').style.display = 'none'; $('product_list').style.display = 'block'; return false;">Cancel</a></li>
		<li><a href="javascript:void(0)" onclick="return deleteProduct();">Delete</a></li>
	</ul>
</div>

<form action="<?php echo $this->Html->url('/admin/merchants/ajax_product/'. $this->Html->value('Product.id')); ?>" method="post" id="EditProductForm" enctype="multipart/form-data" onsubmit="return submitProductForm(this);">
<div class="edit">
	<table width="600px">
		<tr>
			<td class="bt">ID</td>
			<td class="bt br">
				<?php echo $this->Form->value('Product.id'); ?>
				<?php echo $this->Form->input('Product.id'); ?>
				<?php echo $this->Form->input('Product.merchant_id', array('type' => 'hidden')); ?>
			</td>
		</tr>
		<tr>
			<td>Name</td>
			<td>
				<?php echo $this->Form->input('Product.name'); ?>
				<?php // echo $this->Form->error('Product.name', array('required' => 'Name required'), array('required' => 'Name required')); ?>
			</td>
		</tr>
		<tr>
			<td>Highlight</td>
			<td><?php echo $this->Form->input('Product.highlight'); ?></td>
		</tr>
		<tr>
			<td>Details</td>
			<td><?php echo $this->Form->input('Product.details'); ?></td>
		</tr>
		<tr>
			<td>Text</td>
			<td><?php echo $this->Form->input('Product.text'); ?></td>
		</tr>
		<tr>
			<td>Terms &amp; conditions</td>
			<td><?php echo $this->Form->input('Product.terms_and_conditions'); ?></td>
		</tr>
		<tr>
			<td>Keywords</td>
			<td><?php echo $this->Form->input('Product.keyword'); ?></td>
		</tr>
		<tr>
			<td>Show &amp; Save</td>
			<td>
				<?php echo $this->Form->checkbox('Product.show_and_save'); ?>
			</td>
		</tr>
		<tr>
			<td>Book Coupon</td>
			<td>
				<?php echo $this->Form->checkbox('Product.book_coupon'); ?>
			</td>
		</tr>
		<tr>
			<td>Web Coupon</td>
			<td>
				<?php echo $this->Form->checkbox('Product.web_coupon'); ?>
				<a href="javascript:void(0)" onclick="return previewVoucher(); ">preview</a>
			</td>
		</tr>
		<tr>
			<td>Mobile Reward</td>
			<td>
				<?php echo $this->Form->checkbox('Product.mobile_reward'); ?>
			</td>
		</tr>
		<tr>
			<td>Phone Benefit</td>
			<td>
				<?php echo $this->Form->checkbox('Product.phone_benefit'); ?>
			</td>
		</tr>
		<tr>
			<td>Online Purchase</td>
			<td>
				<?php echo $this->Form->checkbox('Product.online_purchase'); ?>
				&nbsp;Price $<?php echo $this->Form->input('Product.price', array('style' => 'width:4em')); ?>
				Qty <?php echo $this->Form->input('Product.quantity', array('style' => 'width:2em')); ?>
			</td>
		</tr>
		<tr>
			<td>Web link 1</td>
			<td>
				Link &nbsp;<?php echo $this->Form->input('Product.link1'); ?><br/>
				Text <?php echo $this->Form->input('Product.link1_text'); ?>
			</td>
		</tr>
		<tr>
			<td>Web link 2</td>
			<td>
				Link &nbsp;<?php echo $this->Form->input('Product.link2'); ?><br/>
				Text <?php echo $this->Form->input('Product.link2_text'); ?>
			</td>
		</tr>
		<tr>
			<td>Special Offer</td>
			<td><?php echo $this->Form->checkbox('Product.special_offer'); ?></td>
		</tr>
		<tr>
			<td>Special Offer headline</td>
			<td>
				<?php echo $this->Form->input('Product.special_offer_headline'); ?>
			</td>
		</tr>
		<tr>
			<td>Special Offer body</td>
			<td>
				<?php echo $this->Form->input('Product.special_offer_body'); ?>
			</td>
		</tr>
		<tr>
			<td>Availability</td>
			<td>
				<table style="width:100%;" id="TableAvailability">
					<tr>
						<td><?php echo $this->Form->checkbox('Product.available_national', array('onClick' => 'ToggleStates(this)')); ?> National<br/>
						</td>
						<td><?php echo $this->Form->checkbox('Product.available_act'); ?> ACT</td>
						<td><?php echo $this->Form->checkbox('Product.available_nsw'); ?> NSW</td>
					</tr>
					<tr>
						<td><?php echo $this->Form->checkbox('Product.available_nt'); ?> NT</td>
						<td><?php echo $this->Form->checkbox('Product.available_qld'); ?> QLD</td>
						<td><?php echo $this->Form->checkbox('Product.available_sa'); ?> SA</td>
					</tr>
					<tr>
						<td><?php echo $this->Form->checkbox('Product.available_tas'); ?> TAS</td>
						<td><?php echo $this->Form->checkbox('Product.available_vic'); ?> VIC</td>
						<td><?php echo $this->Form->checkbox('Product.available_wa'); ?> WA</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td>Expires</td>
			<td>
				<?php echo $this->Form->dateTime ('Product.expires', 'DMY', 'NONE', null, array('style' => 'width:auto',  'minYear' => date('Y'), 'maxYear' => date("Y")+10)); ?>
			</td>
		</tr>
		<tr>
			<td>Renewed</td>
			<td>
				<?php echo $this->Form->checkbox('Product.renewed'); ?>
				&nbsp;&nbsp;Year Until
				<?php echo $this->Form->dateTime ('Product.yearuntil', 'Y', 'NONE', null, array('style' => 'width:auto',  'minYear' => date('Y'), 'maxYear' => date("Y")+10)); ?>
			</td>
		</tr>
		<tr>
			<td>Image</td>
			<td>
				<img src="<?php echo $this->Html->url(PRODUCT_IMAGE_WWW_PATH. $this->Form->value('Product.id'). '.'. $this->Form->value('Product.image_extension'). '?rand='. time()); ?>" alt="" /><br/>
				<?php echo $this->Form->file('ProductImage.file'); ?><br/>
				Remove <?php echo $this->Form->checkbox('ProductImageDelete.file'); ?>
			</td>
		</tr>
		<tr>
			<td>Display image<br/><sub>Image to display on product list pages</sub></td>
			<td><?php echo $this->Form->input('Product.display_image', array('div'=>false,'label'=>false, 'type'=>'select', 'options'=>$display_images)); ?></td>
		</tr>
		<tr>
			<td>Active</td>
			<td><?php echo $this->Form->checkbox('Product.active'); ?></td>
		</tr>
		<tr>
			<td>Cancelled</td>
			<td><?php echo $this->Form->checkbox('Product.cancelled'); ?></td>
		</tr>
		<tr>
			<td>Search weight</td>
			<td>
				<?php echo $this->Form->input('Product.search_weight', array('div'=>false,'label'=>false, 'type'=>'select', 'options'=>$search_weights)); ?>
				<sub>Higher values appear first in search results</sub>
			</td>
		</tr>
		<tr>
			<td>Consultant</td>
			<td><?php echo $this->Form->input('Product.user_id', array('div'=>false,'label'=>false, 'type'=>'select', 'options'=>$users)); ?></td>
		</tr>
		<tr>
			<td>No assignment override</td>
			<td>
				<?php echo $this->Form->checkbox('Product.assignment_override'); ?><br/>
				<sub>Check this to prevent the product being assigned to programs<br/>or clients via the Categories and Programs adminsitration pages</sub>
			</td>
		</tr>
		<tr>
			<td>Categories</td>
			<td>
				<?php echo $this->Form->input('Category.Category', array('multiple' => 'multiple')); ?>
				<input type="hidden" name="data[Category][Category][]" value="" />
			</td>
		</tr>
		<tr>
			<td>Print coupon limit count</td>
			<?php 
			
				if($this->Form->value('Product.used_count')=='') 
					$defaultCount = -1;
				else 
					$defaultCount = $this->Form->value('Product.used_count');
			?>
			<td><?php echo $this->Form->input('Product.used_count',array('default'=>$defaultCount)); ?></td>
		</tr>
		<tr>
			<td class="bb"></td>
			<td class="bb br"><button type="submit">Save product</button></td>
		</tr>
	</table>
</div>
<div class="edit">
	<div class="list">
		<h4>Programs</h4>
		<table cellpadding="0" cellspacing="0" width="200px">
		<tr>
			<th>Name</th>
			<th>Enabled</th>
		</tr>
		<?php
		$i = 0;
		foreach ($programs as $program):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
			<tr<?php echo $class;?>>
				<td>
					<a href="javascript:void(0)" onclick="return getClients('<?php echo $id; ?>', <?php echo $program['Program']['id']; ?>);"><?php echo $program['Program']['name'] ?></a>
				</td>
				<td style="text-align:center">
					<input type="hidden" name="data[ProgramsProduct][id][<?php echo $program['Program']['id']; ?>]" value="0" />
					<input type="checkbox" name="data[ProgramsProduct][id][<?php echo $program['Program']['id']; ?>]" value="1" <?php
						if (isset($this->data['Program'])) {
							foreach ($this->data['Program'] as $product_program) {
								if ($product_program['id'] == $program['Program']['id']) {
									echo 'checked="checked"';
									break;
								}
							}
						}
					?> onclick="clientsOnOff('<?php echo $id; ?>', <?php echo $program['Program']['id']; ?>, this.checked);" class="checkbox" />
				</td>
			</tr>
		<?php endforeach; ?>
		</table>
	</div>
	<div style="clear:both;"></div>

	<?php if (!empty($this->data['Client'])) : ?>
		<?php foreach ($this->data['Client'] as $client) : ?>
			<input type="hidden" name="data[ClientsProduct][id][<?php echo $client['id']; ?>]" value="1"  />
		<?php endforeach; ?>
	<?php endif; ?>
	<?php foreach ($programs as $program) : ?>
	<div class="list clients" id="clients<?php echo $program['Program']['id']; ?>" style="display:none"></div>
	<?php endforeach; ?>
</div>
</form>
<div id="voucher_preview" class="popup_div" onclick="return previewVoucherDisplay();">
	<div class="preview_center" id="preview_center">
		<table id="preview_table">
			<tr>
				<td class="preview_container">
					<div class="preview_content" id="voucher_preview_content"></div>
				</td>
			</tr>
		</table>
	</div>
</div>
<script type="text/javascript">

//	programs/clients functions
loaded_programs = new Array();

getClients = function(product_id, program_id) {
	$('message').innerHTML = '';
	var divs =  document.getElementsByClassName('clients');

	for (var i in divs) {
		if (typeof(divs[i].style) != 'undefined') {
			divs[i].style.display = 'none';
		}
	}

	$('clients' + program_id).style.display = 'block';

	if(typeof(loaded_programs[program_id]) == 'undefined') {
		$('clients' + program_id).innerHTML = 'Retrieving Clients...';
		new Ajax.Updater('clients' + program_id, '<?php echo $this->Html->url('/admin/merchants/ajax_product_clients/'); ?>' + product_id + '/' + program_id + '?data[Product][category_id]=<?php echo $category_id; ?>', {
		evalScripts: true,
		onSuccess: function() {
			loaded_programs[program_id] = program_id;
			}
		});
	}

	return false;
}

clientsOnOff = function(product_id, program_id, on_off) {
	$('message').innerHTML = '';
	var divs =  document.getElementsByClassName('clients');
	for (var i in divs) {
		if (typeof(divs[i].style) != 'undefined') {
			divs[i].style.display = 'none';
		}
	}
	$('clients' + program_id).style.display = 'inline';

	if(typeof(loaded_programs[program_id]) == 'undefined') {
		$('clients' + program_id).innerHTML = 'Retrieving Clients...';
		new Ajax.Updater('clients' + program_id, '<?php echo $this->Html->url('/admin/merchants/ajax_product_clients/'); ?>' + product_id + '/' + program_id, {
		evalScripts: true,
		onComplete: function() {
			loaded_programs[program_id] = program_id;
			doClientsOnOff(program_id, on_off);
			}
		});
	} else {
		doClientsOnOff(program_id, on_off);
	}

}

doClientsOnOff = function(program_id, on_off) {

	var inputs = $('clients' + program_id).getElementsBySelector('input');

	inputs.each(function(input) {
		input.checked = on_off;
	});

}

previewVoucher = function(voucher_id) {

	if (typeof(voucher_id) == 'undefined') {
		voucher_id = '';
	}
	new Ajax.Updater('voucher_preview_content', '<?php echo $this->Html->url('/admin/merchants/ajax_product_preview_voucher/'); ?>' + voucher_id, {
		evalScripts: true,
		parameters: $('EditProductForm').serialize()
	});

	return false;
}

previewVoucherDisplay = function() {

	$('voucher_preview').style.height = $('container').offsetHeight + 'px';
	if ($('voucher_preview').style.visibility == 'visible') {
		$('voucher_preview').style.visibility = 'hidden';
	} else {
		$('voucher_preview').style.visibility = 'visible';
	}

	return false;
}

// functions for saving product
var productSumbitTimeout;
submitProductForm = function(form) {
	form.target = 'upload_target';
	window.parent.scroll(0,0);
	$('Product').style.display = 'none';
	$('ProductMessage').innerHTML = 'Saving product...';
	$('ProductMessage').style.display = 'block';

	productSubmitTimeout = setTimeout(reDisplayProduct, 20000); // if the product does not save in 20 seconds, re-display to be submitted again

}

deleteProduct = function() {
	if (confirm('Are you sure you want to delete this product?')) {
		$('Product').style.display = 'none';
		$('ProductMessage').innerHTML = 'Deleting product...';
		$('ProductMessage').style.display = 'block';
		new Ajax.Updater('Product', '<?php echo $this->Html->url('/admin/merchants/ajax_product_delete/'. $this->Form->value('Product.id')); ?>', {evalScripts: true }); return false;
	}

	return false;
}

ToggleStates = function (National)
{
    var rows = document.getElementById('TableAvailability').getElementsByTagName('input');
    for ( var i = 0; i < rows.length; i++ ) {
        if ( rows[i] && rows[i].type == 'checkbox' && rows[i] != National ) 
	{
		if(National.checked == true)
		{
	            rows[i].checked = true;
		}
		else
		{
		   rows[i].checked = false;
		}
        }
    }

    return true;
}

/*
 * Redispaly a product after X seconds, if the submit does nto go correctly
*/
function reDisplayProduct() {
	$('message').innerHTML = '<div class="message">There was an error saving the product, please try again</div>';
	$('Product').style.display = '';
}

</script>

<?php endif; ?>
