<table cellpadding="0" cellspacing="0" style="border:none !important;width:100%">
	<tbody id="addresses_details">
	<tr id="row">
		<th>Primary</th>
		<th>Name</th>
		<th>Address</th>
		<th>Suburb</th>
		<th>Country</th>
		<th>State</th>
		<th>City</th>
		<th>Region</th>
		<th>Postcode</th>
		<th>Phone</th>
		<th>Latitude</th>
		<th>Longitude</th>
		<th>Actions</th>
	</tr>
	<?php
	$i = 0;
	
	foreach ($merchant_addresses as $address):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
		<tr<?php echo $class;?> id="MerchantAddress_<?php echo $address['MerchantAddress']['id']; ?>">
			<td style="padding:01em !important;">
				<?php ($address['MerchantAddress']['primary'] == '1')? $checked = ' checked="checked"':  $checked = ''; ?>
				<input type="hidden" name="data[MerchantAddress][<?php echo $address['MerchantAddress']['id']; ?>][id]" value="<?php echo $address['MerchantAddress']['id']; ?>" />
				<input type="radio" name="data[PrimaryMerchantAddress][id]" value="<?php echo $address['MerchantAddress']['id']; ?>" <?php echo $checked; ?> class="radio" />
			</td>
			<td style="padding:0.1em !important;">
				<?php echo $address['MerchantAddress']['name'] ?>
			</td>
			<td style="padding:0.1em !important;">
				<?php echo $address['MerchantAddress']['address1'] ?> <?php echo $address['MerchantAddress']['address2'] ?>
			</td>
			<td style="padding:0.1em !important;">
				<?php echo $address['MerchantAddress']['suburb'] ?>
			</td>
			<td style="padding:0.1em !important;">
				<?php echo $address['MerchantAddress']['country'] ?>
			</td>
			<td style="padding:0.1em !important;">
				<?php echo $address['MerchantAddress']['state'] ?>
			</td>
			<td style="padding:0.1em !important;">
				<?php echo $address['MerchantAddress']['city'] ?>
			</td>
			<td style="padding:0.1em !important;">
				<?php echo $address['MerchantAddress']['region'] ?>
			</td>
			<td style="padding:0.1em !important;">
				<?php echo $address['MerchantAddress']['postcode'] ?>
			</td>
			<td style="padding:0.1em !important;">
				<?php echo $address['MerchantAddress']['phone'] ?>
			</td>
			<td style="padding:0.1em !important;">
				<?php echo $address['MerchantAddress']['latitude'] ?>
			</td>
			<td style="padding:0.1em !important;">
				<?php echo $address['MerchantAddress']['longitude'] ?>
			</td>
			<td class="actions">
				<a href="javascript:void(0)" onclick="editAddress(<?php echo $address['MerchantAddress']['id']; ?>);">Edit</a>
				<a href="javascript:void(0)" onclick="removeAddress(<?php echo $address['MerchantAddress']['id']; ?>);">Remove</a>
			</td>
		</tr>
			<?php endforeach; ?>
	</tbody>
</table>
<script type="text/javascript">

getStates = function(country,stateid,adid){
	$.ajax({
		url: '/admin/merchants/ajax_states/'+adid+'/'+country+'/'+stateid,
		type: "GET",
		success: function (data) {
			$('#mrStid'+adid).html(data);
		}
	});
}

addAddress = function() {
	document.getElementById('row').style.display="none";
	var rand = 'new' + Math.round(10000000000*Math.random());
	
	var html = '<tr id="MerchantAddress_new' + rand + '"><td colspan="13" style="border:none !important;"><table width="100%" style="border:none !important;"><tr>';
	html += '<td style="width:100px;border:none !important;">Primary</td><td style="border:none !important;"><input type="hidden" name="data[MerchantAddress][' + rand + '][id]" value="' + rand + '" /><input type="radio" name="data[PrimaryMerchantAddress][id]" value="' + rand + '" class="radio" /></td></tr>';
	html += '<tr><td style="border:none !important;">Name</td><td style="border:none !important;"><input type="text" name="data[MerchantAddress][' + rand + '][name]" style="width:15em" /></td></tr>';
	html += '<tr><td style="border:none !important;">Address</td><td style="border:none !important;"><input type="text" name="data[MerchantAddress][' + rand + '][address1]" style="width:15em;" /><input type="text" name="data[MerchantAddress][' + rand + '][address2]" style="width:15em;" /></td></tr>';
	html += '<tr><td style="border:none !important;">Suburb</td><td style="border:none !important;"><input type="text" name="data[MerchantAddress][' + rand + '][suburb]" style="width:15em"/></td></tr>';
	var scode='';
	html += '<tr><td style="border:none !important;">Country</td><td style="border:none !important;"><select name="data[MerchantAddress][' + rand + '][country]" onchange="getStates(this.value,\''+scode+'\',\''+rand+'\')">';
	html += '<option value=""></option>';
	html += '<option value="Australia">Australia</option>';
	html += '<option value="India">India</option>';
	html += '<option value="Hong Kong">Hong Kong</option>';
	html += '<option value="Philippines">Philippines</option>';
	html += '<option value="New Zealand">New Zealand</option>';
	html += '<option value="Singapore">Singapore</option>';
	html += '</td></tr>';
	
	html += '<tr><td style="border:none !important;">State</td><td style="border:none !important;" id="mrStid'+rand+'"><select name="data[MerchantAddress][' + rand + '][state]">';
	html += '<option value=""></option>';
	
	html += '</select></td></tr>';
	html += '<tr><td style="border:none !important;">City</td><td style="border:none !important;"><input type="text" name="data[MerchantAddress][' + rand + '][city]" style="width:15em"/></td></tr>';
	html += '<tr><td style="border:none !important;">Region</td><td style="border:none !important;"><input type="text" name="data[MerchantAddress][' + rand + '][region]" style="width:15em"/></td></tr>';
	html += '<tr><td style="border:none !important;">Postcode</td><td style="border:none !important;"><input type="text" name="data[MerchantAddress][' + rand + '][postcode]" style="width:15em"/></td></tr>';
	html += '<tr><td style="border:none !important;">Phone</td><td style="border:none !important;"><input type="text" name="data[MerchantAddress][' + rand + '][phone]" style="width:15em"/></td></tr>';
	html += '<tr><td style="border:none !important;">Latitude</td><td style="border:none !important;"><input type="text" name="data[MerchantAddress][' + rand + '][latitude]" style="width:15em"/></td></tr>';
	html += '<tr><td style="border:none !important;">Longitude</td><td style="border:none !important;"><input type="text" name="data[MerchantAddress][' + rand + '][longitude]" style="width:15em"/></td></tr>';
	html += '<tr><td style="border:none !important;">Actions</td><td style="border:none !important;" class="actions"><a href="javascript:void(0)" onclick="return removeAddress(\'new' + rand + '\');">Remove</a></td>';
	html += '</tr></table></td></tr>';
	
	$( html ).insertAfter( "#addresses_details" );
}

function trim(stringToTrim) {
	return stringToTrim.replace(/^\s+|\s+$/g,"");
}
editAddress = function(address_id) {
	document.getElementById('row').style.display="none";
	var className = $('#MerchantAddress_' + address_id).attr('class');
	var id = $('#MerchantAddress_' + address_id).attr('id');
	var td0 = td1 = td2 = td3 = td4 = td5 = td6 = td7 = td8 = td9 = td10 = td11 = '';
	$('#MerchantAddress_' + address_id).each(function() {
		td0 = $(this).find("td").eq(0).html();
		td1 = $(this).find("td").eq(1).html();
		td2 = $(this).find("td").eq(2).html();
		td3 = $(this).find("td").eq(3).html();
		td4 = $(this).find("td").eq(4).html();
		td5 = $(this).find("td").eq(5).html();
		td6 = $(this).find("td").eq(6).html();
		td7 = $(this).find("td").eq(7).html();
		td8 = $(this).find("td").eq(8).html();
		td9 = $(this).find("td").eq(9).html();
		td10 = $(this).find("td").eq(10).html();
		td11 = $(this).find("td").eq(11).html();
	});
	var html = '<tr class="' + className + '" id="' + id + '">';
	html += '<td style="width:100px;border:none !important;">Primary</td><td  style="border:none !important;">' + td0 + '</td></tr>';
	html += '<tr class="' + className + '"><td style="border:none !important;">Name</td><td style="border:none !important;"><input type="text" name="data[MerchantAddress][' + address_id + '][name]" value="' + trim(td1) + '" style="width:15em" /></td></tr>';
	html += '<tr class="' + className + '"><td style="border:none !important;">Address</td><td style="border:none !important;"><input type="text" name="data[MerchantAddress][' + address_id + '][address1]" value="' + trim(td2) + '" style="width:15em;" /><input type="text" name="data[MerchantAddress][' + address_id + '][address2]" style="width:15em;" /></td></tr>';
	html += '<tr class="' + className + '"><td style="border:none !important;">Suburb</td><td style="border:none !important;"><input type="text" name="data[MerchantAddress][' + address_id + '][suburb]" value="' + trim(td3) + '" style="width:15em"/></td></tr>';
	var scode='';
	html += '<tr class="' + className + '"><td style="border:none !important;">Country</td><td style="border:none !important;"><select style="width:15em" name="data[MerchantAddress][' + address_id + '][country]" onchange="getStates(this.value,\''+scode+'\','+address_id+')">';
	
	var countries = new Array();
	countries[0] = 'Australia';
	countries[1] = 'India';
	countries[2] = 'Hong Kong';
	countries[3] = 'Philippines';
	countries[4] = 'New Zealand';
	
	for (var i=0; i < countries.length; i++) {
		if (countries[i] == td4.replace(/^\s+|\s+$/g, '')) {
			html += '<option value="' + countries[i] + '" selected="selected">' + countries[i] + '</option>';
		} else {
			html += '<option value="' + countries[i] + '">' + countries[i] + '</option>';
		}
	}
	html += '</select></td></tr>';
	$.ajax({
		url: '/admin/merchants/ajax_states/'+address_id+'/'+trim(td4)+'/'+trim(td5),
		type: "GET",
		success: function (data) {
			$('#mrStid'+address_id).html(data);
		}
	});
	html += '<tr class="' + className + '"><td style="border:none !important;">State</td><td style="border:none !important;" id="mrStid'+address_id+'"><select style="width:15em" name="data[MerchantAddress][' + address_id + '][state]" id = "stateid">';
	html += '<option value=""></option>';
	html += '</select></td></tr>';
	html += '<tr class="' + className + '"><td style="border:none !important;">City</td><td style="border:none !important;"><input type="text" name="data[MerchantAddress][' + address_id + '][city]" value="' + trim(td6) + '" style="width:15em"/></td></tr>';
	html += '<tr class="' + className + '"><td style="border:none !important;">Region</td><td style="border:none !important;"><input type="text" name="data[MerchantAddress][' + address_id + '][region]" value="' + trim(td7) + '" style="width:15em"/></td></tr>';
	html += '<tr class="' + className + '"><td style="border:none !important;">Postcode</td><td style="border:none !important;"><input type="text" name="data[MerchantAddress][' + address_id + '][postcode]" value="' + trim(td8) + '" style="width:15em"/></td></tr>';
	html += '<tr class="' + className + '"><td style="border:none !important;">Phone</td><td style="border:none !important;"><input type="text" name="data[MerchantAddress][' + address_id + '][phone]" value="' + trim(td9) + '" style="width:15em"/></td></tr>';
	html += '<tr class="' + className + '"><td style="border:none !important;">Latitude</td><td style="border:none !important;"><input type="text" name="data[MerchantAddress][' + address_id + '][latitude]" value="' + trim(td10) + '" style="width:15em"/></td></tr>';
	html += '<tr class="' + className + '"><td style="border:none !important;">Longitude</td><td style="border:none !important;"><input type="text" name="data[MerchantAddress][' + address_id + '][longitude]" value="' + trim(td11) + '" style="width:15em"/></td></tr>';
	html +='<tr class="' + className + '"><td style="border:none !important;">Actions</td><td style="border:none !important;"><input type="hidden" name="data[MerchantAddress][' + address_id + '][edit]" value="' + address_id + '" /></td>';
	html += '</tr>';
	
	$('#MerchantAddress_' + address_id).html(html);
}

removeAddress = function(address_id) {
	if (confirm('Remove address?')) {
		if (!isNaN(address_id)) {
			var html = '<input type="hidden" name="data[RemoveAddress][]" value="' + address_id + '" />';
			$('#edit_form').append(html);
		}
		$('#MerchantAddress_' + address_id).remove();
	}
}
</script>