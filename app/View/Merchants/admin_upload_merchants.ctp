<style>
	#content
	{
		border-left:1px solid #cccccc;
		border-right:1px solid #cccccc;
		border-bottom:1px solid #cccccc;
		margin-left:20px !important;
		padding-left:15px !important;
		width:916px;
	}
</style>
<?php if ($step == '1') : ?>
<h2>Upload merchants</h2>
<div class="actions">
	<ul>
		<li><?php echo $this->Html->link('Cancel', '/admin/merchants/index'); ?></li>
	</ul>
</div>
<div class="edit">
<form action="<?php echo $this->Html->url('/admin/merchants/upload_merchants/2'); ?>" 
method="post" enctype="multipart/form-data">
	<table>
		<tr>
			<td class="bt">CSV file</td>
			<td class="bt">
				<?php echo $this->Form->file('Merchant.merchants'); ?>
				<br/>
				Max file size: <?php echo (ini_get('post_max_size') < ini_get('upload_max_filesize'))? ini_get('post_max_size'): ini_get('upload_max_filesize'); ?>
			</td>
			<td class="bl"><?php if (isset($upload_error)) { echo $upload_error; } ?></td>
		</tr>
		<tr>
			<td class="bb"></td>
			<td class="bb br"><button type="submit">Submit</button></td>
		</tr>
	</table>
</form>
<p>
	File must be in csv format with the following columns, column name must be in the first row, leave empty if not required.<br/>
	If there is a product or merchant address to be uploaded with the merchant, ensure that the product_name and merchant_name fileds are not blank
</p>
<ul>
	<li>name</li>
	<li>contact_title</li>
	<li>contact_first_name</li>
	<li>contact_last_name</li>
	<li>contact_position</li>
	<li>mail_address1</li>
	<li>mail_address2</li>
	<li>mail_suburb</li>
	<li>mail_state</li>
	<li>mail_postcode</li>
	<li>mail_country</li>
	<li>email</li>
	<li>phone</li>
	<li>mobile</li>
	<li>fax</li>
</ul>
<ul>
	<li>product_name</li>
	<li>product_details</li>
	<li>product_highlight</li>
	<li>product_text</li>
	<li>product_keyword</li>
	<li>product_link1</li>
	<li>product_link1_text</li>
	<li>product_link2</li>
	<li>product_link2_text</li>
	<li>product_active</li>
	<li>product_special_offer</li>
	<li>product_special_offer_headline</li>
	<li>product_special_offer_body</li>
	<li>product_type</li>
	<li>product_price</li>
	<li>product_quantity</li>
	<li>product_expires</li>
	<li>product_changed_expire</li>
	<li>product_show_and_save</li>
	<li>product_book_coupon</li>
	<li>product_web_coupon</li>
	<li>product_mobile_reward</li>
	<li>product_phone_benefit</li>
	<li>product_online_purchase</li>
	<li>product_available_national</li>
	<li>product_used_count - Print coupon limit count</li>
	<li>product_coupon_expiry - web coupon expiry</li>
	<li>product_favourites - set to 0 or 1</li>
	<li>product_offer- coupon detail</li>
	<li>product_available_international - set to 0 or 1</li>
	<li>product_app_text</li>
	<li>product_app_coupon</li>
</ul>
<ul>
	<li>address_name</li>
	<li>address_address1</li>
	<li>address_address2</li>
	<li>address_suburb</li>
	<li>address_city</li>
	<li>address_state - Below are the list of state codes<p>Australia   - ACT, NSW, NT, QLD, SA, TAS, VIC, WA
						<br>New Zealand - AUK, BOP, CAN, FIL, GIS, HKB, MBH, MWT, NAB, NTL, OTA, STL, TKI, TMR, TSM, WGI, WGN, WKO, WPP, WTC			
						<br>India       - AP, DL, GJ, HR, KA, MH, TN, UP 						
						<br>Philippines - LUZ, MIN, NCR, VIS</p></li>
	<li>address_region - NZ region, one of Auckland, Bay of Islands, Central N Island, Central S Island, Christchurch, Dunedin, Lower N Island, Lower S Island, Nationwide, Nelson, Q'town/Wanaka, Rotorua/Taupo, Upper N Island, Upper S Island, Wellington</li>
	<li>address_country - one of Australia, New Zealand, India, Hong Kong, Philippines</li>
	<li>address_postcode</li>
	<li>address_phone</li>
</ul>
<p><a href="<?php echo $this->Html->url('/admin/merchants/example_merchants_import'); ?>">Download example</a></p>
</div>
<?php elseif ($step == '2') : ?>
<h2>Confirm import</h2>
<div class="actions">
	<ul>
		<li><?php echo $this->Html->link('Cancel', '/admin/merchants/index'); ?></li>
	</ul>
</div>
	<br/>
	<?php echo $uploaded_merchants; ?> merchants sent, <?php echo $good_merchants; ?> merchants will be imported.
	<?php if (!empty($upload_errors)) : ?>
		<h5>The following rows contain errors:</h5>
		<div class="edit">
		<table>
			<tr>
				<td class="bt">Row</td><td class="bt br">Error</td>
			</tr>
		<?php foreach ($upload_errors as $error) : ?>
			<tr><td><?php echo $error['row']; ?></td><td class="br"><?php echo $error['error']; ?></td></tr>
		<?php endforeach; ?>
			<tr>
				<td class="bb"></td><td class="bb br"></td>
			</tr>
		</table>
		</div>
	<?php endif; ?>
<div style="clear:both">
	<br/>
	<p><a href="<?php echo $this->Html->url('/admin/merchants/upload_merchants/3'); ?>">Confirm</a> or</p>
	<p><a href="javascript:void(0)" onclick=" $('reupload').style.display = 'block'; return false;">Re-upload</a></p>
	<div class="edit" style="display:none;" id="reupload">
	<form action="<?php echo $this->Html->url('/admin/merchants/upload_merchants/2'); ?>" method="post" enctype="multipart/form-data">
		<table>
			<tr>
				<td class="bt">CSV file</td>
				<td class="bt">
					<?php echo $this->Form->file('Merchant.merchants'); ?>
					<br/>
					Max file size: <?php echo (ini_get('post_max_size') < ini_get('upload_max_filesize'))? ini_get('post_max_size'): ini_get('upload_max_filesize'); ?>
				</td>
				<td class="bl"><?php if (isset($upload_error)) { echo $upload_error; } ?></td>
			</tr>
			<tr>
				<td class="bb"></td>
				<td class="bb br"><button type="submit">Submit</button></td>
			</tr>
		</table>
		</form>
	</div>
</div>

<?php elseif ($step == '3') : ?>
<h2>Import complete</h2>
<div class="actions">
	<ul>
		<li><?php echo $this->Html->link('Back to merchants', '/admin/merchants/index'); ?></li>
	</ul>
</div>
<?php echo $product_count; ?> merchants imported.
<?php endif; ?>