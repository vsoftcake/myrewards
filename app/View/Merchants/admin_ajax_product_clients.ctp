<h4>Clients for <?php echo $program['Program']['name']; ?></h4>
	<?php if ($clients) : ?>
	<table cellpadding="0" cellspacing="0">
	<tr>
		<th>Name</th>
		<th>Enabled</th>
	</tr>
	<?php
	$i = 0;
	foreach ($clients as $client):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
		<tr<?php echo $class;?>>
			<td><label for="ClientsProduct<?php echo $client['Client']['id']; ?>"><?php echo $client['Client']['name'] ?></label></td>
			<td style="text-align:center">
				<input type="hidden" name="data[ClientsProduct][id][<?php echo $client['Client']['id']; ?>]" value="0"  />
				<?php //debug($product_clients); die(); ?>
				
				<input type="checkbox" name="data[ClientsProduct][id][<?php echo $client['Client']['id']; ?>]" value="1" id="ClientsCategory<?php echo $client['Client']['id']; ?>" <?php 
				
				foreach ($product_clients as $product_client) {
							if ($product_client['Client']['id'] == $client['Client']['id']) {
								echo 'checked="checked"';
								break;
							}
						}
				?> class="checkbox" />
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
	<?php else : ?>
	<br/>No clients found
	<?php endif; ?>
	
	
