<style type="text/css">
	.rounded-corners 
	{
		-moz-border-radius: 10px 10px 0px 0px; /* Firefox */
		-webkit-border-radius: 10px 10px 0px 0px; /* Safari, Chrome */
		border-radius: 10px 10px 0px 0px; /* CSS3 */
	}
	.tr 
	{
		height:45px;
	}
	.admin_button
	{
		height: 26px;
		padding-bottom: 2px;
		border-radius: 5px 5px 5px 5px;
		background-color: #0066cc;
		border: 1px solid #cccccc;
		color: #FFFFFF; 
		cursor: pointer;
		font-size:13px;
	}
</style>
<?php echo $this->element('tinymce');?>

<div style="width:953px;border-left:1px solid #cccccc;border-right:1px solid #cccccc;border-bottom:1px solid #cccccc;margin-left:-14px;">
	<div style="width:949px;height:50px;">
		<div style=" height: 40px; float:left; width: 608px;padding-left:37px;">
	       	<div style=" height: 40px; float:left;padding-top:5px;">
	        	<img title="Programs" alt="Programs" src="/files/admin_home_icons/programs.png" style="width:40px;height:40px;">
	        </div>
	        <div style=" height: 40px;float:left;padding-left:10px;">
	        	<h2>Program <?php if($program['Program']['name']!=''): ?> - <?php echo $program['Program']['name']; ?> <?php endif;?></h2>
	        	<h4>Edit ProgramPage <?php if($this->Form->value('ProgramPage.page_title')!=''): ?> - <?php echo $this->Form->value('ProgramPage.page_title') ?> <?php endif;?></h4>
	        </div> 
		</div>
        <div class="actions" style="height:40px;float:left;width: 300px;">
			<ul style="padding:20px;margin-left:-28px;">
				<li style="display:inline;"><?php echo $this->Form->button('Cancel', array('onclick' => "location.href='/admin/programs/edit/".$program['Program']['id']."'",
				'class'=>'admin_button','style'=>'width:60px;'));  ?></li>
	        </ul>
		</div>
	</div>
	
	<form action="<?php echo $this->Html->url('/admin/program_pages/edit/'. $this->Html->value('ProgramPage.program_id'). '/'. $this->Html->value('ProgramPage.page_id').'/'.$dashboard_id); ?>" method="post">
		<div class="tabber" id="mytab1">
			<div class="tabbertab"  style="min-height:150px;overflow:hidden">
	  			<h2>General Information</h2>
	    		<h4>ProgramPage Information</h4>
	    		<div style="background-color:#EFEFEF;border:1px solid #cccccc;width:768px;min-height:40px;overflow:hidden;padding:20px;font-weight:bold">
	    			The following field replacements are available:<br/>
					[[client_name]] replaced by the name of the client<br/>
					[[client_email]] replaced by the clients email address<br/>
					[[program_name]] replaced by the name of the program<br/>
					e.g. "Welcome to [[client_name]] online" will result in "Welcome to My Rewards online"
	    		</div>
	    		
	    		<div align="center" style="padding-top:10px;">
					<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(1)">Next</button>
		       	</div>
	    	</div>
	    	
	    	<div class="tabbertab"  style="min-height:220px;overflow:hidden">
	  			<h2>Page Details</h2>
	    		<h4>Page Details</h4>
	    		<div style="background-color:#EFEFEF;border:1px solid #cccccc;width:768px;min-height:40px;overflow:hidden;padding:20px">
	    			<table>
						<tr class="tr">
							<td></td>
							<td><strong>Use Default setting?</strong></td>
							<td></td>
						</tr>
						<tr class="tr">
							<td><strong>Enabled</strong></td>
							<td><?php echo $this->Form->checkbox('ProgramPage.enabled_override', array('onclick' => "updateOverride(this, 'ProgramPageEnabled')")); ?></td>
							<td>
							<?php if($this->Html->value('ProgramPage.page_id')!=60 && $this->Html->value('ProgramPage.page_id')!=61 && $this->Html->value('ProgramPage.page_id')!=63):?>
								<?php 
								($this->Form->value('ProgramPage.enabled_override') == 1)? $options = array('disabled' => 'disabled'): $options = array();
								echo $this->Form->checkbox('ProgramPage.enabled', $options); 
								?>
							<?php endif;?>
							</td>
						</tr>
						<tr class="tr">
							<td><strong>URL</strong><br/><sub>Displayed in the browser address bar</sub></td>
							<td></td>
							<td>
								<?php echo $this->Form->value('Page.name'); ?>
							</td>
						</tr>
						<tr class="tr">
							<td><strong>Page title</strong><br/><sub>Displayed in the browser title bar</sub></td>
							<td><?php echo $this->Form->checkbox('ProgramPage.page_title_override', array('onclick' => "updateOverride(this, 'ProgramPagePageTitle')")); ?></td>
							<td>
								<?php
									($this->Form->value('ProgramPage.page_title_override') == 1)? $options = array('disabled' => 'disabled'): $options = array();
									echo $this->Form->input('ProgramPage.page_title', array('div'=>false, 'label'=>false)+$options); 
								?>
							</td>
						</tr>
						<tr class="tr">
							<td><strong>Meta Description</strong></td>
							<td><?php echo $this->Form->checkbox('ProgramPage.meta_description_override', array('onclick' => "updateOverride(this, 'ProgramPageMetaDescription')")); ?></td>
							<td>
								<?php
									($this->Form->value('ProgramPage.meta_description_override') == 1)? $options = array('disabled' => 'disabled'): $options = array();
									echo $this->Form->input('ProgramPage.meta_description', array('div'=>false, 'label'=>false)+$options); 
								?>
							</td>
						</tr>
						<tr class="tr">
							<td><strong>Meta Keywords</strong></td>
							<td><?php echo $this->Form->checkbox('ProgramPage.meta_keywords_override', array('onclick' => "updateOverride(this, 'ProgramPageMetaKeywords')")); ?></td>
							<td>
								<?php
									($this->Form->value('ProgramPage.meta_keywords_override') == 1)? $options = array('disabled' => 'disabled'): $options = array();
									echo $this->Form->input('ProgramPage.meta_keywords', array('div'=>false, 'label'=>false)+$options); 
								?>
							</td>
						</tr>
						<tr class="tr">
							<td><strong>Meta Robots</strong></td>
							<td><?php echo $this->Form->checkbox('ProgramPage.meta_robots_override', array('onclick' => "updateOverride(this, 'ProgramPageMetaRobots')")); ?></td>
							<td>
								<?php
									($this->Form->value('ProgramPage.meta_robots_override') == 1)? $options = array('disabled' => 'disabled'): $options = array();
									echo $this->Form->input('ProgramPage.meta_robots', array('div'=>false, 'label'=>false)+$options); 
								?>
							</td>
						</tr>
						<tr class="tr">
							<td><strong>Navigation Menu Title</strong></td>
							<td><?php echo $this->Form->checkbox('ProgramPage.title_override', array('onclick' => "updateOverride(this, 'ProgramPageTitle')")); ?></td>
							<td>
								<?php
									($this->Form->value('ProgramPage.title_override') == 1)? $options = array('disabled' => 'disabled'): $options = array();
									echo $this->Form->input('ProgramPage.title', array('div'=>false, 'label'=>false)+$options); 
								?>
							</td>
						</tr>
						<?php if($this->Html->value('ProgramPage.page_id')!=62):?>
						<tr class="tr">
							<td><strong>Heading 1</strong></td>
							<td><?php echo $this->Form->checkbox('ProgramPage.h1_override', array('onclick' => "updateOverride(this, 'ProgramPageH1')")); ?></td>
							<td>
								<?php
									($this->Form->value('ProgramPage.h1_override') == 1)? $options = array('disabled' => 'disabled'): $options = array();
									echo $this->Form->input('ProgramPage.h1', array('div'=>false, 'label'=>false)+$options); 
								?>
							</td>
						</tr>
						<?php endif;?>
						<tr class="tr">
							<td><strong>Heading 2</strong></td>
							<td><?php echo $this->Form->checkbox('ProgramPage.h2_override', array('onclick' => "updateOverride(this, 'ProgramPageH2')")); ?></td>
							<td>
								<?php
									($this->Form->value('ProgramPage.h2_override') == 1)? $options = array('disabled' => 'disabled'): $options = array();
									echo $this->Form->input('ProgramPage.h2', array('div'=>false, 'label'=>false)+$options); 
								?>
							</td>
						</tr>
						<?php if($this->Html->value('ProgramPage.page_id')!=62):?>
						<tr class="tr">
							<td><strong>Heading 3</strong></td>
							<td><?php echo $this->Form->checkbox('ProgramPage.h3_override', array('onclick' => "updateOverride(this, 'ProgramPageH3')")); ?></td>
							<td>
								<?php
									($this->Form->value('ProgramPage.h3_override') == 1)? $options = array('disabled' => 'disabled'): $options = array();
									echo $this->Form->input('ProgramPage.h3', array('div'=>false, 'label'=>false)+$options); 
								?>
							</td>
						</tr>
						<tr class="tr">
							<td><strong>Display special offers?</strong></td>
							<td><?php echo $this->Form->checkbox('ProgramPage.special_offers_override', array('onclick' => "updateOverride(this, 'ProgramPageSpecialOffers')")); ?></td>
							<td>
								<?php
									($this->Form->value('ProgramPage.special_offers_override') == 1)? $options = array('disabled' => 'disabled'): $options = array();
									echo $this->Form->checkbox('ProgramPage.special_offers', $options); 
								?>
							</td>
						</tr>
						<tr class="tr">
							<td><strong>Display News?</strong></td>
							<td><?php echo $this->Form->checkbox('ProgramPage.whats_new_module_override', array('onclick' => "updateOverride(this, 'ProgramPageWhatsNewModule')")); ?></td>
							<td>
								<?php
									($this->Form->value('ProgramPage.whats_new_module_override') == 1)? $options = array('disabled' => 'disabled'): $options = array();
									echo $this->Form->checkbox('ProgramPage.whats_new_module', $options); 
								?>
							</td>
						</tr>
						<?php endif;?>
					</table>
	    		</div>
	    		
	    		<div align="center" style="padding-top:10px;">
					<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(0)">Previous</button>
					<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(2)">Next</button>
		       	</div>
	    	</div>
	    	
	    	<div class="tabbertab"  style="min-height:150px;overflow:hidden">
	  			<h2>Content</h2>
	    		<h4>Content</h4>
	    		<div style="background-color:#EFEFEF;border:1px solid #cccccc;width:768px;min-height:40px;overflow:hidden;padding:20px">
	    			<table style="width:100%">
	    				<tr class="tr">
							<td><strong>Content</strong></td>
							<td><?php echo $this->Form->checkbox('ProgramPage.content_override', array('onclick' => "updateOverride(this, 'ProgramPageContent')")); ?></td>
							<td style="width:200px">
								<?php
									($this->Form->value('ProgramPage.content_override') == 1)? $options = array('disabled' => 'disabled', 'rows' => 10, 'cols' => 80, 'div'=>false, 'label'=>false): $options = array('rows' => 10, 'cols' => 80, 'div'=>false, 'label'=>false);
									echo $this->Form->input('ProgramPage.content', $options); 
								?>
							</td>
						</tr>
						<tr class="tr">
							<td></td>
							<td></td>
							<td>
								<div id="submit" style="padding-top:10px">
									<button class="admin_button" onclick="return displayOverride();">Submit &amp; apply to clients...</button>
								</div>
							</td>
						</tr>
						<tr class="tr">
							<td colspan="3">
								<div id="override" style="display:none">
									<table style="border:none !important">
										<tr class="tr">
											<th style="text-align:left;width:105px;">Items</th><th style="text-align:left;">Clients</th>
										</tr>
										<tr class="tr">
											<td style="vertical-align:top">
												<input type="hidden" name="data[ClientOverride][override]" value="0" id="override_enabled" />
												<label for="ClientOverridePage"><strong>Page </strong><input type="checkbox" name="data[ClientOverride][Page]" value="1"class="checkbox" id="ClientOverridePage" /></label><br/>
											</td>
											<td>
												<select name="data[ClientOverideClients][]" multiple="multiple" id="ClientOverideClientsSelect">
													<option value="all" onclick="selectAllOverride();">All Clients</option>
													<?php foreach ($all_clients as $client_id => $client_name) : ?>
														<option value="<?php echo $client_id; ?>" ><?php echo $client_name; ?></option>
													<?php endforeach; ?>
												</select>
											</td>
										</tr>
										<tr class="tr">
											<td></td>
											<td colspan="2">
												<button class="admin_button" type="submit" onclick="return confirm('Apply changes to all selected clients?');">Submit &amp; apply to clients</button>
												<button class="admin_button" onclick="return cancelOverride();">Cancel</button>
											</td>
										</tr>
									</table>
								</div>
								<script type="text/javascript">
									function displayOverride() {
										$('#submit').hide();
										$('#override').show();
										$('#override_enabled').val('1');
										return false;
									}
									function cancelOverride() {
										$('#submit').show();
										$('#override').hide();
										$('#override_enabled').val('0');
										return false;
									}
									var all_override_selected = false;
									function selectAllOverride()
									{
										if (all_override_selected == false) {
											all_override_selected = true;
										} else {
											all_override_selected = false;
										}

										$("#ClientOverideClientsSelect option").prop('selected', all_override_selected);
										$("#ClientOverideClientsSelect option[value='all']").prop("selected", false);
									}
								</script>
							</td>
						</tr>
	    			</table>
	    		</div>
	    		
	    		<div align="center" style="padding-top:10px;">
					<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(1)">Previous</button>
					<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(3)">Next</button>
					<button class="admin_button" type="submit" onclick="toggleSpellchecker()">Submit</button>
		       	</div>
	    	</div>
	    	
	    	<div class="tabbertab"  style="min-height:220px;overflow:hidden">
	  			<h2>Change Log</h2>
	    		<h4>Change Log</h4>
	    		<div style="background-color:#EFEFEF;border:1px solid #cccccc;width:768px;min-height:40px;overflow:hidden;padding:20px">
	    			<div id="Log" style="margin:0;">Retrieving changes...</div>
					<script type="text/javascript">
					$(function() {
						$.ajax({
							url: "/admin/logs/ajax_list_new/<?php echo Inflector::singularize($this->name);?>/<?php echo $this->Form->value(Inflector::singularize($this->name). '.id');?>",
							type: "GET",
							success: function (data) {
								$('#Log').html(data);
							}
						});
					});</script>
	    		</div>
	    		
	    		<div align="center" style="padding-top:10px;">
					<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(2)">Previous</button>
		       	</div>
	    	</div>
	    </div>	
	    <?php echo $this->Form->input('ProgramPage.id', array('type' => 'hidden')); ?>
	    <?php echo $this->Form->input('ProgramPage.program_id', array('type' => 'hidden')); ?>
	    <?php echo $this->Form->input('ProgramPage.page_id', array('type' => 'hidden')); ?>
	    <?php echo $this->Form->input('ProgramPage.dashboard_id', array('type' => 'hidden')); ?>
	</form>
</div>

<script type="text/javascript">

	function updateOverride(checkbox, input) {
		if ($(checkbox).prop("checked") == true) {
			$("#"+input).attr("disabled", true);
		} else {
			$("#"+input).attr("disabled", false);
		}
	}
	
	function toggleSpellchecker()
	{
		var val = navigator.userAgent.toLowerCase();
		if(val.indexOf("msie") < 0)
		{
			tinyMCE.execCommand('mceFocus', false, 'ProgramPageContent');
			tinyMCE.execCommand('mceSpellCheck', false, 'ProgramPageContent');
		}
	}

</script>
