<style type="text/css">
	#content
	{
		border-left:1px solid #cccccc;
		border-right:1px solid #cccccc;
		border-bottom:1px solid #cccccc;
		margin-left:20px !important;
		padding-left:18px !important;
		width:913px;
	}
	.rounded-corners 
	{
		-moz-border-radius: 10px 10px 0px 0px; /* Firefox */
		-webkit-border-radius: 10px 10px 0px 0px; /* Safari, Chrome */
		border-radius: 10px 10px 0px 0px; /* CSS3 */
	}
	.tr 
	{
		height:45px;
	}
	.admin_button
	{
		height: 26px;
		padding-bottom: 3px;
		border-radius: 5px 5px 5px 5px;
		background-color: #0066cc;
		border: 1px solid #cccccc;
		color: #FFFFFF; 
		cursor: pointer;
		font-size:13px;
	}
</style>
<form action="<?php echo $this->Html->url('/admin/auto_logins/add_details/'. $this->Html->value('AutoLogin.id')); ?>" method="post" id="edit_form" enctype="multipart/form-data">
<div style="width:953px;margin-left:-14px;">
	<div id="message"></div>
	<div style="width:949px;height:60px;">
		<div style=" height: 40px; float:left; width: 570px;padding-left:37px;">
	       	<div style=" height: 40px; float:left;padding-top:5px;">
	        	<img title="Products" alt="Products" src="/files/admin_home_icons/invitation.png" style="width:40px;height:40px;">
	        </div>
	        <div style=" height: 40px;float:left;padding-left:10px;">
	        	<h2>Edit Detail</h2>
	        </div> 
		</div>
        <div class="actions" style=" height: 40px; float:left;  width: 340px;">
			<ul style="padding:20px;margin-left:-28px;">
				<li style="display:inline;"><?php echo $this->Form->button('Cancel', array('onclick' => "location.href='/admin/safs_invitation/index'",
				'class'=>'admin_button','style'=>'width:60px;', 'type'=>'button'));  ?></li>
		        	<li style="display:inline;">
		        		<button class="admin_button" type="submit" onclick="toggleSpellchecker()">Submit</button>
		        	</li>
	        	</ul>
		</div>
	</div>
	
		<div class="tabber" id="mytab1">
			<div class="tabbertab"  style="min-height:200px;overflow:hidden">
	  			<h2>Auto Login Details</h2>
				<div style="background-color:#EFEFEF;border:1px solid #cccccc;padding:10px;width:790px;min-height:100px;overflow:hidden">
					<div style="padding:20px;">
						<?php echo $this->Form->input('AutoLogin.id', array('type' => 'hidden')); ?>	
						<table>
							<tr class="tr">
								<td valign="top"><strong>Domain URL</strong></td>
								<td valign="top">
									<?php echo $this->Form->input('AutoLogin.domain_url');?>
								</td>
							</tr>
							<tr class="tr">
								<td valign="top"><strong>Select Domain</strong></td>
								<td valign="top">
									<?php echo $this->Form->input('AutoLogin.domain_id', array('type' => 'select', 'options'=> $domains));?>
								</td>
							</tr>
							<tr class="tr">
								<td valign="top"><strong>Select Client</strong></td>
								<td valign="top">
									<?php echo $this->Form->input('AutoLogin.client_id', array('type' => 'select', 'options'=> $clients));	?>
								</td>
							</tr>
							<tr class="tr">
								<td style="width:130px"><strong>Username</strong></td>
								<td colspan="2">
									<?php echo $this->Form->input('AutoLogin.username',array('id'=>'autocomplete', 'class'=>'autocomplete'));?>
									<script type="text/javascript">
									$(function() {
										$( "#autocomplete" ).autocomplete({
											source: '/admin/auto_logins/get_users/'+$('#AutoLoginClientId').val(), minLength: 3
										});
									});
									</script>
								</td>
							</tr>
							<!--<tr class="tr">
								<td style="width:130px"><strong>Password</strong></td>
								<td colspan="2"><?php //echo $this->Form->input('AutoLogin.password');?></td>
							</tr>-->
						</table>
					</div>
				</div>
	 		</div>
		</div>
	</div>
</form>