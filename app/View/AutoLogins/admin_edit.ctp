<style type="text/css">
	#content
	{
		border-left:1px solid #cccccc;
		border-right:1px solid #cccccc;
		border-bottom:1px solid #cccccc;
		margin-left:20px !important;
		padding-left:18px !important;
		width:913px;
	}
	.rounded-corners 
	{
		-moz-border-radius: 10px 10px 0px 0px; /* Firefox */
		-webkit-border-radius: 10px 10px 0px 0px; /* Safari, Chrome */
		border-radius: 10px 10px 0px 0px; /* CSS3 */
	}
	.tr 
	{
		height:45px;
	}
	.admin_button
	{
		height: 26px;
		padding-bottom: 3px;
		border-radius: 5px 5px 5px 5px;
		background-color: #0066cc;
		border: 1px solid #cccccc;
		color: #FFFFFF; 
		cursor: pointer;
		font-size:13px;
	}
</style>
<form action="<?php echo $this->Html->url('/admin/auto_logins/edit/'. $this->Html->value('AutoLogin.id')); ?>" method="post" id="edit_form" enctype="multipart/form-data">
<div style="width:953px;margin-left:-14px;">
	<div id="message"></div>
	<div style="width:949px;height:60px;">
		<div style=" height: 40px; float:left; width: 570px;padding-left:37px;">
	       	<div style=" height: 40px; float:left;padding-top:5px;">
	        	<img title="Products" alt="Products" src="/files/admin_home_icons/auto_login.jpg" style="width:40px;height:40px;">
	        </div>
	        <div style=" height: 40px;float:left;padding-left:10px;">
	        	<h2>Edit Detail</h2>
	        </div> 
		</div>
        <div class="actions" style=" height: 40px; float:left;  width: 340px;">
			<ul style="padding:20px;margin-left:-28px;">
				<li style="display:inline;"><?php echo $this->Form->button('Cancel', array('onclick' => "location.href='/admin/auto_logins/index'",
				'class'=>'admin_button','style'=>'width:60px;', 'type'=>'button'));  ?></li>
		        	<li style="display:inline;">
		        		<button class="admin_button" type="submit">Submit</button>
		        	</li>
	        	</ul>
		</div>
	</div>
	
		<div class="tabber" id="mytab1">
			<div class="tabbertab"  style="min-height:200px;overflow:hidden">
	  			<h2>Auto Login Details</h2>
				<div style="background-color:#EFEFEF;border:1px solid #cccccc;padding:10px;width:790px;min-height:100px;overflow:hidden">
					<div style="padding:20px;">
						<?php echo $this->Form->input('AutoLogin.id', array('type' => 'hidden')); ?>
						<table>
							<tr class="tr">
								<td valign="top"><strong>Domain URL</strong></td>
								<td valign="top">
									<?php echo $this->Form->input('AutoLogin.domain_url', array('div'=>false, 'label'=>false));?>&nbsp;<span style="color:red">* </span>
									<br/>
									<?php echo $this->Form->error('AutoLogin.domain_url', array('required' => 'External Domain Name required', 'unique' => 'External Domain already in use'), array('required' => 'External Domain Name required')); ?>
								</td>
							</tr>
							<tr class="tr">
								<td valign="top"><strong>Select Domain</strong></td>
								<td valign="top">
									<?php echo $this->Form->input('AutoLogin.domain_id', array('type' => 'select', 'options'=> $domains, 'empty' => ' ', 'div'=>false, 'label'=>false));?>
									&nbsp;<span style="color:red">* </span>
									<?php 
										$options = array('url' => 'update_clients','update' => 'AutoLoginClientId');
										echo $this->Ajax->observeField('AutoLoginDomainId', $options);?>
									<br/>
									<?php echo $this->Form->error('AutoLogin.domain_id', array('required' => 'Domain required'), array('required' => 'Domain required')); ?>
								</td>
							</tr>
							<tr class="tr">
								<td valign="top"><strong>Select Client</strong></td>
								<td valign="top">
									<?php echo $this->Form->input('AutoLogin.client_id', array('type' => 'select', 'options'=> $clients, 'empty' => ' ', 'div'=>false, 'label'=>false));?>
									&nbsp;<span style="color:red">* </span>
									<br/>
									<?php echo $this->Form->error('AutoLogin.client_id', array('required' => 'Client required'), array('required' => 'Client required')); ?>
								</td>
							</tr>
							<tr class="tr">
								<td style="width:130px"><strong>Username</strong></td>
								<td colspan="2">
									<?php echo $this->Form->input('AutoLogin.username', array('div'=>false, 'label'=>false));?>
								</td>
							</tr>
						</table>
					</div>
				</div>
	 		</div>
		</div>
	</div>
</form>