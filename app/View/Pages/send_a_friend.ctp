<script>
function friendSubmit()
{
	var gcount = parseInt(document.getElementById('count').value);
	var friendname = document.getElementById('friendname'+gcount).value;
	var friendemail = document.getElementById('friendemail'+gcount).value;
	
	if(friendname!="" && friendemail!="")
	{
		$.ajax({
			url: '/saf/saf_request',
			data: $('#Safrequestform').serialize(),
			type: 'POST',
			success: function(data){
				$("#modalboxpopup").html(data).dialog({modal: true, "position": "top"});
			}
		});
		return false;
	}
	else
	{
		alert("please provide your friends name and email");
	}
}

var counts=0;
function addFriends(cnt)
{
	//converting cnt to int
	cnts = parseInt(cnt);
	//if total count and field value not equal then decreament field value
	if(cnts!=counts)
		counts--;
	//hidden total
	var gcount = parseInt(document.getElementById('count').value);
	var friendname = document.getElementById('friendname'+counts).value;
	var friendemail = document.getElementById('friendemail'+counts).value;
	if(friendname=="" || friendemail=="")
	{
		alert("please provide your friends name and email");
	}
	else
	{
		if(gcount<3)
		{
			//document.getElementById('add'+counts).style.display='none';
			counts=cnts+1;
			var html = '<div id="fid'+counts+'" style="float: left;">';
			html += '<table><tr><td>Friends name:</td>';
			html += '<td><input type="text" id="friendname'+counts+'" name="data[ProductsSaf]['+counts+'][friendname]" value="" class="search_input"/><span style="color:red"> * </span></td>';
			html += '<td>Friends email address:</td>';
			html += '<td><input type="text" id="friendemail'+counts+'" name="data[ProductsSaf]['+counts+'][friendemail]" value="" class="search_input"/><span style="color:red"> * </span></td>';
			html += '<td><button onclick="addFriends('+counts+')" id="add'+counts+'" type="button" class="button_search_module rounded_corners_button" style="width:50px">Add</button>';
			html += '</td><td><button type="button"  onClick="removeFriend(this)" class="button_search_module rounded_corners_button btn_remove" style="width:60px;margin-left:5px">Remove</button></td></tr></table></div>';
			$(html).insertAfter( "#fid" );
			document.getElementById('count').value = gcount+1;
		}
		else
		{
			alert('Inviting friends limit is over');
		}
	}	
}
	
function removeFriend(cnt)
{	
	var gcount = parseInt(document.getElementById('count').value);
	//var childDiv = "fid"+parseInt(cnt);
	//if (document.getElementById(childDiv)) {     
      //    var child = document.getElementById(childDiv);
      //    var parent = document.getElementById("fid");
       //   parent.removeChild(child);
	   	$(cnt).parent('td').parent('tr').parent('tbody').parent('table').parent('div').remove();
          document.getElementById('count').value = gcount-1;
   //  }
}



</script>

<style>#MB_window{overflow:scroll !important}</style>

<?php if ($this->Session->read('user') || $this->Session->read('client.Domain.public') == '2') : ?>
<?php echo $this->element('message'); ?>	
	<div id="home_modules">
			<table>
				<tr>
					<td colspan="2">
						<div id="first_row">
							<?php echo $this->element('module2', array('module2' => 'Keyword_search', 'title' => 'Search'));?>
						  	<div style="clear:both;"> </div>
					 	</div> 
					</td>
				</tr>
				<tr>
					<td valign="top" colspan="2">
						<div id="second_row" style="width:950px;height:490px" class="homepage_box rounded-corners_bottom">
					  		<div id="left_column">
					  			<?php if($this->Session->read('client.Client.program_id') == 21) :?>
									<?php echo $this->element('hot_module', array('module' => 'hot_offers', 'title' => 'Hot Offers')); ?>
								<?php else :?>
									<?php echo $this->element('hot_module', array('module' => 'hot_offers', 'title' => 'Daily Deals!')); ?>
								<?php endif; ?>
						  	</div>
						  	
						  	<div id="right_column" style="padding-left:10px;padding-top:6px;">
								<div>
									<?php echo $this->element('stay_in_touch_module'); ?>
								</div>
							</div>
						 </div>
					</td>
					
				</tr>
				<tr>
					<td colspan="2">
						<div id="third_row" style="margin-left:0px">
							<?php echo $this->element('special_offers', array('special_offers' => $module_data['special_offers'], 'usedcount' => $module_data['used_count'])); ?>
						</div>
					</td>
				</tr>
				<tr>
					<td valign="top" style="width:500px">
						<table>
							<tr>
								<td>
									<?php echo $this->element('news_module'); ?>
								</td>
							</tr>
						</table>
					</td>
					<td valign="top">
						<?php echo $this->element('check_online_module'); ?>
					</td>
				</tr>
				<tr><td style="height:10px;"></td></tr>
			</table>
		</div>
<?php else : ?>
<div class="rounded-corners_bottom" style="width:950px;border:1px solid #cccccc">
		<div style="padding-left:5px;">
			<?php echo $this->element('message'); ?>
			<?php echo $page['ClientPage']['content']; ?>
		</div>
	</div>
<?php endif; ?>
 
