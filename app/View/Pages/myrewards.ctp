<?php if ($this->Session->read('user') || $this->Session->read('client.Domain.public') == '1') : ?>
<?php echo $this->element('message'); ?>	
		<div id="home_modules">
			<table>
				<tr>
					<td colspan="2">
						<div id="first_row">
							<?php echo $this->element('module2', array('module2' => 'Keyword_search', 'title' => 'Search'));?>
						  	<div style="clear:both;"> </div>
					 	</div> 
					</td>
				</tr>
				<tr>
					<td valign="top" colspan="2">
						<div id="second_row" style="width:950px;height:490px" class="homepage_box rounded-corners_bottom">
					  		<div id="left_column">
					  			<?php if($this->Session->read('client.Client.program_id') == 21) :?>
									<?php echo $this->element('hot_module', array('module' => 'hot_offers', 'title' => 'Hot Offers')); ?>
								<?php else :?>
									<?php echo $this->element('hot_module', array('module' => 'hot_offers', 'title' => 'Daily Deals!')); ?>
								<?php endif; ?>
						  	</div>
						  	
						  	<div id="right_column" style="padding-left:10px;padding-top:2px;">
								<?php echo $this->element('replacement_module'); ?> 
								<?php //echo $this->element('top_module', array('module' => 'googlemap', 'title' => 'What\'s Around You')); ?> 
							</div>
						 </div>
					</td>
					
				</tr>
				<tr>
					<td colspan="2">
						<div id="third_row" style="margin-left:0px">
							<?php echo $this->element('special_offers', array('special_offers' => $module_data['special_offers'], 'usedcount' => $module_data['used_count'])); ?>
						</div>
					</td>
				</tr>
				<tr>
					<td valign="top" style="width:500px">
						<table>
							<?php if(($this->Session->read('client.Client.id') != 1080) && ($this->Session->read('client.Client.program_id') != 21) && ($this->Session->read('client.Client.program_id') != 38) && ($this->Session->read('client.Client.program_id') != 40) && ($this->Session->read('client.Client.program_id') != 45)) :?>
								<tr>
								<td>
								<div id="fourth_row" style="margin-left:0px;">
								<div id="left_column">
								<?php echo $this->element('module', array('module' => 'favourites', 'title' => '')); ?>
								</div>
								</div>
								</td>
								</tr>
							<?php endif; ?>
							<tr>
								<td>
									<?php echo $this->element('news_module'); ?>
								</td>
							</tr>
						</table>
					</td>
					<td valign="top">
						<?php echo $this->element('check_online_module'); ?>
					</td>
				</tr>
				<tr><td style="height:10px;"></td></tr>
			</table>
		</div>
<?php else : ?>
<div class="rounded-corners_bottom" style="width:950px;border:1px solid #cccccc">
		<div style="padding-left:5px;">
			<?php echo $this->element('message'); ?>
			<?php echo $page['ClientPage']['content']; ?>
		</div>
	</div>
<?php endif; ?>
 
