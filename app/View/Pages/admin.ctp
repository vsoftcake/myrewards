<style>
.icondiv {
         float:left;
         padding: 35px;
         width: 100px; 
         height: 85px;
         white-space:nowrap;
         text-align:center;
         
         }
.link_icons
{
	font-weight:bold;
	font-size:13px;
	color:#565454 !important;
}
         
</style>
<div style="width:953px;border-left:1px solid #cccccc;border-right:1px solid #cccccc;border-bottom:1px solid #cccccc;margin-left:-14px;min-height:250px;overflow:hidden;">
	<div style="width:885px;padding-left:45px;min-height:150px;overflow:hidden;">
			
			<div class="icondiv">
			     <p style="height:80px">
		     	 <a href="<?php echo $this->Html->url('/admin/styles/'); ?>">
		     		<img style="cursor:pointer" src="/files/admin_home_icons/styles.png"  alt="Styles" title="Styles" />
		     	 </a>
			     </p>
			     <p><a class="link_icons" href="<?php echo $this->Html->url('/admin/styles/'); ?>">Styles</a></p> 
			</div>
			<?php if($this->Session->read('user.User.type') == 'Administrator'): ?>
			<div class="icondiv" >
			     <p style="height:80px">
		     	 <a href="<?php echo $this->Html->url('/admin/programs/'); ?>">
		     		<img style="cursor:pointer" src="/files/admin_home_icons/programs.png" alt="Programs" title="Programs" />
		     	 </a>
			     </p> 
			     <p><a class="link_icons"  href="<?php echo $this->Html->url('/admin/programs/'); ?>">Programs</a></p>  
			</div>
			<?php endif;?>
			<div class="icondiv" >
			     <p style="height:80px">
		     	 <a href="<?php echo $this->Html->url('/admin/clients/'); ?>">
		     		<img style="cursor:pointer" src="/files/admin_home_icons/clients.png"  alt="Clients" title="Clients" />
		     	 </a>
			     </p> 
			     <p><a class="link_icons"  href="<?php echo $this->Html->url('/admin/clients/'); ?>">Clients</a></p> 
			</div>
			<?php if($this->Session->read('user.User.type') == 'Administrator'): ?>
			<div class="icondiv" >
			     <p style="height:80px">
		     	 <a href="<?php echo $this->Html->url('/admin/domains/'); ?>">
		     		<img style="cursor:pointer"  src="/files/admin_home_icons/domains.png"  alt="Domains" title="Domains" />
		     	 </a>	
			     	
			     </p>  
			     <p><a class="link_icons"  href="<?php echo $this->Html->url('/admin/domains/'); ?>">Domains</a></p>  
			</div>
			<div class="icondiv" >
			     <p style="height:80px">
		     	 <a href="<?php echo $this->Html->url('/admin/categories/'); ?>">
		     		<img style="cursor:pointer"  src="/files/admin_home_icons/categories.png" alt="Categories" title="Categories" />
		     	 </a>
			     </p>  
			     <p><a class="link_icons"  href="<?php echo $this->Html->url('/admin/categories/'); ?>">Categories</a></p>  
			</div>
			<div class="icondiv" >
			     <p style="height:80px">
		     	 <a href="<?php echo $this->Html->url('/admin/merchants/'); ?>">
		     		<img style="cursor:pointer"  src="/files/admin_home_icons/merchants.png" alt="Merchants" title="Merchants" />
		     	 </a>
			     </p>  
			     <p><a class="link_icons"  href="<?php echo $this->Html->url('/admin/merchants/'); ?>">Merchants</a></p>  
			</div>
			<div class="icondiv" >
			     <p style="height:80px">
		     	 <a href="<?php echo $this->Html->url('/admin/products/'); ?>">
		     		<img style="cursor:pointer"  src="/files/admin_home_icons/products.png" alt="Products" title="Products" />
		     	 </a>
			     </p>  
			     <p><a class="link_icons"  href="<?php echo $this->Html->url('/admin/products/'); ?>">Products</a></p>  
			</div>
			<?php endif; ?>
			<div class="icondiv" >
			     <p style="height:80px">
		     	 <a href="<?php echo $this->Html->url('/admin/users/'); ?>">
		     		<img style="cursor:pointer"  src="/files/admin_home_icons/users.png" alt="Users" title="Users" />
		     	 </a>
			     </p>
			     <p><a class="link_icons"  href="<?php echo $this->Html->url('/admin/users/'); ?>">Users</a></p>  
			</div>
			<?php if($this->Session->read('user.User.type') == 'Administrator'): ?>
			<div class="icondiv" >
			     <p style="height:80px">
		         <a  href="<?php echo $this->Html->url('/admin/reports/'); ?>">
		                <img style="cursor:pointer"  src="/files/admin_home_icons/reports.png"  alt="Reports" title="Reports" />
		         </a>
			     </p>  
			     <p><a class="link_icons"  href="<?php echo $this->Html->url('/admin/reports/'); ?>">Reports</a></p>  
			</div>
			<div class="icondiv" >
			     <p style="height:80px">
		         <a href="<?php echo $this->Html->url('/admin/newsletters/'); ?>">
		                <img style="cursor:pointer"  src="/files/admin_home_icons/e-news.png" alt="Newsletters" title="Newsletters" />
		         </a>
			     </p>  
			     <p><a class="link_icons"  href="<?php echo $this->Html->url('/admin/newsletters/'); ?>">Newsletters</a></p>  
			</div>
			<div class="icondiv" >
			     <p style="height:80px">
		         <a href="<?php echo $this->Html->url('/admin/comments/'); ?>">
		                <img style="cursor:pointer"  src="/files/admin_home_icons/comments.png" alt="Comments" title="Comments" />
		         </a>
			     </p> 
			     <p><a class="link_icons"  href="<?php echo $this->Html->url('/admin/comments/'); ?>">Comments</a></p>  
			</div>
			<?php endif; ?>
			<div class="icondiv" >
			     <p style="height:65px;padding-top:15px;">
		         <a href="<?php echo $this->Html->url('/admin/skyscrapers/'); ?>">
		         	<img style="cursor:pointer"  valign="bottom" src="/files/admin_home_icons/skyscraper.png" alt="SkyScraper" title="SkyScraper" />
		         </a>
			     </p>  
			    <p><a class="link_icons"  href="<?php echo $this->Html->url('/admin/skyscrapers/'); ?>">SkyScraper</a></p>  
			</div>
			<?php if($this->Session->read('user.User.type') == 'Administrator'): ?>
			<div class="icondiv" >
			     <p style="height:80px">
			     <a href="<?php echo $this->Html->url('/admin/user_nominates/'); ?>">
			     		<img style="cursor:pointer"  src="/files/admin_home_icons/imf.png" alt="Invite My Family" title="Invite My Family" />
			     </a>
			     </p>  
			     <p><a class="link_icons"  href="<?php echo $this->Html->url('/admin/user_nominates/'); ?>">Invite My Family</a></p>  
			</div>
			<div class="icondiv" >
			     <p style="height:80px">
			     <a href="<?php echo $this->Html->url('/admin/advt_4_free/'); ?>">
			     		<img style="cursor:pointer"  src="/files/admin_home_icons/advt.png" alt="Advt 4 Free" title="Advt 4 Free" />
			     </a>
			     </p>  
			     <p><a class="link_icons"  href="<?php echo $this->Html->url('/admin/advt_4_free/'); ?>">Advt 4 Free</a></p>  
			</div>
			<div class="icondiv" >
			     <p style="height:80px">
				 <a href="<?php echo $this->Html->url('/admin/campaigns/'); ?>">
					<img style="cursor:pointer;height:75px;height:75px;"  src="/files/admin_home_icons/compaign.png" alt="campaign" title="Campaign" />
				 </a>
			     </p>  
			     <p><a class="link_icons"  href="<?php echo $this->Html->url('/admin/campaigns/'); ?>">Competitions</a></p>  
			</div>
			
			<div class="icondiv" >
			     <p style="height:80px">
				 <a href="<?php echo $this->Html->url('/admin/carts/'); ?>">
					<img style="cursor:pointer;height:75px;height:75px;"  src="/files/admin_home_icons/shop.png" alt="Shopping Cart" title="Shopping Cart" />
				 </a>
			     </p>  
			     <p><a class="link_icons"  href="<?php echo $this->Html->url('/admin/carts/'); ?>">Shopping Cart</a></p>  
			</div>
			<div class="icondiv" >
			     <p style="height:80px">
				 <a href="<?php echo $this->Html->url('/admin/points/'); ?>">
					<img style="cursor:pointer;height:75px;height:75px;"  src="/files/admin_home_icons/points.png" alt="points" title="Points" />
				 </a>
			     </p>  
			     <p><a class="link_icons"  href="<?php echo $this->Html->url('/admin/points/'); ?>">Points</a></p>  
			</div>
			<div class="icondiv" >
			     <p style="height:80px">
				 <a href="<?php echo $this->Html->url('/admin/saf/'); ?>">
					<img style="cursor:pointer;height:75px;height:75px;"  src="/files/admin_home_icons/saf.png" alt="SAF Products" title="SAF Products" />
				 </a>
			     </p>  
			     <p><a class="link_icons"  href="<?php echo $this->Html->url('/admin/saf/'); ?>">SAF Products</a></p>  
			</div>
			<div class="icondiv" >
			     <p style="height:80px;">
				 <a href="<?php echo $this->Html->url('/admin/safs_invitation/'); ?>">
					<img style="cursor:pointer;height:75px;height:75px;"  src="/files/admin_home_icons/invitation.png" alt="Send A Friend Invitation" title="Send A Friend Invitation" />
				 </a>
			     </p>  
			     <p><a class="link_icons"  href="<?php echo $this->Html->url('/admin/safs_invitation/'); ?>">SAF Invitation</a></p>  
			</div>
			<div class="icondiv" >
			     <p style="height:80px;">
				 <a href="<?php echo $this->Html->url('/admin/safs_invitation/saf_logs/'); ?>">
					<img style="cursor:pointer;height:75px;height:75px;"  src="/files/admin_home_icons/saf_log.png" alt="Send A Friend Log" title="Send A Friend Log" />
				 </a>
			     </p>  
			     <p><a class="link_icons"  href="<?php echo $this->Html->url('/admin/safs_invitation/saf_logs/'); ?>">SAF Log</a></p>  
			</div>
			<div class="icondiv" >
			     <p style="height:80px;">
				 <a href="<?php echo $this->Html->url('/admin/auto_logins/'); ?>">
					<img style="cursor:pointer;height:75px;height:75px;"  src="/files/admin_home_icons/auto_login.jpg" alt="Auto Login" title="Auto Login" />
				 </a>
			     </p>  
			     <p><a class="link_icons"  href="<?php echo $this->Html->url('/admin/auto_logins/'); ?>">Auto Login</a></p>  
			</div>
			<?php endif; ?>
	</div>
	<br/><br/>
</div>
