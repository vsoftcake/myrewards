<style type="text/css">
	.rounded-corners 
	{
		-moz-border-radius: 10px 10px 0px 0px; /* Firefox */
		-webkit-border-radius: 10px 10px 0px 0px; /* Safari, Chrome */
		border-radius: 10px 10px 0px 0px; /* CSS3 */
	}
	.tr 
	{
		height:35px;
	}
	.admin_button
	{
		height: 26px;
		padding-bottom: 2px;
		border-radius: 5px 5px 5px 5px;
		background-color: #0066cc;
		border: 1px solid #cccccc;
		color: #FFFFFF; 
		cursor: pointer;
		font-size:13px;
	}
</style>
<div style="width:953px;border-left:1px solid #cccccc;border-right:1px solid #cccccc;border-bottom:1px solid #cccccc;margin-left:-14px;">
	<div style="width:949px;height:50px;">
		<div style=" height: 40px; float:left; width: 608px;padding-left:37px;">
	       	<div style=" height: 40px; float:left;padding-top:5px;">
	        	<img title="Reports" alt="Reports" src="/files/admin_home_icons/reports.png" style="width:40px;height:40px;">
	        </div>
	        <div style=" height: 40px;float:left;padding-left:10px;">
	        	<h2>User Login Info</h2>
	        </div> 
		</div>
        <div class="actions" style=" height: 40px; float:left;  width: 300px;">
			<ul style="padding:20px;margin-left:-28px;">
				<li style="display:inline;"><?php echo $this->Form->button('Cancel', array('onclick' => "location.href='/admin/reports'",
				'class'=>'admin_button','style'=>'width:60px;', 'type'=>'button'));  ?></li>
	        </ul>
		</div>
	</div>
	
	<div>
			<div class="tabber" id="mytab1">
				
				<div class="tabbertab"  style="min-height:100px;overflow:hidden">
					<h2>Client And User</h2>
			    	<h4>Client And User</h4>
		      		<div style="background-color:#EFEFEF;border:1px solid #cccccc;width:750px;padding:30px;min-height:25px;overflow:hidden;"> 
		      			<div>
							<form name="clientsForm"  method="post" >
					       		<table>
						     		<tr>
							   			<td width="65px"><strong>Clients</strong></td>
							     		<td>
								   			<?php echo $this->Form->input('Report.client_id', array('multiple'=>'multiple', 'options'=>$clients, 'div'=>false, 'label'=>false, 'onChange'=>'checkClients()')); ?>
								       	</td>
								    </tr> 
								</table>
							</form>
						</div>	
						<br/>
						<div id="UserListSearch" style="display:none">
							<form onsubmit="return false;" name="UserListSearch">
						    	<table>
							   		<tr>
										<td width="65px"><strong>Find Users</strong></td>
										<td>
											<?php echo $this->Form->input('Report.psearch'); ?>
										</td>
										<td>
											<button onClick="searchUsers()" class="admin_button">Search</button>
										</td>
							   		</tr>
						   		</table>
							</form>
							<br/>
							<div id="users" style="margin:0;"></div>
						</div>
					</div>
		      	</div>
		      	
			</div>
		</form>
	</div>
</div>

<script>
var val = 0;
	function checkClients()
	{
		if(document.getElementById('ReportClientId').value !='')
			document.getElementById('UserListSearch').style.display='block';
		else
		{
			document.getElementById('UserListSearch').style.display='none';
		}	
	}
	
	function searchUsers()
	{
		var user = document.getElementById('ReportPsearch').value;
		name = document.forms["clientsForm"].elements["data[Report][client_id]"].value;
		if(user=="")
			alert("Please type a user name");	
		else
		{
			$.ajax({
				url: '/admin/reports/user_search/'+user+'/'+name,
				type: "GET",
				success: function (data) {
					$('#users').html(data);
				}
			});
		}
	}
	
	function addUsers()
	{
		var url = '/admin/reports/user_export_search/';
		var usersList="";
		var length=document.clientUserExport.elements.length;
		for(var i=0;i<length;i++)
		{
			if(document.clientUserExport.elements[i].name=="data[Report][check]")
			{
				if(document.clientUserExport.elements[i].checked)
				{
					if(usersList.length>0) {
						usersList=usersList+","+document.clientUserExport.elements[i].value;
				}	else {
						usersList=usersList+document.clientUserExport.elements[i].value;
						}
				}
			}
		}
		
		if(usersList=="")
			alert("please check the users to get report");	
		else
			document.location=url+usersList;
	}
	</script>
	
	
	

	
	