<script>
	function showGraph()
	{
		var e = document.getElementById("ReportClientId").value;
		if(e == "")
		 	alert("Please select atleast one client");
		else
			document.userGraph.submit();
	}
</script>
<style type="text/css">

	.rounded-corners 
	{
		-moz-border-radius: 10px 10px 0px 0px; /* Firefox */
		-webkit-border-radius: 10px 10px 0px 0px; /* Safari, Chrome */
		border-radius: 10px 10px 0px 0px; /* CSS3 */
	}
	.tr 
		{
			height:35px;
		}
	.admin_button
		{
		height: 24px;
		padding-bottom: 5px;
		border-radius: 5px 5px 5px 5px;
		background-color: #0066cc;
		border: 1px solid #cccccc;
		color: #FFFFFF; 
		cursor: pointer;
		font-size:13px;
	}
</style>

<div style="width:953px;border-left:1px solid #cccccc;border-right:1px solid #cccccc;border-bottom:1px solid #cccccc;margin-left:-14px;">
	<div style="width:949px;height:50px;">
		<div style=" height: 40px; float:left; width: 608px;padding-left:37px;">
	       	<div style=" height: 40px; float:left;padding-top:5px;">
	        	<img title="Reports" alt="Reports" src="/files/admin_home_icons/reports.png" style="width:40px;height:40px;">
	        </div>
	        <div style="height: 40px;float:left;padding-left:10px;">
	        	<h2>Registered Users Per Client Graph</h2>
	        </div> 
		</div>
        <div class="actions" style=" height: 40px; float:left;  width: 300px;">
			<ul style="padding:20px;margin-left:-28px;">
				<li style="display:inline;"><?php echo $this->Form->button('Cancel', array('onclick' => "location.href='/admin/reports'",
				'class'=>'admin_button','style'=>'width:60px;', 'type'=>'button'));  ?></li>
	        </ul>
		</div>
	</div>
	
	<div>
		<form name="userGraph" action="<?php echo $this->Html->url('/admin/reports/user_graph_report/'); ?>" method="post" enctype="multipart/form-data">
			<div class="tabber" id="mytab1">
			
				<div class="tabbertab"  style="min-height:100px;overflow:hidden">
  					<h2>Clients And Duration</h2>
			    	<h4>Clients And Duration</h4>
		      		<div style="background-color:#EFEFEF;border:1px solid #cccccc;width:750px;padding:30px;">
						<table>
							<tr class="tr">
								<td><strong>Start Date</strong></td>
								<td>
									<?php echo $this->Form->input('Report.start_date', array('dateFormat'=>'DM', 'type'=>'date', 'div'=>false, 'label'=>false, 'selected'=>$start_default, 'class'=>'select')); 
									echo $this->Form->year('start_year', date("Y")-10, date("Y"), array('default'=>"$start_year", 'div'=>false, 'label'=>false));?>
								</td>
							</tr>
							<tr class="tr">
								<td><strong>End Date</strong></td>
								<td>
									<?php echo $this->Form->input('Report.end_date', array('dateFormat'=>'DM', 'type'=>'date', 'div'=>false, 'label'=>false, 'selected'=>$end_default)); 
									echo $this->Form->year('end_year', date("Y")-10, date("Y"), array('default'=>"$end_year", 'div'=>false, 'label'=>false)); ?>
								</td>
							</tr>
							<tr class="tr">
					   			<td><strong>Clients</strong></td>
					     		<td>
						   			<?php echo $this->Form->input('Report.client_id', array('multiple'=>'multiple', 'options'=>$clients, 'div'=>false, 'label'=>false)); ?>
						       	</td>
				    		</tr> 
				    		<tr class="tr">
					     		<td colspan="2">
						   			<b>NOTE:</b> For better apperance select maximum of eight number of clients
						       	</td>
	   	 					</tr>
						</table>
			  		</div>
			  		<br/>
			       	<div align="center" style="padding-top:10px">
						<button type="submit" class="admin_button">Generate Report</button>
			       	</div>  
			  	</div>
			  	
			</div>
		</form>
	</div>
</div>