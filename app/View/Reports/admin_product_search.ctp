<?php $this->Paginator->options(array('update' => '#prods','evalScripts' => true,'url' => array_merge($this->params['pass'], $this->params['named'])));
?>

<form name="clientProductExport" method="post">
<div class="list">
	<table cellpadding="0" cellspacing="0">
	<tr>
		<th>&nbsp;</th>
		<th style="text-align:left">id</th>
		<th style="text-align:left">name</th>
		<th style="text-align:left">client</th>
	</tr>
	<?php
	$i = 0;
	foreach ($products_list as $product):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
		<tr<?php echo $class;?>>
			<td>
				<?php echo $this->Form->checkbox('Report.check',array('value'=>$product['Product']['id'].'|'.$product['Client']['id'])); ?>
			</td>
			<td>
				<?php echo $product['Product']['id'] ;?>
			</td>
			<td>
				<?php echo $product['Product']['name'] ;?>
			</td>
			<td>
				<?php echo $product['Client']['name'];?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
	<div class="paging">
		<?php echo $this->Paginator->first('<<', array('title'=>'first','class'=>'disabled pagination1'));?>
		<?php echo $this->Paginator->prev('<', array('title'=>'prev','class'=>'disabled pagination1'));?>
		<?php echo $this->Paginator->numbers(array('class'=>'pagination1'));?>
		<?php echo $this->Paginator->next('>', array('title'=>'next','class'=>'disabled pagination1'));?>
		<?php echo $this->Paginator->last('>>', array('title'=>'last','class'=>'disabled pagination1'));?>
	
		<p><?php echo $this->Paginator->counter(array('format' => __('Page %page% of %pages%', true)));echo $this->Js->writeBuffer();	?></p>
	</div>
	<button type="button" class="admin_button" onclick="addProducts()">Generate Report</button>
</div>
</form>