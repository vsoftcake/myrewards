<script>
function addProducts()
{
	var url = '/admin/reports/offer_export_report/';
	var productsList="";
	var length=document.offerExportReport.elements.length;
	for(var i=0;i<length;i++)
	{
		if(document.offerExportReport.elements[i].name=="data[Report][check]")
		{
			if(document.offerExportReport.elements[i].checked)
			{
				if(productsList.length>0)
					productsList=productsList+","+document.offerExportReport.elements[i].value;
				else
					productsList=productsList+document.offerExportReport.elements[i].value;
			}
		}
	}
	if(productsList=="")
		alert("please check the products to get report");	
	else{
		var startDay = document.getElementById('ReportStartDateDay');
		startDay = startDay.options[startDay.selectedIndex].value;
		var startMonth = document.getElementById('ReportStartDateMonth');
		startMonth = startMonth.options[startMonth.selectedIndex].value;
		var startYear = document.getElementById('start_yearYear');
		startYear = startYear.options[startYear.selectedIndex].value;
		
		var endDay = document.getElementById('ReportEndDateDay');
		endDay = endDay.options[endDay.selectedIndex].value;
		var endMonth = document.getElementById('ReportEndDateMonth');
		endMonth = endMonth.options[endMonth.selectedIndex].value;
		var endYear = document.getElementById('end_yearYear');
		endYear = endYear.options[endYear.selectedIndex].value;

		var startDate = startYear+'-'+startMonth+'-'+startDay+'/';
		var endDate = endYear+'-'+endMonth+'-'+endDay+'/';
		document.location=url+startDate+endDate+productsList;
	}
}

</script>
<style type="text/css">
	.rounded-corners 
	{
		-moz-border-radius: 10px 10px 0px 0px; /* Firefox */
		-webkit-border-radius: 10px 10px 0px 0px; /* Safari, Chrome */
		border-radius: 10px 10px 0px 0px; /* CSS3 */
	}
	.tr 
	{
		height:35px;
	}
	.admin_button
	{
		height: 26px;
		padding-bottom: 2px;
		border-radius: 5px 5px 5px 5px;
		background-color: #0066cc;
		border: 1px solid #cccccc;
		color: #FFFFFF; 
		cursor: pointer;
		font-size:13px;
	}
</style>
<div style="width:953px;border-left:1px solid #cccccc;border-right:1px solid #cccccc;border-bottom:1px solid #cccccc;margin-left:-14px;">
	<div style="width:949px;height:50px;">
		<div style=" height: 40px; float:left; width: 608px;padding-left:37px;">
	       	<div style=" height: 40px; float:left;padding-top:5px;">
	        	<img title="Reports" alt="Reports" src="/files/admin_home_icons/reports.png" style="width:40px;height:40px;">
	        </div>
	        <div style=" height: 40px;float:left;padding-left:10px;">
	        	<h2>Export selected products</h2>
	        </div> 
		</div>
        <div class="actions" style=" height: 40px; float:left;  width: 300px;">
			<ul style="padding:20px;margin-left:-28px;">
				<li style="display:inline;"><?php echo $this->Form->button('Cancel', array('onclick' => "location.href='/admin/reports'",
				'class'=>'admin_button','style'=>'width:60px;', 'type'=>'button'));  ?></li>
	        </ul>
		</div>
	</div>
	
	<div>
		<p style="padding-left:40px;">Export product information of selected products.</p>
			<div class="tabber" id="mytab1">
				<div class="tabbertab"  style="min-height:180px;overflow:hidden">
					<h2>Export selected products</h2>
			    	<h4>Export selected products</h4>
		      		<div style="background-color:#EFEFEF;border:1px solid #cccccc;width:750px;padding:30px;min-height:100px;overflow:hidden;"> 
						<form action="<?php echo $this->Html->url('/admin/reports/offer_report/'); ?>" method="get" id="SearchForm">
							<table>
								<tr>
									<td><strong>Find Products</strong></td>
									<td>
										<?php echo $this->Form->input('Report.search', array('div'=>false, 'label'=>false)); ?>
									</td>
									<td><button>Search</button></td>
								</tr>
								<tr class="tr">
									<td><strong>Start Date</strong></td>
									<td>
										<?php echo $this->Form->input('Report.start_date', array('dateFormat'=>'DM', 'type'=>'date', 'div'=>false, 'label'=>false, 'selected'=>$start_default, 'class'=>'select')); 
										echo $this->Form->year('start_year', date("Y")-10, date("Y"), array('default'=>"$start_year", 'div'=>false, 'label'=>false));?>
									</td>
								</tr>
								<tr class="tr">
									<td><strong>End Date</strong></td>
									<td>
										<?php echo $this->Form->input('Report.end_date', array('dateFormat'=>'DM', 'type'=>'date', 'div'=>false, 'label'=>false, 'selected'=>$end_default)); 
										echo $this->Form->year('end_year', date("Y")-10, date("Y"), array('default'=>"$end_year", 'div'=>false, 'label'=>false)); ?>
									</td>
								</tr>
							</table>
						</form>
						<br/>
						<form name="offerExportReport" method="post">
							<div class="list">
								<div style="float:left;padding-right:30px">
									<table cellpadding="0" cellspacing="0" style="width:350px">
										<tr>
											<th>&nbsp;</th>
											<th>id</th>
											<th>name</th>
										</tr>
										<?php
										$i = 0;$count=0;
										foreach ($products as $product):
											$class = null;
											if ($i++ % 2 == 0) {
												$class = ' class="altrow"';
											}
											$count++;
											if($count==21):
										?>
										</table>
									</div>
									<div style="float:left">
										<table cellpadding="0" cellspacing="0" style="width:350px">
										<tr>
											<th>&nbsp;</th>
											<th>id</th>
											<th>name</th>
										</tr>
										<?php endif; ?>
										<tr<?php echo $class;?>>
											<td>
												<?php echo $this->Form->checkbox('Report.check',array('value'=>$product['Product']['id'])); ?>
											</td>
											<td>
												<?php echo $product['Product']['id'] ;?>
											</td>
											<td>
												<?php echo $product['Product']['name'] ;?>
											</td>
										</tr>
										<?php endforeach; ?>
									</table>
								</div>
								<br/>
								<div class="paging">
									<?php echo $this->Paginator->first('<<', array('title'=>'first','class'=>'disabled pagination1'));?>
									<?php echo $this->Paginator->prev('<', array('title'=>'prev','class'=>'disabled pagination1'));?>
									<?php echo $this->Paginator->numbers(array('class'=>'pagination1'));?>
									<?php echo $this->Paginator->next('>', array('title'=>'next','class'=>'disabled pagination1'));?>
									<?php echo $this->Paginator->last('>>', array('title'=>'last','class'=>'disabled pagination1'));?>
								
									<p><?php echo $this->Paginator->counter(array('format' => __('Page %page% of %pages%', true)));	?></p>
								</div>
								<br/>
								<div align="center">
									<button class="admin_button" type="button" onclick="addProducts()">Generate Report</button>
								</div>
							</div>
						</form>
					</div>
		      	</div>
			</div>
		</form>
	</div>
</div>


