<style type="text/css">
	.rounded-corners 
	{
		-moz-border-radius: 10px 10px 0px 0px; /* Firefox */
		-webkit-border-radius: 10px 10px 0px 0px; /* Safari, Chrome */
		border-radius: 10px 10px 0px 0px; /* CSS3 */
	}
	.tr 
	{
		height:35px;
	}
	.admin_button
	{
		height: 26px;
		padding-bottom: 2px;
		border-radius: 5px 5px 5px 5px;
		background-color: #0066cc;
		border: 1px solid #cccccc;
		color: #FFFFFF; 
		cursor: pointer;
		font-size:13px;
	}
</style>
<div style="width:953px;border-left:1px solid #cccccc;border-right:1px solid #cccccc;border-bottom:1px solid #cccccc;margin-left:-14px;">
	<div style="width:949px;height:50px;">
		<div style=" height: 40px; float:left; width: 608px;padding-left:37px;">
	       	<div style=" height: 40px; float:left;padding-top:5px;">
	        	<img title="Reports" alt="Reports" src="/files/admin_home_icons/reports.png" style="width:40px;height:40px;">
	        </div>
	        <div style=" height: 40px;float:left;padding-left:10px;">
	        	<h2>Export users</h2>
	        </div> 
		</div>
        <div class="actions" style=" height: 40px; float:left;  width: 300px;">
			<ul style="padding:20px;margin-left:-28px;">
				<li style="display:inline;"><?php echo $this->Form->button('Cancel', array('onclick' => "location.href='/admin/reports'",
				'class'=>'admin_button','style'=>'width:60px;', 'type'=>'button'));  ?></li>
	        </ul>
		</div>
	</div>
	
	<div>
		<p style="padding-left:40px;">Export client data.</p>
		<form action="<?php echo $this->Html->url('/admin/reports/users_with_prefix_export/'); ?>" method="post" onsubmit="return checkForm();">
			<div class="tabber" id="mytab1">
				
				<div class="tabbertab"  style="min-height:180px;overflow:hidden">
					<h2>Export Users With Prefix</h2>
			    	<h4>Export Users With Prefix</h4>
		      		<div style="background-color:#EFEFEF;border:1px solid #cccccc;width:750px;padding:30px;"> 
						<table>
							<tr class="tr">
								<td class="bt">Clients</td>
								<td class="br bt"><?php echo $this->Form->input('Report.client', array('div'=>false, 'label'=>false, 'type'=>'select', 'options'=>$clients)); ?></td>
							</tr>
							<tr class="tr">
								<td>Prefix</td>
								<td class="br">
									<?php echo $this->Form->input('Report.prefix', array('div'=>false, 'label'=>false)); ?>
								</td>
							</tr>
							<tr class="tr">
								<td>Please select one </td>
								<td class="br">
									<?php echo $this->Form->input('Report.check',	array('type' => 'radio','options' => array('R'=>'Registered', 'U'=>'Unregistered','A'=>'All'),'class'=>'input_radio','separator'=>'<br/>','legend'=>false));?>
								</td>
							</tr>
						</table>
					</div>
			    	<br/>
			       	<div align="center" style="padding-top:10px">
						<button type="submit" class="admin_button">Generate Report</button>
			       	</div>
		      	</div>
		      	
			</div>
		</form>
	</div>
</div>

<script type="text/javascript" src="<?php echo $this->Html->url('/js/reports.js'); ?>"></script>
