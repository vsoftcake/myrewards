<script>
	var val = 0;
	function checkClients(id)
	{
		if (document.getElementById(id).checked==true)
		{
			document.getElementById('prodListSearch').style.display='block';
			val = val+1;
		}
		else
		{
			val = val-1;
			if(val==0)
				document.getElementById('prodListSearch').style.display='none';
		}
	}
	function searchPrdts()
	{
		var prod = document.getElementById('ReportPsearch').value;
		
		var clientsList="";
		var length=document.clientsForm.elements.length;
		for(var i=0;i<length;i++)
		{
			if(document.clientsForm.elements[i].name=="data[Report][check]")
			{
				if(document.clientsForm.elements[i].checked)
				{
					if(clientsList.length>0)
						clientsList=clientsList+","+document.clientsForm.elements[i].value;
					else
						clientsList=clientsList+document.clientsForm.elements[i].value;
				}
			}
		}
		if(clientsList=="")
			alert("please check the products to get report");	
		else
		{
			$.ajax({
				url: '/admin/reports/product_search/'+prod+'/'+clientsList,
				type: "GET",
				success: function (data) {
					$('#prods').html(data);
				}
			});
		}
	}
	function addProducts()
	{
		var url = '/admin/reports/product_export_search/';
		var productsList="";
		var length=document.clientProductExport.elements.length;
		for(var i=0;i<length;i++)
		{
			if(document.clientProductExport.elements[i].name=="data[Report][check]")
			{
				if(document.clientProductExport.elements[i].checked)
				{
					if(productsList.length>0)
						productsList=productsList+","+document.clientProductExport.elements[i].value;
					else
						productsList=productsList+document.clientProductExport.elements[i].value;
				}
			}
		}
		
		if(productsList=="")
			alert("please check the products to get report");	
		else
			document.location=url+productsList;
	}
</script>

<style type="text/css">

	.rounded-corners 
	{
		-moz-border-radius: 10px 10px 0px 0px; /* Firefox */
		-webkit-border-radius: 10px 10px 0px 0px; /* Safari, Chrome */
		border-radius: 10px 10px 0px 0px; /* CSS3 */
	}
	.tr 
		{
			height:35px;
		}
	.admin_button
		{
		height: 24px;
		padding-bottom: 5px;
		border-radius: 5px 5px 5px 5px;
		background-color: #0066cc;
		border: 1px solid #cccccc;
		color: #FFFFFF; 
		cursor: pointer;
		font-size:13px;
	}
</style>
<div style="width:953px;border-left:1px solid #cccccc;border-right:1px solid #cccccc;border-bottom:1px solid #cccccc;margin-left:-14px;">
	<div style="width:949px;height:50px;">
		<div style=" height: 40px; float:left; width: 608px;padding-left:37px;">
	       	<div style=" height: 40px; float:left;padding-top:5px;">
	        	<img title="Reports" alt="Reports" src="/files/admin_home_icons/reports.png" style="width:40px;height:40px;">
	        </div>
	        <div style=" height: 40px;float:left;padding-left:10px;">
	        	<h2>Export selected products for clients</h2>
	        </div> 
		</div>
        <div class="actions" style=" height: 40px; float:left;  width: 300px;">
			<ul style="padding:20px;margin-left:-28px;">
				<li style="display:inline;"><?php echo $this->Form->button('Cancel', array('onclick' => "location.href='/admin/reports'",
				'class'=>'admin_button','style'=>'width:60px;', 'type'=>'button'));  ?></li>
				
	        </ul>
		</div>
	</div>

   <p style="padding-left:40px">Export product information for selected clients.</p>
   <div class="tabber" id="mytab1">
            <div class="tabbertab"  style="min-height:100px;overflow:hidden">
			     <h2>Products</h2>
			  <h4>Products</h4>   
				  <div style="background-color:#EFEFEF;border:1px solid #cccccc;width:750px;min-height:260px;overflow:hidden;padding:30px;">
						<form action="<?php echo $this->Html->url('/admin/reports/client_products/'); ?>" method="get" id="SearchForm" name="SearchForm">	
							<table>
								<tr>
									<td>Find Clients</td>
									<td>
										<?php echo $this->Form->input('Report.search', array('div'=>false, 'label'=>false)); ?>
									</td>
									<td><button class="admin_button">Search</button></td>
								</tr>
							</table>
							</form>
	                     <br/><br/>
						<div class="list">
							<form name="clientsForm" onsubmit="return false;">
								<table cellpadding="0" cellspacing="0">
								<!--<tr><td colspan="2"><button onClick="checkClients()">Get products</button></td></tr>-->
								<tr style="text-align:left">
								<th>&nbsp;</th>
								<th><?php echo $this->Paginator->sort('name');?></th>
								</tr>
								<?php
								$i = 0;
								foreach ($clients as $client): ?>
									<?php if ($client['Client']['password'] != 'SBDummy@#%#' ) : ?>
									<?php
										$class = null;
										if ($i++ % 2 == 0) {
											$class = ' class="altrow"';
										}
									?>
										<?php if($client['Client']['name']!='') :?>
											<tr<?php echo $class;?>>
												<td>
													<?php echo $this->Form->checkbox('Report.check',array('value'=>$client['Client']['id'],'id'=>$client['Client']['id'], 'onClick'=>'checkClients(this.value)')); ?>
												</td>
												<td style="width:180px">
													<?php echo $client['Client']['name'] ?>
												</td>
											</tr>
										<?php endif; ?>
									<?php endif; ?>
								<?php endforeach; ?>
								</table>
							
								<div class="paging">
									<!--<?php echo $this->Paginator->prev('<< '.__('previous', true), array(), null, array('class'=>'disabled'));?>
								  	<?php echo $this->Paginator->numbers();?>
									<?php echo $this->Paginator->next(__('next', true).' >>', array(), null, array('class'=>'disabled'));?>-->
									<?php echo $this->Paginator->first('<<', array('title'=>'first','class'=>'disabled pagination1'));?>
									<?php echo $this->Paginator->prev('<', array('title'=>'prev','class'=>'disabled pagination1'));?>
									<?php echo $this->Paginator->numbers(array('class'=>'pagination1'));?>
									<?php echo $this->Paginator->next('>', array('title'=>'next','class'=>'disabled pagination1'));?>
									<?php echo $this->Paginator->last('>>', array('title'=>'last','class'=>'disabled pagination1'));?>
								</div>
							</form>
						</div>

						<div id="prodListSearch" style="display:none">
								<form onsubmit="return false;" name="prodListSearch">
									<table>
										<tr>
											<td>Find Products</td>
											<td>
												<?php echo $this->Form->input('Report.psearch'); ?>
											</td>
											<td><button class="admin_button" onClick="searchPrdts()">Search</button></td>
										</tr>
									</table>
								</form>
							   <div id="prods" style="margin:0;"></div>
						</div>
	        </div>
   </div>
</div>

	
