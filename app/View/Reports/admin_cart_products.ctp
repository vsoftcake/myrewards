<style type="text/css">
	.rounded-corners 
	{
		-moz-border-radius: 10px 10px 0px 0px; /* Firefox */
		-webkit-border-radius: 10px 10px 0px 0px; /* Safari, Chrome */
		border-radius: 10px 10px 0px 0px; /* CSS3 */
	}
	.tr 
	{
		height:35px;
	}
	.admin_button
	{
		height: 26px;
		padding-bottom: 2px;
		border-radius: 5px 5px 5px 5px;
		background-color: #0066cc;
		border: 1px solid #cccccc;
		color: #FFFFFF; 
		cursor: pointer;
		font-size:13px;
	}
</style>
<div style="width:953px;border-left:1px solid #cccccc;border-right:1px solid #cccccc;border-bottom:1px solid #cccccc;margin-left:-14px;">
	<div style="width:949px;height:50px;">
		<div style=" height: 40px; float:left; width: 608px;padding-left:37px;">
	       	<div style=" height: 40px; float:left;padding-top:5px;">
	        	<img title="Reports" alt="Reports" src="/files/admin_home_icons/reports.png" style="width:40px;height:40px;">
	        </div>
	        <div style=" height: 40px;float:left;padding-left:10px;">
	        	<h2>Export Cart Products</h2>
	        </div> 
		</div>
        <div class="actions" style=" height: 40px; float:left;  width: 300px;">
			<ul style="padding:20px;margin-left:-28px;">
				<li style="display:inline;"><?php echo $this->Form->button('Cancel', array('onclick' => "location.href='/admin/reports'",
				'class'=>'admin_button','style'=>'width:60px;', 'type'=>'button'));  ?></li>
	        </ul>
		</div>
	</div>
		
	<div>
		<p style="padding-left:40px;">Export cart products information.</p>
		<form action="<?php echo $this->Html->url('/admin/reports/cart_products_export/'); ?>" method="post">
			<div class="tabber" id="mytab1">
				<div class="tabbertab"  style="min-height:100px;overflow:hidden">
				  	<h4>Cart Products Report</h4>
			        <div style="background-color:#EFEFEF;border:1px solid #cccccc;width:750px;padding:30px;">
						<table>
							<tr class="tr">
								<td colspan="4"><h4>Product Type</h4></td>
							</tr>
							<!-- <tr class="tr">
								<td style="padding-right:2px"><?php echo $this->Form->checkbox('Report.shopping_cart');?></td>
								<td style="padding-right:35px"><strong>Shopping Cart</strong></td>
								<td style="padding-right:2px"><?php echo $this->Form->checkbox('Report.points');?></td>
								<td style="padding:2px"><strong>Points</strong></td>
							</tr> -->
							<tr class="tr">
								<td style="padding-right:2px">
									<?php echo $this->Form->input('Report.type', array('type' => 'radio','options' => array('cart'=>'Shopping Cart', 'points'=>'Points'),'class'=>'input_radio','separator'=>'<br/>','legend'=>false));?>
								</td>
							</tr>
						</table>
						<table>
							<tr class="tr">
								<td colspan="4"><h4>Status</h4></td>
							</tr>
							<tr class="tr">
								<td style="padding-right:2px"><?php echo $this->Form->checkbox('Report.pending');?></td>
								<td style="padding-right:35px"><strong>Pending</strong></td>
								<td style="padding-right:2px"><?php echo $this->Form->checkbox('Report.shipped');?></td>
								<td style="padding-right:35px"><strong>Shipped</strong></td>
								<td style="padding-right:2px"><?php echo $this->Form->checkbox('Report.completed');?></td>
								<td style="padding-right:2px"><strong>Completed</strong></td>
							</tr>
						</table>
						<table>
							<tr class="tr">
								<td colspan="2"><h4>Duration</h4></td>
							</tr>
							<tr class="tr">
								<td><strong>Order Date From</strong></td>
								<td class="br">
									<input type="checkbox" onclick="enableDate(this, 'ReportCreatedFrom', 'created_from_yearYear');"  class="checkbox" />
									<?php echo $this->Form->input('Report.created_from', array('disabled'=>'disabled', 'dateFormat'=>'DM', 'type'=>'date', 'div'=>false, 'label'=>false, 'selected'=>$start_default)); ?> 
									<?php echo $this->Form->year('created_from_year', date("Y")-5, date("Y"), array('default'=>"$start_year", 'div'=>false, 'label'=>false, 'disabled' => 'disabled'));?>
								</td>
							</tr>
							<tr class="tr">
								<td><strong>Order Date To</strong></td>
								<td class="br">
									<input type="checkbox" onclick="enableDate(this, 'ReportCreatedTo', 'created_to_yearYear');"  class="checkbox" />
									<?php echo $this->Form->input('Report.created_to', array('disabled'=>'disabled', 'dateFormat'=>'DM', 'type'=>'date', 'div'=>false, 'label'=>false, 'selected'=>$end_default)); ?>
									<?php echo $this->Form->year('created_to_year', date("Y")-5, date("Y"), array('default'=>"$end_year", 'div'=>false, 'label'=>false, 'disabled' => 'disabled')); ?>
								</td>
							</tr>
						</table>
					</div>
					<br/>
			       	<div align="center" style="padding-top:10px">
						<button type="submit" class="admin_button">Generate Report</button>
			       	</div>  
			    </div>    
			</div>
		</form>
	</div>
</div>

<script type="text/javascript" src="<?php echo $this->Html->url('/js/reports.js'); ?>"></script>
<script type="text/javascript">

function enableDate(checkbox, name1, name2) {
	$(name1 + 'Day').disabled = !checkbox.checked;
	$(name1 + 'Month').disabled = !checkbox.checked;
	$(name2).disabled = !checkbox.checked;
}

</script>



<!--date enable script-->

 <script type="text/javascript">
 $(document).ready(function(){

  $('.checkbox').click(function(){
    
  if($(this).is(':checked'))
   
   $(this).parent('td').find('select').removeAttr('disabled');  
    
    else
 $(this).parent('td').find('select').attr('disabled','disabled');  
  
  });      

});
 </script>