<?php

if (isset($reports[0])) {

	$line = array();
	foreach ($reports[0]['Export'] as $key => $value) {
		$line[] = $key;
	}
	$this->Csv->addRow($line);
	
	foreach ($reports as $key => $value) {
		$line = $value['Export'];
		$this->Csv->addRow($line);
	}
} else {
	$this->Csv->addRow(array('No products found'));
}
echo $this->Csv->render('product_views.csv');
//echo $this->Csv->render(false);

?>
