<style>
#content
{
border-left:1px solid #cccccc;
border-right:1px solid #cccccc;
border-bottom:1px solid #cccccc;
margin-left:20px !important;
padding-left:18px !important;
width:913px;
}

</style>

<table style="margin-left:-2px;margin-top:10px;">
	<tr>
		<td>
            &nbsp;&nbsp;<img style="width:40px;height:40px;" src="/files/admin_home_icons/reports.png"  alt="Clients" title="Clients" />&nbsp;&nbsp;
        </td>
        <td> 
            <h2><?php echo "Reports";?></h2>
        </td>
    </tr>        
</table>

<div class="list">
	<table cellpadding="0" cellspacing="0">
		<tr>
	    	<td valign="top" style="padding:20px">
	        	<table width="400">
		            <tr height="30">
		                <th style="width:240px;background-color:#cccccc;text-align:left;padding-left:5px;border-left:1px solid #969090;border-bottom:1px solid #969090;border-top:1px solid #969090;">Name</th>
		                <th style="width:60px;background-color:#cccccc;padding-left:14px;border-right:1px solid #969090;border-bottom:1px solid #969090;border-top:1px solid #969090;">Action</th>
		            </tr>
					<tr>
						<td>
							User actions per client
						</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href = "<?php echo $this->Html->url("user_actions_pc")?>" >Enter</a></td>
					</tr>
					<tr>
						<td>
							User actions
						</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href = "<?php echo $this->Html->url("user_actions")?>" >Enter</a></td>
					</tr>
					<tr>
						<td>
							Users per client
						</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href = "<?php echo $this->Html->url("users_per_client")?>" >Enter</a></td>
					</tr>
					<tr>
						<td>
							Product views by catgeory
						</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href = "<?php echo $this->Html->url("product_views")?>" >Enter</a></td>
					</tr>
					<tr>
						<td>
							Export users
						</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href = "<?php echo $this->Html->url("export_users")?>" >Enter</a></td>
					</tr>
					<tr>
						<td>
							Export registered users
						</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href = "<?php echo $this->Html->url("export_users/registered")?>" >Enter</a></td>
					</tr>
					<tr>
						<td>
							Export un-registered users
						</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href = "<?php echo $this->Html->url("export_users/unregistered")?>" >Enter</a></td>
					</tr>
					<tr>
						<td>
							Export users with prefix
						</td>
					
						<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href = "<?php echo $this->Html->url("export_users_with_prefix")?>" >Enter</a></td>
					</tr>
					<tr>
						<td>
							Export products
						</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href = "<?php echo $this->Html->url("products")?>" >Enter</a></td>
					</tr>
					<tr>
						<td>
							Product availability matrix
						</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href = "<?php echo $this->Html->url("product_availability")?>" >Enter</a></td>
					</tr>
					<tr>
						<td>
							Export merchants
						</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href = "<?php echo $this->Html->url("merchants")?>" >Enter</a></td>
					</tr>
					<tr>
						<td>
							Export merchant addresses
						</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href = "<?php echo $this->Html->url("merchant_addresses")?>" >Enter</a></td>
					</tr>
				</table>
	   		</td>
	   		<td valign="top" style="padding:20px">
		       	<table width="400">
		            <tr height="30">
		                <th style="width:240px;background-color:#cccccc;text-align:left;padding-left:5px;border-left:1px solid #969090;border-bottom:1px solid #969090;border-top:1px solid #969090;">Name</th>
		                <th style="width:60px;background-color:#cccccc;padding-left:14px;border-right:1px solid #969090;border-bottom:1px solid #969090;border-top:1px solid #969090;">Action</th>
		            </tr>
					<tr>
						<td>
							Logged on Users
						</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href = "<?php echo $this->Html->url("logged_on_users")?>" >Enter</a></td>
					</tr>
					
					<tr>
						<td>
							Client Referrer Login Statistics
						</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href = "<?php echo $this->Html->url("user_statistics")?>" >Enter</a></td>
					</tr>
					<tr>
						<td>
							Product hits by offer
						</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href = "<?php echo $this->Html->url("offer_report")?>" >Enter</a></td>
					</tr>
					<tr>
						<td>
							Products by clients
						</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href = "<?php echo $this->Html->url("client_products")?>" >Enter</a></td>
					</tr>
					<tr>
						<td>
							Users with no activity
						</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href = "<?php echo $this->Html->url("clients_users_not_logged")?>" >Enter</a></td>
					</tr>
					<tr>
						<td>
							Multiple Logins By Members
						</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href = "<?php echo $this->Html->url("multiple_logins_by_member")?>" >Enter</a></td>
					</tr>
					<tr>
						<td>
							Actions By Membership Number
						</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href = "<?php echo $this->Html->url("actions_by_membership_number")?>" >Enter</a></td>
					</tr>
					<tr>
			           <td>
			                   Login Info By Membership Number
			           </td>
			           <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href = "<?php echo $this->Html->url("login_info_by_membership_number")?>" >Enter</a></td>
				   </tr>
				   <tr>
			           <td>
			                  Users Bar graph
			           </td>
			           <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href = "<?php echo $this->Html->url("user_graph")?>" >Enter</a></td>
				   </tr>
				   <tr>
						<td>
							Product Views By Client
						</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href = "<?php echo $this->Html->url("product_views_clients")?>" >Enter</a></td>
					</tr>
					<tr>
						<td>
							Export Fist Time Login Members
						</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href = "<?php echo $this->Html->url("export_first_time_login_members")?>" >Enter</a></td>
					</tr>
					<tr>
						<td>
							Cart Products
						</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href = "<?php echo $this->Html->url("cart_products")?>" >Enter</a></td>
					</tr>
					<tr>
						<td>
							Advt4Free Merchants
						</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href = "<?php echo $this->Html->url("advt4free_merchant")?>" >Enter</a></td>
					</tr>
				</table>
	    	</td>
	  	</tr>
	</table>
</div>