<h2>Un-registered users per client</h2>
<div class="actions">
	<ul>
		<li><?php echo $this->Html->link('Cancel', '/admin/reports'); ?></li>
	</ul>
</div>
<div class="edit">
<form action="<?php echo $this->Html->url('/admin/reports/registered_user_per_client_export/'. $registered); ?>" method="post" enctype="multipart/form-data">
	<table>
		<tr>
			<td class="bt">Programs</td>
			<td class="br bt"><?php echo $this->Form->input('Report.program', array('multiple'=>'multiple', 'options'=>$programs, 'div'=>false, 'label'=>false)); ?></td>
			<td class="bl"></td>
		</tr>
		<tr>
			<td>Clients</td>
			<td class="br"><?php echo $this->Form->input('Report.client', array('multiple'=>'multiple', 'options'=>$clients, 'div'=>false, 'label'=>false)); ?></td>
		</tr>
		<tr>
		
			<td class="bb"></td>
			<td class="bb br"><button type="submit">Generate Report</button></td>
		</tr>
	</table>
	</form>
</div>