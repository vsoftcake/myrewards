<style type="text/css">
	.rounded-corners 
	{
		-moz-border-radius: 10px 10px 0px 0px; /* Firefox */
		-webkit-border-radius: 10px 10px 0px 0px; /* Safari, Chrome */
		border-radius: 10px 10px 0px 0px; /* CSS3 */
	}
	.tr 
	{
		height:35px;
	}
	.admin_button
	{
		height: 26px;
		padding-bottom: 2px;
		border-radius: 5px 5px 5px 5px;
		background-color: #0066cc;
		border: 1px solid #cccccc;
		color: #FFFFFF; 
		cursor: pointer;
		font-size:13px;
	}
</style>
<script>
	function loadAdminApproval(check)
	{
		if(check.value=='yes')
		{
			 $('admin_approve').show();
		}
		else
		{
			$('admin_approve').hide();
		}
	}
</script>
<div style="width:953px;border-left:1px solid #cccccc;border-right:1px solid #cccccc;border-bottom:1px solid #cccccc;margin-left:-14px;">
	<div style="width:949px;height:50px;">
		<div style=" height: 40px; float:left; width: 608px;padding-left:37px;">
	       	<div style=" height: 40px; float:left;padding-top:5px;">
	        	<img title="Reports" alt="Reports" src="/files/admin_home_icons/reports.png" style="width:40px;height:40px;">
	        </div>
	        <div style=" height: 40px;float:left;padding-left:10px;">
	        	<h2>Export Advt4Free Merchants</h2>
	        </div> 
		</div>
        <div class="actions" style=" height: 40px; float:left;  width: 300px;">
			<ul style="padding:20px;margin-left:-28px;">
				<li style="display:inline;"><?php echo $this->Form->button('Cancel', array('onclick' => "location.href='/admin/reports'",
				'class'=>'admin_button','style'=>'width:60px;', 'type'=>'button'));  ?></li>
	        </ul>
		</div>
	</div>
		
	<div>
		<p style="padding-left:40px;">Export advt4free merchants information.</p>
		<form action="<?php echo $this->Html->url('/admin/reports/advt4free_merchant_export/'); ?>" method="post">
			<div class="tabber" id="mytab1">
				<div class="tabbertab"  style="min-height:100px;overflow:hidden">
				  	<h4>Advt4Free Merchants Report</h4>
			        <div style="background-color:#EFEFEF;border:1px solid #cccccc;width:750px;padding:30px;">
						<table>
							<tr class="tr">
								<td colspan="4"><h4>Merchant Approval Type</h4></td>
							</tr>
							<tr class="tr">
								<td colspan="4">
									<?php echo $this->Form->input('Report.merchant_approve', array('type' => 'radio','options' => array('yes'=>'Merchant Approved', 'no'=>'Merchant Not Approved'),'class'=>'input_radio','separator'=>'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;','legend'=>false,'onclick'=>'loadAdminApproval(this)'));?>
								</td>
								<!-- <td style="padding-right:2px"><?php echo $this->Form->checkbox('Report.merchant_approve');?></td>
								<td style="padding-right:35px"><strong>Merchant Approved</strong></td>
								<td style="padding-right:2px"><?php echo $this->Form->checkbox('Report.merchant_not_approve');?></td>
								<td style="padding:2px"><strong>Merchant Not Approved</strong></td> -->
							</tr>
							<tr id="admin_approve" style="display:none">
								<td colspan="4">
									<table>
										<tr class="tr">
											<td colspan="4"><h4>Admin Approval Type</h4></td>
										</tr>
										<tr class="tr">
											<td style="padding-right:2px"><?php echo $this->Form->checkbox('Report.admin_approve');?></td>
											<td style="padding-right:35px">Admin Approved</td>
											<td style="padding-right:2px"><?php echo $this->Form->checkbox('Report.admin_not_approve');?></td>
											<td style="padding:2px">Admin Not Approved</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</div>
					<br/>
			       	<div align="center" style="padding-top:10px">
						<button type="submit" class="admin_button">Generate Report</button>
			       	</div>  
			    </div>    
			</div>
		</form>
	</div>
</div>

<script type="text/javascript" src="<?php echo $this->Html->url('/js/reports.js'); ?>"></script>
