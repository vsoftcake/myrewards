<?php

if (isset($reports[0])) {

	$line = array();
	foreach ($reports[0]['Export'] as $key => $value) {
		$line[] = $key;
	}
	$this->Csv->addRow($line);
	
	foreach ($reports as $key => $value) {
		$line = $value['Export'];
		$this->Csv->addRow($line);
	}
} else {
	$this->Csv->addRow(array('No users found'));
}
if($registered ==1)
	echo $this->Csv->render('registered_user_per_client.csv');
else
	echo $this->Csv->render('unregistered_user_per_client.csv');

//echo $this->Csv->render(false);

?>
