<style type="text/css">

	.rounded-corners 
	{
		-moz-border-radius: 10px 10px 0px 0px; /* Firefox */
		-webkit-border-radius: 10px 10px 0px 0px; /* Safari, Chrome */
		border-radius: 10px 10px 0px 0px; /* CSS3 */
	}
	.tr 
	{
		height:35px;
	}
	.admin_button
	{
		height: 26px;
		padding-bottom: 2px;
		border-radius: 5px 5px 5px 5px;
		background-color: #0066cc;
		border: 1px solid #cccccc;
		color: #FFFFFF; 
		cursor: pointer;
		font-size:13px;
	}
</style>

<div style="width:953px;border-left:1px solid #cccccc;border-right:1px solid #cccccc;border-bottom:1px solid #cccccc;margin-left:-14px;">
	<div style="width:949px;height:50px;">
		<div style=" height: 40px; float:left; width: 608px;padding-left:37px;">
	       	<div style=" height: 40px; float:left;padding-top:5px;">
	        	<img title="Reports" alt="Reports" src="/files/admin_home_icons/reports.png" style="width:40px;height:40px;">
	        </div>
	        <div style="height: 40px;float:left;padding-left:10px;">
	        	<h2>Users with no activity</h2>
	        </div> 
		</div>
        <div class="actions" style=" height: 40px; float:left;  width: 300px;">
			<ul style="padding:20px;margin-left:-28px;">
				<li style="display:inline;"><?php echo $this->Form->button('Cancel', array('onclick' => "location.href='/admin/reports'",
				'class'=>'admin_button','style'=>'width:60px;', 'type'=>'button'));  ?></li>
	        </ul>
		</div>
	</div>
	
	<div>
		<form action="<?php echo $this->Html->url('/admin/reports/clients_users_not_logged_export/'); ?>" method="post" enctype="multipart/form-data">
			<div class="tabber" id="mytab1">
			
				<div class="tabbertab"  style="min-height:100px;overflow:hidden">
  					<h2>Programs, Clients And Prefix</h2>
			    	<h4>Programs, Clients And Prefix</h4>
		      		<div style="background-color:#EFEFEF;border:1px solid #cccccc;width:750px;padding:30px;">
						<table>
							<tr class="tr">
								<td style="width:100px">
									<strong>Programs</strong>
								</td>
								<td>
									Click a program to select the program's clients Multiple programs can be selected
								</td>
							</tr>
							<tr class="tr">
								<td colspan="2">
									<?php $count=0;?>
									<div style="float:left;width:200px">
										<ul>
											<?php foreach ($programs as $program_id => $program_name) : ?>
												<?php $count++;
												if($count==15):
											?>
										</ul>
									</div>
									<div style="float:left;width:200px">
										<ul>
									<?php endif; ?>  
										<li><a href="javascript:void(0)" onclick="selectClients('<?php echo $program_id; ?>'); return false;"><?php echo $program_name; ?></a></li>
										<?php endforeach; ?>
										<li><a href="javascript:void(0)" onclick="selectAllClients(); return false;">All Clients</a></li>
									</ul>
								</td>
							</tr>
							<tr class="tr">
								<td  colspan="2"><strong>Clients</strong></td>
							</tr>
							<tr class="tr">
								<td colspan="2"><?php echo $this->Form->input('Report.client', array('multiple'=>'multiple', 'options'=>$clients, 'div'=>false, 'label'=>false)); ?></td>
							</tr>
							<tr class="tr">
								<td><strong>Prefix</strong></td>
								<td class="br"><?php echo $this->Form->input('Report.Prefix', array('div'=>false, 'label'=>false));?></td>
							</tr>
						</table>
			  		</div>
			  		<br/>
			       	<div align="center" style="padding-top:10px">
						<button type="submit" class="admin_button">Generate Report</button>
			       	</div>  
			  	</div>
			  	
			  	
			</div>
		</form>
	</div>
</div>

<script type="text/javascript" src="<?php echo $this->Html->url('/js/reports.js'); ?>"></script>
<script type="text/javascript">

var programs = new Array();
var selected_programs = new Array();
<?php foreach ($programs_clients as $key => $clients) : ?>
programs[<?php echo $key ; ?>] = new Array();
	<?php foreach ($clients as $client_id => $client_name) : ?>
	programs[<?php echo $key ; ?>].push(<?php echo $client_id; ?>);
	<?php endforeach; ?>
selected_programs[<?php echo $key ; ?>] = 0;
<?php endforeach; ?>
var all_clients = 0;

</script>