<?php $this->Paginator->options(array('update' => '#users','evalScripts' => true,'url' => array_merge($this->params['pass'], $this->params['named'])));

?>

<form name="clientUserExport" method="post">
<div class="list">
<table>
	<tr>
		<td>
			<table cellpadding="0" cellspacing="0" width="400">
				<tr>
					<th>&nbsp;</th>
					<th style="text-align:left">User Id</th>
					<th style="text-align:left">User Name</th>
					<th style="text-align:left">User Client Id</th>
				</tr>
				<?php foreach($users_list as $user): ?>
					<tr>
						<td>
							<?php echo $this->Form->checkbox('Report.check',array('value'=>$user['user']['id'].'|'.$user['user']['client_id'])); ?>
						</td>
						<td>
							<?php echo $user['user']['id'] ;?>
						</td>
						<td>
							<?php echo $user['user']['username'] ;?>
						</td>
						<td>
							<?php echo $user['user']['client_id'] ;?>
						</td>
					</tr>
				<?php endforeach; ?>
			</table>
		</td>
	</tr>
</table>
</div>
<div>
<div class="paging">
		<?php echo $this->Paginator->first('<<', array('title'=>'first','class'=>'disabled pagination1'));?>
		<?php echo $this->Paginator->prev('<', array('title'=>'prev','class'=>'disabled pagination1'));?>
		<?php echo $this->Paginator->numbers(array('class'=>'pagination1'));?>
		<?php echo $this->Paginator->next('>', array('title'=>'next','class'=>'disabled pagination1'));?>
		<?php echo $this->Paginator->last('>>', array('title'=>'last','class'=>'disabled pagination1'));?>
	
		<p><?php echo $this->Paginator->counter(array('format' => __('Page %page% of %pages%', true)));	echo $this->Js->writeBuffer();?></p>
	</div>
</div>
<div>
		    <button class="admin_button" type="button" onclick="addUsers()">Generate Report</button> 
		
</div>
</form>