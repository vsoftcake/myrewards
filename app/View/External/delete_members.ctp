<?php if (isset($return)) : ?>
<?php echo $return; ?>
<?php else : ?>
<h1>Delete member</h1>
<p>
	Use this form to delete members, it can also be sent from a external server as a form post.<br/>
	If sending from a external server, look at the source of this page to see the formatting for the form fields. an unlimited number of users can be deleted, but it would be better to send it in batches of less than 2000
</p>
<p>
	If the users are successfully deleted, the word 'success' will be returned, otherwise an error message will be displayed.
</p>
<form action="<?php echo $this->Html->url('/external/delete_members'); ?>" method="POST">
<table>
	<tr>
		<td>access_username</td>
		<td><input type="text" name="access_username" /></td>
		<td>required</td>
	</tr>
	<tr>
		<td>access_password</td>
		<td><input type="text" name="access_password" /></td>
		<td>required</td>
	</tr>
	<tr>
		<td>username 1</td>
		<td><input type="text" name="username[]" /></td>
	</tr>
	<tr>
		<td>username 2</td>
		<td><input type="text" name="username[]" /></td>
	</tr>
	<tr>
		<td>username 3</td>
		<td><input type="text" name="username[]" /></td>
	</tr>
	<tr>
		<td><input type="submit" /></td>
	</tr>
</table>
</form>
<?php endif; ?>