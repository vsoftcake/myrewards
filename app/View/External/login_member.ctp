<?php if (isset($return)) : ?>
<?php echo $return; ?>
<?php else : ?>
<h1>Login member</h1>
<p>
	Use this form to login a member, it can also be sent from a external server as a form post.<br/>
	If sending from a external server, look at the source of this page to see the formatting for the form fields.<br/>
	The user will be able to automaticlly login if using the returned url.
</p>
<p>
	If the user is successfull added or updated, a url will be returned to login the user, otherwise an error message will be displayed.
</p>
<form action="<?php echo $this->Html->url('/external/login_member'); ?>" method="POST">
<table>
	<tr>
		<td>access_username</td>
		<td><input type="text" name="access_username" /></td>
		<td>required</td>
	</tr>
	<tr>
		<td>access_password</td>
		<td><input type="text" name="access_password" /></td>
		<td>required</td>
	</tr>
	<tr>
		<td>username</td>
		<td><input type="text" name="username" /></td>
		<td>required</td>
	</tr>
	<tr>
		<td><input type="submit" /></td>
	</tr>
</table>
</form>
<?php endif; ?>