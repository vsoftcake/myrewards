<?php if (isset($return)) : ?>
<?php echo $return; ?>
<?php else : ?>
<h1>Add member</h1>
<p>
	Use this form to add or update members, it can also be sent from a external server as a form post.<br/>
	If sending from a external server, look at the source of this page to see the formatting for the form fields.<br/>
	The user will be able to automaticlly login if using the returned url.
</p>
<p>
	If the user is successfull added or updated, a url will be returned to login the user, otherwise an error message will be displayed.
</p>
<form action="<?php echo $this->Html->url('/external/add_member'); ?>" method="POST">
<table>
	<tr>
		<td>access_username</td>
		<td><input type="text" name="access_username" /></td>
		<td>required</td>
	</tr>
	<tr>
		<td>access_password</td>
		<td><input type="text" name="access_password" /></td>
		<td>required</td>
	</tr>
	<tr>
		<td>username</td>
		<td><input type="text" name="username" /></td>
		<td>required</td>
	</tr>
	<tr>
		<td>password</td>
		<td><input type="text" name="password" /></td>
	</tr>
	<tr>
		<td>firstname</td>
		<td><input type="text" name="firstname" /></td>
		<td>required</td>
	</tr>
	<tr>
		<td>lastname</td>
		<td><input type="text" name="lastname" /></td>
		<td>required</td>
	</tr>
	<tr>
		<td>email</td>
		<td><input type="text" name="email" /></td>
	</tr>
	<tr>
		<td>state</td>
		<td>
			<select name="state">
				<option value=""></option>
				<option value="ACT">ACT</option>
				<option value="NSW">NSW</option>
				<option value="NT">NT</option>
				<option value="QLD">QLD</option>
				<option value="SA">SA</option>
				<option value="TAS">TAS</option>
				<option value="VIC">VIC</option>
				<option value="WA">WA</option>
			</select>
		</td>
	</tr>
	<tr>
		<td>newsletter</td>
		<td>
			<select name="newsletter">
				<option value="0">0</option>
				<option value="1">1</option>
			</select>
		</td>
	</tr>
	<tr>
		<td><input type="submit" /></td>
	</tr>
</table>
</form>
<?php endif; ?>