<?php $this->Paginator->options(
		array(
			'update' => 'Log',
			'url' => $this->params['pass'],
			'model' => 'Log'
		));
			
	?>
	<div class="list">
	<table cellpadding="0" cellspacing="0" style="width: 1px;">
		<tbody id="log_details">
		<tr>
			<th style="width:1%">Date</th>
			<th style="width:1%">User</th>
			<th>Description</th>
			<th></th>
		</tr>
		<?php
		$i = 0;
		
		foreach ($logs as $log):
			$class = ' class="altrow"';
			if ($i++ % 2 == 1) {
				$class = null;
			}
		?>
			<tr<?php echo $class;?>>
				<td style="white-space:nowrap;">
					<?php echo date('D, M j Y, g:ia', strtotime($log['Log']['created'])); ?>
				</td>
				<td style="white-space:nowrap;">
					<?php echo $log['User']['first_name']. ' '. $log['User']['last_name']; ?>
				</td>
				<td style="white-space:nowrap;"><?php echo $log['Log']['description'] ?></td>
				<td style="white-space:nowrap;">
					<a href="#" onclick="return displayLogNotes('<?php echo $log['Log']['id'] ?>');">details</a>
				</td>
			</tr>
			<tr<?php echo $class;?> id="Log_<?php echo $log['Log']['id']; ?>" style="display:none">
				<td colspan="4">
					Notes:<br/>
					<?php echo str_replace("\n", '<br/>', $log['Log']['notes']); ?>
					<div id="LogNotes_<?php echo $log['Log']['id']; ?>">Retrieving changes...</div>
				</td>
			</tr>
		<?php endforeach; ?>
		</tbody>
	</table>
	<div class="paging">
		<?php echo $this->Paginator->prev('<< '.__('previous', true), array(), null, array('class'=>'disabled'));?>
		<?php echo $this->Paginator->numbers();?>
		<?php echo $this->Paginator->next(__('next', true).' >>', array(), null, array('class'=>'disabled'));?>
	</div>
	<p><?php echo $this->Paginator->counter(array('format' => __('Page %page% of %pages%', true)));	?></p>
</div>
<script type="text/javascript">

displayLogNotes = function(id) {
	
	if ($('Log_' + id).style.display == 'none') {
		$('Log_' + id).style.display = '';
	} else {
		$('Log_' + id).style.display = 'none';
	}
	
	if ($('LogNotes_' + id).innerHTML == 'Retrieving changes...') {
		new Ajax.Updater('LogNotes_' + id, '<?php echo $this->Html->url('/admin/logs/ajax_changes/'); ?>/' + id, {evalScripts: true });
	}
	
	return false;
}

</script>