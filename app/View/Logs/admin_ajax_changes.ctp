<table cellpadding="0" cellspacing="0" style="width: 100%;">
	<tbody>
		<tr>
			<th style="text-align:left;">Field</th>
			<th style="text-align:left;">Old</th>
			<th style="text-align:left;">New</th>
		</tr>
	<?php
	$i = 0;
	
	foreach ($log_changes as $change):
		$class = ' class="altrow"';
		if ($i++ % 2 == 1) {
			$class = null;
		}
	?>
		<tr<?php echo $class;?>>
			<td>
				<?php echo Inflector::humanize($change['LogChange']['key']); ?>
			</td>
			<td>
				<?php echo $change['LogChange']['old_value']; ?>
			</td>
			<td>
				<?php echo $change['LogChange']['new_value']; ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</tbody>
</table>