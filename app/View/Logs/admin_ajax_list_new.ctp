<?php $this->Paginator->options(
		array(
			'update' => '#Log',
			'evalScripts' => true,
			'url' => $this->params['pass'],
			'model' => 'Log'
		));
			
	?>
	<div class="list">
	<table cellpadding="0" cellspacing="0" style="width: 1px;">
		<tbody id="log_details">
		<tr>
			<th style="width:1%;text-align:left;padding: 0.59em 3.9em 0.59em 0.9em !important;background-color:#cccccc;border-left:1px solid #969090;border-bottom:1px solid #969090;border-top:1px solid #969090;">Date</th>
			<th style="width:1%;text-align:left; padding: 0.59em 3.9em !important;background-color:#cccccc;border-bottom:1px solid #969090;border-top:1px solid #969090;">User</th>
			<th style="text-align:left; padding: 0.59em 3.9em !important;background-color:#cccccc;border-bottom:1px solid #969090;border-top:1px solid #969090;">Description</th>
			<th style="border-right:1px solid #969090;border-bottom:1px solid #969090;border-top:1px solid #969090;background-color:#cccccc;"></th>
		</tr>
		<?php
		$i = 0;
		
		foreach ($logs as $log):
			$class = ' class="altrow"';
			if ($i++ % 2 == 1) {
				$class = null;
			}
		?>
			<tr<?php echo $class;?>>
				<td style="white-space:nowrap;padding: 0.29em 3.9em 0.29em 0.9em !important;">
					<?php echo date('D, M j Y, g:ia', strtotime($log['Log']['created'])); ?>
				</td>
				<td style="white-space:nowrap;padding: 0.29em 3.9em !important;">
					<?php echo $log['User']['first_name']. ' '. $log['User']['last_name']; ?>
				</td>
				<td style="white-space:nowrap;padding: 0.29em 3.9em !important;"><?php echo $log['Log']['description'] ?></td>
				<td style="white-space:nowrap;padding: 0.29em 0.9em 0.29em 9.9em !important;">
					<a href="javascript:void(0)" onclick="return displayLogNotes('<?php echo $log['Log']['id'] ?>');">details</a>
				</td>
			</tr>
			<tr<?php echo $class;?> id="Log_<?php echo $log['Log']['id']; ?>" style="display:none">
				<td colspan="4">
					Notes:<br/>
					<?php echo str_replace("\n", '<br/>', $log['Log']['notes']); ?>
					<div id="LogNotes_<?php echo $log['Log']['id']; ?>">Retrieving changes...</div>
				</td>
			</tr>
		<?php endforeach; ?>
		</tbody>
	</table>
	<div class="paging">
		<?php echo $this->Paginator->prev('<< '.__('previous', true), array(), null, array('class'=>'disabled'));?>
		<?php echo $this->Paginator->numbers();?>
		<?php echo $this->Paginator->next(__('next', true).' >>', array(), null, array('class'=>'disabled'));?>
	</div>
	<p><?php echo $this->Paginator->counter(array('format' => __('Page %page% of %pages%', true)));	echo $this->Js->writeBuffer();?></p>
</div>
<script type="text/javascript">
function displayLogNotes(id) {
	
	$('#Log_' + id).toggle();
	
	if ($('#LogNotes_' + id).html() == 'Retrieving changes...') {
		$.ajax({
			url: "/admin/logs/ajax_changes/"+id,
			type: "GET",
			success: function (data) {
				$('#LogNotes_'+id).html(data);
			}
		});
	}
	return false;
}

</script>