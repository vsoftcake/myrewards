<style>
	#content
	{
	border-left:1px solid #cccccc;
	border-right:1px solid #cccccc;
	border-bottom:1px solid #cccccc;
	margin-left:20px !important;
	padding-left:15px !important;
	width:916px;
	}
</style>
<h1>Preview Newsletter</h1>
<br/>
<script type="text/javascript">
function updateClient() {
	var client_id = $('NewslettersClientClientId').options[$('NewslettersClientClientId').selectedIndex].value;
	if(document.getElementById('UserState')!=null)
	{
		var state = $('UserState').options[$('UserState').selectedIndex].value;
	}
	else
	{
		var state = '';
	}
	
	$('preview_frame').src = '<?php echo $this->Html->url('/admin/newsletters/newsletter/'. $newsletter['Newsletter']['id']. '/'); ?>' + client_id + '<?php echo  '/'. $newsletter['Newsletter']['dashboard_id']?>/' + state;
	
}

/*
 * Send a test email to the email address in test_email
*/
function sendTest() {

	var email = document.getElementById('test_email').value;
	var client_id = $('#NewslettersClientClientId').val();
	if(document.getElementById('UserState')!=null)
	{
		var state = $('#UserState').val();
	}
	else
	{
		var state = '';
	}
	var newsletter_id = <?php echo $newsletter['Newsletter']['id']; ?>;

	$('#sending').show();
	$('#error').hide();
	$('#sent').hide();
	$('#test_newsletter_button').attr("disabled", true);

	$.ajax({
		url: '/admin/newsletters/send_test/<?php echo $from_email ?>/' + email + '/' + newsletter_id + '/' + client_id +'/<?php echo $newsletter['Newsletter']['dashboard_id']?>/'+ state,
		type: "GET",
		success: function (data) {
			email_sent(data);
		}
	});
	return false;
}

function email_sent(data) {

	if (data == 'sent') {
		$('#sending').hide();
		$('#sent').show();
	} else {
		$('#sending').hide();
		$('#error').show();
	}

	$('#test_newsletter_button').attr("disabled", false);

}

function sendMail()
{
	document.getElementById('process').style.display="inline";
}

</script>
<form action="<?php echo $this->Html->url('/admin/newsletters/preview/'. $newsletter['Newsletter']['id']); ?>" method="post">
<table class="newsletter_table">
	<tr>
		<th>Client</th>
		<td><?php echo $this->Form->input('NewslettersClient.client_id', array('div'=>false, 'label'=>false, 'type'=>'select', 'options'=>$clients, 'onchange' => 'updateClient();')); ?></td>
	</tr>
	<?php if(isset($countries)) : ?>
	<tr>
		<th>Country</th>
		<td><?php echo $this->Form->input('User.country', array('div'=>false, 'label'=>false, 'type'=>'select', 'options'=>$countries)); ?></td>
	</tr>
	<?php endif; ?>
	<?php if(isset($states)) : ?>
	<tr>
		<th>State</th>
		<td><?php echo $this->Form->input('User.state', array('div'=>false, 'label'=>false, 'type'=>'select', 'options'=>$states, 'onchange' => 'updateClient();')); ?></td>
	</tr>
	<?php endif; ?>
	<tr>
		<th>Subject&nbsp;&nbsp;&nbsp;&nbsp;</th>
		<td><?php echo $newsletter['Newsletter']['subject']; ?></td>
	</tr>
	<tr>
		<th></th>
		<td>
			<!--<iframe src="<?php echo $this->Html->url('/admin/newsletters/newsletter/'. $newsletter['Newsletter']['id']. '/'. $first_client. '/'. $first_state); ?>" id="preview_frame" style="width:840px;height:500px;border: 1px solid black;padding: 2px;"></iframe>-->
			<iframe src="<?php echo $this->Html->url('/admin/newsletters/newsletter/'. $newsletter['Newsletter']['id'].'/'. $first_client.'/'.$newsletter['Newsletter']['dashboard_id'].'/'. $first_state); ?>" id="preview_frame" style="width:782px;height:500px;border: 1px solid black;padding: 2px;"></iframe>
		</td>
	</tr>
	<tr>
		<th></th>
		<td>
			<input type="text" value="<?php echo $this->Session->read('user.User.email'); ?>" id="test_email" />
			<button type="button" onclick="return sendTest();" id="test_newsletter_button">Send Test</button>
			<span id="sending" style="display:none;">sending</span>
			<span id="sent" style="display:none;">Test sent, check your email</span>
			<span id="error" style="display:none;">error</span>
		</td>
	<tr>
	<tr>
		<th></th>
		<td>
			<button type="submit" name="previous" >edit</button>&nbsp;&nbsp;&nbsp;
			<button type="submit" name="next" onclick="sendMail()">send</button>&nbsp;&nbsp;&nbsp;
			<button type="submit" name="cancel">cancel</button>
			<span id="process" style="display: none">
				<img style="position: relative; top:4px; " src="/files/newimg/ajax-loader.gif" alt="Working..." />&nbsp;&nbsp;Processing please wait....
			</span>
		</td>
	</tr>
</table>
</form>