<style type="text/css">	
	#content
	{
		border-left:1px solid #cccccc;
		border-right:1px solid #cccccc;
		border-bottom:1px solid #cccccc;
		margin-left:20px !important;
		padding-left:18px !important;
		width:913px;
	}
	.rounded-corners 
	{
		-moz-border-radius: 10px 10px 0px 0px; /* Firefox */
		-webkit-border-radius: 10px 10px 0px 0px; /* Safari, Chrome */
		border-radius: 10px 10px 0px 0px; /* CSS3 */
	}
	.tr 
	{
		height:35px;
	}
	.admin_button
	{
		height: 26px;
		padding-bottom: 3px;
		border-radius: 5px 5px 5px 5px;
		background-color: #0066cc;
		border: 1px solid #cccccc;
		color: #FFFFFF; 
		cursor: pointer;
		font-size:13px;
	}
	.newsletter1 
	{
		margin-right: 1em;
	}
	.newsletter1 input 
	{
		width: 100px;
	}
	.newsletter1 select 
	{
		width: 200px;
	}
	.newsletter1 table td 
	{
		padding: 2px;
	}
	.news_input1
	{
		width:185px !important;
	}
</style>
<?php echo $this->element('tinymce'); ?>  
<?php echo $this->Html->script('jscolor'); ?>

<form action="<?php echo $this->Html->url('/admin/newsletters/edit/'. $this->Html->value('Newsletter.id')); ?>" method="post"  enctype="multipart/form-data">
<div style="width:953px;margin-left:-14px;">
	<div id="message"></div>
		<div style="width:949px;height:35px;">
			<div style=" height: 40px; float:left; width: 608px;padding-left:37px;">
		       	<div style=" height: 40px; float:left;padding-top:5px;">
		        	<img title="Newsletters" alt="Newsletters" src="/files/admin_home_icons/e-news.png" style="width:40px;height:40px;">
		        </div>
		        <div style=" height: 40px;float:left;padding-left:10px;">
		        	<?php if($this->Html->value('Newsletter.id')>0) :?>
		        		<h2>Edit Newsletter</h2>
		        	<?php else : ?>
		        		<h2>Create Newsletter</h2>
		        	<?php endif; ?>
		        </div> 
			</div>
	        <div class="actions" style=" height: 40px; float:left;  width: 300px;">
				<ul style="padding:20px;margin-left:-28px;">
					<li style="display:inline;"><?php echo $this->Form->button('Cancel', array('onclick' => "location.href='/admin/newsletters/'",
					'class'=>'admin_button','style'=>'width:60px;', 'type'=>'button'));  ?></li>
					<li style="display:inline;">
						<button class="admin_button" type="submit"  onclick="saveNewsletter()">Save</button>
							<input type="hidden" name="save" id="save" />
					</li>
					<li style="display:inline;">
						<button type="submit" class="admin_button" name="send" id="NewsletterSend" style="cursor:pointer" onclick="previewNewsletter()">Preview</button>
						<input type="hidden" name="send" id="send" />
					</li>
		        </ul>
			</div>
		</div>
	
		<div class="newsletter1">
			<div class="tabber" id="mytab1" style="width:920px !important;padding-left:15px !important;">
				<div class="tabbertab"  style="min-height:615px;overflow:hidden;padding-left:20px !important;">
		  			<h2>Details</h2>
		    		<h4>Details</h4>
					<div style="background-color:#EFEFEF;border:1px solid #cccccc;padding:10px;width:855px;min-height:475px;overflow:hidden">
						<table>
							<tr class="tr">
								<td colspan="3"><strong>Disable?</strong></td>
							</tr>
							<?php if($this->Html->value('Newsletter.id')==null && !empty($newsletter_options)): ?>
								<tr class="tr">
									<td colspan="2"><strong>Copy from existing newsletters</strong></td>
									<td>
										<?php echo $this->Form->input('Newsletter.copy', array('div'=>false,'label'=>false, 'type'=>'select', 'options'=>$newsletter_options));?>
									</td>
								</tr>
								<tr class="tr">
									<td colspan="2"></td>
									<td><button class="admin_button" type="submit" name="copy">Select</button></td>
								</tr>
							<?php endif ; ?>
						</table>
						<table>
							<tr>
								<td style="width: 450px;">
									<table>
										<tr class="tr">
											<td colspan="2" style="width: 170px"><strong>Dashboard Type</strong></td>
											<td>
												<?php echo $this->Form->input('Newsletter.dashboard_id', array('div'=>false,'label'=>false, 'type'=>'select', 'options'=>$dashboard));?>
											</td>
										</tr>
										<tr class="tr">
											<td colspan="2" style="width: 170px"><strong>Name</strong></td>
											<td colspan="2">
												<?php
													echo $this->Form->input('Newsletter.name',array('class'=>'news_input1', 'div'=>false, 'label'=>false));
												?>
											<br/><?php echo $this->Form->error('Newsletter.name', array('required' => 'Name required'), array('required' => 'Name required')); ?></td>
										</tr>
										<tr class="tr">
											<td colspan="2"><strong>Page Title</strong></td>
											<td>
												<?php
													echo $this->Form->input('Newsletter.page_title',array('class'=>'news_input1', 'div'=>false, 'label'=>false));
												?>
											</td>
											<td></td>
										</tr>
									</table>
								</td>
								<td>
									<table>
										<tr class="tr">
											<td colspan="2" style="width: 120px;"><strong>Subject</strong></td>
											<td>
												<?php
													echo $this->Form->input('Newsletter.subject',array('class'=>'news_input1', 'div'=>false, 'label'=>false));
												?>
											</td>
											<td></td>
										</tr>
										<tr class="tr">
											<td colspan="2"><strong>Email Id</strong></td>
											<td>
												<?php
													echo $this->Form->input('Newsletter.email_id',array('class'=>'news_input1', 'div'=>false, 'label'=>false, 'type'=>'text'));
												?>
											</td>
											<td></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<table>
							<tr class="tr">
								<td><strong>Disable Date</strong></td>
								<td colspan="2"><?php echo $this->Form->checkbox('Newsletter.date_disabled'); ?></td>
								<td></td>
							</tr>							
							<tr class="tr">
								<td><strong>Heading 1</strong></td>
								<td><?php echo $this->Form->checkbox('Newsletter.h1_disabled', array('onclick' => "updateOverride(this, 'NewsletterH1')", 'div'=>false, 'label'=>false)); ?></td>
								<td>
									<?php
										($this->Form->value('Newsletter.h1_disabled') == 1)? $options = array('disabled' => 'disabled'): $options = array();
										echo $this->Form->input('Newsletter.h1',array('class'=>'news_input1', 'div'=>false, 'label'=>false)+$options);
									?>
								</td>
								<td></td>
							</tr>
							<tr class="tr">
								<td><strong>Heading 2</strong></td>
								<td><?php echo $this->Form->checkbox('Newsletter.h2_disabled', array('onclick' => "updateOverride(this, 'NewsletterH2')", 'div'=>false, 'label'=>false)); ?></td>
								<td>
									<?php
										($this->Form->value('Newsletter.h2_disabled') == 1)? $options = array('disabled' => 'disabled'): $options = array();
										echo $this->Form->input('Newsletter.h2', array('class'=>'news_input1', 'div'=>false, 'label'=>false)+$options);
									?>
								</td>
								<td></td>
							</tr>
							<tr class="tr">
								<td><strong>Heading 3</strong></td>
								<td><?php echo $this->Form->checkbox('Newsletter.h3_disabled', array('onclick' => "updateOverride(this, 'NewsletterH3')", 'div'=>false, 'label'=>false)); ?></td>
								<td>
									<?php
										($this->Form->value('Newsletter.h3_disabled') == 1)? $options = array('disabled' => 'disabled'): $options = array();
										echo $this->Form->input('Newsletter.h3', array('class'=>'news_input1', 'div'=>false, 'label'=>false)+$options);
									?>
								</td>
								<td></td>
							</tr>
							<tr class="tr">
								<td><strong>Content</strong></td>
								<td><?php echo $this->Form->checkbox('Newsletter.content_disabled', array('onclick' => "updateOverride(this, 'NewsletterContent')", 'div'=>false, 'label'=>false)); ?></td>
								<td>
									<?php
										($this->Form->value('Newsletter.content_disabled') == 1)? $options = array('disabled' => 'disabled'): $options = array();
										echo $this->Form->input('Newsletter.content', array('div'=>false, 'label'=>false)+$options);
									?><br><b>For better appearance add image with 550px width in the editor.</b>
								</td>
								<td>
									The following field replacements are available:<br/>
									[[client_name]] replaced by the name of the client<br/>
									[[client_email]] replaced by the clients email address<br/>
									[[program_name]] replaced by the name of the program<br/>
									[[first_name]] replaced by the first name of the recipient<br/>
									[[last_name]] replaced by the last name of the recipient<br/>
									[[web_site]] replaced by the domain name of the client<br/>
									[[twitter_link]] replaced by the twitter link of the client<br/>
									[[user_name]] replaced by the username of the recipient<br/>
									[[password]] replaced by the password of the recipient<br/>
									[[unique_id]] replaced by the unique id of the recipient
								</td>
							</tr>
							<tr class="tr">
								<td><strong>Font</strong></td>
								<td></td>
								<td>
									<?php echo $this->Form->input('Newsletter.font', array('div'=>false,'label'=>false, 'type'=>'select', 'options'=>$fonts)); ?>
								</td>
								<td></td>
							</tr>
						</table>
					</div>
					<div align="center" style="padding-top:10px">
						<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(1)">Next</button>
					</div>
				</div>
				
				<div class="tabbertab"  style="min-height:200px;overflow:hidden;padding-left:20px !important;">
		  			<h2>State Limit And Products</h2>
		    		<h4>State Limit And Products</h4>
					<div style="background-color:#EFEFEF;border:1px solid #cccccc;padding:10px;width:855px;min-height:150px;overflow:hidden">
						<table>
							<tr class="tr">
								<td style="width:120px"><strong>Limit newsletter to country and states</strong></td>
								<td style="width:50px"><?php echo $this->Form->checkbox('Newsletter.limit_users_to_state'); ?></td>
								<td>
									<?php echo $this->Form->input('NewsletterState.country',array('default'=>$selected_newsletter_country, 'type'=>'select','label'=>false,'options'=>$newsletter_countries,'empty'=>' '));?>
									<?php 
										$options = array('url' => 'update_newsletter_states','update' => 'NewsletterStateState');
									echo $this->Ajax->observeField('NewsletterStateCountry', $options);?>
								</td>
							</tr>
							<tr class="tr">
								<td colspan="2"></td>
								<td style="width:200px"><?php echo $this->Form->input('NewsletterState.state', array('div'=>false,'label'=>false, 'type'=>'select', 'options'=>$states, 'selected'=>$selected_newsletter_states, 'multiple'=>'multiple')); ?></td>
								<td>This will send the newsletter only to users in the selected states</td>
							</tr>
							<tr class="tr">
								<td><strong>Limit products to recipient's state</strong></td>
								<td colspan="2"><?php echo $this->Form->checkbox('Newsletter.limit_products_to_state'); ?></td>
							</tr>
							<tr class="tr">
								<td><strong>Products Short</strong></td>
								<td><?php echo $this->Form->checkbox("Newsletter.products_short_disabled"); ?></td>
								<td id="products_short" >
									<div id="shortProducts">
									<?php if (isset($this->data['NewslettersShortProduct'])) : ?>
										<?php foreach ($this->data['NewslettersShortProduct'] as  $product ) : ?>
											<div id="shortProduct<?php echo $product['product_id']; ?>">
												<input type="text" name="data[NewslettersShortProduct][]" value="<?php echo $product['product_id']; ?>" />
												<a href="javascript:void(0)" onclick="return deleteOffer('shortProduct<?php echo $product['product_id']; ?>');">delete</a>
											</div>
										<?php endforeach ; ?>
									<?php endif; ?>
									</div>
									<a href="javascript:void(0)" onclick="return addShortProduct();">add another offer</a>
								</td>
								<td></td>
							</tr>
							<tr class="tr">
								<td colspan="2"><strong>Products Title: </strong>&nbsp;&nbsp; </td>
								<td><?php echo $this->Form->input("Newsletter.products_title", array('class'=>'news_input1', 'div'=>false, 'label'=>false))?></td>
								<td></td>
							</tr>
							<tr class="tr">
								<td><strong>Products</strong></td>
								<td><?php echo $this->Form->checkbox("Newsletter.products_disabled"); ?></td>
								<td id="offers" colspan=2">
									<?php if (empty($products['NewsletterCategory'])) { $key = 0; }else{ ?>
									<?php foreach ($products['NewsletterCategory'] as $key => $category) : ?>
									<div id="category<?php echo $key; ?>">
										<div>
											<select name="data[Product][NewsletterCategory][<?php echo $key; ?>][category_id]">
												<option></option>
												<?php foreach ($categories as $cat_id => $cat) : ?>
													<option value="<?php echo $cat_id; ?>" <?php echo ($cat_id == $category['category_id'])? 'selected="selected"': '';?>><?php echo $cat	; ?></option>
												<?php endforeach; ?>
											</select>
										</div>
										<?php foreach ($category['NewsletterOffer'] as  $offer ) : ?>
										<div id="offer<?php echo $offer['NewsletterOffer']['id']; ?>">
											<input type="text" class="news_input1" name="data[Product][NewsletterCategory][<?php echo $key; ?>][NewsletterOffer][]" value="<?php echo $offer['NewsletterOffer']['offer_id']; ?>" />
											<a href="javascript:void(0)" onclick="return deleteOffer('offer<?php echo $offer['NewsletterOffer']['id']; ?>');">delete</a>
										</div>
										<?php endforeach ; ?>
										<div id="categorylastoffer<?php echo $key; ?>">
											<input type="text" class="news_input1" name="data[Product][NewsletterCategory][<?php echo $key; ?>][NewsletterOffer][]" value="" />
											<a href="javascript:void(0)" onclick="return deleteOffer('categorylastoffer<?php echo $key; ?>');">delete</a>
										</div>
									</div>
									<a href="javascript:void(0)" onclick="return addProductOffer('<?php echo $key; ?>');">add another offer</a>
									<br/><br/>
									<?php endforeach; ?>
									<div id="categorynew">
										<div>
											<select name="data[Product][NewsletterCategory][new][category_id]">
												<option></option>
												<?php foreach ($categories as $cat_id => $cat) : ?>
													<option value="<?php echo $cat_id; ?>"><?php echo $cat; ?></option>
												<?php endforeach; ?>
											</select>
										</div>
										<div id="categorylastoffernew">
											<input type="text" class="news_input1" name="data[Product][NewsletterCategory][new][NewsletterOffer][]" value="" />
											<a href="javascript:void(0)" onclick="return deleteOffer('categorylastoffernew');">delete</a>
										</div>
									</div>
									<a href="javascript:void(0)" onclick="return addProductOffer('new');">add another offer</a>
									<br/><br/>
									<?php } ?>
									<a href="javascript:void(0)" onclick="return addProductCategory(this);">add another category</a>
								</td>
							</tr>
							<tr class="tr">
								<td colspan="2"><strong>Product link </strong><br/><sub>leave blank for default Rewards CMS product view<br/><br/></sub></td>
								<td colspan="2"><?php echo $this->Form->input('Newsletter.product_link',array('class'=>'news_input1', 'div'=>false, 'label'=>false)); ?> <br/>Ensure to include the full http link e.g. http://www.australianunity.com.au/wellplanrewards</td>
							</tr>
						</table>
					</div>
					<div align="center" style="padding-top:10px">
						<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(0)">Previous</button>
						<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(2)">Next</button>
					</div>
				</div>
				
				<div class="tabbertab"  style="min-height:200px;overflow:hidden;padding-left:20px !important;">
		  			<h2>Show And Save</h2>
		    		<h4>Show And Save</h4>
					<div style="background-color:#EFEFEF;border:1px solid #cccccc;padding:10px;width:855px;min-height:150px;overflow:hidden">
						<table>
							<tr class="tr">
								<td colspan="3"><strong>Show and Save Titles</strong></td>
								<td><?php echo $this->Form->input('Newsletter.show_and_save_title',array('class'=>'news_input1', 'div'=>false, 'label'=>false)); ?></td>
							</tr>
							<tr class="tr">
								<td><strong>Show and Save Coupons</strong></td>
								<td><?php echo $this->Form->checkbox('Newsletter.show_and_save_disabled'); ?></td>
								<td colspan="2">
									<table cellspacing="0" cellpadding="0" style="border:0;">
										<tr class="tr">
											<td rowspan="2"  style="border:0;">Link 1</td>
											<td style="border:0;">Link</td><td style="border:0;"><?php echo $this->Form->input('Newsletter.show_and_save1_link', array('div'=>false, 'label'=>false)); ?></td>
											<td rowspan="2"  style="border:0;">Link 2</td>
											<td style="border:0;">Link</td><td style="border:0;"><?php echo $this->Form->input('Newsletter.show_and_save2_link', array('div'=>false, 'label'=>false)); ?></td>
											<td rowspan="2"  style="border:0;">Link 3</td>
											<td style="border:0;">Link</td><td style="border:0;"><?php echo $this->Form->input('Newsletter.show_and_save3_link', array('div'=>false, 'label'=>false)); ?></td>
										</tr>
										<tr class="tr">
											<td style="border:0;">Text</td><td style="border:0;"><?php echo $this->Form->input('Newsletter.show_and_save1', array('div'=>false, 'label'=>false)); ?></td>
											<td style="border:0;">Text</td><td style="border:0;"><?php echo $this->Form->input('Newsletter.show_and_save2', array('div'=>false, 'label'=>false)); ?></td>
											<td style="border:0;">Text</td><td style="border:0;"><?php echo $this->Form->input('Newsletter.show_and_save3', array('div'=>false, 'label'=>false)); ?></td>
										</tr>
										<tr class="tr">
											<td rowspan="2"  style="border-right:0">Link 4</td>
											<td  style="border-right:0">Link</td><td style="border-right:0;"><?php echo $this->Form->input('Newsletter.show_and_save4_link', array('div'=>false, 'label'=>false)); ?></td>
											<td rowspan="2"  style="border-right:0;">Link 5</td>
											<td style="border-right:0;">Link</td><td style="border-right:0;"><?php echo $this->Form->input('Newsletter.show_and_save5_link', array('div'=>false, 'label'=>false)); ?></td>
											<td rowspan="2"  style="border-right:0;">Link 6</td>
											<td style="border-right:0;">Link</td><td style="border-right:0;"><?php echo $this->Form->input('Newsletter.show_and_save6_link', array('div'=>false, 'label'=>false)); ?></td>
										</tr>
										<tr class="tr">
											<td style="border:0;">Text</td><td style="border:0;"><?php echo $this->Form->input('Newsletter.show_and_save4', array('div'=>false, 'label'=>false)); ?></td>
											<td style="border:0;">Text</td><td style="border:0;"><?php echo $this->Form->input('Newsletter.show_and_save5', array('div'=>false, 'label'=>false)); ?></td>
											<td style="border:0;">Text</td><td style="border:0;"><?php echo $this->Form->input('Newsletter.show_and_save6', array('div'=>false, 'label'=>false)); ?></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</div>
					<div align="center" style="padding-top:10px">
						<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(1)">Previous</button>
						<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(3)">Next</button>
					</div>
				</div>
				
				<div class="tabbertab"  style="min-height:200px;overflow:hidden;padding-left:20px !important;">
		  			<h2>Footer</h2>
		    		<h4>Footer</h4>
					<div style="background-color:#EFEFEF;border:1px solid #cccccc;padding:10px;width:855px;min-height:150px;overflow:hidden">
						<table>
							<tr class="tr">
								<td><strong>What's Changed</strong></td>
								<td>
									<?php echo $this->Form->checkbox('Newsletter.whats_changed_disabled'); ?>
								</td>
								<td>
									<?php
										($this->Form->value('Newsletter.whats_changed_disabled') == 1)? $options = array('disabled' => 'disabled'): $options = null;
										echo $this->Form->input('Newsletter.whats_changed_title', array('class'=>'news_input1', 'div'=>false, 'label'=>false,$options));
									?><br/>
								</td>
								<td></td>
							</tr>
							<tr class="tr">
								<td><strong>Page Footer</strong></td>
								<td><?php echo $this->Form->checkbox('Newsletter.footer_disabled', array('onclick' => "updateOverride(this, 'NewsletterFooterContent')")); ?></td>
								<td >
									<?php
										($this->Form->value('Newsletter.footer_disabled') == 1)? $options = array('disabled' => 'disabled', 'div'=>false, 'label'=>false): $options = array('div'=>false, 'label'=>false);
										echo $this->Form->input('Newsletter.footer_content', $options);
					
									?>
								</td>
								<td></td>
							</tr>
						</table>
					</div>
					<div align="center" style="padding-top:10px">
						<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(2)">Previous</button>
						<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(4)">Next</button>
					</div>
				</div>
				
				<div class="tabbertab"  style="min-height:200px;overflow:hidden;padding-left:20px !important;">
		  			<h2>Clients And Preview</h2>
		    		<h4>Clients And Preview</h4>
					<div style="background-color:#EFEFEF;border:1px solid #cccccc;padding:10px;width:855px;min-height:150px;overflow:hidden">
						<table>
							<tr class="tr">
								<td><strong>Clients</strong></td>
								<td></td>
								<td>
									<?php echo $this->Form->input('Client.Client', array('multiple' => 'multiple','div'=>false, 'label'=>false,'onclick' => "updateField(this, 'NewsletterSend')")); ?>
									<input type="hidden" name="data[Client][Client][]" value="" />
								</td>
								<td></td>
							</tr>
							<tr class="tr">
								<td colspan="2"><strong>Display old/new format</strong></td>
								<td>
									<?php $previewVal = !empty($this->data["Newsletter"]["preview"])?$this->data["Newsletter"]["preview"]:"O";echo $this->Form->input('Newsletter.preview', array('type' => 'radio','options' => array('O' => 'Results - Old Format', 'N' => 'Results - New Format', '1'=>'Template - 1','2'=>'Template - 2','3'=>'Template - 3'),'value'=>$previewVal,'class'=>'input_radio','separator'=>'<br/>','legend'=>false));?>
								</td>
								<td></td>
							</tr>
						</table>
					</div>
					<div align="center" style="padding-top:10px">
						<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(3)">Previous</button>
						<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(5)">Next</button>
					</div>
				</div>
				
				<div class="tabbertab"  style="min-height:200px;overflow:hidden;padding-left:20px !important;">
		  			<h2>Preview Styles</h2>
		    		<h4>Preview styles</h4>
					<div style="background-color:#EFEFEF;border:1px solid #cccccc;padding:10px;width:855px;min-height:150px;overflow:hidden">
						<div style="float:left">
							<table style="width:450px">
								<tr class="tr">
									<td colspan="2">
										<strong>Twitter Link</strong>
									</td>
									<td>
										<?php echo $this->Form->input('Newsletter.twitter_link',array('class'=>'news_input1', 'div'=>false, 'label'=>false));?>
									</td>
								</tr>
								<tr class="tr">
									<td colspan="2">
										<strong>Coupon Book Link</strong>
									</td>
									<td>
										<?php echo $this->Form->input('Newsletter.coupon_book_link',array('class'=>'news_input1', 'div'=>false, 'label'=>false));?>
									</td>
								</tr>
								<tr class="tr">
									<td colspan="2">
										<strong>Preview Menu Background Color</strong>
									</td>
									<td>
										<?php echo $this->Form->input('Newsletter.preview_menu_color', array('class' => 'color news_input1', 'div'=>false, 'label'=>false)); ?>
										<span style="background-color:#<?php echo $this->Form->value('Newsletter.preview_menu_color'); ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
									</td>
								</tr>
								<tr class="tr">
									<td colspan="2">
										<strong>Online Version Background Color</strong>
									</td>
									<td>
										<?php echo $this->Form->input('Newsletter.online_version_color', array('class' => 'color news_input1', 'div'=>false, 'label'=>false)); ?>
										<span style="background-color:#<?php echo $this->Form->value('Newsletter.online_version_color'); ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
									</td>
								</tr>
								<tr class="tr">
									<td colspan="2">
										<strong>Online Coupon Book Background <br>Color</strong>
									</td>
									<td>
										<?php echo $this->Form->input('Newsletter.coupon_book_color', array('class' => 'color news_input1', 'div'=>false, 'label'=>false)); ?>
										<span style="background-color:#<?php echo $this->Form->value('Newsletter.coupon_book_color'); ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
									</td>
								</tr>
								<tr class="tr">
									<td colspan="2">
										<strong>Call background Color</strong>
									</td>
									<td>
										<?php echo $this->Form->input('Newsletter.call_color', array('class' => 'color news_input1', 'div'=>false, 'label'=>false)); ?>
										<span style="background-color:#<?php echo $this->Form->value('Newsletter.call_color'); ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
									</td>
								</tr>
								<tr class="tr">
									<td colspan="2">
										<strong>Newsletter Preview Background <br>Color</strong>
									</td>
									<td>
										<?php echo $this->Form->input('Newsletter.newsletter_background', array('class' => 'color news_input1', 'div'=>false, 'label'=>false)); ?>
										<span style="background-color:#<?php echo $this->Form->value('Newsletter.newsletter_background'); ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
									</td>
								</tr>
							</table>
						</div>
						<div>
							<table style="width:400px">
								<tr class="tr">
									<td colspan="2">
										<strong>Facebook Link</strong>
									</td>
									<td>
										<?php echo $this->Form->input('Newsletter.facebook_link',array('class'=>'news_input1', 'div'=>false, 'label'=>false));?>
									</td>
								</tr>
								<tr class="tr">
									<td colspan="2">
										<strong>Phone Number</strong>
									</td>
									<td>
										<?php echo $this->Form->input('Newsletter.phone_number',array('class'=>'news_input1', 'div'=>false, 'label'=>false));?>
									</td>
								</tr>
								<tr class="tr">
									<td colspan="2">
										<strong>Preview Menu Text Color</strong>
									</td>
									<td>
										<?php echo $this->Form->input('Newsletter.menu_text_color', array('class' => 'color news_input1', 'div'=>false, 'label'=>false)); ?>
										<span style="background-color:#<?php echo $this->Form->value('Newsletter.menu_text_color'); ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
									</td>
								</tr>
								<tr class="tr">
									<td colspan="2">
										<strong>Online Version Text Color</strong>
									</td>
									<td>
										<?php echo $this->Form->input('Newsletter.version_text_color', array('class' => 'color news_input1', 'div'=>false, 'label'=>false)); ?>
										<span style="background-color:#<?php echo $this->Form->value('Newsletter.version_text_color'); ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
									</td>
								</tr>
								<tr class="tr">
									<td colspan="2">
										<strong>Online Coupon Book Text <br>Color</strong>
									</td>
									<td>
										<?php echo $this->Form->input('Newsletter.book_text_color', array('class' => 'color news_input1', 'div'=>false, 'label'=>false)); ?>
										<span style="background-color:#<?php echo $this->Form->value('Newsletter.book_text_color'); ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
									</td>
								</tr>
								<tr class="tr">
									<td colspan="2">
										<strong>Call Text Color</strong>
									</td>
									<td>
										<?php echo $this->Form->input('Newsletter.call_text_color', array('class' => 'color news_input1', 'div'=>false, 'label'=>false)); ?>
										<span style="background-color:#<?php echo $this->Form->value('Newsletter.call_text_color'); ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
									</td>
								</tr>
								<tr class="tr">
									<td colspan="2">
										<strong>Newsletter Preview Text <br>Color</strong>
									</td>
									<td>
										<?php echo $this->Form->input('Newsletter.newsletter_text_color', array('class' => 'color news_input1', 'div'=>false, 'label'=>false)); ?>
										<span style="background-color:#<?php echo $this->Form->value('Newsletter.newsletter_text_color'); ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
									</td>
								</tr>
							</table>
						</div>
						<br/>
						<div>
							<table>
								<tr class="tr">
									<td>
										<strong>Template 2 and 3 Contact Us And Image Background Color</strong>
									</td>
									<td>
										<?php echo $this->Form->input('Newsletter.contact_image_background', array('class' => 'color news_input1', 'div'=>false, 'label'=>false)); ?>
										<span style="background-color:#<?php echo $this->Form->value('Newsletter.contact_image_background'); ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
									</td>
								</tr>
							</table>
						</div>
						<br/>
						<div>
							<table>
								<tr class="tr">
									<td colspan="2">
										<strong>For Template1 and Template2 use image1 and image2</strong>
										<br/>
										For better appearance please upload image1 and image2 with width 250px and height 170px
									</td>
								</tr>
								<tr class="tr">
									<td style="width:150px">
										<strong>Upload Image1</strong>
									</td>
									<td>
										<?php echo $this->Form->file('Image1.file'); ?><br/>
										Remove <?php echo $this->Form->checkbox('ImageDelete1.file'); ?>
									</td>
								</tr>
								<tr class="tr">
									<td>
										<strong>Upload Image2</strong>
									</td>
									<td>
										<?php echo $this->Form->file('Image2.file'); ?><br/>
										Remove <?php echo $this->Form->checkbox('ImageDelete2.file'); ?>
									</td>
								</tr>
								<tr class="tr">
									<td colspan="4">
										<strong>For Template3 use image3 and image4</strong>
										<br/>
										For better appearance please upload image3 with width 210px and height 285px<br/>
										For better appearance please upload image4 with width 210px and height 245px
									</td>
								</tr>
								<tr class="tr">
									<td>
										<strong>Upload Image3</strong>
									</td>
									<td>
										<?php echo $this->Form->file('Image3.file'); ?><br/>
										Remove <?php echo $this->Form->checkbox('ImageDelete3.file'); ?>
									</td>
								</tr>
								<tr class="tr">
									<td>
										<strong>Upload Image4</strong>
									</td>
									<td>
										<?php echo $this->Form->file('Image4.file'); ?><br/>
										Remove <?php echo $this->Form->checkbox('ImageDelete4.file'); ?>
									</td>
								</tr>
							</table>
						</div>
					</div>
					<div align="center" style="padding-top:10px">
						<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(4)">Previous</button>
					</div>
				</div>
			</div>
			<?php echo $this->Form->input('Newsletter.id', array('type' => 'hidden')); ?>
			<?php echo $this->Form->input('Newsletter.user_id', array('type' => 'hidden')); ?>
			<?php echo $this->Form->input('Newsletter.program_id', array('type' => 'hidden')); ?>
		</div>
	</div>
	</form>



				<script type="text/javascript">
				function deleteOffer(offer) {

					$("#"+offer).remove();

					return false;
				}

				function addProductOffer(category) {

					var rand = Math.round(10000000000*Math.random());
					var html = '<div id="categorylastoffer' + rand + '">';
					html += '<input type="text" name="data[Product][NewsletterCategory][' + category + '][NewsletterOffer][]" value="" id="input' + rand + '"/>';
					html += ' <a href="javascript:void(0)" onclick="return deleteOffer(\'categorylastoffer' + rand + '\');">delete</a>';
					html += '</div>';

					$(html).insertAfter('#category' + category);

					//$('#input' + rand).focus();

					return false;
				}

				function addProductCategory(element) {

					var rand = Math.round(10000000000*Math.random());
					
					var html = '<div id="category' + rand + '">';
					html += '<div>';
					html += '<select name="data[Product][NewsletterCategory][' + rand + '][category_id]">';
					html += '<option></option>';
					<?php foreach ($categories as $cat_id => $cat) : ?>  
					html += '<option value="<?php echo $cat_id; ?>"><?php echo addslashes($cat); ?></option>';
					<?php endforeach; ?>
					html += '</select>';
					html += '</div>';
					html += '<div id="offer' + rand + '"><input type="text" name="data[Product][NewsletterCategory][' + rand + '][NewsletterOffer][]" value="" /> ';
					html += '<a href="javascript:void(0)" onclick="return deleteOffer(\'offer' + rand + '\');">delete</a></div>';
					html += '</div>';
					html += '<a href="javascript:void(0)" onclick="return addProductOffer(\'' + rand + '\');">add another offer</a>';
					html += '<br/>';
					html += '<br/>';

					$(html).insertBefore(element);

					return false;

				}
				function addShortProduct() {

					var rand = Math.round(10000000000*Math.random());
					var html = '<div id="shortProduct' + rand + '">';
					html += '<input type="text" name="data[NewslettersShortProduct][]" value="" id="input' + rand + '"/>';
					html += ' <a href="javascript:void(0)" onclick="return deleteOffer(\'shortProduct' + rand + '\');">delete</a>';
					html += '</div>';

					$(html).insertBefore('#shortProducts');

					//$('input' + rand).focus();

					return false;
				}

				function addShortCategory(element) {

					var rand = Math.round(10000000000*Math.random());

					var html = '<div id="category' + rand + '">';
					html += '<div>';
					html += '<select name="data[Short][NewsletterCategory][' + rand + '][category_id]">';
					html += '<option></option>';
					<?php foreach ($categories as $cat_id => $cat) : ?>
					html += '<option value="<?php echo $cat_id; ?>"><?php echo addslashes($cat); ?></option>';
					<?php endforeach; ?>
					html += '</select>';
					html += '</div>';
					html += '<div id="offer' + rand + '"><input type="text" name="data[Short][NewsletterCategory][' + rand + '][NewsletterOffer][]" value="" /> ';
					html += '<a href="javascript:void(0)" onclick="return deleteOffer(\'offer' + rand + '\');">delete</a></div>';
					html += '</div>';
					html += '<a href="javascript:void(0)" onclick="return addShortOffer(\'' + rand + '\');">add another offer</a>';
					html += '<br/>';
					html += '<br/>';

					$(html).insertBefore(element);

					return false;

				}

			</script>
	

<script type="text/javascript">

	function updateOverride(checkbox, input) {

		if ($(checkbox).checked == true) {
			$(input).disabled = true;
		} else {
			$(input).disabled = false;
		}

	}

	function updateField(field, input){
		if($(field).value==""){
			$(input).disabled = true;
		}else{
			$(input).disabled = false;
		}
	}

	function saveNewsletter()
	{
		toggleSpellchecker();
		$('save').value='1'; 
	}

	function previewNewsletter()
	{
		$('#send').val('1');
		toggleSpellchecker();
	}

	function toggleSpellchecker()
	{
		var val = navigator.userAgent.toLowerCase();
		if(val.indexOf("msie") < 0)
		{
			tinyMCE.execCommand ('mceFocus', false, 'NewsletterContent');
			tinyMCE.execCommand('mceSpellCheck',false,'NewsletterContent');

			tinyMCE.execCommand ('mceFocus', false, 'NewsletterFooterContent');
			tinyMCE.execCommand('mceSpellCheck',false,'NewsletterFooterContent');
		}
	}

</script>
