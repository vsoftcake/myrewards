<style>
	#content
	{
	border-left:1px solid #cccccc;
	border-right:1px solid #cccccc;
	border-bottom:1px solid #cccccc;
	margin-left:20px !important;
	padding-left:15px !important;
	width:916px;
	}
</style>
<h1>Confirm Send</h1>
<br/>
<p>You are about to send the newsletter <i><b><?php echo $newsletter['Newsletter']['name']; ?></b></i> to the following clients:</p>
<br/>
<ul><table>
<?php foreach ($newsletter['NewslettersClient'] as $newsletter_client) : ?>
	<li><b><tr><td><?php echo $newsletter_client['Client']['name']; ?>:</b> </td><td><?php echo $newsletter_client['users']; ?> Users</td></tr></li>
<?php endforeach; ?>
</table></ul>
<br/>
<p>Total emails to send: <?php echo $total_users; ?></p>
<form action="<?php echo $this->Html->url('/admin/newsletters/confirm/'. $newsletter['Newsletter']['id']); ?>" method="post">
<br/>
			<input type="hidden" name="data[Newsletter][id]" value="<?php echo $newsletter['Newsletter']['id']; ?>" id="NewsletterId" />
			<button type="submit" name="previous" >Previous</button>&nbsp;&nbsp;&nbsp;
			<button type="submit" name="next" onclick="sending();">Send Emails</button>
			<span id="process" style="display: none">
				<img style="position: relative; top:4px; " src="/files/newimg/ajax-loader.gif" alt="Working..." />
			</span>
<span id="sending_message"></span>
</form>
<script type="text/javascript">
	function sending() {
		document.getElementById('process').style.display="inline";
		$('sending_message').innerHTML = 'Queueing emails, this can take up to 10 minuites for a large mailout (more than 10,000 emails)';
	}
</script>