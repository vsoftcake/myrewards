<style>
	#content
	{
	border-left:1px solid #cccccc;
	border-right:1px solid #cccccc;
	border-bottom:1px solid #cccccc;
	margin-left:20px !important;
	padding-left:15px !important;
	width:916px;
	}
</style>
<h1>View Newsletter</h1>
<br/>
<script type="text/javascript">
function updateClient() {
	var client_id = $('NewslettersClientClientId').options[$('NewslettersClientClientId').selectedIndex].value;
	if(document.getElementById('UserState')!=null)
	{
		var state = $('UserState').options[$('UserState').selectedIndex].value;
	}
	else 
	{
		var state = '';
	}
	
	$('preview_frame').src = '<?php echo $this->Html->url('/admin/newsletters/newsletter/'. $newsletter['Newsletter']['id']. '/'); ?>' + client_id +'<?php echo  '/'. $newsletter['Newsletter']['dashboard_id']?>/' + state;
	
}

/*
 * Send a test email to the email address in test_email
*/
function sendTest() {

	var email = document.getElementById('test_email').value;
	var client_id = $('NewslettersClientClientId').options[$('NewslettersClientClientId').selectedIndex].value;
	if(document.getElementById('UserState')!=null)
	{
		var state = $('UserState').options[$('UserState').selectedIndex].value;
	}
	else
	{
		var state = '';
	}
	var newsletter_id = <?php echo $newsletter['Newsletter']['id']; ?>;

	Element.show('sending');
	Element.hide('error');
	Element.hide('sent');
	$('test_newsletter_button').disabled = true;

	
	new Ajax.Request(
		'<?php echo $this->Html->url('/admin/newsletters/send_test/'.$from_email); ?>'+'/' + email + '/' + newsletter_id + '/' + client_id + '/' + state,
		{
			method:'get',
			onSuccess: email_sent
		}
	);
	
	return false;
}

function email_sent(transport) {

	if (transport.responseText.substring(0, 4) == 'sent') {
		Element.hide('sending');
		Element.show('sent');
	} else {
		Element.hide('sending');
		Element.show('error');
	}

	$('test_newsletter_button').disabled = false;

}

</script>
<form action="<?php echo $this->Html->url('/admin/newsletters/edit/'. $newsletter['Newsletter']['id']); ?>" method="post">
<table class="newsletter_table">
	<tr>
		<th>Client</th>
		<td><?php echo $this->Form->input('NewslettersClient/client_id', array('div'=>false, 'label'=>false, 'type'=>'select', 'options'=>$clients, 'onchange' => 'updateClient();')); ?></td>
	</tr>
	<?php if(isset($countries)) : ?>
	<tr>
		<th>Country</th>
		<td><?php echo $this->Form->input('User.country', array('div'=>false, 'label'=>false, 'type'=>'select', 'options'=>$countries)); ?></td>
	</tr>
	<?php endif; ?>
	<?php if(isset($states)) : ?>
	<tr>
		<th>State</th>
		<td><?php echo $this->Form->input('User.state', array('div'=>false, 'label'=>false, 'type'=>'select', 'options'=>$states,  'onchange' => 'updateClient();')); ?></td>
	</tr>
	<?php endif; ?>
	<tr>
		<th>Subject&nbsp;&nbsp;&nbsp;&nbsp;</th>
		<td><?php echo $newsletter['Newsletter']['subject']; ?></td>
	</tr>
	<tr>
		<th></th>
		<td>
			<iframe src="<?php echo $this->Html->url('/admin/newsletters/newsletter/'. $newsletter['Newsletter']['id']. '/'. $first_client. '/'.$newsletter['Newsletter']['dashboard_id'].'/'. $first_state); ?>" id="preview_frame" style="width:782px;height:500px;border: 1px solid black;padding: 2px;"></iframe>
		</td>
	</tr>
	<tr>
		<th></th>
		<td>
			<button type="submit" name="view_copy" >copy</button>&nbsp;&nbsp;&nbsp;
			<button type="submit" name="cancel">cancel</button>
		</td>
	</tr>
</table>
</form>