<?php echo $this->element('message'); ?>
<?php if ($page['ClientPage']['h1'] != '') : ?>
	<h1><?php echo $page['ClientPage']['h1']; ?></h1>
<?php endif; ?>
<?php if ($page['ClientPage']['h2'] != '') : ?>
	<h2><?php echo $page['ClientPage']['h2']; ?></h2>
<?php endif; ?>
<?php if ($page['ClientPage']['h3'] != '') : ?>
	<h3><?php echo $page['ClientPage']['h3']; ?></h3>
<?php endif; ?>
<?php if ($page['ClientPage']['content'] != '') : ?>
	<?php echo $page['ClientPage']['content']; ?>
<?php endif; ?>
<form  method="post" action="<?php echo $this->Html->url('/newsletters/unsubscribe/'); ?>">
<table border="0" cellpadding="0" cellspacing="0" class="content_table_2col">
  <tr align="left" valign="middle" class="content_table_row">
    <td><p>Email:&nbsp;</p></td>
    <td valign="middle"><p><?php echo $this->Form->input('User.email'); ?></p></td>
	<td>
		<?php if (isset($error)) : ?>
			<span class="form_error_message"><img alt="Error" src="<?php echo $this->Html->url('/img/cross.gif'); ?>"/> <span>Email address not found</span></span>
		<?php endif; ?>
	</td>
  </tr>
  <tr align="left" valign="middle" class="content_table_row">
    <td><p>&nbsp;</p></td>
    <td><button type="submit">Confirm >></button></td>
  </tr>
</table>
</form>
<?php if ($page['ClientPage']['special_offers'] == '1') : ?>
	<div class="module" style="width: 66%">
		<div class="tl">
			<div class="tr">
				<div class="t">
					&nbsp;
				</div>
			</div>
		</div>
		<div class="content">
			<?php echo $this->element('special_offers'); ?>
		</div>
		<div class="bl">
			<div class="br">
				<div class="b">&nbsp;</div>
			</div>
		</div>
	</div>
<?php endif; ?>