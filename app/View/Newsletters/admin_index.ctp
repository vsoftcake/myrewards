<style>
#content
{
border-left:1px solid #cccccc;
border-right:1px solid #cccccc;
border-bottom:1px solid #cccccc;
margin-left:20px !important;
padding-left:15px !important;
width:916px;
}
.admin_button
	{
	height: 24px;
	padding-top: 2px;
	padding-bottom: 5px;
	border-radius: 5px 5px 5px 5px;
	background-color: #0066cc;
	border: 1px solid #cccccc;
	color: #FFFFFF; 
	cursor: pointer;
	font-size:13px;
	}

</style>

<div style="width=900px;">
<table style="margin-left:-2px;margin-top:10px;">
	<tr>
		<td>
            &nbsp;&nbsp;<img style="width:40px;height:40px;" src="/files/admin_home_icons/e-news.png"  alt="e-news" title="Newsletters" />&nbsp;&nbsp;
        </td>
        <td> 
            <h2><?php echo "Newsletters";?></h2>
        </td>
    </tr>        
</table>
</div>

<div class="actions" style="width:923px;background-color:#cccccc;height:65px;margin-left:-2px;border:1px solid #969090;">
	<div style="float:left;width:670px">
		<form action="<?php echo $this->Html->url('/admin/newsletters/'); ?>" method="post" enctype="multipart/form-data">
			<table style="margin-top:19px">
				<tr>
					<td style="padding-left:5px;"><strong>Program:</strong></td>
					<td><?php echo $this->Form->input('Newsletter.search', array('div'=>false, 'label'=>false, 'type'=>'select', 'options'=>$programs,'empty'=>' ')); ?></td>
					<td><button type="submit" class="admin_button">Select</button></td>
					<td style="width:70px;"></td>
				</tr>
			</table>
		</form>
	</div>
	<div style="padding-top:20px">
		<button class ="admin_button" onclick="location.href='<?php echo $this->Html->url('edit'); ?>';">Create New Newsletter</button>
	</div>
</div>
<br/><br/>
<?php if($show) :?>
<div class="list">
	<table width="923">
	<tr height="30">
		<th style="background-color:#cccccc;text-align:left;padding-left:5px;border-left:1px solid #969090;border-bottom:1px solid #969090;border-top:1px solid #969090;"><strong>Sent By</strong></th>
		<th style="background-color:#cccccc;text-align:left;padding-left:1px;border-bottom:1px solid #969090;border-top:1px solid #969090;"><strong>Name</strong></th>
		<th style="background-color:#cccccc;text-align:left;padding-left:1px;border-bottom:1px solid #969090;border-top:1px solid #969090;"><strong>Start Time</strong></th>
		<th style="background-color:#cccccc;text-align:left;padding-left:1px;border-bottom:1px solid #969090;border-top:1px solid #969090;"><strong>Total</strong></th>
		<th style="background-color:#cccccc;text-align:left;padding-left:1px;border-bottom:1px solid #969090;border-top:1px solid #969090;"><strong>Sent</strong></th>
		<th style="background-color:#cccccc;text-align:left;padding-left:1px;border-bottom:1px solid #969090;border-top:1px solid #969090;"><strong>Failed</strong></th>
		<th style="background-color:#cccccc;text-align:left;padding-left:1px;border-bottom:1px solid #969090;border-top:1px solid #969090;"><strong>Canceled</strong></th>
		<th style="background-color:#cccccc;text-align:left;padding-left:1px;border-bottom:1px solid #969090;border-top:1px solid #969090;"><strong>Remaining</strong></th>
		<th style="background-color:#cccccc;text-align:left;padding-left:1px;border-bottom:1px solid #969090;border-top:1px solid #969090;"><strong>Status</strong></th>
		<th class="actions" style="background-color:#cccccc;text-align:left;padding-left:4px;border-right:1px solid #969090;border-bottom:1px solid #969090;border-top:1px solid #969090;"><strong><?php echo "Actions";?></strong></th>
	</tr>
	<?php
	$i = 0;
	foreach ($newsletters as $newsletter):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
		
		$sent = 0;
		$failed =0;
		$canceled =0;
		$remaining=0;
	?>
		<tr<?php echo $class;?>>
			<td>
				<?php echo $newsletter['User']['first_name']." ".$newsletter['User']['last_name'] ?>
			</td>
			<td>
				<?php echo $newsletter['Newsletter']['name'] ?>
			</td>
			<td>
				<?php echo  ($newsletter['Newsletter']['status'] != '')? $newsletter['Newsletter']['modified']: ''; ?>
			</td>
			<td>
				<?php 
					$total = 0;
					if (isset($newsletter['Total'])) {
						foreach ($newsletter['Total'] as $status) {
							$total += $status;
						}
					}
					echo $total;
				?>
			</td>
			<td>
				<?php echo (isset($newsletter['Total']['sent']))? $newsletter['Total']['sent']: 0; ?>
			</td>
			<td>
				<?php echo (isset($newsletter['Total']['failed']))? $newsletter['Total']['failed']: 0; ?>
			</td>
				<td>
				<?php echo (isset($newsletter['Total']['canceled']))? $newsletter['Total']['canceled']: 0; ?>
			</td>
				<td>
				<?php 
					$remaining = 0;
					if (isset($newsletter['Total']['queued'])) {
						$remaining += $newsletter['Total']['queued'];
					}
					if (isset($newsletter['Total']['sending'])) {
						$remaining += $newsletter['Total']['sending'];
					}
					echo $remaining;
				?>
			</td>
			<td>
				<?php echo $newsletter['Newsletter']['status'] ?>
			</td>
			<td class="actions">
				<?php echo $this->Html->link(__('View', true), array('action'=>'view', $newsletter['Newsletter']['id']))."&nbsp;";?>
				<?php 
				if($newsletter['Newsletter']['status'] ==''){
					echo $this->Html->link(__('Edit', true), array('action'=>'edit', $newsletter['Newsletter']['id']))."&nbsp;"; 
					
					if($newsletter['Client']['count'] != 0)
						echo $this->Html->link(__('Send', true), array('action'=>'preview', $newsletter['Newsletter']['id'])); 
				}
				?>
				<?php 
				if($newsletter['Newsletter']['status'] !='sending')
					echo $this->Html->link(__('Delete', true), array('action'=>'delete', $newsletter['Newsletter']['id']), null, sprintf(__('Are you sure you want to delete newsletter \'%s\'?', true), $newsletter['Newsletter']['name'])); 
				else
					echo $this->Html->link(__('Cancel', true), array('action'=>'cancel', $newsletter['Newsletter']['id']), null, sprintf(__('Are you sure you want to cancel newsletter \'%s\'?', true), $newsletter['Newsletter']['name'])); 
				?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
	
</div>
<?php endif;?>






