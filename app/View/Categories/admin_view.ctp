<h3><?php echo $data['Category']['name']; ?></h3>
<div class="list">
	<table cellpadding="0" cellspacing="0">
	<tr>
		<th><?php echo $this->Paginator->sort('id');?></th>
		<th><?php echo $this->Paginator->sort('name');?></th>
		<th class="actions"><?php echo "Actions";?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($products as $product):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
		<tr<?php echo $class;?>>
			<td>
				<?php echo $product['Product']['id'] ?>
			</td>
			<td>
				<?php echo $product['Product']['name'] ?>
			</td>
			<td class="actions">
				<?php echo $this->Html->link(__('View', true), array('action'=>'view', $product['Product']['id'])); ?>
				<?php echo $this->Html->link(__('Edit', true), array('action'=>'edit', $product['Product']['id'])); ?>
				<?php echo $this->Html->link(__('Delete', true), array('action'=>'delete', $product['Product']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $product['Product']['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
		<div class="paging">
		<?php echo $this->Paginator->prev('<< Previous', array(), null, array('class'=>'disabled'));?>
		<?php echo $this->Paginator->numbers();?>
		<?php echo $this->Paginator->next('Next >>', array(), null, array('class'=>'disabled'));?>
	</div>
	<p><?php echo $this->Paginator->counter(array('format' => __('Page %page% of %pages%', true)));	?></p>
</div>