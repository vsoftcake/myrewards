<style type="text/css">

	.rounded-corners 
	{
		-moz-border-radius: 10px 10px 0px 0px; /* Firefox */
		-webkit-border-radius: 10px 10px 0px 0px; /* Safari, Chrome */
		border-radius: 10px 10px 0px 0px; /* CSS3 */
	}
	.tr 
	{
		height:35px;
	}
	.admin_button
	{
		height: 24px;
		padding-bottom: 5px;
		border-radius: 5px 5px 5px 5px;
		background-color: #0066cc;
		border: 1px solid #cccccc;
		color: #FFFFFF; 
		cursor: pointer;
		font-size:13px;
	}
</style>
	<?php echo $this->Html->script('jscolor'); ?>
	<script type="text/javascript">
		document.write('<style type="text/css">.tabber{display:none;}<\/style>');
		function confirmation(name,id)
	    {
			var answer = confirm("Are you sure you want to delete client '<?php echo $this->Form->value('Category.name') ?>'")
			if (answer)
			{
				location.href="/admin/categories/delete/<?php echo $this->Form->value('Category.id') ?>";
			}
	    }
	</script>
	<div style="width:953px;border-left:1px solid #cccccc;border-right:1px solid #cccccc;border-bottom:1px solid #cccccc;margin-left:-14px;">
		<div style="width:949px;height:50px;">
			<div style=" height: 40px; float:left; width: 608px;padding-left:37px;">
		       	<div style=" height: 40px; float:left;padding-top:5px;">
		        	<img title="Categories" alt="Categories" src="/files/admin_home_icons/categories.png" style="width:40px;height:40px;">
		        </div>
		        <div style=" height: 40px;float:left;padding-left:10px;">
		        	<h2>Edit Category <?php if($this->Form->value('Category.name')!=''): ?>- <?php echo $this->Form->value('Category.name') ?><?php endif;?></h2>
		        </div> 
			</div>
	        <div class="actions" style=" height: 40px; float:left;  width: 300px;">
				<ul style="padding:20px;margin-left:-28px;">
					<li style="display:inline;"><?php echo $this->Form->button('Cancel', array('onclick' => "location.href='".$this->Session->read('redirect')."'",
					'class'=>'admin_button','style'=>'width:60px;', 'type'=>'button'));  ?></li><li>
					<?php $conname=$this->Form->value('Category.name');$conid=$this->Form->value('Category.id');?>
					<li style="display:inline;"><?php echo $this->Form->button('Delete Category', array('onclick' => "confirmation('$conname','$conid')",'style'=>'width:110px;','class'=>'admin_button'));  ?></li>
		        </ul>
			</div>
		</div>
		<form action="<?php echo $this->Html->url('/admin/categories/edit/'. $this->Html->value('Category.id')); ?>" method="post"  enctype="multipart/form-data">
			<?php echo $this->Form->input('Category.id', array('type' => 'hidden')); ?>
			<div class="tabber" id="mytab1">
				<div class="tabbertab" style="min-height:200px;overflow:hidden;">
		  			<h2>Category Details</h2>
	     			<div style="background-color:#EFEFEF;border:1px solid #cccccc;padding:10px;width:790px;min-height:100px;overflow:hidden">
						<table style="padding:10px;">
							<tr class="tr">
								<td class="bt"><strong>Name</strong></td>
								<td class="bt"><?php echo $this->Form->input('Category.name', array('div'=>false, 'label'=>false)); ?></td>
								<td class="bl"><?php echo $this->Form->error('Category.name', array('required' => 'Name required'), array('required' => 'Name required')); ?></td>
							</tr>
							<tr class="tr">
								<td><strong>Description</strong></td>
								<td><?php echo $this->Form->input('Category.description', array('div'=>false, 'label'=>false)); ?></td>
								<td class="bl"></td>
							</tr>
							<tr class="tr">
								<td><strong>Details</strong></td>
								<td><?php echo $this->Form->input('Category.details', array('div'=>false, 'label'=>false)); ?></td>
								<td class="bl"></td>
							</tr>
							<tr class="tr">
								<td><strong>Order</strong></td>
								<td><?php echo $this->Form->input('Category.sort', array('div'=>false, 'label'=>false, 'type'=>'text')); ?></td>
								<td class="bl"></td>
							</tr>
							<tr class="tr">
								<td style="padding-right:18px;"><strong>Included clients</strong></td>
								<td>
									<?php echo $this->Form->input('Client.Client', array('multiple' => 'multiple', 'div'=>false, 'label'=>false)); ?>
									<input type="hidden" name="data[Client][Client][]" value="" />
								</td>
								<td class="bl"></td>
							</tr>
							<tr class="tr">
								<td><strong>Included programs</strong></td>
								<td>
									<?php echo $this->Form->input('Program.Program', array('multiple' => 'multiple', 'div'=>false, 'label'=>false)); ?>
									<input type="hidden" name="data[Program][Program][]" value="" />
								</td>
								<td class="bl"></td>
				    		</tr>
				    		<tr class="tr">
							<td><strong>Country</strong></td>
							<td class="br"><?php echo $this->Form->input('Category.countries', array('div'=>false, 'label'=>false, 'type'=>'select', 'options'=>$country)); ?></td>
						</tr>
						<tr class="tr">
							<td><strong>Search weight</strong></td>
							<td>
								<?php echo $this->Form->input('Category.search_weight', array('div'=>false,'label'=>false, 'type'=>'select', 'options'=>$search_weights)); ?>
								<sub>Higher values appear first in search results</sub>
							</td>
						</tr>
						<tr class="tr">
							<td><strong>Icon</strong></td>
							<td class="br">
								<img src="<?php echo $this->Html->url(CATEGORY_LOGO_WWW_PATH. $this->Form->value('Category.id'). '.'. $this->Form->value('Category.logo_extension'). '?rand='. time()); ?>" alt="" /><br/>
								<a href="javascript:void(0)" onclick="this.style.display='none'; $('#CategoryLogoFile').show(); return false;">Change</a>
								<?php echo $this->Form->file('CategoryLogo.file', array('style' => 'display:none;')); ?><br/>
								Remove <?php echo $this->Form->checkbox('CategoryLogoImageDelete.file'); ?>
							</td>
						</tr>	  
					</table>           
			 	</div>
			       	<div style="padding-top:10px" align="center">
						<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(1)">Next</button>
						<button type="submit" class="admin_button">Submit</button>
			       	</div>  
			 	</div>
			 
			 	<div class="tabbertab" style="min-height:190px;overflow:hidden">
			 		<h2>Change Log</h2>
			 		<h4>Change Log</h4>
					<div class="list" id="log_list" style="background-color:#EFEFEF;border:1px solid #cccccc;padding:10px;width:790px;min-height:90px;overflow:hidden">
						<div id="Log" style="margin:0;">Retrieving changes...</div>
						<script type="text/javascript">
						$(function() {
							$.ajax({
								url: "/admin/logs/ajax_list_new/<?php echo Inflector::singularize($this->name);?>/<?php echo $this->Form->value(Inflector::singularize($this->name). '.id');?>",
								type: "GET",
								success: function (data) {
									$('#Log').html(data);
								}
							});
						});</script>
					</div>
					<div style="padding-top:10px" align="center">
						<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(0)">Previous</button>
			       	</div> 
				</div>
			</div>	
		</form>
	</div>
