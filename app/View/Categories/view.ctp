	<?php echo $this->element('message'); ?>
	<div style="width:66%;float:left;">
		<h1 style="margin:  0 0 5px 0">Special Offers</h1>
		<?php if (!empty($sub_categories)) : ?>
			<h4>Sub categories</h4>
			<ul>
				<?php foreach ($sub_categories as $sub_category) : ?>
					<li><a href="<?php echo $this->Html->url('/categories/view/'. $sub_category['Category']['id']); ?>"><?php echo $sub_category['Category']['name']; ?></a></li>
				<?php endforeach; ?>
			</ul>
		<?php endif; ?>
		<div class="module">
			<div class="tl">
				<div class="tr">
					<div class="t">
						<?php echo $category['Category']['name']; ?>
					</div>
				</div>
			</div>
			<div class="content">
				<?php echo $this->element('special_offers'); ?>
				&nbsp;
			</div>
			<div class="bl">
				<div class="br">
					<div class="b">&nbsp;
					</div>
				</div>
			</div>
		</div>
	</div>
	<div style="float:left;width:33%;">
		<h3 style="margin: 1px 0 5px 15px;"><i><?php echo $category['Category']['name']; ?></i> search results</h3>
		<?php echo $this->element('products'); ?>
	</div>
	<div style="clear:both">&nbsp;</div>