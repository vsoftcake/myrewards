	<?php if ($programs) : ?>
	<form name="ProgramsClients"  id="ProgramsClients" method="post">
	<div class="list" style="width:600px">
		<div style="float:left;padding-right: 30px;">
			<table cellpadding="0" cellspacing="0" style="width:320px">
				<tr>
					<th>Name</th>
					<th>Enabled</th>
				</tr>
				<?php
				$i = 0;$count=0;
				foreach ($programs as $program):
					$class = null;
					if ($i++ % 2 == 0) {
						$class = ' class="altrow"';
					}
					$count++;
					if($count==12):
				?>
			</table>
		</div>
		<div>
			<table cellpadding="0" cellspacing="0" style="width:320px">
				<tr>
					<th>Name</th>
					<th>Enabled</th>
				</tr>
				<?php endif; ?> 
				<tr<?php echo $class;?>>
					<td>
						<a href="javascript:void(0)" onclick="return getClients(<?php echo $id; ?>, <?php echo $program['Program']['id']; ?>);"><?php echo $program['Program']['name'] ?></a>
					</td>
					<td style="text-align:center">
						<input type="hidden" name="data[ProgramsCategory][id][<?php echo $program['Program']['id']; ?>]" value="0" />
						<input type="checkbox" name="data[ProgramsCategory][id][<?php echo $program['Program']['id']; ?>]" value="1" <?php echo ($program['ProgramsCategory']['category_id'] != '')? 'checked="checked"': "";  ?> onclick="clientsOnOff(<?php echo $id; ?>, <?php echo $program['Program']['id']; ?>, this.checked);" class="checkbox" />
					</td>
				</tr>
				<?php endforeach; ?>
			</table>
			<button class="admin_button" type="submit" onclick="return updateProgramsClients();" id="ProgramsClientsButton">Submit</button>
		</div>
	</div>
	<br/>
	<div>
		<?php foreach ($programs as $program) : ?>
		<div class="list clients" id="clients<?php echo $program['Program']['id']; ?>"></div>
		<?php endforeach; ?>
	</div>
	</form>
	<script type="text/javascript">

	loaded_programs = new Array();

	window.getClients = function(category_id, program_id) {
		$('message').innerHTML = '';
		var divs =  document.getElementsByClassName('clients');
		for (var i=0; i >= divs.length; i++) {
			divs[i].style.display = 'none';
		}
		$('clients' + program_id).style.display = 'block';

		if(typeof(loaded_programs[program_id]) == 'undefined') {
			$('#clients' + program_id).html('Retrieving Clients...');
			$.ajax({
				url: '/admin/categories/ajax_program_clients/'+category_id+'/'+program_id,
				type: "GET",
				success: function (data) {
					$('#clients'+program_id).html(data);
					loaded_programs[program_id] = program_id;
				}
			});
		}

		return false;
	}

	window.clientsOnOff = function(category_id, program_id, on_off) {
		$('message').innerHTML = '';
		var divs =  document.getElementsByClassName('clients');
		for (var i in divs) {
			if (typeof(divs[i].style) != 'undefined') {
				divs[i].style.display = 'none';
			}
		}
		$('clients' + program_id).style.display = 'inline';
	
		/*
		var divs =  document.getElementsByClassName('clients');
		divs.each(function(div) {
			div.style.display = 'none';
		});
		$('clients' + program_id).style.display = 'inline';
		*/

		if(typeof(loaded_programs[program_id]) == 'undefined') {
			$('#clients' + program_id).html('Retrieving Clients...');
			$.ajax({
				url: '/admin/categories/ajax_program_clients/'+category_id+'/'+program_id,
				type: "GET",
				success: function (data) {
					$('#clients'+program_id).html(data);
					loaded_programs[program_id] = program_id;
					doClientsOnOff(program_id, on_off);
				}
			});
		} else {
			doClientsOnOff(program_id, on_off);
		}

	}

	window.doClientsOnOff = function(program_id, on_off) {

		var inputs = $('clients' + program_id).getElementsBySelector('input');

		inputs.each(function(input) {
			input.checked = on_off;
		});

	}

	window.updateProgramsClients = function() {
		$.ajax({
			url: '/admin/categories/ajax_programs_clients_update/<?php echo $id?>',
			type: "GET",
			success: function (data) {
				$('#message').html(data);
				$('ProgramsClientsButton').disabled = false;
			}
		});

		$('ProgramsClientsButton').disabled = true;
		$('message').innerHTML = '<div class="message">Updating Programs and Clients, this can take a while.</div>';

		return false;
	}

	</script>


	<?php endif; ?>
