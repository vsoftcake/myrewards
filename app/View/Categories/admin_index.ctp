<style>
	#content
	{
		border-left:1px solid #cccccc;
		border-right:1px solid #cccccc;
		border-bottom:1px solid #cccccc;
		margin-left:20px !important;
		padding-left:15px !important;
		width:916px;
	}
	.admin_button
	{
		height: 24px;
		padding-top: 2px;
		padding-bottom: 5px;
		border-radius: 5px 5px 5px 5px;
		background-color: #0066cc;
		border: 1px solid #cccccc;
		color: #FFFFFF; 
		cursor: pointer;
		font-size:13px;
	}
	.tr
	{
		height:30px;
	}
	.current
	{
	    font-family: 'Droid Sans',sans-serif;
	    font-size: 12px;
	   	padding:5px 8px !important;   
	    background-color: #0066CC;
	    border: 1px solid #DFDFDF;
	    color:#ffffff;
	}
	.cpagination1
	{
		/*padding:1px 2px 1px 2px;*/
		font-family: 'Droid Sans',sans-serif;
	    font-size: 12px;
		text-decoration:none !important;
		background-color: #FFFFFF;
		padding:3px 8px;
		border: 1px solid #0066CC;
		color:#808080 !important;
	}
</style>
<table style="margin-left:-2px;margin-top:10px;">
	<tr>
		<td>
            &nbsp;&nbsp;<img style="width:40px;height:40px;" src="/files/admin_home_icons/categories.png"  alt="Categories" title="Categories" />&nbsp;&nbsp;
        </td>
        <td> 
            <h2><?php echo "Categories";?></h2>
        </td>
        <td width="500"></td>
        <td>
	   		<button class ="admin_button" onclick="location.href='<?php echo $this->Html->url('add'); ?>';">Create New Category</button>	
      	</td>
    </tr>        
</table>
<div id="message"></div>
<table>
	<tr>
		<td valign="top" style="padding-right:15px;"> 
			<div class="categories">
				<?php echo $this->Tree->show('Category.description', $categories, null, '/admin/categories/index/'); ?>
			</div>
		</td>
		<td valign="top">
			<?php if  (isset($category)) : ?>
				<div id="category" style="border:1px solid #DFDFDF;min-height: 100px; overflow: hidden;padding:10px;">
					<table>
						<tr class="tr">
							<td>
								<h3 style="display:inline;"><?php echo $category['Category']['description']; ?></h3>
								<a href="<?php echo $this->Html->url('/admin/categories/edit/'. $category['Category']['id']); ?>">Edit</a>
							</td>
						</tr>
						<tr class="tr">
							<td>
								<h4 style="display:inline;">Public name: <?php echo $category['Category']['name']; ?></h4>
							</td>
						</tr>
						<tr class="tr">
							<td>
								<h5 style="display:inline;">Details: <?php echo $category['Category']['details']; ?></h5>
							</td>
						</tr>
					</table>
					
					<div class="list">
						<h4>Products</h4>&nbsp;&nbsp;&nbsp;&nbsp;<a href="<?php echo $this->Html->url('/admin/products/edit/category_id:'. $category['Category']['id']); ?>">Create product...</a>
						<div id="products" style="margin:0px 0px!important;">Retrieving Products...</div>
						<script type="text/javascript">
						$(function() {
							$.ajax({
								url: "/admin/categories/ajax_products/<?php echo $category['Category']['id'];?>",
								type: "GET",
								success: function (data) {
									$('#products').html(data);
								}
							});
						});</script>
					</div>
					
					<div class="list" style="width:600px">
						<h4>Programs</h4>
						<div id="programs">Retrieving Programs...</div>
						<script type="text/javascript">
						$(function() {
							$.ajax({
								url: "/admin/categories/ajax_programs/<?php echo $category['Category']['id'];?>",
								type: "GET",
								success: function (data) {
									$('#programs').html(data);
								}
							});
						});</script>
					</div>
				</div>
			<?php endif; ?>
		</td>
	</tr>
</table>