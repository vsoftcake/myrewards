<h4>Clients for <?php echo $program['Program']['name']; ?></h4>
	<?php if ($clients) : ?>
	<table cellpadding="0" cellspacing="0">
	<tr>
		<th>Name</th>
		<th>Enabled</th>
	</tr>
	<?php
	$i = 0;
	foreach ($clients as $client):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
		<tr<?php echo $class;?>>
			<td><?php echo $client['Client']['name'] ?></td>
			<td style="text-align:center">
				<input type="hidden" name="data[ClientsCategory][id][<?php echo $client['Client']['id']; ?>]" value="0"  />
				<input type="checkbox" name="data[ClientsCategory][id][<?php echo $client['Client']['id']; ?>]" value="1" <?php echo ($client['ClientsCategory']['category_id'] != '')? 'checked="checked"': "";  ?> class="checkbox" />
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
	<?php else : ?>
	<br/>No clients found
	<?php endif; ?>
	
	
