<style type="text/css">

	.rounded-corners 
	{
		-moz-border-radius: 10px 10px 0px 0px; /* Firefox */
		-webkit-border-radius: 10px 10px 0px 0px; /* Safari, Chrome */
		border-radius: 10px 10px 0px 0px; /* CSS3 */
	}
	.tr 
		{
			height:35px;
		}
	.admin_button
		{
		height: 24px;
		padding-bottom: 5px;
		border-radius: 5px 5px 5px 5px;
		background-color: #0066cc;
		border: 1px solid #cccccc;
		color: #FFFFFF; 
		cursor: pointer;
		font-size:13px;
	}
</style>
<div style="width:953px;border-left:1px solid #cccccc;border-right:1px solid #cccccc;border-bottom:1px solid #cccccc;margin-left:-14px;">
<div style="height: 40px; float:left; width: 608px;padding-left:37px;">
	<table style="margin-left:-2px;margin-top:10px;width:920px">
		<tr>
			<td>
	            &nbsp;&nbsp;<img title="Categories" alt="Categories" src="/files/admin_home_icons/categories.png" style="width:40px;height:40px;">&nbsp;&nbsp;
	        </td>
	        <td> 
	            <h2><?php echo "Create New Category";?></h2>
	        </td>
	        <td width="500"></td>
	        <td>
		   		<ul style="padding:20px;margin-left:-28px;">
					<li style="display:inline;"><?php echo $this->Form->button('Cancel', array('onclick' => "location.href='/admin/categories/'",
					'class'=>'admin_button','style'=>'width:60px;', 'type'=>'button'));  ?></li>
		        </ul>
	      	</td>
	    </tr>        
	</table>
 </div>

<br />
<div style="padding-top:30px;">
<form action="<?php echo $this->Html->url('/admin/categories/add/'); ?>" method="post" enctype="multipart/form-data">
    <div class="tabber">
			<div class="tabbertab" style="height:340px">
	  			<h2>General Information</h2>
	    		<h4>Category General Information</h4>
	     	   <div style="background-color:#EFEFEF;border:1px solid #cccccc;padding:10px;width:790px;min-height:250px;overflow:hidden">
	                 <table>
						<tr>
							<td><strong>Name</strong></td>
							<td><?php echo $this->Form->input('Category.name', array('div'=>false,'label'=>false)); ?></td>
							<td><?php echo $this->Form->error('Category.name', array('required' => 'Name required'), array('required' => 'Name required')); ?></td>
						</tr>
						<tr>
							<td><strong>Description</strong></td>
							<td><?php echo $this->Form->input('Category.description', array('div'=>false,'label'=>false)); ?></td>
							<td><?php echo $this->Form->error('Category.description', array('required' => 'Description required'), array('required' => 'Description required')); ?></td>
						</tr>
						<tr>
							<td><strong>Parent Category</strong></td>
							<td><?php echo $this->Form->input('Category.parent_id', array('div'=>false, 'label'=>false, 'type'=>'select', 'options'=>$categories, 'empty'=>'')); ?></td>
						</tr>
						<tr>
							<td><strong>Sort Order</strong></td>
							<td><?php echo $this->Form->input('Category.sort', array('div'=>false,'label'=>false, 'type'=>'text')); ?></td>
						</tr>
						<tr>
							<td><strong>Country</strong></td>
							<td><?php echo $this->Form->input('Category.countries', array('div'=>false, 'label'=>false, 'type'=>'select', 'options'=>$country_list)); ?></td>
						</tr>
						<tr>
							<td><strong>Search weight</strong></td>
							<td>
								<?php echo $this->Form->input('Merchant.search_weight', array('div'=>false, 'label'=>false, 'type'=>'select', 'options'=>$search_weights)); ?>
								<sub>Higher values appear first in search results</sub>
							</td>
						</tr>
						<tr>
							<td><strong>Logo</strong></td>
							<td>
								<a href="javascript:void(0)" onclick="this.style.display='none'; $('#CategoryLogoFile').show(); return false;">Upload Logo</a>
								<?php echo $this->Form->file('CategoryLogo.file', array('style' => 'display:none;')); ?><br/>
							</td>
						</tr>
					</table>
						
			   </div>
			    <br/>
	       	<div>
				<table>
					<tr>
						<td style="width:340px;"></td> 
						<td><button type="submit" class="admin_button">Submit</button></td>
					</tr>
				</table>
	       	</div> 
			</div>
    </div>			   		
	</form>
</div>
</div>
