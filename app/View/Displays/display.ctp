<script type="text/javascript">
count=1;
function nominateSubmit()
{
	if(document.getElementById('fname'+count))
	{
		var fname = document.getElementById('fname'+count).value;
		var lname = document.getElementById('lname'+count).value;
		var relation = document.getElementById('relation'+count).value;
		var email = document.getElementById('email'+count).value;
	}
	if(fname!="" && lname!="" && relation != "" && email != "")
	{
		$.post('/user_nominates/add', $('#addform').serialize(), function( data ) {
			$("#modalboxpopup").html(data).dialog({width: 500, modal: true, "position": "top", close: function() {window.location='/display/nominate_a_friend';}});
		});
	}
	else
	{
		alert("please fill up all the fields");
	}
}
Nominate = function(cnt) 
{
	count1=4-cnt;
	if(document.getElementById('fname'+count))
	{
		var fname = document.getElementById('fname'+count).value;
		var lname = document.getElementById('lname'+count).value;
		var relation = document.getElementById('relation'+count).value;
		var email = document.getElementById('email'+count).value;
	}
	if(fname=="" || lname=="" || relation == "" || email == "")
	{
		alert("please fill up all the fields");
	}
	else
	{
		if(count<=count1)
		{
			count=count+1;
			var html = '<div id="nominate'+count+'" style="width:295px;background-color:#00B9F2;-moz-border-radius:15px;border-radius: 15px;float:left;padding-left:4px;padding-top:10px;padding-right:4px;margin-right:4px;margin-left:2px;"><table>';
			html += '<tr><td width="85px"><p>Your Name: </p></td><td><input type="text" name="data[UserNominate]['+count+'][clientname]" id="fname'+count+'" class="map_input1 rounded_corners_button" value="<?php echo $this->Session->read('user.User.first_name').' '.$this->Session->read('user.User.last_name'); ?>"></td>';
			html += '</tr>';
			html += '<tr><td width="85px"><p>Family Member First Name: </p></td><td><input type="text" name="data[UserNominate]['+count+'][firstname]" id="fname'+count+'" class="map_input1 rounded_corners_button" /></td>';
			html += '</tr>';
			html += '<tr><td><p>Family Member Last Name: </p></td><td><input type="text" name="data[UserNominate]['+count+'][lastname]" id="lname'+count+'" class="map_input1 rounded_corners_button" size="10" /></td>';
			html += '</tr>';
			html += '<tr><td><p>Relationship:  </p></td><td><input type="text" name="data[UserNominate]['+count+'][relationship]" id="relation'+count+'" class="map_input1 rounded_corners_button" /></td>';
			html += '</tr>';
			html += '<tr><td><p>Email address: </p></td><td><input type="text" name="data[UserNominate]['+count+'][email]" id="email'+count+'" class="map_input1 rounded_corners_button"/></td>';
			html += '</tr><tr><td colspan="3">';
			html += '<br><div style="dashed #ffffff;border-top:1px dashed #ffffff;padding:5px;font-size:12px;"></div>';
			html += '<a href="#add" onclick="return Nominate('+cnt+');" ><span style="font-size:12px;">Add another family member</span></a>';
			html += '&nbsp;<button type="button" onclick="nominateSubmit()" class="map_button_style rounded_corners_button">Submit</button>';
			html += '&nbsp;<button type="button" onClick="return removeElement('+count+');" class="map_button_style rounded_corners_button">Remove</button><br><br></td></tr></table></div>';
			
		}
		else
		{
			alert('Free membership access to up to 4 other family members only. Your limit is over');
		}
		$( html ).insertAfter( "#nominate" );
		
		return false;
		
	}
}

function removeElement(id){var childDiv = "nominate"+id;
	if (document.getElementById(childDiv)) {     
          var child = document.getElementById(childDiv);
          var parent = document.getElementById("nominate");
          parent.removeChild(child);
          count=count-1;
     }
}
var counts=0;

function friendSubmit()
{	
	var gcount = parseInt(document.getElementById('count').value);
	var friendname = document.getElementById('friendname'+gcount).value;
	var friendemail = document.getElementById('friendemail'+gcount).value;
	
	if(friendname!="" && friendemail!="")
	{
		$.ajax({
			url: '/saf/saf_request',
			data: $('#Safrequestform').serialize(),
			type: 'POST',
			success: function(data){
				$("#modalboxpopup").html(data).dialog({modal: true, "position": "top"});
			}
		});
		return false;
	}
	else
	{
		alert("please provide your friends name and email");
	}
}

function addFriends(cnt)
{
	//converting cnt to int
	cnts = parseInt(cnt);
	//if total count and field value not equal then decreament field value
	if(cnts!=counts)
		counts--;
	//hidden total
	var gcount = parseInt(document.getElementById('count').value);
	var friendname = document.getElementById('friendname'+counts).value;
	var friendemail = document.getElementById('friendemail'+counts).value;
	if(friendname=="" || friendemail=="")
	{
		alert("please provide your friends name and email");
	}
	else
	{
		if(gcount<3)
		{
			counts=cnts+1;
			var html = '<div id="fid'+counts+'" style="float: left;">';
			html += '<table><tr><td>Friends name:</td>';
			html += '<td><input type="text" id="friendname'+counts+'" name="data[ProductsSaf]['+counts+'][friendname]" value="" class="search_input"/><span style="color:red"> * </span></td>';
			html += '<td>Friends email address:</td>';
			html += '<td><input type="text" id="friendemail'+counts+'" name="data[ProductsSaf]['+counts+'][friendemail]" value="" class="search_input"/><span style="color:red"> * </span></td>';
			html += '<td><button onclick="addFriends('+counts+')" type="button" class="button_search_module rounded_corners_button" style="width:50px">Add</button>';
			html += '</td><td><button type="button" onClick="return removeFriend('+counts+');" class="button_search_module rounded_corners_button" style="width:60px;margin-left:5px">Remove</button></td></tr></table></div>';
			$( html ).insertAfter( "#fid" );
			document.getElementById('count').value = gcount+1;
		}
		else
		{
			alert('Inviting friends limit is over');
		}
	}	
}
	
function removeFriend(cnt)
{
	var gcount = parseInt(document.getElementById('count').value);
	var childDiv = "fid"+parseInt(cnt);
	if (document.getElementById(childDiv)) {     
          var child = document.getElementById(childDiv);
          var parent = document.getElementById("fid");
          parent.removeChild(child);
          document.getElementById('count').value = gcount-1;
     }
}

</script>
<style>#MB_window{overflow:scroll !important}</style>

	<?php echo $this->element('module2', array('module2' => 'Keyword_search', 'title' => 'Search'));?>
	<div class="search_content search_results"  style="margin-bottom:0px !important;min-height:10px;overflow:hidden;">
			<?php echo $this->element('message'); ?>
			<div style="float:left;">
			<?php if ($page['ClientPage']['h1'] != '') : ?>
				<h1 style="padding-left:10px;margin-top:10px;"><?php echo $page['ClientPage']['h1']; ?></h1><br/>
			<?php endif; ?>
			<?php if ($page['ClientPage']['h2'] != '') : ?>
				<h2 style="padding-left:10px;margin-top:10px;"><?php echo $page['ClientPage']['h2']; ?></h2>
				<!-- <br/>-->
			<?php endif; ?>
			<?php if ($page['ClientPage']['h3'] != '') : ?>
				<h3 style="padding-left:10px;margin-top:10px;"><?php echo $page['ClientPage']['h3']; ?></h3><br/>
			<?php endif; ?>
			</div>
			
			<?php if($page['ClientPage']['page_id'] != 22 && $page['ClientPage']['page_id'] != 31 && $page['ClientPage']['page_id'] != 24) :?>
				<?php if ($page['ClientPage']['content'] != '') : ?>
					<div style="padding-left:10px;width:930px;padding-right:10px;min-height:20px;overflow:hidden;">
						<?php echo $page['ClientPage']['content']; ?>
					</div><br/>
				<?php endif; ?>
			<?php endif; ?> 
			
			<?php if ($page['ClientPage']['page_id'] == 24 && $page['ClientPage']['content_override'] == 0) : ?>
				<?php if ($page['ClientPage']['content'] != '') : ?>
					<div style="padding-left:10px;width:930px;padding-right:10px;min-height:20px;overflow:hidden;"><?php echo $page['ClientPage']['content']; ?></div><br/>
				<?php endif; ?>
			<?php elseif ($page['ClientPage']['page_id'] == 24 && $page['ClientPage']['content_override'] == 1): ?>
				<div style="padding-left:10px;width:930px;padding-right:10px;min-height:20px;overflow:hidden;">
					<table cellspacing="0" cellpadding="0" border="0" style="width: 85%;">
						<tr>
							<td>
								<p><span style="font-size: small;">If you get stuck or want to know more information on any item in this website, simply call customer service on <strong>1300 368 846</strong>.</span><br><br><span style="font-size: small;">All the services including the customer service operate 9am to 5pm Monday to Friday Eastern Standard Time. If you call after hours, please leave a message and we will get back to as soon as possible on the next working day.</span><br><br><span style="font-size: small;">You can also send an email request or feedback to <a href="goto:support@therewardsteam.com">support@therewardsteam.com</a></span></p>
								<p><img width="100%" height="100%" border="0" src="http://www.myrewards.com.au/img/myrewards/img/GuySurfingNet.png"></p>
							</td>
						</tr>
					</table>
				</div>				
			<?php endif; ?>
			<?php if ($page['ClientPage']['page_id'] == 22 && $page['ClientPage']['content_override'] == 0) : ?>
				<?php if ($page['ClientPage']['content'] != '') : ?>
					<div style="padding-left:10px;width:930px;padding-right:10px;min-height:20px;overflow:hidden;"><?php echo $page['ClientPage']['content']; ?></div><br/>
				<?php endif; ?>
			<?php elseif ($page['ClientPage']['page_id'] == 22 && $page['ClientPage']['content_override'] == 1): ?>
				<div style="min-height: 20px; overflow: hidden; width: 925px; padding-left: 10px; padding-bottom: 20px;">
					<table width="100%" style="padding:10px;">
						<tr>
							<td valign="top" width="600px" style="padding-top:5px;padding-bottom:5px;padding-left:5px;padding-right:10px;">
								<p>
									<span style="font-size:12px;">
											My Rewards has 1000's of offers from all over the globe and every merchant offer has a redemption method.
											It's easy to follow but if you get stuck here are a few pointers so to assit you we would like to explain our icons
											and terms used througout the site.
									</span>
								</p>
								<p>
									<span style="font-size:12px;">
											Our icons and terms are very simple but just in case you need some assistance the following explains each.
									</span>
								</p>
								<p>
									<image src="/files/newimg/how_it_works.png"/>
								</p>
								<br>
								<div class="how_it_works" style="background-color:#00B9F2;-moz-border-radius:15px;border-radius: 15px;padding-bottom:8px;padding-top:14px;padding-left: 10px;padding-right:10px; min-height: 200px;overflow:hidden;">
									<table style="margin: 10px 10px 10px 15px;">
										<tr>
											<td valign="top" style="padding:5px;width:80px;">
												<div style="background-color:#ffffff;text-align: center; width: 100px; height: 65px;" class="rounded_corners_map">
													<image src="/files/styles/icon_images/voucher.png" height="50" width="50" style="padding: 10px 5px;"/>
												</div>
											</td>
	
											<td valign="top" style="padding:5px;width:156px;">
												<span style="color:white;font-size:12px;"><strong>Web Coupon:</strong>
												When this icon is displayed the offer is available as a coupon you can print to redeem in-store.
												</span>
											</td>
	
											<td valign="top" style="padding:5px;">
												<div style="background-color:#ffffff;text-align: center; width: 100px; height: 65px;" class="rounded_corners_map">
													<image src="/files/styles/icon_images/print.png" height="50" width="50" style="padding: 10px 5px;"/>
												</div>
											</td>
	
											<td valign="top" style="padding:5px;">
												<span style="color:white;font-size:12px;"><strong>Print Coupon:</strong>
												Click this icon to print the coupon.
												</span>
											</td>
										</tr>
										<tr>
											<td valign="top" style="padding:5px;">
												<div style="background-color:#ffffff;text-align: center; width: 100px; height: 65px;" class="rounded_corners_map">
													<image src="/files/styles/icon_images/card.png" height="50" width="50" style="padding: 10px 5px;"/>
												</div>
											</td>
	
											<td valign="top" style="padding:5px;">
												<span style="color:white;font-size:12px;"><strong>Show & Save:</strong>
												When this icon is displayed, you can show your membership card at the retailer to redeem the nominated offer.
												</span>
											</td>
	
											<td valign="top" style="padding:5px;">
												<div style="background-color:#ffffff;text-align: center; width: 100px; height: 65px;" class="rounded_corners_map">
													<image src="/files/styles/icon_images/mobile.png" height="50" width="50" style="padding: 10px 5px;"/>
												</div>
											</td>
											<td valign="top" style="padding:5px;">
												<span style="color:white;font-size:12px;"><strong>Mobile Reward:</strong>
												When this icon is displayed the offer can be redemmed as a Mobile Coupon via the app Grab it Now!
												</span>
											</td>
										</tr>
										<tr>
											<td valign="top" style="padding:5px;">
												<div style="background-color:#ffffff;text-align: center; width: 100px; height: 65px;" class="rounded_corners_map">
													<image src="/files/styles/icon_images/shop.png" height="50" width="50" style="padding: 10px 5px;"/>
												</div>
											</td>
											<td valign="top" style="padding:5px;">
												<span style="color:white;font-size:12px;"><strong>Shop Online:</strong>
												Click this icon to shop online and use the Promo Code listed in the offer description.
												</span>
											</td>
	
											<td valign="top" style="padding:5px;">
												<div style="background-color:#ffffff;text-align: center; width: 100px; height: 65px;" class="rounded_corners_map">
													<image src="/files/styles/icon_images/link.png" height="50" width="50" style="padding: 10px 5px;"/>
												</div>
											</td>
	
											<td valign="top" style="padding:5px;">
												<span style="color:white;font-size:12px;"><strong>Web Link:</strong>
												Click this icon to be taken directly to the merchant's website.
												</span>
											</td>
										</tr>
										<tr>
											<td valign="top" style="padding:5px;">
												<div style="background-color:#ffffff;text-align: center; width: 100px; height: 65px;" class="rounded_corners_map">
													<image src="/files/styles/icon_images/more_info.png" height="50" width="50" style="padding: 10px 5px;"/>
												</div>
											</td>
	
											<td valign="top" style="padding:5px;">
												<span style="color:white;font-size:12px;"><strong>More Icon:</strong>
												Click this icon to find out more about the merchant and offer.
												</span>
											</td>
	
											<td valign="top" style="padding:5px;">
												<div style="background-color:#ffffff;text-align: center; width: 100px; height: 65px;" class="rounded_corners_map">
													<image src="/files/styles/icon_images/wishlist.png" height="50" width="50" style="padding: 10px 5px;"/>
												</div>
											</td>
	
											<td valign="top" style="padding:5px;">
												<span style="color:white;font-size:12px;"><strong>My Favourite's:</strong>
												Click this icon for the coupon offer to be added to your favourites. You can then print all coupons
												off together (up to 5 per page).
												</span>
											</td>
										</tr>
									</table>
								</div>
							</td>
							<td valign="top" width="360px">
								<div style="background-color:#fff;-moz-border-radius:15px;border-radius: 15px;padding:10px;" class="how_it_works">
	
									<!--<span class="how_it_works_color" style="color:#E61873;font-size:19px;">-->
										<h2>Terms you will often see throughout the site: </h2>
									<!--</span>-->
									<p>
										<span style="font-size:12px;">
											<strong>View locations:</strong> Offers are spread around the world. Remember to click
											"more locations" to find the one close to you.
										</span>
									</p>
									<p>
										<span style="font-size:12px;">
											<strong>My Favourites:</strong> This option allows you to save
											a number of favourite coupons in the one place
											and then print them all on one or more sheets of
											paper. You can save up to 5 coupons per page.
										</span>
									</p>
									<p>
										<span style="font-size:12px;">
											<strong>Reward Me:</strong> This outlines how you
											redeem the offer. Redemption
											instructions will be included here.
										</span>
									</p>
									<p>
										<span style="font-size:12px;">
											<strong>Book Only Offer:</strong> This refers to offers that are
											only available in an original printed coupon
											book. They cannot be redeemed using an online
											coupon, mobile coupon or show & save offer.
										</span>
									</p>
									<p>
										<span style="font-size:12px;">
											<strong>Mobile Reward:</strong> Some of the offers
											available can be redeemed by downloading
											a mobile coupon through the app Grab it
											Now! on your smart mobile phone.
										</span>
									</p>
									<p>
										<span style="font-size:12px;">
											<strong>Merchant:</strong> The name of the supplier
											offering a discount and/or special offer.
										</span>
									</p>
									<p>
										<span style="font-size:12px;">
											<strong>E-newsletter:</strong> An electronic newsletter
											sent to your nominated email address
											when you sign-up to receive this.
										</span>
									</p>
									<p>
										<span style="font-size:12px;">
											<strong>5-4-1:</strong> This application allows you to invite up
											to 4 family members to join the online rewards
											program and access the savings themselves.
											No more sharing your login details!
										</span>
									</p>
									<p>
										<span style="font-size:12px;">
											<strong>Dashboard:</strong> The homepage with updated special
											offers, offer of the week and the map search.
										</span>
									</p>
									<p>
										<span style="font-size:12px;">
											<strong>Print Limit Exceeded:</strong> This appears when you try
											to print a coupon offer you have printed too many
											times. We want you to use the program as much
											as you like however, some merchant have limits
											on the number of coupons that can be redeemed
											by individuals. If this appears we are pretty sure
											there will be another similar offer you can use :)
										</span>
									</p>
	
								</div>
	
							</td>
						</tr>
					</table>
				</div>
			<?php endif;?>
			
			<?php if ($page['ClientPage']['page_id'] == 31 && $page['ClientPage']['content_override'] == 0) : ?>
				<?php if ($page['ClientPage']['content'] != '') : ?>
					<div style="padding-left:10px;width:930px;padding-right:10px;min-height:20px;overflow:hidden;"><?php echo $page['ClientPage']['content']; ?></div><br/>
				<?php endif; ?>
			<?php elseif ($page['ClientPage']['page_id'] == 31 && $page['ClientPage']['content_override'] == 1): ?>
				<div style="min-height: 20px; overflow: hidden; width: 945px; padding-left: 5px; padding-bottom: 20px;">
				    <?php if ($count[0][0]['count'] <4 ): ?>
							<form  method="post" action="/user_nominates/add" name="addform" id="addform">
							
								<table border="0" cellpadding="0" cellspacing="0"  style="margin-left:5px;">
									<tbody>
										<tr>
											<td valign="top" width="300px">
											
													<image src="/files/inviteafriend/imf_left.png" width="300"/>
											</td>								
										
											<td valign="top" width="310px" style="padding-top: 7px; padding-right: 5px; padding-left: 5px;">
												<table>
													<tr>
													<!--added program condion-->
													<?php 													
													 if($this->Session->read('client.Client.program_id')==31){ ?>
														<td valign="top" width="310px">
															<image src="/files/inviteafriend/imf_middle1.png" height="140" width="300"/>
														</td>
													<?php }else{  ?>
														<td valign="top" width="310px">
															<image src="/files/inviteafriend/imf_middle.png" height="140" width="300"/>
														</td>														
														<?php } ?>
														
													</tr>
													<tr>
														<td>
															<p style="padding-top: 10px;">
																<span style="color:#00B9F2;font-size:19px;padding-top:6px;">
																	<strong>How does your family get a FREE membership ?</strong>
																</span>
															</p>
															<p>
																<span style="font-size:12px;line-height: 21px;">
																	Simply enroll up to 4 immediate family members in the form provided along with their email address
																	and we will send them an invite to join My Rewards Family program for FREE. Once they accept they
																	will then be able to log in, and access the offered range of discounts applicable to that membership*.  
																	They will be able to print coupons, purchase selected products online, and sign up to monthly e-news
																	that showcases new and special offers and more!
																	
																</span>
															</p>
															<p>
																<span style="font-size:10px;line-height: 20px;">* 
																	The services offered vary from program to program and no guarantee is made that the Family Membership products,
																	services and content is the same as the primary members program offering. 
																</span>
															</p>
														</td>
													</tr>
												</table>
											</td>
										
											<td valign="top" width="310px" style="padding-top: 7px;">
												<div style="background-color:#00B9F2;-moz-border-radius:15px;border-radius: 15px;">
													<table>
													  <tr>
														  <td colspan="3" style="padding: 10px;">
														  <span style="color:white;font-size:20px;"><strong>What are you waiting for?</strong></span><br>
														  <span style="color:white;font-size:13px;"><strong>Reward the ones you love now!</strong></span><br>
														  <p><span style="font-size:13px;">Complete the form, press submit and they will receive an instant invitation to join.</span></p>
														  
														  <div style="border-bottom:1px dashed #ffffff;border-top:1px dashed #ffffff;padding:10px;font-size:13px;width:265px;">Your Remaining invitations left : 
															  <div align="center" style="width:50px;padding:2px; background-color:white;-moz-border-radius:7px;border-radius:7px;float:right">
																<strong>
																	<?php echo 4-$count[0][0]['count']; ?>
																</strong>
															  </div>
														  </div>
														  </td>
													  </tr>
													  </table>
													  <table style="margin-left:10px;">
													  <tr>
													    <td width="100px"><p>Your Name: </p></td>
													    <?php  if (isset($_SERVER['HTTP_USER_AGENT']) && (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 7.0') !== false)) : ?>
													    <td><?php echo $this->Form->input('UserNominate.0.clientname',array('div'=>false,'label'=>false,'id'=>'clientname1','class'=>'invite_family_textbox rounded_corners_button','style'=>'width:130px !important;', 'value'=>$this->Session->read('user.User.first_name'). ' '. $this->Session->read('user.User.last_name'))); ?></td>
													    <?php else : ?>
													     <td><?php echo $this->Form->input('UserNominate.0.clientname',array('div'=>false,'label'=>false,'id'=>'clientname1','class'=>'invite_family_textbox rounded_corners_button','value'=>$this->Session->read('user.User.first_name'). ' '. $this->Session->read('user.User.last_name'))); ?></td>
													    <?php endif;?>
													    <td><?php echo $this->Form->error('Usernominate.0.clientname', array('required' => 'Your Name required'), array('required' => 'Your Name required')); ?></td>
													  </tr>
													  <tr>
													    <td><p>Family Member First Name: </p></td>
													    <?php  if (isset($_SERVER['HTTP_USER_AGENT']) && (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 7.0') !== false)) : ?>
													    	<td><?php echo $this->Form->input('UserNominate.0.firstname',array('div'=>false,'label'=>false,'id'=>'fname1','class'=>'invite_family_textbox rounded_corners_button','style'=>'width:130px !important;')); ?></td>
													    <?php else: ?>
													    	<td><?php echo $this->Form->input('UserNominate.0.firstname',array('div'=>false,'label'=>false,'id'=>'fname1','class'=>'invite_family_textbox rounded_corners_button')); ?></td>
													    <?php endif; ?>
														<td><?php echo $this->Form->error('Usernominate.0.firstname', array('required' => 'First Name required'), array('required' => 'First Name required')); ?></td>
													  </tr>
													  <tr>
													    <td><p>Family Member Last Name: </p></td>
													    <?php  if (isset($_SERVER['HTTP_USER_AGENT']) && (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 7.0') !== false)) : ?>
													    	<td><p><?php echo $this->Form->input('UserNominate.0.lastname',array('div'=>false,'label'=>false,'id'=>'lname1','class'=>'invite_family_textbox rounded_corners_button','style'=>'width:130px !important;')); ?></p></td>
													    <?php else : ?>
													    	<td><p><?php echo $this->Form->input('UserNominate.0.lastname',array('div'=>false,'label'=>false,'id'=>'lname1','class'=>'invite_family_textbox rounded_corners_button')); ?></p></td>
													    <?php endif; ?>
													    <td><?php echo $this->Form->error('Usernominate.lastname', array('required' => 'Last Name required'), array('required' => 'Last Name required')); ?></td>
													  </tr>
													  <tr>
													    <td><p>Relationship: </p></td>
													    <?php  if (isset($_SERVER['HTTP_USER_AGENT']) && (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 7.0') !== false)) : ?>
													    	<td><p><?php echo $this->Form->input('UserNominate.0.relationship',array('div'=>false,'label'=>false,'id'=>'relation1','class'=>'invite_family_textbox rounded_corners_button','style'=>'width:130px !important;')); ?></p></td>
													    <?php else : ?>
													    	<td><p><?php echo $this->Form->input('UserNominate.0.relationship',array('div'=>false,'label'=>false,'id'=>'relation1','class'=>'invite_family_textbox rounded_corners_button')); ?></p></td>
													    <?php endif; ?>
													    <td><?php echo $this->Form->error('Usernominate.relationship', array('required' => 'Last Name required'), array('required' => 'Last Name required')); ?></td>
													  </tr>
													  <tr>
													    <td><p>Email address: </p></td>
													    <?php  if (isset($_SERVER['HTTP_USER_AGENT']) && (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 7.0') !== false)) : ?>
													    	<td><p><?php echo $this->Form->input('UserNominate.0.email',array('div'=>false,'label'=>false,'id'=>'email1','class'=>'invite_family_textbox rounded_corners_button','style'=>'width:130px !important;')); ?></p></td>
													    <?php else : ?>
													    	<td><p><?php echo $this->Form->input('UserNominate.0.email',array('div'=>false,'label'=>false,'id'=>'email1','class'=>'invite_family_textbox rounded_corners_button')); ?></p></td>
													    <?php endif; ?>
													    <td><?php echo $this->Form->error('Usernominate.email', array('valid_email' => 'Invalid email address'), array('valid_email' => 'Invalid email address')); ?></td>
													  </tr>
													  <tr>
														  <td colspan="3">
														   <div style="dashed #ffffff;border-top:1px dashed #ffffff;padding:10px;font-size:13px;"></div>
														  <a style="padding-left:15px;" href="#add" onclick="return Nominate(<?php echo $count[0][0]['count']+1; ?> );" ><span style="font-size:13px;">Add another family member</span></a>
														  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
														  <button type="button" onclick="nominateSubmit()" class="map_button_style rounded_corners_button">Submit</button><br><br>
														  </td>
													  </tr>
													  </table>
												</div>
											</td>
									 </tr>
									 <tr><td colspan="3"><div id="nominate"></div></td></tr>
								</tbody>
									  
							</table>
						<?php endif; ?>
					</form>
			
					<?php if ($count[0][0]['count'] >=4): ?>
						<?php echo "<center><strong>Free membership access to up to 4 other family members only.Your Limit is Over</strong></center>"; ?>
					<?php endif; ?>
				<br>	
			<!--</div>-->
		</div>
	<?php endif;?>
	</div>

	<?php if (isset($new_products)) : ?>
		<div class="module" style="width:952px;">
			<div class="rounded-corners">	
				<div class="content" style="overflow:hidden">
					<?php echo $this->element('special_offers_new', array('special_offers' => $new_products,'new_products'=>'1')); ?>
				</div>
				<div class="b rounded-corners_bottom">&nbsp;</div>
			</div>	
		</div>
	<?php endif; ?>
	
	<?php if($page['ClientPage']['page_id']!=34) : ?>
		<?php if ($page['ClientPage']['special_offers'] == '1') : ?>
			<div class="module" style="width:952px;">
				<div class="rounded-corners">
					<div class="content" style="overflow:hidden">
						<?php echo $this->element('special_offers_new'); ?>
					</div>
					<div class="b rounded-corners_bottom">&nbsp;</div>
				</div>
			</div>
		<?php endif; ?>
	<?php endif; ?>
	<?php if($page['ClientPage']['page_id']!=22) : ?>
		<?php if (isset($page['ClientPage']['whats_new_module']) && $page['ClientPage']['whats_new_module'] == '1') : ?>
			<div class="module" style="width:952px;">
				<div class="rounded-corners">
					<div class="content" style="overflow:hidden">
						<?php echo $this->element('special_offers_new', array('special_offers' => $whats_new_offers)); ?>
					</div>
					<div class="b rounded-corners_bottom">&nbsp;</div>
				</div>
			</div>
		<?php endif; ?>
	<?php endif; ?>


<?php if($page['ClientPage']['page_id']==62): ?>
	<div style="margin-bottom:0px !important;" class="search_content search_results">
		<?php echo $this->element('my-savings-calculator'); ?>
	</div>
<?php endif;?>