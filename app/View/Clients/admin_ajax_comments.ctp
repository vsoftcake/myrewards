<table cellpadding="0" cellspacing="0" style="width: 100%;">
	<tbody id="comments_details">
	<tr>
		<th style="width:5%;background-color:#cccccc;text-align: left;width: 180px;border-left:1px solid #969090;border-bottom:1px solid #969090;border-top:1px solid #969090;">Date</th>
		<th style="width:5%;background-color:#cccccc;text-align: left;width: 120px;border-bottom:1px solid #969090;border-top:1px solid #969090;">User</th>
		<th style="width:20%;background-color:#cccccc;text-align: left; width: 400px;border-bottom:1px solid #969090;border-top:1px solid #969090;">Description</th>
		<th style="width:3%;background-color:#cccccc;text-align: left;width: 150px;border-right:1px solid #969090;border-bottom:1px solid #969090;border-top:1px solid #969090;">Actions</th>
	</tr>
	<tr id="NewClientCommentSaving" style="display:none;" class="altrow">
		<td colspan="4">Saving comment...</td>
	</tr>

	<tr id="NewClientComment" style="display:none" class="altrow">
				<td>
					<?php echo date('D, M j Y', time()); ?>
				</td>
				<td ><?php echo $this->Session->read('user.User.first_name'); ?> <?php echo $this->Session->read('user.User.last_name'); ?> </td>
				<td>
					<?php echo $this->Form->input('description', array('div'=>false, 'label'=>false,'id'=>'ClientCommentDescription','name'=>'data[ClientComment][description]')); ?>
					<span id="ClientCommentDescriptionError"></span>
				</td>
				<td class="actions" style="border-bottom:0">
						<a href="javascript:void(0)" onclick="return saveComment();">Save</a> |
						<a href="javascript:void(0)" onclick="return cancelAddComment();">Cancel</a>
				</td>
			</tr>
			<tr id="NewClientComment2" style="display:none"  class="altrow">
				<td>Notes:</td>
				<td colspan="3">
					<textarea name="data[ClientComment][notes]" style="width: 99%" rows="" id="ClientCommentNotes"></textarea>
				</td>
		</tr>
	<?php 	foreach ($comments as $comment): 
	$i = 0;
	

		$class = ' class="altrow"';
		if ($i++ % 2 == 0) {
			$class = null;
		}
	?>
		<tr<?php echo $class;?>>
			<td style="white-space:nowrap;">
				<?php echo date('D, M j Y, g:ia', strtotime($comment['ClientComment']['created'])); ?>
			</td>
			<td style="white-space:nowrap;">
				<?php echo ($comment['User']['id'] != '')? $comment['User']['first_name']. ' '. $comment['User']['last_name'] : $comment['ClientComment']['username']; ?>
			</td>
			<td>
				<a href="#" onclick="return displayNotes('ClientComment_<?php echo $comment['ClientComment']['id'] ?>');"><?php echo $comment['ClientComment']['description'] ?></a>
			</td>
			<td class="actions">
				<?php if (isset($comment['User']) && $comment['User']['id'] == $this->Session->read('user.User.id') && strtotime($comment['ClientComment']['modified']) >= time() - 1800) : ?>
					<a href="javascript:void(0)" onclick="return removeComment(<?php echo $comment['ClientComment']['id']; ?>);">Remove</a>
				<?php endif; ?>
			</td>
		</tr>
		<tr<?php echo $class;?> id="ClientComment_<?php echo $comment['ClientComment']['id']; ?>" style="display:none">
			<td></td>
			<td>Notes:</td>
			<td colspan="2"><?php echo str_replace("\n", '<br/>', $comment['ClientComment']['notes']); ?></td>
		</tr>
		
	
	<?php endforeach; ?>
	</tbody>
</table>
<br/>
<table style="border:none !important">
	<tr id="NewClientCommentLink">
		<td colspan="4" style="border:none !important;"><button  class="admin_button" onclick="return addComment();" type="button">Add Comment</button></td>
	</tr>
</table>
<div id="CommentUpdateResults"></div>
<script type="text/javascript">
displayNotes = function(element) {
	
	$('#'+element).toggle();	
	
	return false;
}

addComment = function() {
	$('#NewClientCommentLink').hide();
	$('#NewClientComment').show();
	$('#NewClientComment2').show();
	$('#ClientCommentDescription').focus();
	return false;
}

cancelAddComment = function() {
	$('#NewClientCommentLink').show();
	$('#NewClientComment').hide();
	$('#NewClientComment2').hide();
	return false;
}

saveComment = function() {

	$('#NewClientComment').hide();
	$('#NewClientComment2').hide();
	$('#NewClientCommentSaving').show();
	
	
	var desc = document.getElementById('ClientCommentDescription').value;
	var notes = document.getElementById('ClientCommentNotes').value;
		
		
	$.ajax({
		url: '/admin/clients/ajax_comment_save/<?php echo $id?>/'+desc+'/'+notes,
		type: "GET",
		parameters: { 
			'data[ClientComment][description]': desc, 
			'data[ClientComment][notes]': notes
			},
		success: function (data) {
		
			$('#ClientCommentDescriptionError').html(data);
		}
	});
	
	
	
	
	
	return false;
}

removeComment = function(id) {
	$.ajax({
		url: '/admin/clients/ajax_comment_delete/<?php echo $id?>/'+id,
		type: "GET",
		success: function (data) {
			$('#CommentUpdateResults').html(data);
		}
	});
	return false;
}
</script>