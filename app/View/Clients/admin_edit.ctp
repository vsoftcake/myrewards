<style type="text/css">

	.rounded-corners 
	{
		-moz-border-radius: 10px 10px 0px 0px; /* Firefox */
		-webkit-border-radius: 10px 10px 0px 0px; /* Safari, Chrome */
		border-radius: 10px 10px 0px 0px; /* CSS3 */
	}
	.tr 
		{
			height:35px;
		}
	.admin_button
		{
		height: 24px;
		padding-bottom: 5px;
		border-radius: 5px 5px 5px 5px;
		background-color: #0066cc;
		border: 1px solid #cccccc;
		color: #FFFFFF; 
		cursor: pointer;
		font-size:13px;
	}
	.tr 
	{
		height:35px;
	}
	.checkbox input
	{
		width:10px;
		border:none !important;	
	}
	.checkbox
	{
		padding:14px;
	}
</style>
<?php echo $this->element('tinymce');?>
<?php echo $this->Html->script('jscolor'); ?>
<script type="text/javascript">
	document.write('<style type="text/css">.tabber{display:none;}<\/style>');
	function confirmation(name,id)
    {
		var answer = confirm("Are you sure you want to delete client '<?php echo $this->Form->value('Client.name') ?>'")
		if (answer)
		{
			location.href="/admin/clients/delete/<?php echo $this->Form->value('Client.id') ?>";
		}
    }
	function dispFBShare (it, box) {
		var vis = (box.checked) ? "block" : "none";
		document.getElementById(it).style.display = vis;
	}
</script>

<form action="<?php echo $this->Html->url('/admin/clients/edit/'. $this->Html->value('Client.id')); ?>" method="post" id="edit_form" enctype="multipart/form-data">


<div style="width:953px;border-left:1px solid #cccccc;border-right:1px solid #cccccc;border-bottom:1px solid #cccccc;margin-left:-14px;">
<div id="message"></div>
		<div style="width:949px;height:50px;">
			<div style=" height: 40px; float:left; width: 608px;padding-left:37px;">
		       	<div style=" height: 40px; float:left;padding-top:5px;">
		        	<img title="Clients" alt="Clients" src="/files/admin_home_icons/clients.png" style="width:40px;height:40px;">
		        </div>
		        <div style=" height: 40px;float:left;padding-left:10px;">
		        	<h2>Edit Client <?php if($this->Form->value('Client.name')!=''):?>- <?php echo $this->Form->value('Client.name') ?><?php endif;?></h2>
		        </div> 
			</div>
	        <div class="actions" style=" height: 40px; float:left;  width: 300px;">
				<ul style="padding:20px;margin-left:-28px;">
					<li style="display:inline;"><?php echo $this->Form->button('Cancel', array('onclick' => "location.href='".$this->Session->read('redirect')."'",
					'class'=>'admin_button','style'=>'width:60px;', 'type'=>'button'));  ?></li>
					<?php if($this->Session->read('user.User.type') != 'Client Admin' && $this->Session->read('user.User.type') != 'Country Admin') :?>
						<?php if($this->Form->value('Client.id')!=''):
						
						$name=$this->Form->value('Client.name');$id=$this->Form->value('Client.id');?>
						
							<li style="display:inline;">
						<button class="admin_button" style="width:50px" type="submit" name="copy">Copy</button>
					</li>
						
							<li style="display:inline;"><?php echo $this->Form->button('Delete', array('onclick' => "confirmation('$name','$id')",'style'=>'','class'=>'admin_button','type'=>'button'));  ?></li>
			        	<?php endif;?>
			        <?php endif; ?>
					
		        	<li style="display:inline;">
		        		<button class="admin_button" type="submit">Submit</button>
		        	</li>
		        </ul>
			</div>
		</div>

		<?php echo $this->Form->input('Client.id', array('type' => 'hidden')); ?>
		
		<div class="tabber" id="mytab1">
			<div class="tabbertab"  style="min-height:600px;overflow:hidden;">
	  			<h2>General Info</h2>
	    		<h4>Client General Info</h4>
	     			<div style="background-color:#EFEFEF;border:1px solid #cccccc;padding:10px;width:790px;min-height:475px;overflow:hidden">
	      				<div style="width:850px; margin:0 auto;float:left">
			 				<div style=" height: 175px; float:left; width: 400px;">
		 					<table>
								<tr  class="tr">
									<td class="bt" style="width:140px"><strong>Name</strong></td>
									<td class="bt"><?php echo $this->Form->input('Client.name', array('div'=>false,'label'=>false)); ?></td>
									<td class="bl"><?php echo $this->Form->error('Client.name', array('required' => 'Name required', 'unique' => 'Name already in use'), array('required' => 'Name required')); ?></td>
								</tr>
								
					<!--<tr  class="tr">
									<td class="bt" style="width:140px"><strong>id</strong></td>
									<td class="bt">--><?php //echo $this->Form->value('Client.id'); 
									echo $this->Form->input('cid',array('type'=>'hidden','value'=>$cid));
									
									?><!--</td>
								
								</tr>	-->		
								
								
								<tr  class="tr">
									<td><strong>Description</strong></td>
									<td class="br"><?php echo $this->Form->input('Client.description', array('type' => 'text', 'div'=>false, 'label'=>false)); ?></td>
								</tr>
								<tr  class="tr">
									<td><strong>Email</strong></td>
									<td><?php echo $this->Form->input('Client.email', array('div'=>false,'label'=>false)); ?></td>
									<td class="bl"><?php echo $this->Form->error('Client.email', array('valid_email' => 'Invalid email address'), array('valid_email' => 'Invalid email address')); ?></td>
								</tr>
								<tr  class="tr">
									<td><strong>Username</strong></td>
									<td><?php echo $this->Form->input('Client.username', array('div'=>false,'label'=>false)); ?></td>
									<td class="bl"><?php echo $this->Form->error('Client.username', array('required' => 'Username required', 'unique' => 'Username alread in use'), array('required' => 'Username required')); ?></td>
								</tr>
								<tr  class="tr">
									<td><strong>Password</strong></td>
									<td><?php echo $this->Form->input('Client.password', array('type' => 'text','div'=>false,'label'=>false)); ?></td>
									<td class="bl"><?php echo $this->Form->error('Client.password', array('required' => 'Password required'), array('required' => 'Password required')); ?></td>
								</tr>
								<tr  class="tr">
									<td><strong>Temporary User Password</strong></td>
									<td><?php echo $this->Form->input('Client.temp_password', array('type' => 'text','div'=>false,'label'=>false)); ?></td>
									<td class="bl"></td>
								</tr>
							</table>
		 				</div>
						<div style=" height: 215px; float:left; width: 400px;">
							<table>
								<tr  class="tr">
							       <td style="width:120px"><strong>Copyright Text</strong></td>
							       <td ><?php echo $this->Form->input('Client.copyright_text', array('div'=>false,'label'=>false)); ?>
							       &nbsp;&nbsp;<?php echo $this->Form->checkbox('Client.copyright_disabled'); ?>&nbsp;Remove</td>
						       </tr>
						       <tr  class="tr">
							       <td><strong>Facebook Link</strong></td>
							       <td ><?php echo $this->Form->input('Client.facebook_link', array('div'=>false,'label'=>false)); ?>
							       &nbsp;&nbsp;<?php echo $this->Form->checkbox('Client.facebook_disabled'); ?>&nbsp;Remove</td>
						       </tr>
						       <tr  class="tr">
							       <td><strong>Twitter Link</strong></td>
							       <td ><?php echo $this->Form->input('Client.twitter_link', array('div'=>false,'label'=>false)); ?>
							       &nbsp;&nbsp;<?php echo $this->Form->checkbox('Client.twitter_disabled'); ?>&nbsp;Remove</td>
			               		</tr>
			               		<tr  class="tr">
									<td><strong>Default Country</strong></td>
									<td class="br"><?php echo $this->Form->input('Client.country', array('type'=>'select', 'options'=>$defaultcountry, 'div'=>false, 'label'=>false)); ?></td>
								</tr>
								<tr  class="tr">
									<td valign="top" style="padding-top:5px;"><strong>Client Country</strong></td>
									<td>
										<?php echo $this->Form->input('Country.Country', array('multiple' => 'multiple',  'empty' => ' ', 'div'=>false, 'label'=>false)); ?>
										<input type="hidden" name="data[Country][Country][]" value="" />
									</td>
								</tr>
							</table>
						</div>
		 				<div style="width:850px;float:left">
		 					<table>
		 						<tr  class="tr">
									<td style="width:140px"><strong>Program</strong></td>
									<td class="br">
										<?php 
											if($this->Session->read('user.User.type') == 'Client Admin' || $this->Session->read('user.User.type') == 'Country Admin') : 
												echo $this->Form->input('Client.program_id', array('type' => 'select', 'options' => $programs, 'disabled'=>'disabled','label'=>false));
											else :
												echo $this->Form->input('Client.program_id', array('type'=>'select', 'options'=>$programs, 'div'=>false, 'label'=>false));
										 	endif; 
										 ?>
									</td>
								</tr>
								
								<?php if (($this->Form->value('Client.id')=='') || ((!empty($dashboard_styles[1]) && $this->Form->value('Client.id')!=''))) { ?>
									<tr class="tr">
									    <td><strong>Myrewards Style</strong></td>
									    <td class="br">
										<table>
										    <tr  class="tr">
											<td><?php echo $this->Form->input('Client.style_id1', array('type'=>'select', 'label'=>false, 'div'=>false, 'selected'=>$dashboard_styles[1], 'options'=>$styles));
											echo $this->Ajax->observeField('ClientStyleId1', array('url' => 'update_style/1','update' => 'style1'));
											?>
											</td>
											<td>
											    <div id="style1">
												    <div class="rounded-corners" style="margin-left:10px;margin-right:10px;float:left;width: 50px;height: 20px;background-color: #<?php echo $styles_client['1']['Style']['tab_background_color']; ?>;">

												    </div>
												    <div style="margin-right:5px;margin-top:5px;float:left;width: 25px;height: 15px;background-color: #<?php echo $styles_client['1']['Style']['button_border_color']; ?> ">     </div>
												    <div style=" float:left;margin-top:5px;width: 20px;height: 15px;background-color: #<?php echo $styles_client['1']['Style']['h1_color']; ?> ">     </div> 
											    </div>
											   </td>
										    </tr>
										</table>
									    </td>
									</tr>
									<?php } ?>

									<?php if (($this->Form->value('Client.id')=='') || ((!empty($dashboard_styles[2]) && $this->Form->value('Client.id')!=''))) { ?>
									<tr class="tr">
									    <td><strong>Send A Friend Style </strong></td>
									    <td class="br">
										<table>
										    <tr  class="tr">
											<td><?php echo $this->Form->input('Client.style_id2', array('type'=>'select', 'label'=>false, 'div'=>false, 
												      'selected'=>$dashboard_styles[2], 'options'=>$styles));
											    echo $this->Ajax->observeField('ClientStyleId2', array('url' => 'update_style/2','update' => 'style2'));
											     ?>
											   </td>
											<td>
											    <div id="style2">
												    <div class="rounded-corners" style="margin-left:10px;margin-right:10px;float:left;width: 50px;height: 20px;background-color: #<?php echo $styles_client['2']['Style']['tab_background_color']; ?>;">

												    </div>
												    <div style="margin-right:5px;margin-top:5px;float:left;width: 25px;height: 15px;background-color: #<?php echo $styles_client['2']['Style']['button_border_color']; ?> ">     </div>
												    <div style=" float:left;margin-top:5px;width: 20px;height: 15px;background-color: #<?php echo $styles_client['2']['Style']['h1_color']; ?> ">     </div> 
											    </div>
											   </td>
										    </tr>
										</table>
									    </td>
									</tr>
									<?php } ?>

									<?php if (($this->Form->value('Client.id')=='') || ((!empty($dashboard_styles[3]) && $this->Form->value('Client.id')!=''))) { ?>
									<tr class="tr">
									    <td><strong>My Points Style </strong></td>
									    <td class="br">
										<table>
										    <tr  class="tr">
											<td><?php echo $this->Form->input('Client.style_id3', array('type'=>'select', 'label'=>false, 'div'=>false, 
												      'selected'=>$dashboard_styles[3], 'options'=>$styles));
											    echo $this->Ajax->observeField('ClientStyleId3', array('url' => 'update_style/3','update' => 'style3'));
											     ?>
											   </td>
											<td>
											    <div id="style3">
												    <div class="rounded-corners" style="margin-left:10px;margin-right:10px;float:left;width: 50px;height: 20px;background-color: #<?php echo $styles_client['3']['Style']['tab_background_color']; ?>;">

												    </div>
												    <div style="margin-right:5px;margin-top:5px;float:left;width: 25px;height: 15px;background-color: #<?php echo $styles_client['3']['Style']['button_border_color']; ?> ">     </div>
												    <div style=" float:left;margin-top:5px;width: 20px;height: 15px;background-color: #<?php echo $styles_client['3']['Style']['h1_color']; ?> ">     </div> 
											    </div>
											   </td>
										    </tr>
										</table>
									    </td>
									</tr>
									<?php } ?>
									<tr  class="tr">
									<td><strong>Domain</strong></td>
									<td>
										<?php 
											if($this->Session->read('user.User.type') == 'Client Admin' || $this->Session->read('user.User.type') == 'Country Admin') :
												echo $this->Form->input('Client.domain_id', array('type'=>'select', 'options'=>$domains, 'div'=>false, 'label'=>false, 'disabled'=>'disabled'));
											else :
												echo $this->Form->input('Client.domain_id', array('type'=>'select', 'options'=>$domains, 'div'=>false, 'label'=>false));
											endif;
										?>
									</td>
									<td class="bl">
										<?php if (isset($existing_users) && !empty($existing_users)) : ?>
											The following users already exist for the domain:
											<ul>
												<?php foreach ($existing_users as $user) : ?>
													<li><?php echo $user['User']['username']; ?></ul>
												<?php endforeach; ?>
											</ul>
										<?php endif; ?>
									</td>
								</tr>
								<tr class="tr">
									<td><strong>Client Logo</strong></td>
									<td style="width:373px">
										<?php echo $this->Form->file('Image.client_logo'); ?><br/>
										Remove <?php echo $this->Form->checkbox('ImageDelete.client_logo'); ?>
									</td>
									<td class="bl">
										<?php 
											$filepath = WWW_ROOT.'/files/client_logo/';
											$filename = $this->Form->value('Client.id'). '.'. $this->Form->value('Client.client_logo_extension');
											if (file_exists($filepath. $filename)) :
											
											$path = $this->Html->url('/files/client_logo/'. $this->Form->value('Client.id'). '.'. $this->Form->value('Client.client_logo_extension')); 
											$path_prod = substr($path,strpos($path)+1);
											$img=$this->ImageResize->getResizedDimensions($path_prod, 200, 100);
										?>
												<img width="<?php echo $img['width'];?>" height="<?php echo $img['height'];?>" src="<?php echo $this->Html->url(FILES_WWW_PATH. 'client_logo/'. $this->Form->value('Client.id'). '.'. $this->Form->value('Client.client_logo_extension')); ?>" alt="" />
										<?php endif;?>
									</td>
								</tr>
								
								<tr class="tr">
									<td><strong>Mobile Membership Card</strong></td>
									<script>
										function showDiv()
										{
											if (document.getElementById('mcard').checked==true)
												document.getElementById('ClientImage1File').style.display='inline';
											else
												document.getElementById('ClientImage1File').style.display='none';
										}
										function removeCheck()
										{
											if (document.getElementById('imgRem').checked==true)
												document.getElementById('mcard').checked=false;
										}
									</script>
									<td>
										<?php 
											$path = $this->Html->url(CLIENT_MEMBERSHIP_CARD_IMAGE_WWW_PATH.'/'. $this->Form->value('Client.id'). '.'. $this->Form->value('Client.mobile_membership_card_extension')); 
											$path_prod = substr($path,strpos($path)+1);
											$img=$this->ImageResize->getResizedDimensions($path_prod, 350, 300);
										?>
										<?php echo $this->Form->input('Client.mobile_membership_card',array('div'=>false, 'label'=>false,'type'=>'checkbox','id'=>'mcard', 'onclick'=>"showDiv()")); ?> 
										<?php 
											if($this->Form->value('Client.mobile_membership_card')==1) :?>
												<img width="<?php echo $img['width'];?>" height="<?php echo $img['height'];?>" src="<?php echo $this->Html->url(CLIENT_MEMBERSHIP_CARD_IMAGE_WWW_PATH. $this->Form->value('Client.id'). '.'. $this->Form->value('Client.mobile_membership_card_extension'). '?rand='. time()); ?>" alt="" /><br/>
												<a href="javascript:void(0)" onclick="this.style.display='none'; $('ClientImage1File').style.display = 'inline'; return false;">Change</a><br>
												<?php echo $this->Form->file('ClientImage1.file',array('style' => 'display:none;'));?> 
												Remove 
												<?php echo $this->Form->input('ClientImage1Delete.file',array('div'=>false, 'label'=>false,'type'=>'checkbox','id'=>'imgRem', 'onclick'=>"removeCheck()")); ?> 
												<?php //echo $this->Form->checkbox('ClientImage1Delete.file'); ?>
											<?php else :
												echo $this->Form->file('ClientImage1.file',array('style' => 'display:none;')); 
											endif;	
										?>
									</td>
									<td class="bl"></td>
								</tr>
		 					</table>
		 				</div>
			 			</div>
			 		</div>
			 		<br/>
			       	<div align="center">
						<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(1)">Next</button>
			       	</div>
			 </div>
			 
			 <div class="tabbertab" style="width:831px;" title="">
			  	<h2>Products</h2>
			  		<div class="tabber" id="mytab2" style="margin-left: -59px; width: 809px;">
						<div class="tabbertab"  style="min-height:600px;overflow:hidden;width:810px;padding:15px;" title="">
				  			<h2>My Rewards</h2>
				    		<h4>My Rewards</h4>
				    		<div style="background-color:#EFEFEF;border:1px solid #cccccc;width:800px;padding:5px;">
                    		<div style="padding:5px;">
                    			<h4>Special Offer Module</h4>	
		                    		<table>	
										<tr>
											<td style="height:10px">&nbsp;</td>
										</tr>
										<tr>
											<td class="bt" style="width:135px;"><strong>Limit</strong></td>
											<td class="br bt" colspan="5"><?php echo $this->Form->input('Client.special_offer_module_limit', array('div'=>false,'label'=>false)); ?></td>
										</tr>
										<tr><td style="height:20px;">&nbsp;</td></tr>
										<tr>
											<td class="bb" valign="top"><strong>Products</strong></td>
											<td>
												<input type="hidden" name="data[SpecialProduct][SpecialProduct][]" value="" />
												<?php foreach ($this->data['SpecialProduct'] as $product) :
													if($product['ClientsSpecialProduct']['dashboard_id']==1){ ?>
													<div id="SpecialProduct<?php echo $product['id']; ?>">
													<input type="hidden" name="data[SpecialProduct][SpecialProduct][]" value="<?php echo $product['id']; ?>" />
													<?php echo $product['id']; ?> - <?php echo $product['name']; ?> <a href="javascript:void(0)" onclick="$('#SpecialProduct<?php echo $product['id']; ?>').remove(); return false;">Remove</a>
													</div>
												<?php } ?>
												<?php endforeach; ?>
												
											</td>
											<td class="br bb" colspan="4" valign="top" style="padding-left:100px;">
												<p id="addSpecialProduct"></p>
												<a href="javascript:void(0)" onclick="return addSpecialProduct();"><strong>Add Special Offer Product</strong></a>
												<script type="text/javascript">
													function addSpecialProduct() {
														var rand = Math.round(10000000000*Math.random());
														$('<input id="sp' + rand + '" type="text" name="data[SpecialProduct][SpecialProduct][]" autocomplete="off" /><br/>').insertBefore('p#addSpecialProduct');
														$(function() {
															$( "#sp"+ rand ).autocomplete({
																source: "/admin/clients/xml_products/<?php echo $this->Html->value('Client.id')?>", delay: .25
															});
														});
														return false;
													}
												</script>
											</td>
										</tr>
										<tr><td style="height:20px;">&nbsp;</td></tr>
									</table>
			 					</div>
			   				</div>
			   	 			<br>
			   				<div style="background-color:#EFEFEF;border:1px solid #cccccc;width:800px;padding:5px;">
								<div style="padding:5px;">
									<table>
										<tr>
											<td style="height:20px;">&nbsp;</td>
										</tr>
										<tr>
										    <td>
												<table style="width:800px;">
													<tr>
														<td valign="top"><strong>Blocklisted Products</strong><br/><sub>Product ids listed here <br/>will not be shown for this client.<br/>List the products <br/>"comma" seperated and put a <br> "full stop" after the <br> last product id.</sub></td>
														<td valign="top"><?php echo $this->Form->input('Client.blacklisted_product',array('class'=>'mceNoEditor','div'=>false,'label'=>false)); ?></td>
														<td style="width:5px;">&nbsp;</td>
														<td valign="top" style="width:35px;padding-left:10px;"><strong>Categories</strong></td>
														<td valign="top"  style="padding-left:10px;padding-right:10px;">
															<?php echo $this->Form->input('Category.Category', array('multiple' => 'multiple','style'=>'height:120px')); ?>
															<input type="hidden" name="data[Category][Category][]" value="" />
														</td>
														<td class="bl"></td>
													</tr>
												</table> 
											</td>	
										</tr>
										<tr>
											<td style="height:20px;">&nbsp;</td>
										</tr>
										<tr> 
											<td>
												<table style="width:800px;">
													<tr>
														<td style="width:135px;"><strong>Default Product Image</strong></td>
														<td>
															<?php echo $this->Form->file('Image.default_client_product_image'); ?><br/>
															Remove <?php echo $this->Form->checkbox('ImageDelete.default_client_product_image'); ?>
														</td>
														<td class="bl">
															<img src="<?php echo $this->Html->url(FILES_WWW_PATH. 'default_client_product_image/'. $this->Form->value('Client.id'). '.'. $this->Form->value('Client.default_client_product_image_extension')); ?>" alt="" />
														</td>
													</tr>
												</table>
									    	</td>  
										</tr>
									</table>
			 					</div>
			 				</div>
			 			<br/>
				 	<div align="center">
						<button type="button" class="admin_button" onclick="document.getElementById('mytab2').tabber.tabShow(1)">Next</button>
					</div>
	    		</div>
	    		<div class="tabbertab"  style="min-height:600px;overflow:hidden;width:810px;padding:15px;" title="">
				  			<h2>Send A Friend</h2>
				    		<h4>Send A Friend</h4>
				    		<div style="background-color:#EFEFEF;border:1px solid #cccccc;width:800px;padding:5px;">
                    		<div style="padding:5px;">
                    			<h4>Special Offer Module</h4>	
		                    		<table>	
										<tr>
											<td style="height:10px">&nbsp;</td>
										</tr>
										<tr>
											<td class="bt" style="width:135px;"><strong>Limit</strong></td>
											<td class="br bt" colspan="5"><?php echo $this->Form->input('Client.special_offer_module_limit_saf', array('div'=>false,'label'=>false)); ?></td>
										</tr>
										<tr><td style="height:20px;">&nbsp;</td></tr>
										<tr>
											<td class="bb" valign="top"><strong>Products</strong></td>
											<td>
												<input type="hidden" name="data[SpecialProduct][SpecialProduct_2][]" value="" />
												<?php foreach ($this->data['SpecialProduct'] as $product) : 
													if($product['ClientsSpecialProduct']['dashboard_id']==2){?>
													<div id="SpecialProduct<?php echo $product['id'].$product['ClientsSpecialProduct']['dashboard_id']; ?>">
													<input type="hidden" name="data[SpecialProduct][SpecialProduct_2][]" value="<?php echo $product['id']; ?>" />
													<?php echo $product['id']; ?> - <?php echo $product['name']; ?> <a href="javascript:void(0)" onclick="$('#SpecialProduct<?php echo $product['id'].$product['ClientsSpecialProduct']['dashboard_id']; ?>').remove(); return false;">Remove</a>
													</div>
													<?php } ?>
												<?php endforeach; ?>
												
											</td>
											<td class="br bb" colspan="4" valign="top" style="padding-left:100px;">
												<p id="addSpecialProductSAF"></p>
												<a href="javascript:void(0)" onclick="return addSpecialProductSAF();"><strong>Add Special Offer Product</strong></a>
												<script type="text/javascript">
													function addSpecialProductSAF() {
														var rand = Math.round(10000000000*Math.random());
														$('<input id="sp' + rand + '" type="text" name="data[SpecialProduct][SpecialProduct_2][]" autocomplete="off" /><br/>').insertBefore('p#addSpecialProductSAF');
														$(function() {
															$( "#sp"+ rand ).autocomplete({
																source: "/admin/clients/xml_products_saf/<?php echo $this->Html->value('Client.id')?>", delay: .25
															});
														});
														return false;
													}
												</script>
											</td>
										</tr>
										<tr><td style="height:20px;">&nbsp;</td></tr>
									</table>
			 					</div>
			   				</div>
			   	 	<div align="center">
						<button type="button" class="admin_button" onclick="document.getElementById('mytab2').tabber.tabShow(0)">Previous</button>
						<button type="button" class="admin_button" onclick="document.getElementById('mytab2').tabber.tabShow(2)">Next</button>
					</div>
	    		</div>
	    		<div class="tabbertab"  style="min-height:600px;overflow:hidden;width:810px;padding:15px;" title="">
				  			<h2>Points</h2>
				    		<h4>Points</h4>
				    		<div style="background-color:#EFEFEF;border:1px solid #cccccc;width:800px;padding:5px;">
                    		<div style="padding:5px;">
                    			<h4>Special Offer Module</h4>	
		                    		<table>	
										<tr>
											<td style="height:10px">&nbsp;</td>
										</tr>
										<tr>
											<td class="bt" style="width:135px;"><strong>Limit</strong></td>
											<td class="br bt" colspan="5"><?php echo $this->Form->input('Client.special_offer_module_limit_points', array('div'=>false,'label'=>false)); ?></td>
										</tr>
										<tr><td style="height:20px;">&nbsp;</td></tr>
										<tr>
											<td class="bb" valign="top"><strong>Products</strong></td>
											<td>
												<input type="hidden" name="data[SpecialProduct][SpecialProduct_3][]" value="" />
												<?php foreach ($this->data['SpecialProduct'] as $product) : 
												if($product['ClientsSpecialProduct']['dashboard_id']==3){ ?>
													<div id="SpecialProduct<?php echo $product['id']; ?>">
													<input type="hidden" name="data[SpecialProduct][SpecialProduct_3][]" value="<?php echo $product['id']; ?>" />
													<?php echo $product['id']; ?> - <?php echo $product['name']; ?> <a href="javascript:void(0)" onclick="$('#SpecialProduct<?php echo $product['id']; ?>').remove(); return false;">Remove</a>
													</div>
												<?php } ?>
												<?php endforeach; ?>
												
											</td>
											<td class="br bb" colspan="4" valign="top" style="padding-left:100px;">
												<p id="addSpecialProductPoints"></p>
												<a href="javascript:void(0)" onclick="return addSpecialProductPoints();"><strong>Add Special Offer Product</strong></a>
												<script type="text/javascript">
													function addSpecialProductPoints() {
														var rand = Math.round(10000000000*Math.random());
														$('<input id="sp' + rand + '" type="text" name="data[SpecialProduct][SpecialProduct_3][]" autocomplete="off" /><br/>').insertBefore('p#addSpecialProductPoints');
														$(function() {
															$( "#sp"+ rand ).autocomplete({
																source: "/admin/clients/xml_products_points/<?php echo $this->Html->value('Client.id')?>", delay: .25
															});
														});
														return false;
													}
												</script>
											</td>
										</tr>
										<tr><td style="height:20px;">&nbsp;</td></tr>
									</table>
			 					</div>
			   				</div>
			   	 		<div align="center">
						<button type="button" class="admin_button" onclick="document.getElementById('mytab2').tabber.tabShow(1)">Previous</button>
					</div>
	    		</div>
	    	</div>
	    	<div align="center">
						<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(0)">Previous</button>
						<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(2)">Next</button>
			</div>
   		 </div>			 
			  <div class="tabbertab">
				  <h2>Links</h2>
				  <h4>Links</h4>
				  	<div style="width:810px;height:330px;background-color:#EFEFEF;border:1px solid #cccccc;">
			 			<div style=" height: 100px; float:left; width: 325px;padding:20px;">
							<table style="width:100%">
								<tr>
									<td><strong>Link enabled</strong></td>
									<td class="br"><?php echo $this->Form->input('Client.link_enabled', array('div'=>false,'label'=>false)); ?></td>
								</tr>
								<tr><td style="height:5px;">&nbsp;</td></tr>
								<tr>
									<td><strong>Link</strong></td>
									<td class="br"><?php echo $this->Form->input('Client.link', array('div'=>false,'label'=>false)); ?></td>
								</tr>
								<tr><td style="height:5px;">&nbsp;</td></tr>
								<tr>
									<td ><strong>Link text</strong></td>
									<td class="br"><?php echo $this->Form->input('Client.link_text', array('div'=>false,'label'=>false)); ?></td>
								</tr>
								<tr><td style="height:5px;">&nbsp;</td></tr>
								<tr>
									<td><strong>Home Page</strong></td>
									<td class="br"><?php echo $this->Form->input('Client.home_page', array('div'=>false,'label'=>false)); ?></td>
								</tr>
								<tr>
									<td style="width: 200px;"><strong>Redirect logout page</strong><br/><sub>If enabled, the user will be sent back to the external page they logged in from</sub></td>
									<td class="br"><?php echo $this->Form->input('Client.logout_redirect', array('div'=>false,'label'=>false)); ?></td>
								</tr>
							</table>
						</div>
						<div style="height: 100px; float:left; width: 395px;padding:20px 10px 10px 35px">
							<table style="width:100%">				
								<tr>
									<td style="width:150px"><strong>Cart Enable</strong></td>
									<td class="br">
										<?php echo $this->Form->input('Client.cart_enabled',array('onclick'=>'showPaypalName()','div'=>false,'label'=>false));?>
										<br/>
										<script>
											function showPaypalName()
											{
												if (document.getElementById('ClientCartEnabled').checked==true)
												{
													document.getElementById('paypal').style.display='block';
													document.getElementById('currency').style.display='block';
												}
												else
												{
													document.getElementById('paypal').style.display='none';
													document.getElementById('currency').style.display='none';
												}
											}
										</script>
									</td>
								</tr>	
								
								<?php 
									$mdStyle="none"; 
									if($this->data['Client']['cart_enabled'] == '0'):
										$mdStyle="display:none";
									elseif($this->data['Client']['cart_enabled'] == 1):
										$mdStyle="display:block";
									endif;
								?>	
								<tr style=<?php echo $mdStyle;?>><td style="height:5px;">&nbsp;</td></tr>	
								<tr>
									<td colspan="2">
										<div id="paypal" style=<?php echo $mdStyle;?>>
											<strong>Paypal Business Name</strong>
											<?php echo $this->Form->input('Client.paypal_business_name',array('value'=>$business_name,'div'=>false,'label'=>false));?>
											<br/>
											<span style="color:red">Please Provide Paypal Business Name if cart is enabled</span>
										</div>
									</td>
								</tr>
								<tr style=<?php echo $mdStyle;?>><td style="height:5px;">&nbsp;</td></tr>
								<tr>
									<td colspan="2">
										<div id="currency" style=<?php echo $mdStyle;?>>
											<strong>Paypal Currency Code</strong>&nbsp;&nbsp;&nbsp;
											<?php 
												echo $this->Form->input('Client.paypal_currency_code', array('type' => 'select', 'options' => $codes, 'default'=>$currency_code,'div'=>false,'label'=>false)); 
											?>
											<br/>
											<span style="color:red">Please Select Currency Code if cart is enabled</span>
										</div>
									</td>
								</tr>
								<tr><td style="height:5px;">&nbsp;</td></tr>		
								<tr>
									<td><strong>Live Chat Enable</strong></td>
									<td class="br">
										<?php 
											if($this->Session->read('user.User.type') == 'Client Admin' || $this->Session->read('user.User.type') == 'Country Admin') :
												 echo $this->Form->input('Client.live_chat_enabled',array('disabled'=>'disabled','div'=>false,'label'=>false));
											else : 
												echo $this->Form->input('Client.live_chat_enabled', array('div'=>false,'label'=>false));
											endif; 
										?>
									</td>
								</tr>
								<tr><td style="height:5px;">&nbsp;</td></tr>
								<tr>
									<td><strong>Header Links Disable</strong></td>
									<td class="br"><?php echo $this->Form->input('Client.header_links_disabled', array('div'=>false,'label'=>false)); ?></td>
								</tr>
								<tr><td style="height:5px;">&nbsp;</td></tr>
								<tr>
									<td><strong>Footer Links Disable</strong></td>
									<td class="br"><?php echo $this->Form->input('Client.footer_links_disabled', array('div'=>false,'label'=>false)); ?></td>
								</tr>
								<tr><td style="height:5px;">&nbsp;</td></tr>
								<tr>
									<td ><strong>Refine Search Disable</strong></td>
									<td class="br"><?php echo $this->Form->input('Client.refine_search_disabled', array('div'=>false,'label'=>false)); ?></td>
								</tr>
								<tr><td style="height:5px;">&nbsp;</td></tr>
								<tr>
									<td class="bb"><strong>Email Alerts Disable</strong></td>
									<td class="bb br"><?php echo $this->Form->input('Client.email_alerts_disabled', array('div'=>false,'label'=>false)); ?></td>
								</tr>	
							</table>
						</div>
			 		</div>
			 		<br/>
			 		<div align="center">
						<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(1)">Previous</button>
						<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(3)">Next</button>
					</div>
			 	</div>
			 
				<div class="tabbertab" style="width:831px;">
					<div class="tabber" id="mytab3" style="margin-left: -59px; width: 847px;">
						<h2>Bg Images</h2>
						<div class="tabbertab">
							<h2>My Rewards</h2>
							<table style="border:0;background-color:#EFEFEF;border:1px solid #cccccc;width:820px;height:200px; margin-left: -19px;">
								<tr>
									<td class="bt br"></td>
									<td class="bt br" style="padding-top:20px;padding-left:52px;padding-right:50px;"><strong>Left</strong></td>
									<td class="bt br" style="padding-top:20px;padding-left:52px;padding-right:50px;"><strong>Right</strong></td>
								</tr>
								<tr>
									<td class="bt br" valign="top" style="padding-left:20px;"><strong>Upload Images</strong></td>
									<td class="bt br" style="padding-left:50px;padding-right:50px;">
										<?php echo $this->Form->file('Image.client_background_images_left_1'); ?><br/>
										Remove <?php echo $this->Form->checkbox('ImageDelete.client_background_images_left'); ?>
									</td>
									<td class="bt br" style="padding-left:50px;padding-right:50px;">
										<?php echo $this->Form->file('Image.client_background_images_right_1'); ?><br/>
										Remove <?php echo $this->Form->checkbox('ImageDelete.client_background_images_right'); ?>
									</td>
								</tr>
								<tr>
									<td style="height:5px">&nbsp;</td>
								</tr>
								<tr> 
									<td colspan="3" style="padding-left:20px;padding-bottom:20px"><b><span style="color:red">* Left/Right Backgrount Image : Upload image only with 170px width and 700px height</span></b></td> 
								</tr>
								<tr>
									<td style="padding-left:20px;"><strong>Client background left image link</strong></td>
									<td style="padding-left:50px;">
										<?php echo $this->Form->input('ClientBanner.left_link_1', array('div'=>false,'label'=>false));?>
									</td>
								</tr>
								<tr>
									<td style="padding-left:20px;"><strong>Client background right image link</strong></td>
									<td style="padding-left:50px;">
										<?php echo $this->Form->input('ClientBanner.right_link_1', array('div'=>false,'label'=>false));?>
									</td>
								</tr>
								<tr>
									<td style="height:5px">&nbsp;</td>
								</tr>
							</table>
							<br />
							<div align="center">
								<button type="button" class="admin_button" onclick="document.getElementById('mytab3').tabber.tabShow(1)">Next</button>
							</div>
						</div>

						<div class="tabbertab">
							<h2>Send A Friend</h2>
							<table style="border:0;background-color:#EFEFEF;border:1px solid #cccccc;width:820px;height:200px; margin-left: -19px;">
								<tr>
									<td class="bt br"></td>
									<td class="bt br" style="padding-top:20px;padding-left:52px;padding-right:50px;"><strong>Left</strong></td>
									<td class="bt br" style="padding-top:20px;padding-left:52px;padding-right:50px;"><strong>Right</strong></td>
								</tr>
								<tr>
									<td class="bt br" valign="top" style="padding-left:20px;"><strong>Upload Images</strong></td>
									<td class="bt br" style="padding-left:50px;padding-right:50px;">
										<?php echo $this->Form->file('Image.client_background_images_left_2'); ?><br/>
										Remove <?php echo $this->Form->checkbox('ImageDelete.client_background_images_left_2'); ?>
									</td>
									<td class="bt br" style="padding-left:50px;padding-right:50px;">
										<?php echo $this->Form->file('Image.client_background_images_right_2'); ?><br/>
										Remove <?php echo $this->Form->checkbox('ImageDelete.client_background_images_right_2'); ?>
									</td>
								</tr>
								<tr>
								    <td style="height:5px">&nbsp;</td>
								</tr>
								<tr> 
									<td colspan="3" style="padding-left:20px;padding-bottom:20px"><b><span style="color:red">* Left/Right Backgrount Image : Upload image only with 170px width and 700px height</span></b></td> 
								</tr>
								<tr>
									<td style="padding-left:20px;"><strong>Client background left image link</strong></td>
									<td style="padding-left:50px;">
										<?php echo $this->Form->input('ClientBanner.left_link_2', array('div'=>false,'label'=>false));?>
									</td>
								</tr>
								<tr>
									<td style="padding-left:20px;"><strong>Client background right image link</strong></td>
									<td style="padding-left:50px;">
										<?php echo $this->Form->input('ClientBanner.right_link_2', array('div'=>false,'label'=>false));?>
									</td>
								</tr>
								<tr>
									<td style="height:5px">&nbsp;</td>
								</tr>
							</table>
							<br />
							<div align="center">
								<button type="button" class="admin_button" onclick="document.getElementById('mytab3').tabber.tabShow(0)">Previous</button>
								<button type="button" class="admin_button" onclick="document.getElementById('mytab3').tabber.tabShow(2)">Next</button>
							</div>
						</div>

						<div class="tabbertab">
							<h2>Points</h2>
							<table style="border:0;background-color:#EFEFEF;border:1px solid #cccccc;width:820px;height:200px; margin-left: -19px;">
								<tr>
									<td class="bt br"></td>
									<td class="bt br" style="padding-top:20px;padding-left:52px;padding-right:50px;"><strong>Left</strong></td>
									<td class="bt br" style="padding-top:20px;padding-left:52px;padding-right:50px;"><strong>Right</strong></td>
								</tr>
								<tr>
									<td class="bt br" valign="top" style="padding-left:20px;"><strong>Upload Images</strong></td>
									<td class="bt br" style="padding-left:50px;padding-right:50px;">
										<?php echo $this->Form->file('Image.client_background_images_left_3'); ?><br/>
										Remove <?php echo $this->Form->checkbox('ImageDelete.client_background_images_left_3'); ?>
									</td>
									<td class="bt br" style="padding-left:50px;padding-right:50px;">
										<?php echo $this->Form->file('Image.client_background_images_right_3'); ?><br/>
										Remove <?php echo $this->Form->checkbox('ImageDelete.client_background_images_right_3'); ?>
									</td>
								</tr>
								<tr>
								     <td style="height:5px">&nbsp;</td>
								</tr>
								<tr> 
									  <td colspan="3" style="padding-left:20px;padding-bottom:20px"><b><span style="color:red">* Left/Right Backgrount Image : Upload image only with 170px width and 700px height</span></b></td> 
								</tr>
								<tr>
									<td style="padding-left:20px;"><strong>Client background left image link</strong></td>
									<td style="padding-left:50px;">
										<?php echo $this->Form->input('ClientBanner.left_link_3', array('div'=>false,'label'=>false));?>
									</td>
								</tr>
								<tr>
									<td style="padding-left:20px;"><strong>Client background right image link</strong></td>
									<td style="padding-left:50px;">
										<?php echo $this->Form->input('ClientBanner.right_link_3', array('div'=>false,'label'=>false));?>
									</td>
								</tr>
								<tr>
								     <td style="height:5px">&nbsp;</td>
								</tr>
							</table>
							<br/>
							<div align="center">
								<button type="button" class="admin_button" onclick="document.getElementById('mytab3').tabber.tabShow(1)">Previous</button>
							</div>
						</div>
					</div>
					<div align="center">
						<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(2)">Previous</button>
						<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(4)">Next</button>
					</div>
				</div>
			 
			 	<div class="tabbertab" style="width:831px;">
	  				<div class="tabber" id="mytab4" style="margin-left: -59px; width: 809px;">
					<h2>HBanners</h2>
						<div class="tabbertab"  style="min-height:600px;overflow:hidden;width:810px;">
							<h2>My Rewards</h2>
									<table width="100%">
										<tr>
											<td colspan="3">
												<div style="background-color:#efefef;border:1px solid #cccccc;padding-left:15px;padding-top:10px;padding-bottom:10px;width:795px;margin-left:-15px;" >
													<table width="100%">
														<tr>
															<td class="bt br"></td>
															<td class="bt br" style="padding-left:2px"><strong>Left</strong></td>
															<td class="bt br" style="padding-left:2px"><strong>Right</strong></td>
														</tr>
														<tr>
															<td class="bt br"><strong>Image</strong></td>
															<td class="bt br">
																<?php echo $this->Form->file('Image.banner_background_image_left_1'); ?><br/>
																Remove <?php echo $this->Form->checkbox('ImageDelete.banner_background_image_left'); ?>
															</td>
															<td class="bt br">
																<?php echo $this->Form->file('Image.banner_background_image_right_1'); ?><br/>
																Remove <?php echo $this->Form->checkbox('ImageDelete.banner_background_image_right'); ?>
															</td>
														</tr>
														<tr>
															<td style="height:5px">&nbsp;</td>
														</tr>
														<tr>
															<td class="br" style="width:140px;"><strong>Align</strong></td>
															<td class="br"><?php echo $this->Form->input('Client.banner_left_align_1', array('onchange' => 'updateBanners();','default'=>$dashboard['0']['client_banners']['banner_left_align'],'type'=>'select','options'=>$aligns,'div'=>false,'label'=>false)); ?></td>
															<td class="br"><?php echo $this->Form->input('Client.banner_right_align_1', array('onchange' => 'updateBanners();','default'=>$dashboard['0']['client_banners']['banner_right_align'],'type'=>'select','options'=>$aligns,'div'=>false,'label'=>false)); ?></td>
														</tr>
														<tr>
															<td style="height:5px">&nbsp;</td>
														</tr>
														<tr>
															<td class="br bb"><strong>Repeat</strong></td>
															<td class="br bb"><?php echo $this->Form->input('Client.banner_left_repeat_1', array('onchange' => 'updateBanners();','default'=>$dashboard['0']['client_banners']['banner_left_repeat'],'type'=>'select','options'=>$repeats,'div'=>false,'label'=>false)); ?></td>
															<td class="br bb"><?php echo $this->Form->input('Client.banner_right_repeat_1', array('onchange' => 'updateBanners();','default'=>$dashboard['0']['client_banners']['banner_right_repeat'],'type'=>'select','options'=>$repeats,'div'=>false,'label'=>false)); ?></td>
														</tr>
														<tr><td style="height:5px">&nbsp;</td></tr>
														<tr>
															<td colspan="3"><b><span style="color:red">*Single banner : Upload only LEFT image with 950px width and 100px height</span></b></td> 
														</tr>
														<tr>
															<td colspan="3"><b><span style="color:red">*Two banners : Upload LEFT image with 475px width and 100px height and RIGHT image with 475px width and 100px height</span></b></td> 
														</tr>
														<tr><td colspan="3"style="height:5px">&nbsp;</td></tr>
													</table>
												</div>
											</td>
										</tr>
										<tr><td style="height:20px">&nbsp;</td></tr>
										<tr>
											<td>
												<h4 style="margin-left:-15px;">Header Banner Background colour</h4>
												<div style="background-color:#efefef;border:1px solid #cccccc;padding-left:15px;padding-top:10px;padding-bottom:10px;width:795px;margin-left: -15px;" >
													
													<table>
														<tr>
															<td class="br bb" style="width:136px;"><strong>Background colour</strong></td>
															<td class="br bb">
															<?php echo $this->Form->input('Client.banner_background_color_1', array('class' => 'color', 'id' =>'banner_background_color_1','value'=>$dashboard['0']['client_banners']['banner_background_color'],'div'=>false,'label'=>false)); ?>
															<span style="background-color:#<?php echo $this->Form->value('Client.banner_background_color_1'); ?>"></span>
															</td>
															<td>&nbsp;</td>
														</tr>
													</table>
												</div>
											</td>
										</tr>
									</table>
												
									<script type="text/javascript">
			
									function updateBanners()
									 {
										$('left_banner').style.backgroundPosition = 'top ' + $('ClientBannerLeftAlign').value;
										$('left_banner').style.backgroundRepeat = $('ClientBannerLeftRepeat').value;
										$('right_banner').style.backgroundPosition = 'top ' + $('ClientBannerRightAlign').value;
										$('right_banner').style.backgroundRepeat = $('ClientBannerRightRepeat').value;
										$('center_banner2').style.backgroundPosition = 'top ' + $('ClientBannerAlign').value;
										$('center_banner2').style.backgroundRepeat = $('ClientBannerRepeat').value;
									 }
			
									</script>
														
									<h4 style="padding-top:20px;padding-bottom:10px;margin-left: -15px;">Header Text Alignment</h4>
									<div style="background-color: #EFEFEF;border:1px solid #cccccc;width:810px;margin-left: -15px;">
									<div style="padding:20px;"><?php $banner_option_1 = !empty($dashboard['0']['client_banners']['banner_option'])?$dashboard['0']['client_banners']['banner_option']:0;?>
										<?php echo $this->Form->radio('Client.banner_option_1', array('0'=> ''), array('class' => 'checkbox','value'=>$banner_option_1)); ?><strong> Centred</strong>
										<table style="border-left:none;width: 100%" cellpadding="0" cellspacing="0">
											<tr id="header_top">
												<td style="white-space:nowrap;text-align:center;">Welcome back <b> First_name Last_name </b> My favourires | My Account | Sign Out  | Admin</td>
											</tr>
											<tr>
												<td style=""height:5px">&nbsp;</td>
											</tr>
											<tr>
												<td align="right" style="border:1px solid #DFDFDF;padding:0;<?php
													if ($dashboard['0']['client_banners']['banner_background_image_left_extension'] != '') {
														echo "background-image:  url('". $this->Html->url('/files/banner_background_image_left/'. $this->Form->value('Client.id'). '.'. $dashboard['0']['client_banners']['banner_background_image_left_extension']). "');";
													}
			
													echo 'background-repeat: no-repeat;height:100px';
													?>">
													<?php if($dashboard['0']['client_banners']['banner_background_image_right_extension'] !=''){ ?>
													<img src="<?php echo $this->Html->url('/files/banner_background_image_right/'. $this->Form->value('Client.id'). '.'. $dashboard3['0']['client_banners']['banner_background_image_righ_extension']); ?>" alt=""  border="0" />
													<?php } ?>
												</td>
											</tr>
										</table>
										<br/>
										<?php echo $this->Form->radio('Client.banner_option_1', array('1'=> ''), array('class' => 'checkbox','value'=>$banner_option_1)); ?><strong> Left & right</strong>
										<table style="width: 100%;border:0;" cellpadding="0" cellspacing="0">
											<tr id="header_top">
												<td style="white-space:nowrap;text-align:right;">Welcome back <b> First_name Last_name </b> My favourires | My Account | Sign Out  | Admin</td>
											</tr>
											<tr>
												<td style=""height:5px">&nbsp;</td>
											</tr>
											<tr>
												<td align="right" style="border:1px solid #DFDFDF;padding:0;<?php
													if ($dashboard['0']['client_banners']['banner_background_image_left_extension'] != '') {
														echo "background-image:  url('". $this->Html->url('/files/banner_background_image_left/'. $this->Form->value('Client.id'). '.'. $dashboard['0']['client_banners']['banner_background_image_left_extension']). "');";
													}
			
													echo 'background-repeat: no-repeat;height:100px';
													?>">
													<?php if($dashboard['0']['client_banners']['banner_background_image_right_extension'] !=''){ ?>
													<img src="<?php echo $this->Html->url('/files/banner_background_image_right/'. $this->Form->value('Client.id'). '.'. $dashboard['0']['client_banners']['banner_background_image_left_extension']); ?>" alt=""  border="0" />
													<?php } ?>
												</td>
											</tr>
										</table>
									</div>										
								</div>
								<br />
								<div align="center">
									<button type="button" class="admin_button" onclick="document.getElementById('mytab4').tabber.tabShow(1)">Next</button>
								</div>	
						</div>
						<div class="tabbertab"  style="min-height:600px;overflow:hidden;width:810px;">
							<h2>Send A Friend</h2>
									<table width="100%">
										<tr>
											<td colspan="3">
												<div style="background-color:#efefef;border:1px solid #cccccc;padding-left:15px;padding-top:10px;padding-bottom:10px;width:795px;margin-left:-15px;" >
													<table width="100%">
														<tr>
															<td class="bt br"></td>
															<td class="bt br" style="padding-left:2px"><strong>Left</strong></td>
															<td class="bt br" style="padding-left:2px"><strong>Right</strong></td>
														</tr>
														<tr>
															<td class="bt br"><strong>Image</strong></td>
															<td class="bt br">
																<?php echo $this->Form->file('Image.banner_background_image_left_2'); ?><br/>
																Remove <?php echo $this->Form->checkbox('ImageDelete.banner_background_image_left_saf'); ?>
															</td>
															<td class="bt br">
																<?php echo $this->Form->file('Image.banner_background_image_right_2'); ?><br/>
																Remove <?php echo $this->Form->checkbox('ImageDelete.banner_background_image_right_saf'); ?>
															</td>
														</tr>
														<tr>
															<td style="height:5px">&nbsp;</td>
														</tr>
														<tr>
															<td class="br" style="width:140px;"><strong>Align</strong></td>
															<td class="br"><?php echo $this->Form->input('Client.banner_left_align_2', array('onchange' => 'updateBanners();','default'=>$dashboard2['0']['client_banners']['banner_left_align'],'type'=>'select','options'=>$aligns,'div'=>false,'label'=>false)); ?></td>
															<td class="br"><?php echo $this->Form->input('Client.banner_right_align_2', array('onchange' => 'updateBanners();','default'=>$dashboard2['0']['client_banners']['banner_right_align'],'type'=>'select','options'=>$aligns,'div'=>false,'label'=>false)); ?></td>
														</tr>
														<tr>
															<td style="height:5px">&nbsp;</td>
														</tr>
														<tr>
															<td class="br bb"><strong>Repeat</strong></td>
															<td class="br bb"><?php echo $this->Form->input('Client.banner_left_repeat_2', array('onchange' => 'updateBanners();','default'=>$dashboard2['0']['client_banners']['banner_left_repeat'],'type'=>'select','options'=>$repeats,'div'=>false,'label'=>false)); ?></td>
															<td class="br bb"><?php echo $this->Form->input('Client.banner_right_repeat_2', array('onchange' => 'updateBanners();','default'=>$dashboard2['0']['client_banners']['banner_right_repeat'],'type'=>'select','options'=>$repeats,'div'=>false,'label'=>false)); ?></td>
														</tr>
														<tr><td style="height:5px">&nbsp;</td></tr>
														<tr>
															<td colspan="3"><b><span style="color:red">*Single banner : Upload only LEFT image with 950px width and 100px height</span></b></td> 
														</tr>
														<tr>
															<td colspan="3"><b><span style="color:red">*Two banners : Upload LEFT image with 475px width and 100px height and RIGHT image with 475px width and 100px height</span></b></td> 
														</tr>
														<tr><td colspan="3"style="height:5px">&nbsp;</td></tr>
													</table>
												</div>
											</td>
										</tr>
										<tr><td style="height:20px">&nbsp;</td></tr>
										<tr>
											<td>
												<h4 style="margin-left:-15px;">Header Banner Background colour</h4>
												<div style="background-color:#efefef;border:1px solid #cccccc;padding-left:15px;padding-top:10px;padding-bottom:10px;width:795px;margin-left: -15px;" >
													
													<table>
														<tr>
															<td class="br bb" style="width:136px;"><strong>Background colour</strong></td>
															<td class="br bb">
															<?php echo $this->Form->input('Client.banner_background_color_2', array('class' => 'color', 'id' =>'banner_background_color_2','value'=>$dashboard2['0']['client_banners']['banner_background_color'],'div'=>false,'label'=>false)); ?>
															<span style="background-color:#<?php echo $this->Form->value('Client.banner_background_color_2'); ?>"></span>
															</td>
															<td>&nbsp;</td>
														</tr>
													</table>
												</div>
											</td>
										</tr>
									</table>
												
									<script type="text/javascript">
			
									function updateBanners()
									 {
										$('left_banner').style.backgroundPosition = 'top ' + $('ClientBannerLeftAlign').value;
										$('left_banner').style.backgroundRepeat = $('ClientBannerLeftRepeat').value;
										$('right_banner').style.backgroundPosition = 'top ' + $('ClientBannerRightAlign').value;
										$('right_banner').style.backgroundRepeat = $('ClientBannerRightRepeat').value;
										$('center_banner2').style.backgroundPosition = 'top ' + $('ClientBannerAlign').value;
										$('center_banner2').style.backgroundRepeat = $('ClientBannerRepeat').value;
									 }
			
									</script>
														
									<h4 style="padding-top:20px;padding-bottom:10px;margin-left: -15px;">Header Text Alignment</h4>
									<div style="background-color: #EFEFEF;border:1px solid #cccccc;width:810px;margin-left: -15px;">
									<div style="padding:20px;"><?php $banner_option_2 = !empty($dashboard2['0']['client_banners']['banner_option'])?$dashboard2['0']['client_banners']['banner_option']:0;?>
										<?php echo $this->Form->radio('Client.banner_option_2', array('0'=> ''), array('class' => 'checkbox','value'=>$banner_option_2)); ?><strong> Centred</strong>
										<table style="border-left:none;width: 100%" cellpadding="0" cellspacing="0">
											<tr id="header_top">
												<td style="white-space:nowrap;text-align:center;">Welcome back <b>First_name Last_name</b> My favourires | My Account | Sign Out  | Admin</td>
											</tr>
											<tr>
												<td style=""height:5px">&nbsp;</td>
											</tr>
											<tr>
												<td align="right" style="border:1px solid #DFDFDF;padding:0;<?php
													if ($dashboard2['0']['client_banners']['banner_background_image_left_extension']!= '') {
														echo "background-image:  url('". $this->Html->url('/files/banner_background_image_left_saf/'. $this->Form->value('Client.id'). '.'. $dashboard2['0']['client_banners']['banner_background_image_left_extension']). "');";
													}
			
													echo 'background-repeat: no-repeat;height:100px';
													?>">
													<?php if($dashboard2['0']['client_banners']['banner_background_image_right_extension'] !=''){ ?>
													<img src="<?php echo $this->Html->url('/files/banner_background_image_right_saf/'. $this->Form->value('Client.id'). '.'. $dashboard2['0']['client_banners']['banner_background_image_right_extension']); ?>" alt=""  border="0" />
													<?php } ?>
												</td>
											</tr>
										</table>
										<br/>
										<?php echo $this->Form->radio('Client.banner_option_2', array('1'=> ''), array('class' => 'checkbox','value'=>$banner_option_2)); ?><strong> Left & right</strong>
										<table style="width: 100%;border:0;" cellpadding="0" cellspacing="0">
											<tr id="header_top">
												<td style="white-space:nowrap;text-align:right;">Welcome back <b>First_name Last_name</b> My favourires | My Account | Sign Out  | Admin</td>
											</tr>
											<tr>
												<td style=""height:5px">&nbsp;</td>
											</tr>
											<tr>
												<td align="right" style="border:1px solid #DFDFDF;padding:0;<?php
													if ($dashboard2['0']['client_banners']['banner_background_image_left_extension'] != '') {
														echo "background-image:  url('". $this->Html->url('/files/banner_background_image_left_saf/'. $this->Form->value('Client.id'). '.'. $dashboard2['0']['client_banners']['banner_background_image_left_extension']). "');";
													}
			
													echo 'background-repeat: no-repeat;height:100px';
													?>">
													<?php if($dashboard2['0']['client_banners']['banner_background_image_right_extension'] !=''){ ?>
													<img src="<?php echo $this->Html->url('/files/banner_background_image_right_saf/'. $this->Form->value('Client.id'). '.'. $dashboard2['0']['client_banners']['banner_background_image_right_extension']); ?>" alt=""  border="0" />
													<?php } ?>
												</td>
											</tr>
										</table>
									</div>										
								</div>
								<br />
								<div align="center">
									<button type="button" class="admin_button" onclick="document.getElementById('mytab4').tabber.tabShow(0)">Previous</button>
									<button type="button" class="admin_button" onclick="document.getElementById('mytab4').tabber.tabShow(2)">Next</button>
								</div>						
						</div>
						<div class="tabbertab"  style="min-height:600px;overflow:hidden;width:810px;">
							<h2>Points</h2>
								<table width="100%">
										<tr>
											<td colspan="3">
												<div style="background-color:#efefef;border:1px solid #cccccc;padding-left:15px;padding-top:10px;padding-bottom:10px;width:795px;margin-left:-15px;" >
													<table width="100%">
														<tr>
															<td class="bt br"></td>
															<td class="bt br" style="padding-left:2px"><strong>Left</strong></td>
															<td class="bt br" style="padding-left:2px"><strong>Right</strong></td>
														</tr>
														<tr>
															<td class="bt br"><strong>Image</strong></td>
															<td class="bt br">
																<?php echo $this->Form->file('Image.banner_background_image_left_3'); ?><br/>
																Remove <?php echo $this->Form->checkbox('ImageDelete.banner_background_image_left_points'); ?>
															</td>
															<td class="bt br">
																<?php echo $this->Form->file('Image.banner_background_image_right_3'); ?><br/>
																Remove <?php echo $this->Form->checkbox('ImageDelete.banner_background_image_right_points'); ?>
															</td>
														</tr>
														<tr>
															<td style="height:5px">&nbsp;</td>
														</tr>
														<tr>
															<td class="br" style="width:140px;"><strong>Align</strong></td>
															<td class="br"><?php echo $this->Form->input('Client.banner_left_align_3', array('onchange' => 'updateBanners();','default'=>$dashboard3['0']['client_banners']['banner_left_align'],'type'=>'select','options'=>$aligns,'div'=>false,'label'=>false)); ?></td>
															<td class="br"><?php echo $this->Form->input('Client.banner_right_align_3', array('onchange' => 'updateBanners();','default'=>$dashboard3['0']['client_banners']['banner_right_align'],'type'=>'select','options'=>$aligns,'div'=>false,'label'=>false)); ?></td>
														</tr>
														<tr>
															<td style="height:5px">&nbsp;</td>
														</tr>
														<tr>
															<td class="br bb"><strong>Repeat</strong></td>
															<td class="br bb"><?php echo $this->Form->input('Client.banner_left_repeat_3', array('onchange' => 'updateBanners();','default'=>$dashboard3['0']['client_banners']['banner_left_repeat'],'type'=>'select','options'=>$repeats,'div'=>false,'label'=>false)); ?></td>
															<td class="br bb"><?php echo $this->Form->input('Client.banner_right_repeat_3', array('onchange' => 'updateBanners();','default'=>$dashboard3['0']['client_banners']['banner_right_repeat'],'type'=>'select','options'=>$repeats,'div'=>false,'label'=>false)); ?></td>
														</tr>
														<tr><td style="height:5px">&nbsp;</td></tr>
														<tr>
															<td colspan="3"><b><span style="color:red">*Single banner : Upload only LEFT image with 950px width and 100px height</span></b></td> 
														</tr>
														<tr>
															<td colspan="3"><b><span style="color:red">*Two banners : Upload LEFT image with 475px width and 100px height and RIGHT image with 475px width and 100px height</span></b></td> 
														</tr>
														<tr><td colspan="3"style="height:5px">&nbsp;</td></tr>
													</table>
												</div>
											</td>
										</tr>
										<tr><td style="height:20px">&nbsp;</td></tr>
										<tr>
											<td>
												<h4 style="margin-left:-15px;">Header Banner Background colour</h4>
												<div style="background-color:#efefef;border:1px solid #cccccc;padding-left:15px;padding-top:10px;padding-bottom:10px;width:795px;margin-left: -15px;" >
													
													<table>
														<tr>
															<td class="br bb" style="width:136px;"><strong>Background colour</strong></td>
															<td class="br bb">
															<?php echo $this->Form->input('Client.banner_background_color_3', array('class' => 'color', 'id' =>'banner_background_color_3','value'=>$dashboard3['0']['client_banners']['banner_background_color'],'div'=>false,'label'=>false)); ?>
															<span style="background-color:#<?php echo $this->Form->value('Client.banner_background_color_3'); ?>"></span>
															</td>
															<td>&nbsp;</td>
														</tr>
													</table>
												</div>
											</td>
										</tr>
									</table>
												
									<script type="text/javascript">
			
									function updateBanners()
									 {
										$('left_banner').style.backgroundPosition = 'top ' + $('ClientBannerLeftAlign').value;
										$('left_banner').style.backgroundRepeat = $('ClientBannerLeftRepeat').value;
										$('right_banner').style.backgroundPosition = 'top ' + $('ClientBannerRightAlign').value;
										$('right_banner').style.backgroundRepeat = $('ClientBannerRightRepeat').value;
										$('center_banner2').style.backgroundPosition = 'top ' + $('ClientBannerAlign').value;
										$('center_banner2').style.backgroundRepeat = $('ClientBannerRepeat').value;
									 }
			
									</script>
														
									<h4 style="padding-top:20px;padding-bottom:10px;margin-left: -15px;">Header Text Alignment</h4>
									<div style="background-color: #EFEFEF;border:1px solid #cccccc;width:810px;margin-left: -15px;">
									<div style="padding:20px;"><?php $banner_option_3 = !empty($dashboard3['0']['client_banners']['banner_option'])?$dashboard3['0']['client_banners']['banner_option']:0;?>
										<?php echo $this->Form->radio('Client.banner_option_3', array('0'=> ''), array('class' => 'checkbox','value'=>$banner_option_3)); ?><strong> Centred</strong>
										<table style="border-left:none;width: 100%" cellpadding="0" cellspacing="0">
											<tr id="header_top">
												<td style="white-space:nowrap;text-align:center;">Welcome back <b>First_name Last_name</b> My favourires | My Account | Sign Out  | Admin</td>
											</tr>
											<tr>
												<td style=""height:5px">&nbsp;</td>
											</tr>
											<tr>
												<td align="right" style="border:1px solid #DFDFDF;padding:0;<?php
													if ($dashboard3['0']['client_banners']['banner_background_image_left_extension']!= '') {
														echo "background-image:  url('". $this->Html->url('/files/banner_background_image_left_points/'. $this->Form->value('Client.id'). '.'. $dashboard3['0']['client_banners']['banner_background_image_left_extension']). "');";
													}
			
													echo 'background-repeat: no-repeat;height:100px';
													?>">
													<?php if($dashboard3['0']['client_banners']['banner_background_image_right_extension'] !=''){ ?>
													<img src="<?php echo $this->Html->url('/files/banner_background_image_right_points/'. $this->Form->value('Client.id'). '.'. $dashboard3['0']['client_banners']['banner_background_image_right_extension']); ?>" alt=""  border="0" />
													<?php } ?>
												</td>
											</tr>
										</table>
										<br/>
										<?php echo $this->Form->radio('Client.banner_option_3', array('1'=> ''), array('class' => 'checkbox','value'=>$banner_option_3)); ?><strong> Left & right</strong>
										<table style="width: 100%;border:0;" cellpadding="0" cellspacing="0">
											<tr id="header_top">
												<td style="white-space:nowrap;text-align:right;">Welcome back <b>First_name Last_name</b> My favourires | My Account | Sign Out  | Admin</td>
											</tr>
											<tr>
												<td style=""height:5px">&nbsp;</td>
											</tr>
											<tr>
												<td align="right" style="border:1px solid #DFDFDF;padding:0;<?php
													if ($dashboard3['0']['client_banners']['banner_background_image_left_extension'] != '') {
														echo "background-image:  url('". $this->Html->url('/files/banner_background_image_left_points/'. $this->Form->value('Client.id'). '.'. $dashboard3['0']['client_banners']['banner_background_image_left_extension']). "');";
													}
			
													echo 'background-repeat: no-repeat;height:100px';
													?>">
													<?php if($dashboard3['0']['client_banners']['banner_background_image_right_extension'] !=''){ ?>
													<img src="<?php echo $this->Html->url('/files/banner_background_image_right_points/'. $this->Form->value('Client.id'). '.'. $dashboard3['0']['client_banners']['banner_background_image_right_extension']); ?>" alt=""  border="0" />
													<?php } ?>
												</td>
											</tr>
										</table>
									</div>										
								</div>
								<br />
								<div align="center">
									<button type="button" class="admin_button" onclick="document.getElementById('mytab4').tabber.tabShow(1)">Previous</button>
								</div>	
									</div>
								</div>
								<div align="center">
									<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(3)">Previous</button>
									<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(5)">Next</button>
								</div>
				</div>			 
				<div class="tabbertab" style="width:831px;">
					  		 	<div class="tabber" id="mytab5" style="margin-left: -59px; width: 847px;">
							 		<h2>NBanner</h2>
							 			<div class="tabbertab">
							 			<h2>My Rewards</h2>
							 	  			<table style="border:0;background-color:#EFEFEF;border:1px solid #cccccc;width:820px;height:200px; margin-left: -19px;">
					                      		<tr>
						                        	<td style="padding-left:20px;padding-top:20px">
							                        	<?php echo $this->Form->file('Image.newsletter_banner_image_1'); ?>
							                         	Remove <?php echo $this->Form->checkbox('ImageDelete.newsletter_banner_image_1'); ?>
						                         	</td>
					                      		</tr>
					                      		<tr>
					                      			<td style="height:15px"></td>
					                      		</tr>
						                      	<tr>
							                    	<td style="padding-bottom:20px;padding-left:20px;">
							                        	<img src="<?php echo $this->Html->url(FILES_WWW_PATH. 'clients/newsletter_banner_image_1/'. $dashboard['0']['client_banners']['newsletter_banner_image']); ?>" alt="" />
							                        </td>
						                      	</tr>
				                		    </table>
							 		        <br />
							 				<div align="center">
							 					<button type="button" class="admin_button" onclick="document.getElementById('mytab5').tabber.tabShow(1)">Next</button>
							 				</div>
				     		 	</div>
				 			 	<div class="tabbertab">
							 	<h2>Send A Friend</h2>
							  			<table style="border:0;background-color:#EFEFEF;border:1px solid #cccccc;width:820px;height:200px; margin-left: -19px;">
				                    		<tr>
					                        	<td style="padding-left:20px;padding-top:20px">
						                        	<?php echo $this->Form->file('Image.newsletter_banner_image_2'); ?>
						                         	Remove <?php echo $this->Form->checkbox('ImageDelete.newsletter_banner_image_2'); ?>
					                         	</td>
				                      		</tr>
				                      		<tr>
				                      			<td style="height:15px"></td>
				                      		</tr>
					                      	<tr>
						                    	<td style="padding-bottom:20px;padding-left:20px;">
						                        	<img src="<?php echo $this->Html->url(FILES_WWW_PATH. 'clients/newsletter_banner_image_2/'. $dashboard2['0']['client_banners']['newsletter_banner_image']); ?>" alt="" />
						                        </td>
					                      	</tr>
				                		</table>
								        <br />
										<div align="center">
											<button type="button" class="admin_button" onclick="document.getElementById('mytab5').tabber.tabShow(0)">Previous</button>
											<button type="button" class="admin_button" onclick="document.getElementById('mytab5').tabber.tabShow(2)">Next</button>
										</div>
				     			</div>
				     			<div class="tabbertab">
									<h2>Points</h2>
								  		<table style="border:0;background-color:#EFEFEF;border:1px solid #cccccc;width:820px;height:200px; margin-left: -19px;">
				                     		<tr>
					                        	<td style="padding-left:20px;padding-top:20px">
						                        	<?php echo $this->Form->file('Image.newsletter_banner_image_3'); ?>
						                         	Remove <?php echo $this->Form->checkbox('ImageDelete.newsletter_banner_image_3'); ?>
					                         	</td>
				                      		</tr>
				                      		<tr>
				                      			<td style="height:15px"></td>
				                      		</tr>
					                      	<tr>
						                    	<td style="padding-bottom:20px;padding-left:20px;">
						                        	<img src="<?php echo $this->Html->url(FILES_WWW_PATH. 'clients/newsletter_banner_image_3/'. $dashboard3['0']['client_banners']['newsletter_banner_image']); ?>" alt="" />
						                        </td>
					                      	</tr>
				                		</table>
								        <br />
										<div align="center">
												<button type="button" class="admin_button" onclick="document.getElementById('mytab5').tabber.tabShow(1)">Previous</button>
										</div>
				     			</div>
				     		</div>
						<div align="center">
							<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(4)">Previous</button>
							<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(6)">Next</button>
						</div>
				     </div>

    				<div class="tabbertab" style="min-height:100px;overflow:hidden;">
					<h4>ErrorLogin/FirstTimeLogin Text</h4>
					<div style="background-color:#EFEFEF;border:1px solid #cccccc;padding:10px;width:790px;min-height:100px;overflow:hidden">
						<table>
							<tr>
								<td><strong>Login Error Text</strong></td>
							</tr>
							<tr>
								<td>
									<?php echo $this->Form->input('Client.login_error_text', array('div'=>false,'label'=>false)); ?>
								</td>
							</tr>
						</table>
					</div>
					<br />
					<div style="background-color:#EFEFEF;border:1px solid #cccccc;padding:10px;width:790px;min-height:100px;overflow:hidden">
						<table>
							<tr>
								<td><strong>First Time Login Text</strong></td>

							</tr>
							<tr>
								<td>
									<?php echo $this->Form->input('Client.first_time_login_txt', array('div'=>false,'label'=>false)); ?>
								</td>
							</tr>
						</table>
					</div>
					<br/>
					<?php echo $this->Form->input('Client.fb_share_disabled', array('onclick'=>"dispFBShare('fbShareText', this)")); ?> Share on Facebook
					<?php $fb_text_disp = ($this->data['Client']['fb_share_disabled']==1)?"":"none";?>
					<div id="fbShareText" style="background-color:#EFEFEF;border:1px solid #cccccc;padding:10px;width:790px;min-height:100px;overflow:hidden;display:<?php echo $fb_text_disp?>">
						<table>
							<tr>
								<td><strong>Facebook Share Description Text</strong></td>
							</tr>
							<tr>
								<td><?php echo $this->Form->input('Client.fb_description_txt', array('div'=>false,'label'=>false)); ?></td>
							</tr>
						</table>
					</div>
					<br/><?php echo $this->Form->input('Client.payroll_deduct_disabled', array('div'=>false,'label'=>false)); ?> Payroll Deduct
					<br/><?php echo $this->Form->input('Client.referral_no_disabled', array('div'=>false,'label'=>false)); ?> Referral Number
							
					<div align="center">
						<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(5)">Previous</button>
						<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(7)">Next</button>
					</div>
				</div>
					
				<div class="tabbertab">
					<h2>Comments</h2>
					<div class="list" id="comment_list" style="float:none !important;">
						<div class="edit_new" style="border:1px solid #cccccc;padding:10px;">
							<h4>Comments</h4><br/><br/>
						<div id="Comments" style="margin:0;">Retrieving comments...</div>
						<script type="text/javascript">
						$(function() {
							/*$.ajax({
								url: "/admin/clients/ajax_comments/<?php echo $this->Form->value('Client.id');?>",
								type: "GET",
								success: function (data) {
									$('#Comments').html(data);
								}
							});*/
						});</script>
						</div>
					</div>	
					<br />
					<div align="center">
						<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(6)">Previous</button>
						<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(8)">Next</button>
					</div>
     				</div>
     				<?php if($this->Session->read('user.User.type') == 'Administrator'):?>
				<div class="tabbertab" style="min-height:100px;overflow:hidden;">
					<h2>Dashboard</h2>
					<h4>Dashboard</h4>
					<div style="background-color:#EFEFEF;border:1px solid #cccccc;padding:10px;width:790px;min-height:100px;overflow:hidden">
						<table>
							<tr>
								<td><strong>Dashboard Type</strong></td>
								<td><strong>Default Landing Page</strong></td>
							</tr>
							<tr>
								<td>
									<?php
										echo $this->Form->input('Dashboard.Dashboard',array('type'=>'select','multiple'=>'checkbox','label'=>false,'options'=>array('1' => 'My Rewards','2' => 'Send A Friend','3'=>'My Points')));
									?>
								</td>
								<td>
									<?php echo $this->Form->input('Client.dashboard', array('type' => 'radio', 'options' => array('1'=>'', '2'=>'', '3'=>''),'class'=>'input_radio','separator'=>'<br><br><br>','legend'=>false));?>
								</td>
							</tr>
						</table>
					</div>
				</div>
				<?php endif; ?>
     				<?php if ($this->Html->value('Client.id') != '0') : ?>
				     				<div class="tabbertab" style="width:831px;">
					  					<div class="tabber" id="mytab4" style="margin-left: -59px; width: 847px;">
					  							<h2>Client Pages</h2>
												<div class="tabbertab">
													<h2>My Rewards</h2>
													<div class="edit_new">
														<table>
															<tr>
																<td style="vertical-align:top">
																	<div>
																		<div class="categories" style="margin-right: 5px;margin-left:-17px; padding: 10px; width: 510px;height:358px;">
																			<h4>Member Pages</h4>
																			<?php echo $this->Tree->showPages('Page.title', $client_pages, null, '/admin/client_pages/edit/'. $this->Form->value('Client.id'). '/'. $this->Form->value('Client.program_id'). '/', false, 'Page.id',1); ?> 
																		</div>
																		<div class="categories" style="padding: 10px; width: 252px;height:358px;">
																			<h4>Public Pages</h4>
																			<div>
																				<?php echo $this->Tree->showPages('Page.title', $client_public_pages, null, '/admin/client_pages/edit/'. $this->Form->value('Client.id'). '/'. $this->Form->value('Client.program_id'). '/', false, 'Page.id',1); ?> 
																			</div>
																		</div>
																	</div>
																</td>
															</tr>
														</table>
												   </div>
													<br />
													<div align="center">
														<button type="button" class="admin_button" onclick="document.getElementById('mytab4').tabber.tabShow(1)">Next</button>
													</div>
						     						                            					
						     					</div>
						     					<div class="tabbertab">
						     					     <h2>Send A Friend</h2>
						     					     <div class="edit_new">
															<table>
																<tr>
																	<td style="vertical-align:top">
																		<div>
																			<h3>Client Pages</h3>
																			<div class="categories" style="margin-right: 5px;margin-left:-17px; padding: 10px; width: 510px;height:358px;">
																				<h4>Send A Friend Pages</h4>
																				<?php echo $this->Tree->showPages('Page.title', $client_pages_sendafriend, null, '/admin/client_pages/edit/'. $this->Form->value('Client.id'). '/'. $this->Form->value('Client.program_id'). '/', false, 'Page.id',2); ?> 
																			</div>
																			<div class="categories" style="padding: 10px; width: 252px;height:358px;">
																				<h4>Public Pages</h4>
																				<div>
																					<?php echo $this->Tree->showPages('Page.title', $client_public_pages_sendafriend, null, '/admin/client_pages/edit/'. $this->Form->value('Client.id'). '/'. $this->Form->value('Client.program_id'). '/', false, 'Page.id',2); ?> 
																				</div>
																			</div>
																		</div>
																	</td>
																</tr>
															</table>
													</div>
												<br />
												<div align="center">
													<button type="button" class="admin_button" onclick="document.getElementById('mytab4').tabber.tabShow(0)">Previous</button>
													<button type="button" class="admin_button" onclick="document.getElementById('mytab4').tabber.tabShow(2)">Next</button>
												</div>
						     				</div>
						     				<div class="tabbertab">
						     					     <h2>Points</h2>
						     					     <div class="edit_new">
															<table>
																<tr>
																	<td style="vertical-align:top">
																		<div>
																			<h3>Client Pages</h3>
																			<div class="categories" style="margin-right: 5px;margin-left:-17px; padding: 10px; width: 510px;height:358px;">
																				<h4>Points Pages</h4>
																				<?php echo $this->Tree->showPages('Page.title', $client_pages_points, null, '/admin/client_pages/edit/'. $this->Form->value('Client.id'). '/'. $this->Form->value('Client.program_id'). '/', false, 'Page.id',3); ?> 
																			</div>
																			<div class="categories" style="padding: 10px; width: 252px;height:358px;">
																				<h4>Public Pages</h4>
																				<div>
																					<?php echo $this->Tree->showPages('Page.title', $client_public_pages_point, null, '/admin/client_pages/edit/'. $this->Form->value('Client.id'). '/'. $this->Form->value('Client.program_id'). '/', false, 'Page.id',3); ?> 
																				</div>
																			</div>
																		</div>
																	</td>
																</tr>
															</table>
													</div>
												<br />
												<div align="center">
													<button type="button" class="admin_button" onclick="document.getElementById('mytab4').tabber.tabShow(1)">Previous</button>
												</div>
						     				</div>
						     			</div>
						     			
						     			<div align="center">
						     					<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(6)">Previous</button>
						     					<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(8)">Next</button>
										</div>
						     		</div>	
						     		
			 			 <div class="tabbertab">
							<h2>Log</h2>
							<div class="edit_new">
								<table>
									<tr>
										<td style="vertical-align:top">
											<div class="list" id="log_list">
												<h4>Change Log</h4><br/><br/>
												<div id="Log" style="margin:0;">Retrieving changes...</div>
												<script type="text/javascript">
												$(function() {
													$.ajax({
														url: "/admin/logs/ajax_list_new/<?php echo Inflector::singularize($this->name);?>/<?php echo $this->Form->value(Inflector::singularize($this->name). '.id');?>",
														type: "GET",
														success: function (data) {
															$('#Log').html(data);
														}
													});
												});</script>
											</div>
										</td>
									</tr>
								</table>
							</div>
							<br />
							<div align="center">
								<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(7)">Previous</button>
							</div>
						</div>
				<?php endif; ?>
		</div>
	</div>	
</form>