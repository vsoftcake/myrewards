<style>
.current
{
    font-family: 'Droid Sans',sans-serif;
    font-size: 12px;
   	padding:5px 8px !important;   
    background-color: #0066CC;
    border: 1px solid #DFDFDF;
    color:#ffffff;
}
.cpagination1
{
	/*padding:1px 2px 1px 2px;*/
	font-family: 'Droid Sans',sans-serif;
    font-size: 12px;
	text-decoration:none !important;
	background-color: #FFFFFF;
	padding:3px 8px;
	 border: 1px solid #0066CC;
	 color:#808080 !important;
}
#content
{
border-left:1px solid #cccccc;
border-right:1px solid #cccccc;
border-bottom:1px solid #cccccc;
margin-left:20px !important;
padding-left:15px !important;
width:916px;
}
.admin_button
	{
	height: 24px;
	padding-top: 2px;
	padding-bottom: 5px;
	border-radius: 5px 5px 5px 5px;
	background-color: #0066cc;
	border: 1px solid #cccccc;
	color: #FFFFFF; 
	cursor: pointer;
	font-size:13px;
	}

</style>

<table style="margin-left:-2px;">
	<tr>
		<td>
            &nbsp;&nbsp;<img style="width:40px;height:40px;" src="/files/admin_home_icons/clients.png"  alt="Clients" title="Clients" />&nbsp;&nbsp;
        </td>
        <td> 
            <h2><?php echo "Clients";?></h2>
        </td>
    </tr>        
</table>
<?php if($this->Session->read('user.User.type') != 'Client Admin' && $this->Session->read('user.User.type') != 'Country Admin') :?>
<table style="width:927px;background-color:#cccccc;height:85px;margin-left:-2px;border:1px solid #969090;">
	<tr>
		<td style="padding-top:5px;">
			<form action="<?php echo $this->Html->url('/admin/clients/'); ?>" method="get" id="SearchForm">
				<table>
					<tr>
						<td valign="top">
							<table>
								<tr>
									<td style="padding-left:5px;"><strong>Find Clients</strong></td>
									<td style="padding-left:4px;">
										<?php echo $this->Form->input('Client.search', array('div'=>false, 'label'=>false)); ?>
										<script type="text/javascript">
										$(function() {
											if ($('#ClientSearch').val() == '') {
												$('#ClientSearch').val('ID or name');
												$('#ClientSearch').css( "color", "#aaaaaa" );
											}
											$( "#ClientSearch" ).focus(function() {
												if ($(this).val() == 'ID or name') {
													$(this).val('');
													$(this).css( "color", "#000" );
												}
											});
											$( "#SearchForm" ).submit(function() {
												if ($("#ClientSearch").val() == 'ID or name') {
													$("#ClientSearch").val('');
													$("#ClientSearch").css( "color", "#000" );
												}
											});
										});
										</script>
									</td>
								</tr>
							</table>
						</td>
						<td style="width:10px;">
						
						</td>
						<td>
							<table>
								<tr>
									<td><strong>Limit to Programs</strong></td>
									<td style="padding-left:4px;"><?php echo $this->Form->input('Client.program_id', array('div'=>false,'label'=>false, 'type'=>'select', 'options'=>$programs, 'empty'=>'')); ?></td>
									<td style="padding-left:3px;"><button class="admin_button">Search</button></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>    	
		</form>
	    </td>
			
	    <td style="padding-top:20px;">
			<button style="margin-right:50px;border:none;" type="submit" onclick="return addClient();" id="addClient" class="admin_button">Create New Client</button>
			<div style="padding-bottom:15px;">
			<form id="addClientForm" action="<?php echo $this->Html->url('/admin/clients/edit/0/'); ?>" method="get" style="display:none">
				<div style="padding-bottom:5px;padding-left:4px;"><strong>Create client for:</strong></div>
				<?php echo $this->Form->input('Client.program_id', array('div'=>false,'label'=>false)); ?>
				<button type="submit" class="admin_button">Go</button>
			</form>
			</div>

			<script type="text/javascript">
				function addClient() {
					$('#addClient').hide();
					$('#addClientForm').show();
				}
			</script>
           </td>
     </tr>
</table>
<?php endif; ?>	
<br/>
	
<div class="list">
	
	<table cellpadding="0" cellspacing="0" style="width:927px;margin-left:-2px;">
	<tr style="height:30px;background-color:#cccccc;">
		<th style="background-color:#cccccc;text-align:left;padding-left:5px;border-left:1px solid #969090;border-bottom:1px solid #969090;border-top:1px solid #969090;"><?php echo $this->Paginator->sort('name');?></th>
		<th style="background-color:#cccccc;text-align:left;padding-left:5px;border-bottom:1px solid #969090;border-top:1px solid #969090;"><?php echo $this->Paginator->sort('description');?></th>
		<th style="background-color:#cccccc;text-align:left;padding-left:4px;border-bottom:1px solid #969090;border-top:1px solid #969090;">Program</th>
		<th style="background-color:#cccccc;text-align:left;padding-left:5px;border-bottom:1px solid #969090;border-top:1px solid #969090;">Style</th>
		<th class="actions" style="background-color:#cccccc;text-align:left;padding-left:4px;border-right:1px solid #969090;border-bottom:1px solid #969090;border-top:1px solid #969090;"><?php echo "Actions";?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($clients as $client): ?>
		<?php if ($client['Client']['password'] != 'SBDummy@#%#' ) : ?>
		<?php
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
			<?php if($client['Client']['name']!='') :?>
				<tr<?php echo $class;?>>
				        
					<td style="width:230px;">
						<?php echo $client['Client']['name'] ?>
					</td>
					<td style="width:200px">
						<?php echo $client['Client']['description'] ?>
					</td>
					<td style="width:180px">
						<?php echo $client['Program']['name']; ?>
					</td>
					<td style="width:220px">
						<?php echo $client['Style']['name']; ?>
					</td>
					<td class="actions">
						<?php echo $this->Html->link(__('Edit', true), array('action'=>'edit', $client['Client']['id'])); ?> |
						<?php echo $this->Html->link(__('Delete', true), array('action'=>'delete', $client['Client']['id']), null, sprintf(__('Are you sure you want to delete Client \'%s\'?', true), $client['Client']['name'])); ?>
					</td>
				</tr>
			<?php endif; ?>
		<?php endif; ?>
	<?php endforeach; ?>
	</table>
	
	<br/>
	<table style="border:none;width:932px;">
			<tr>
				
				<td style="border:none;float:right;" valign="top">
					<div class="paging" style="padding-top:7px;">
						<table style="border:none !important;">
							<tr>
								<td style="border:none !important;" valign="top">
									<?php echo $this->Paginator->first('<<', array('title'=>'first','class'=>'disabled cpagination1'));?>
									<?php echo $this->Paginator->prev('<', array('title'=>'prev','class'=>'disabled cpagination1'));?>
									<?php echo $this->Paginator->numbers(array('separator' => ' ','class'=>'cpagination1'));?>
									<?php echo $this->Paginator->next('>', array('title'=>'next','class'=>'disabled cpagination1'));?>
									<?php echo $this->Paginator->last('>>', array('title'=>'last','class'=>'disabled cpagination1'));?>
								</td>
								<td style="border:none !important;padding-top:5px;">
								<select name="page" id="selectpage" style="margin-top:-5px;">
									<?php for ($i = 1; $i <= $this->Paginator->counter(array('format' => '%pages%')); $i++):?>
									<option value="<?php echo $i;?>"<?php echo ($this->Paginator->current($params['page']) == $i) ? ' selected="selected"' : ''?>><?php echo $i ?></option>
									<?php endfor ?>
								</select>
								<script>
								$("#selectpage").change(function() {
									var url = window.location.href;
									var n = url.indexOf("/?");
									var extra = "";
									if(n>0){extra = url.substring(n,url.length);}
									window.location.href = '/admin/clients/index/page:'+$(this).val()+extra;
								});
								</script>
								</td>
							</tr>
						</table>
					</div>
				</td>
			</tr>
		</table>
	<br/>
</div>