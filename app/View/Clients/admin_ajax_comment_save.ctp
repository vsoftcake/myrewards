
<?php 
echo str_replace("'", "\'",$this->Form->error('ClientComment.description', array('required' => 'Description required','name'=>'data[ClientComment][description]'))); ?>
<script type="text/javascript">
<?php if (isset($error)) : ?>
	
	$('message').innerHTML = '<div id="flashMessage" class="message">Please correct the errors below.</div>';
	$('NewClientComment').style.display = 'table-row';
	$('NewClientComment2').style.display = 'table-row';
	$('NewClientCommentSaving').style.display = 'none';

<?php else : ?>
	$('#message').html('<div id="flashMessage" class="message">Comment saved.</div>');
	$('#NewClientCommentSaving').hide();
	$('#Comments').html('Retrieving comments...');
	$.ajax({
		url: '/admin/clients/ajax_comments/<?php echo $id;?>',
		type: "GET",
		success: function (data) {
		
			$('#Comments').html(data);
		}
	});
<?php endif; ?>
</script> 