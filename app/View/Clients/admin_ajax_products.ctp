	<?php $this->Paginator->options(
		array(
			'update' => '#products',
			'evalScripts' => true,
			'url' => $this->params['pass'],
			'model'=>'Client',
			'sort' => 'Product.name'));
			
	?>
	<table cellpadding="0" cellspacing="0">
	<tr>
		<th><?php echo $this->Paginator->sort('id', 'Product.id');?></th>
		<th><?php echo $this->Paginator->sort('Name', 'Product.name');?></th>
		<th class="actions"><?php echo "Actions";?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($clients_product as $product):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
		<tr<?php echo $class;?>>
			<td>
				<?php echo $product['Product']['id'] ?>
			</td>
			<td>
				<?php echo $product['Product']['name'] ?>
			</td>
			<td class="actions">
				<?php echo $this->Html->link(__('Edit', true), '/admin/products/edit/'. $product['Product']['id']); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
		<div class="paging">
		<?php echo $this->Paginator->prev('<< Previous', null, null, array('class'=>'disabled'));?>
		<?php echo $this->Paginator->numbers();?>
		<?php echo $this->Paginator->next('Next >>', null, null, array('class'=>'disabled'));?>
	</div>
	<p><?php echo $this->Paginator->counter(array('format' => __('Page %page% of %pages%', true)));echo $this->Js->writeBuffer();	?></p>
