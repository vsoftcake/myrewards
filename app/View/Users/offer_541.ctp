<div class="main">
	<?php echo $this->element('message'); ?>

	<div class="module">
		<div class="tl">
			<div class="tr">
				<div class="t">
					541 Offer : Your Family members can join FREE!
						
				</div>
			</div>
		</div>
		<div class="content">
			<form action="<?php echo $this->Html->url('/users/offer_541'); ?>" method="post" class="standard">
				<table>
				
					<tr>
						<td colspan="3">
						<h1>5-4-1 Offer</h1>
						<h3>Receive 5 memberships for the price of 1! </h3>
						<h3>My Rewards gives more bang for your membership buck your family members can join for FREE!</h3>
						<p>My Rewards not only gives you access to over 6,000 merchant discounts Australia wide but also gives <b>free membership access to up to 4 other family members</b>. Your family members can enjoy the same access and benefits and save dollars.</p>
						<p><b>How does your family get membership, valued at $99 for free?</b></p>
						<p>Easy! Simply enroll up to 4 members below along with their email address and we will send them an invite to join My Rewards for FREE.</p>
						<p>Once they accept they will then be able to login, and access a range of discounts with the click of a finger. They will be able to print coupons, purchase products online, and sign up to monthly e-news that showcases new and special offers. </p>
						<p>They can even choose to get a membership card and benefits book if they want one for just a small fee!*</p>
						<p>* Please see website Terms & Conditions for cost.</p>
						<p><h3>What are you waiting for Reward the ones you love now!</h3></p>
						<p>Complete the form, press submit and they will receive an instant invitation to join:</p>
								
						
					</tr>
					<tr>
						<td>First Name:</td>
						<td><?php echo $this->Form->input('User.first_name'); ?></td>
						<td><?php echo $this->Form->error('User.first_name', array('required' => 'First name required'), array('required' => 'First name required')); ?></td>
					</tr>
					<tr>
						<td>Last Name:</td>
						<td><?php echo $this->Form->input('User.last_name'); ?></td>
						<td><?php echo $this->Form->error('User.last_name', array('required' => 'Last name required'), array('required' => 'Last name required')); ?></td>
					</tr>
					<tr>
						<td>Email Address</td>
						<td><?php echo $this->Form->input('User.email'); ?></td>
						<td class="bl">
							<?php echo $this->Form->error('User.email', array('valid_email' => 'Invalid email address', 'required' => 'Email address required to receive newsletter'), array('valid_email' => 'Invalid email address	')); ?>
						</td>
					</tr>
					
					<tr>
						<td></td>
						<td colspan="2"><button type="submit">Submit &gt;&gt;</button></td>
					</tr>
														
				</table>
			</form>
			&nbsp;
		</div>
		<div class="bl">
			<div class="br">
				<div class="b">&nbsp;
				</div>
			</div>
		</div>
	</div>
	
</div>
