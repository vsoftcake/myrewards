<?php if ($step == '1') : ?>
<h2>Delete Users</h2>
<div class="actions">
	<ul>
		<li><?php echo $this->Html->link('Cancel', '/admin/users'); ?></li>
	</ul>
</div>
<div class="edit">
<p>
	File must be in csv format with the following columns (comumn name must be in the first row):<br/>
	<ul>
		<li>client - client name (required) </li>
		<li>username - users username (required)</li>
	</ul>
	Other columns can be included, but they will be ignored (usefull if you want to delete a prevoiusly uploaded csv file of users)
</p>
<form action="<?php echo $this->Html->url('/admin/users/delete_multiple/2'); ?>" method="post" enctype="multipart/form-data">
	<table>
		<!--
		<tr>
			<td class="bt">Client</td>
			<td class="bt"><?php echo $this->Form->input('User.client_id', array('div'=>false, 'label'=>false, 'type'=>'select', 'options'=>$clients, 'empty'=>'Please select...')); ?></td>
			<td class="bl"><?php echo $this->Form->error('User.client_id', array('required' => 'Select a client')); ?></td>
		</tr>
		-->
		<tr>
			<td class="bt">CSV file</td>
			<td class="bt"><?php echo $this->Form->file('User.users'); ?></td>
			<td class="bl"><?php if (isset($upload_error)) { echo $upload_error; } ?></td>
		</tr>
		<tr>
			<td class="bb"></td>
			<td class="bb br"><button type="submit">Submit</button></td>
		</tr>
	</table>
	</form>
</div>
<?php elseif ($step == '2') : ?>
<h2>Confirm delete</h2>
<div class="actions">
	<ul>
		<li><?php echo $this->Html->link('Cancel', '/admin/users'); ?></li>
	</ul>
</div>
	<br/>
	<?php echo $uploaded_users; ?> users uploaded.<br/>
	<?php echo $delete_users; ?> users will be deleted.
	
<div style="clear:both">
	<p><a href="<?php echo $this->Html->url('/admin/users/delete_multiple/3'); ?>">Confirm</a></p>
</div>

<?php elseif ($step == '3') : ?>
<h2>delete complete</h2>
<div class="actions">
	<ul>
		<li><?php echo $this->Html->link('Done', '/admin/users'); ?></li>
	</ul>
</div>
<?php echo $user_count; ?> users deleted.
<?php endif; ?>