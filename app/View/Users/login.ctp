<div style="padding:10px;border:1px solid #BBB9C4;margin-right:10px">
	<?php echo $this->element('message'); ?>
	<div class="tab_padding">
		<div style="font-size:15px;">
			Login
		</div>
		
		<div>
			
				<?php if(isset($registered_error)) : ?>
					<p>You have not yet registered, please go to the <a href="<?php echo $this->Html->url('/users/first_time_login'); ?>">first time login page</a> to register.</p>
				<?php endif; ?>
				<form action="<?php echo $this->Html->url('/users/login');?>" method="post" class="standard">
					<table>
						<tr>
							<td>Membership number:</td>
							<td>
								<?php echo $this->Form->input('User.username',array('class'=>'search_input', 'label'=>false, 'div'=>false,
										'error' => array('attributes' => array('wrap' => 'span', 'class' => 'form_error_message')))); ?>
							</td>
						</tr>
						<tr>
							<td>Password:</td>
							<td>
								<?php echo $this->Form->input('User.password',array('class'=>'search_input', 'label'=>false, 'div'=>false,
										'error' => array('attributes' => array('wrap' => 'span', 'class' => 'form_error_message')))); ?>
							</td>
						</tr>
						<tr>
							<td style="text-align:right"><?php echo $this->Form->checkbox('User.rember'); ?></td>
							<td>Remember me on this computer.</td>
						</tr>
						<tr>
							<td></td>
							<td><button type="submit" class="button_search_module rounded_corners_button">Login</button></td>
						</tr>
						<tr>
							<td></td>
							<td><br/><?php echo $this->Html->link('Forgot your password?',array('controller'=>'users','action' => 'forgot_password'));?>
							</td>
						</tr>
					</table>
				</form>
				&nbsp;
				<?php if($login_error == 1) :?>
					<?php if(!empty($error_text['Client']['login_error_text']) || $error_text['Client']['login_error_text']!=''):?>
						<?php echo $error_text['Client']['login_error_text'];?>
					<?php else : ?>
							<div>
								<span style="color:#FF00FF;font-size:14px;font-weight:bold">Ooops! ...something is not quite right!!!</span><br/>
								<p>Your Login details are incorrect...</p>
								<p>Please try again to login <a href="/users/login"><strong>Click here to login</strong></a></p>				
							</div>
					<?php endif;?>
				<?php endif;?>
			
		</div>
	</div>
<!--<?php if ($this->data['User']['username'] != '') : ?>
	<?php if ($page['ClientPage']['h1'] != '') : ?>
		<h1><?php echo $page['ClientPage']['h1']; ?></h1>
	<?php endif; ?>
	<?php if ($page['ClientPage']['h2'] != '') : ?>
		<h2><?php echo $page['ClientPage']['h2']; ?></h2>
	<?php endif; ?>
	<?php if ($page['ClientPage']['h3'] != '') : ?>
		<h3><?php echo $page['ClientPage']['h3']; ?></h3>
	<?php endif; ?>

	<?php if ($page['ClientPage']['content'] != '') : ?>
		<?php echo $page['ClientPage']['content']; ?>
	<?php endif; ?>
<?php endif; ?>-->
</div>