<style>#MB_window {min-height:400px !important;overflow:hidden !important;}</style>
<div style="padding:10px;border:1px solid #BBB9C4;margin-right:10px">
	<div class="tab_padding">
		<div style="font-size:15px;">
			<?php echo $this->element('message'); ?>
			<?php if ($page['ClientPage']['h1'] != '') : ?>
				<h1><?php echo $page['ClientPage']['h1']; ?></h1>
			<?php endif; ?>
			<?php if ($page['ClientPage']['h2'] != '') : ?>
				<h2><?php echo $page['ClientPage']['h2']; ?></h2>
			<?php endif; ?>
			<?php if ($page['ClientPage']['h3'] != '') : ?>
				<h3><?php echo $page['ClientPage']['h3']; ?></h3>
			<?php endif; ?>
			<?php if ($page['ClientPage']['content'] != '') : ?>
				<?php echo $page['ClientPage']['content']; ?>
			<?php endif; ?>
		</div>
			
		<div>
			<?php if ($step == 2) { ?>
				<p><b>Step 2</b>: Choose a password and enter your personal details. * Denotes mandatory fields.
				<?php  if (isset($_SERVER['HTTP_USER_AGENT']) && (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 9.0') !== false)) { ?>
					<form onsubmit="checkconditions(this)" action="<?php echo $this->Html->url('/users/first_time_login/2'); ?>" id="UsersFirstTimeLoginForm2" method="post">
						<?php if ($this->Session->read('client.Client.id') == '1012'|| $this->Session->read('client.Client.id') == 735) : ?>
						<span style="font-size:12px;">
							<span style="margin-right:70px;"><b>Chartis Policy Number:</b></span>
							<?php echo $this->Form->input('User.policy', array('div'=>false, 'label'=>false)); ?>&nbsp;*<br>
							<?php echo $this->Form->error('User.policy', array('required' => 'Policy Number required'), array('required' => 'Policy Number required')); ?>
						</span>
						<?php endif; ?>
						<br/><br/>
						<span style="font-size:12px;">
							<span style="float:left">
								<span style="margin-right:70px;"><b>Password:</b></span>
								<?php echo $this->Form->input('User.password',array('class' => 'first_time_input', 'div'=>false, 'label'=>false)); ?>&nbsp;*<br>
								<?php echo $this->Form->error('User.password', array('length' => 'Password must be 6 characters or more')); ?>
							</span>
							<span style="padding-left:40px;float:left">
								<span style="margin-right:25px;"><b>Confirm password:</b></span>
								<?php echo $this->Form->input('User.password2', array('type' => 'password','class' => 'first_time_input', 'div'=>false, 'label'=>false)); ?>&nbsp;*<br>
								<?php echo $this->Form->error('User.password', array('required' => 'Password does not match')); ?>
							</span>
						</span>
						<br/><br/>
						<span style="font-size:12px;">
							<span style="float:left">
								<span style="margin-right:60px;"><b>First Name:</b></span>
								<?php echo $this->Form->input('User.first_name',array('class' => 'first_time_input', 'div'=>false, 'label'=>false)); ?>&nbsp;*<br>
								<?php echo $this->Form->error('User.first_name', array('required' => 'First name required'), array('required' => 'First name required')); ?>
							</span>
							<span style="padding-left:40px;float:left">
								<span style="margin-right:68px;"><b>Last Name:</b></span>
								<?php echo $this->Form->input('User.last_name',array('class' => 'first_time_input', 'div'=>false, 'label'=>false)); ?>&nbsp;*<br>
								<?php echo $this->Form->error('User.last_name', array('required' => 'Last name required'), array('required' => 'Last name required')); ?>
							</span>
						</span>
						<br/>
						<span style="font-size:12px;">
							<span style="float:left">
								<span style="margin-right:90px;"><b>Email:</b></span>
								<?php echo $this->Form->input('User.email',array('class' => 'first_time_input', 'div'=>false, 'label'=>false)); ?>&nbsp;*<br>
								<?php echo $this->Form->error('User.email', array('valid_email' => 'Invalid email address', 'required' => 'Email address required to receive newsletter'), array('valid_email' => 'Invalid email address'), array('style'=>'height:25px')); ?>
							</span>
						<?php if ($this->Session->read('client.Client.id') != 1469 && $this->Session->read('client.Client.id') != 1473 ) : ?>
							<span style="padding-left:40px;">
								<span style="margin-right:40px;"><b>Mobile Number:</b></span>
								<?php echo $this->Form->input('User.mobile',array('class' => 'first_time_input', 'div'=>false, 'label'=>false)); ?><br>
								<?php echo $this->Form->error('User.mobile', array('valid_mobile' => 'Invalid mobile number', 'required' => 'mobile number is required to avail sms feature'), array('valid_mobile' => 'Invalid mobile number ')); ?>
							</span>
							<?php endif; ?>
							<br/>
						</span>
						<?php if ($this->Session->read('tmp_client.Client.payroll_deduct_disabled') == 1 || $this->Session->read('tmp_client.Client.referral_no_disabled') == 1) : ?>
						<br/>
						<span style="font-size:12px;">
							<?php if ($this->Session->read('tmp_client.Client.payroll_deduct_disabled') == 1) : ?>
							<span style="float:left">
								<span style="margin-right:90px;"><b>Payroll Number:</b></span>
								<?php echo $this->Form->input('User.payroll_deduct',array('class' => 'first_time_input', 'div'=>false, 'label'=>false)); ?>&nbsp;*<br>
								<?php echo $this->Form->error('User.payroll_deduct', array('required' => 'Payroll Number Required'),array('required' => 'Payroll Number Required')); ?>
							</span>
							<?php endif;
							if ($this->Session->read('tmp_client.Client.referral_no_disabled') == 1) : ?>
							<span style="padding-left:40px;">
								<span style="margin-right:40px;"><b>Referral Number:</b></span>
								<?php echo $this->Form->input('User.referral_no',array('class' => 'first_time_input', 'div'=>false, 'label'=>false)); ?><br>
								<?php echo $this->Form->error('User.referral_no', array('required' => 'Referral Number Required'),array('required' => 'Referral Number Required')); ?>
							</span>
							<?php endif;?><br/>
						</span>
						<?php endif; ?>
						<br/>
						<?php if ($this->Session->read('client.Client.id') == 837) : ?>
						<span style="font-size:12px;">
							<span style="margin-right:70px;"><b>Hospital Name:</b></span>
							<?php echo $this->Form->input('User.hospital_name', array('div'=>false, 'label'=>false)); ?>&nbsp;*<br>
							<?php echo $this->Form->error('User.hospital_name', array('required' => 'Hospital Name required'), array('required' => 'Hospital Name required')); ?>
						</span>
						<?php endif; ?>
						<br/><br/>
						<span style="font-size:12px;">
							<span style="float:left">
								<span style="margin-right:12px"><b>Receive Newsletter:</b></span>
								<span style="margin-right:50px">
									<?php echo $this->Form->radio('User.newsletter', array(1 => 'Yes', 0 => 'No'), array('style' => 'vertical-align:middle;height:14px')); ?>
								</span>
							</span>
						<?php if ($this->Session->read('client.Client.id') != 1469 && $this->Session->read('client.Client.id') != 1473 ) : ?>
							<span style="padding-left:135px;">
								<span style="margin-right:90px"><b>D.O.B:</b></span>
								<?php echo $this->Form->dateTime ('User.dob', 'DMY', 'NONE', null, array('style' => 'width:auto',  'minYear' => date('Y')-100, 'maxYear' => date("Y")-10)); ?>
							</span>
							<?php endif; ?>
						</span>
						<br/><br/>
						<?php if ($this->Session->read('client.Client.id') != 1469 && $this->Session->read('client.Client.id') != 1473 ) : ?>
						<span style="font-size:12px;">
							<b>Interests:</b>
							<span id="TableInterests">
									<span style="margin-left: 68px; margin-right: 98px;"><?php echo $this->Form->checkbox('User.int_all', array('onClick' => 'ToggleStates(this)')); ?> All</span>
									<span style="margin-right:26px"><?php echo $this->Form->checkbox('User.int_travel'); ?> Travel & Accommodation</span>
									<span><?php echo $this->Form->checkbox('User.int_shopping'); ?> Shopping</span><br/>
									<span style="margin-left: 125px; margin-right: 15px;"><?php echo $this->Form->checkbox('User.int_dining'); ?> Dining & FastFood</span>
									<span style="margin-right:30px"><?php echo $this->Form->checkbox('User.int_leisure'); ?> Leisure & Entertainment</span>
									<span style="margin-right:30px"><?php echo $this->Form->checkbox('User.int_beauty'); ?> Health & Beauty</span>
									<span style="margin-right:30px"><?php echo $this->Form->checkbox('User.int_auto'); ?> Auto</span>
								</span>
						</span>
						<?php endif; ?>

						<br/><br/>
						<span style="font-size:12px;">
							<span style="margin-right:82px;"><b>Country</b></span>
							<?php 
								echo $this->Form->input('User.country', array('type'=>'select', 'div'=>false, 'label'=>false, 'options'=>$this->Session->read('client.Countries'),'default'=>$this->Session->read('client.Client.country'),'class'=>'select_input'));
								$options = array('url' => 'update_state','update' => 'UserState');
								echo $this->Ajax->observeField('UserCountry', $options);
							?>
						</span>
						<br/><br/>
						<?php if  ($this->Session->read('client.Client.country') != 'Hong Kong') :  ?>
						<span>
							<span style="float:left;margin-right: 40px;">
								<span style="margin-right:90px;"><b>State:</b></span>
								<?php echo $this->Form->input('User.state',array('class' => 'first_time_select','options'=> $states,'default'=>$dstate,'empty'=>'', 'div'=>false, 'label'=>false)); ?>&nbsp;* &nbsp;&nbsp;<br>
								<?php echo $this->Form->error('User.state', array('required' => 'Please select your state'), array('required' => 'Please select your state')); ?>
							</span>
							<span style="float:left;">
								<span style="margin-right:50px;"><b>Postcode:</b></span>
								<?php echo $this->Form->input('User.postcode',array('class' => 'first_time_input', 'div'=>false, 'label'=>false)); ?><br>
								<?php echo $this->Form->error('User.postcode', array('required' => 'Postcode required'), array('required' => 'Postcode required')); ?>
							</span>
						</span>
						<br/><br/>
						<?php endif; ?>
						
					<div style="float: left; width: 130px;">
						<span><b>Terms and Conditions:</b></span>
					</div>
					<?php if($this->Session->read('tmp_client.Client.first_time_login_txt') != ''){?>
						<div style="float: left; border: 1px solid; width: 600px;">		
							<?php echo $this->Form->checkbox('User.terms'); ?> *
							<?php echo $this->Form->error('User.terms', array('required' => 'Please agree to the terms and conditions')); ?>
							<div id=divContent style="HEIGHT: 50px; WIDTH:100%; OVERFLOW: auto">
							<?php echo $this->Session->read('tmp_client.Client.first_time_login_txt');?>
							</div>
						</div>
						<?php }else{ ?>
							<div style="float: left; border: 1px solid; width: 600px;">				
								<?php echo $this->Form->checkbox('User.terms'); ?> *<br/>
								<?php echo $this->Form->error('User.terms', array('required' => 'Please agree to the terms and conditions')); ?><br/>
								<div id=divContent style="HEIGHT: 50px; WIDTH:100%; OVERFLOW: auto">
										<p><b>Terms & Conditions</b><br>
										1. Definitions in this Agreement<br>
										1.1 `We`, `our` and `us` means The Rewards Factory Limited ACN 095 009 742,
										(TRF) the operators of the My Rewards program.<br>
										1.2 `Card` means the My Rewards program membership card provided on behalf
										of the TRF client.<br>
										1.3 `Voucher` means the My Rewards program membership voucher provided by
										TRF on behalf of our client via the membership booklet or internet site.<br>
										1.4 `You` and `your` means the member that is the person with a current
										membership number.<br>
										1.5 `The Services` and `Benefits` means the My Rewards program benefits provided
										by TRF.<br>
										2. Acceptance<br>
										2.1 Use of the services by you constitutes your agreement to the terms and
										conditions, which shall govern use of the card/voucher and the provision of
										the service.<br>
										2.2 The Card remains the property of TRF and shall be returned to us promptly
										upon your request in accordance with this Agreement.<br>
										3. The Services<br>
										3.1 The Card entitles you to use the My Rewards program services and you agree
										that you will use the services only for your own benefit or for the benefit of
										your immediate family.<br>
										3.2 The merchant reserves the right to change, modify or cancel the offer without
										notice. *To access up-to-the minute offer details refer to the web site shown
										on your member card. If you are unable to log on call the My Rewards hotline
										on your membership card and select the customer service option. All offers
										included in this book are valid as at 1 June 2009 but are subject to change or
										removal at anytime.<br>
										4. Third Parties<br>
										4.1 It is a condition of the membership that you consent to providing information
										about yourself to any outside party contracted by us to provide benefits to
										you. All outside parties have signed confidentiality agreements with us in
										relation to the provision of client information.<br>
										4.2 TRF will not be liable for any mistake, failure or negligent action on the part of
										any external party contracted with us to provide benefits.<br>
										4.3 Members agree to enter into any arrangement with a supplier at their own risk.<br>
										4.4 Third party suppliers reserve the right to change, modify or cancel any offers
										without notice.<br>
										5. Termination<br>
										5.1 You may terminate your membership at any time by giving written notice to
										us and returning the card.<br>
										5.2 We may terminate your membership at any time by giving written notice
										to you.<br>
										5.3 Termination will not affect obligations incurred before the termination.<br>
										6. Notice<br>
										6.1 Any notice to be given in writing to us should be addressed to:<br>
										My Rewards, PO Box 622, Carnegie, VIC 3163.<br>
										6.2 Any notice to be given to you will be sent by prepaid post addressed to you at
										the last address notified by you to us.<br>
										My Rewards Offer Guarantee<br>
										In the unlikely event that a partner does not honour the offer as shown in this
										book and the member has met all the current conditions* My Rewards will
										refund the amount equivalent to the saving the member would have received up
										to a maximum of $50. To receive your refund, simply send a copy of the itemised
										invoice and your membership details to the address below. A refund cheque will
										be forwarded to you or a credit transaction will be applied to the credit card you
										paid the account with.<br>
										*See Terms and Conditions.<br>
										My Rewards, PO Box 622, Carnegie, VIC 3163<br>
										</p>
									</div>
								</div>
						<?php } ?>
						<br/>
						<span>
							<?php echo $this->Form->submit('Submit', array('style'=>'margin-left:130px;margin-top:5px;','class'=>'button_search_module rounded_corners_button', 'p'=>false, 'type'=>'button', 'onclick'=>"submitFirstTimeLoginForm('/users/first_time_login/2', 'UsersFirstTimeLoginForm2')"));?>
							<?php if ($this->Session->read('tmp_client.Client.fb_share_disabled') == 1) : 
							$fbCName = $this->Session->read('tmp_client.Client.name');
							$fbText = strip_tags($this->Session->read('tmp_client.Client.fb_description_txt'));?>
							<a title="send to Facebook" href="javascript:void(0)" 
							onclick="window.open('http://www.facebook.com/sharer/sharer.php?s=100&p[title]=<?php echo $fbCName;?>&p[summary]=<?php echo $fbText;?>',
							'facebook-share-dialog', 'width=626,height=436');return false;"><img src='/images/fbshare.jpg'></a>
							<?php endif;?>
						</span>										
					</form>
					<br/>
				<?php }else { ?>
				<form onSubmit="return checkconditions(this);" id="UsersFirstTimeLoginForm2" action="<?php echo $this->Html->url('/users/first_time_login/2'); ?>" method="post" class="standard_first_time_login">
					<table style="width:780px">
						<?php if ($this->Session->read('client.Client.id') == '1012'|| $this->Session->read('client.Client.id') == 735) : ?>
						<tr>
							<td><b>Chartis Policy Number:</b></td>
							<td><?php echo $this->Form->input('User.policy', array('div'=>false, 'label'=>false)); ?>&nbsp;*</td>
							<td><?php echo $this->Form->error('User.policy', array('required' => 'Policy Number required'), array('required' => 'Policy Number required')); ?></td>
						</tr>
						<?php endif; ?>
						<tr>
							<td width="120px"><b>Password:</b></td>
							<td width="230px"><?php echo $this->Form->input('User.password',array('class' => 'first_time_input', 'div'=>false, 'label'=>false)); ?>&nbsp;*<br>
							<?php echo $this->Form->error('User.password', array('length' => 'Password must be 6 characters or more')); ?></td>
						
							<td width="120px"><b>Confirm password:</b></td>
							<td width="230px"><?php echo $this->Form->input('User.password2', array('type' => 'password','class' => 'first_time_input', 'div'=>false, 'label'=>false)); ?>&nbsp;*<br>
							<?php echo $this->Form->error('User.password', array('required' => 'Password does not match')); ?></td>
						</tr>
						<tr>
							<td><b>First Name:</b></td>
							<td><?php echo $this->Form->input('User.first_name',array('class' => 'first_time_input', 'div'=>false, 'label'=>false)); ?>&nbsp;*<br>
							<?php echo $this->Form->error('User.first_name', array('required' => 'First name required'), array('required' => 'First name required')); ?></td>
						
							<td><b>Last Name:</b></td>
							<td><?php echo $this->Form->input('User.last_name',array('class' => 'first_time_input', 'div'=>false, 'label'=>false)); ?>&nbsp;*<br>
							<?php echo $this->Form->error('User.last_name', array('required' => 'Last name required'), array('required' => 'Last name required')); ?></td>
						</tr>
						<tr>
							<td><b>Email:</b></td>
							<td><?php echo $this->Form->input('User.email',array('class' => 'first_time_input', 'div'=>false, 'label'=>false)); ?>&nbsp;*<br>
								<?php echo $this->Form->error('User.email', array('valid_email' => 'Invalid email address', 'required' => 'Email address required to receive newsletter'), array('valid_email' => 'Invalid email address')); ?>
							</td>
						<?php if ($this->Session->read('client.Client.id') != 1469 && $this->Session->read('client.Client.id') != 1473 ) : ?>
							<td><b>Mobile Number:</b></td>
							<td>
								<?php echo $this->Form->input('User.mobile',array('class' => 'first_time_input', 'div'=>false, 'label'=>false)); ?><br>
								<?php echo $this->Form->error('User.mobile', array('valid_mobile' => 'Invalid mobile number', 'required' => 'mobile number is required to avail sms feature'), array('valid_mobile' => 'Invalid mobile number ')); ?>
							</td>
							<?php endif; ?>
						</tr>
						<?php if ($this->Session->read('tmp_client.Client.payroll_deduct_disabled') == 1 || $this->Session->read('tmp_client.Client.referral_no_disabled') == 1) : ?>
						<tr>
							<?php if ($this->Session->read('tmp_client.Client.payroll_deduct_disabled') == 1) : ?>
							<td><b>Payroll Number:</b></td>
							<td><?php echo $this->Form->input('User.payroll_deduct',array('class' => 'first_time_input', 'div'=>false, 'label'=>false)); ?>&nbsp;*<br>
								<?php echo $this->Form->error('User.payroll_deduct', array('required' => 'Payroll Number Required'),array('required' => 'Payroll Number Required')); ?>
							</td>
							<?php endif;
							if ($this->Session->read('tmp_client.Client.referral_no_disabled') == 1) : ?>
							<td><b>Referral Number:</b></td>
							<td><?php echo $this->Form->input('User.referral_no',array('class' => 'first_time_input', 'div'=>false, 'label'=>false)); ?>&nbsp;*<br>
								<?php echo $this->Form->error('User.referral_no', array('required' => 'Referral Number Required'),array('required' => 'Referral Number Required')); ?>
							</td>
							<?php endif;?>
						</tr>
						<?php endif; ?>
						<?php if ($this->Session->read('client.Client.id') == 837) : ?>
						<tr>
							<td><b>Hospital Name:</b></td>
							<td><?php echo $this->Form->input('User.hospital_name',array('class' => 'first_time_input', 'div'=>false, 'label'=>false)); ?>&nbsp;*</td>
							<td><?php echo $this->Form->error('User.hospital_name', array('required' => 'Hospital Name required'), array('required' => 'Hospital Name required')); ?></td>
						</tr>
						<?php endif; ?>
						<tr>
							<td><b>Receive Newsletter:</b></td>
							<td><?php echo $this->Form->radio('User.newsletter', array(1 => 'Yes', 0 => 'No'), array('style' => 'vertical-align:middle;height:14px')); ?></td>
						<?php if ($this->Session->read('client.Client.id') != 1469 && $this->Session->read('client.Client.id') != 1473 ) : ?>
							<td><b>D.O.B:</b></td>
							<td>
								<?php echo $this->Form->input('User.dob', array('div'=>false, 'label'=>false, 'type'=>'date', 'dateFormat'=>'DMY', 'style' => 'width:auto',  'minYear' => date('Y')-100, 'maxYear' => date("Y")-10)); ?>
							</td>		
							<?php endif; ?>

						</tr>
						<?php if ($this->Session->read('client.Client.id') != 1469 && $this->Session->read('client.Client.id') != 1473 ) : ?>
						<tr>
							<td><b>Interests:</b></td>
							<td colspan="3">
								<table border=0 style="width:100%;" id="TableInterests">
									<tr><td><?php echo $this->Form->checkbox('User.int_all', array('onClick' => 'ToggleStates(this)')); ?> All</td>
									<td><?php echo $this->Form->checkbox('User.int_travel'); ?> Travel & Accommodation</td>
									<td><?php echo $this->Form->checkbox('User.int_shopping'); ?> Shopping</td></tr>
									<tr><td><?php echo $this->Form->checkbox('User.int_dining'); ?> Dining & FastFood</td>
									<td><?php echo $this->Form->checkbox('User.int_leisure'); ?> Leisure & Entertainment</td>
									<td><?php echo $this->Form->checkbox('User.int_beauty'); ?> Health & Beauty</td>
									<td><?php echo $this->Form->checkbox('User.int_auto'); ?> Auto</td></tr>
								</table>
							</td>
						</tr> 
						<?php endif; ?>
						<tr>
							<td><b>Country</b></td>
							<td colspan="3">
								<?php 
									echo $this->Form->input('User.country', array('type'=>'select', 'div'=>false, 'label'=>false, 'options'=>$this->Session->read('client.Countries'),'default'=>$this->Session->read('client.Client.country'),'class'=>'select_input'));
									$options = array('url' => 'update_state','update' => 'UserState');
									echo $this->Ajax->observeField('UserCountry', $options);
								?>
							</td>
						</tr>
						<?php if  ($this->Session->read('client.Client.country') != 'Hong Kong') :  ?>
						<tr>
							<td><b>State:</b></td>
							<td><?php echo $this->Form->input('User.state',array('class' => 'first_time_select','options'=> $states,'default'=>$dstate,'empty'=>'', 'div'=>false, 'label'=>false)); ?>&nbsp;* &nbsp;&nbsp;<br>
							<?php echo $this->Form->error('User.state', array('required' => 'Please select your state'), array('required' => 'Please select your state')); ?>
							</td>
						
							<td><b>Postcode:</b></td>
							<td><?php echo $this->Form->input('User.postcode',array('class' => 'first_time_input', 'div'=>false, 'label'=>false)); ?><br>
							<?php echo $this->Form->error('User.postcode', array('required' => 'Postcode required'), array('required' => 'Postcode required')); ?></td>
						</tr>
						<?php endif; ?>
						<?php if ($this->Session->read('tmp_client.Client.id') == 1667) : ?>
							<tr><td colspan=4><table border=1 style="width:100%;">
							<tr><td><table border=0 style="width:100%;">
							<tr><td><b>Credit Card Details:</b></td></tr>
							<tr>
								<td><b>Card Type:</b></td>
								<td><?php  echo $this->Form->input('card_type', array ('class' => 'select','type' => 'select','label'=>'','options'=>array('Visa'=>'Visa','MasterCard'=>'MasterCard')));	 ?></td>
								<td><b>Cardholder’s Name:*:</b></td>
								<td><?php echo $this->Form->input('card_name',array('class' => 'first_time_input', 'id'=>'card_holder','empty'=>'', 'div'=>false, 'label'=>false)); ?>	<br><span style="color:red;" id="cardholder_val"></span>    	</td>
							</tr>
							<tr>
								<td><b>Card Number:*:</b></td>
								<td><?php echo $this->Form->input('card_number',array('class' => 'first_time_input', 'id'=>'card_number','empty'=>'', 'div'=>false, 'label'=>false)); 	 ?><br><span style="color:red;" id="cardnum_val"></span>
									
								</td>			
								<td><b>CVV Number:*:</b></td>
								<td><?php  echo $this->Form->input('cvv_number',array('class' => 'first_time_input','id'=>'cvv_number','empty'=>'', 'div'=>false, 'label'=>false));	 ?>	<br><span style="color:red;" id="cvvnum_val"></span></td>
							</tr>
							<tr>
								<td><b>Expired Date:*:</b></td>
								<td><?php echo $this->Form->input('expired_date', array('div'=>false, 'label'=>false, 'type'=>'date', 'dateFormat'=>'MY', 'style' => 'width:auto',  'minYear' => date('Y'), 'maxYear' => date("Y")+10)); ?></td>
							</tr>
							</table></td></tr>
							</table>
							</td></tr>

						<?php endif; ?>
						<tr>
							<td valign="top"><b>Terms and Conditions:</b>
							</td>
							<td colspan=3>				
								<table border=1 style="width:100%;">
									<tr>
										<td>
										<?php if($this->Session->read('tmp_client.Client.first_time_login_txt') != ''){?>
											<?php echo $this->Form->checkbox('User.terms'); ?> *
											<?php echo $this->Form->error('User.terms', array('required' => 'Please agree to the terms and conditions')); ?>
											<div id=divContent style="HEIGHT: 50px; WIDTH:100%; OVERFLOW: auto">
												<?php echo $this->Session->read('tmp_client.Client.first_time_login_txt');?>
											</div>
										<?php }else{ ?>
											<?php echo $this->Form->checkbox('User.terms'); ?> *
											<?php echo $this->Form->error('User.terms', array('required' => 'Please agree to the terms and conditions')); ?>
											<div id=divContent style="HEIGHT: 50px; WIDTH:100%; OVERFLOW: auto">
													<p><b>Terms & Conditions</b><br>
													1. Definitions in this Agreement<br>
													1.1 `We`, `our` and `us` means The Rewards Factory Limited ACN 095 009 742,
													(TRF) the operators of the My Rewards program.<br>
													1.2 `Card` means the My Rewards program membership card provided on behalf
													of the TRF client.<br>
													1.3 `Voucher` means the My Rewards program membership voucher provided by
													TRF on behalf of our client via the membership booklet or internet site.<br>
													1.4 `You` and `your` means the member that is the person with a current
													membership number.<br>
													1.5 `The Services` and `Benefits` means the My Rewards program benefits provided
													by TRF.<br>
													2. Acceptance<br>
													2.1 Use of the services by you constitutes your agreement to the terms and
													conditions, which shall govern use of the card/voucher and the provision of
													the service.<br>
													2.2 The Card remains the property of TRF and shall be returned to us promptly
													upon your request in accordance with this Agreement.<br>
													3. The Services<br>
													3.1 The Card entitles you to use the My Rewards program services and you agree
													that you will use the services only for your own benefit or for the benefit of
													your immediate family.<br>
													3.2 The merchant reserves the right to change, modify or cancel the offer without
													notice. *To access up-to-the minute offer details refer to the web site shown
													on your member card. If you are unable to log on call the My Rewards hotline
													on your membership card and select the customer service option. All offers
													included in this book are valid as at 1 June 2009 but are subject to change or
													removal at anytime.<br>
													4. Third Parties<br>
													4.1 It is a condition of the membership that you consent to providing information
													about yourself to any outside party contracted by us to provide benefits to
													you. All outside parties have signed confidentiality agreements with us in
													relation to the provision of client information.<br>
													4.2 TRF will not be liable for any mistake, failure or negligent action on the part of
													any external party contracted with us to provide benefits.<br>
													4.3 Members agree to enter into any arrangement with a supplier at their own risk.<br>
													4.4 Third party suppliers reserve the right to change, modify or cancel any offers
													without notice.<br>
													5. Termination<br>
													5.1 You may terminate your membership at any time by giving written notice to
													us and returning the card.<br>
													5.2 We may terminate your membership at any time by giving written notice
													to you.<br>
													5.3 Termination will not affect obligations incurred before the termination.<br>
													6. Notice<br>
													6.1 Any notice to be given in writing to us should be addressed to:<br>
													My Rewards, PO Box 622, Carnegie, VIC 3163.<br>
													6.2 Any notice to be given to you will be sent by prepaid post addressed to you at
													the last address notified by you to us.<br>
													My Rewards Offer Guarantee<br>
													In the unlikely event that a partner does not honour the offer as shown in this
													book and the member has met all the current conditions* My Rewards will
													refund the amount equivalent to the saving the member would have received up
													to a maximum of $50. To receive your refund, simply send a copy of the itemised
													invoice and your membership details to the address below. A refund cheque will
													be forwarded to you or a credit transaction will be applied to the credit card you
													paid the account with.<br>
													*See Terms and Conditions.<br>
													My Rewards, PO Box 622, Carnegie, VIC 3163<br>
													</p>
												</div>
										<?php } ?>
									</td>
								</tr>
							</table>
							</td>
						</tr>
						<tr>
							<td></td>
							<td>
								<!--<button type="submit" class="button_search_module rounded_corners_button">Submit</button>-->
								<?php echo $this->Form->submit('Submit', array('class'=>'button_search_module rounded_corners_button', 'div'=>false, 'type'=>'button', 'onclick'=>"submitFirstTimeLoginForm('/users/first_time_login/2', 'UsersFirstTimeLoginForm2')"));?>
							</td>
						</tr>
					</table>
				</form>
				<br/>
			<?php }} else { ?>
				<form action="<?php echo $this->Html->url('/users/first_time_login/1'); ?>" id="UsersFirstTimeLoginForm1" method="post" class="standard_first_time_login">
					<p>As this is your first time here we need you to complete the first time registration</p>
					<p><b>Step 1</b>: Please enter in your membership number in the box below:</p>
					<?php if (isset($error)) :?>
						<p class="form_error_message" style="color: #FF4F4F"><span id="UserUsernameErrorMessage"><?php echo $error; ?></span></p>
					<?php endif; ?>
					<table>
						<tr>
							<td>Membership number:</td>
							<td>
								<?php echo $this->Form->input('User.username',array('class'=>'first_time_input', 'div'=>false, 'label'=>false)); ?>
								<?php if (isset($error)) :?>
									<span class="form_error_message" id="UserUsernameError"><img src="<?php echo $this->Html->url('/img/cross.gif'); ?>" alt="Error" /></span>
								<?php endif; ?>
							</td>
						</tr>
						<tr>
							<td></td>
							<td>
							
								<button type="button" id="fr_login" class="button_search_module rounded_corners_button">Submit</button>
								
								<?php echo $this->Form->submit('Submit', array('id'=>'fr_login_sub', 'class'=>'button_search_module rounded_corners_button', 'div'=>false, 'type'=>'button', 'onclick'=>"submitFirstTimeLoginForm('/users/first_time_login/1', 'UsersFirstTimeLoginForm1')"));?>	
							</td>
						</tr>
					</table>
					<script type="text/javascript">$(function() {$('#UserUsername').focus();});</script>
					<br/>
				</form>
				&nbsp;
			<?php }?>
			
		</div>
	</div>
</div>
<script>
function submitFirstTimeLoginForm(actionUrl, formId) {
	$.ajax({
		url: actionUrl,
		data: $('#'+formId).serialize(),
		type: 'POST',
		success: function(data){
			$("#modalboxpopup").html(data).dialog({width: 850, modal: true, "position": "top"});
		}
	});
	return false;
}
ToggleStates = function (National)
{
    var rows = document.getElementById('TableInterests').getElementsByTagName('input');
    for ( var i = 0; i < rows.length; i++ ) {
        if ( rows[i] && rows[i].type == 'checkbox' && rows[i] != National ) 
		{
			if(National.checked == true)
			{
		            rows[i].checked = true;
			}
			else
			{
			   rows[i].checked = false;
			}
        }
    }

    return true;
}

checkconditions = function (cond)
{
	if (document.getElementById("terms").checked==false){alert("You have to agree to the terms and conditions"); document.getElementById("terms").focus();return false;}
 
}
</script>






 <!--validation script-->
<script type="text/javascript">

	$(document).ready(function(){
		$('#fr_login').click(function(){
			$('#UserPasswordError').html('');
			$('#UserPasswordError2').html('');
			$('#UserFirstNameError').html('');
			$('#UserLastNameError').html('');
			$('#UserEmailError').html('');
			$('#cardnum_val').html('');
			$('#cvvnum_val').html('');
			$('#cardholder_val').html('');
			$('#UserStateError').text('');
			
			var error = 0;
			
			if($('#UserPassword').val()==""){				
				$('#UserPasswordError').html('please enter a pasword').css('display', 'block');
				error = 1;
				return false;
			}
			if($('#UserPassword2').val()!=$('#UserPassword').val()){				
				$('#UserPasswordError2').html("password doesn't match").css('display', 'block');
				error = 1;
				return false;
			}
			if($('#UserFirstName').val()==""){				
				$('#UserFirstNameError').html("enter first name").css('display', 'block');
				error = 1;
				return false;
			}
			
			if($('#UserLastName').val()==""){
				$('#UserLastNameError').html("enter first name").css('display', 'block');
				error = 1;
				return false;
			}
			 var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
			if( !emailReg.test($('#UserEmail').val()) ) {
				$('#UserEmailError').html("enter first name").css('display', 'block');
				error = 1;
				return false;
			  } 	
			
			
			if(($('#UserState').val() == "")&&($('#UserCountry').val() != "Hong Kong")){	
						           
				$('#UserStateError').text('Please select state').css('display', 'block');
				error = 1;
				return false;
            }
			
			
			if($('#card_holder').val()==""){				
				$('#cardholder_val').html('please enter card holder name');
				error = 1;
				return false;
			}
						
			
			if($('#card_number').val()==""){
			$('#cardnum_val').html('enter card number');
			error = 1;
			return false;
			}
			var numericReg = /^\d*[0-9](|.\d*[0-9]|,\d*[0-9])?$/;
			if(!numericReg.test($('#card_number').val())) {
				$('#cardnum_val').text('Numeric characters only');
				error = 1;
				return false;
			}
			
			if($('#cvv_number').val()==""){
			$('#cvvnum_val').html('enter cvv number');
			error = 1;
			return false;
			}			
			if(!numericReg.test($('#cvv_number').val())) {
				$('#cvvnum_val').text('Numeric characters only');
				error = 1;
				return false;
			}
			
			if($('#UserTerms').prop("checked") == false){
				           
				$('#UserTermsError').text('Please Agree the terms and conditions').css('display', 'block');
				error = 1;
				return false;
            }
			
			
			
			
			if(error!=1){				
				$('#fr_login_sub').click();
			}
						 
		});
	});
</script>
<style>
#fr_login_sub{
	display:none;
}
span{
	color:red;
}
</style>

