<style>
	.tr
	{
		height:30px;
		font-size:13px;
	}
</style>
<?php echo $this->element('message'); ?>
<div class="main search_content search_results">
	<div style="margin:15px 0px 20px 40px">
		<div class="tab_padding" style="font-size:15px;">
			<?php echo $this->element('message'); ?>
			<div style="font-size:16px;font-weight:bold">Reset Password</div>
		</div>
		<?php if($step == 2) {?>
			<div style="min-height:100px;">
				<form action="<?php echo $this->Html->url('/users/forgot_password/2'); ?>" method="post" class="standard_first_time_login" id="UsersForgotPassword">
					<table>
						<tr class="tr">
							<td style="padding-left:15px" colspan="2">You have provided the email <strong><?php echo $email;?></strong></td>
						</tr>
						<tr class="tr">
							<td style="padding-left:15px">
								<?php echo $this->Form->input('User.email',array('type'=>'hidden', 'value'=>$email));?>
								<?php echo $this->Form->input('User.username',array('type'=>'hidden', 'value'=>$username));?>
								Please select the client you forgot the password : 	
							</td>
							<td>
								<?php echo $this->Form->input('User.clients', array('type' => 'select', 'options' => $clients, 'label'=>false,'class'=>'select_input', 'style'=>'width:200px;'));?>
							</td>
						</tr>
						<tr class="tr">
							<td style="padding-left:15px" colspan="2">
								<?php echo $this->Form->submit('Reset Password', array('class'=>'button_search_module rounded_corners_button', 'div'=>false, 'type'=>'button', 'style'=>'width:150px;', 'onClick'=>'submitForm()'));?>
							</td>
						</tr>
					</table>
				</form>
			</div>
		<?php }else{?>
		<div style="padding-left:5px">
				<?php if (isset($message) && $message == 'user_not_found') : ?>
					<p>You can try again or contact us at <a href="mailto:<?php echo $this->Session->read('client.Client.email'); ?>"><?php echo $this->Session->read('client.Client.email'); ?></a></p>
				<?php endif; ?>
				<form action="<?php echo $this->Html->url('/users/forgot_password/1');?>" method="post" class="standard" >
					<table>
						<tr>
							<td>Username:</td>
							<td>
								<?php echo $this->Form->input('User.username',array('class'=>'search_input', 'div'=>false, 'label'=>false)); ?>
								<?php echo $this->Form->error('User.username', array('required')); ?>
							</td>
						</tr>
						<tr>
							<td>Email address:</td>
							<td>
								<?php echo $this->Form->input('User.email',array('class'=>'search_input', 'div'=>false, 'label'=>false)); ?>
								<?php echo $this->Form->error('User.email', array('valid_email' => 'Invalid email address'), array('valid_email' => 'Invalid email address	')); ?>
							</td>
						</tr>
						<tr>
							<td></td>
							<td>
							<?php echo $this->Form->submit('Submit', array('class'=>'button_search_module rounded_corners_button', 'div'=>false));?>
							</td>
						</tr>
						<tr>
							<td></td>
							<td><br/>
								<?php echo $this->Html->link('Return to Login',array('controller'=>'users','action' => 'login'));?>
							</td>
						</tr>
					</table>	
				</form>
		</div>
		<script type="text/javascript">
		$('#UserEmail').focus();
		</script>
		<?php }?>
	</div>
</div>
<script>
function submitForm() {
	$.ajax({
		url: '/users/forgot_password/2',
		data: $('#UsersForgotPassword').serialize(),
		type: 'POST',
		success: function(data){
			$("#modalboxpopup").html(data).dialog({modal: true, "position": "top"});
		}
	});
	return false;
}
</script>