<div style="padding-left:30px" class="rounded-corners_bottom">
		<?php echo $this->element('message'); ?>
		<?php if ($page['ClientPage']['h1'] != '') : ?>
			<h1 style="margin-top:0px;"><?php echo $page['ClientPage']['h1']; ?></h1>
		<?php endif; ?>
		<?php if ($page['ClientPage']['h2'] != '') : ?>
			<h2 style="margin-top:0px;"><?php echo $page['ClientPage']['h2']; ?></h2>
		<?php endif; ?>
		<?php if ($page['ClientPage']['h3'] != '') : ?>
			<h3 style="margin-top:0px;"><?php echo $page['ClientPage']['h3']; ?></h3>
		<?php endif; ?>
		<?php if ($page['ClientPage']['content'] != '') : ?>
			<?php echo $page['ClientPage']['content']; ?>
		<?php endif; ?>
		<div class="module">
			<div class="tab_padding" style="font-size:15px;padding-bottom:0px !important;">
				My Membership
			</div>
				
			<div>
				
					<?php echo $this->Form->create('User', array('class'=>'standard'));?>
					<table>
						<tr>
							<td>Account ID:</td>
							<td><?php echo $this->Session->read('user.User.username'); ?></td>
							<td colspan = 2></td>
						</tr>
						<tr>
							<td><span style="color:red;">*</span>&nbsp;First Name:</td>
							<td><?php echo $this->Form->input('User.first_name',array('class'=>'search_input', 'div'=>false, 'label'=>false)); ?><br />
							<?php echo $this->Form->error('User.first_name', array('required' => 'First name required'), array('required' => 'First name required')); ?></td>
							<td><span style="color:red;">*</span>Last Name:</td>
							<td><?php echo $this->Form->input('User.last_name',array('class'=>'search_input', 'div'=>false, 'label'=>false)); ?><br />
							<?php echo $this->Form->error('User.last_name', array('required' => 'Last name required'), array('required' => 'Last name required')); ?></td>
						</tr>
						
						<tr>
							<td><span style="color:red;">*</span>Password:</td>
							<td><?php echo $this->Form->input('User.password',array('class'=>'search_input', 'div'=>false, 'label'=>false)); ?>
							<?php echo $this->Form->error('User.password', array('length' => 'Password must be 6 characters or more')); ?>
							</td>
							<td><span style="color:red;">*</span>Confirm password:</td>
							<td><?php echo $this->Form->input('User.password2', array('type' => 'password', 'class'=>'search_input', 'div'=>false, 'label'=>false)); ?><br />
							<?php echo $this->Form->error('User.password', array('required' => 'Password does not match')); ?></td>
						</tr>
						
						<tr>
							<td><span style="color:red;">*</span>Country</td>
							<td>
								<?php 
									echo $this->Form->input('User.country', array('type'=>'select', 'div'=>false, 'label'=>false, 'options'=>$this->Session->read('client.Countries'),'default'=>$country,'class'=>'select_input'));
									$options = array('url' => 'update_state','update' => 'UserState');
									echo $this->Ajax->observeField('UserCountry', $options);
								?>
							<?php echo $this->Form->error('User.country', array('valid_country' => 'Invalid country', 'required' => 'country required'), array('valid_country' => 'Invalid country')); ?>
							</td>
							
							<?php if  ($this->Session->read('client.Client.country') != 'Hong Kong') :  ?>
							<td><span style="color:red;">*</span>State</td>
							<td>
								<?php echo $this->Form->input('User.state', array('type'=>'select', 'div'=>false, 'label'=>false, 'options'=>$states,'default'=>$state,'class'=>'select_input')); ?>
								<br />
							<?php echo $this->Form->error('User.state', array('required' => 'Select a state')); ?></td>							
							<?php endif; ?>	
						</tr>
						<tr>
							<td>Mobile</td>
							<td><?php echo $this->Form->input('User.mobile',array('class'=>'search_input', 'div'=>false, 'label'=>false)); ?></td>
							<?php if ($this->Session->read('client.Client.id') == 1012 || $this->Session->read('client.Client.id') == 735) : ?>
							<td>Mobile</td>
							<td><?php echo $this->Form->input('User.policy',array('class'=>'search_input', 'div'=>false, 'label'=>false)); ?></td>
							<?php endif; ?>
							<?php if ($this->Session->read('client.Client.id') == 837) : ?>
							<td>Hospital Name</td>
							<td><?php echo $this->Form->input('User.hospital_name',array('class'=>'search_input', 'div'=>false, 'label'=>false)); ?></td>
							<?php endif; ?>
						</tr>
						<tr>
							<td></td>
							<td colspan="3">If you would like to receive mobile coupons, please register your mobile number.<br /></td>
						</tr>
						<tr>
							<td><span style="color:red;">*</span>Email Address</td>
							<td><?php echo $this->Form->input('User.email',array('class'=>'search_input', 'div'=>false, 'label'=>false)); ?><br />
								<?php echo $this->Form->error('User.email', array('valid_email' => 'Invalid email address', 'required' => 'Email address required to receive newsletter'), array('valid_email' => 'Invalid email address	')); ?>
							</td>
							<td>Receive Newsletter:</td>
							<td><?php echo $this->Form->input('User.newsletter', array('type'=>'radio', 'options'=>array(1 => 'Yes', 0 => 'No'), 'div'=>false, 'label'=>false, 'legend'=>false, 'style' => 'vertical-align:middle;height:14px')); ?></td>
						</tr>
						<tr>
							<td colspan="4">Please enter your email address to receive e-communications. You will shortly receive a confirmation email stating that your registration has been successful. Please contact us if you have not received a confirmation email within 24 hours.</td>
						</tr>
						
						<tr>
							<td colspan=4 align="center">
							<?php echo $this->Form->submit('Submit', array('class'=>'button_search_module rounded_corners_button', 'div'=>false, 'type'=>'button', 'onclick'=>"submitUserEditForm()"));?>
							</td>
						</tr>
					</table>
				<?php echo $this->Form->end();?>
			</div>
		</div>
		
	</div>
<script>
function submitUserEditForm() {
	$.ajax({
		url: '/users/edit',
		data: $('#UserEditForm').serialize(),
		type: 'POST',
		success: function(data){
			$("#modalboxpopup").html(data).dialog({modal: true, "position": "top"});
		}
	});
	return false;
}
</script>
