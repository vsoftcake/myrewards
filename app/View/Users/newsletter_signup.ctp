<div style="border-bottom:0px !important">
	<div>
		<?php echo $this->element('message'); ?>
		<?php if ($page['ClientPage']['h1'] != '') : ?>
			<h1 style="margin-top:0px"><?php echo $page['ClientPage']['h1']; ?></h1>
		<?php endif; ?>
		<?php if ($page['ClientPage']['h2'] != '') : ?>
			<h2 style="margin-top:0px"><?php echo $page['ClientPage']['h2']; ?></h2>
		<?php endif; ?>
		<?php if ($page['ClientPage']['h3'] != '') : ?>
			<h3 style="margin-top:0px"><?php echo $page['ClientPage']['h3']; ?></h3>
		<?php endif; ?>
		<?php if ($page['ClientPage']['content'] != '') : ?>
			<?php echo $page['ClientPage']['content']; ?>
		<?php endif; ?>
		<?php if ($step == '1') : ?>
			<div style="margin:10px;">
				<p>If you would like to sign up to receive the e-newsletter please enter your email address below.</p>
				<form id="UsersNewsletterForm" method="post" action="/users/newsletter_signup/2">
					<?php echo $this->Form->input('User.email',array('class'=>'search_input rounded_corners_button', 'div'=>false, 'label'=>false)); ?> 
					<?php echo $this->Form->submit('Subscribe', array('class'=>'button_search_module rounded_corners_button', 'div'=>false, 'onclick'=>"submitNewSignupForm()"));?>
					<?php echo $this->Form->error('User.email', array('valid_email' => 'Invalid email address', 'required' => 'Email address required to receive newsletter'), array('valid_email' => 'Invalid email address	')); ?>
				</form>
			</div>
		<?php endif; ?>
	</div>
</div>
<script>
function submitNewSignupForm() {
	$.ajax({
		url: '/users/newsletter_signup/2',
		data: $('#UsersNewsletterForm').serialize(),
		type: 'POST',
		success: function(data){
			$("#modalboxpopup").html(data).dialog({modal: true, "position": "top"});
		}
	});
	return false;
}
</script>