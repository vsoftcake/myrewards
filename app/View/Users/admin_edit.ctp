<style type="text/css">
	#content
	{
		border-left:1px solid #cccccc;
		border-right:1px solid #cccccc;
		border-bottom:1px solid #cccccc;
		margin-left:20px !important;
		padding-left:18px !important;
		width:913px;
	}
	.rounded-corners 
	{
		-moz-border-radius: 10px 10px 0px 0px; /* Firefox */
		-webkit-border-radius: 10px 10px 0px 0px; /* Safari, Chrome */
		border-radius: 10px 10px 0px 0px; /* CSS3 */
	}
	.tr 
	{
		height:28px;
	}
	.admin_button
	{
		height: 26px;
		padding-bottom: 2px;
		border-radius: 5px 5px 5px 5px;
		background-color: #0066cc;
		border: 1px solid #cccccc;
		color: #FFFFFF; 
		cursor: pointer;
		font-size:13px;
		
	}
</style>
<?php echo $this->Html->script('jscolor'); ?>

<script type="text/javascript">
	document.write('<style type="text/css">.tabber{display:none;}<\/style>');
	function confirmation(name,id)
    {   
		var answer = confirm("Are you sure you want to delete user '<?php echo $this->Form->value('User.username'); ?>'")
		if (answer)
		{
			location.href="/admin/users/delete/<?php echo $this->Form->value('User.id') ?>";
		}
    }
    
    function reset_password()
    {
		var answer = confirm("Are you sure you want to reset the password for user '<?php echo $this->Form->value('User.username'); ?>'")
		if (answer)
		{
			location.href="/admin/users/reset_password/<?php echo $this->Form->value('User.id') ?>";
		}
    }
</script>
<form action="<?php echo $this->Html->url('/admin/users/edit/'. $this->Html->value('User.id')); ?>" method="post">
	<div style="width:953px;margin-left:-14px;">
		<div style="width:949px;height:50px;">
			<div style=" height: 40px; float:left; width: 608px;padding-left:37px;">
		       	<div style=" height: 40px; float:left;padding-top:5px;">
		        	<img title="Users" alt="Userss" src="/files/admin_home_icons/users.png" style="width:40px;height:40px;">
		        </div>
		        <div style=" height: 40px;float:left;padding-left:10px;">
		        	<h2>Edit User <?php if($this->Form->value('User.username')!=''): ?>- <?php echo $this->Form->value('User.username') ?><?php endif;?></h2>
		        </div> 
			</div>
	        <div class="actions" style=" height: 40px; float:left;  width: 300px;">
				<ul style="padding:20px;margin-left:-28px;">
					<li style="display:inline;"><?php echo $this->Form->button('Cancel', array('onclick' => "location.href='".$this->Session->read('redirect')."'",
					'class'=>'admin_button','style'=>'width:60px;', 'type'=>'button'));  ?></li>
					<?php if($this->Form->value('User.id')!='') :
					$conname=$this->Form->value('User.username');$conid=$this->Form->value('User.id'); ?>
					<li style="display:inline;"><?php echo $this->Form->button('Delete User', array('onclick' => "confirmation('$conname','$conid')",'style'=>'width:110px;','class'=>'admin_button','type'=>'button'));  ?></li>
					<?php endif;?>
			        <li style="display:inline;"><?php echo $this->Form->button('Submit', array('class'=>'admin_button','style'=>'width:60px;','type'=>'submit'));  ?></li>
		        </ul>
			</div>
		</div>

		<?php echo $this->Form->input('User.id', array('type' => 'hidden')); ?>
	 	<div class="tabber" id="mytab1">
			<div class="tabbertab"  style="min-height:285px">
		  		<h2>General Information</h2>
	    		<h4>User General Information</h4>
				<div style="background-color:#EFEFEF;border:1px solid #cccccc;padding:10px;width:790px;min-height:115px;overflow:hidden">	    
					<table style="padding:30px;width:100%;">
						<tr>
					    	<td style="padding-right:30px">
					        	<table width="100%">
									<tr class="tr">
										<td style="width:80px"><strong>Username</strong></td>
										<td><?php echo $this->Form->input('User.username', array('div'=>false, 'label'=>false)); ?></td>
										<td><?php echo $this->Form->error('User.username', array('required' => 'Username required', 'unique' => 'Username already in use'), array('required' => 'Username required')); ?></td>
									</tr>
									<tr class="tr">
										<td><strong>First name</strong></td>
										<td><?php echo $this->Form->input('User.first_name', array('div'=>false, 'label'=>false)); ?></td>
										<td><?php echo $this->Form->error('User.first_name', array('required' => 'First name required'), array('required' => 'First name required')); ?></td>
									</tr>
									<tr class="tr">
										<td><strong>Email</strong></td>
										<td><?php echo $this->Form->input('User.email', array('div'=>false, 'label'=>false)); ?></td>
										<td><?php echo $this->Form->error('User.email', array('valid_email' => 'Invalid email address', 'required' => 'Email address required'), array('valid_email' => 'Invalid email address	')); ?></td>
									</tr>
									<tr class="tr">
										<td><strong>Policy</strong></td>
										<td><?php echo $this->Form->input('User.policy', array('div'=>false, 'label'=>false)); ?></td>
									</tr>
								</table>
							</td>
							<td style="padding-left:30px;vertical-align:top;">
								<table width="100%">
									<?php if($this->Html->value('User.id') != '') : ?>
									<tr class="tr">
										<td style="width:80px"><strong>Password</strong></td>
										<td>
											<?php if($this->Html->value('User.id') != '') : ?>
												&nbsp;&nbsp;
												<button type="button" class="admin_button" onClick="reset_password()">Reset</button>
											<?php endif;?>
										</td>
										<td></td>
									</tr>
									<?php endif;?>
									<tr class="tr">
										<td><strong>Surname</strong></td>
										<td><?php echo $this->Form->input('User.last_name', array('div'=>false, 'label'=>false)); ?></td>
										<td><?php echo $this->Form->error('User.last_name', array('required' => 'Surname required'), array('required' => 'Surname required')); ?></td>
									</tr>
									<tr class="tr">
										<td><strong>Mobile</strong></td>
										<td><?php echo $this->Form->input('User.mobile', array('div'=>false, 'label'=>false)); ?></td>
									</tr>
									<tr class="tr">
										<td><strong>Hospital Name</strong></td>
										<td><?php echo $this->Form->input('User.hospital_name', array('div'=>false, 'label'=>false)); ?></td>
									</tr>
								</table>
						    </td>
						</tr>
					</table>
					<table width="100%">
						<tr>
							<td width="53%">
								<table>	
									<tr class="tr">
										<td style="width:80px"><strong>Country</strong></td>
										<td>
											<?php 
											echo $this->Form->input('User.country', array('div'=>false, 'label'=>false, 'type'=>'select', 'empty'=>'', 'options'=>$this->Session->read('client.Countries'))); 
											$options = array('url' => 'update_state','update' => 'UserState');
											echo $this->Ajax->observeField('UserCountry', $options);?>
										</td>
										<td><?php echo $this->Form->error('User.country', array('required' => 'Select a country')); ?></td>
									</tr>
								</table>
					      	</td>
					      	<td style="padding-left:7px;">  
								<table width="100%">	
									<?php if  ($this->Session->read('client.Client.country') != 'Hong Kong') :  ?>
									<tr class="tr">
										<td style="width:80px"><strong>State</strong></td>
										<td><?php echo $this->Form->input('User.state', array('div'=>false, 'label'=>false, 'type'=>'select', 'options'=>$states, 'empty'=>'Please select...')); ?></td>
										<td><?php echo $this->Form->error('User.state', array('required' => 'Select a state')); ?></td>
									</tr>
								   	<?php endif;?>
							  	</table>
				         	</td>
						</tr>
					</table>
				    	<table>
						<tr class="tr">
							<td style="width:80px"><strong>Client</strong></td>
							<td>
								<?php 
									if($this->Session->read('user.User.type') == 'Client Admin' || $this->Session->read('user.User.type') == 'Country Admin') : 
										if($this->Html->value('User.id')!='') : 
											echo $this->Form->input('User.client_id', array('div'=>false, 'label'=>false, 'type'=>'select', 'options'=>$clients, 'disabled'=>'disabled', 'empty'=>'Please select...')); 
										else : 
											echo $this->Form->input('User.client_id', array('div'=>false, 'label'=>false, 'type'=>'select', 'options'=>$clients)); 
										endif;
									else : 
										echo $this->Form->input('User.client_id', array('div'=>false, 'label'=>false, 'type'=>'select', 'options'=>$clients, 'empty'=>'Please select...'));
									endif;
								?>
							</td>
							<td><?php echo $this->Form->error('User.client_id', array('required' => 'Select a client')); ?></td>
						</tr>
					</table>
					<?php if($this->Html->value('User.id') == '') : ?>
						<p>If registered is checked then <strong>temporary password of client(if set) or the "First 10 characters of client name in lower case without spaces, with string '123'"</strong> will be assigned as password for the new user</p>
					<?php endif; ?>
			  	</div>
			  	<br/>
		      	<div>
					<table>
						<tr class="tr">
							<td style="width:375px;"></td>  
							<td>
								<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(1)">Next</button>
							</td>
						</tr>
					</table>
				</div>
			</div>
			
			<div class="tabbertab"  style="height:185px">
	        	<h2>Settings</h2>
	          	<h4>Settings</h4>
            	<div style="background-color:#EFEFEF;border:1px solid #cccccc;padding:10px;width:790px;min-height:108px;overflow:hidden">	
		       		<table style="width:100%;padding:20px">
		          		<tr>
		            		<td style="padding-right: 30px; width: 420px;">
				        		<table>
									<tr class="tr">
										<td><strong>User Level</strong></td>
										<td>
											<?php  
												if($this->Session->read('user.User.type') == 'Client Admin' ) :
													$types =$options = array('Member' => 'Member', 'Client Admin' => 'Client Admin');
													echo $this->Form->input('User.type', array('div'=>false, 'label'=>false, 'type'=>'select', 'options'=>$types));
												elseif($this->Session->read('user.User.type') == 'Country Admin' ) :
													$types =$options = array('Member' => 'Member', 'Client Admin' => 'Client Admin', 'Country Admin' => 'Country Admin');
													echo $this->Form->input('User.type', array('div'=>false, 'label'=>false, 'type'=>'select', 'options'=>$types));
												else :
													echo $this->Form->input('User.type', array('div'=>false, 'label'=>false, 'type'=>'select', 'options'=>$types)); 
												endif;		
											?>
										</td>
									</tr>
									<tr class="tr">
										<td><strong>Expiry</strong></td>
										<td><?php echo $this->Form->input('User.expires', array('div'=>false, 'label'=>false, 'type'=>'date', 'dateFormat'=>'DMY', 'class'=>'select')); ?></td>
									</tr>
									<tr class="tr">
										<td style="padding-right:10px;"><strong>Print coupon limit count</strong></td>
										<?php 
											if($this->Form->value('User.used_count')=='') 
												$defaultCount = -1;
											else 
												$defaultCount = $this->Form->value('User.used_count');
										?>
										<td><?php echo $this->Form->input('User.used_count', array('value'=>$defaultCount, 'div'=>false, 'label'=>false)); ?></td>
									</tr>
								</table>
				    		</td>
						    <td valign="top">
								<table>	
									<tr class="tr">
										<td><strong>Registered</strong></td>
										<td><?php echo $this->Form->input('User.registered', array('div'=>false, 'label'=>false)); ?></td>
									</tr>
									<tr class="tr">
										<td style="padding-right:10px"><strong>Receive Newsletter</strong></td>
										<td><?php echo $this->Form->input('User.newsletter', array('div'=>false, 'label'=>false)); ?></td>
									</tr>
								</table>
						    </td>
				   		</tr>
					</table>
					<?php echo $this->Form->input('User.id', array('type' => 'hidden')); ?>
	         	</div>
			    <div style="margin-top:10px;">
					<table>
						<tr class="tr">
							<td style="width:375px;"></td>  
							<td >
								<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(2)">Next</button>
								<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(0)">Previous</button>
							</td>
						</tr>
					</table>
				</div>
		   	</div>
		   	<div class="tabbertab"  style="min-height:100px;overflow:hidden;">
				<h2>Points</h2>
					<div class="tabber" id="mytab2" style="padding:10px;min-height:110px;overflow:hidden;margin-left:-10px;margin-top:-8px;width:820px;">
					<div class="tabbertab" style="width:777px;min-height:100px;">
							<h2>History</h2>
								<div class="list">
									<table style="width: 787px; margin-left: -20px;">
										<tbody>
											<tr class="tr">
												<th style="width:0.8%;text-align:left;padding: 0.5em 1em 0.59em 0.9em !important;background-color:#cccccc;border:1px solid #969090;">Activity</th>
												<th style="width:1%;text-align:left;padding: 0.5em 1em 0.59em 0.9em !important;background-color:#cccccc;border:1px solid #969090;">Product Name</th>
												<th style="width:1%;text-align:left;padding: 0.5em 1em 0.59em 0.9em !important;background-color:#cccccc;border:1px solid #969090;">Points</th>
												<th style="width:0.5%;text-align:left;padding: 0.5em 1em 0.59em 0.9em !important;background-color:#cccccc;border:1px solid #969090;">Activiry Date</th>
											</tr>
										</tbody>
											<?php
											$i = 0;
											foreach ($user_points_data as $user_points_data1):
												$class = null;
												if ($i++ % 2 == 0) {
													$class = ' class="altrow"';
												}
											?>
											<tr <?php echo $class;?>>
												<td style="white-space:nowrap;padding: 0.29em 3.9em 0.29em 0.9em !important;">
													<?php echo $user_points_data1['RedeemTracker']['activity'];?>
												</td>
												<td style="white-space:nowrap;padding: 0.29em 3.9em 0.29em 0.9em !important;">
													<a href=<?php echo "/products/view/".$gmp['RedeemTracker']['product_id'] ?>><?php echo $user_points_data1['Products']['name'];?></a>
												</td>
												<td style="white-space:nowrap;padding: 0.29em 3.9em 0.29em 0.9em !important;">
													<?php echo $user_points_data1['RedeemTracker']['points'];?>
												</td>
												<td style="white-space:nowrap;padding: 0.29em 3.9em 0.29em 0.9em !important;">
													<?php echo $user_points_data1['RedeemTracker']['activity_date'];?>
												</td>
											</tr>
										<?php endforeach; ?>
									</table>
								 </div>
									<div align="center" style="margin-top:30px;">
										<button type="button" class="admin_button" onclick="document.getElementById('mytab2').tabber.tabShow(1)">Next</button>
									</div>     		
							</div>
							 <div class="tabbertab" style="height:180px;width:777px;background-color:#EFEFEF">
								<h2>Add Points</h2>
								    <table width="100%">
										<tr>
												<td style="width:100px"><strong>Total Points</strong></td>
												<td><?php echo $this->Form->input('User.points',array('readonly'=> 'true', 'div'=>false, 'label'=>false, 'type'=>'text')); ?></td>
											</tr>
										<tr>
												<td style="width:100px"><strong>Add New Points</strong></td>
												<td>
													<?php if($this->Session->read('user.User.type') == 'Client Admin' || $this->Session->read('user.User.type') == 'Country Admin'){?>
													<input type="text" name="user_points" readonly>
													<?php }else{?> 
													<input type="text" name="user_points">
													<?php }?>
												</td>
											</tr>
											<tr>
												<td style="width:100px"><strong>Description</strong></td>
												<td>
													<?php if($this->Session->read('user.User.type') == 'Client Admin' || $this->Session->read('user.User.type') == 'Country Admin'){?>
													<textarea  cols="30" name="desc" readonly></textarea>
													<?php }else{?> 
													<textarea  cols="30" name="desc"></textarea>
													<?php }?>
												</td>
											</tr>
								</table>
								<div align="center" style="margin-top:36px;">
									<button type="button" class="admin_button" onclick="document.getElementById('mytab2').tabber.tabShow(0)">Previous</button>
								</div>
							 </div>
						</div>
					<div align="center" style="margin-top:10px;">
						<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(2)">Previous</button>
					</div>
										
			</div>
	      	<div class="tabbertab"  style="min-height:108px;overflow:hidden">
		        <h2>Change Log</h2>
		        <h4>Change Log</h4>
		        <div style="border:1px solid #cccccc;padding:10px;width:790px;min-height:110px;overflow:hidden">
			        <div class="list" id="log_list" style="width:800px;">
						<div id="Log" style="margin:0;">Retrieving changes...</div>
						<script type="text/javascript">
						$(function() {
							$.ajax({
								url: "/admin/logs/ajax_list_new/<?php echo Inflector::singularize($this->name);?>/<?php echo $this->Form->value(Inflector::singularize($this->name). '.id');?>",
								type: "GET",
								success: function (data) {
									$('#Log').html(data);
								}
							});
						});</script>
					</div>
				</div>
				<div align="center" style="margin-top:10px;">
					<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(1)">Previous</button>
				</div>
			</div>
	 	</div>
	</div>
</form>