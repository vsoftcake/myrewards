<style>
	.current
	{
	    font-family: 'Droid Sans',sans-serif;
	    font-size: 12px;
	   	padding:5px 8px !important;   
	    background-color: #0066CC;
	    border: 1px solid #DFDFDF;
	    color:#ffffff;
	}
	.cpagination1
	{
		/*padding:1px 2px 1px 2px;*/
		font-family: 'Droid Sans',sans-serif;
	    font-size: 12px;
		text-decoration:none !important;
		background-color: #FFFFFF;
		padding:3px 8px;
		border: 1px solid #0066CC;
		color:#808080 !important;
	}
	#content
	{
		border-left:1px solid #cccccc;
		border-right:1px solid #cccccc;
		border-bottom:1px solid #cccccc;
		margin-left:20px !important;
		padding-left:15px !important;
		width:916px;
	}
	.admin_button
	{
		height: 24px;
		padding-top: 2px;
		padding-bottom: 5px;
		border-radius: 5px 5px 5px 5px;
		background-color: #0066cc;
		border: 1px solid #cccccc;
		color: #FFFFFF; 
		cursor: pointer;
		font-size:13px;
	}
</style>

<table style="margin-left:-2px;">
	<tr>
		<td>
            &nbsp;&nbsp;<img style="width:40px;height:40px;" src="/files/admin_home_icons/users.png"  alt="Userss" title="Users" />&nbsp;&nbsp;
        </td>
        <td> 
            <h2><?php echo "Users";?></h2>
        </td>
        <td style="padding-left:300px;">
          <!-- <?php echo $this->Html->link('Create new user', array('action'=>'edit')); ?> -->
           <button class ="admin_button" style="width:130px" onclick="location.href='<?php echo $this->Html->url('edit'); ?>';">Create New User</button>
        </td>
        <td style="padding-left:25px">   
		 <!--  <?php echo $this->Html->link('Upload multiple users', array('action'=>'upload')); ?> -->
		   <button class ="admin_button" style="width:165px" onclick="location.href='<?php echo $this->Html->url('upload'); ?>';">Upload Multiple Users</button>
		</td>
		<td style="padding-left:25px">   
		 <!--  <?php echo $this->Html->link('Delete multiple users', array('action'=>'delete_multiple')); ?> -->
		    <button class ="admin_button" style="width:170px" onclick="location.href='<?php echo $this->Html->url('delete_multiple'); ?>';">Delete Multiple Users</button>
        </td>
    </tr>        
</table>
<div class="actions" style="padding-bottom:20px;">
	<form action="<?php echo $this->Html->url('/admin/users/'); ?>" method="get" id="SearchForm">
	<table style="width:920px;background-color:#cccccc;height:65px;border:1px solid #969090;">
		<tr>
			<td style="padding-left:5px;"><strong>Find Users</strong></td>
			<td>
				<?php echo $this->Form->input('UserSearch.search', array('div'=>false, 'label'=>false)); ?>
				<script type="text/javascript">
				$(function() {
					if ($('#UserSearch').val() == '') {
						$('#UserSearch').val('ID, username, name or email');
						$('#UserSearch').css( "color", "#aaaaaa" );
					}
					$( "#UserSearch" ).focus(function() {
						if ($(this).val() == 'ID, username, name or email') {
							$(this).val('');
							$(this).css( "color", "#000" );
						}
					});
					$( "#SearchForm" ).submit(function() {
						if ($("#UserSearch").val() == 'ID, username, name or email') {
							$("#UserSearch").val('');
							$("#UserSearch").css( "color", "#000" );
						}
					});
				});
				</script>
			</td>
		
			<td><strong>Limit to Clients</strong></td>
			<td>
				<?php if($this->Session->read('user.User.type') == 'Client Admin' || $this->Session->read('user.User.type') == 'Country Admin') : ?>
					<?php echo $this->Form->input('UserSearch.client_id', array('options' => $clients,'default'=>$this->Session->read('user.User.client_id'),'disabled'=>'disabled', 'style'=>'width:200px','label'=>false,'empty'=>' ')); ?>
				<?php else : ?>
					<?php echo $this->Form->input('UserSearch.client_id', array('options' => $clients,'style'=>'width:200px','label'=>false,'empty'=>'', 'type'=>'select')); ?>
				<?php endif; ?>
			</td>
		
			<td><strong>Limit to</strong></td>
			<td><?php echo  $this->Form->input('UserSearch.limit_to', array('div'=>false, 'label'=>false, 'type'=>'select', 'options'=>$limits,'empty'=>' ')); ?></td>
		
			<td></td>
			<td><button class="admin_button">Search</button></td>
		</tr>
	</table>
	</form>
	
</div>


<div class="list" style="width:920px;">
	<table cellpadding="0" cellspacing="0" width="920">
	<tr height="30">
		<th style="background-color:#cccccc;text-align:left;padding-left:5px;border-left:1px solid #969090;border-bottom:1px solid #969090;border-top:1px solid #969090;"><?php echo $this->Paginator->sort('id');?></th>
		<th style="background-color:#cccccc;text-align:left;padding-left:1px;border-bottom:1px solid #969090;border-top:1px solid #969090;"><?php echo $this->Paginator->sort('username');?></th>
		<th style="background-color:#cccccc;text-align:left;padding-left:1px;border-bottom:1px solid #969090;border-top:1px solid #969090;">Name</th>
		<th style="background-color:#cccccc;text-align:left;padding-left:1px;border-bottom:1px solid #969090;border-top:1px solid #969090;"><?php echo $this->Paginator->sort('email');?></th>
		<th style="background-color:#cccccc;text-align:left;padding-left:1px;border-bottom:1px solid #969090;border-top:1px solid #969090;">Client</th>
		<th class="actions" style="background-color:#cccccc;text-align:left;padding-left:4px;border-right:1px solid #969090;border-bottom:1px solid #969090;border-top:1px solid #969090;">Actions</th>
	</tr>
	<?php
	$i = 0;
	foreach ($users as $user):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
		<tr<?php echo $class;?>>
			<td style="width:20px">
				<?php echo $user['User']['id'] ?>
			</td>
			<td style="width:60px">
				<?php echo $user['User']['username'] ?>
			</td>
			<td style="width:230px">
				<?php echo $user['User']['first_name'] ?> <?php echo $user['User']['last_name'] ?>
			</td>
			<td style="width:90px">
				<?php echo $user['User']['email'] ?>
			</td>
			<td style="width:150px">
				<a href="<?php echo $this->Html->url('/admin/clients/edit/'. $user['Client']['id']); ?>"><?php echo $user['Client']['name'] ?></a>
			</td>
			<td class="actions" style="width:100px">
				<?php echo $this->Html->link(__('Edit', true), array('action'=>'edit', $user['User']['id'])); ?> |
				<?php echo $this->Html->link(__('Delete', true), array('action'=>'delete', $user['User']['id']), null, sprintf(__('Are you sure you want to delete User \'%s\'?', true), $user['User']['username'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
	<div class="paging" style="padding-top:15px;">
		<table style="border:none !important;width:925px;">
			<tr>
				<td style="border:none !important;float:right;" valign="top">
					<?php echo $this->Paginator->first('<<', array('title'=>'first','class'=>'disabled cpagination1'));?>
					<?php echo $this->Paginator->prev('<', array('title'=>'prev','class'=>'disabled cpagination1'));?>
					<?php echo $this->Paginator->numbers(array('separator' => ' ','class'=>'cpagination1'));?>
					<?php echo $this->Paginator->next('>', array('title'=>'next','class'=>'disabled cpagination1'));?>
					<?php echo $this->Paginator->last('>>', array('title'=>'last','class'=>'disabled cpagination1'));?>
					<br/>
					<p><?php echo $this->Paginator->counter(array('format' => __('Page %page% of %pages%', true)));	?></p>
				</td>
			</tr>
		</table>
	</div>
</div>