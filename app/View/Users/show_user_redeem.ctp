
<p style="font-size:16px;font-weight:bold;padding-left:50px">Redeemed Product Detail</p>
<table style="margin-left:50px;margin-bottom:20px;">
	<tr>
		<td style="width:100px;padding:10px"><strong>Product Id</strong></td>
		<td style="padding:10px"><?php echo $details['Product']['id'];?></td>
	</tr>
	<tr>
		<td style="padding:10px"><strong>Product Name</strong></td>
		<td style="padding:10px"><?php echo $details['Product']['name'];?></td>
	</tr>
	<tr>
		<td style="padding:10px"><strong>Offer</strong></td>
		<td style="padding:10px"><?php echo $details['Product']['highlight'];?></td>
	</tr>
	
	<tr>
		<td style="padding:10px"><strong>Points</strong></td>
		<td style="padding:10px"><?php echo $points;?></td>
	</tr>
		
	<tr>
		<td style="padding:10px"><strong>Activity Date</strong></td>
		<td style="padding:10px"><?php echo $activity_date;?></td>		
	</tr>	
	 
	<tr>
		<td style="padding:10px"><strong>Quantity</strong></td>
		<td style="padding:10px"><?php echo $qty;?></td>		
	</tr>
	
</table>
