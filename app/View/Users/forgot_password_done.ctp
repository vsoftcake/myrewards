<div class="main">
	<?php //echo $this->element('message'); ?>
	<div class="module">
		<div class="t content tab_padding" style="font-size:15px;">
			<?php echo $this->element('message'); ?>
			<?php if ($page['ClientPage']['h1'] != '') : ?>
				<h1><?php echo $page['ClientPage']['h1']; ?></h1>
			<?php endif; ?>
			<?php if ($page['ClientPage']['h2'] != '') : ?>
				<h2><?php echo $page['ClientPage']['h2']; ?></h2>
			<?php endif; ?>
			<?php if ($page['ClientPage']['h3'] != '') : ?>
				<h3><?php echo $page['ClientPage']['h3']; ?></h3>
			<?php endif; ?>
			<?php if ($page['ClientPage']['content'] != '') : ?>
				<?php echo $page['ClientPage']['content']; ?>
			<?php endif; ?>
		</div>
		
		<div class="rounded-corners">
			<div class="content">
				<p>Your login details have been sent to: <?php echo $user['User']['email']; ?></p>
				<form action="<?php echo $this->Html->url('/users/forgot_password');?>" method="post" class="standard">
					<table>
						<tr>
							<td>Email address:</td>
							<td>
								<?php echo $this->Form->input('User.email',array('class'=>'search_input')); ?>
								<?php echo $this->Form->error('User.email', array('valid_email' => 'Invalid email address'), array('valid_email' => 'Invalid email address	')); ?>
							</td>
						</tr>
						<tr>
							<td></td>
							<td><button type="submit" id="submit" class="rounded_corners_button" style="padding-bottom:5px;">Submit request</button></td>
						</tr>
						<tr>
							<td></td>
							<td><br/><a href="<?php echo $this->Html->url('/users/login'); ?>">Return to login</a></td>
						</tr>
					</table>	
				</form>
				<?php echo $this->Form->focus('User.email'); ?>
				&nbsp;
			</div>
			<div class="b rounded-corners_bottom">&nbsp;</div>
		</div>
	</div>
</div>