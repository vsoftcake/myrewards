<?php if ($step == '1') : ?>
<h2>Upload Users</h2>
<div class="actions">
	<ul>
		<li><?php echo $this->Html->link('Cancel', '/admin/users'); ?></li>
	</ul>
</div>
<div class="edit">
<p>
	File must be in csv format with the following columns (column name must be in the first row):<br/>
	<ul>
		<li>client - client name (required) </li>
		<li>username - users username (required)</li>
		<li>password - users password</li>
		<li>email - users email (required)</li>
		<li>first_name - users first name</li>
		<li>last_name - users last name</li>
		<!--<li>state - users state, must be one of ACT, NSW, NT, QLD, SA, TAS, VIC, WA</li>-->
		<li>newsletter - '1' if the user is to recieve the newsletter</li>
		<li>registered - '1' of the user is registered</li>
		<li>expires - expiry date in YYYY-MM-DD format</li>
	</ul>
	<a href="<?php echo $this->Html->url('/admin/users/example_users_import'); ?>">Download example</a>
</p>
<form action="<?php echo $this->Html->url('/admin/users/upload/2'); ?>" method="post" enctype="multipart/form-data">
	<table>
		<tr>
			<td class="bt">CSV file</td>
			<td class="bt">
				<?php echo $this->Form->file('User.users'); ?><br/>
				Max file size: <?php echo (ini_get('post_max_size') < ini_get('upload_max_filesize'))? ini_get('post_max_size'): ini_get('upload_max_filesize'); ?>
			</td>
			<td class="bl"><?php if (isset($upload_error)) { echo $upload_error; } ?></td>
		</tr>
		<tr>
			<td class="bb"></td>
			<td class="bb br"><button type="submit">Submit</button></td>
		</tr>
	</table>
	</form>
</div>
<?php elseif ($step == '2') : ?>
<h2>Confirm import</h2>
<div class="actions">
	<ul>
		<li><?php echo $this->Html->link('Cancel', '/admin/users'); ?></li>
	</ul>
</div>
	<br/>
	<?php echo $uploaded_users; ?> users sent, <?php echo $good_users; ?> users will be imported.
	<?php if (!empty($upload_errors)) : ?>
		<h5>The following rows contain errors and will be ignored:</h5>
		<div class="edit">
		<table>
			<tr>
				<td class="bt">Row</td><td class="bt br">Error</td>
			</tr>
		<?php foreach ($upload_errors as $error) : ?>
			<tr><td><?php echo $error['row']; ?></td><td class="br"><?php echo $error['error']; ?></td></tr>
		<?php endforeach; ?>
			<tr>
				<td class="bb"></td><td class="bb br"></td>
			</tr>
		</table>
		</div>
	<?php endif; ?>
<div style="clear:both">
	<br/>
	<p><a href="<?php echo $this->Html->url('/admin/users/upload/3'); ?>">Confirm</a> or</p>
	<p><a href="javascript:void(0)" onclick=" $('reupload').style.display = 'block'; return false;">Re-upload</a></p>
	<div class="edit" style="display:none;" id="reupload">
	<form action="<?php echo $this->Html->url('/admin/users/upload/2'); ?>" method="post" enctype="multipart/form-data">
		<table>
			<tr>
				<td class="bt">CSV file</td>
				<td class="bt">
					<?php echo $this->Form->file('User.users'); ?><br/>
					Max file size: <?php echo (ini_get('post_max_size') < ini_get('upload_max_filesize'))? ini_get('post_max_size'): ini_get('upload_max_filesize'); ?>
				</td>
				<td class="bl"><?php if (isset($upload_error)) { echo $upload_error; } ?></td>
			</tr>
			<tr>
				<td class="bb"></td>
				<td class="bb br"><button type="submit">Submit</button></td>
			</tr>
		</table>
		</form>
	</div>
</div>

<?php elseif ($step == '3') : ?>
<h2>Import complete</h2>
<div class="actions">
	<ul>
		<li><?php echo $this->Html->link('Done', '/admin/users'); ?></li>
	</ul>
</div>
<?php echo $user_count; ?> users imported.
<?php endif; ?>