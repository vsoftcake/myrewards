	
	<div class="module" style="width:952px;">
		<div class="t content tab_padding" style="font-size;15px;">
			
		</div>
		<div class="rounded-corners">
			<div class="content">
				<p><b>Step 3</b>: Join up complete.</p>
				<p>Thank for registering to <?php echo $this->Session->read('client.Client.name'); ?>. Your registration is now complete.</p>
				<p><a href="<?php echo $this->Html->url('/'); ?>">Click here to access your member benefits.</a></p>
				<p>If you would like to edit your member details at any stage please <a href="javascript:void(0)" onclick="editDetails()">click here</a></p>
				<br/>
				<?php if ($this->Session->read('client.Client.fb_share_disabled') == 1) {
					$fbCName = $this->Session->read('client.Client.name');
					$fbText = $this->Session->read('client.Client.fb_description_txt');
					$fbText = str_replace("\"", "'", $fbText);?>
					<p><?php echo $fbText ?></p>
					<p><a title='Share to Facebook' href="http://www.facebook.com/sharer/sharer.php?s=100&p[url]=<?php echo $this->Session->read('client.Domain.name'); ?>&p[title]=<?php echo addslashes($fbCName)?>&p[summary]=<?php echo addslashes($fbText)?>" target="_blank"><img src='/images/fbshare.jpg'></a></p>
				<?php }?>
			</div>
			<div class="b rounded-corners_bottom">&nbsp;</div>
		</div>
	</div>
