	<style type="text/css">
		
		#content
		{
			border-left:1px solid #cccccc;
			border-right:1px solid #cccccc;
			border-bottom:1px solid #cccccc;
			margin-left:20px !important;
			padding-left:18px !important;
			width:913px;
		}
		.rounded-corners 
		{
			-moz-border-radius: 10px 10px 0px 0px; /* Firefox */
			-webkit-border-radius: 10px 10px 0px 0px; /* Safari, Chrome */
			border-radius: 10px 10px 0px 0px; /* CSS3 */
		}
		.tr 
		{
			height:35px;
		}
		.admin_button
		{
			height: 26px;
			padding-bottom: 2px;
			border-radius: 5px 5px 5px 5px;
			background-color: #0066cc;
			border: 1px solid #cccccc;
			color: #FFFFFF; 
			cursor: pointer;
			font-size:13px;
		}
	</style>
	
	<?php echo $this->Html->script('jscolor'); ?>
	<script type="text/javascript">
		document.write('<style type="text/css">.tabber{display:none;}<\/style>');
		function confirmation(name,id)
	    {
			var answer = confirm("Are you sure you want to delete domain '<?php echo $this->Form->value('Domain.name') ?>'")
			if (answer)
			{
				location.href="/admin/domains/delete/<?php echo $this->Form->value('Domain.id') ?>";
			}
	    }
	</script>
	<form action="<?php echo $this->Html->url('/admin/domains/edit/'. $this->Html->value('Domain.id')); ?>" method="post">	
		<div style="width:953px;margin-left:-14px;">
			<div style="width:949px;height:50px;">
				<div style=" height: 40px; float:left; width: 608px;padding-left:37px;">
			       	<div style=" height: 40px; float:left;padding-top:5px;">
			        	<img title="Domains" alt="Domains" src="/files/admin_home_icons/domains.png" style="width:40px;height:40px;">
			        </div>
			        <div style=" height: 40px;float:left;padding-left:10px;">
			        	<h2>Edit Domain <?php if($this->Form->value('Domain.name')!=''):?>- <?php echo $this->Form->value('Domain.name') ;?><?php endif;?></h2>
			        </div> 
				</div>
		        <div class="actions" style=" height: 40px; float:left;  width: 300px;">
					<ul style="padding:20px;margin-left:-28px;">
						<li style="display:inline;"><?php echo $this->Form->button('Cancel', array('onclick' => "location.href='".$this->Session->read('redirect')."'",
						'class'=>'admin_button','style'=>'width:60px;', 'type'=>'button'));  ?></li>
						<?php if($this->Form->value('Domain.id')!='') : 
						$conname=$this->Form->value('Domain.name');$conid=$this->Form->value('Domain.id');?>
						<li style="display:inline;"><?php echo $this->Form->button('Delete Domain', array('onclick' => "confirmation('$conname','$conid')",'style'=>'width:110px;','class'=>'admin_button','type'=>'button'));  ?></li>
			       		<?php endif;?>
			        	<li style="display:inline;"><?php echo $this->Form->button('Submit', array('class'=>'admin_button','style'=>'width:60px;','type'=>'submit'));  ?></li>
			        </ul>
				</div>
			</div>

			<?php echo $this->Form->input('Domain.id', array('type' => 'hidden')); ?>
			<div class="tabber" id="mytab1">
				<div class="tabbertab" style="min-height:300px;overflow:hidden">
		  			<h2>General Information</h2>
		    		<h4>Domain General Information</h4>
	     			<div style="background-color:#EFEFEF;border:1px solid #cccccc;padding:10px;width:790px;min-height:200px;overflow:hidden">
		 				<table>
							<tr class="tr">
								<td class="bt"><strong>Name</strong></td>
								<td class="bt"><?php echo $this->Form->input('Domain.name', array('div'=>false, 'label'=>false)); ?></td>
								<td class="bl"><?php echo $this->Form->error('Domain.name', array('required' => 'Name required', 'unique' => 'Name already in use'), array('required' => 'Name required')); ?></td>
							</tr>
							<tr class="tr">
								<td><strong>Password</strong></td>
								<td><?php echo $this->Form->input('Domain.password', array('type' => 'text', 'div'=>false, 'label'=>false)); ?></td>
								<td class="bl"><?php echo $this->Form->error('Domain.password', array('required' => 'Password required'), array('required' => 'Password required')); ?></td>
							</tr>
							<tr class="tr">
								<td><strong>Home page</strong><br/><sub>Redirects the first page you see when entering<br/>this domain. This happens before you log in</sub></td>
								<td><?php echo $this->Form->input('Domain.home_page', array('type' => 'text', 'div'=>false, 'label'=>false)); ?></td>
								<td class="bl"></td>
							</tr>
							<tr class="tr">
								<td><strong>Public</strong></td>
								<td class="br">
									<?php echo $this->Form->checkbox('Domain.public', array('div'=>false, 'label'=>false)); ?>
								</td>
							</tr>
							<tr class="tr">
								<td><strong>Users editable by all clients</strong><br/><sub>Check this to enable all clients of this<br/> domain edit all users of this domain</sub></td>
								<td class="br">
									<?php echo $this->Form->checkbox('Domain.all_client', array('div'=>false, 'label'=>false)); ?>
								</td>
							</tr>
							<tr class="tr">
								<td><strong>Default Client</strong></td>
								<td><?php echo $this->Form->input('Domain.default_client_id', array('div'=>false, 'label'=>false, 'type'=>'select', 'options'=>$clients)); ?></td>
								<td class="bl"></td>
							</tr>
						</table>
			 		</div>
			 		<br/>
			       	<div>
						<table>
							<tr class="tr">
								<td style="width:375px;"></td>  
								<td class="bb br" style="">
									<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(1)">Next</button>
								</td>
							</tr>
						</table>
			       	</div>
			 	</div>
			 	<div class="tabbertab" style="min-height:260px;overflow:hidden;">
			 		<h2>Change Log</h2>
			 		<h4>Change Log</h4>
					<div class="list" id="log_list" style="border:1px solid #cccccc;padding:10px;width:790px;min-height:200px;overflow:hidden">
						<div id="Log" style="margin:0;">Retrieving changes...</div>
						<script type="text/javascript">
						$(function() {
							$.ajax({
								url: "/admin/logs/ajax_list_new/<?php echo Inflector::singularize($this->name);?>/<?php echo $this->Form->value(Inflector::singularize($this->name). '.id');?>",
								type: "GET",
								success: function (data) {
									$('#Log').html(data);
								}
							});
						});</script>
					</div>
					<div align="center" style="margin-top:10px;">
						<button type="button" style="margin-top:10px" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(0)">Previous</button>
					</div>
				</div>
			</div>
		</div>	
	</form>