<script type="text/javascript" charset="utf-8">
	function selectAll(chkObj)
	{
		var multi=document.getElementById('ClientClient');
		if(chkObj.checked)
			for(i=0;i<multi.options.length;i++)
			multi.options[i].selected=true;
		else
			for(i=0;i<multi.options.length;i++)
			multi.options[i].selected=false;	
	}
</script>
<style type="text/css">
	#content
	{
		border-left:1px solid #cccccc;
		border-right:1px solid #cccccc;
		border-bottom:1px solid #cccccc;
		margin-left:20px !important;
		padding-left:18px !important;
		width:913px;
	}
	.rounded-corners 
	{
		-moz-border-radius: 10px 10px 0px 0px; /* Firefox */
		-webkit-border-radius: 10px 10px 0px 0px; /* Safari, Chrome */
		border-radius: 10px 10px 0px 0px; /* CSS3 */
	}
	.tr 
	{
		height:35px;
	}
	.admin_button
	{
		height: 26px;
		padding-bottom: 1px;
		border-radius: 5px 5px 5px 5px;
		background-color: #0066cc;
		border: 1px solid #cccccc;
		color: #FFFFFF; 
		cursor: pointer;
		font-size:13px;
	}
</style>
<?php echo $this->Html->script('jscolor'); ?>
<script type="text/javascript">
	document.write('<style type="text/css">.tabber{display:none;}<\/style>');
	function confirmation(name,id)
    {
		var answer = confirm("Are you sure you want to delete Skyscraper '<?php echo $this->Form->value('Skyscraper.sname') ?>'");
		if (answer)
		{
			location.href="/admin/skyscrapers/delete/<?php echo $this->Form->value('Skyscraper.id') ?>";
		}
    }
</script>
<form action="<?php echo $this->Html->url('/admin/skyscrapers/edit/'. $this->Html->value('Skyscraper.id')); ?>" enctype="multipart/form-data"  method="post">
	<div style="width:953px;margin-left:-14px;">
		<div style="width:949px;height:50px;">
			<div style=" height: 40px; float:left; width: 608px;padding-left:37px;">
		       	<div style=" height: 40px; float:left;padding-top:5px;">
		        	<img title="Skyscraper" alt="Skyscraper" src="/files/admin_home_icons/skyscraper.png" style="width:40px;height:40px;">
		        </div>
		        <div style=" height: 40px;float:left;padding-left:10px;">
		        	<h2>Edit Skyscraper Banner <?php if($this->Form->value('Skyscraper.sname')!= '' ) : ?> - <?php echo $this->Form->value('Skyscraper.sname') ?> <?php endif;?></h2>
		        </div> 
			</div>
	        <div class="actions" style=" height: 40px; float:left;  width: 300px;">
				<ul style="padding:20px;margin-left:-28px;">
					<li style="display:inline;"><?php echo $this->Form->button('Cancel', array('onclick' => "location.href='".$this->Session->read('redirect')."'",
					'class'=>'admin_button','style'=>'width:60px;', 'type'=>'button'));  ?></li>
					<?php if($this->Form->value('Skyscraper.id')): ?>
					<?php if($this->Session->read('user.User.type') != 'Client Admin' && $this->Session->read('user.User.type') != 'Country Admin') :
					$conname=$this->Form->value('Skyscraper.sname');$conid=$this->Form->value('Skyscraper.id'); ?>
						<li style="display:inline;"><?php echo $this->Form->button('Delete Skyscraper', array('onclick' => "confirmation('$conname','$conid')",'style'=>'width:130px;','class'=>'admin_button','type'=>'button'));  ?></li>
			        	<?php endif;?>
			        <?php endif;?>
		        	<li style="display:inline;"><?php echo $this->Form->button('Submit', array('class'=>'admin_button','style'=>'width:60px;','type'=>'submit'));  ?></li>
		        </ul>
			</div>
		</div>	

		<div class="tabber" id="mytab1">
			<div class="tabbertab" style="mn-height:340px;overflow:hidden;">
		  		<h2>General Information</h2>
		    	<h4>Skyscraper General Information</h4>
	     		<div style="background-color:#EFEFEF;border:1px solid #cccccc;padding:10px;width:790px;min-height:150px;overflow:hidden">
	            	<table width="770">
						<tr class="tr">
							<td width="150"><strong>Name</strong></td>
							<td><?php echo $this->Form->input('Skyscraper.sname', array('div'=>false, 'label'=>false)); ?></td>
							<td></td>
						</tr>
						<tr class="tr">
							<td><strong>Url Link</strong></td>
							<td><?php echo $this->Form->input('Skyscraper.link_url', array('div'=>false, 'label'=>false)); ?></td>
							<td class="bl"></td>
						</tr>
						<tr class="tr">
							<td valign="top"><strong>Select Program</strong></td>
							<td>
								<?php 
						    		if($this->Session->read('user.User.type') == 'Client Admin' || $this->Session->read('user.User.type') == 'Country Admin') : 
						    			if($this->Html->value('Skyscraper.id') != '') : 
						    				echo $this->Form->input('Program.Program', array('div'=>false, 'label'=>false, 'type'=>'select', 'options'=>$programlist, 'multiple' => 'multiple','disabled'=>'disabled')); 
						    			else :
						    				echo $this->Form->input('Program.Program', array('div'=>false, 'label'=>false, 'type'=>'select', 'options'=>$programlist, 'multiple' => 'multiple','style'=>'height:25px')); 
						    			endif;
									else : 
										echo $this->Form->input('Program.Program', array('div'=>false, 'label'=>false, 'type'=>'select', 'options'=>$programlist, 'multiple' => 'multiple')); 
										$options = array('url' => 'update_program_clients','update' => 'ClientClient');
							           	echo $this->Ajax->observeField('ProgramProgram', $options); 
						    		endif; 
						    	?>
						    </td>
							<td></td>
						</tr>
						<tr class="tr">
							<td valign="top"><strong>Select Client</strong></td>
							<td>
								<?php if($this->Session->read('user.User.type') == 'Client Admin' || $this->Session->read('user.User.type') == 'Country Admin') : ?>
									<table style="border:none !important;">
									<tr>
										<td>
											<?php if($this->Html->value('Skyscraper.id') != '') : ?>
												<?php echo $this->Form->input('Client.Client', array('multiple' => 'multiple','disabled'=>'disabled', 'div'=>false, 'label'=>false)); ?>
											<?php else : ?>
												<?php echo $this->Form->input('Client.Client', array('multiple' => 'multiple','style'=>'height:25px', 'div'=>false, 'label'=>false)); ?>
											<?php endif; ?>
											<input type="hidden" name="data[Client][Client][]" value="" /><br/>
											<sub>Select program and client</sub>
										</td>
									</tr>
								</table>
								<?php else : ?>
									<table style="border:none !important;">
										<tr>
											<td>
												<?php echo $this->Form->input('Client.Client', array('multiple' => 'multiple', 'div'=>false, 'label'=>false)); ?>
												<input type="hidden" name="data[Client][Client][]" value="" /><br/>
												<sub>Select program to load clients</sub>
											</td>
											<td valign="bottom" style="padding-bottom:12px;">
												<?php echo $this->Form->checkbox('done', array('onclick' => "selectAll(this)")); ?>&nbsp;
												<span style="color:red">Check to select <b>All Clients</b></span>
											</td>
										</tr>
									</table>
								<?php endif; ?>
							</td>
							<td></td>
						</tr>
					</table>
				</div>
						
				<div style="padding-top:10px;" align="center">
					<table>
						<tr class="tr">
							<td><button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(1)">Next</button></td>
						</tr>
					</table>
		       	</div>
			</div>
			<div class="tabbertab" style="min-height:250px;overflow:hidden;">
	  			<h2>Image</h2>
	    		<h4>Skyscraper Image</h4>
	     		<div style="background-color:#EFEFEF;border:1px solid #cccccc;padding:10px;width:790px;min-height:200px;overflow:hidden">
					<table>
						<tr class="tr">
							<td valign="top"><strong>Image</strong></td>
							<td style="padding-left:5px;">
								<img src="<?php echo $this->Html->url(SKYSCRAPER_IMAGE_WWW_PATH. $this->Form->value('Skyscraper.id'). '.'. $this->Form->value('Skyscraper.scraper_extension'). '?rand='. time()); ?>" alt="" /><br/>
								<a href="javascript:void(0)" onclick="this.style.display='none'; $('#ImageFile').show(); return false;">Change</a>
								<?php echo $this->Form->file('Image.file', array('style' => 'display:none;')); ?><br/>
								Remove <?php echo $this->Form->checkbox('ImageDelete.file'); ?>
								<br/><br/><span style="color:red"><b>*&nbsp;Upload image with width 170px and height 670px</b></span><br/>
							</td>
						</tr>
					</table>
					<?php echo $this->Form->input('Skyscraper.id', array('type' => 'hidden')); ?>
				</div>
				<div id="hidden_div" style="display:none"></div>
				<div style="padding-top:20px;" align="center">
					<table>
						<tr class="tr">
							<td>
								<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(0)">Previous</button>
							</td>
						</tr>
					</table>
			   	</div>
			</div>
		</div>
	</form>