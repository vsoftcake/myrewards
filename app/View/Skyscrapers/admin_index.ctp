<style>
	#content
	{
		border-left:1px solid #cccccc;
		border-right:1px solid #cccccc;
		border-bottom:1px solid #cccccc;
		margin-left:20px !important;
		padding-left:18px !important;
		width:913px;
	}
	.rounded-corners 
	{
		-moz-border-radius: 10px 10px 0px 0px; /* Firefox */
		-webkit-border-radius: 10px 10px 0px 0px; /* Safari, Chrome */
		border-radius: 10px 10px 0px 0px; /* CSS3 */
	}
	.current
	{
	    font-family: 'Droid Sans',sans-serif;
	    font-size: 12px;
	   	padding:5px 8px !important;   
	    background-color: #0066CC;
	    border: 1px solid #DFDFDF;
	    color:#ffffff;
	}
	.cpagination1
	{
		/*padding:1px 2px 1px 2px;*/
		font-family: 'Droid Sans',sans-serif;
	    font-size: 12px;
		text-decoration:none !important;
		background-color: #FFFFFF;
		padding:3px 8px;
		border: 1px solid #0066CC;
		color:#808080 !important;
	}
	.admin_button
	{
		height: 26px;
		padding-top: 2px;
		padding-bottom: 2px;
		border-radius: 5px 5px 5px 5px;
		background-color: #0066cc;
		border: 1px solid #cccccc;
		color: #FFFFFF; 
		cursor: pointer;
		font-size:13px;
	}
</style>

<div style="width:920px;">
	<table style="margin-left:-2px;margin-top: 10px;">
		<tr>
			<td>
	            &nbsp;&nbsp;<img style="width:40px;height:40px;" src="/files/admin_home_icons/skyscraper.png"  alt="Skyscrapers" title="Skyscrapers" />&nbsp;&nbsp;
	        </td>
	        <td> 
	            <h2><?php echo "Skyscrapers";?></h2>
	        </td>
	        <td width="560"></td>
	        <td>
	        	<button class ="admin_button" onclick="location.href='<?php echo $this->Html->url('edit'); ?>';">Create New Skyscraper</button>
	        </td>
	    </tr>        
	</table>
</div>
<div class="list" style="padding-top:15px;">
	<table cellpadding="0" cellspacing="0" width="920">
		<tr height="30">
			<th style="width:200px;background-color:#cccccc;text-align:left;padding-left:5px;border-left:1px solid #969090;border-bottom:1px solid #969090;border-top:1px solid #969090;"><?php echo $this->Paginator->sort('name');?></th>
			<th style="width:500px;background-color:#cccccc;text-align:left;padding-left:5px;border-bottom:1px solid #969090;border-top:1px solid #969090;"><?php echo $this->Paginator->sort('url');?></th>
			<th style="width:100px;background-color:#cccccc;text-align:left;padding-left:4px;border-right:1px solid #969090;border-bottom:1px solid #969090;border-top:1px solid #969090;" class="actions"><?php echo "Actions";?></th>
		</tr>
		<?php
		$i = 0;
		foreach ($skyscrapers as $skyscraper): ?>
		<?php
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td>
				<?php echo $skyscraper['Skyscraper']['sname'] ?>
			</td>
			<td>
				<?php echo $skyscraper['Skyscraper']['link_url'] ?>
			</td>
			<td class="actions">
				<?php echo $this->Html->link(__('Edit', true), array('action'=>'edit', $skyscraper['Skyscraper']['id'])); ?> 
				<?php if($this->Session->read('user.User.type') != 'Client Admin' && $this->Session->read('user.User.type') != 'Country Admin') : ?>
				|
				<?php echo $this->Html->link(__('Delete', true), array('action'=>'delete', $skyscraper['Skyscraper']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $skyscraper['Skyscraper']['id'])); ?>
				<?php endif; ?>
			</td>
		</tr>
		<?php endforeach; ?>
	</table>
	<div class="paging" style="padding-top:15px;float:right;">
		<table style="border:none !important;width:820px;">
			<tr>
				<td style="border:none !important;" valign="top">
					<?php echo $this->Paginator->first('<<', array('title'=>'first','class'=>'disabled cpagination1'));?>
					<?php echo $this->Paginator->prev('<', array('title'=>'prev','class'=>'disabled cpagination1'));?>
					<?php echo $this->Paginator->numbers(array('separator' => ' ','class'=>'cpagination1'));?>
					<?php echo $this->Paginator->next('>', array('title'=>'next','class'=>'disabled cpagination1'));?>
					<?php echo $this->Paginator->last('>>', array('title'=>'last','class'=>'disabled cpagination1'));?>
				</td>
				<td style="border:none !important;padding-top:5px;">
				<select name="page" id="selectpage" style="margin-top:-5px;">
					<?php for ($i = 1; $i <= $this->Paginator->counter(array('format' => '%pages%')); $i++):?>
					<option value="<?php echo $i;?>"<?php echo ($this->Paginator->current($params['page']) == $i) ? ' selected="selected"' : ''?>><?php echo $i ?></option>
					<?php endfor ?>
				</select>
				<script>
				$("#selectpage").change(function() {
					var url = window.location.href;
					var n = url.indexOf("/?");
					var extra = "";
					if(n>0){extra = url.substring(n,url.length);}
					window.location.href = '/admin/skyscrapers/index/page:'+$(this).val()+extra;
				});
				</script>
				</td>
			</tr>
		</table>
	</div>
</div>