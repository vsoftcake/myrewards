<table>
<tr class="tr1">
	<td valign="top">
		<table>
			<tr>
				<td valign="top" style="width:90px"><strong>Programs</strong></td>
				<td valign="top">
					<div style="width:300px;min-height:100px">
						<div style="float:left;height:350px;width:295px;background-color:#fff;overflow:auto;border:1px solid #ccc;margin-left:10px;">
							<?php 
								$check = "checked= 'checked'";
							?>
							<?php foreach ($programsList as $pval=>$pname):?>
								<input type="checkbox" id="ProgramProgram" <?php if(in_array($pval,$programs)) echo $check; ?> value="<?php echo $pval;?>" onclick="loadClientsList(this)" name="data[Program][Program][]" class="checkbox">
								&nbsp;&nbsp;<?php echo $pname ?><br>
							<?php endforeach; ?>
						</div>
					</div>
				</td>
			</tr>
		</table>
	</td>
	<td style="padding-left:18px" valign="top">
		<table>
			<tr>
				<td valign="top" style="width:70px"><strong>Clients</strong></td>
				<td valign="top">
					<div style="width: 250px; overflow: auto; height: 350px;background-color:#fff;border:1px solid #ccc;" id="clientslist">
						<?php $pid=0;?>
						<?php foreach ($allclients as $client):?>
						<?php if($pid != $client['Client']['program_id']){?>
							<?php if($pid>0){?></div><?php }?>
						<div style="width:245px;background-color:#fff;overflow:auto;border:1px solid #ccc;padding-bottom:5px;" id="ic<?php echo $client['Client']['program_id'] ?>">
						<strong><?php echo $client['Program']['name']; ?></strong><br>

						<input type="checkbox" class="checkbox" id="checkall<?php echo $client['Client']['program_id'] ?>" onclick="selectAll('<?php echo $client['Client']['program_id'] ?>');"><span style="color:red"><b>Select All Clients</b></span><br>
						<?php }?>
						<input type="checkbox" name="data[Client][Client][]" value="<?php echo $client['Client']['id'];?>" <?php if(in_array($client['Client']['id'], $clients)) : echo $check; endif;?> class="checkbox"/>
						&nbsp;&nbsp;<?php echo $client['Client']['name'] ?><br>
						<?php $pid = $client['Client']['program_id'];
						endforeach?>
						</div>
				</td>
			</tr>
		</table>
	</td>
</tr>
</table>
