<style type="text/css">
#content
{
	border-left:1px solid #cccccc;
	border-right:1px solid #cccccc;
	border-bottom:1px solid #cccccc;
	margin-left:20px !important;
	padding-left:18px !important;
	width:913px;
}
.rounded-corners 
{
	-moz-border-radius: 10px 10px 0px 0px; /* Firefox */
	-webkit-border-radius: 10px 10px 0px 0px; /* Safari, Chrome */
	border-radius: 10px 10px 0px 0px; /* CSS3 */
}
.tr 
{
	height:45px;
}
.tr1 
{
	height:35px;
}
.admin_button
{
	height: 26px;
	padding-bottom: 2px;
	border-radius: 5px 5px 5px 5px;
	background-color: #0066cc;
	border: 1px solid #cccccc;
	color: #FFFFFF; 
	cursor: pointer;
	font-size:13px;
}
</style>
<script>
function update_order_status(id)
	{
	    var status= document.getElementById("status"+id).value;
		var url = '/carts/update_order_status/'+id+'/'+status;
		new Ajax.Request(url, {
			method: 'get',
			onComplete:function(transport) {
				window.location.reload();
			}
		});
	}
function update_shipping_status(id,productid)
{
   // var modify= document.getElementById("modify"+id).value;
	var url = '/carts/update_shipping_status/'+id+'/'+productid+'/';
	new Ajax.Request(url, {
			method: 'get',
			onComplete:function(transport) {
				window.location.reload();
			}
		});
}
</script>

<table style="margin-left:-2px;margin-top: 10px;">
	<tr>
		<td>
           <img style="width:40px;height:40px;" src="/files/styles/icon_images/shop.png""  alt="Shopping Cart" title="Shopping Cart" />&nbsp;&nbsp;
        </td>
        <td> 
            <h2><?php echo "Send A Friend";?></h2>
        </td>
    </tr>        
</table>
				
<div class="tabber" id="mytab1">
		<div class="tabbertab" style="min-height:210px;overflow:hidden">
	  			<h2>Products List</h2>
	    		<h4>All Products</h4>	
    			<div style="background-color:#EFEFEF;border:1px solid #cccccc;width:768px;min-height:40px;overflow:hidden;padding:20px">	 
					<div>
						<div class="actions" style="padding-bottom:20px;padding-top:10px">
							<table style="width:760px;background-color:#cccccc;height:75px;border:1px solid #969090;">
								<tr>
									<td>
										<form action="<?php echo $this->Html->url('/admin/saf/'); ?>" method="get" id="SearchForm">
											<table>
												<tr>
													<td style="padding-left:10px;"><strong>Find Products</strong></td>
													<td>
														<?php echo $this->Form->input('Product.search', array('div'=>false, 'label'=>false)); ?>
														<script type="text/javascript">
														$(function() {
															if ($('#ProductSearch').val() == '') {
																$('#ProductSearch').val('ID or name');
																$('#ProductSearch').css( "color", "#aaaaaa" );
															}
															$( "#ProductSearch" ).focus(function() {
																if ($(this).val() == 'ID or name') {
																	$(this).val('');
																	$(this).css( "color", "#000" );
																}
															});
															$( "#SearchForm" ).submit(function() {
																if ($("#ProductSearch").val() == 'ID or name') {
																	$("#ProductSearch").val('');
																	$("#ProductSearch").css( "color", "#000" );
																}
															});
														});
														</script>
													</td>
													<td><button class="admin_button">Search</button></td>
												</tr>
											</table>
										</form>
									</td>
									<td>
										<button class ="admin_button" onclick="location.href='<?php echo $this->Html->url('edit'); ?>';">Add New Product</button>
									</td>
								</tr>
							</table>
						</div>
					</div>
					
					<div style="background-color:#EFEFEF;min-height:40px;overflow:hidden;">	 
					<div class="list" style="width: 99%;">
						<table cellpadding="0" cellspacing="0" width="100%">
							<tr height="30">
								<th style="width:100px;background-color:#cccccc;text-align:left;padding-left:5px;border-left:1px solid #969090;border-bottom:1px solid #969090;border-top:1px solid #969090;">Product id</th>
								<th style="width:550px;background-color:#cccccc;text-align:left;padding-left:5px;border-bottom:1px solid #969090;border-top:1px solid #969090;">Product Name</th>
								<th class="actions" style="background-color:#cccccc;text-align:left;padding-left:4px;border-right:1px solid #969090;border-bottom:1px solid #969090;border-top:1px solid #969090;">Actions</th>
							</tr>
							<?php if (empty($searchproducts)) { ?>
							<?php
							$i = 1;
							foreach ($products as $product):
								$class = null;
								if ($i++ % 2 == 0) {
									$class = 'style="background-color: white;"';
								}
							?>
							<tr <?php echo $class;?>>
								<td>
									<?php echo $product['Product']['id'] ?>
								</td>
								<td>
									<?php echo $product['Product']['name'] ?>
								</td>
								<td class="actions">
									<?php echo $this->Html->link(__('Edit', true), array('action'=>'edit', $product['ProductsSaf']['id'])); ?> |
									<?php echo $this->Html->link(__('Delete', true), array('action'=>'delete', $product['ProductsSaf']['id']), null, sprintf(__('Are you sure you want to delete product \'%s\'?', true), $product['Product']['name'])); ?>
								</td>
							</tr>
						<?php endforeach; ?>
						<?php } else {
							foreach($searchproducts as $searchproduct){ ?>
							<tr>
								<td>
									<?php echo $searchproduct['Product']['id'] ?>
								</td>
								<td>
									<?php echo $searchproduct['Product']['name'] ?>
								</td>
								<td class="actions">
									<?php echo $this->Html->link(__('Edit', true), array('action'=>'edit', $searchproduct['ProductsSaf']['id'])); ?> |
									<?php echo $this->Html->link(__('Delete', true), array('action'=>'delete', $searchproduct['ProductsSaf']['id']), null, sprintf(__('Are you sure you want to delete product \'%s\'?', true), $searchproduct['Product']['name'])); ?>
								</td>
							</tr>
						<?php }} ?>
						</table>
						
						<!-- pagination -->
						<div class="paging" style="padding-top:7px;float:right">
						<table style="border:none !important;">
							<tr>
								<td style="border:none !important;" valign="top">
									<?php echo $this->Paginator->first('<<', array('title'=>'first','class'=>'disabled cpagination1'));?>
									<?php echo $this->Paginator->prev('<', array('title'=>'prev','class'=>'disabled cpagination1'));?>
									<?php echo $this->Paginator->numbers(array('separator' => ' ','class'=>'cpagination1'));?>
									<?php echo $this->Paginator->next('>', array('title'=>'next','class'=>'disabled cpagination1'));?>
									<?php echo $this->Paginator->last('>>', array('title'=>'last','class'=>'disabled cpagination1'));?>
								</td>
								<td style="border:none !important;padding-top:3px;">
								<select name="page" id="selectpage" style="margin-top:-5px;">
									<?php for ($i = 1; $i <= $this->Paginator->counter(array('format' => '%pages%')); $i++):?>
									<option value="<?php echo $i;?>"<?php echo ($this->Paginator->current($params['page']) == $i) ? ' selected="selected"' : ''?>><?php echo $i ?></option>
									<?php endfor ?>
								</select>
								<script>
								$("#selectpage").change(function() {
									var url = window.location.href;
									var n = url.indexOf("/?");
									var extra = "";
									if(n>0){extra = url.substring(n,url.length);}
									window.location.href = '/admin/saf/index/page:'+$(this).val()+extra;
								});
								</script>
								</td>
							</tr>
						</table>
					</div>
	  
						</div>
					</div>
			
				</div>
		</div>
		
		</div>
	    		
		</div>
</div>