<?php
$result = array();
foreach($products as $product) :
	$key = $product['Product']['id'];
	array_push($result, array("id"=>$key, "label"=>strip_tags($key.' - '.$product['Product']['name']), "value"=>$key));
endforeach;
echo json_encode($result);?>