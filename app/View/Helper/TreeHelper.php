<?php
class TreeHelper extends AppHelper
{
	var $helpers = array('Html');
	var $tab = "  ";

	var $child_count = 0;
	var $child_limit = false;
	var $link_field = 'id';
	var $link_model;
	var $bullet = '';
	var $menu_path = array();
	var $current_page = false;
	var $current_page_id = false;

	/**
	 * Generate a bunch of nested li ul tags
	 *
	 * @param	str		$name	name of Model
	 * @param	array	$data	Array of data for the list
	 * @param	str		$extra	string of options for the ul tags
	 * @param	str		$url		url to prepend to each link
	 * @param	int		$child_limit	number of levels to drill down
	 * @param	str		$link_field	column data to use for the link
	*/
	function show($name, $data, $extra = '', $url = '', $child_limit=false, $link_field=false, $bullet='', $disabled_field = null, $disabled_value = null) {

		$this->child_limit = $child_limit;

		$this->bullet = $bullet;

		list($modelName, $fieldName) = explode('.', $name);

		if ($link_field) {
			list($this->link_model, $this->link_field) = explode('.', $link_field);
		} else {
			$this->link_model = $modelName;
		}

		$output = $this->_list_element($data, $modelName, $fieldName, 0, $extra, $url, $disabled_field, $disabled_value);

		return $this->output($output);
	}

	/**
	 * Create the navigation menu
	*/
	function show_menu($client,$dashId) {

		$this->menu_pages = $client['Page'];
		$check_id = $client['Client']['id'];
		$this->_flatten_menu($this->menu_pages);
		if ($this->current_page_id === false) {
			$this->current_page_id = $this->menu_pages[key($this->menu_pages)]['Page']['id'];
		}

		$pages = $this->_menu_pages();

		foreach($pages as $key=>$page)
		{
			if(empty($page['ClientPage']['id']))
			{
				unset($pages[$key]);
			}
		}

		if($dashId==2)
		{
			sort($pages);
			$firstData = array();
			$tdata = array();
			foreach($pages as $key=>$val)
			{
				if($key == 0)
					array_push($firstData, $val);
				else
					array_push($tdata,$val);
			}
			$pages=null;
			$pages= array_merge($tdata,$firstData);
		}

		if ($check_id == '965') {
			return $this->output($this->_list_restrict_menu($pages));
		} else if ($check_id == '968') {
			return $this->output($this->_list_restrict_menu_public($pages));
		} else {
			return $this->output($this->_list_menu($pages));
		}
	}

	function _flatten_menu($pages) {
		$pages = $this->request->pages;
		if($pages!=null){
		foreach ($pages as $key => $page) {
			$this->pages[$page['Page']['id']] = $page;

			if (strpos('/'. $_GET['url'], $page['Page']['name']) !== false) {
				$this->current_page_id = $page['Page']['id'];

			}

			if (isset($page['children'][0])) {
				$this->_flatten_menu($page['children']);
			}
		}}


	}

	/**
	 * Return an array of allowed pages
	 *
	 * @param	array	$page_names	array of page names to check for
	 * @param	array	$pages 	array of pages (usually from the session)
	*/
	function allowed_pages($page_names, $pages) {

		$allowed_pages = array();
		foreach ($pages as $key => $page) {

			if (in_array($page['Page']['name'], $page_names)) {
				$allowed_pages[$page['Page']['name']] = $page;
			}

			if (!empty($page['children'])) {
				$allowed_child_pages = $this->allowed_pages($page_names, $page['children']);
				$allowed_pages = array_merge($allowed_pages, $allowed_child_pages);
			}
		}

		$return_pages = array();
		//	reorder as for the page_names
		foreach ($page_names as $page_name) {
			if (isset($allowed_pages[$page_name])) {
				$return_pages[$page_name] = $allowed_pages[$page_name];
			}
		}

		return $return_pages;

	}
	function _menu_pages() {

		$current_page = $this->pages[$this->current_page_id];
		$allowed_pages = array();

		//	get the path of allowed pages
		$parent_id = $current_page['Page']['parent_id'];
		while ($parent_id != 0) {

			$allowed_pages[] =& $this->pages[$parent_id]['Page']['id'];

			//	add  pages siblings to the allowed path
			if ($this->pages[$parent_id]['Page']['id'] != 0) {
				$parent_page = $this->pages[$parent_id];
				foreach ($parent_page['children'] as $key => $page) {
					$allowed_pages[] = $page['Page']['id'];
				}
			}

			$parent_id = $this->pages[$parent_id]['Page']['parent_id'];

		}

		//	add this pages children to the allowed path
		if(!empty($current_page['children'])){
		foreach ($current_page['children'] as $key => $page) {
			$allowed_pages[] = $page['Page']['id'];
		}}

		$pages = $this->_remove_unused($this->menu_pages, $allowed_pages);

		return $pages;
	}

	function _remove_unused($pages, $allowed_pages) {

		foreach ($pages as $key => $page) {

			if($page['Page']['parent_id'] != '0' && !in_array($page['Page']['id'], $allowed_pages)) {
				//unset($pages[$key]);
			} else {
				$pages[$key]['children'] = $this->_remove_unused($page['children'], $allowed_pages);
			}
		}

		return $pages;

	}

	function _list_menu($data,$dashId='') {

		$output = '<ul id="menu">';
		if($dashId == 2)
				sort($data);
		foreach ($data as $key=>$val) {
			$text = $this->_generate_link($val['ClientPage']['title'], $val['Page']['name']);
			if ($val['Page']['parent_id'] == '0') {
				$output .= '<li>'. $text;
			} else {
				$output .= '<li>'. $text;
			}

			if(!empty($val['children'])) {
				$output .= $this->_list_menu($val['children']);
			}
			$output .= '</li>';
		}
		$output .= '</ul>';

		return $output;
	}

	function _list_restrict_menu($data) {

			$output = '<ul id="menu">';
			foreach ($data as $key=>$val) {
				$text = $this->_generate_link($val['ClientPage']['title'], $val['Page']['name']);
				if ($val['ClientPage']['title'] != 'My Membership') {
					if ($val['Page']['parent_id'] == '0') {
						$output .= '<li>'. $text;
					} else {
						$output .= '<li>'. $text;
					}

					if(!empty($val['children'])) {
						$output .=  $this->_list_menu($val['children']);
					}
					$output .= '</li>';
				}
			}
			$output .= '</ul>';

			return $output;

		}

	/*function _list_restrict_menu_public($data) {

			$output = '<ul>';
			foreach ($data as $key=>$val) {
				$text = $this->_generate_link($val['ClientPage']['title'], $val['Page']['name']);
				// if ($val['ClientPage']['title'] != 'About us' || $val['ClientPage']['title'] != 'FAQ' || $val['ClientPage']['title'] != 'Become a Supplier') {
				if (!stristr($val['ClientPage']['title'],"Home") && !stristr($val['ClientPage']['title'],"About") && !stristr($val['ClientPage']['title'],"FAQ") && !stristr($val['ClientPage']['title'],"Become")){
					if ($val['Page']['parent_id'] == '0') {
						$output .= '<li class="root"><div class="root">'. $text. '</div>';
					} else {
						$output .= '<li>'. $text;
					}

					if(!empty($val['children'])) {
						$output .= '<div class="children">'. $this->_list_menu($val['children']). '</div>'	;
					}
					$output .= '</li>';
				}
			}
			$output .= '</ul>';

			return $output;

		}*/
	function _list_restrict_menu_public($data) {

			$output = '<ul id="menu">';
			foreach ($data as $key=>$val) {
				$text = $this->_generate_link($val['ClientPage']['title'], $val['Page']['name']);
				// if ($val['ClientPage']['title'] != 'About us' || $val['ClientPage']['title'] != 'FAQ' || $val['ClientPage']['title'] != 'Become a Supplier') {
				if (!stristr($val['ClientPage']['title'],"Home") && !stristr($val['ClientPage']['title'],"About") && !stristr($val['ClientPage']['title'],"FAQ") && !stristr($val['ClientPage']['title'],"Become")){
					if ($val['Page']['parent_id'] == '0') {
						$output .= '<li>'. $text;
					} else {
						$output .= '<li>'. $text;
					}

					if(!empty($val['children'])) {
						$output .= $this->_list_menu($val['children']);
					}
					$output .= '</li>';
				}
			}
			$output .= '</ul>';

			return $output;

		}

	function _list_element($data, $modelName, $fieldName, $level, $extra = '', $url = false, $disabledField = null, $disabledValue = null) {

		if ($this->child_limit !== false && ($this->child_count > $this->child_limit)) {
			return;
		}

		$this->child_count++;

		$tabs = "\n" . str_repeat($this->tab, $level * 2);
		$li_tabs = $tabs . $this->tab;

		$output = $tabs. '<ul' . $extra . '>';

		foreach ($data as $key=>$val) {
			if($url === false) {
				$text = $val[$modelName][$fieldName];
			} else {

				$class = null;
				if (isset($val[$modelName][$disabledField])) {
					if (is_array($disabledValue)) {
						if  (in_array($val[$modelName][$disabledField], $disabledValue)) {
							$class = 'disabled';

						}

					} else {
						if  ($val[$modelName][$disabledField] == $disabledValue) {
							$class = 'disabled';
						}
					}
				}
				$text = $this->_generate_link1($val[$modelName][$fieldName], $url. $val[$this->link_model][$this->link_field], $this->bullet, $class);

			}

			if($val[$modelName][$fieldName]!='Feedback')
				$output .= $li_tabs . '<li>' . $text;
			else
				$output .= $li_tabs . '<li>' ;
			if(isset($val['children'][0])) {
				$output .= $this->_list_element($val['children'], $modelName, $fieldName, $level+1, '', $url,$disabledField, $disabledValue);
				$output .= $li_tabs . '</li>';
			} else {
				$output .= '</li>';
			}
		}
		$output .= $tabs . '</ul>';

		return $output;
	}
	function _generate_link1($name, $url, $bullet = null, $class = null) {

		if (!empty($class)) {
			$class = 'class="'. $class. '"';
		}

		$text = $bullet. '<a href="' . $this->Html->url($url ) . '" '. $class. '>' . $name . '</a>';

		return $text;
	}


	function _generate_link($name, $url, $bullet = null, $class = null) {
		if (!empty($class)) {
			$class = 'class="'. $class. '"';
		}
		if($name=='Change Details')
			$text = $bullet. '<a href="javascript:void(0)" onclick="editDetails()">'.$name.'</a>';
		elseif($name=='Contact Us')
			$text = $bullet. '<a href="javascript:void(0)" onclick="contactUs()">'.$name.'</a>';
		elseif($name=='Feedback')
			$text = $bullet. '<a href="javascript:void(0)" onclick="feedback()">'.$name.'</a>';
		else
			$text = $bullet. '<a href="' . $this->Html->url($url ) . '" '. $class. '>' . $name . '</a>';
		return $text;
	}
	function _list_menu_header($data)
	{

		$output = '<ul id="menu">';
		foreach ($data as $key=>$val) {
			$text = $this->_generate_link($val['ClientPage']['title'], $val['Page']['name']);
			if ($val['Page']['parent_id'] == '0') {
				$output .= '<li class="root"><div class="root">'. $text. '</div>';
			} else {
				$output .= '<li>'. $text;
			}

			if(!empty($val['children'])) {
				$output .= '<div class="children">'. $this->_list_menu($val['children']). '</div>'	;
			}
			$output .= '</li>';
		}
		$output .= '</ul>';

		return $output;
	}

	/**
	 * Create the navigation menu for footer
	*/
	function show_footer_menu($client, $dashId)
	{

		$this->menu_pages = $client['Page'];
		$check_id = $client['Client']['id'];
		$this->_flatten_menu($this->menu_pages);
		if ($this->current_page_id === false) {
			$this->current_page_id = $this->menu_pages[key($this->menu_pages)]['Page']['id'];
		}

		$pages = $this->_menu_pages();

		foreach($pages as $key=>$page)
		{
			if(empty($page['ClientPage']['id']))
			{
				unset($pages[$key]);
			}
		}

		if($dashId==2)
		{
			sort($pages);
			$firstData = array();
			$tdata = array();
			foreach($pages as $key=>$val)
			{
				if($key == 0)
					array_push($firstData, $val);
				else
					array_push($tdata,$val);
			}
			$pages=null;
			$pages= array_merge($tdata,$firstData);
		}

		if ($check_id == '965') {
			return $this->output($this->_list_restrict_footer_menu($pages));
		} else if ($check_id == '968') {
			return $this->output($this->_list_restrict_footer_menu_public($pages));
		} else {
			return $this->output($this->_list_footer_menu($pages));
		}
	}

	function _list_footer_menu($data) {

		$output = '<ul id="menuf">';
		foreach ($data as $key=>$val) {
			$text = $this->_generate_link_footer($val['ClientPage']['title'], $val['Page']['name']);
			if ($val['Page']['parent_id'] == '0') {
				$output .= '<li>'. $text;
			} else {
				$output .= '<li>'. $text;
			}

			if(!empty($val['children'])) {
				$output .= $this->_list_footer_menu_sub($val['children']);
			}
			$output .= '</li>';
		}
		$output .= '</ul>';

		return $output;
	}

	function _list_footer_menu_sub($data) {

		$output = '<ul id="menuf">';
		foreach ($data as $key=>$val) {
			$text = $this->_generate_link($val['ClientPage']['title'], $val['Page']['name']);
			if ($val['Page']['parent_id'] == '0') {
				$output .= '<li>'. $text;
			} else {
				$output .= '<li>'. $text;
			}

			$output .= '</li>';
		}
		$output .= '</ul>';

		return $output;
	}

	function _list_restrict_footer_menu($data)
	{
		$output = '<ul id="menuf">';
		foreach ($data as $key=>$val) {
			$text = $this->_generate_link_footer($val['ClientPage']['title'], $val['Page']['name']);
			if ($val['ClientPage']['title'] != 'My Membership') {
				if ($val['Page']['parent_id'] == '0') {
					$output .= '<li>'. $text;
				} else {
					$output .= '<li>'. $text;
				}

				if(!empty($val['children'])) {
					$output .=  $this->_list_footer_menu_sub($val['children']);
				}
				$output .= '</li>';
			}
		}
		$output .= '</ul>';
		return $output;
	}

	function _list_restrict_footer_menu_public($data)
	{
		$output = '<ul id="menuf">';
		foreach ($data as $key=>$val) {
			$text = $this->_generate_link_footer($val['ClientPage']['title'], $val['Page']['name']);
			// if ($val['ClientPage']['title'] != 'About us' || $val['ClientPage']['title'] != 'FAQ' || $val['ClientPage']['title'] != 'Become a Supplier') {
			if (!stristr($val['ClientPage']['title'],"Home") && !stristr($val['ClientPage']['title'],"About") && !stristr($val['ClientPage']['title'],"FAQ") && !stristr($val['ClientPage']['title'],"Become")){
				if ($val['Page']['parent_id'] == '0') {
					$output .= '<li>'. $text;
				} else {
					$output .= '<li>'. $text;
				}

				if(!empty($val['children'])) {
					$output .= $this->_list_footer_menu_sub($val['children']);
				}
				$output .= '</li>';
			}
		}
		$output .= '</ul>';

		return $output;
	}

	function _generate_link_footer($name, $url, $bullet = null, $class = null) {
		if (!empty($class)) {
			$class = 'class="'. $class. '"';
		}
		if($name=='Change Details')
			$text = $bullet. '<a href="javascript:void(0)" onclick="editDetails()">'.$name.'</a>';
		elseif($name=='Contact Us')
			$text = $bullet. '<a href="javascript:void(0)" onclick="contactUs()">'.$name.'</a>';
		elseif($name=='Feedback')
			$text = $bullet. '<a href="javascript:void(0)" onclick="feedback()">'.$name.'</a>';
		else
			$text = $bullet. '<a href="' . $this->Html->url($url ) . '" '. $class. '><b>' . strtoupper($name) . '</b></a>';
		return $text;
	}

	function showPages($name, $data, $extra = '', $url = '', $child_limit=false, $link_field=false, $dashbrd_id, $bullet='', $disabled_field = null, $disabled_value = null) {

			$this->child_limit = $child_limit;

			$this->bullet = $bullet;

			list($modelName, $fieldName) = explode('.', $name);

			if ($link_field) {
				list($this->link_model, $this->link_field) = explode('.', $link_field);
			} else {
				$this->link_model = $modelName;
			}

			$output = $this->_list_element_pages($data, $modelName, $fieldName, 0, $extra, $url, $disabled_field, $disabled_value, $dashbrd_id);

			return $this->output($output);
	}

	function _list_element_pages($data, $modelName, $fieldName, $level, $extra = '', $url = false, $disabledField = null, $disabledValue = null, $dashbrd_id) {

			if ($this->child_limit !== false && ($this->child_count > $this->child_limit)) {
				return;
			}

			$this->child_count++;

			$tabs = "\n" . str_repeat($this->tab, $level * 2);
			$li_tabs = $tabs . $this->tab;

			$output = $tabs. '<ul' . $extra . ' style=\'width:230px;float:left;border-right:1px solid #dfdfdf;padding-right:20px;\'>';
			$i = 0;
			foreach ($data as $key=>$val) {$i++;
				if($url === false) {
					$text = $val[$modelName][$fieldName];
				} else {

					$class = null;
					if (isset($val[$modelName][$disabledField])) {
						if (is_array($disabledValue)) {
							if  (in_array($val[$modelName][$disabledField], $disabledValue)) {
								$class = 'disabled';

							}

						} else {
							if  ($val[$modelName][$disabledField] == $disabledValue) {
								$class = 'disabled';
							}
						}
					}
					$text = $this->_generate_link_main($val[$modelName][$fieldName], $url. $val[$this->link_model][$this->link_field].'/'.$dashbrd_id, $this->bullet, $class);

				}
				if($i>(39%(sizeof($data)))){$output .= '</ul><ul style=\'width:245px;float:left;padding-left:12px;\'>';$i=0;}
				if($val[$modelName][$fieldName]!='Feedback')
					$output .= $li_tabs . '<li>' . $text;
				else
					$output .= $li_tabs . '<li>' ;
				if(isset($val['children'][0])) {
					$output .= $this->_list_element_sub($val['children'], $modelName, $fieldName, $level+1, '', $url,$disabledField, $disabledValue,$dashbrd_id);
					$output .= $li_tabs . '</li>';
				} else {
					$output .= '</li>';
				}
			}
			$output .= $tabs . '</ul>';

			return $output;
	}

	function _list_element_sub($data, $modelName, $fieldName, $level, $extra = '', $url = false, $disabledField = null, $disabledValue = null, $dashbrd_id) {

			$tabs = "\n" . str_repeat($this->tab, $level * 2);
			$li_tabs = $tabs . $this->tab;

			$output = $tabs. '<ul' . $extra . '>';
			foreach ($data as $key=>$val) {

					$text = $this->_generate_link_sub($val[$modelName][$fieldName], $url. $val[$this->link_model][$this->link_field].'/'.$dashbrd_id, $this->bullet, $class);

					$output .= $li_tabs . '<li>' ;
					$output .= $text;
					$output .= $li_tabs . '</li>';


			}
			$output .= $tabs . '</ul>';

			return $output;
		}

		function _generate_link_main($name, $url, $bullet = null, $class = null) {

			if (!empty($class)) {
				$class = 'class="'. $class. '"';
			}

			if($name == 'More Info')
				$name = 'Map Replacement';

			$text = $bullet. '<a style="color:red !important;" href="' . $this->Html->url($url ) . '" '. $class. '>' . $name . '</a>';

			return $text;
		}
		function _generate_link_sub($name, $url, $bullet = null, $class = null) {

			if (!empty($class)) {
				$class = 'class="'. $class. '"';
			}

			$text = $bullet. '<a style="color:#0066ff !important;padding-left:10px;" href="' . $this->Html->url($url ) . '" '. $class. '>' . $name . '</a>';

			return $text;
		}



}
?>
