<?php

/*
 * CakeMap -- a google maps integrated application built on CakePHP framework.
 * Copyright (c) 2005 Garrett J. Woodworth : gwoo@rd11.com
 * rd11,inc : http://rd11.com
 *
 * @author      gwoo <gwoo@rd11.com>
 * @version     0.10.1311_pre_beta
 * @license     OPPL
 *
 * Modified by     Mahmoud Lababidi <lababidi@bearsontherun.com>
 * Date        Dec 16, 2006
 *
 * Modified by  Stefano Manfredini <info@stefanomanfredini.info>
 * Date        Jul 1, 2008
 */
class GoogleMapHelper extends Helper {

	var $errors = array();

	// var $key = "http://www.google.com/jsapi?key=ABQIAAAAIAmaQuGhV73yKqAF7EdINBSw-rZd5qlMzzza300rtu9pH9MKrxRo7f0kHRWd6w9S5sBRXnGrvcj5NQ";
	//var $key = "http://maps.google.com/maps/api/js?sensor=true";
	var $key ="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=true";

	function map($default, $style = 'width: 400px; height: 400px' )
	{
		$dlat=$default['lat'];
		$dlong=$default['long'];
		$out = "<div style=\"border:2px solid #cccccc;height:275px;width:350px;padding-left:1px\">";
		$out .= "<div style=\"border:2px solid #ffffff;height:270px;width:345px;\">";
		$out .= "<div id=\"gmap\" ";
		$out .= isset($style) ? "style=\"".$style."\"" : null;
		$out .= " ></div></div></div>";
		$out .= "<script>
		function initialize() {
			var latlng = new google.maps.LatLng($dlat,$dlong);
			var myOptions = {
				zoom: 3,
				center: latlng
			}

			var map = new google.maps.Map(document.getElementById(\"gmap\"), myOptions);
			var marker = new google.maps.Marker({position: latlng, map: map, title: 'My Location'});
		}
		google.maps.event.addDomListener(window, 'load', initialize);
		</script>";
		return $out;
	}

    function addMarkers(&$data, $icon=null)
    {
        ////<![CDATA[
        $out = "<script type=\"text/javascript\">

            if (GBrowserIsCompatible())
            {
            ";

            if(is_array($data))
            {
                if($icon)
                {
                    $out .= $icon;
                }
                else
                {
                    $out .= 'var icon = new GIcon();

                        icon.image = "http://labs.google.com/ridefinder/images/mm_20_red.png";
                        icon.shadow = "http://labs.google.com/ridefinder/images/mm_20_shadow.png";
                        icon.iconSize = new GSize(12, 20);
                        icon.shadowSize = new GSize(22, 20);
                        icon.iconAnchor = new GPoint(6, 20);
                        icon.infoWindowAnchor = new GPoint(5, 1);
                    ';

                }
                $i = 0;
                foreach($data as $n=>$m){
                    $keys = array_keys($m);
                    $point = $m[$keys[0]];
                    if(!preg_match('/[^0-9\\.\\-]+/',$point['longitude']) && preg_match('/^[-]?(?:180|(?:1[0-7]\\d)|(?:\\d?\\d))[.]{1,1}[0-9]{0,15}/',$point['longitude'])
                        && !preg_match('/[^0-9\\.\\-]+/',$point['latitude']) && preg_match('/^[-]?(?:180|(?:1[0-7]\\d)|(?:\\d?\\d))[.]{1,1}[0-9]{0,15}/',$point['latitude']))
                    {
                        $out .= "
                            window.point".$i." = new GPoint(".$point['longitude'].",".$point['latitude'].");
                            window.marker".$i." = new GMarker(window.point".$i.",icon);
                            window.map.addOverlay(window.marker".$i.");
                            window.marker$i.html = \"$point[title]$point[html]\";
                            GEvent.addListener(window.marker".$i.", \"click\",
                            function() {
                                window.marker$i.openInfoWindowHtml(window.marker$i.html);
                            });";
                        $data[$n][$keys[0]]['js']="window.marker$i.openInfoWindowHtml(window.marker$i.html);";
                        $i++;
                    }
                }
            }
        $out .=    "}

            </script>";
        return $out;
        //]]>
    }


    function addClick($var, $script=null)
    {
        //
        $out = "<script type=\"text/javascript\">
            <![CDATA[

            if (GBrowserIsCompatible())
            {
            "
            .$script
            .'GEvent.addListener(window.map, "click", '.$var.', true);'
            ."}
                //]]>
            </script>";
        return $out;
    }

    function addMarkerOnClick($innerHtml = null)
    {
        $mapClick = '
            var mapClick = function (overlay, point) {
                window.point = new GPoint(point.x,point.y);
                window.marker = new GMarker(window.point,icon);
                window.map.addOverlay(window.marker)
                GEvent.addListener(window.marker, "click",
                function() {
                    window.marker.openInfoWindowHtml('.$innerHtml.');
                });
            }
        ';
        return $this->addClick('mapClick', $mapClick);

    }

	function map_and_markers ($data, $default, $style = 'width: 400px; height: 400px', $smallmap=false, $icon=null)
	{
		$dlat=$default['lat'];
		$dlong=$default['long'];
		$out = "<div style=\"border:2px solid #cccccc;height:275px;width:350px;padding-left:1px\">";
		$out .= "<div style=\"border:2px solid #ffffff;height:270px;width:345px;\">";
		$out .= "<div id=\"map\"";
		$out .= isset($style) ? "style=\"".$style."\"" : null;
        $out .= " ></div></div></div>";
        $out .= "

        <script type=\"text/javascript\">
         window.latlng = new google.maps.LatLng($dlat,$dlong);
    	 window.myOptions = {
     		zoom: 3,
      		center: latlng,
      		mapTypeControl: false,
			streetViewControl: false,
      		mapTypeId: google.maps.MapTypeId.ROADMAP
    	};

    	window.map = new google.maps.Map(document.getElementById(\"map\"), window.myOptions);
    	   ";

      //  $out .= 'var image="http://www.google.com/mapfiles/marker.png";';

      $out .= ' var image = new google.maps.MarkerImage("http://www.google.com/mapfiles/marker.png", null, null, new google.maps.Point(0, 32), new google.maps.Size(14, 24));
      ';
	   $dcontains=false;
      $out.='window.latlangbounds=new google.maps.LatLngBounds();';
	  $out.='var infowindow = new google.maps.InfoWindow();';
        foreach($data as $n=>$m)
        {
        	$keys = array_keys($m);
            $point = $m[$keys[0]];
            $dcontains=true;
            $out.='var latlng1 = new google.maps.LatLng('.$point['latitude'].','.$point['longitude'].');

  			var beachMarker = new google.maps.Marker({
     			position: latlng1,
      			map: window.map,
      			icon: image
			});

			attachSecretMessage(beachMarker,"'.$point['html'].'");';
            $out.='window.latlangbounds.extend(latlng1);';
		    $out.='function attachSecretMessage(beachMarker, message) {

  				google.maps.event.addListener(beachMarker, "click", function(e) {
    				infowindow.setContent(message);
    				infowindow.open(window.map, this);
  				});
        	}';
        }

              if($dcontains==true)
            $out.='window.map.fitBounds(window.latlangbounds);';

            $out.='if(window.map.getZoom()>=14)window.map.setZoom(12);';
           // $out .='window.map.setCenter(latlng);';
            $out .= "</script>";
		return $out;
	}
}
?>
