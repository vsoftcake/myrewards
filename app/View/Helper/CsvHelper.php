<?php 
 
class CsvHelper extends Helper {
	
	var $delimiter = ',';
	var $enclosure = '"';
	var $line_break = "\r\n";
	var $filename = 'export.csv';
	var $line = array();
	var $row = '';
	var $buffer;
	
	function CsvHelper() {
		$this->clear();
	}
	
	function clear() {
		$this->line = array();
		$this->row = '';
	}
	
	function addField($value) {
		$this->line[] = $value;
	}
	
	function endRow() {
		$this->addRow($this->line);
		$this->line = array();
	}
	
	function addRow($row) {
		foreach ($row as $key => $value) {
			$row[$key] = str_replace('"', '""', $value);
		}
		$this->row .= $this->enclosure. implode($this->enclosure. $this->delimiter. $this->enclosure, $row). $this->enclosure. $this->line_break;
	}
	
	function renderHeaders() { 
		header("Content-type: text/csv");
		header('Content-Disposition: attachment; filename="'. $this->filename);
	}
	
	function setFilename($filename) {
		$this->filename = $filename;
		if (strtolower(substr($this->filename, -4)) != '.csv') {
			$this->filename .= '.csv';
		}
	}
	
	function render($outputHeaders = true, $to_encoding = null, $from_encoding = "auto") {
		if ($outputHeaders) {
			if (is_string($outputHeaders)) {
				$this->setFilename($outputHeaders);
			}
			$this->renderHeaders();
		}
		
		if ($to_encoding) {
			$this->row = mb_convert_encoding($this->row, $to_encoding, $from_encoding);
		}
		return $this->output($this->row);
	}
	
	######################################################################################################################
	/*
	 * backup_render function to download data to server as backup as per products created dates.
	 */
	function backup_prod_created_render($outputHeaders = true, $to_encoding = null, $from_encoding = "auto") {
		if ($outputHeaders) {
			if (is_string($outputHeaders)) {
				$this->setFilename($outputHeaders);
			}
			$this->backup_prod_ctd_renderHeaders();
		}
		
		if ($to_encoding) {
			$this->row = mb_convert_encoding($this->row, $to_encoding, $from_encoding);
		}
		return $this->output($this->row);
	}
	
	function backup_prod_ctd_renderHeaders() {
		file_put_contents("/var/www/html/mr2_new/app/webroot/".$this->filename."_created_".date("Y-m-d").".csv", $this->output($this->row));
		//file_put_contents("D:/".$this->filename."_".date("Y-m-d").".csv", $this->output($this->row));
	}
	
	/*
	 * backup_render function to download data to server as backup as per products modified dates.
	 */
	function backup_prod_modified_render($outputHeaders = true, $to_encoding = null, $from_encoding = "auto") {
		if ($outputHeaders) {
			if (is_string($outputHeaders)) {
				$this->setFilename($outputHeaders);
			}
			$this->backup_prod_mdfd_renderHeaders();
		}
		
		if ($to_encoding) {
			$this->row = mb_convert_encoding($this->row, $to_encoding, $from_encoding);
		}
		return $this->output($this->row);
	}
	
	function backup_prod_mdfd_renderHeaders() {
		file_put_contents("/var/www/html/mr2_new/app/webroot/".$this->filename."_modified_".date("Y-m-d").".csv", $this->output($this->row));
		//file_put_contents("D:/".$this->filename."_".date("Y-m-d").".csv", $this->output($this->row));
	}
	
	######################################################################################################################
	
	/*
	 * backup_render function to download data to server as backup as per merchants created dates.
	 */
	function backup_merchant_created_render($outputHeaders = true, $to_encoding = null, $from_encoding = "auto") {
		if ($outputHeaders) {
			if (is_string($outputHeaders)) {
				$this->setFilename($outputHeaders);
			}
			$this->backup_mrct_ctd_renderHeaders();
		}
		
		if ($to_encoding) {
			$this->row = mb_convert_encoding($this->row, $to_encoding, $from_encoding);
		}
		return $this->output($this->row);
	}
	
	function backup_mrct_ctd_renderHeaders() {
		file_put_contents("/var/www/html/mr2_new/app/webroot/".$this->filename."_created_".date("Y-m-d").".csv", $this->output($this->row));
		//file_put_contents("D:/".$this->filename."_".date("Y-m-d").".csv", $this->output($this->row));
	}
	
	/*
	 * backup_render function to download data to server as backup as per merchants  modified dates.
	 */
	function backup_merchant_modified_render($outputHeaders = true, $to_encoding = null, $from_encoding = "auto") {
		if ($outputHeaders) {
			if (is_string($outputHeaders)) {
				$this->setFilename($outputHeaders);
			}
			$this->backup_mrct_mdfd_renderHeaders();
		}
		
		if ($to_encoding) {
			$this->row = mb_convert_encoding($this->row, $to_encoding, $from_encoding);
		}
		return $this->output($this->row);
	}
	
	function backup_mrct_mdfd_renderHeaders() {
		file_put_contents("/var/www/html/mr2_new/app/webroot/".$this->filename."_modified_".date("Y-m-d").".csv", $this->output($this->row));
		//file_put_contents("D:/".$this->filename."_".date("Y-m-d").".csv", $this->output($this->row));
	}
	######################################################################################################################
	/*
	 * backup_madd_created_render function to download data to server as backup as per merchantaddresses created date
	 */
	function backup_madd_created_render($outputHeaders = true, $to_encoding = null, $from_encoding = "auto") {
		if ($outputHeaders) {
			if (is_string($outputHeaders)) {
				$this->setFilename($outputHeaders);
			}
			$this->backup_madd_created_renderHeaders();
		}
		
		if ($to_encoding) {
			$this->row = mb_convert_encoding($this->row, $to_encoding, $from_encoding);
		}
		return $this->output($this->row);
	}
	
	function backup_madd_created_renderHeaders() 
	{ 
		file_put_contents("/var/www/html/mr2_new/app/webroot/".$this->filename."_created_".date("Y-m-d").".csv", $this->output($this->row));
		//file_put_contents("D:/".$this->filename."_created".date("Y-m-d").".csv", $this->output($this->row));
	}
	
	/*
	 * backup_madd_modified_render function to download data to server as backup as per merchantaddresses modified date
	 */
	function backup_madd_modified_render($outputHeaders = true, $to_encoding = null, $from_encoding = "auto") {
		if ($outputHeaders) {
			if (is_string($outputHeaders)) {
				$this->setFilename($outputHeaders);
			}
			$this->backup_madd_modified_renderHeaders();
		}
		
		if ($to_encoding) {
			$this->row = mb_convert_encoding($this->row, $to_encoding, $from_encoding);
		}
		return $this->output($this->row);
	}
	
	function backup_madd_modified_renderHeaders() 
	{ 
		file_put_contents("/var/www/html/mr2_new/app/webroot/".$this->filename."_modified_".date("Y-m-d").".csv", $this->output($this->row));
		//file_put_contents("D:/".$this->filename."_modified".date("Y-m-d").".csv", $this->output($this->row));
	}
	######################################################################################################################
}

?>