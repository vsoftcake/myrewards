<?php
class ImageResizeHelper extends Helper
{
    function getResizedDimensions($file, $width, $height)
    {

        // Logic to get proportional width and height
	    $proportional = true;

		if ( $height <= 0 && $width <= 0 )
		{
	      return false;
	    }

	    $info = getimagesize($file);
	    $image = '';

	    $final_width = 0;
	    $final_height = 0;
	    list($width_old, $height_old) = $info;

	    if ($proportional && ($width_old>$width || $height_old>$height))
	    {
	    	if ($width == 0) $factor = $height/$height_old;
	      	elseif ($height == 0) $factor = $width/$width_old;
	      	else $factor = min ( $width / $width_old, $height / $height_old);

	      	$final_width = round ($width_old * $factor);
	      	$final_height = round ($height_old * $factor);
	    }
	    else
	    {
	      	$final_width = $width_old;
	     	$final_height = $height_old;
	    }

    	$data = array('width'=>$final_width,'height'=>$final_height);
		return $data;

    }
}

?>
