<?php echo $this->element('tinymce');?>
<?php echo $this->Html->script('jscolor'); ?>
<style type="text/css">
	#content
	{
		border-left:1px solid #cccccc;
		border-right:1px solid #cccccc;
		border-bottom:1px solid #cccccc;
		margin-left:20px !important;
		padding-left:18px !important;
		width:913px;
	}
	.rounded-corners 
	{
		-moz-border-radius: 10px 10px 0px 0px; /* Firefox */
		-webkit-border-radius: 10px 10px 0px 0px; /* Safari, Chrome */
		border-radius: 10px 10px 0px 0px; /* CSS3 */
	}
	.tr 
	{
		height:35px;
	}
	.tr1
	{
		height:85px;
	}
	.admin_button
	{
		height: 26px;
		padding-bottom: 3px;
		border-radius: 5px 5px 5px 5px;
		background-color: #0066cc;
		border: 1px solid #cccccc;
		color: #FFFFFF; 
		cursor: pointer;
		font-size:13px;
	}
</style>



<form action="<?php echo $this->Html->url('/admin/safs_invitation/edit/'. $this->Html->value('SafsInvitation.id')); ?>" method="post" id="edit_form" enctype="multipart/form-data">
<div style="width:953px;margin-left:-14px;">
	<div id="message"></div>
	<div style="width:949px;height:60px;">
		<div style=" height: 40px; float:left; width: 570px;padding-left:37px;">
	       	<div style=" height: 40px; float:left;padding-top:5px;">
	        	<img title="Products" alt="Products" src="/files/admin_home_icons/invitation.png" style="width:40px;height:40px;">
	        </div>
	        <div style=" height: 40px;float:left;padding-left:10px;">
	        	<h2>Edit Invitation</h2>
	        </div> 
		</div>
        <div class="actions" style=" height: 40px; float:left;  width: 340px;">
			<ul style="padding:20px;margin-left:-28px;">
				<li style="display:inline;"><?php echo $this->Form->button('Cancel', array('onclick' => "location.href='/admin/safs_invitation/index'",
				'class'=>'admin_button','style'=>'width:60px;','type'=>'button'));  ?></li>
		        	<li style="display:inline;">
		        		<button class="admin_button" type="submit" onclick="toggleSpellchecker()">Submit</button>
		        	</li>
	        	</ul>
		</div>
	</div>
	
		<div class="tabber" id="mytab1">

			<div class="tabbertab"  style="min-height:200px;overflow:hidden">
	  			<h2>Mail Invitation Details</h2>
				<div style="background-color:#EFEFEF;border:1px solid #cccccc;padding:10px;width:790px;min-height:475px;overflow:hidden">
					<div style="padding:20px;">
						<?php echo $this->Form->input('SafsInvitation.id', array('type' => 'hidden')); ?>	
						<table>
							<tr class="tr1">
								<td valign="top"><strong>Included programs</strong></td>
								<td valign="top">
									<?php echo $this->Form->input('Program.Program', array('multiple' => 'multiple', 'type' => 'select', 'options'=> $programsList, 'selected'=>$programs, 'div'=>false, 'label'=>false));?>
									<input type="hidden" name="data[Program][Program][]" value="" />
									<?php 
										$options = array('url' => 'update_program_clients','update' => 'ClientClient');
							           	echo $this->Ajax->observeField('ProgramProgram', $options);?>
								</td>
							</tr>
							<tr class="tr1">
								<td valign="top"><strong>Included clients</strong></td>
								<td valign="top">
									<?php echo $this->Form->input('Client.Client', array('multiple' => 'multiple', 'type' => 'select', 'options'=> $clientsList, 'selected'=>$clients, 'div'=>false, 'label'=>false));	?>
									<input type="hidden" name="data[Client][Client][]" value="" />
									<br/>
									<sub>Select program to load clients</sub>
								</td>
							</tr>
							<tr class="tr1">
								<td style="width:130px"><strong>Subject</strong></td>
								<td colspan="2"><?php echo $this->Form->input('SafsInvitation.subject',array('class'=>'map_input', 'style'=>'width:450px', 'div'=>false, 'label'=>false));?></td>
							</tr>
							<tr class="tr1">
								<td valign="top"><strong>Invite Message </strong></td>
								<td valign="top">
									<?php echo $this->Form->input('SafsInvitation.message_body',array('type' => 'textarea', 'div'=>false, 'label'=>false)); ?>
								</td>
								<td valign="top" style="padding-left:10px">
									The following field replacements are available:<br/>
									[[client_name]] replaced by the name of the client<br/>
									[[friend_name]] replaced by the name of the friends name<br/>
									<!--[[sales_manager_name]] replaced by the name of the sales manager<br/>-->
									[[user_first_name]] replaced by the name of the users first name<br/>
									[[user_last_name]] replaced by the name of the users last name<br/>
									[[user_membership_number]] replaced by the name of the users membership number<br/>
								</td>
							</tr>
							
							<tr class="tr1">
								<td style="width:130px"><strong>Sender Subject</strong></td>
								<td colspan="2"><?php echo $this->Form->input('SafsInvitation.sender_subject',array('class'=>'map_input', 'style'=>'width:450px', 'div'=>false, 'label'=>false));?></td>
							</tr>
							<tr class="tr1">
								<td valign="top"><strong>Sender Message </strong></td>
								<td valign="top">
									<?php echo $this->Form->input('SafsInvitation.sender_message_body',array('type' => 'textarea', 'div'=>false, 'label'=>false)); ?>
								</td>
								<td valign="top" style="padding-left:10px">
									The following field replacements are available:<br/>
									[[client_name]] replaced by the name of the client<br/>
									[[friend_name]] replaced by the name of the friends name<br/>
									<!--[[sales_manager_name]] replaced by the name of the sales manager<br/>-->
									[[user_first_name]] replaced by the name of the users first name<br/>
									[[user_last_name]] replaced by the name of the users last name<br/>
									[[user_membership_number]] replaced by the name of the users membership number<br/>
								</td>
							</tr>
						</table>
					</div>
				</div>
	 		</div>
		</div>
	</div>
</form>

<script type="text/javascript">
	
	function toggleSpellchecker()
	{
		var val = navigator.userAgent.toLowerCase();
		if(val.indexOf("msie") < 0)
		{
			tinyMCE.execCommand ('mceFocus', false, 'SafsInvitationMessageBody');
			tinyMCE.execCommand('mceSpellCheck',false,'SafsInvitationMessageBody');

		}
	}
	 
</script>