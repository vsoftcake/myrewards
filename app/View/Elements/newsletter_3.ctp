<?php // do the replacements
$nContent = $newsletter["Newsletter"]["content"];
$nContent = str_replace("[[client_name]]", $client['Client']['name'], $nContent);
$nContent = str_replace("[[client_email]]", $client['Client']['email'], $nContent);
$nContent = str_replace("[[program_name]]", $client['Program']['name'], $nContent);
$nContent = str_replace("[[web_site]]", $client['Domain']['name'], $nContent);
$nContent = str_replace("[[first_name]]", $user['User']['first_name'], $nContent);
$nContent = str_replace("[[last_name]]", $user['User']['last_name'], $nContent);
$nContent = str_replace("[[twitter_link]]", $client['Client']['twitter_link'], $nContent);
$nContent = str_replace("[[user_name]]", $user['User']['username'], $nContent);
$nContent = str_replace("[[password]]", $user['User']['password'], $nContent);
$nContent = str_replace("[[unique_id]]", $user['User']['unique_id'], $nContent);
$newsletter["Newsletter"]["content"] = $nContent;
?>3333
<div id="newsletter" align="center" style="font-family: <?php echo $newsletter['Newsletter']['font']; ?>;font-size:12px;<?php echo ($newsletter['Newsletter']['newsletter_background'] != '' && $newsletter['Newsletter']['newsletter_background'] != 'FFFFFF')? 'background-color: #'. $newsletter['Newsletter']['newsletter_background']. ';': "background-color: #8E8989"; ?>;padding:15px;min-height:400px;overflow:hidden;">
	<div style="margin-left:20px;width:600px;color:#009EC7;background-color:#FFFFFF;font-size:30px;font-weight:bold;text-align:center;padding-bottom:10px;border-left:2px solid #969393;border-right:2px solid #969393;border-top:2px solid #969393;">
		my rewards newsletter
	</div>
	<div style="border-left:2px solid #969393;border-right:2px solid #969393;border-bottom:2px solid #969393;margin-left:20px;width:600px;min-height: 100px; overflow: hidden; background-color: #ffffff;">
		<div style="border-bottom:2px solid #787878;">
			<?php if ($this->action != 'newsletter') : ?>
				<div style="text-align:center;<?php echo ($newsletter['Newsletter']['online_version_color'] != '' && $newsletter['Newsletter']['online_version_color'] != 'FFFFFF')? 'background-color: #'. $newsletter['Newsletter']['online_version_color']. ';': "background-color: #CBE60B"; ?>;font-size:14px;font-weight:bold;line-height:25px;">
					<a style="text-decoration:none;<?php echo ($newsletter['Newsletter']['version_text_color'] != '' && $newsletter['Newsletter']['version_text_color'] != 'FFFFFF')? 'color: #'. $newsletter['Newsletter']['version_text_color']. ';': "color: #3399FF"; ?>" href="http://<?php echo $client['Domain']['name']; ?><?php echo $this->Html->url('/newsletters/newsletter/'.$client['Client']['id'].'/'. $newsletter['Newsletter']['id']. '/'. $user['User']['unique_id']); ?>" target="newsletter">
						View the online version
					</a>
				</div>
			<?php endif;?>
			<div id="header">
				<?php 
					if($newsletter['Newsletter']['dashboard_id']==1) 
					{
						$path_image = 'files/clients/newsletter_banner_image_1/'. $clientbanner['0']['ClientBanner']['newsletter_banner_image']; 
					}
					if($newsletter['Newsletter']['dashboard_id']==2) 
					{
						$path_image = 'files/clients/newsletter_banner_image_2/'. $clientbanner['0']['ClientBanner']['newsletter_banner_image']; 
					}
					if($newsletter['Newsletter']['dashboard_id']==3) 
					{
						$path_image = 'files/clients/newsletter_banner_image_3/'. $clientbanner['0']['ClientBanner']['newsletter_banner_image'];
					}
				?>
				<?php $img=$this->ImageResize->getResizedDimensions($path_image, 600);?>
				<a href="http://<?php echo $client['Domain']['name']; ?><?php echo $this->Html->url('/'); ?>" style="<?php echo ($client['Style']['link_color'] != '')? 'color: #'. $client['Style']['link_color']. ';': ''; ?>" target="newsletter">
					<img width="<?php echo $img['width'];?>" src="http://<?php echo $client['Domain']['name']; ?>/<?php echo $path_image;?>" alt="" border="0" target="newsletter"/>
				</a>
			</div>
			<?php if (isset($newsletter_categories)) : ?>
			<?php if(!empty($newsletter_categories)) :?>
				<div style="min-height:20px;overflow:hidden;font-size:14px;<?php echo ($newsletter['Newsletter']['preview_menu_color'] != '' && $newsletter['Newsletter']['preview_menu_color'] != 'FFFFFF')? 'background-color: #'. $newsletter['Newsletter']['preview_menu_color']. ';': "background-color: #E8008A"; ?>;<?php echo ($newsletter['Newsletter']['menu_text_color'] != '' && $newsletter['Newsletter']['menu_text_color'] != 'FFFFFF')? 'color: #'. $newsletter['Newsletter']['menu_text_color']. ';': "color: #FFFFFF"; ?>">
					<ul style="margin:0px;padding-left:5px;">
					<?php $i=1;?>
						<?php foreach ($newsletter_categories as $category) : ?>
							<li style="font-weight:bold;float:left;list-style-type:none;width:auto;padding-top: 10px; padding-left: 5px; padding-right: 5px;">
								<div style="height:30px;font-size:12px;">
									<?php echo $category['Category']['name'];?> 
									<?php if($i<count($newsletter_categories)) :?>
										|
									<?php endif;?>
								</div>
							</li><?php $i++;?>
						<?php endforeach; ?>
					</ul>
				</div>	
			<?php endif;?>	
		<?php endif;?>	
	</div>
		
		<?php if (isset($newsletter_categories)) : ?>
		<?php if(!empty($newsletter_categories)) :?>
			<div style="width:600px;min-height: 200px; overflow: hidden; <?php echo ($newsletter['Newsletter']['contact_image_background'] != '' && $newsletter['Newsletter']['contact_image_background'] != 'FFFFFF')? 'background-color: #'. $newsletter['Newsletter']['contact_image_background']. ';': "background-color: #F3F3F3"; ?>;border-bottom:2px solid #969393; ">
			<table>
				<tr>
					<td valign="top">
						<div style="width:355px;padding-left:15px;float:left;text-align:left;">
							<?php foreach ($newsletter_categories as $category) : ?>
								<?php if (isset($category['Product'])) : ?>
									<?php foreach($category['Product'] as $product) : ?>
										<table>
											<tr>
												<td style="width:145px;vertical-align:top;padding:1em 0;">
													<div style="min-height:100px;overflow:hidden;padding-bottom:5px;">
														<?php if (trim($newsletter['Newsletter']['product_link']) == '') {
															$link = 'http://'. $client['Domain']['name']. '/products/view/'. $product['Product']['id'];
														} else {
															$link = trim($newsletter['Newsletter']['product_link']);
														}
														?>
														<a href="<?php echo $link; ?>" target="newsletter">
															<font style="font-size:12px;" face="<?php echo $newsletter['Newsletter']['font']; ?>">
																<img src="http://<?php echo $client['Domain']['name']; ?><?php echo $this->Html->url('/images/product/'. $product['Product']['id']. '.'. $product['Product']['image_extension']. '/no_logo'); ?>" alt="" border="" />
															</font>
														</a>
													</div>
													<div align="center">
														<?php if (trim($newsletter['Newsletter']['product_link']) == '') {
														$link = 'http://'. $client['Domain']['name']. '/products/view/'. $product['Product']['id'];
													} else {
														$link = trim($newsletter['Newsletter']['product_link']);
													}
													?>
														<a href="<?php echo $link; ?>" style="
															background-color: #00ADEF;
															color: #FFFFFF;
															text-decoration:none;
															padding:3px;
															" class="more rounded-corners" target="newsletter">
																<font style="font-size:12px;" face="<?php echo $newsletter['Newsletter']['font']; ?>">
																	&nbsp;&nbsp;&nbsp;&nbsp;Read More&nbsp;&nbsp;&nbsp;&nbsp;
																</font>
															</a>
													</div>
												</td>
												<td style="padding:1em 8px 1em 10px;vertical-align:top;">
													<?php if (trim($newsletter['Newsletter']['product_link']) == '') {
														$link = 'http://'. $client['Domain']['name']. '/products/view/'. $product['Product']['id'];
													} else {
														$link = trim($newsletter['Newsletter']['product_link']);
													}
													?>
													<a href="<?php echo $link; ?>"
														target="newsletter" class="item_header" style="text-decoration:none;color: #5F2A93;	
														<?php echo ($client['Style']['h4_size'] != '')? 'size: '. $client['Style']['h4_size']. ';': ""; ?>">
														<b>
															<font style="font-size:14px;" face="<?php echo $newsletter['Newsletter']['font']; ?>">
																<?php echo ($product['Product']['special_offer_headline'] != ''? $product['Product']['special_offer_headline']: $product['Product']['name']); ?>
															</font>
														</b>
													</a>
													<br/>
													<font face="<?php echo $newsletter['Newsletter']['font']; ?>">
														<?php if(!empty($product['Product']['highlight'] ) || $product['Product']['highlight']!='') : ?>
																<?php 
																	$h = strip_tags($product['Product']['highlight']);
																	$length = strlen($h);
																	if($length>40) :
																?>
																	<span style="font-size:13px;color:#33B1D2">
																		<b><?php echo substr($h,0,40).'....'; ?></b>
																	</span><br/><br/>
																<?php else : ?>
																	<span style="font-size:13px;color:#33B1D2">
																		<b><?php echo $product['Product']['highlight'];?></b>
																	</span><br/><br/>
																<?php endif; ?>
														<?php endif; ?>
															<?php 
																$d = strip_tags($product['Product']['details']);
																$len = strlen($d);
																if($len>70) :
																	echo substr($d,0,70).'....';
																else :
																	echo $product['Product']['details'];
																endif;
															?>
													</font>
												</td>
											</tr>
										</table>								
								  	<?php endforeach; ?>
								<?php endif; ?>
							<?php endforeach; ?>
						</div>
					</td>
					<td valign="top" style="border-left:1px solid #969393;">
						<div style="float:left;width:228px;">
							<div style="border-bottom:2px solid #969393;padding-bottom:10px;padding-top:10px">
								<div style="color:#009EC7;font-size:20px;text-align:center;">Follow us..</div>
								<div style="text-align:center">
									
									<?php if(!empty($newsletter['Newsletter']['twitter_link'])) : ?>
										<?php 
											$twitterurl = '';
											if (false === strpos($newsletter['Newsletter']['twitter_link'], '://')) 
											{
												$twitterurl = 'http://'.$newsletter['Newsletter']['twitter_link'];
											}
											else
											{
												$twitterurl = $newsletter['Newsletter']['twitter_link'];
											}
										?>
										<a href="<?php echo $twitterurl;?>" style="text-decoration:none;" target="_blank">
											<img style="vertical-align:middle;border:none !important;" src="http://<?php echo $client['Domain']['name']; ?>/files/social_icons/twitter.png" title="twitter"/>&nbsp;&nbsp;&nbsp;&nbsp;
										</a>
									<?php else : ?>
										<a href="http://twitter.com/#!/MyRewardsIntl" style="text-decoration:none;" target="_blank">
											<img style="vertical-align:middle;border:none !important;" src="http://<?php echo $client['Domain']['name']; ?>/files/social_icons/twitter.png" title="twitter"/>&nbsp;&nbsp;&nbsp;&nbsp;
										</a>
									<?php endif; ?> 
									
									<?php if(!empty($newsletter['Newsletter']['facebook_link'])) : ?>
										<?php 
											$facebookurl = '';
											if (false === strpos($newsletter['Newsletter']['facebook_link'], '://')) 
											{
												$facebookurl = 'http://'.$newsletter['Newsletter']['facebook_link'];
											}
											else
											{
												$facebookurl = $newsletter['Newsletter']['facebook_link'];
											}
										?>
										<a style="text-decoration:none;" target="_blank" href="<?php echo $facebookurl;?>">
											<img style="vertical-align:middle;border:none !important;" src="http://<?php echo $client['Domain']['name']; ?>/files/social_icons/fb.png" title="facebook"/><br/>
										</a>
									<?php else : ?>
										<a style="text-decoration:none;" target="_blank" href="http://www.facebook.com/pages/My-Rewards-International/133544976702355?v=wall">
											<img style="vertical-align:middle;border:none !important;" src="http://<?php echo $client['Domain']['name']; ?>/files/social_icons/fb.png" title="facebook"/><br/>
										</a>
									<?php endif; ?>
								</div>
								<div style="text-align:center">
									<?php 
										$filepath3 = NEWSLETTER_IMAGE3_PATH;
										$filename3 = $newsletter['Newsletter']['id']. '.'. $newsletter['Newsletter']['image3_ext'];
										$path3 = $this->Html->url(NEWSLETTER_IMAGE3_WWW_PATH. $newsletter['Newsletter']['id']. '.'. $newsletter['Newsletter']['image3_ext']); 
										$path_image3 = substr($path3,strpos($path3)+1);
									?>
									<?php if (file_exists($filepath3. $filename3)) :?>
										<?php $img=$this->ImageResize->getResizedDimensions($path_image3, 210, 285);?>
										<img width="<?php echo $img['width'];?>" height="<?php echo $img['height'];?>"  src="http://<?php echo $client['Domain']['name']; ?>/<?php echo $path_image3;?>" style="padding-right:5px;"/>
									<?php else : ?>
										<img style="padding-right:5px;" src="http://<?php echo $client['Domain']['name']; ?>/files/newimg/images/img/temp_connect.png"/>
									<?php endif;?>
								</div>
							</div>
							<br/>
							<!--<div style="border-bottom:2px solid #969393;">
								<span style="color:#E9008A;font-size:20px;">New Offers!! </span>
								<span style="color:#E9008A;font-size:16px;">Click here</span>
							</div>
							<br/>-->
							<div style="text-align:center">
								<span style="color:#E9008A;font-size:20px;">Mobile Coupons</span><br/>
								<?php 
									$filepath4 = NEWSLETTER_IMAGE4_PATH;
									$filename4 = $newsletter['Newsletter']['id']. '.'. $newsletter['Newsletter']['image4_ext'];
									$path4 = $this->Html->url(NEWSLETTER_IMAGE4_WWW_PATH. $newsletter['Newsletter']['id']. '.'. $newsletter['Newsletter']['image4_ext']); 
									$path_image4 = substr($path4,strpos($path4)+1);
								?>
								<?php if (file_exists($filepath4. $filename4)) :?>
									<?php $img=$this->ImageResize->getResizedDimensions($path_image4, 210, 245);?>
									<img width="<?php echo $img['width'];?>" height="<?php echo $img['height'];?>"  src="http://<?php echo $client['Domain']['name']; ?>/<?php echo $path_image4;?>" style="padding-right:10px;"/>
								<?php else : ?>
									<img src="http://<?php echo $client['Domain']['name']; ?>/files/newimg/images/img/mobile3.png" style="padding-right:10px;"/>
								<?php endif;?>
							</div>
						</div>
					</td>
				</tr>
			</table>
			</div>
		<?php endif;?>	
	<?php endif; ?>
	<div style="text-align:center;width:600px;padding-bottom:10px;padding-top:10px">
		<img src="http://<?php echo $client['Domain']['name']; ?>/files/newimg/images/cat_images/1.png" style="padding-left:10px"/>
		<img src="http://<?php echo $client['Domain']['name']; ?>/files/newimg/images/cat_images/2.png" style="padding-left:10px"/>
		<img src="http://<?php echo $client['Domain']['name']; ?>/files/newimg/images/cat_images/3.png" style="padding-left:10px"/>
		<img src="http://<?php echo $client['Domain']['name']; ?>/files/newimg/images/cat_images/4.png" style="padding-left:10px"/>
		<img src="http://<?php echo $client['Domain']['name']; ?>/files/newimg/images/cat_images/5.png" style="padding-left:10px"/>
		<img src="http://<?php echo $client['Domain']['name']; ?>/files/newimg/images/cat_images/6.png" style="padding-left:10px"/>
		<img src="http://<?php echo $client['Domain']['name']; ?>/files/newimg/images/cat_images/7.png" style="padding-left:10px"/>
		<img src="http://<?php echo $client['Domain']['name']; ?>/files/newimg/images/cat_images/8.png" style="padding-left:10px"/>
	</div>
	
	</div>
	<div style="margin-left:20px;width:600px;text-align:center;<?php echo ($newsletter['Newsletter']['coupon_book_color'] != '' && $newsletter['Newsletter']['coupon_book_color'] != 'FFFFFF')? 'background-color: #'. $newsletter['Newsletter']['coupon_book_color']. ';': "background-color: #F3F3F3"; ?>;<?php echo ($newsletter['Newsletter']['book_text_color'] != '' && $newsletter['Newsletter']['book_text_color'] != 'FFFFFF')? 'color: #'. $newsletter['Newsletter']['book_text_color']. ';': "color: #E9008A"; ?>;font-size:18px;line-height:55px;border-left:2px solid #969393;border-right:2px solid #969393;border-bottom:2px solid #969393;">
		See also the online Coupon Books! 
		<?php if(!empty($newsletter['Newsletter']['coupon_book_link'])) : ?>
			<?php 
				$linkurl = '';
				if (false === strpos($newsletter['Newsletter']['coupon_book_link'], '://')) 
				{
					$linkurl = 'http://'.$newsletter['Newsletter']['coupon_book_link'];
				}
				else
				{
					$linkurl = $newsletter['Newsletter']['coupon_book_link'];
				}
			?>
			<a style="text-decoration:none;<?php echo ($newsletter['Newsletter']['book_text_color'] != '' && $newsletter['Newsletter']['book_text_color'] != 'FFFFFF')? 'color: #'. $newsletter['Newsletter']['book_text_color']. ';': "color: #E9008A"; ?>" href="<?php echo $linkurl;?>" target="_blank"><b>click here</b></a>
		<?php endif; ?>
	</div>
	<div style="width:600px;margin-left:20px;text-align:center;<?php echo ($newsletter['Newsletter']['call_color'] != '' && $newsletter['Newsletter']['call_color'] != 'FFFFFF')? 'background-color: #'. $newsletter['Newsletter']['call_color']. ';': "background-color: #ADABAB"; ?>;<?php echo ($newsletter['Newsletter']['call_text_color'] != '' && $newsletter['Newsletter']['call_text_color'] != 'FFFFFF')? 'color: #'. $newsletter['Newsletter']['call_text_color']. ';': "color: #F9F7F7"; ?>;font-weight:bold;font-size:15px;line-height:50px;border-left:2px solid #969393;border-right:2px solid #969393;border-bottom:2px solid #969393;">
		Call <?php if(!empty($newsletter['Newsletter']['phone_number'])) : echo $newsletter['Newsletter']['phone_number']; else : echo "1300 368 846"; endif;?> for further information or search for more fabulous offers online.
	</div>
	<?php if ($newsletter['Newsletter']['footer_disabled'] == '0') : ?>
		<div style="width: 560px; text-align:left;margin-left: 20px;padding:10px 10px 1px 30px;<?php echo ($newsletter['Newsletter']['newsletter_text_color'] != '' && $newsletter['Newsletter']['newsletter_text_color'] != 'FFFFFF')? 'color: #'. $newsletter['Newsletter']['newsletter_text_color']. ';': "color: #7D7D7D"; ?>;<?php echo ($newsletter['Newsletter']['contact_image_background'] != '' && $newsletter['Newsletter']['contact_image_background'] != 'FFFFFF')? 'background-color: #'. $newsletter['Newsletter']['contact_image_background']. ';': "background-color: #F3F3F3"; ?>;border-left:2px solid #969393;border-right:2px solid #969393;">
			<font style="font-size:12px;" face="<?php echo $newsletter['Newsletter']['font']; ?>"><?php echo $newsletter['Newsletter']['footer_content']; ?></font>
		</div>
	<?php endif; ?>
	<?php if ($this->action != 'newsletter') : ?>
		<div style="width:560px;margin-left:20px;text-align:left;margin-bottom:10px;padding:1px 10px 10px 30px;<?php echo ($newsletter['Newsletter']['newsletter_text_color'] != '' && $newsletter['Newsletter']['newsletter_text_color'] != 'FFFFFF')? 'color: #'. $newsletter['Newsletter']['newsletter_text_color']. ';': "color: #7D7D7D"; ?>;<?php echo ($newsletter['Newsletter']['contact_image_background'] != '' && $newsletter['Newsletter']['contact_image_background'] != 'FFFFFF')? 'background-color: #'. $newsletter['Newsletter']['contact_image_background']. ';': "background-color: #F3F3F3"; ?>;border-left:2px solid #969393;border-right:2px solid #969393;border-bottom:2px solid #969393;">
			<a style="text-decoration:none;<?php echo ($newsletter['Newsletter']['newsletter_text_color'] != '' && $newsletter['Newsletter']['newsletter_text_color'] != 'FFFFFF')? 'color: #'. $newsletter['Newsletter']['newsletter_text_color']. ';': "color: #7D7D7D"; ?>" href="http://<?php echo $client['Domain']['name']; ?><?php echo $this->Html->url('/newsletters/unsubscribe/'. $user['User']['email']); ?>" target="newsletter">
				<b>Click here</b>
			</a> to unsubscribe.
		</div>
	<?php endif;?>
</div>
