<?php if (isset($hot_offers)) : ?>
<div style="padding-left:18px;">
	<div style="border:2px solid #cccccc;width:535px;height:435px">
		<div style="border:2px solid #ffffff;width:530px;height:430px">
			<?php foreach($hot_offers as $offer) : ?>
				<?php
					if($this->Session->read("dashboardidinsession") == 1) :
						$path = '/files/hot_offer_image/'. $offer['Product']['id']. '.'. $offer['Product']['hotoffer_extension'];
					elseif($this->Session->read("dashboardidinsession") == 2) :
						$path = '/files/hot_offer_img_saf/'. $offer['Product']['id']. '.'. $offer['ProductsSaf']['logo_extension'];
					elseif($this->Session->read("dashboardidinsession") == 3) :
						$path = '/files/hot_offer_img_points/'. $offer['Product']['id']. '.'. $offer['ProductsPoint']['logo_extension'];
					endif;
				?>
				<a href="/products/view/<?php echo $offer['Product']['id'];?>">
					<img width="530" height="430" src="<?php echo $path; ?>" alt="" />
				</a>
			<?php endforeach; ?>
		</div>
	</div>
</div>
<?php endif; ?>