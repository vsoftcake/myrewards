<style>
div#update1 {
      position:absolute;
      width: 488px !important;
      background-color:white;
      color: #353030;
      border:1px solid #ccc;
      margin:0px;
      padding:0px;
      font-size:11px;
}
div#update1 ul {
      list-style-type:none;
      margin:0px;
      padding:0px;
}
div#update1 ul li.selected 
{ 
      background-color: #E0EDFF;
}

div#update1 ul li span.highlight
{ 
      font-weight:bold;
      color:#0000CC;
}
div#update1 ul li {
      list-style-type:none;
      display:block;
      margin:0;
      padding:5px;
      cursor:pointer;
      line-height:13px;
      font-size:15px;
}
div.acwrap1 {
    height: 30px;
    padding: 0px;
    width:930px;
}
</style>

<script>
function searchProducts()
{
	var val = document.getElementById('keyword').value;
	if(val != '')
	{
		document.kform.submit();
		return true;
	}
	else
	{
		alert("please enter keyword to search products");
		return false;
	}
}
</script>
<div class="acwrap1"> 
	<form action="<?php echo $this->Html->url('/products/search_products'); ?>" method="post" name="kform" onsubmit="return searchProducts()">
    	<div>
    		<table style="width:600px; margin: 0px;"> 
    			<tr> 
		      		<td width="500px" style="border-bottom:none;">		      								
					<input type="text" name="keyword" id="keyword" autocomplete="off" value="" style="width:845px;color:#000;border-radius:0; font-style: italic; height:30px;padding:5px; border-color:#a9a9a9; box-shadow: 0 0 1px 0 #bcbcbc; " placeholder="Search keywords: E.g. movie tickets, hotels, gyms, car rental, pizza, suburb, city name"  onfocus="this.placeholder = ''" onblur="this.placeholder = 'Search keywords:  E.g. movie tickets, hotels, gyms, car rental, pizza, suburb, city name'"  class="search_input rounded_corners_button" maxlength="50" />					
		      		</td>
					
		      		<td style="border-bottom:none;">
					
					<img src="/images/search.png" id="search_glass" style="cursor: pointer;  margin: 4px 0 0 15px;  float: left;   height: 20px;   position: absolute;   width: 20px;" />
					
						<button id="search" class="map_button_style rounded_corners_button" type="submit" style="font-style: italic; font-weight: normal; background-color: rgb(188, 188, 188); margin-left: 11px;
    padding: 0 8px 0 23px; border-radius: 0px; width: 93px; border: medium none;">SEARCH</button>
					
			      		<span id="rotate" style="display: none">
							<img style="position: relative; top:4px;right:93px; " src="/files/newimg/ajax-loader.gif" alt="Working..." />
						</span>
			      	</td>
		      	</tr>
			</table>  
		</div>	
  	</form>
</div>
<script type="text/javascript">
function positionAuto(element, entry) {
	setTimeout( function() {
		Element.clonePosition('update1', 'keyword', {
			'setWidth': false,
			'setHeight': false,
			'offsetTop': $('keyword').offsetHeight
		});
	}, 300);
	return entry;
}

 /* $(function() {
		$( "#keyword" ).autocomplete({
			source: "/products/auto_complete_keyword/",
			minLength: 2
		});
	}); */

</script>