<style>
.bottom_rounded-corners
{
	-moz-border-radius: 0px 0px 10px 10px; /* Firefox */
	-webkit-border-radius: 0px 0px 10px 10px; /* Safari, Chrome */
	border-radius: 0px 0px 10px 10px; /* CSS3 */
}
.rounded-corners
{
	-moz-border-radius: 10px 10px 10px 10px; /* Firefox */
	-webkit-border-radius: 10px 10px 10px 10px; /* Safari, Chrome */
	border-radius: 10px 10px 10px 10px; /* CSS3 */
}
</style>
<?php if($newsletter['Newsletter']['preview']=='O') :?>
	<?php echo $this->element('newsletter_old', array('newsletter_categories' => $newsletter_categories)); ?>
<?php elseif($newsletter['Newsletter']['preview']=='N') :?>
	<?php echo $this->element('newsletter_new', array('newsletter_categories' => $newsletter_categories)); ?>
<?php elseif($newsletter['Newsletter']['preview']=='1') :?>
	<?php echo $this->element('newsletter_1', array('newsletter_categories' => $newsletter_categories)); ?>
<?php elseif($newsletter['Newsletter']['preview']=='2') :?> 
	<?php echo $this->element('newsletter_2', array('newsletter_categories' => $newsletter_categories)); ?>
<?php elseif($newsletter['Newsletter']['preview']=='3') :?> 
	<?php echo $this->element('newsletter_3', array('newsletter_categories' => $newsletter_categories)); ?>
<?php elseif($newsletter['Newsletter']['preview']=='4') :?> 
	<?php echo $this->element('newsletter_4', array('newsletter_categories' => $newsletter_categories)); ?>
<?php endif;?>
