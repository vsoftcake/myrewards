"<table bgcolor='white' style='font-family:"+font_style+";width:985px'>"+
	"<tr>"+
		"<td>"+
			"<div style='width:972px'>"+
				"<div style='padding:18px;font-size:15px;background:"+background+" #"+tab_background_color+";' class='tm rounded-corners menup_color'>"+
					"<ul id='menup'>"+
						"<li><a href='/' style='font-family:"+menu_font_style+";color:#"+tab_menu_text_color+"'>Dashboard</a></li>"+
					"</ul>"+
					"<ul id='menup'>"+
						"<li><a href='/display/member_services' style='font-family:"+menu_font_style+";color:#"+tab_menu_text_color+"'>How it Works</a></li>"+
						"<li><a href='/display/savings_calculator' style='font-family:"+menu_font_style+";color:#"+tab_menu_text_color+"'>Savings Calculator</a></li>"+
						"<li><a href='/display/whats_new' style='font-family:"+menu_font_style+";color:#"+tab_menu_text_color+"'>What's New</a>"+
							"<ul id='menup' style='background:#"+tab_background_color+"'>"+
								"<li><a style='font-family:"+menu_font_style+";color:#"+tab_menu_text_color+"' href='/display/special_offers'>Special Offers</a></li>"+
							"</ul>"+
						"</li>"+
						"<li><a style='font-family:"+menu_font_style+";color:#"+tab_menu_text_color+"' href='/display/my_membership'>My Membership</a>"+
							"<ul id='menup' style='background:#"+tab_background_color+"'>"+
								"<li><a style='font-family:"+menu_font_style+";color:#"+tab_menu_text_color+"' href='/users/edit'>Change Details</a></li>"+
								"<li><a style='font-family:"+menu_font_style+";color:#"+tab_menu_text_color+"' href='/display/nominate_a_friend'>Nominate a Friend</a></li>"+
								"<li><a style='font-family:"+menu_font_style+";color:#"+tab_menu_text_color+"' href='/contacts/feedback'>Feedback</a></li>"+
							"</ul>"+
						"</li>"+
						"<li><a style='font-family:"+menu_font_style+";color:#"+tab_menu_text_color+"' href='/display/customer_service'>Customer Service</a>"+
							"<ul id='menup' style='background:#"+tab_background_color+"'>"+
								"<li><a style='font-family:"+menu_font_style+";color:#"+tab_menu_text_color+"' href='/display/terms_and_conditions'>Terms &amp; Conditions</a></li>"+
								"<li><a style='font-family:"+menu_font_style+";color:#"+tab_menu_text_color+"' href='/display/fair_use_policy'>Fair Use Policy</a></li>"+
								"<li><a style='font-family:"+menu_font_style+";color:#"+tab_menu_text_color+"' href='/display/member_faq'>FAQS</a></li>"+
								"<li><a style='font-family:"+menu_font_style+";color:#"+tab_menu_text_color+"' href='/display/privacy_policy'>Privacy Policy</a></li>"+
								"<li><a style='font-family:"+menu_font_style+";color:#"+tab_menu_text_color+"' href='/contacts/contact_us'>Contact Us</a></li>"+
							"</ul>"+
						"</li>"+
					"</ul>"+	
					"<ul id='menup'>"+
						"<li>"+
							"<a style='font-family:"+menu_font_style+";color:#"+tab_menu_text_color+"' href='#'>Advanced Search</a>"+
						"</li>"+ 
					"</ul>"+
				"</div>"+
			"</div>"+
			"<div style='width:970px;padding-top:10px;padding-bottom:10px;border-top:1px solid #"+border_color+";border-right:1px solid #"+border_color+";border-left:1px solid #"+border_color+";background-color:#"+tab_content_background_color+";font-size:15px;font-family:arial;'>"+
				"<table style='font-family:"+font_style+";'>"+
					"<tr>"+
  						"<td style='padding-bottom:5px;font-size:12px;'>"+
	        				"<table>"+
								"<tr>"+
  									"<td style='padding-bottom:5px;padding-left:20px;'>Country</td>"+
	        						"<td><input style='border:1px solid #"+border_color+";width:150px;height:20px;' type='text' /></td>"+
	        					"<tr>"+
	        					"<tr>"+
  									"<td style='padding-bottom:5px;padding-left:20px;'>Category</td>"+
	        						"<td>"+
	        							"<select style='width:165px;height:25px;border:1px solid #"+border_color+";'>"+
											"<option value=''>  </option>"+
										"</select>"+
									"</td>"+
	        					"<tr>"+
	        				"</table>"+
	        			"</td>"+
        				"<td style='padding-bottom:5px;font-size:13px;'>"+
        					"<table>"+
								"<tr>"+
  									"<td style='padding-bottom:5px;padding-left:20px;'>State</td>"+
	        						"<td>"+
	        							"<select style='width:165px;height:25px;border:1px solid #"+border_color+";'>"+
											"<option value=''>  </option>"+
										"</select>"+
	        						"</td>"+
	        					"<tr>"+
	        					"<tr>"+
  									"<td style='padding-bottom:5px;padding-left:20px;'>Keyword/s</td>"+
	        						"<td><input style='border:1px solid #"+border_color+";width:150px;height:20px;' type='text' /></td>"+
	        					"<tr>"+
	        				"</table>"+
        				"</td>"+
        				"<td style='padding-bottom:5px;font-size:13px;'>"+
        					"<table>"+
        						"<tr>"+
  									"<td style='padding-bottom:5px;padding-left:20px;'>City/Suburb/Postcode</td>"+
	        						"<td><input style='border:1px solid #"+border_color+";width:150px;height:20px;' type='text' /></td>"+
	        					"<tr>"+
								"<tr>"+
  									"<td style='padding-bottom:5px;padding-left:20px;'>Radius Search</td>"+
	        						"<td>"+
	        							"<select style='width:120px;height:25px;border:1px solid #"+border_color+";'>"+
											"<option value=''>  </option>"+
										"</select>"+
	        						"approx </td>"+
	        					"<tr>"+
	        				"</table>"+
        				"</td>"+
        				"<td style='padding-left:20px;'>"+
    						"<button type='submit' class='rounded-corners_button' style='height:40px;width:65px;font-size:16px;margin-top:10px;font-weight:bold;color:#"+button_text_color+";border:#"+button_border_color+" 1px solid;background-color:#"+button_background_color+"'>Go !</button>"+
    						"<button onclick='resetdata()' class='rounded_corners_button' style='padding:3px;margin-left:10px;color:#"+button_text_color+";border:#"+button_border_color+" 1px solid;background-color:#"+button_background_color+"' type='button'>Clear</button>"+
    					"</td>"+
					"</tr>"+
				 "</table>"+
			"</div>"+
		"</td>"+
	"</tr>"+
"</table>"+

"<table padding='6' bgcolor='white' style='font-family:"+font_style+";width:985px'>"+
	"<tr>"+
		"<td valign='top'>"+                      
			"<div class='rounded-corners_bottom' style='width:972px;min-height:490px;overflow:hidden;float:left;margin-right:5px;'>"+
				"<table style='width:972px'>"+
					"<tr>"+
						"<td style='width:767px;border-top: 1px solid #"+border_color+";'>"+
							"<div style='float:left;width:780px;'>"+
								"<div style='padding:6px 12px 0 ! important;white-space:normal !important;border-right: 1px solid #"+border_color+";border-left: 1px solid #"+border_color+";background-color: #"+tab_content_background_color+";font-size:13px;'>"+
									"<div style='padding:5px;float:left'>"+
										"<a href='javascript:window.history.back()' id='back' style='color:#"+link_color+";font-weight:normal;display:none'><img title='Back' alt='Back' src='/files/newimg/back.png'></a>"+
									"</div>"+
									"<div style='padding-top:5px;float:right;padding-right:10px; padding-bottom: 10px;width:228px'>"+
										"<form name='myform' method='get' action='/products/search2'>"+
											"<div style='float:left;padding-top:14px;padding-right:2px;'>"+
												"<img width='25' src='/files/newimg/magnifier.png'>"+
											"</div>"+
											"<div align='center' style='float: left;padding-top: 3px; position: relative; width: 150px; height: 40px; top: 12px;'>"+
												"<input type='text' style='color:#C8D6D6;font-style:italic;height:20px;width:130px;border:1px solid #"+border_color+"' onblur='checkblur()' onfocus='checkFocus()' value='Quick Search' id='check' class='rounded_corners_button' name='data[Product][keywords]'>"+
											"</div>"+
											"<div style='float:right;padding-top:15px;'>"+
												"<button style='color:#"+button_text_color+";border:#"+button_border_color+" 1px solid;background-color:#"+button_background_color+";font-size:12px;font-weight:bold;height:24px;padding:2px 3px 4px;width:42px' onclick='checkClick()' class='rounded_corners_button' type='submit'>Go !</button>"+
											"</div>"+
										"</form>"+
									"</div>"+
									"<div style='padding:10px;background-color:#ffffff;' class='rounded_corners_map map_border'>"+
										"<div class='wishlist' style='font-weight: bold;' id='divout'></div>"+
											"<table width='100%'>"+
												"<tr>"+
													"<td>"+
														"<div style='width:565px;'>"+
															"<div style='font-size:16px;font-weight:bold;padding-bottom:8px'>Madame Tussauds Sydney</div>"+
															"<table>"+
																"<tr>"+
																	"<td>"+
																		"<img src='/files/newimg/offer_highlight.png' style='margin-bottom:5px;left:-25px;position:relative'>"+
																	"</td>"+
																	"<td>"+	
																		"<h3 style='margin-bottom:15px !important;color:#"+h3_color+";padding-bottom: 0px;'>20% Discount</h3>"+
																	"</td>"+
																"</tr>"+
															"</table>"+
															"<div>"+
																"<p>"+
																"Experience the glittering world of fame for a day at Madame Tussauds Sydney by starting your journey on the red carpet and strike a pose "+
																"for the paparazzi. Step back in time as you join Captain Cook's first voyage to Sydney Harbour before you challenge your favourite Aussie "+
																"sports heroes, sing along with your favourite international music icons and star in one Australia's popular television shows before you "+
																"arrive at the biggest celebrity party ever staged. So, who do you want to meet?"+
																"</p>"+												
															"</div>"+
															"<p style='font-size:15px;color:#C0C0C0;font-style:italic;'>"+
																"Leisure & Entertainment Sydney New South Wales"+
															"</p>"+
															"<p>"+
																"<strong>OFFER:</strong>20% discount on single adult and child admission tickets."+
															"</p>"+
															"<p>"+
																"<strong>REWARD ME:</strong>Print and present coupon to receive discount."+
															"</p>"+
															"<p>"+
																"Terms & Conditions: No valid in conjunction with any other offer. 20% off single adult & child admission tickets. "+
																"Offer not valid on group, concession, family or combined tickets. Not valid in conjunction with any other offer."+
															"</p>"+
															"<p style='font-size:13px;font-style:italic;'>"+
																"Aquarium Wharf, Darling Harbour, Sydney New South Wales 2000, Australia"+
															"</p>"+
															"<table>"+
																"<tr>"+
																	"<td valign='top'>"+
																		"<img src='/files/newimg/web.png' style='padding-right:10px'>"+
																	"</td>"+
																	"<td valign='middle'>"+
																		"<a style='color:#"+tab_link_color+";' target='_blank' href='http://www.madametussauds.com/sydney'>"+
																			"www.madametussauds.com/sydney"+
																		"</a>"+
																	"</td>"+
																"</tr>"+
															"</table>"+
														"</div>"+
													"</td>"+
													"<td style='vertical-align:top;text-align:center;width:150px;'>"+
														"<img width='145' height='110' alt='' src='/files/search_result_images/1024499.jpg'><br><br>"+
														"<img width='145' height='110' alt='' src='/files/search_result_images/1027308.jpg'>"+
													"</td>"+
												"</tr>"+
												"<tr>"+
													"<td colspan='2'>"+
														"<hr style='border:1px dashed #cccccc;'>"+
														"<div style='float:left;'>"+
															"<a target='_blank' href='http://www.madametussauds.com/sydney'>"+
																"<img title='Web Link' alt='web link' src='/files/styles/icon_images/link.png' style='padding:5px 25px;border-right:1px solid #cccccc'>"+
															"</a>"+
															"<a href='/products/voucher/1027308/25485'>"+
																"<img title='Print' alt='print' src='/files/styles/icon_images/print.png' style='padding:5px 25px;border-right:1px solid #cccccc'>"+
															"</a>"+
															"<a href='/products/voucher/1027308/25485'>"+
																"<img title='Web Coupon' alt='web coupon' src='/files/styles/icon_images/voucher.png' style='padding:5px 25px;border-right:1px solid #cccccc'>"+
															"</a>"+
															"<a onclick='showWish(1027308,3097144)' href='#'>"+
																"<img title='Add to Favourites' alt='Add to Favourites' src='/files/styles/icon_images/wishlist.png' style='padding:5px;'>"+
															"</a>"+
														"</div>"+
													"</td>"+
												"</tr>"+
											"</table>"+
										"</div>"+
										"<br>"+
										"<div style='padding:15px;background-color:#ffffff;' class='rounded_corners_map map_border'>"+
											"<div style='width:700px;text-align:right'>"+
												"<a style='color:#"+tab_link_color+";' onclick='Modalbox.show(this.href, {title: this.title, width: 450}); return false;' href='/comments/add_comments/1027308'>Add Comments</a>"+
											"</div>"+
											"<div style='color:blue'>Be the first to comment on this Product</div>"+
										"</div>"+
										"<br>"+
									"</div>"+
									"<div style='border-bottom: 1px solid #"+border_color+";border-right:1px solid #"+border_color+";border-left:1px solid #"+border_color+";background-color:#"+tab_content_background_color+";' class=' rounded-corners_bottom'>&nbsp;</div>"+
							"</div>"+
						"</td>"+
						"<td style='width:5px;border-top: 1px solid #"+border_color+";'>&nbsp;</td>"+
						"<td valign='top' style='width:180px;border-top: 1px solid #"+border_color+";'>"+
							"<div>"+
								"<div style='padding:13px 12px 0 ! important;border-right:1px solid #"+border_color+";border-left:1px solid #"+border_color+";background-color:#"+tab_content_background_color+";'>"+
									"<div align='center'>"+
										"<a href='/products/view/1012934'>"+
											"<img width='120' height='600' alt='' src='/files/search_result_images/25.jpg'>"+
										"</a>"+
									"</div>"+
								"</div>"+
								"<div style='border-right:1px solid #"+border_color+";border-left:1px solid #"+border_color+";background-color:#"+tab_content_background_color+";border-bottom: 1px solid #"+border_color+";' class='rounded-corners_bottom'>&nbsp;</div>"+
							"</div>"+
						"</td>"+
					"</tr>"+
				"</table>"+	
			"</div>"+
		"</td>"+
	"</tr>"+
"</table>"+
"<div style='height: 8px;background-color:white'></div>"+
"<table bgcolor='white' style='font-family:"+font_style+";width:985px'>"+
	"<tr>"+
		"<td colspan='2'>"+
			"<div style='font-family:"+font_style+";width:1005px;margin-top:0px;margin-left:-10px;background-color:#"+footer_background_color+";'>"+
				"<div style='padding-top: 10px; padding-left: 10px;height:120px;'>"+
					"<table style='width:986px'>"+
						"<tr>"+
							"<td valign='top'>"+
								"<ul id='menuf'>"+
									"<li><a style='color:#"+footer_text_color+";text-transform:uppercase;' href='/'><b>Dashboard</b></a></li>"+
								"</ul>"+
								"<ul id='menuf'>"+
									"<li><a style='color:#"+footer_text_color+";' href='/display/how_it_works'><b>HOW IT WORKS</b></a></li>"+
									"<li><a style='color:#"+footer_text_color+";' href='/display/savings_calculator'><b>SAVINGS CALCULATOR</b></a></li>"+
									"<li><a style='color:#"+footer_text_color+";' href='/display/whats_new'><b>WHAT'S NEW</b></a>"+
										"<ul id='menuf'>"+
											"<li><a style='color:#"+footer_text_color+";' href='/display/special_offers'>Hot Offers</a></li>"+
											"<li><a style='color:#"+footer_text_color+";' href='/display/news'>Online Coupon</a></li>"+
										"</ul>"+
									"</li>"+
									"<li><a style='color:#"+footer_text_color+";' href='/display/my_membership'><b>MY MEMBERSHIP</b></a>"+
										"<ul id='menuf'>"+
											"<li><a style='color:#"+footer_text_color+";' href='/users/edit'>Change Details</a></li>"+
											"<li><a style='color:#"+footer_text_color+";' href='/display/nominate_a_friend'>Invite My Family </a></li>"+
										"</ul>"+
									"</li>"+
									"<li><a style='color:#"+footer_text_color+";' href='/display/customer_service'><b>CUSTOMER SERVICE</b></a>"+
										"<ul id='menuf'>"+
											"<li><a style='color:#"+footer_text_color+";' href='/display/terms_and_conditions'>Terms &amp; Conditions</a></li>"+
											"<li><a style='color:#"+footer_text_color+";' href='/display/fair_use_policy'>Fair Use Policy</a></li>"+
											"<li><a style='color:#"+footer_text_color+";' href='/display/member_faq'>FAQs</a></li>"+
											"<li><a style='color:#"+footer_text_color+";' href='/display/privacy_policy'>Privacy Policy</a></li>"+
											"<li><a style='color:#"+footer_text_color+";' href='/contacts/contact_us'>Contact Us</a></li>"+
										"</ul>"+
									"</li>"+
								"</ul>"+							
							"</td>"+
							"<td style='text-align:center;width:230px;padding-top:35px;'>"+
								"<img width='' height='' alt='' src='/blank.png'>"+
							"</td>"+
						"</tr>"+
					"</table>"+
				"</div>"+
				"<br>"+
				"<div style='padding-left:30px;padding-bottom:8px;color:#"+footer_text_color+";'>"+
					"&copy; My Rewards International  Ltd 2012 All Rights Reserved. <br>Follow us on &nbsp;"+									
					"<a href='http://www.facebook.com/pages/My-Rewards-International/133544976702355?v=wall' target='_blank' style='text-decoration:none;border:none !important;'>"+
						"<img width='20' title='facebook' src='/files/social_icons/fb.png' style='vertical-align:middle'>"+
					"</a>"+
					"&nbsp;&nbsp;"+
					"<a href='http://twitter.com/#!/MyRewardsIntl' target='_blank' style='text-decoration:none;border:none !important;'>"+
						"<img width='20' title='twitter' src='/files/social_icons/twitter.png' style='vertical-align:middle'>"+
					"</a>"+
				"</div>"+
			"</div>"
		"</td>"+
	"</tr>"+
"</table>"


