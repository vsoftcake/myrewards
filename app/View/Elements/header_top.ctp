<script type="text/JavaScript" src="//<?php echo $this->Session->read('client.Domain.name');?>/livehelp/scripts/jquery-latest.js"></script>
<script type="text/javascript">
	var JQry = jQuery.noConflict();
</script>
<?php if ($this->Session->read('client.Client.live_chat_enabled') == '1') : ?>
	<!-- stardevelop.com Live Help International Copyright - All Rights Reserved //-->
	<!-- BEGIN stardevelop.com Live Help Messenger Code - Copyright - NOT PERMITTED TO MODIFY COPYRIGHT LINE / LINK //-->
	
	<script type="text/javascript">
	<!--
	
	    var LiveHelpSettings = {};
	    var name1 = '<?php echo $this->Session->read('user.User.username');?>';
	    var email1 = '<?php echo $this->Session->read('user.User.email');?>';
	    var client = '<?php echo $this->Session->read('client.Client.name');?>';

	    LiveHelpSettings.server = '<?php echo $this->Session->read('client.Domain.name');?>';
	    LiveHelpSettings.embedded = true;
	    (function(d, JQry, undefined) {
		JQry(window).ready(function() {
		    // JavaScript
		    var LiveHelp = d.createElement('script'); LiveHelp.type = 'text/javascript'; LiveHelp.async = true;
		    LiveHelp.src = ('https:' == d.location.protocol ? 'https://' : 'http://') + LiveHelpSettings.server + '/livehelp/scripts/jquery.livehelp.js';
		    var s = d.getElementsByTagName('script')[0];
		    s.parentNode.insertBefore(LiveHelp, s);
		});
	    })(document, jQuery);
	-->
	</script>
	<!-- END stardevelop.com Live Help Messenger Code - Copyright - NOT PERMITTED TO MODIFY COPYRIGHT LINE / LINK //-->
	<!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js">-->
</script>
<script>
JQry(document).ready(function(){
  JQry("#live_chat_img").click(function(){
    JQry(this).hide();
  });
});
</script>
	<!-- stardevelop.com Live Help International Copyright - All Rights Reserved //-->
		<!--<div style="position:fixed; bottom:0;right:1px;z-index:1005">
		<p id="live_chat_img" style="margin:0"><img src="http://www.myrewards.com.au/app/webroot/livehelp/locale/en/images/eyecatcher_en.png" /></p>
		<a href="#" class="LiveHelpButton">
		<img src="http://<?php echo $this->Session->read('client.Domain.name');?>/livehelp/include/status.php" id="LiveHelpStatus" name="LiveHelpStatus" class="LiveHelpStatus" border="0" alt="Live Help" id="Image-Maps_2201309240648291" usemap="#Image-Maps_2201309240648291" /></a>
<map id="_Image-Maps_2201309240648291" name="Image-Maps_2201309240648291">
<area shape="rect" coords="105,0,205,22" href="https://www.facebook.com/myrewardsinternational?v=wall/" alt="" title="" target="_blank" />
<area shape="rect" coords="208,0,295,22" href="https://twitter.com/MyRewardsIntl" alt="" title="" target="_blank" />
<area shape="rect" coords="298,25,300,27" href="http://www.image-maps.com/index.php?aff=mapped_users_2201309240648291" alt="Image Map" title="Image Map" target="_blank" />
</map>
		</div>//-->
	<!-- END Live Help HTML Code - NOT PERMITTED TO MODIFY IMAGE MAP/CODE/LINKS //-->

<?php endif;?>
<?php
	//for getting default dashboard banner_option
	$dashboard_banner_option = 1;
	if($this->Session->read('dashboardidinsession') == 1)
	{
		if($this->Session->read('client.ClientBanner.0.dashboard_id')==1)
		{
			$dashboard_banner_option=$this->Session->read('client.ClientBanner.0.banner_option');
		}
	}
	if($this->Session->read('dashboardidinsession') == 2)
	{
		if($this->Session->read('client.ClientBanner.1.dashboard_id')==2)
		{
			$dashboard_banner_option=$this->Session->read('client.ClientBanner.1.banner_option');
		}
	}
	if($this->Session->read('dashboardidinsession') == 3)
	{
		if($this->Session->read('client.ClientBanner.2.dashboard_id')==3)
		{
			$dashboard_banner_option=$this->Session->read('client.ClientBanner.2.banner_option');
		}
	}
	if($this->Session->read('dashboardidinsession') == 3)
	{
		if($this->Session->read('client.ClientBanner.1.dashboard_id')==3)
		{
			$dashboard_banner_option=$this->Session->read('client.ClientBanner.1.banner_option');
		}
	}
			
 ?>
<?php if ($dashboard_banner_option == '0') : ?>
	<?php if  ($this->Session->read('client.Client.header_links_disabled') != '1') :  ?>
	 	<div style="background-image:url(/files/newimg/topmenubg.png); background-repeat:repeat-x;  height:50px" class="header_color">
	 		<div style="padding-left:420px;padding-top:25px;float:left">
				<div style="height:25px;padding-left:5px;padding-right:5px" class="header_tab_color rounded-corners" id="header_top">
					<table>
						<tr id="header_top" height="24px">
							<td style="white-space:nowrap;text-align:center;" class="header_tab_text_color">
								<?php if  ($this->Session->check('user')) : ?>
							<b style="padding-right:30px">Welcome back <?php echo $this->Session->read('user.User.first_name'). ' '. $this->Session->read('user.User.last_name'); ?></b>  
							
							<?php if(in_array('1', $this->Session->read('dashboard'))) : ?>
								<?php if($this->Session->read('dashboardidinsession') == 1) :?>
									<?php if(($this->Session->read('client.Client.cart_enabled') == 1) && (count($this->Session->read('cart_products_count')) > 0)) : ?>
										<a href="<?php echo $this->Html->url('/products/shop_now/'); ?>">Shop Now</a> |
									<?php endif;?>
								<?php endif;?>
							<?php endif;?>
							
							<?php if(in_array('1', $this->Session->read('dashboard'))) : ?>
							<?php if(($this->Session->read('dashboardidinsession') != 1)) : ?>
									<?php echo $this->Html->link('My Rewards','/myrewards');?> |
							<?php endif;?>
							<?php endif;?>

							<?php if((in_array('2', $this->Session->read('dashboard')))) : ?>
							<?php if(($this->Session->read('dashboardidinsession') != 2)) : ?>
									<?php echo $this->Html->link('Send A Friend','/send_a_friend');?> |
							<?php endif;?>
							<?php endif;?>

							<?php if((in_array('3', $this->Session->read('dashboard')))) : ?>
							<?php if(($this->Session->read('dashboardidinsession') != 3)) : ?>
								<?php echo $this->Html->link('My Points','/mypoints');?> |
							<?php endif;?>
							<?php endif;?>
							
							<?php if(in_array($this->Session->read('dashboardidinsession'),$this->Session->read('campaign_dashboard')) && $this->Session->read('competitions')!= null) :?>
								<?php echo $this->Html->link('Competitions',array('controller'=>'campaigns','action' => 'details'));?> |
							<?php endif;?>
							
							<?php if(($this->Session->read('dashboardidinsession') == 3)) : ?>
								<?php if(($this->Session->read('client.Client.cart_enabled') == 1) && (count($this->Session->read('cart_products_points')) > 0)) : ?>
									<a href="<?php echo $this->Html->url('/carts/show_cart/'); ?>">My Cart</a> |
								<?php endif;?>
							<?php endif;?>

							<?php if(($this->Session->read('dashboardidinsession') == 1)) : ?>
								<?php if(($this->Session->read('client.Client.cart_enabled') == 1) && (count($this->Session->read('cart_products_count')) > 0)) : ?>
									<a href="<?php echo $this->Html->url('/carts/show_cart/'); ?>">My Cart</a> |
								<?php endif;?>
							<?php endif;?>
							
							<a href="<?php echo $this->Html->url('/products/view_wishlist/'.$this->Session->read('user.User.last_name')); ?>">My Favorites</a> |
							<?php $pages = $this->Tree->allowed_pages(array('/users/edit'), $this->Session->read('client.Page')); ?>
							<?php if (isset($pages['/users/edit'])) : ?>
								<?php if  (($this->Session->read('client.Client.id') != '965') || ($this->Session->read('client.Client.id') != '1618')):  ?>
									<?php echo $this->Html->link('My Account','javascript:void(0)',array('onclick' => "editDetails()"));?> |
								<?php endif; ?>
							<?php endif; ?>
							<?php if($this->Session->read('client.Client.program_id') != 7) : ?>
								<a href="<?php echo $this->Html->url('/users/logout/'); ?>">Sign Out</a>
							<?php endif; ?>
							<?php if($this->Session->read('client.Client.program_id') == 7 && $this->Session->read('client.Client.link_enabled') == 1 ) : ?>
								<a href="<?php echo $this->Session->read('client.Client.link');?>" >
									<?php echo $this->Session->read('client.Client.link_text');?>
								</a>
							<?php endif; ?>
							<?php if ($this->Session->read('user.User.type') == 'Administrator' || $this->Session->read('user.User.type') == 'Power User' || 
									$this->Session->read('user.User.type') == 'Client Admin' || $this->Session->read('user.User.type') == 'Country Admin') : ?>
							| <a href="<?php echo $this->Html->url('/admin/'); ?>">Admin</a>
							<?php endif; ?>
						<?php else : ?>
							<?php echo $this->Html->link('Login','javascript:void(0)',array('onclick' => "showLogin()"));?> |
							<!--<?php echo $this->Html->link('First Time Login','javascript:void(0)',array('onclick' => "showFirstTimeLogin()"));?> 
							<?php echo $this->Html->link('First Time Login',array('controller'=>'users','action' => 'first_time_login'),array('onclick' => "showFirstTimeLogin(this.href, {title: this.title, width: 800}); return false;"));?> -->
							<a href="<?php echo $this->Html->url('/users/first_time_login/'); ?>">First Time Login</a>
						<?php endif; ?>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		
	<?php endif; ?>
<?php elseif ($dashboard_banner_option == '1') : ?>
	<?php if  ($this->Session->read('client.Client.header_links_disabled') != '1') :  ?>
		<div style="background-image:url(/files/newimg/topmenubg.png); background-repeat:repeat-x; height:50px" class="header_color">
			<div style="padding-right:171px;padding-top:25px;float:right">
				<div style="height:25px;padding-left:5px;padding-right:5px" class="header_tab_color rounded-corners" id="header_top">
					<table>
						<tr id="header_top" height="24px">
							<td style="white-space:nowrap;text-align:right;" class="header_tab_text_color">
								<?php if  ($this->Session->check('user')) : ?>
								<b style="padding-right:30px">Welcome back <?php echo $this->Session->read('user.User.first_name'). ' '. $this->Session->read('user.User.last_name'); ?></b>   
								
								<?php if(in_array('1', $this->Session->read('dashboard'))) : ?>
									<?php if($this->Session->read('dashboardidinsession') == 1) :?>
										<?php if(($this->Session->read('client.Client.cart_enabled') == 1) && (count($this->Session->read('cart_products_count')) > 0)) : ?>
											<a href="<?php echo $this->Html->url('/products/shop_now/'); ?>">Shop Now</a> |
										<?php endif;?>
									<?php endif;?>
								<?php endif;?>
							
								<?php if(in_array('1', $this->Session->read('dashboard'))) : ?>
								<?php if(($this->Session->read('dashboardidinsession') != 1)) : ?>
										<?php echo $this->Html->link('My Rewards','/myrewards');?> |
								<?php endif;?>
								<?php endif;?>

								<?php if((in_array('2', $this->Session->read('dashboard')))) : ?>
								<?php if(($this->Session->read('dashboardidinsession') != 2)) : ?>
										<?php echo $this->Html->link('Send A Friend','/send_a_friend');?> |
								<?php endif;?>
								<?php endif;?>

								<?php if((in_array('3', $this->Session->read('dashboard')))) : ?>
								<?php if(($this->Session->read('dashboardidinsession') != 3)) : ?>
									<?php echo $this->Html->link('My Points','/mypoints');?> |
								<?php endif;?>
								<?php endif;?>
							
								<?php if(in_array($this->Session->read('dashboardidinsession'),$this->Session->read('campaign_dashboard')) && $this->Session->read('competitions')!= null) :?>
									<?php echo $this->Html->link('Competitions',array('controller'=>'campaigns','action' => 'details'));?> |
								<?php endif;?>
								
								<?php if(($this->Session->read('dashboardidinsession') == 3)) : ?>
									<?php if(($this->Session->read('client.Client.cart_enabled') == 1) && (count($this->Session->read('cart_products_points')) > 0)) : ?>
										<a href="<?php echo $this->Html->url('/carts/show_cart/'); ?>">My Cart</a> |
									<?php endif;?>
								<?php endif;?>

								<?php if(($this->Session->read('dashboardidinsession') == 1)) : ?>
									<?php if(($this->Session->read('client.Client.cart_enabled') == 1) && (count($this->Session->read('cart_products_count')) > 0)) : ?>
										<a href="<?php echo $this->Html->url('/carts/show_cart/'); ?>">My Cart</a> |
									<?php endif;?>
								<?php endif;?>

								<a href="<?php echo $this->Html->url('/products/view_wishlist/'); ?>">My Favourites</a> |
								<?php $pages = $this->Tree->allowed_pages(array('/users/edit'), $this->Session->read('client.Page')); ?>
								
								<?php if  (($this->Session->read('client.Client.id') != '965') && ($this->Session->read('client.Client.id') != '1618')):  ?>
									<?php echo $this->Html->link('My Account','javascript:void(0)',array('onclick' => "editDetails()"));?> |
								<?php endif; ?>
								
								<?php if($this->Session->read('client.Client.program_id') != 7) : ?>
									<a href="<?php echo $this->Html->url('/users/logout/'); ?>">Sign Out</a>
								<?php endif; ?>
								
								<?php if($this->Session->read('client.Client.program_id') == 7 && $this->Session->read('client.Client.link_enabled') == 1 ) : ?>
									<a href="<?php echo $this->Session->read('client.Client.link');?>" >
										<?php echo $this->Session->read('client.Client.link_text');?>
									</a>
								<?php endif; ?>
								
								<?php if ($this->Session->read('user.User.type') == 'Administrator' || $this->Session->read('user.User.type') == 'Power User' || 
									$this->Session->read('user.User.type') == 'Client Admin' || $this->Session->read('user.User.type') == 'Country Admin') : ?>
								| <a href="<?php echo $this->Html->url('/admin/'); ?>">Admin</a>
								<?php endif; ?>
							<?php else : ?>
								<?php echo $this->Html->link('Login','javascript:void(0)',array('onclick' => "showLogin()"));?> |
								<!--<?php echo $this->Html->link('First Time Login','javascript:void(0)',array('onclick' => "showFirstTimeLogin()"));?> -->
								<a href="<?php echo $this->Html->url('/users/first_time_login/'); ?>">First Time Login</a>
								<?php endif; ?>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	<?php endif; ?>
<?php endif; ?>