<style>
p {margin:0; padding: 0;}
</style>
<?php if(($module_data['stay_touch']['ClientPage']['enabled'] == 1 || $module_data['stay_touch']['ClientPage']['enabled_override'] == 1) && ($module_data['stay_touch']['ClientPage']['page_id'] == 60)): ?>
	<?php if (isset($module_data['stay_touch']['ClientPage']['content'])) : ?>
		<div class="tab_padding stay_touch" style="padding-left:0px !important;">
			<?php echo $module_data['stay_touch']['ClientPage']['title']; ?>
		</div>
		<?php echo ($module_data['stay_touch']['ClientPage']['content']); ?>
	<?php endif; ?>
<?php endif;?>