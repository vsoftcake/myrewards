
<script language="javascript" type="text/javascript">
<?php if($preset = "basic")
{
	$options = '
	mode : "textareas",
	theme : "advanced",
	convert_urls : false,
	remove_script_host : false,
	editor_deselector : "mceNoEditor",
	plugins : "autolink,lists,pagebreak,style,layer,table,advhr,advimage,advlink,fullscreen,advlist,autosave,paste,spellchecker,iespell",
	theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,fontselect,fontsizeselect,|,bullist,numlist",
	theme_advanced_buttons2 : "outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,forecolor,backcolor,|,hr,fullscreen,spellchecker,iespell",
	theme_advanced_buttons3 : "pastetext,pasteword,selectall",
	valid_children : "+body[style]",
	paste_auto_cleanup_on_paste : true,
	paste_preprocess : function(pl, o) {
		// Content string containing the HTML from the clipboard
		o.content =  o.content;
	},
	paste_postprocess : function(pl, o) {
		// Content DOM node containing the DOM structure of the clipboard
		o.node.innerHTML = o.node.innerHTML ;
	},
	theme_advanced_toolbar_location : "top",
	theme_advanced_toolbar_align : "left",
	theme_advanced_path_location : "bottom",
	theme_advanced_resizing : true,
	extended_valid_elements : "a[name|href|target|title|onclick],img[usemap|class|src|border=0|alt|title|hspace|vspace|width|height|align|onmouseover|onmouseout|name|style],hr[class|width|size|noshade],font[face|size|color|style],span[class|align|style],map[id|name],area[shape|alt|coords|href|target|onclick],script[charset|defer|language|src|type]",
	content_css : "/css/'.$this->layout.'.css" ,
	init_instance_callback  : "enableSpellcheck"
	';
}
?>

tinyMCE.init({<?php echo($options); ?>});
function enableSpellcheck(inst)
{
	var val = navigator.userAgent.toLowerCase();
	if(val.indexOf("msie") < 0)
		tinyMCE.execCommand("mceSpellCheck",false,inst.id);
	//Form.focusFirstElement(document.forms[0]);
}
</script>