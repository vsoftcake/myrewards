"<table bgcolor='white' style='font-family:"+font_style+";width:985px'>"+
	"<tr>"+
		"<td>"+
			"<div style='width:974px'>"+
				"<div style='padding:18px;font-size:15px;background:"+background+" #"+tab_background_color+";' class='tm rounded-corners menup_color'>"+
					"<ul id='menup'>"+
						"<li><a href='/' style='font-family:"+menu_font_style+";color:#"+tab_menu_text_color+"'>Dashboard</a></li>"+
					"</ul>"+
					"<ul id='menup'>"+
						"<li><a href='/display/member_services' style='font-family:"+menu_font_style+";color:#"+tab_menu_text_color+"'>How it Works</a></li>"+
						"<li><a href='/display/savings_calculator' style='font-family:"+menu_font_style+";color:#"+tab_menu_text_color+"'>Savings Calculator</a></li>"+
						"<li><a href='/display/whats_new' style='font-family:"+menu_font_style+";color:#"+tab_menu_text_color+"'>What's New</a>"+
							"<ul id='menup' style='background:#"+tab_background_color+"'>"+
								"<li><a style='font-family:"+menu_font_style+";color:#"+tab_menu_text_color+"' href='/display/special_offers'>Special Offers</a></li>"+
							"</ul>"+
						"</li>"+
						"<li><a style='font-family:"+menu_font_style+";color:#"+tab_menu_text_color+"' href='/display/my_membership'>My Membership</a>"+
							"<ul id='menup' style='background:#"+tab_background_color+"'>"+
								"<li><a style='font-family:"+menu_font_style+";color:#"+tab_menu_text_color+"' href='/users/edit'>Change Details</a></li>"+
								"<li><a style='font-family:"+menu_font_style+";color:#"+tab_menu_text_color+"' href='/display/nominate_a_friend'>Nominate a Friend</a></li>"+
								"<li><a style='font-family:"+menu_font_style+";color:#"+tab_menu_text_color+"' href='/contacts/feedback'>Feedback</a></li>"+
							"</ul>"+
						"</li>"+
						"<li><a style='font-family:"+menu_font_style+";color:#"+tab_menu_text_color+"' href='/display/customer_service'>Customer Service</a>"+
							"<ul id='menup' style='background:#"+tab_background_color+"'>"+
								"<li><a style='font-family:"+menu_font_style+";color:#"+tab_menu_text_color+"' href='/display/terms_and_conditions'>Terms &amp; Conditions</a></li>"+
								"<li><a style='font-family:"+menu_font_style+";color:#"+tab_menu_text_color+"' href='/display/fair_use_policy'>Fair Use Policy</a></li>"+
								"<li><a style='font-family:"+menu_font_style+";color:#"+tab_menu_text_color+"' href='/display/member_faq'>FAQS</a></li>"+
								"<li><a style='font-family:"+menu_font_style+";color:#"+tab_menu_text_color+"' href='/display/privacy_policy'>Privacy Policy</a></li>"+
								"<li><a style='font-family:"+menu_font_style+";color:#"+tab_menu_text_color+"' href='/contacts/contact_us'>Contact Us</a></li>"+
							"</ul>"+
						"</li>"+
					"</ul>"+	
					"<ul id='menup'>"+
						"<li>"+
							"<a style='font-family:"+menu_font_style+";color:#"+tab_menu_text_color+"' href='#'>Advanced Search</a>"+
						"</li>"+ 
					"</ul>"+
				"</div>"+
			"</div>"+
			"<div style='width:972px;padding-top:10px;padding-bottom:10px;border-top:1px solid #"+border_color+";border-right:1px solid #"+border_color+";border-left:1px solid #"+border_color+";background-color:#"+tab_content_background_color+";font-size:15px;font-family:arial;'>"+
				"<table style='font-family:"+font_style+";'>"+
					"<tr>"+
  						"<td style='padding-bottom:5px;font-size:12px;'>"+
	        				"<table>"+
								"<tr>"+
  									"<td style='padding-bottom:5px;padding-left:20px;'>Country</td>"+
	        						"<td><input style='border:1px solid #"+border_color+";width:150px;height:20px;' type='text' /></td>"+
	        					"<tr>"+
	        					"<tr>"+
  									"<td style='padding-bottom:5px;padding-left:20px;'>Category</td>"+
	        						"<td>"+
	        							"<select style='width:165px;height:25px;border:1px solid #"+border_color+";'>"+
											"<option value=''>  </option>"+
										"</select>"+
									"</td>"+
	        					"<tr>"+
	        				"</table>"+
	        			"</td>"+
        				"<td style='padding-bottom:5px;font-size:13px;'>"+
        					"<table>"+
								"<tr>"+
  									"<td style='padding-bottom:5px;padding-left:20px;'>State</td>"+
	        						"<td>"+
	        							"<select style='width:165px;height:25px;border:1px solid #"+border_color+";'>"+
											"<option value=''>  </option>"+
										"</select>"+
	        						"</td>"+
	        					"<tr>"+
	        					"<tr>"+
  									"<td style='padding-bottom:5px;padding-left:20px;'>Keyword/s</td>"+
	        						"<td><input style='border:1px solid #"+border_color+";width:150px;height:20px;' type='text' /></td>"+
	        					"<tr>"+
	        				"</table>"+
        				"</td>"+
        				"<td style='padding-bottom:5px;font-size:13px;'>"+
        					"<table>"+
        						"<tr>"+
  									"<td style='padding-bottom:5px;padding-left:20px;'>City/Suburb/Postcode</td>"+
	        						"<td><input style='border:1px solid #"+border_color+";width:150px;height:20px;' type='text' /></td>"+
	        					"<tr>"+
								"<tr>"+
  									"<td style='padding-bottom:5px;padding-left:20px;'>Radius Search</td>"+
	        						"<td>"+
	        							"<select style='width:120px;height:25px;border:1px solid #"+border_color+";'>"+
											"<option value=''>  </option>"+
										"</select>"+
	        						"approx </td>"+
	        					"<tr>"+
	        				"</table>"+
        				"</td>"+
        				"<td style='padding-left:20px;'>"+
    						"<button type='submit' class='rounded-corners_button' style='height:40px;width:65px;font-size:16px;margin-top:10px;font-weight:bold;color:#"+button_text_color+";border:#"+button_border_color+" 1px solid;background-color:#"+button_background_color+"'>Go !</button>"+
    						"<button onclick='resetdata()' class='rounded_corners_button' style='padding:3px;margin-left:10px;color:#"+button_text_color+";border:#"+button_border_color+" 1px solid;background-color:#"+button_background_color+"' type='button'>Clear</button>"+
    					"</td>"+
					"</tr>"+
				 "</table>"+
			"</div>"+
		"</td>"+
	"</tr>"+
"</table>"+

"<table padding='6' bgcolor='white' style='font-family:"+font_style+";width:985px'>"+
	"<tr>"+
		"<td valign='top'>"+                      
			"<div class='rounded-corners_bottom' style='border:1px solid #"+border_color+";width:972px;min-height:490px;overflow:hidden;background-color:#"+tab_content_background_color+";float:left;margin-right:5px;'>"+
				"<table width='100%'>"+
					"<tr>"+
						"<td>"+
							"<div style='float:left;margin:1em 1em 0em;'>"+
								"Order by"+
								"<select id='ProductSort' name='data[Product][sort]'>"+
									"<option selected='selected' value='default'>Default</option>"+
									"<option value='name'>Name</option>"+
									"<option value='suburb'>Suburb</option>"+
									"<option value='latest'>Latest Offers</option>"+
									"<option value='state'>By State</option>"+
									"</select>"+												
							"</div>"+
							"<div style='float:right;width:420px;'>"+
								"<div style='line-height: 35px; float: right; margin: 0.5em 0.5em 1em;'>"+
									"<span style='padding:2px 8px 2px 8px;font:11px arial;border:1px solid #"+button_border_color+";color: #"+button_text_color+" !important;background-color: #"+button_background_color+";'>1</span>"+
									"&nbsp;"+
									"<span>"+
										"<a style='padding:2px 8px 2px 8px;font:11px arial;border:1px solid #"+button_border_color+";color: #"+button_background_color+" !important;background-color: #"+button_text_color+";' href=''>2</a>"+
									"</span>"+
									"&nbsp;"+
									"<span>"+
										"<a style='padding:2px 8px 2px 8px;font:11px arial;border:1px solid #"+button_border_color+";color: #"+button_background_color+" !important;background-color: #"+button_text_color+";' href=''>3</a>"+
									"</span>"+
									"&nbsp;"+
									"<span>"+
										"<a style='padding:2px 8px 2px 8px;font:11px arial;border:1px solid #"+button_border_color+";color: #"+button_background_color+" !important;background-color: #"+button_text_color+";' href=''>4</a>"+
									"</span>"+
									"&nbsp;"+
									"<span>"+
										"<a style='padding:2px 8px 2px 8px;font:11px arial;border:1px solid #"+button_border_color+";color: #"+button_background_color+" !important;background-color: #"+button_text_color+";' href=''>5</a>"+
									"</span>"+
									"&nbsp;"+
									"<span>"+
										"<a style='padding:2px 8px 2px 8px;font:11px arial;border:1px solid #"+button_border_color+";color: #"+button_background_color+" !important;background-color: #"+button_text_color+";' href=''>6</a>"+
									"</span>"+
									"&nbsp;"+
									"<span>"+
										"<a style='padding:2px 8px 2px 8px;font:11px arial;border:1px solid #"+button_border_color+";color: #"+button_background_color+" !important;background-color: #"+button_text_color+";' href=''>7</a>"+
									"</span>"+
									"&nbsp;"+
									"<span>"+
										"<a style='padding:2px 8px 2px 8px;font:11px arial;border:1px solid #"+button_border_color+";color: #"+button_background_color+" !important;background-color: #"+button_text_color+";' href=''>8</a>"+
									"</span>"+
									"&nbsp;"+
									"<span>"+
										"<a style='padding:2px 8px 2px 8px;font:11px arial;border:1px solid #"+button_border_color+";color: #"+button_background_color+" !important;background-color: #"+button_text_color+";' href=''>9</a>"+
									"</span>"+
									"&nbsp;"+		
									"<a style='padding:2px 8px 2px 8px;font:11px arial;border:1px solid #"+button_border_color+";color: #"+button_background_color+" !important;background-color: #"+button_text_color+";' title='next' href=''>"+
										">"+
									"</a>"+		
									"&nbsp;"+										
									"<span>"+
										"<a style='padding:2px 8px 2px 8px;font:11px arial;border:1px solid #"+button_border_color+";color: #"+button_background_color+" !important;background-color: #"+button_text_color+";' title='last' href=''>"+
											">>"+
										"</a>"+
									"</span>&nbsp;"+
									"<select name='page'>"+
										"<option selected='selected' value='1'>1</option>"+
										"<option value='2'>2</option>"+
										"<option value='3'>3</option>"+
										"<option value='4'>4</option>"+
										"<option value='5'>5</option>"+
										"<option value='6'>6</option>"+
										"<option value='7'>7</option>"+
										"<option value='8'>8</option>"+
										"<option value='9'>9</option>"+
										"<option value='10'>10</option>"+
										"<option value='11'>11</option>"+
										"<option value='12'>12</option>"+
										"<option value='13'>13</option>"+
										"<option value='14'>14</option>"+
										"<option value='15'>15</option>"+
										"<option value='16'>16</option>"+
										"<option value='17'>17</option>"+
										"<option value='18'>18</option>"+
										"<option value='19'>19</option>"+
										"<option value='20'>20</option>"+
								 	"</select>"+
								"</div>"+
							"</div>"+
						"</td>"+
					"</tr>"+
					"<tr>"+
						"<td>"+
							"<div style='float:left;width:770px'>"+
								"<div style='padding-left:8px;'>"+
									"<div style='height:272px;width:254px;float:left;margin:0px;margin-left:0px;margin-bottom: 0px;margin-right:0px;'>"+
										"<div>"+
											"<div>"+
												"<table width='100%'>"+
													"<tr>"+
														"<td rowspan='2'>"+
															"<div class='module35'>"+
																"<div style=' height:195px; width:222px; vertical-align:top;text-align:center;'>"+
																	"<div style='height:128px;padding-top:20px;'>"+
																		"<img width='145' align='middle' height='110' alt='' src='/files/search_result_images/1000260.jpg' style=' padding-top:16px; padding-left:9px;'>"+
																	"</div>"+
									
																	"<div style='margin-top: 6px; margin-left: 8px; display: none;'> "+
																		"<a href='#'>"+
																			"<img alt='' src='/files/newimg/quick_view-1.png'>"+
																		"</a>"+								
																	"</div>"+
																"</div>"+
																"<div style='font-size:13px;display:block;padding-left:13px;width:294px;'>"+
																	"<a title='Metung Holiday Villas' style='color:#"+link_color+";font-weight:bold;text-decoration:none' href='/products/view/1000260'>"+
																		"HotelClub - FLASH SALE"+
																	"</a>"+
																"</div>"+
																"<div style='padding-left:13px;margin-top:5px;' class='highlight' title='New tooltip' id='text1'>"+
																	"Save up to 57% PLUS ... "+
																"</div>"+
															"</div>"+
														"</td>"+
													"</tr>"+
												"</table>"+
											"</div>"+
											"<div class='b rounded-corners_bottom'></div>"+
										"</div>"+
									"</div>"+
									"<div style='height:272px;width:254px;float:left;margin:0px;margin-left:0px;margin-bottom: 0px;margin-right:0px;'>"+
										"<div>"+
											"<div>"+
												"<table width='100%'>"+
													"<tr>"+
														"<td rowspan='2'>"+
															"<div class='module35'>"+
																"<div style=' height:195px; width:222px; vertical-align:top;text-align:center;'>"+
																	"<div style='height:128px;padding-top:20px;'>"+
																		"<img width='145' align='middle' height='110' alt='' src='/files/search_result_images/1024499.jpg' style=' padding-top:16px; padding-left:9px;'>"+
																	"</div>"+
									
																	"<div style='margin-top: 6px; margin-left: 8px; display: none;'> "+
																		"<a href='#'>"+
																			"<img alt='' src='/files/newimg/quick_view-1.png'>"+
																		"</a>"+								
																	"</div>"+
																"</div>"+
																"<div style='font-size:13px;display:block;padding-left:13px;width:294px;'>"+
																	"<a title='Metung Holiday Villas' style='color:#"+link_color+";font-weight:bold;text-decoration:none' href='/products/view/1024499'>"+
																		"Madame Tussauds Sydney"+
																	"</a>"+
																"</div>"+
																"<div style='padding-left:13px;margin-top:5px;' class='highlight' title='New tooltip' id='text1'>"+
																	"20% Discount "+
																"</div>"+
															"</div>"+
														"</td>"+
													"</tr>"+
												"</table>"+
											"</div>"+
											"<div class='b rounded-corners_bottom'></div>"+
										"</div>"+
									"</div>"+
									"<div style='height:272px;width:254px;float:left;margin:0px;margin-left:0px;margin-bottom: 0px;margin-right:0px;'>"+
										"<div>"+
											"<div>"+
												"<table width='100%'>"+
													"<tr>"+
														"<td rowspan='2'>"+
															"<div class='module35'>"+
																"<div style=' height:195px; width:222px; vertical-align:top;text-align:center;'>"+
																	"<div style='height:128px;padding-top:20px;'>"+
																		"<img width='145' align='middle' height='110' alt='' src='/files/search_result_images/1000390.gif' style=' padding-top:16px; padding-left:9px;'>"+
																	"</div>"+
									
																	"<div style='margin-top: 6px; margin-left: 8px; display: none;'> "+
																		"<a href='#'>"+
																			"<img alt='' src='/files/newimg/quick_view-1.png'>"+
																		"</a>"+								
																	"</div>"+
																"</div>"+
																"<div style='font-size:13px;display:block;padding-left:13px;width:294px;'>"+
																	"<a title='Metung Holiday Villas' style='color:#"+link_color+";font-weight:bold;text-decoration:none' href='/products/view/1000390'>"+
																		"ABS Auto Brakes Service"+
																	"</a>"+
																"</div>"+
																"<div style='padding-left:13px;margin-top:5px;' class='highlight' title='New tooltip' id='text1'>"+
																	"10% Discount"+
																"</div>"+
															"</div>"+
														"</td>"+
													"</tr>"+
												"</table>"+
											"</div>"+
											"<div class='b rounded-corners_bottom'></div>"+
										"</div>"+
									"</div>"+
									"<div style='height:272px;width:254px;float:left;margin:0px;margin-left:0px;margin-bottom: 0px;margin-right:0px;'>"+
										"<div>"+
											"<div>"+
												"<table width='100%'>"+
													"<tr>"+
														"<td rowspan='2'>"+
															"<div class='module35'>"+
																"<div style=' height:195px; width:222px; vertical-align:top;text-align:center;'>"+
																	"<div style='height:128px;padding-top:20px;'>"+
																		"<img width='145' align='middle' height='110' alt='' src='/files/search_result_images/1010789.gif' style=' padding-top:16px; padding-left:9px;'>"+
																	"</div>"+
									
																	"<div style='margin-top: 6px; margin-left: 8px; display: none;'> "+
																		"<a href='#'>"+
																			"<img alt='' src='/files/newimg/quick_view-1.png'>"+
																		"</a>"+								
																	"</div>"+
																"</div>"+
																"<div style='font-size:13px;display:block;padding-left:13px;width:294px;'>"+
																	"<a title='Metung Holiday Villas' style='color:#"+link_color+";font-weight:bold;text-decoration:none' href='/products/view/1010789'>"+
																		"Metung Holiday Villas"+
																	"</a>"+
																"</div>"+
																"<div style='padding-left:13px;margin-top:5px;' class='highlight' title='New tooltip' id='text1'>"+
																	"Save over $500!"+
																"</div>"+
															"</div>"+
														"</td>"+
													"</tr>"+
												"</table>"+
											"</div>"+
											"<div class='b rounded-corners_bottom'></div>"+
										"</div>"+
									"</div>"+
									"<div style='height:272px;width:254px;float:left;margin:0px;margin-left:0px;margin-bottom: 0px;margin-right:0px;'>"+
										"<div>"+
											"<div>"+
												"<table width='100%'>"+
													"<tr>"+
														"<td rowspan='2'>"+
															"<div class='module35'>"+
																"<div style=' height:195px; width:222px; vertical-align:top;text-align:center;'>"+
																	"<div style='height:128px;padding-top:20px;'>"+
																		"<img width='145' align='middle' height='110' alt='' src='/files/search_result_images/1015565.jpg'                                                      style=' padding-top:16px; padding-left:9px;'>"+
																	"</div>"+
									
																	"<div style='margin-top: 6px; margin-left: 8px; display: none;'> "+
																		"<a href='#'>"+
																			"<img alt='' src='/files/newimg/quick_view-1.png'>"+
																		"</a>"+								
																	"</div>"+
																"</div>"+
																"<div style='font-size:13px;display:block;padding-left:13px;width:294px;'>"+
																	"<a title='' style='color:#"+link_color+";font-weight:bold;text-decoration:none' href='/products/view/1015565'>"+
																		"Absolute Q Signature, Koh..."+
																	"</a>"+
																"</div>"+
																"<div style='padding-left:13px;margin-top:5px;' class='highlight' title='New tooltip' id='text1'>"+
																	"25% off on booking"+
																"</div>"+
															"</div>"+
														"</td>"+
													"</tr>"+
												"</table>"+
											"</div>"+
											"<div class='b rounded-corners_bottom'></div>"+
										"</div>"+
									"</div>"+
									"<div style='height:272px;width:254px;float:left;margin:0px;margin-left:0px;margin-bottom: 0px;margin-right:0px;'>"+
										"<div>"+
											"<div>"+
												"<table width='100%'>"+
													"<tr>"+
														"<td rowspan='2'>"+
															"<div class='module35'>"+
																"<div style=' height:195px; width:222px; vertical-align:top;text-align:center;'>"+
																	"<div style='height:128px;padding-top:20px;'>"+
																		"<img width='145' align='middle' height='110' alt='' src='/files/search_result_images/1023143.jpg'                                                      style=' padding-top:16px; padding-left:9px;'>"+
																	"</div>"+
									
																	"<div style='margin-top: 6px; margin-left: 8px; display: none;'> "+
																		"<a href='#'>"+
																			"<img alt='' src='/files/newimg/quick_view-1.png'>"+
																		"</a>"+								
																	"</div>"+
																"</div>"+
																"<div style='font-size:13px;display:block;padding-left:13px;width:294px;'>"+
																	"<a title='' style='color:#"+link_color+";font-weight:bold;text-decoration:none' href='/products/view/1023143'>"+
																		"Adelaide City Park Motel"+
																	"</a>"+
																"</div>"+
																"<div style='padding-left:13px;margin-top:5px;' class='highlight' title='New tooltip' id='text1'>"+
																	"Up to 50% Discount"+
																"</div>"+
															"</div>"+
														"</td>"+
													"</tr>"+
												"</table>"+
											"</div>"+
											"<div class='b rounded-corners_bottom'></div>"+
										"</div>"+
									"</div>"+
									"<div style='height:272px;width:254px;float:left;margin:0px;margin-left:0px;margin-bottom: 0px;margin-right:0px;'>"+
										"<div>"+
											"<div>"+
												"<table width='100%'>"+
													"<tr>"+
														"<td rowspan='2'>"+
															"<div class='module35'>"+
																"<div style=' height:195px; width:222px; vertical-align:top;text-align:center;'>"+
																	"<div style='height:128px;padding-top:20px;'>"+
																		"<img width='145' align='middle' height='110' alt='' src='/files/search_result_images/1022778.jpg'                                                      style=' padding-top:16px; padding-left:9px;'>"+
																	"</div>"+
									
																	"<div style='margin-top: 6px; margin-left: 8px; display: none;'> "+
																		"<a href='#'>"+
																			"<img alt='' src='/files/newimg/quick_view-1.png'>"+
																		"</a>"+								
																	"</div>"+
																"</div>"+
																"<div style='font-size:13px;display:block;padding-left:13px;width:294px;'>"+
																	"<a title='' style='color:#"+link_color+";font-weight:bold;text-decoration:none' href='/products/view/1022778'>"+
																		"Alaturka Cuisine"+
																	"</a>"+
																"</div>"+
																"<div style='padding-left:13px;margin-top:5px;' class='highlight' title='New tooltip' id='text1'>"+
																	"25% Discount"+
																"</div>"+
															"</div>"+
														"</td>"+
													"</tr>"+
												"</table>"+
											"</div>"+
											"<div class='b rounded-corners_bottom'></div>"+
										"</div>"+
									"</div>"+
									"<div style='height:272px;width:254px;float:left;margin:0px;margin-left:0px;margin-bottom: 0px;margin-right:0px;'>"+
										"<div>"+
											"<div>"+
												"<table width='100%'>"+
													"<tr>"+
														"<td rowspan='2'>"+
															"<div class='module35'>"+
																"<div style=' height:195px; width:222px; vertical-align:top;text-align:center;'>"+
																	"<div style='height:128px;padding-top:20px;'>"+
																		"<img width='145' align='middle' height='110' alt='' src='/files/search_result_images/1012154.jpg'                                                      style=' padding-top:16px; padding-left:9px;'>"+
																	"</div>"+
									
																	"<div style='margin-top: 6px; margin-left: 8px; display: none;'> "+
																		"<a href='#'>"+
																			"<img alt='' src='/files/newimg/quick_view-1.png'>"+
																		"</a>"+								
																	"</div>"+
																"</div>"+
																"<div style='font-size:13px;display:block;padding-left:13px;width:294px;'>"+
																	"<a title='' style='color:#"+link_color+";font-weight:bold;text-decoration:none' href='/products/view/1012154'>"+
																		"Beaubelle Beauty Clinic "+
																	"</a>"+
																"</div>"+
																"<div style='padding-left:13px;margin-top:5px;' class='highlight' title='New tooltip' id='text1'>"+
																	"50% Discount"+
																"</div>"+
															"</div>"+
														"</td>"+
													"</tr>"+
												"</table>"+
											"</div>"+
											"<div class='b rounded-corners_bottom'></div>"+
										"</div>"+
									"</div>"+
									"<div style='height:272px;width:254px;float:left;margin:0px;margin-left:0px;margin-bottom: 0px;margin-right:0px;'>"+
										"<div>"+
											"<div>"+
												"<table width='100%'>"+
													"<tr>"+
														"<td rowspan='2'>"+
															"<div class='module35'>"+
																"<div style=' height:195px; width:222px; vertical-align:top;text-align:center;'>"+
																	"<div style='height:128px;padding-top:20px;'>"+
																		"<img width='145' align='middle' height='110' alt='' src='/files/search_result_images/1009979.gif'                                                      style=' padding-top:16px; padding-left:9px;'>"+
																	"</div>"+
									
																	"<div style='margin-top: 6px; margin-left: 8px; display: none;'> "+
																		"<a href='#'>"+
																			"<img alt='' src='/files/newimg/quick_view-1.png'>"+
																		"</a>"+								
																	"</div>"+
																"</div>"+
																"<div style='font-size:13px;display:block;padding-left:13px;width:294px;'>"+
																	"<a title='' style='color:#"+link_color+";font-weight:bold;text-decoration:none' href='/products/view/1009979'>"+
																		"Amante Jewellery"+
																	"</a>"+
																"</div>"+
																"<div style='padding-left:13px;margin-top:5px;' class='highlight' title='New tooltip' id='text1'>"+
																	"15% Discount"+
																"</div>"+
															"</div>"+
														"</td>"+
													"</tr>"+
												"</table>"+
											"</div>"+
											"<div class='b rounded-corners_bottom'></div>"+
										"</div>"+
									"</div>"+
								"</div>"+
							"</div>"+
							"<div style='float:left;padding-right:14px;text-align:center;width:160px;'>"+
								"<a href='http://www.myrewards.com.au/products/view/1016326'>"+
									"<img width='120' height='600' alt='' src='/files/search_result_images/25.jpg'>"+
								"</a>"+
							"</div>"+
						"</td>"+
					"</tr>"+
					"<tr>"+
						"<td>"+
							"<div style='float:right;width:420px;'>"+
								"<div style='line-height: 35px; float: right; margin: 0.5em 0.5em 1em;'>"+
									"<span style='padding:2px 8px 2px 8px;font:11px arial;border:1px solid #"+button_border_color+";color: #"+button_text_color+" !important;background-color: #"+button_background_color+";'>1</span>"+
									"&nbsp;"+
									"<span>"+
										"<a style='padding:2px 8px 2px 8px;font:11px arial;border:1px solid #"+button_border_color+";color: #"+button_background_color+" !important;background-color: #"+button_text_color+";' href=''>2</a>"+
									"</span>"+
									"&nbsp;"+
									"<span>"+
										"<a style='padding:2px 8px 2px 8px;font:11px arial;border:1px solid #"+button_border_color+";color: #"+button_background_color+" !important;background-color: #"+button_text_color+";' href=''>3</a>"+
									"</span>"+
									"&nbsp;"+
									"<span>"+
										"<a style='padding:2px 8px 2px 8px;font:11px arial;border:1px solid #"+button_border_color+";color: #"+button_background_color+" !important;background-color: #"+button_text_color+";' href=''>4</a>"+
									"</span>"+
									"&nbsp;"+
									"<span>"+
										"<a style='padding:2px 8px 2px 8px;font:11px arial;border:1px solid #"+button_border_color+";color: #"+button_background_color+" !important;background-color: #"+button_text_color+";' href=''>5</a>"+
									"</span>"+
									"&nbsp;"+
									"<span>"+
										"<a style='padding:2px 8px 2px 8px;font:11px arial;border:1px solid #"+button_border_color+";color: #"+button_background_color+" !important;background-color: #"+button_text_color+";' href=''>6</a>"+
									"</span>"+
									"&nbsp;"+
									"<span>"+
										"<a style='padding:2px 8px 2px 8px;font:11px arial;border:1px solid #"+button_border_color+";color: #"+button_background_color+" !important;background-color: #"+button_text_color+";' href=''>7</a>"+
									"</span>"+
									"&nbsp;"+
									"<span>"+
										"<a style='padding:2px 8px 2px 8px;font:11px arial;border:1px solid #"+button_border_color+";color: #"+button_background_color+" !important;background-color: #"+button_text_color+";' href=''>8</a>"+
									"</span>"+
									"&nbsp;"+
									"<span>"+
										"<a style='padding:2px 8px 2px 8px;font:11px arial;border:1px solid #"+button_border_color+";color: #"+button_background_color+" !important;background-color: #"+button_text_color+";' href=''>9</a>"+
									"</span>"+
									"&nbsp;"+		
									"<a style='padding:2px 8px 2px 8px;font:11px arial;border:1px solid #"+button_border_color+";color: #"+button_background_color+" !important;background-color: #"+button_text_color+";' title='next' href=''>"+
										">"+
									"</a>"+		
									"&nbsp;"+										
									"<span>"+
										"<a style='padding:2px 8px 2px 8px;font:11px arial;border:1px solid #"+button_border_color+";color: #"+button_background_color+" !important;background-color: #"+button_text_color+";' title='last' href=''>"+
											">>"+
										"</a>"+
									"</span>&nbsp;"+
									"<select name='page'>"+
										"<option selected='selected' value='1'>1</option>"+
										"<option value='2'>2</option>"+
										"<option value='3'>3</option>"+
										"<option value='4'>4</option>"+
										"<option value='5'>5</option>"+
										"<option value='6'>6</option>"+
										"<option value='7'>7</option>"+
										"<option value='8'>8</option>"+
										"<option value='9'>9</option>"+
										"<option value='10'>10</option>"+
										"<option value='11'>11</option>"+
										"<option value='12'>12</option>"+
										"<option value='13'>13</option>"+
										"<option value='14'>14</option>"+
										"<option value='15'>15</option>"+
										"<option value='16'>16</option>"+
										"<option value='17'>17</option>"+
										"<option value='18'>18</option>"+
										"<option value='19'>19</option>"+
										"<option value='20'>20</option>"+
								 	"</select>"+
								"</div>"+
							"</div>"+
						"</td>"+
					"</tr>"+
				"</table>"+					
			"</div>"+
		"</td>"+
	"</tr>"+
"</table>"+
"<div style='height: 8px;background-color:white'></div>"+

"<div style='height:8px;background-color:white'></div>"+
"<table bgcolor='white' style='font-family:"+font_style+";width:985px'>"+
	"<tr>"+
		"<td colspan='2'>"+
			"<div style='font-family:"+font_style+";width:1005px;margin-top:0px;margin-left:-10px;background-color:#"+footer_background_color+"'>"+
				"<div style='padding-top: 10px; padding-left: 10px;height:120px;'>"+
					"<table style='width:986px'>"+
						"<tr>"+
							"<td valign='top'>"+
								"<ul id='menuf'>"+
									"<li><a style='color:#"+footer_text_color+";text-transform:uppercase;' href='/'><b>Dashboard</b></a></li>"+
								"</ul>"+
								"<ul id='menuf'>"+
									"<li><a style='color:#"+footer_text_color+";' href='/display/how_it_works'><b>HOW IT WORKS</b></a></li>"+
									"<li><a style='color:#"+footer_text_color+";' href='/display/savings_calculator'><b>SAVINGS CALCULATOR</b></a></li>"+
									"<li><a style='color:#"+footer_text_color+";' href='/display/whats_new'><b>WHAT'S NEW</b></a>"+
										"<ul id='menuf'>"+
											"<li><a style='color:#"+footer_text_color+";' href='/display/special_offers'>Hot Offers</a></li>"+
											"<li><a style='color:#"+footer_text_color+";' href='/display/news'>Online Coupon</a></li>"+
										"</ul>"+
									"</li>"+
									"<li><a style='color:#"+footer_text_color+";' href='/display/my_membership'><b>MY MEMBERSHIP</b></a>"+
										"<ul id='menuf'>"+
											"<li><a style='color:#"+footer_text_color+";' href='/users/edit'>Change Details</a></li>"+
											"<li><a style='color:#"+footer_text_color+";' href='/display/nominate_a_friend'>Invite My Family </a></li>"+
										"</ul>"+
									"</li>"+
									"<li><a style='color:#"+footer_text_color+";' href='/display/customer_service'><b>CUSTOMER SERVICE</b></a>"+
										"<ul id='menuf'>"+
											"<li><a style='color:#"+footer_text_color+";' href='/display/terms_and_conditions'>Terms &amp; Conditions</a></li>"+
											"<li><a style='color:#"+footer_text_color+";' href='/display/fair_use_policy'>Fair Use Policy</a></li>"+
											"<li><a style='color:#"+footer_text_color+";' href='/display/member_faq'>FAQs</a></li>"+
											"<li><a style='color:#"+footer_text_color+";' href='/display/privacy_policy'>Privacy Policy</a></li>"+
											"<li><a style='color:#"+footer_text_color+";' href='/contacts/contact_us'>Contact Us</a></li>"+
										"</ul>"+
									"</li>"+
								"</ul>"+							
							"</td>"+
							"<td style='text-align:center;width:230px;padding-top:35px;'>"+
								"<img width='' height='' alt='' src='/blank.png'>"+
							"</td>"+
						"</tr>"+
					"</table>"+
				"</div>"+
				"<br>"+
				"<div style='padding-left:30px;padding-bottom:8px;color:#"+footer_text_color+";'>"+
					"&copy; My Rewards International  Ltd 2012 All Rights Reserved. <br>Follow us on &nbsp;"+									
					"<a href='http://www.facebook.com/pages/My-Rewards-International/133544976702355?v=wall' target='_blank' style='text-decoration:none;border:none !important;'>"+
						"<img width='20' title='facebook' src='/files/social_icons/fb.png' style='vertical-align:middle'>"+
					"</a>"+
					"&nbsp;&nbsp;"+
					"<a href='http://twitter.com/#!/MyRewardsIntl' target='_blank' style='text-decoration:none;border:none !important;'>"+
						"<img width='20' title='twitter' src='/files/social_icons/twitter.png' style='vertical-align:middle'>"+
					"</a>"+
				"</div>"+
			"</div>"
		"</td>"+
	"</tr>"+
"</table>"


