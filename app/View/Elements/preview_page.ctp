"<table bgcolor='white' style='font-family:"+font_style+";width:985px'>"+
	"<tr>"+
		"<td>"+
			"<div style='width:974px'>"+
				"<div style='padding:18px;font-size:15px;background:"+background+" #"+tab_background_color+";' class='tm rounded-corners menup_color'>"+
					"<ul id='menup'>"+
						"<li><a href='/' style='font-family:"+menu_font_style+";color:#"+tab_menu_text_color+"'>Dashboard</a></li>"+
					"</ul>"+
					"<ul id='menup'>"+
						"<li><a href='/display/member_services' style='font-family:"+menu_font_style+";color:#"+tab_menu_text_color+"'>How it Works</a></li>"+
						"<li><a href='/display/savings_calculator' style='font-family:"+menu_font_style+";color:#"+tab_menu_text_color+"'>Savings Calculator</a></li>"+
						"<li><a href='/display/whats_new' style='font-family:"+menu_font_style+";color:#"+tab_menu_text_color+"'>What's New</a>"+
							"<ul id='menup' style='background:#"+tab_background_color+"'>"+
								"<li><a style='font-family:"+menu_font_style+";color:#"+tab_menu_text_color+"' href='/display/special_offers'>Special Offers</a></li>"+
							"</ul>"+
						"</li>"+
						"<li><a style='font-family:"+menu_font_style+";color:#"+tab_menu_text_color+"' href='/display/my_membership'>My Membership</a>"+
							"<ul id='menup' style='background:#"+tab_background_color+"'>"+
								"<li><a style='font-family:"+menu_font_style+";color:#"+tab_menu_text_color+"' href='/users/edit'>Change Details</a></li>"+
								"<li><a style='font-family:"+menu_font_style+";color:#"+tab_menu_text_color+"' href='/display/nominate_a_friend'>Nominate a Friend</a></li>"+
								"<li><a style='font-family:"+menu_font_style+";color:#"+tab_menu_text_color+"' href='/contacts/feedback'>Feedback</a></li>"+
							"</ul>"+
						"</li>"+
						"<li><a style='font-family:"+menu_font_style+";color:#"+tab_menu_text_color+"' href='/display/customer_service'>Customer Service</a>"+
							"<ul id='menup' style='background:#"+tab_background_color+"'>"+
								"<li><a style='font-family:"+menu_font_style+";color:#"+tab_menu_text_color+"' href='/display/terms_and_conditions'>Terms &amp; Conditions</a></li>"+
								"<li><a style='font-family:"+menu_font_style+";color:#"+tab_menu_text_color+"' href='/display/fair_use_policy'>Fair Use Policy</a></li>"+
								"<li><a style='font-family:"+menu_font_style+";color:#"+tab_menu_text_color+"' href='/display/member_faq'>FAQS</a></li>"+
								"<li><a style='font-family:"+menu_font_style+";color:#"+tab_menu_text_color+"' href='/display/privacy_policy'>Privacy Policy</a></li>"+
								"<li><a style='font-family:"+menu_font_style+";color:#"+tab_menu_text_color+"' href='/contacts/contact_us'>Contact Us</a></li>"+
							"</ul>"+
						"</li>"+
					"</ul>"+	
					"<ul id='menup'>"+
						"<li>"+
							"<a style='font-family:"+menu_font_style+";color:#"+tab_menu_text_color+"' href='#'>Advanced Search</a>"+
						"</li>"+ 
					"</ul>"+
				"</div>"+
			"</div>"+
			"<div style='width:972px;padding-top:10px;padding-bottom:10px;border-top:1px solid #"+border_color+";border-right:1px solid #"+border_color+";border-left:1px solid #"+border_color+";background-color:#"+tab_content_background_color+";font-size:15px;font-family:arial;'>"+
				"<table style='font-family:"+font_style+";'>"+
					"<tr>"+
  						"<td style='padding-bottom:5px;font-size:12px;'>"+
	        				"<table>"+
								"<tr>"+
  									"<td style='padding-bottom:5px;padding-left:20px;'>Country</td>"+
	        						"<td><input style='border:1px solid #"+border_color+";width:150px;height:20px;' type='text' /></td>"+
	        					"<tr>"+
	        					"<tr>"+
  									"<td style='padding-bottom:5px;padding-left:20px;'>Category</td>"+
	        						"<td>"+
	        							"<select style='width:165px;height:25px;border:1px solid #"+border_color+";'>"+
											"<option value=''>  </option>"+
										"</select>"+
									"</td>"+
	        					"<tr>"+
	        				"</table>"+
	        			"</td>"+
        				"<td style='padding-bottom:5px;font-size:13px;'>"+
        					"<table>"+
								"<tr>"+
  									"<td style='padding-bottom:5px;padding-left:20px;'>State</td>"+
	        						"<td>"+
	        							"<select style='width:165px;height:25px;border:1px solid #"+border_color+";'>"+
											"<option value=''>  </option>"+
										"</select>"+
	        						"</td>"+
	        					"<tr>"+
	        					"<tr>"+
  									"<td style='padding-bottom:5px;padding-left:20px;'>Keyword/s</td>"+
	        						"<td><input style='border:1px solid #"+border_color+";width:150px;height:20px;' type='text' /></td>"+
	        					"<tr>"+
	        				"</table>"+
        				"</td>"+
        				"<td style='padding-bottom:5px;font-size:13px;'>"+
        					"<table>"+
        						"<tr>"+
  									"<td style='padding-bottom:5px;padding-left:20px;'>City/Suburb/Postcode</td>"+
	        						"<td><input style='border:1px solid #"+border_color+";width:150px;height:20px;' type='text' /></td>"+
	        					"<tr>"+
								"<tr>"+
  									"<td style='padding-bottom:5px;padding-left:20px;'>Radius Search</td>"+
	        						"<td>"+
	        							"<select style='width:120px;height:25px;border:1px solid #"+border_color+";'>"+
											"<option value=''>  </option>"+
										"</select>"+
	        						"approx </td>"+
	        					"<tr>"+
	        				"</table>"+
        				"</td>"+
        				"<td style='padding-left:20px;'>"+
    						"<button type='submit' class='rounded-corners_button' style='height:40px;width:65px;font-size:16px;margin-top:10px;font-weight:bold;color:#"+button_text_color+";border:#"+button_border_color+" 1px solid;background-color:#"+button_background_color+"'>Go !</button>"+
    						"<button onclick='resetdata()' class='rounded_corners_button' style='padding:3px;margin-left:10px;color:#"+button_text_color+";border:#"+button_border_color+" 1px solid;background-color:#"+button_background_color+"' type='button'>Clear</button>"+
    					"</td>"+
					"</tr>"+
				 "</table>"+
			"</div>"+
		"</td>"+
	"</tr>"+
"</table>"+

"<table padding='6' bgcolor='white' style='font-family:"+font_style+";width:985px'>"+
	"<tr>"+
		"<td valign='top'>"+                      
			"<div class='rounded-corners_bottom' style='border:1px solid #"+border_color+";width:972px;height:490px;background-color:#"+tab_content_background_color+";float:left;margin-right:5px;'>"+
				"<div style='float:left'>"+
					"<div style='font-family:"+tab_font_style+";color:#"+tab_text_color+";padding-left:25px;padding-top:10px;font-size:13px;'><strong>Daily Deals</strong></div>"+
					"<table style='width:535px'>"+
						"<tr>"+
							"<td valign='top' style='padding-left:25px'>"+
								"<div style='border:2px solid #cccccc;width:535px;height:435px'>"+
									"<div style='border:2px solid #ffffff;width:530px;height:430px'>"+		
										"<img width='530' height='430px' src='/files/favourites/1012049.png'/>"+
									"</div>"+
								"</div>"+
							"</td>"+
						"</tr>"+
					"</table>"+
				"</div>"+
				"<div style='float: left; padding-right: 5px;padding-left: 5px; margin-top: 50px; height: 380px;'></div>"+
				"<div style='float:left;width:348px;'>"+
					"<div style='font-family:"+tab_font_style+";color:#"+tab_text_color+";padding-left:10px;padding-top:10px;font-size:13px;'><strong>What's Around You</strong></div>"+
						"<div style='height:350px;padding-left:10px;'>"+
							"<div style='border:2px solid #cccccc;height:275px;width:367px;padding-left:1px'>"+
								"<div style='border:2px solid #ffffff;height:268px;width:362px;margin-top:1px;'>"+
								"</div>"+
							"</div>"+
							"<div style='margin-top:5px;position:relative;height:20px;'></div>"+
							"<div style='font-family:"+font_style+";text-align:left;'> "+
								"<table>"+
									"<tr>"+
										"<td>"+
      										"<input type='text' name='maplocation' id='maplocation' style='height:22px;width:275px;border: 1px solid #"+border_color+"'>"+
      									"</td>"+
      									"<td>"+
      										"<button type='submit' style='border: 1px solid #"+button_border_color+";color:#"+button_text_color+";background-color:#"+button_background_color+";'  class='map_button_style rounded_corners_button'>Search</button>"+
      									"</td>"+
      								"</tr>"+
      							"</table>"+
    						"</div>"+
    						"<div style='padding-top:3px;'>"+
    							"<img align='middle' alt='' src='/files/newimg/tooltip/help16.gif' vertical-align='top'>"+
    							"<span style='padding-top:3px'>Hints to help you search.</span>"+
    						"</div>"+ 
						"</div>"+
						"<div style='font-family:"+tab_font_style+";color:#"+tab_text_color+";padding-left:10px;padding-top:10px;font-size:13px;padding-bottom:15px;'><strong>Stay In Touch</strong></div>"+
							"<div style='padding-left:15px'>"+
								"<img width='336' border='0' height='54' src='http://www.myrewards.com.au/img/myrewards/img/Social_media_icons.png'>"+
							"</div>"+
					"</div>"+
			"</div>"+
		"</td>"+
		
	"</tr>"+
"</table>"+
"<div style='height: 8px;background-color:white'></div>"+

"<div style='font-family:"+font_style+";width:955px;height:250px;padding-left:30px;background-color:white;'>"+
	"<div align='center' class='special_offer' style='float:left'>"+
		"<div align='center' style='height:185px; width:155px; vertical-align:top;text-align:center;'>"+
			"<div align='center' style='height:145px;'>"+	
				"<a href='/products/view/1011718'>"+
					"<img height='110' align='middle' width='145' alt='' src='/files/special_offer_slider/1011028.gif' style=' padding-top:36px; padding-right:7px;'>"+
				"</a>"+
			"</div>"+
			
			"<div class='offer_ribbon'></div>"+
		"</div>"+
			
		"<div style='font-size:13px; text-align:left; margin-left:7px;'>"+						
			"<a href='/products/view/1011718/9726' title='Golfers PLUS' style='text-decoration:none;color:#"+link_color+"'>"+
				"<strong>Golfers PLUS</strong>"+
			"</a>"+
		"</div>"+
		"<div style='margin-top:5px; text-align:left; margin-left:7px;' class='highlight' title='Golfers PIP Insurance' >"+
			"Golfers PLUS"+
		"</div>"+
	"</div>"+
	
	"<div align='center' class='special_offer' style='float:left'>"+
		"<div align='center' style='height:185px; width:155px; vertical-align:top;text-align:center;'>"+
			"<div align='center' style='height:145px;'>"+	
				"<a href='/products/view/1003037'>"+
					"<img height='110' align='middle' width='145' alt='' src='/files/special_offer_slider/1003037.gif' style=' padding-top:36px; padding-right:7px;'>"+
				"</a>"+
			"</div>"+
			
			"<div class='offer_ribbon'></div>"+
		"</div>"+
			
		"<div style='font-size:13px; text-align:left; margin-left:7px;'>"+						
			"<a href='/products/view/1003037/4056' title='GR8 Toys' style='text-decoration:none;color:#"+link_color+";'>"+
				"<strong>GR8 Toys</strong>"+
			"</a>"+
		"</div>"+
		"<div style='margin-top:5px; text-align:left; margin-left:7px;' class='highlight' title='15% Discount' >"+
			"15% Discount"+
		"</div>"+
	"</div>"+
	
	"<div align='center' class='special_offer' style='float:left'>"+
		"<div align='center' style='height:185px; width:155px; vertical-align:top;text-align:center;'>"+
			"<div align='center' style='height:145px;'>"+	
				"<a href='/products/view/1010075'>"+
					"<img height='110' align='middle' width='145' alt='' src='/files/special_offer_slider/1010075.gif' style=' padding-top:36px; padding-right:7px;'>"+
				"</a>"+
			"</div>"+
			
			"<div class='offer_ribbon'></div>"+
		"</div>"+
			
		"<div style='font-size:13px; text-align:left; margin-left:7px;'>"+						
			"<a href='/products/view/1010075/6085' title='deLUXE Linen & Towels' style='text-decoration:none;color:#"+link_color+";'>"+
				"<strong>deLUXE Linen & Towel...</strong>"+
			"</a>"+
		"</div>"+
		"<div style='margin-top:5px; text-align:left; margin-left:7px;' class='highlight' title='15% Discount' >"+
			"Up to 80% Discount! "+
		"</div>"+
	"</div>"+
	
	"<div align='center' class='special_offer'>"+
		"<div align='center' style='height:185px; width:155px; vertical-align:top;text-align:center;'>"+
			"<div align='center' style='height:145px;'>"+	
				"<a href='/products/view/1012330'>"+
					"<img height='110' align='middle' width='145' alt='' src='/files/special_offer_slider/1012005.gif' style=' padding-top:36px; padding-right:7px;'>"+
				"</a>"+
			"</div>"+
			
			"<div class='offer_ribbon'></div>"+
		"</div>"+
			
		"<div style='font-size:13px; text-align:left; margin-left:7px;'>"+						
			"<a href='/products/view/1012330/10850' title='Chocolate Graphics' style='text-decoration:none;color:#"+link_color+";'>"+
				"<strong>Chocolate Graphics</strong>"+
			"</a>"+
		"</div>"+
		"<div style='margin-top:5px; text-align:left; margin-left:7px;' class='highlight' title='20% off chocolates and packaging' >"+
			"20% off chocolates a..."+
		"</div>"+
	"</div>"+
"</div>"+

"<div style='height: 8px;background-color:white'></div>"+
"<table bgcolor='white' style='font-family:"+font_style+";width:985px'>"+
	"<tr>"+
		"<td valign='top'>"+
			"<table>"+
				"<tr>"+
					"<td>"+
						"<div class='rounded-corners_search_right' style='border:1px solid #"+border_color+";padding-bottom:10px;width:585px;background-color:#"+tab_content_background_color+";float:left;'>"+
							"<div style='font-family:"+tab_font_style+";color:#"+tab_text_color+";font-size:13px;padding-left:25px;padding-top:10px;padding-bottom:10px;'><strong>Our Favourites</strong></div>"+
							"<table>"+
								"<tr>"+
									"<td style='padding-left:15px;background-color:#"+tab_content_background_color+"'>"+
										"<div class='favourites'>"+
											"<span style='display:table-cell;height:130px;width:130px; vertical-align:middle;text-align:center'>"+
												"<img width='100' style='padding:10px;' src='/files/favourites/21189.gif'>"+
											"</span>"+
										"</div>"+
									"</td>"+
									"<td style='padding-left:10px;background-color:#"+tab_content_background_color+"'>"+
										"<div class='favourites'>"+
											"<span style='display:table-cell;height:130px;width:130px; vertical-align:middle;text-align:center'>"+
												"<img width='100' style='padding:10px;' src='/files/favourites/21165.jpg'>"+
											"</span>"+
										"</div>"+
									"</td>"+
									"<td style='padding-left:10px;background-color:#"+tab_content_background_color+"'>"+
										"<div class='favourites'>"+
											"<span style='display:table-cell;height:130px;width:130px; vertical-align:middle;text-align:center'>"+
												"<img width='100' style='padding:10px;' src='/files/favourites/21190.jpg'>"+
											"</span>"+
										"</div>"+
									"</td>"+
									"<td style='padding-left:10px;background-color:#"+tab_content_background_color+"'>"+
										"<div class='favourites'>"+
											"<span style='display:table-cell;height:130px;width:130px; vertical-align:middle;text-align:center'>"+
												"<img width='100' style='padding:10px;' src='/files/favourites/21191.jpg'>"+
											"</span>"+
										"</div>"+
									"</td>"+
								"</tr>"+
								"<tr><td style='height:10px;'></td></tr>"+
								"<tr>"+
									"<td style='padding-left:15px;background-color:#"+tab_content_background_color+"'>"+
										"<div class='favourites'>"+
											"<span style='display:table-cell;height:130px;width:130px; vertical-align:middle;text-align:center'>"+
												"<img width='100' style='padding:10px;' src='/files/favourites/21207.jpg'>"+
											"</span>"+
										"</div>"+
									"</td>"+
									"<td style='padding-left:10px;background-color:#"+tab_content_background_color+"'>"+
										"<div class='favourites'>"+
											"<span style='display:table-cell;height:130px;width:130px; vertical-align:middle;text-align:center'>"+
												"<img width='100' style='padding:10px;' src='/files/favourites/21240.jpg'>"+
											"</span>"+
										"</div>"+
									"</td>"+
									"<td style='padding-left:10px;background-color:#"+tab_content_background_color+"'>"+
										"<div class='favourites'>"+
											"<span style='display:table-cell;height:130px;width:130px; vertical-align:middle;text-align:center'>"+
												"<img width='100' style='padding:10px;' src='/files/favourites/21334.gif'>"+
											"</span>"+
										"</div>"+
									"</td>"+
									"<td style='padding-left:10px;background-color:#"+tab_content_background_color+"'>"+
										"<div class='favourites'>"+
											"<span style='display:table-cell;height:130px;width:130px; vertical-align:middle;text-align:center'>"+
												"<img width='100' style='padding:10px;' src='/files/favourites/21333.gif'>"+
											"</span>"+
										"</div>"+
									"</td>"+
								"</tr>"+
								"<tr><td style='height:10px;'></td></tr>"+
								"<tr>"+
									"<td style='padding-left:15px;background-color:#"+tab_content_background_color+"'>"+
										"<div class='favourites'>"+
											"<span style='display:table-cell;height:130px;width:130px; vertical-align:middle;text-align:center'>"+
												"<img width='100' style='padding:10px;' src='/files/favourites/21371.gif'>"+
											"</span>"+
										"</div>"+
									"</td>"+
									"<td style='padding-left:10px;background-color:#"+tab_content_background_color+"'>"+
										"<div class='favourites'>"+
											"<span style='display:table-cell;height:130px;width:130px; vertical-align:middle;text-align:center'>"+
												"<img width='100' style='padding:10px;' src='/files/favourites/21536.gif'>"+
											"</span>"+
										"</div>"+
									"</td>"+
									"<td style='padding-left:10px;background-color:#"+tab_content_background_color+"'>"+
										"<div class='favourites'>"+
											"<span style='display:table-cell;height:130px;width:130px; vertical-align:middle;text-align:center'>"+
												"<img width='100' style='padding:10px;' src='/files/favourites/21360.gif'>"+
											"</span>"+
										"</div>"+
									"</td>"+
									"<td style='padding-left:10px;background-color:#"+tab_content_background_color+"'>"+
										"<div class='favourites'>"+
											"<span style='display:table-cell;height:130px;width:130px; vertical-align:middle;text-align:center'>"+
												"<img width='100' style='padding:10px;' src='/files/favourites/24745.jpg'>"+
											"</span>"+
										"</div>"+
									"</td>"+
								"</tr>"+
							"</table>"+
						"</div>"+
					"</td>"+
				"</tr>"+
			"</table>"+
		"</td>"+
		"<td valign='top'>"+
			"<div class='rounded-corners_search_right' style='border:1px solid #"+border_color+";padding-bottom:30px;width:360px;background-color:#"+tab_content_background_color+";float:left;padding-left:10px;height:455px;'>"+
				"<div style='font-family:"+tab_font_style+";color:#"+tab_text_color+";font-size:13px;padding-left:1px;padding-top:10px;padding-bottom:10px;'><strong>Check Out Whats Online </strong></div>"+
				"<img height='204' border='0' width='300' src='/files/favourites/mobile1.png'><br>"+
				"<img height='183' border='0' width='300' src='/files/favourites/wish_card.png'>"+
			"</div>"+
		"</td>"+
	"</tr>"+
	"<tr>"+
		"<td colspan='2'>"+
			"<div style='font-family:"+font_style+";width:1005px;margin-top:5px;margin-left:-10px;background-color:#"+footer_background_color+"'>"+
				"<div style='padding-top: 10px; padding-left: 10px;height:120px;'>"+
					"<table style='width:986px'>"+
						"<tr>"+
							"<td valign='top'>"+
								"<ul id='menuf'>"+
									"<li><a style='color:#"+footer_text_color+";text-transform:uppercase;' href='/'><b>Dashboard</b></a></li>"+
								"</ul>"+
								"<ul id='menuf'>"+
									"<li><a style='color:#"+footer_text_color+";' href='/display/how_it_works'><b>HOW IT WORKS</b></a></li>"+
									"<li><a style='color:#"+footer_text_color+";' href='/display/savings_calculator'><b>SAVINGS CALCULATOR</b></a></li>"+
									"<li><a style='color:#"+footer_text_color+";' href='/display/whats_new'><b>WHAT'S NEW</b></a>"+
										"<ul id='menuf'>"+
											"<li><a style='color:#"+footer_text_color+";' href='/display/special_offers'>Hot Offers</a></li>"+
											"<li><a style='color:#"+footer_text_color+";' href='/display/news'>Online Coupon</a></li>"+
										"</ul>"+
									"</li>"+
									"<li><a style='color:#"+footer_text_color+";' href='/display/my_membership'><b>MY MEMBERSHIP</b></a>"+
										"<ul id='menuf'>"+
											"<li><a style='color:#"+footer_text_color+";' href='/users/edit'>Change Details</a></li>"+
											"<li><a style='color:#"+footer_text_color+";' href='/display/nominate_a_friend'>Invite My Family </a></li>"+
										"</ul>"+
									"</li>"+
									"<li><a style='color:#"+footer_text_color+";' href='/display/customer_service'><b>CUSTOMER SERVICE</b></a>"+
										"<ul id='menuf'>"+
											"<li><a style='color:#"+footer_text_color+";' href='/display/terms_and_conditions'>Terms &amp; Conditions</a></li>"+
											"<li><a style='color:#"+footer_text_color+";' href='/display/fair_use_policy'>Fair Use Policy</a></li>"+
											"<li><a style='color:#"+footer_text_color+";' href='/display/member_faq'>FAQs</a></li>"+
											"<li><a style='color:#"+footer_text_color+";' href='/display/privacy_policy'>Privacy Policy</a></li>"+
											"<li><a style='color:#"+footer_text_color+";' href='/contacts/contact_us'>Contact Us</a></li>"+
										"</ul>"+
									"</li>"+
								"</ul>"+							
							"</td>"+
							"<td style='text-align:center;width:230px;padding-top:35px;'>"+
								"<img width='' height='' alt='' src='/blank.png'>"+
							"</td>"+
						"</tr>"+
					"</table>"+
				"</div>"+
				"<br>"+
				"<div style='padding-left:30px;padding-bottom:8px;color:#"+footer_text_color+";'>"+
					"&copy; My Rewards International  Ltd 2012 All Rights Reserved. <br>Follow us on &nbsp;"+									
					"<a href='http://www.facebook.com/pages/My-Rewards-International/133544976702355?v=wall' target='_blank' style='text-decoration:none;border:none !important;'>"+
						"<img width='20' title='facebook' src='/files/social_icons/fb.png' style='vertical-align:middle'>"+
					"</a>"+
					"&nbsp;&nbsp;"+
					"<a href='http://twitter.com/#!/MyRewardsIntl' target='_blank' style='text-decoration:none;border:none !important;'>"+
						"<img width='20' title='twitter' src='/files/social_icons/twitter.png' style='vertical-align:middle'>"+
					"</a>"+
				"</div>"+
			"</div>"
		"</td>"+
	"</tr>"+
"</table>"


