<script type="text/javascript">
	function showWish(pid,uid)
	{
		var url = '/products/wishlist/'+pid+'/'+uid+'/'+Math.random(10);
		new Ajax.Request(url,
		{
		 method:'get',
		 onSuccess: function(transport){
		   var response = transport.responseText || "no response text";
		   alert(response);
		   }
		 });		
		
	}
</script>  

<div class="module25" style="width:245px;">
	<div class="rounded-corners">
		<div class="content">
			<table width="100%">
				<tr>
					<td rowspan="2">
						<div id="<?php echo $product['id']; ?>" class="module35" onmouseover="changeBgImg('<?php echo $product['id']; ?>')" onmouseout="changeOldBg('<?php echo $product['id']; ?>')">
							<div style=" height:195px; width:222px; vertical-align:top;text-align:center;">
								<div style="height:128px;padding-top:20px;">
									<?php 
										$logo="no_logo";
										if ($product['prdtDisplayImage'] == 'Product Image') {
											$filepath = PRODUCT_IMAGE_PATH;
											$filename = $product['id']. '.'. $product['prdtImage'];
										} else {
											$filepath = MERCHANT_LOGO_PATH;
											$filename = $product['merchantId']. '.'. $product['merchantLogo'];
										}
										
										if (!file_exists($filepath. $filename)) 
										{
											//	try for a merchant image
											/*$filepath = CATEGORY_LOGO_PATH;
											$filename = $product['categoryId']. '.'. $product['categoryLogo'];*/
											
											$filepath = MERCHANT_IMAGE_PATH;
											$filename = $product['merchantId']. '.'. $product['merchantLogo'];
											if (!file_exists($filepath. $filename)) {
												//	try for a merchant logo
												$filepath = MERCHANT_LOGO_PATH;
												if ($logo != 'no_logo') {	//	if logo is not meant to appear, set a dummy name so it will not be found
													$filename = 'no_file';
												} else {

													$filename = $product['merchantId']. '.'. $product['merchantLogo'];
												}
												if (!file_exists($filepath. $filename)) {
													//	try for a client image
													$filepath = FILES_PATH. 'default_client_product_image/';
													$filename = $this->Session->read('client.Client.id'). '.'. $this->Session->read('client.Client.default_client_product_image_extension');
													if (!file_exists($filepath. $filename)) {
														//	try for a program image
														$filepath = FILES_PATH. 'default_product_image/';
														$filename = $this->Session->read('client.Program.id'). '.'. $this->Session->read('client.Program.default_product_image_extension');
														if (!file_exists($filepath. $filename)) {
															//	use the default no image image
															$filepath = WWW_ROOT. 'img/';
															$filename = 'no_image.gif';
														}
													}
												}
											}
										}
										
										$path = substr($filepath,strpos($filepath,'webroot')+8);
										$path_prod = $path.$filename;
			 						  	
										$a=$this->ImageResize->getResizedDimensions($path_prod, 180, 140);
										
									?>		
									<a href="/products/view/<?php echo $product['id'].'/'.$keyword ?>"><img align="center" style=" padding-top:16px; padding-left:9px;" width="<?php echo $a['width'];?>" height="<?php echo $a['height'];?>"  src="<?php echo $this->Html->url('/'. $path_prod);?>" alt="" /></a> 
								</div>
								
								<div id="<?php echo $product['id'].'quick'; ?>" style="margin-top:6px; margin-left:8px;float:center; display:none;"> 
									<?php 
									$quickviewimg = $this->Session->read('client.Program.id')==7?"/files/newimg/wellplan_quick_view.png":"/files/newimg/quick_view-1.png";
									echo $this->Html->image($quickviewimg,array('escape' => false,'onclick' => "productpopup1('".$keyword."',".$product['id'].")"));
							 		
									?>
								</div>		                  
							</div>
							 <div style="font-size:13px;display:block;padding-left:13px;width:294px;">
								<?php 
									 $pname = strip_tags($product['name']);
								if (strlen($pname)>'25') {?>
								<?php  $title=mb_strcut($pname,0,25,'UTF-8').'...';?>
								<?php  echo $this->Html->link($title,'/products/view/'. $product['id'].'/'.$keyword,array('style'=>'font-weight:bold;text-decoration:none','title'=>$pname));?>
								<?php } else {?>
								<?php  echo $this->Html->link($pname,'/products/view/'. $product['id'].'/'.$keyword,array('style'=>'font-weight:bold;text-decoration:none','title'=>$pname)); ?>
								<?php } ?>
								<?php //if ($form->value('Product.sort') == 'suburb' && $product['MerchantAddress']['suburb'] != '') { echo ' - '. $product['MerchantAddress']['suburb']; }?>
							</div>
					         <div id="text1" title="<?php echo strip_tags($product['prdtHighlight']); ?>" class="highlight" style="padding-left:13px;margin-top:5px;">
					                 <?php if (strlen($product['prdtHighlight'])>'20') {?>
								<?php  echo mb_strcut(strip_tags($product['prdtHighlight']),0,20,'UTF-8').'...' ?>
								<?php } else {?>
								<?php  echo $product['prdtHighlight']; ?>
								<?php } ?>
								      <script type="text/javascript">
                                      document.getElementById('text1').title="New tooltip"
                                      </script>
							 </div>
						  </div>
					</td>
				</tr>
			</table>
		</div>
	</div>
</div>