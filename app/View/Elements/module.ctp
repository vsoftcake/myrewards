<div class="module">
	<div class="t rounded-corners content">
        <?php if ($title == "What's New") { $title = "Hot off the Press";}  ?>
		<?php if  ($this->Session->read('client.Client.country') == 'Hong Kong' ) :  ?>
		<?php if ($title == "Special Offers") { $title = "Upcoming Events";}  ?>
		<?php endif; ?>
		<div class="tab_padding">
			<span class="tab_font_style"><?php echo $title; ?></span>
		</div>
    </div>
	<div class="rounded-corners">
		<div class="content">
			<?php echo $this->element($module. '_module'); ?>
			<div style="height:2px"></div>
		</div>
	
		<div class="b rounded-corners_bottom">&nbsp;</div>
	</div>
</div>
	