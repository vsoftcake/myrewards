<p>Thank you for registering for e-communications from the <?php echo $this->Session->read('client.Client.name');?> program. E-communications are a great way for you to keep up to date with all the fantastic offers available to you. </p>

<p>To access your <?php echo $this->Session->read('client.Client.name');?> offers please <a href="<?php echo $this->Session->read('client.Domain.name');?>">sign in</a> or for more information, <a href="mailto:<?php echo $this->Session->read('client.Client.email');?>?subject=More%20info">email us</a>.</p>

<p>Yours sincerely,<br />

<?php echo $this->Session->read('client.Client.name');?> Customer Service </p>