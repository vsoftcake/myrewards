<?php // do the replacements
$nContent = $newsletter["Newsletter"]["content"];
$nContent = str_replace("[[client_name]]", $client['Client']['name'], $nContent);
$nContent = str_replace("[[client_email]]", $client['Client']['email'], $nContent);
$nContent = str_replace("[[program_name]]", $client['Program']['name'], $nContent);
$nContent = str_replace("[[web_site]]", $client['Domain']['name'], $nContent);
$nContent = str_replace("[[first_name]]", $user['User']['first_name'], $nContent);
$nContent = str_replace("[[last_name]]", $user['User']['last_name'], $nContent);
$nContent = str_replace("[[twitter_link]]", $client['Client']['twitter_link'], $nContent);
$nContent = str_replace("[[user_name]]", $user['User']['username'], $nContent);
$nContent = str_replace("[[password]]", $user['User']['password'], $nContent);
$nContent = str_replace("[[unique_id]]", $user['User']['unique_id'], $nContent);
$newsletter["Newsletter"]["content"] = $nContent;
?>
<div id="newsletter" align="center" style="font-family: <?php echo $newsletter['Newsletter']['font']; ?>;font-size:12px;<?php echo ($newsletter['Newsletter']['newsletter_background'] != '' && $newsletter['Newsletter']['newsletter_background'] != 'FFFFFF')? 'background-color: #'. $newsletter['Newsletter']['newsletter_background']. ';': "background-color: #F3F3F3"; ?>;padding:15px;min-height:400px;overflow:hidden;">
	<div style="color:#009EC7;font-size:30px;font-weight:bold;text-align:center;padding-bottom:10px;">
		my rewards newsletter
	</div>
	<div style="border:2px solid #969393;margin-left:20px;width:600px;min-height: 200px; overflow: hidden; background-color: #ffffff;">
		<div style="border-bottom:2px solid #787878;">
			<?php if ($this->action != 'newsletter') : ?>
				<div style="text-align:center;<?php echo ($newsletter['Newsletter']['online_version_color'] != '' && $newsletter['Newsletter']['online_version_color'] != 'FFFFFF')? 'background-color: #'. $newsletter['Newsletter']['online_version_color']. ';': "background-color: #CBE60B"; ?>;font-size:14px;font-weight:bold;line-height:25px;">
					<a style="text-decoration:none;<?php echo ($newsletter['Newsletter']['version_text_color'] != '' && $newsletter['Newsletter']['version_text_color'] != 'FFFFFF')? 'color: #'. $newsletter['Newsletter']['version_text_color']. ';': "color: #3399FF"; ?>" href="http://<?php echo $client['Domain']['name']; ?><?php echo $this->Html->url('/newsletters/newsletter/'.$client['Client']['id'].'/'. $newsletter['Newsletter']['id']. '/'. $user['User']['unique_id']); ?>" target="newsletter">
						View the online version
					</a>
				</div>
			<?php endif;?>
			<div id="header">
				<?php 
					if($newsletter['Newsletter']['dashboard_id']==1) 
					{
						$path_image = 'files/clients/newsletter_banner_image_1/'. $clientbanner['0']['ClientBanner']['newsletter_banner_image']; 
					}
					if($newsletter['Newsletter']['dashboard_id']==2) 
					{
						$path_image = 'files/clients/newsletter_banner_image_2/'. $clientbanner['0']['ClientBanner']['newsletter_banner_image']; 
					}
					if($newsletter['Newsletter']['dashboard_id']==3) 
					{
						$path_image = 'files/clients/newsletter_banner_image_3/'. $clientbanner['0']['ClientBanner']['newsletter_banner_image'];
					}
				?>
				<?php $img=$this->ImageResize->getResizedDimensions($path_image, 600);?>
				<a href="http://<?php echo $client['Domain']['name']; ?><?php echo $this->Html->url('/'); ?>" style="<?php echo ($client['Style']['link_color'] != '')? 'color: #'. $client['Style']['link_color']. ';': ''; ?>" target="newsletter">
					<img width="<?php echo $img['width'];?>" src="http://<?php echo $client['Domain']['name']; ?>/<?php echo $path_image;?>" alt="" border="0" target="newsletter"/>
				</a>
			</div>
			<?php if (isset($newsletter_categories)) : ?>
			<?php if(!empty($newsletter_categories)) :?>
				<div style="min-height:20px;overflow:hidden;<?php echo ($newsletter['Newsletter']['preview_menu_color'] != '' && $newsletter['Newsletter']['preview_menu_color'] != 'FFFFFF')? 'background-color: #'. $newsletter['Newsletter']['preview_menu_color']. ';': "background-color: #E8008A"; ?>;<?php echo ($newsletter['Newsletter']['menu_text_color'] != '' && $newsletter['Newsletter']['menu_text_color'] != 'FFFFFF')? 'color: #'. $newsletter['Newsletter']['menu_text_color']. ';': "color: #FFFFFF"; ?>">
					<ul style="margin:0px;padding-left:5px;">
					<?php $i=1;?>
						<?php foreach ($newsletter_categories as $category) : ?>
							<li style="float:left;list-style-type:none;width:auto;padding-top: 10px; padding-left: 5px; padding-right: 5px;">
								<div style="height:30px;font-size:12px;font-weight:bold">
									<?php echo $category['Category']['name'];?> 
									<?php if($i<count($newsletter_categories)) :?>
										|
									<?php endif;?>
								</div>
							</li><?php $i++;?>
						<?php endforeach; ?>
					</ul>
				</div>	
			<?php endif;?>	
		<?php endif;?>
		</div>
		<!--<div style="border-bottom:2px solid #787878">
			<font style="color:#E9008A;font-size:25px;">Hot Offer</font>
		</div>
		-->
		<?php if (isset($newsletter_categories)) : ?>
		<?php if(!empty($newsletter_categories)) :?>
			<div style="padding-bottom:10px;">
				<font style="color:#E9008A;font-size:20px;">Monthly specials</font>
			</div>
			<div style="margin-left:20px;width:600px;min-height: 200px; overflow: hidden; background-color: #ffffff; border: 1px solid #ffffff;">
				<?php foreach ($newsletter_categories as $category) : ?>
					<?php if (isset($category['Product'])) : ?>
						<?php foreach($category['Product'] as $product) : ?>
							<div style="float:left;">
								<div style=" height:150px; width:180px; vertical-align:top;text-align:center;">
									<div style="min-height:115px;overflow:hidden;padding-bottom:5px;">
											<?php if (trim($newsletter['Newsletter']['product_link']) == '') {
												$link = 'http://'. $client['Domain']['name']. '/products/view/'. $product['Product']['id'];
											} else {
												$link = trim($newsletter['Newsletter']['product_link']);
											}
											?>
											<a href="<?php echo $link; ?>" target="newsletter">
												<font style="font-size:12px;" face="<?php echo $newsletter['Newsletter']['font']; ?>">
													<img src="http://<?php echo $client['Domain']['name']; ?><?php echo $this->Html->url('/images/product/'. $product['Product']['id']. '.'. $product['Product']['image_extension']. '/no_logo'); ?>" alt="" border="" />
												</font>
											</a>
									</div>
									<div align="center">
										<?php if (trim($newsletter['Newsletter']['product_link']) == '') {
										$link = 'http://'. $client['Domain']['name']. '/products/view/'. $product['Product']['id'];
									} else {
										$link = trim($newsletter['Newsletter']['product_link']);
									}
									?>
									<a href="<?php echo $link; ?>" style="
										background-color: #00ADEF;
										color: #FFFFFF;
										text-decoration:none;
										padding:3px;
										" class="more rounded-corners" target="newsletter">
											<font style="font-size:12px;" face="<?php echo $newsletter['Newsletter']['font']; ?>">
												&nbsp;&nbsp;&nbsp;&nbsp;Read More&nbsp;&nbsp;&nbsp;&nbsp;
											</font>
										</a>
									</div>	
								</div>
					  		</div>
					  	<?php endforeach; ?>
					<?php endif; ?>
				<?php endforeach; ?>
			</div>
		<?php endif;?>	
	<?php endif; ?>
		<div style="text-align:center;width:600px;padding-bottom:10px;padding-top:10px">
			<img src="http://<?php echo $client['Domain']['name']; ?>/files/newimg/images/cat_images/1.png" style="padding-left:10px"/>
			<img src="http://<?php echo $client['Domain']['name']; ?>/files/newimg/images/cat_images/2.png" style="padding-left:10px"/>
			<img src="http://<?php echo $client['Domain']['name']; ?>/files/newimg/images/cat_images/3.png" style="padding-left:10px"/>
			<img src="http://<?php echo $client['Domain']['name']; ?>/files/newimg/images/cat_images/4.png" style="padding-left:10px"/>
			<img src="http://<?php echo $client['Domain']['name']; ?>/files/newimg/images/cat_images/5.png" style="padding-left:10px"/>
			<img src="http://<?php echo $client['Domain']['name']; ?>/files/newimg/images/cat_images/6.png" style="padding-left:10px"/>
			<img src="http://<?php echo $client['Domain']['name']; ?>/files/newimg/images/cat_images/7.png" style="padding-left:10px"/>
			<img src="http://<?php echo $client['Domain']['name']; ?>/files/newimg/images/cat_images/8.png" style="padding-left:10px"/>
		</div>
	</div>
	<div style="margin-left:20px;width:600px;text-align:center;<?php echo ($newsletter['Newsletter']['coupon_book_color'] != '' && $newsletter['Newsletter']['coupon_book_color'] != 'FFFFFF')? 'background-color: #'. $newsletter['Newsletter']['coupon_book_color']. ';': "background-color: #97D5E5"; ?>;<?php echo ($newsletter['Newsletter']['book_text_color'] != '' && $newsletter['Newsletter']['book_text_color'] != 'FFFFFF')? 'color: #'. $newsletter['Newsletter']['book_text_color']. ';': "color: #E9008A"; ?>;font-size:18px;line-height:45px;border-left:2px solid #969393;border-right:2px solid #969393;border-bottom:2px solid #969393;">
		See also the online Coupon Books! 
		<?php if(!empty($newsletter['Newsletter']['coupon_book_link'])) : ?>
			<?php 
				$linkurl = '';
				if (false === strpos($newsletter['Newsletter']['coupon_book_link'], '://')) 
				{
					$linkurl = 'http://'.$newsletter['Newsletter']['coupon_book_link'];
				}
				else
				{
					$linkurl = $newsletter['Newsletter']['coupon_book_link'];
				}
			?>
			<a style="text-decoration:none;<?php echo ($newsletter['Newsletter']['book_text_color'] != '' && $newsletter['Newsletter']['book_text_color'] != 'FFFFFF')? 'color: #'. $newsletter['Newsletter']['book_text_color']. ';': "color: #E9008A"; ?>" href="<?php echo $linkurl;?>" target="_blank"><b>click here</b></a>
		<?php endif; ?>
	</div>
	<div style="width:600px;text-align:center;padding-bottom:20px;padding-top:15px;margin-left:20px;border-left:2px solid #969393;border-right:2px solid #969393;border-bottom:2px solid #969393;<?php echo ($newsletter['Newsletter']['contact_image_background'] != '' && $newsletter['Newsletter']['contact_image_background'] != 'FFFFFF')? 'background-color: #'. $newsletter['Newsletter']['contact_image_background']. ';': "background-color: #FFFFFF"; ?>;">
		<?php 
			$filepath1 = NEWSLETTER_IMAGE1_PATH;
			$filename1 = $newsletter['Newsletter']['id']. '.'. $newsletter['Newsletter']['image1_ext'];
			$path1 = $this->Html->url(NEWSLETTER_IMAGE1_WWW_PATH. $newsletter['Newsletter']['id']. '.'. $newsletter['Newsletter']['image1_ext']); 
			$path_image1 = substr($path1,strpos($path1)+1);
			
			$filepath2 = NEWSLETTER_IMAGE2_PATH;
			$filename2 = $newsletter['Newsletter']['id']. '.'. $newsletter['Newsletter']['image2_ext'];
			$path2 = $this->Html->url(NEWSLETTER_IMAGE2_WWW_PATH. $newsletter['Newsletter']['id']. '.'. $newsletter['Newsletter']['image2_ext']); 
			$path_image2 = substr($path2,strpos($path2)+1);
		?>
		<?php if (file_exists($filepath1. $filename1)) :?>
			<?php $img=$this->ImageResize->getResizedDimensions($path_image1, 250, 170);?>
			<img width="<?php echo $img['width'];?>" height="<?php echo $img['height'];?>"  src="http://<?php echo $client['Domain']['name']; ?>/<?php echo $path_image1;?>" style="padding-right:10px;"/>
		<?php else : ?>
			<img src="http://<?php echo $client['Domain']['name']; ?>/files/newimg/images/img/mobile2.png" style="padding-right:10px;"/>
		<?php endif;?>
		
		<?php if (file_exists($filepath2. $filename2)) :?>
			<?php $img=$this->ImageResize->getResizedDimensions($path_image2, 250, 170);?>
			<img width="<?php echo $img['width'];?>" height="<?php echo $img['height'];?>"  src="http://<?php echo $client['Domain']['name']; ?>/<?php echo $path_image2;?>"/>
		<?php else : ?>
			<img src="http://<?php echo $client['Domain']['name']; ?>/files/newimg/images/img/temp_connect2.png"/>
		<?php endif;?>
	</div>
	<div style="width:600px;margin-left:20px;text-align:center;<?php echo ($newsletter['Newsletter']['call_color'] != '' && $newsletter['Newsletter']['call_color'] != 'FFFFFF')? 'background-color: #'. $newsletter['Newsletter']['call_color']. ';': "background-color: #ADABAB"; ?>;<?php echo ($newsletter['Newsletter']['call_text_color'] != '' && $newsletter['Newsletter']['call_text_color'] != 'FFFFFF')? 'color: #'. $newsletter['Newsletter']['call_text_color']. ';': "color: #F9F7F7"; ?>;font-size:15px;font-weight:bold;line-height:50px;border-left:2px solid #969393;border-right:2px solid #969393;border-bottom:2px solid #969393;">
		Call <?php if(!empty($newsletter['Newsletter']['phone_number'])) : echo $newsletter['Newsletter']['phone_number']; else : echo "1300 368 846"; endif;?> for further information or search for more fabulous offers online.
	</div>
	<?php if ($newsletter['Newsletter']['footer_disabled'] == '0') : ?>
	<div style="width: 560px; text-align:left;margin-left: 20px;padding:10px 10px 1px 30px;<?php echo ($newsletter['Newsletter']['newsletter_text_color'] != '' && $newsletter['Newsletter']['newsletter_text_color'] != 'FFFFFF')? 'color: #'. $newsletter['Newsletter']['newsletter_text_color']. ';': "color: #7D7D7D"; ?>;<?php echo ($newsletter['Newsletter']['contact_image_background'] != '' && $newsletter['Newsletter']['contact_image_background'] != 'FFFFFF')? 'background-color: #'. $newsletter['Newsletter']['contact_image_background']. ';': "background-color: #FFFFFF"; ?>;border-left:2px solid #969393;border-right:2px solid #969393;">
		<font style="font-size:12px;" face="<?php echo $newsletter['Newsletter']['font']; ?>"><?php echo $newsletter['Newsletter']['footer_content']; ?></font>
	</div>
	<?php endif; ?>
	<?php if ($this->action != 'newsletter') : ?>
		<div style="width:560px;margin-left:20px;margin-bottom:10px;text-align:left; padding:1px 10px 10px 30px;<?php echo ($newsletter['Newsletter']['contact_image_background'] != '' && $newsletter['Newsletter']['contact_image_background'] != 'FFFFFF')? 'background-color: #'. $newsletter['Newsletter']['contact_image_background']. ';': "background-color: #FFFFFF"; ?>;<?php echo ($newsletter['Newsletter']['newsletter_text_color'] != '' && $newsletter['Newsletter']['newsletter_text_color'] != 'FFFFFF')? 'color: #'. $newsletter['Newsletter']['newsletter_text_color']. ';': "color: #7D7D7D"; ?>;border-left:2px solid #969393;border-right:2px solid #969393;border-bottom:2px solid #969393;">
			<a style="text-decoration:none;<?php echo ($newsletter['Newsletter']['newsletter_text_color'] != '' && $newsletter['Newsletter']['newsletter_text_color'] != 'FFFFFF')? 'color: #'. $newsletter['Newsletter']['newsletter_text_color']. ';': "color: #7D7D7D"; ?>" href="http://<?php echo $client['Domain']['name']; ?><?php echo $this->Html->url('/newsletters/unsubscribe/'. $user['User']['email']); ?>" target="newsletter">
				<b>Click here</b>
			</a> to unsubscribe.
		</div>
	<?php endif; ?>
</div>
