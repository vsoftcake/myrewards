<?php echo $this->Html->script('MyCalc');?>
	<div id="my-wraper">
			<div class="inside_page_main_content">
				<div id="main_content_without_left">
					<div class="padded_10">
						
						<div id="CalcPage">
							<div id="Calcbg">
								<div id="Calc">
									<form name="savingForm">
										<span class="calcSelect">Select your spending period:
											<select name="selectTime" onchange="MyCalc()">
											    <option value="0">Please select</option>
											    <option value="52">Weekly</option>
											    <option value="26">Fortnightly</option>
											    <option value="12">Monthly</option>
											    <option value="1">Yearly</option>
										  	</select>
										</span>
									
										<div id="calcItemTitle" class="calcTitle">Items</div>
										<div id="calcOtherTitle" class="calcTitle">Spending</div>
										<div id="calcOtherTitle" class="calcTitle">New Price</div>
										<div id="calcOtherTitle" class="calcTitle">Saving</div>
										
										<div id="tableColumn">
											<div id="calcItem" class="tableSpace" align="left">Entertainment</div>
											<div id="calcContent" class="tableSpace" align="left">
												<input name="inputEntertain" id="inputForm" type="text" class="field" onkeyup="MyCalc()" value="0" size="8" maxlength="7" />
											</div>
											<div id="calcContent" class="tableSpace" align="left"><span id="outputEntertainAmt">0.00</span></div>
											<div id="calcContent" class="tableSpace" align="left"><span id="outputEntertainSav">0.00</span></div>
										</div>
										
										<div id="tableColumn">
											<div id="calcItem" class="tableSpace" align="left">Movie Tickets</div>
											<div id="calcContent" class="tableSpace" align="left">
											<input name="inputMovie" id="inputForm" type="text" class="field" onKeyUp="MyCalc()" value="0" size="8" maxlength="7">
											</div>
											<div id="calcContent" class="tableSpace" align="left"><span id="outputMovieAmt">0.00</span></div>
											<div id="calcContent" class="tableSpace" align="left"><span id="outputMovieSav">0.00</span></div>
										</div>
										
										<div id="tableColumn">
											<div id="calcItem" class="tableSpace" align="left">Automotive Services</div>
											<div id="calcContent" class="tableSpace" align="left">
												<input name="inputAuto" id="inputForm" type="text" class="field" onKeyUp="MyCalc()" value="0" size="8" maxlength="7">
											</div>
											<div id="calcContent" class="tableSpace" align="left"><span id="outputAutoAmt">0.00</span></div>
											<div id="calcContent" class="tableSpace" align="left"><span id="outputAutoSav">0.00</span></div>
										</div>
										
										<div id="tableColumn">
											<div id="calcItem" class="tableSpace" align="left">Clothing</div>
											<div id="calcContent" class="tableSpace" align="left">
												<input name="inputClothing" id="inputForm" type="text" class="field" onKeyUp="MyCalc()" value="0" size="8" maxlength="7">
											</div>
											<div id="calcContent" class="tableSpace" align="left"><span id="outputClothingAmt">0.00</span></div>
											<div id="calcContent" class="tableSpace" align="left"><span id="outputClothingSav">0.00</span></div>
										</div>
										
										<div id="tableColumn">
											<div id="calcItem" class="tableSpace" align="left">Restaurants</div>
											<div id="calcContent" class="tableSpace" align="left">
												<input name="inputRest" id="inputForm" type="text" class="field" onKeyUp="MyCalc()" value="0" size="8" maxlength="7">
											</div>
											<div id="calcContent" class="tableSpace" align="left"><span id="outputRestAmt">0.00</span></div>
											<div id="calcContent" class="tableSpace" align="left"><span id="outputRestSav">0.00</span></div>
										</div>
										
										<div id="tableColumn">
											<div id="calcItem" class="tableSpace" align="left">Health & Beauty</div>
											<div id="calcContent" class="tableSpace" align="left">
												<input name="inputHealth" id="inputForm" type="text" class="field" onKeyUp="MyCalc()" value="0" size="8" maxlength="7">
											</div>
											<div id="calcContent" class="tableSpace" align="left"><span id="outputHealthAmt">0.00</span></div>
											<div id="calcContent" class="tableSpace" align="left"><span id="outputHealthSav">0.00</span></div>
										</div>
										
										<div id="tableColumn">
											<div id="calcItem" class="tableSpace" align="left">Electrical goods</div>
											<div id="calcContent" class="tableSpace" align="left">
												<input name="inputElectric" id="inputForm" type="text" class="field" onKeyUp="MyCalc()" value="0" size="8" maxlength="7">
											</div>
											<div id="calcContent" class="tableSpace" align="left"><span id="outputElectricAmt">0.00</span></div>
											<div id="calcContent" class="tableSpace" align="left"><span id="outputElectricSav">0.00</span></div>
										</div>
										
										<div id="tableColumn">
											<div id="calcItem" class="tableSpace" align="left">Fuel*</div>
											<div id="calcContent" class="tableSpace" align="left">
												<input name="inputFuel" id="inputForm" type="text" class="field" onKeyUp="MyCalc()" value="0" size="8" maxlength="7"/>
											</div>
											<div id="calcContent" class="tableSpace" align="left"><span id="outputFuelAmt">0.00</span></div>
											<div id="calcContent" class="tableSpace" align="left"><span id="outputFuelSav">0.00</span></div>
										</div>
										
										<div id="tableColumn">
											<div id="calcItem" class="tableSpace" align="left">Groceries*</div>
											<div id="calcContent" class="tableSpace" align="left">
												<input name="inputGroceries" id="inputForm" type="text" class="field" onKeyUp="MyCalc()" value="0" size="8" maxlength="7">
											</div>
											<div id="calcContent" class="tableSpace" align="left"><span id="outputGroceriesAmt">0.00</span></div>
											<div id="calcContent" class="tableSpace" align="left"><span id="outputGroceriesSav">0.00</span></div>
										</div>
										
										<div id="tableColumn">
											<div id="calcItem" class="tableSpace" align="left"><span class="calcSmallText">Travel & Accommodation</span></div>
											<div id="calcContent" class="tableSpace" align="left">
												<input name="inputTravel" id="inputForm" type="text" class="field" onKeyUp="MyCalc()" value="0" size="8" maxlength="7">
											</div>
											<div id="calcContent" class="tableSpace" align="left"><span id="outputTravelAmt">0.00</span></div>
											<div id="calcContent" class="tableSpace" align="left"><span id="outputTravelSav">0.00</span></div>
										</div>
										
										<div id="tableColumn">
											<div id="calcItem" class="tableSpace" align="left">TOTAL</div>
											<div id="calcContent" class="tableSpace" align="left"><span id="outputSpendAll">0.00</span></div>
											<div id="calcContent" class="tableSpace" align="left"><span id="outputAmtAll">0.00</span></div>
											<div id="calcContent" class="tableSpace" align="left"><span id="outputSavAll">0.00</span></div>
										</div>
										
										<div id="calcItemResults" class="calcResults">Savings</div>
										<div id="calcOtherResults" class="calcResults">Weekly: <span id="outputWeekSave">0.00</span></div>
										<div id="calcOtherResults" class="calcResults">Monthly: <span id="outputMonthSave">0.00</span></div>
										<div id="calcOtherResults" class="calcResults">Yearly: <span id="outputYearSave">0.00</span></div>
									</form>
								</div>
							</div>
						</div>
					</div><!--end of padded_10--><!--end of main_content-->
			</div>
		</div>
		<div style="height:60px; width:920px; float:left;"></div>
		<div style="clear:both"></div>
	</div>
