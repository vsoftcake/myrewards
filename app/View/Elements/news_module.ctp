<?php if(($module_data['news']['ClientPage']['enabled'] == 1 || $module_data['news']['ClientPage']['enabled_override'] == 1) && ($module_data['news']['ClientPage']['page_id'] == 63) && $this->Session->read('client.Client.dashboard')==1): ?>
<div id="fourth_second_row" style="margin-top:10px;">
	<div id="left_column">
		<div class="module">
			<div class="t rounded-corners content">
		        <div class="tab_padding">
					<span class="tab_font_style"><?php //echo $module_data['news']['ClientPage']['title']; ?></span>
				</div>
		    </div>
			<div class="rounded-corners">
				<div class="content">
					<?php if (isset($module_data['news']['ClientPage']['content'])) : ?>
						<div class="tab_padding">
							<?php echo ($module_data['news']['ClientPage']['content']); ?>
						</div>
					<?php endif; ?>
					<div style="height:2px"></div>
				</div>
			
				<div class="b rounded-corners_bottom">&nbsp;</div>
			</div>
		</div>
		
  	</div>
</div>
<?php endif;?>
<?php if(($module_data['news']['ClientPage']['enabled'] == 1 || $module_data['news']['ClientPage']['enabled_override'] == 1) && ($module_data['news']['ClientPage']['page_id'] == 63) && $this->Session->read('client.Client.dashboard')==2): ?>
<div id="fourth_second_row" style="margin-top:10px;">
	<div id="left_column">
		<div class="module">
			<div class="t rounded-corners content">
		        <div class="tab_padding">
					<span class="tab_font_style"><?php echo $module_data['news']['ClientPage']['title']; ?></span>
				</div>
		    </div>
			<div class="rounded-corners">
				<div class="content">
					<?php if (isset($module_data['news']['ClientPage']['content'])) : ?>
						<div class="tab_padding">
							<?php echo ($module_data['news']['ClientPage']['content']); ?>
						</div>
					<?php endif; ?>
					<div style="height:2px"></div>
				</div>
			
				<div class="b rounded-corners_bottom">&nbsp;</div>
			</div>
		</div>
		
  	</div>
</div>
<?php endif;?>
<?php if(($module_data['news']['ClientPage']['enabled'] == 1 || $module_data['news']['ClientPage']['enabled_override'] == 1) && ($module_data['news']['ClientPage']['page_id'] == 63) && $this->Session->read('client.Client.dashboard')==3): ?>
<div id="fourth_second_row" style="margin-top:10px;">
	<div id="left_column">
		<div class="module">
			<div class="t rounded-corners content">
		        <div class="tab_padding">
					<span class="tab_font_style"><?php echo $module_data['news']['ClientPage']['title']; ?></span>
				</div>
		    </div>
			<div class="rounded-corners">
				<div class="content">
					<?php if (isset($module_data['news']['ClientPage']['content'])) : ?>
						<div class="tab_padding">
							<?php echo ($module_data['news']['ClientPage']['content']); ?>
						</div>
					<?php endif; ?>
					<div style="height:2px"></div>
				</div>
			
				<div class="b rounded-corners_bottom">&nbsp;</div>
			</div>
		</div>
		
  	</div>
</div>
<?php endif;?>