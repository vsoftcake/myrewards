<?php
if($row_num <= MAX_REPORT_ROW_NUM){
	if (isset($reports[0])) {

		$line = array();
		foreach ($reports[0]['Export'] as $key => $value) {
			$line[] = $key;
		}
		$this->Csv->addRow($line);
		
		foreach ($reports as $key => $value) {
			$line = $value['Export'];
			$this->Csv->addRow($line);
		}
	} else {
		$this->Csv->addRow(array('No records found'));
	}
	echo $this->Csv->render($name.'.csv');
}else{  
?>
<h2>Download report</h2>
<div class="actions">
	<ul>
		<li><?php echo $this->Html->link('Cancel', '/admin/reports'); ?></li>
		<li><a href="javascript:void(0)" onclick="history.back(); return false;">Back to report</a></li>
	</ul>
</div>
<p>The report contains <?php echo number_format($row_num, 0, 0, ','); ?> records, it has been split into <?php echo number_format(MAX_REPORT_ROW_NUM, 0, 0, ','); ?> record segments.</p>
<ul>
<?php
	$file_num = 0;
	while($file_num * MAX_REPORT_ROW_NUM <= $row_num) :
		$file_num ++;
?>
<li><a href="<?php echo $this->Html->url('/admin/reports/subreport_export/'. $name. '/'. $file_num); ?>"><?php echo $name; ?> part <?php echo $file_num; ?></a></li>
	<?php endwhile; ?>
</ul>
<?php } ?>
