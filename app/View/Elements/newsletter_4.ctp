<?php // do the replacements
$nContent = $newsletter["Newsletter"]["content"];
$nContent = str_replace("[[client_name]]", $client['Client']['name'], $nContent);
$nContent = str_replace("[[client_email]]", $client['Client']['email'], $nContent);
$nContent = str_replace("[[program_name]]", $client['Program']['name'], $nContent);
$nContent = str_replace("[[web_site]]", $client['Domain']['name'], $nContent);
$nContent = str_replace("[[first_name]]", $user['User']['first_name'], $nContent);
$nContent = str_replace("[[last_name]]", $user['User']['last_name'], $nContent);
$nContent = str_replace("[[twitter_link]]", $client['Client']['twitter_link'], $nContent);
$nContent = str_replace("[[user_name]]", $user['User']['username'], $nContent);
$nContent = str_replace("[[password]]", $user['User']['password'], $nContent);
$nContent = str_replace("[[unique_id]]", $user['User']['unique_id'], $nContent);
$newsletter["Newsletter"]["content"] = $nContent;
?>4444
<div id="newsletter" align="center" style="font-family: <?php echo $newsletter['Newsletter']['font']; ?>;font-size:12px;background-color:#8E8989;padding:15px;min-height:400px;overflow:hidden;">
	<div style="border: 1px solid #808080;width:640px;margin-left:10px;">
		<div style="color:#808080;background-color:#FFFFFF;font-size:20px;font-weight:bold;text-align:center;padding:10px;height:30px;">
			my rewards newsletter
		</div>
		<div style="border-bottom:1px solid #808080;min-height: 100px; overflow: hidden; background-color: #ffffff;">
			
			<div style="width:640px;min-height: 200px; overflow: hidden; background-color: #ffffff;border-bottom:1px solid #808080; ">
				<table>
					<tr>
						<td valign="top" style="width:340px;">
							<div style="float:left;">
								<img style="width:340px" src="http://<?php echo $client['Domain']['name']; ?>/files/newimg/images/img/rewards.png"/>
							</div>
						</td>
						<td valign="top" style="width:290px;">
							<?php if (isset($newsletter_categories)) : ?>
								<?php if(!empty($newsletter_categories)) :?>
									<div style="float:left;text-align:left;width:300px;border-top:1px solid #808080;margin-left:-4px;">
										<?php foreach ($newsletter_categories as $category) : ?>
											<?php if (isset($category['Product'])) : ?>
												<?php foreach($category['Product'] as $product) : ?>
													<div style="border-bottom:1px solid #808080;padding:10px;">
														<?php if (trim($newsletter['Newsletter']['product_link']) == '') {
															$link = 'http://'. $client['Domain']['name']. '/products/view/'. $product['Product']['id'];
														} else {
															$link = trim($newsletter['Newsletter']['product_link']);
														}
														?>
														<div style="padding:2px;">
															<a href="<?php echo $link; ?>" target="newsletter" class="item_header" style="text-decoration:none;color:#5F2A93;
																<?php echo ($client['Style']['h4_size'] != '')? 'size: '. $client['Style']['h4_size']. ';': ""; ?>">
																<b>
																	<font style="font-size:14px;" face="<?php echo $newsletter['Newsletter']['font']; ?>">
																		<?php echo ($product['Product']['special_offer_headline'] != ''? $product['Product']['special_offer_headline']: $product['Product']['name']); ?>
																	</font>
																</b>
															</a>
														</div>
														<div style="padding:2px;">
															<font style="font-size:12px;" face="<?php echo $newsletter['Newsletter']['font']; ?>">
																<?php if(!empty($product['Product']['highlight'] ) || $product['Product']['highlight']!='') : ?>
																	<?php 
																		$h = strip_tags($product['Product']['highlight']);
																		$length = strlen($h);
																		if($length>40) :
																	?>
																		<span style="font-size:13px;color:#33B1D2">
																			<b><?php echo substr($h,0,40).'....'; ?></b>
																		</span><br/><br/>
																	<?php else : ?>
																		<span style="font-size:13px;color:#33B1D2">
																			<b><?php echo $product['Product']['highlight'];?></b>
																		</span><br/><br/>
																<?php endif; ?>
																<?php endif;?>
																<?php 
																	$d = strip_tags($product['Product']['details']);
																	$len = strlen($d);
																	if($len>70) :
																		echo substr($d,0,70).'....';
																	else :
																		echo $product['Product']['details'];
																	endif;
																?>
															</font>
														</div>
														<div style="padding:2px;">
															<table>
																<tr>
																	<td>
																		<div style="width:170px"> 
																			<?php if (trim($newsletter['Newsletter']['product_link']) == '') {
																				$link = 'http://'. $client['Domain']['name']. '/products/view/'. $product['Product']['id'];
																			} else {
																				$link = trim($newsletter['Newsletter']['product_link']);
																			}
																			?>
																			<a href="<?php echo $link; ?>" target="newsletter" style="text-decoration:none;">
																				<img src="http://<?php echo $client['Domain']['name']; ?><?php echo $this->Html->url('/images/product/'. $product['Product']['id']. '.'. $product['Product']['image_extension']. '/no_logo'); ?>" alt="" border="" />
																			</a>
																			<?php if (trim($newsletter['Newsletter']['product_link']) == '') :
																					$link = 'http://'. $client['Domain']['name']. '/products/view/'. $product['Product']['id'];
																				else :
																					$link = trim($newsletter['Newsletter']['product_link']);
																			    endif;
																			?>
																		</div>
																	</td>
																	<td valign="bottom" style="padding-bottom:2px;">
																		<div style="width:100px"> 
																			<a href="<?php echo $link; ?>" style="background-color: #00ADEF; color: #FFFFFF;text-decoration:none;padding:3px;" class="more rounded-corners" target="newsletter">
																				<font style="font-size:12px;" face="<?php echo $newsletter['Newsletter']['font']; ?>">
																					&nbsp;&nbsp;&nbsp;&nbsp;Read More&nbsp;&nbsp;&nbsp;&nbsp;
																				</font>
																			</a>
																		</div>
																	</td>
																</tr>
															</table>	
														</div>
													</div>									
											  	<?php endforeach; ?>
											<?php endif; ?>
										<?php endforeach; ?>
									</div>
								<?php endif;?>	
							<?php endif; ?>
						</td>
					</tr>
				</table>
			</div>		
				
			<?php if (isset($newsletter_categories)) : ?>
				<?php if(!empty($newsletter_categories)) :?>
					<div style="min-height:20px;overflow:hidden;font-size:14px;background-color:#F3F3F3;">
						<ul style="margin:0px;padding-left:5px;">
						<?php $i=1;?>
							<?php foreach ($newsletter_categories as $category) : ?>
								<li style="font-weight:bold;float:left;list-style-type:none;width:auto;padding-top: 10px; padding-left: 5px; padding-right: 5px;">
									<div style="height:30px;font-size:12px;">
										<?php echo $category['Category']['name'];?> 
										<?php if($i<count($newsletter_categories)) :?>
											|
										<?php endif;?>
									</div>
								</li><?php $i++;?>
							<?php endforeach; ?>
						</ul>
					</div>	
				<?php endif;?>	
			<?php endif;?>	
			
		</div>
		<div style="background-color:#FFFFFF;">
			<?php if ($newsletter['Newsletter']['content_disabled'] == '0') : ?>
				<div style="text-align:left;padding-left:20px;padding-right:20px;padding-top:10px;">
					<font style="font-size:12px;" face="<?php echo $newsletter['Newsletter']['font']; ?>"><?php echo $newsletter['Newsletter']['content']; ?></font>
				</div>
			<?php endif; ?>
	
			<div style="text-align:center;padding-top:20px">
				<img src="http://<?php echo $client['Domain']['name']; ?>/files/newimg/images/cat_images/1.png" style="padding-left:10px"/>
				<img src="http://<?php echo $client['Domain']['name']; ?>/files/newimg/images/cat_images/2.png" style="padding-left:10px"/>
				<img src="http://<?php echo $client['Domain']['name']; ?>/files/newimg/images/cat_images/3.png" style="padding-left:10px"/>
				<img src="http://<?php echo $client['Domain']['name']; ?>/files/newimg/images/cat_images/4.png" style="padding-left:10px"/>
				<img src="http://<?php echo $client['Domain']['name']; ?>/files/newimg/images/cat_images/5.png" style="padding-left:10px"/>
				<img src="http://<?php echo $client['Domain']['name']; ?>/files/newimg/images/cat_images/6.png" style="padding-left:10px"/>
				<img src="http://<?php echo $client['Domain']['name']; ?>/files/newimg/images/cat_images/7.png" style="padding-left:10px"/>
				<img src="http://<?php echo $client['Domain']['name']; ?>/files/newimg/images/cat_images/8.png" style="padding-left:10px"/>
			</div>
			<div style="text-align:center;width:640px;color:#E9008A;font-size:18px;line-height:55px;">
				See also the online Coupon Books! <b>click here</b>
			</div>
		</div>
		<div style="background-color:#FFFFFF;border-top:1px solid #808080;border-bottom:1px solid #808080;font-weight:bold;">
			<table width="100%" style="text-align:center;">
				<tr>
					<td width="120px" valign="top" style="border-right:1px solid #808080;">
						<div style="color:#00AEEF;padding-bottom:10px;">Support email</div>
						<div><b>info@myrewards.com.au</b></div>
					</td>
					<td width="130px" valign="top" style="border-right:1px solid #808080;">
						<div style="color:#43C3F2;padding-bottom:10px;">Call us</div>
						<div><b>1300 368 846</b></div>
					</td>
					<td width="140px">
						<div style="color:#43C3F2;padding-bottom:3px;">Follow us..</div>
						<div>
							<a href="http://twitter.com/#!/MyRewardsIntl" style="text-decoration:none;" target="_blank">
								<img style="vertical-align:middle;border:none !important;" width="30px" src="http://<?php echo $client['Domain']['name']; ?>/files/social_icons/twitter.png" title="twitter"/>
							</a>
							<a style="text-decoration:none;" target="_blank" href="http://www.facebook.com/pages/My-Rewards-International/133544976702355?v=wall">
								<img style="vertical-align:middle;border:none !important;" width="30px" src="http://<?php echo $client['Domain']['name']; ?>/files/social_icons/fb.png" title="facebook"/>
							</a>
						</div>
					</td>
				</tr>
			</table>
		</div>
		<?php if ($newsletter['Newsletter']['footer_disabled'] == '0') : ?>
			<div style=" text-align:left;padding:10px 10px 1px 30px;color:#7D7D7D;background-color:#F3F3F3;">
				<font style="font-size:12px;" face="<?php echo $newsletter['Newsletter']['font']; ?>"><?php echo $newsletter['Newsletter']['footer_content']; ?></font>
			</div>
		<?php endif; ?>
		<?php if ($this->action != 'newsletter') : ?>
			<div style="text-align:left;padding:1px 10px 10px 30px;color:#7D7D7D;background-color:#F3F3F3;border-bottom:1px solid #808080;">
				<a style="text-decoration:none;color:#7D7D7D" href="http://<?php echo $client['Domain']['name']; ?><?php echo $this->Html->url('/newsletters/unsubscribe/'. $user['User']['email']); ?>" target="newsletter">
					<b>Click here</b>
				</a> to unsubscribe.
			</div>
		<?php endif;?>
		<?php if ($this->action != 'newsletter') : ?>
			<div style="text-align:center;background-color:#ED008C;font-size:14px;line-height:45px;">
				<a style="text-decoration:none;color:#FFFFFF" href="http://<?php echo $client['Domain']['name']; ?><?php echo $this->Html->url('/newsletters/newsletter/'.$client['Client']['id'].'/'. $newsletter['Newsletter']['id']. '/'. $user['User']['unique_id']); ?>" target="newsletter">
					View the online version
				</a>
			</div>
		<?php endif; ?>
	</div>
</div>
