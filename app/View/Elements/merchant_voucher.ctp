<style type="text/css">
.voucher_content {
	border-left: 2px solid #<?php echo $voucher['voucher_border_color']; ?>;
	border-right: 2px solid #<?php echo $voucher['voucher_border_color']; ?>;
	border-bottom: 0;
	border-top: 0;
	background: url('<?php echo  $this->Html->url(STYLE_WWW_PATH. 'voucher_background/'. $voucher['id']. '.'. $voucher['voucher_background_extension']); ?>') repeat-y top left;
}
</style>

<table  cellpadding="0" cellspacing="0" style="width:20px;" border="0">
	<tr><td><img src="<?php echo $this->Html->url(STYLE_WWW_PATH. 'voucher_top/'. $voucher['id']. '.'. $voucher['voucher_top_extension']); ?>" alt="Voucher Header" /></td></tr>
	<tr>
		<td style="vertical-align: top">
			<div class="voucher_content">
				<?php if ($this->Session->read('client.Client.client_logo_extension') != '') : ?>
					<img src="<?php echo  $this->Html->url('/images/image/client_logo/'. $this->Session->read('client.Client.id'). '.'. $this->Session->read('client.Client.client_logo_extension')); ?>" alt="" />
				<?php endif; ?>
				<h1 style="margin-top:0;"><?php echo $product['Product']['highlight']; ?></h1><br/>
				Coupon for use by: <?php echo $this->Session->read('user.User.first_name'). ' '. $this->Session->read('user.User.last_name'); ?><br/>
				Username: <?php echo $this->Session->read('user.User.username') ?><br/>
				Offer ID: <?php echo $product['Product']['id']; ?><br/>
				Offer Expires:  <?php echo date("d-M-Y", strtotime('+ 30 days')); ?><br/>
				<h2><?php echo (isset($product['MerchantAddress']['name']) && $product['MerchantAddress']['name'] != ''? $product['MerchantAddress']['name']: $product['Product']['name']); ?></h2><br/>
				<?php if ($product['Merchant']['logo_extension'] != '') : ?>
					<img src="<?php echo $this->Html->url('/images/image/merchant_logo/'. $product['Merchant']['id']. '.'. $product['Merchant']['logo_extension']); ?>" alt="" /><br/>
				<?php endif; ?>
				<?php echo ($product['MerchantAddress']['address1'] != '')? $product['MerchantAddress']['address1']. '<br/>': ''; ?>
				<?php echo ($product['MerchantAddress']['address2'] != '')? $product['MerchantAddress']['address2']. '<br/>': ''; ?>
				<?php echo ($product['MerchantAddress']['suburb'] != '')? $product['MerchantAddress']['suburb']. '<br/>': ''; ?>
				<?php if ($product['MerchantAddress']['state'] != '' || ($product['MerchantAddress']['postcode'] != '' && $product['MerchantAddress']['postcode'] != '0')) : ?>
					<?php echo ($product['MerchantAddress']['state'] != '')? $product['MerchantAddress']['state']: ''; ?>
					<?php echo ($product['MerchantAddress']['postcode'] != '')? $product['MerchantAddress']['postcode']: ''; ?>
					<br/>
				<?php endif; ?>
				<?php echo ($product['MerchantAddress']['phone'] != ''? 'Tel: '. $product['MerchantAddress']['phone']: ''); ?><br/>
				<p><?php  echo $product['Product']['terms_and_conditions']; ?></p>
				&nbsp;
			</div>
		</td>
	</tr>
	<tr><td><img src="<?php echo $this->Html->url(STYLE_WWW_PATH. 'voucher_bottom/'. $voucher['id']. '.'. $voucher['voucher_bottom_extension']); ?>" alt="voucher footer" /></td></tr>
	<tr>
		<td style="text-align:center">
			<span class="no_print"><br/><br/>
				<button type="button" onclick="window.print();return false;">Print Coupon</button>
			</span>
		</td>
	</tr>
</table>