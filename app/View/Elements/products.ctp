<?php if (!empty($products)) : ?>
<div id="products">
<form action="<?php echo $this->Html->url('/products/view'); ?>" method="post" id="products_form" onsubmit="return checkForm();">
<table>
<?php foreach($products as $product) : ?>
	<tr>
		<td style="vertical-align:top;"><input type="checkbox" name="data[Product][selected][]" value="<?php echo $product['Product']['id']; ?>" class="checkbox" /></td>
		<td>
			<a href="<?php echo $this->Html->url('/products/view/'. $product['Product']['id']. '/'. $product['MerchantAddress']['id']); ?>"><h5 style="margin-top:1px"><?php echo (isset($product['MerchantAddress']['name']) && $product['MerchantAddress']['name'] != ''? $product['MerchantAddress']['name']: $product['Product']['name']); ?></h5></a>
			<b><?php echo $product['Product']['highlight']; ?></b>
			<ul>
				<?php if ($product['MerchantAddress']['address1'] != '') : ?>
					<li><?php  echo $product['MerchantAddress']['address1']; ?></li>
				<?php endif; ?>
				<?php if ($product['MerchantAddress']['address2'] != '') : ?>
					<li><?php  echo $product['MerchantAddress']['address2']; ?></li>
				<?php endif; ?>
				<?php if ($product['MerchantAddress']['suburb'] != '') : ?>
					<li><?php  echo $product['MerchantAddress']['suburb']; ?></li>
				<?php endif; ?>
				<?php if ($product['MerchantAddress']['state'] != '' || ($product['MerchantAddress']['postcode'] != '0' && $product['MerchantAddress']['postcode'] != '')) : ?>
					<li>
						<?php if ($product['MerchantAddress']['state'] != '') : ?>
							<?php  echo $product['MerchantAddress']['state']; ?>
						<?php endif; ?>
						<?php if ($product['MerchantAddress']['postcode'] != '0' && $product['MerchantAddress']['postcode'] != '') : ?>
							 <?php  echo $product['MerchantAddress']['postcode']; ?>
						<?php endif; ?>
					</li>
				<?php endif; ?>
			</ul>
			<br/>
		</td>
	</tr>
<?php endforeach; ?>
	<tr>
		<td></td>
		<td style="whitespace:nowrap;text-align:center;">
			<style type="text/css">.disabled { display:inline; }</style>
			<?php 
				
				$url_args['url'] = $this->params['pass'];
				if (isset($this->params['url']['data']['Product']['search'])) {
					$url_args['url']['search'] = $this->params['url']['data']['Product']['search'];
				}
				
				if (isset($this->params['url']['data']['Product']['category_id'])) {
					$url_args['url']['category_id'] = $this->params['url']['data']['Product']['category_id'];
				}
				
				$paginator->options($url_args);
				
			?>
			<?php echo $paginator->prev('<< Prev', array(), null, array('class'=>'disabled'));?>
			<?php echo $paginator->numbers();?>
			<?php echo $paginator->next('Next >>', array(), null, array('class'=>'disabled'));?><br/>
			<?php echo $paginator->counter(array('format' => __('Page %page% of %pages%', true)));	?>
		</td>
	</tr>
	<tr>
		<td></td>
		<td><br/><button type="submit" id="products_submit">View selected offers &gt;&gt;</button></td>
	</tr>
	<tr>
		<td></td>
		<td><br/>Click <a href="<?php echo $this->Html->url('/products/search/'); ?>">here</a> to limit search results to your area</td>
	</tr>
</table>
</form>
<script type="text/javascript">
function checkForm() {

	var inputs = $('products_form').getInputs('checkbox');
	
	var checked = false;
	inputs.each(function(i) {
		if (i.checked == true) {
			checked = true;
			throw $break;
		}
	});

	if (!checked) {
		alert('Please select some offers.');
		return false;
	}
}
</script>
</div>
<?php endif; ?>