<style>
div.acwrap {
  position: relative;
  height: 30px;
}

div.autocomplete {
  position: absolute !important;
  top: 25px  !important;
  left: 0px !important;
  width:250px;
  margin:0;
  padding:0;
}
</style>
<div id="gogleMap" height="280px;">
	<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
	<?php
	$ip = $_SERVER['REMOTE_ADDR'];
	$i=0;
	$user_country=(unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip='.$ip))); 
	
	$default = array('type'=>'0','zoom'=>4,'lat'=>$user_country['geoplugin_latitude'],'long'=>$user_country['geoplugin_longitude']);
   	
   	echo $this->GoogleMap->map($default, $style = 'width:344px; height: 268px;margin-top:1px;');
	?>
	<div style='font-size:15px;color:red;margin-top:5px;position:relative;height:20px;'></div>
</div>

<div id="search_spinner" style="margin-left:2px;width:348px; height: 270px;margin-top:1px; float:left; display:none; margin-top:-300px; position:absolute;
filter: alpha(opacity=90); opacity: 0.8; background-color:#ffffff;">
<img style="margin-top:110px;margin-left:150px;" alt="Spinner" src="/files/newimg/wait30.gif"  />
</div>
  
<div class="acwrap"> 
	<form id="gmapform">
    	<div> 
      		<input type="text" name="maplocation" id="maplocation" class="map_input" autocomplete="off" value="" />
      		<button type="submit" class="map_button_style rounded_corners_button">Search</button>
      		<span id="indicator1" style="display: none; ">
				<img style="padding-right:80px;position:relative;bottom:23px;" align="right" src="/files/newimg/ajax-loader.gif" alt="Working..." />
			</span>
		</div>
  	</form>
</div>
<?php $tooltip = "Adelaide, SA, Australia<br>Hyderabad, AP, India<br>Makati City, NCR, Philippines<br>Lower hutt, WGN, New Zealand<br>Kowloon, Hong Kong";?>
<div class="hotspot" style="float:left" onmouseover="tooltip.show('<?php echo $tooltip?>');" onmouseout="tooltip.hide();"> 
	<img src="/files/newimg/tooltip/help16.gif" alt="" />
</div>
<div style="padding-top:3px;">Hints to help you search.</div>
<script type="text/javascript">
function positionAuto(element, entry) {
	setTimeout( function() {
		Element.clonePosition('update', 'maplocation', {
			'setWidth': false,
			'setHeight': false,
			'offsetTop': $('maplocation').offsetHeight
		});
	}, 300);
	return entry;
}

$(function() {
	$( "#maplocation" ).autocomplete({
		source: '/products/autocomplete/', minLength: 1
	});
	$("#gmapform").submit(function() {
		$.ajax({
			url: '/products/googlemap/'+$("#maplocation").val(),
			type: "GET",
			success: function (data) {
				$('#gogleMap').html(data);
			}
		});
		return false;
	});
});
</script> 

<div style="margin-top:8px;">
	<?php echo $this->element('stay_in_touch_module'); ?>
</div>