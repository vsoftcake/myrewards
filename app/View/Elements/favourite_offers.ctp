<?php if (isset($favourites)) : ?>
	<div style="overflow:hidden;width:600px;height:420px;">
		<?php foreach($favourites as $offer) : ?>	
			<?php $display_image = $offer['Product']['display_image'];?>
				<?php 
					
				$filepath = MERCHANT_LOGO_PATH;
				$filename = $offer['Merchant']['id']. '.'. $offer['Merchant']['logo_extension'];
				$path = $this->Html->url(MERCHANT_LOGO_WWW_PATH. $offer['Merchant']['id']. '.'. $offer['Merchant']['logo_extension']); 
				$path_prod = substr($path,strpos($path)+1);
				?>
			
			<?php
			if (file_exists($filepath. $filename)) :?>
				<div class="favourite_image">
					<?php $img=$this->ImageResize->getResizedDimensions($path_prod, 110, 110);?>
					<?php  if (isset($_SERVER['HTTP_USER_AGENT']) && (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 7.0') !== false)) : ?>
											<span style="width: 130px; position: relative; top: 15px;">
										<?php else: ?>
											<span style="display:table-cell;height:130px;width:130px; vertical-align:middle;text-align:center">
					<?php endif;?>
						<a href="/products/view/<?php echo $offer['Product']['id'];?>">
							<!--<img src="/image.php?url=<?php echo $path_prod;?>&width=110&height=110" alt="" />-->
							<img width="<?php echo $img['width'];?>" height="<?php echo $img['height'];?>"  src="<?php echo $this->Html->url('/'. $path_prod);?>" alt="" title="<?php echo $offer['Product']['highlight'];?>"/>
						</a>
					</span>
				</div>
			<?php endif;?>
		<?php endforeach; ?>
	</div>
<?php endif; ?>
