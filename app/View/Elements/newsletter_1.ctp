<?php // do the replacements
$nContent = $newsletter["Newsletter"]["content"];
$nContent = str_replace("[[client_name]]", $client['Client']['name'], $nContent);
$nContent = str_replace("[[client_email]]", $client['Client']['email'], $nContent);
$nContent = str_replace("[[program_name]]", $client['Program']['name'], $nContent);
$nContent = str_replace("[[web_site]]", $client['Domain']['name'], $nContent);
$nContent = str_replace("[[first_name]]", $user['User']['first_name'], $nContent);
$nContent = str_replace("[[last_name]]", $user['User']['last_name'], $nContent);
$nContent = str_replace("[[twitter_link]]", $client['Client']['twitter_link'], $nContent);
$nContent = str_replace("[[user_name]]", $user['User']['username'], $nContent);
$nContent = str_replace("[[password]]", $user['User']['password'], $nContent);
$nContent = str_replace("[[unique_id]]", $user['User']['unique_id'], $nContent);
$newsletter["Newsletter"]["content"] = $nContent;
?>
<div id="newsletter" align="center" style="font-family: <?php echo $newsletter['Newsletter']['font']; ?>;font-size:12px;<?php echo ($newsletter['Newsletter']['newsletter_background'] != '' && $newsletter['Newsletter']['newsletter_background'] != 'FFFFFF')? 'background-color: #'. $newsletter['Newsletter']['newsletter_background']. ';': "background-color: #6026AD"; ?>;padding:15px;min-height:400px;overflow:hidden;">
	<div class="bottom_rounded-corners" style="width:600px;min-height: 200px; overflow: hidden; margin-bottom:10px;margin-left:10px;background-color: #ffffff; border: 1px solid #ffffff; ">
		<?php if ($this->action != 'newsletter') :	?>
			<div style="text-align:center;<?php echo ($newsletter['Newsletter']['online_version_color'] != '' && $newsletter['Newsletter']['online_version_color'] != 'FFFFFF')? 'background-color: #'. $newsletter['Newsletter']['online_version_color']. ';': "background-color: #CBE60B"; ?>;font-size:14px;font-weight:bold;line-height:25px;">
				<a style="text-decoration:none;<?php echo ($newsletter['Newsletter']['version_text_color'] != '' && $newsletter['Newsletter']['version_text_color'] != 'FFFFFF')? 'color: #'. $newsletter['Newsletter']['version_text_color']. ';': "color: #3399FF"; ?>" href="http://<?php echo $client['Domain']['name']; ?><?php echo $this->Html->url('/newsletters/newsletter/'.$client['Client']['id'].'/'. $newsletter['Newsletter']['id']. '/'. $user['User']['unique_id']); ?>" target="newsletter">
					View the online version
				</a>
			</div>
		<?php endif; ?>
		
		<div id="header">
			<?php 
				if($newsletter['Newsletter']['dashboard_id']==1) 
				{
					$path_image = 'files/clients/newsletter_banner_image_1/'. $clientbanner['0']['ClientBanner']['newsletter_banner_image']; 
				}
				if($newsletter['Newsletter']['dashboard_id']==2) 
				{
					$path_image = 'files/clients/newsletter_banner_image_2/'. $clientbanner['0']['ClientBanner']['newsletter_banner_image']; 
				}
				if($newsletter['Newsletter']['dashboard_id']==3) 
				{
					$path_image = 'files/clients/newsletter_banner_image_3/'. $clientbanner['0']['ClientBanner']['newsletter_banner_image'];
				}
			?>
			<?php $img=$this->ImageResize->getResizedDimensions($path_image, 600);?>
			<a href="http://<?php echo $client['Domain']['name']; ?><?php echo $this->Html->url('/'); ?>" style="<?php echo ($client['Style']['link_color'] != '')? 'color: #'. $client['Style']['link_color']. ';': ''; ?>" target="newsletter">
				<img width="<?php echo $img['width'];?>" src="http://<?php echo $client['Domain']['name']; ?>/<?php echo $path_image;?>"  alt="" border="0" target="newsletter" />
			</a>
		</div>
		<?php if (isset($newsletter_categories)) : ?>
			<?php if(!empty($newsletter_categories)) :?>
				<div style="min-height:20px;overflow:hidden;<?php echo ($newsletter['Newsletter']['preview_menu_color'] != '' && $newsletter['Newsletter']['preview_menu_color'] != 'FFFFFF')? 'background-color: #'. $newsletter['Newsletter']['preview_menu_color']. ';': "background-color: #E8008A"; ?>;<?php echo ($newsletter['Newsletter']['menu_text_color'] != '' && $newsletter['Newsletter']['menu_text_color'] != 'FFFFFF')? 'color: #'. $newsletter['Newsletter']['menu_text_color']. ';': "color: #FFFFFF"; ?>">
					<ul style="margin:0px;padding-left:5px;">
					<?php $i=1;?>
						<?php foreach ($newsletter_categories as $category) : ?>
							<li style="float:left;list-style-type:none;width:auto;padding-top: 10px; padding-left: 5px; padding-right: 5px;">
								<div style="height:30px;font-size:12px;font-weight:bold">
									<?php echo $category['Category']['name'];?> 
									<?php if($i<count($newsletter_categories)) :?>
										|
									<?php endif;?>
								</div>
							</li><?php $i++;?>
						<?php endforeach; ?>
					</ul>
				</div>	
			<?php endif;?>	
		<?php endif;?>
		<?php if ($newsletter['Newsletter']['content_disabled'] == '0') : ?>
			<div style="text-align:left;padding-left:20px;padding-right:20px;padding-top:10px;">
				<font style="font-size:12px;" face="<?php echo $newsletter['Newsletter']['font']; ?>"><?php echo $newsletter['Newsletter']['content']; ?></font>
			</div>
		<?php endif; ?>

		<div style="text-align:center;padding-bottom:10px;padding-top:10px">
			<img src="http://<?php echo $client['Domain']['name']; ?>/files/newimg/images/cat_images/1.png" style="padding-left:10px"/>
			<img src="http://<?php echo $client['Domain']['name']; ?>/files/newimg/images/cat_images/2.png" style="padding-left:10px"/>
			<img src="http://<?php echo $client['Domain']['name']; ?>/files/newimg/images/cat_images/3.png" style="padding-left:10px"/>
			<img src="http://<?php echo $client['Domain']['name']; ?>/files/newimg/images/cat_images/4.png" style="padding-left:10px"/>
			<img src="http://<?php echo $client['Domain']['name']; ?>/files/newimg/images/cat_images/5.png" style="padding-left:10px"/>
			<img src="http://<?php echo $client['Domain']['name']; ?>/files/newimg/images/cat_images/6.png" style="padding-left:10px"/>
			<img src="http://<?php echo $client['Domain']['name']; ?>/files/newimg/images/cat_images/7.png" style="padding-left:10px"/>
			<img src="http://<?php echo $client['Domain']['name']; ?>/files/newimg/images/cat_images/8.png" style="padding-left:10px"/>
		</div>
	</div>
	<?php if (isset($newsletter_categories)) : ?>
		<?php if(!empty($newsletter_categories)) :?>
			<div class="rounded-corners" style="text-align:left;margin-bottom:10px;width:600px;margin-left:10px; min-height: 200px; overflow: hidden;background-color: #ffffff; border: 1px solid #ffffff;">
				<?php foreach ($newsletter_categories as $category) : ?>
					<?php if (isset($category['Product'])) : ?>
						<table style="width:100%;padding-left:50px;padding-right:50px;" cellpadding="0" cellspacing="0" border="0">
								<?php foreach($category['Product'] as $product) : ?>
									<tr>
										<td style="width:145px;padding:1em 0;">
											<div style="min-height:80px;overflow:hidden;padding-bottom:5px;">
												<?php if (trim($newsletter['Newsletter']['product_link']) == '') {
													$link = 'http://'. $client['Domain']['name']. '/products/view/'. $product['Product']['id'];
												} else {
													$link = trim($newsletter['Newsletter']['product_link']);
												}
												?>
												<a href="<?php echo $link; ?>" target="newsletter">
													<font style="font-size:12px;" face="<?php echo $newsletter['Newsletter']['font']; ?>">
														<img src="http://<?php echo $client['Domain']['name']; ?><?php echo $this->Html->url('/images/product/'. $product['Product']['id']. '.'. $product['Product']['image_extension']. '/no_logo'); ?>" alt="" border="" />
													</font>
												</a>
											</div>
											<div align="center">
												<?php if (trim($newsletter['Newsletter']['product_link']) == '') {
												$link = 'http://'. $client['Domain']['name']. '/products/view/'. $product['Product']['id'];
											} else {
												$link = trim($newsletter['Newsletter']['product_link']);
											}
											?>
												<a href="<?php echo $link; ?>" style="
													background-color: #00ADEF;
													color: #FFFFFF;
													text-decoration:none;
													padding:3px;
													" class="more rounded-corners" target="newsletter">
														<font style="font-size:12px;" face="<?php echo $newsletter['Newsletter']['font']; ?>">
															&nbsp;&nbsp;&nbsp;&nbsp;Read More&nbsp;&nbsp;&nbsp;&nbsp;
														</font>
													</a>
											</div>
										</td>
										<td style="padding:1em 0 1em 25px;vertical-align:top;">
											<?php if (trim($newsletter['Newsletter']['product_link']) == '') {
												$link = 'http://'. $client['Domain']['name']. '/products/view/'. $product['Product']['id'];
											} else {
												$link = trim($newsletter['Newsletter']['product_link']);
											}
											?>
											<a href="<?php echo $link; ?>"
												target="newsletter" class="item_header" style="text-decoration:none;color: #5F2A93;
												<?php echo ($client['Style']['h4_size'] != '')? 'size: '. $client['Style']['h4_size']. ';': ""; ?>">
												<b>
													<font style="font-size:14px;" face="<?php echo $newsletter['Newsletter']['font']; ?>">
														<?php echo ($product['Product']['special_offer_headline'] != ''? $product['Product']['special_offer_headline']: $product['Product']['name']); ?>
													</font>
												</b>
											</a>
											<br/>
											<p>
												<font style="font-size:13px;" face="<?php echo $newsletter['Newsletter']['font']; ?>">
													<?php  if  ($product['Product']['special_offer'] == '1') : ?>
														<?php  echo $product['Product']['special_offer_body']; ?>
													<?php else : ?>
														<span style="color:#33B1D2""><b><?php  echo $product['Product']['highlight']; ?></b></span><br/><br/>
														<?php  echo $product['Product']['details']; ?>
													<?php endif; ?>
												</font>
											</p>
											
										</td>
									</tr>
									<tr>
										<td colspan="2">
											<hr>
										</td>
									</tr>
								<?php endforeach; ?>
							</table>
					<?php endif; ?>
				<?php endforeach; ?>
			</div>
		<?php endif;?>	
	<?php endif; ?>
	<div style="margin-bottom:10px;margin-left:10px;width:600px;text-align:center;<?php echo ($newsletter['Newsletter']['coupon_book_color'] != '' && $newsletter['Newsletter']['coupon_book_color'] != 'FFFFFF')? 'background-color: #'. $newsletter['Newsletter']['coupon_book_color']. ';': "background-color: #EEF276"; ?>;<?php echo ($newsletter['Newsletter']['book_text_color'] != '' && $newsletter['Newsletter']['book_text_color'] != 'FFFFFF')? 'color: #'. $newsletter['Newsletter']['book_text_color']. ';': "color: #3399FF"; ?>;font-size:18px;line-height:45px;">
		See also the online Coupon Books! 
		
		<?php if(!empty($newsletter['Newsletter']['coupon_book_link'])) : ?>
			<?php 
				$linkurl = '';
				if (false === strpos($newsletter['Newsletter']['coupon_book_link'], '://')) 
				{
					$linkurl = 'http://'.$newsletter['Newsletter']['coupon_book_link'];
				}
				else
				{
					$linkurl = $newsletter['Newsletter']['coupon_book_link'];
				}
			?>
			<a style="text-decoration:none;<?php echo ($newsletter['Newsletter']['book_text_color'] != '' && $newsletter['Newsletter']['book_text_color'] != 'FFFFFF')? 'color: #'. $newsletter['Newsletter']['book_text_color']. ';': "color: #E9008A"; ?>" href="<?php echo $linkurl;?>" target="_blank"><b>click here</b></a>
		<?php endif; ?>
	</div>
	<div style="text-align:center;margin-bottom:10px;margin-left:10px;">
		<?php 
			$filepath1 = NEWSLETTER_IMAGE1_PATH;
			$filename1 = $newsletter['Newsletter']['id']. '.'. $newsletter['Newsletter']['image1_ext'];
			$path1 = $this->Html->url(NEWSLETTER_IMAGE1_WWW_PATH. $newsletter['Newsletter']['id']. '.'. $newsletter['Newsletter']['image1_ext']); 
			$path_image1 = substr($path1,strpos($path1)+1);
			
			$filepath2 = NEWSLETTER_IMAGE2_PATH;
			$filename2 = $newsletter['Newsletter']['id']. '.'. $newsletter['Newsletter']['image2_ext'];
			$path2 = $this->Html->url(NEWSLETTER_IMAGE2_WWW_PATH. $newsletter['Newsletter']['id']. '.'. $newsletter['Newsletter']['image2_ext']); 
			$path_image2 = substr($path2,strpos($path2)+1);
			
			/*$filepath3 = NEWSLETTER_IMAGE3_PATH;
			$filename3 = $newsletter['Newsletter']['id']. '.'. $newsletter['Newsletter']['image3_ext'];
			$path3 = $this->Html->url(NEWSLETTER_IMAGE3_WWW_PATH. $newsletter['Newsletter']['id']. '.'. $newsletter['Newsletter']['image3_ext']); 
			$path_image3 = substr($path3,strpos($path3)+1);
			
			$filepath4 = NEWSLETTER_IMAGE4_PATH;
			$filename4 = $newsletter['Newsletter']['id']. '.'. $newsletter['Newsletter']['image4_ext'];
			$path4 = $this->Html->url(NEWSLETTER_IMAGE4_WWW_PATH. $newsletter['Newsletter']['id']. '.'. $newsletter['Newsletter']['image4_ext']); 
			$path_image4 = substr($path4,strpos($path4)+1);*/
		?>
		<?php if (file_exists($filepath1. $filename1)) :?>
			<?php $img=$this->ImageResize->getResizedDimensions($path_image1, 250, 170);?>
			<img width="<?php echo $img['width'];?>" height="<?php echo $img['height'];?>"  src="http://<?php echo $client['Domain']['name']; ?>/<?php echo $path_image1;?>" style="padding-right:10px;"/>
		<?php else : ?>
			<img src="http://<?php echo $client['Domain']['name']; ?>/files/newimg/images/img/mobile1.png" style="padding-right:10px;"/>
		<?php endif;?>
		
		<?php if (file_exists($filepath2. $filename2)) :?>
			<?php $img=$this->ImageResize->getResizedDimensions($path_image2, 250, 170);?>
			<img width="<?php echo $img['width'];?>" height="<?php echo $img['height'];?>"  src="http://<?php echo $client['Domain']['name']; ?>/<?php echo $path_image2;?>" style="padding-right:10px;"/>
		<?php else : ?>
			<img src="http://<?php echo $client['Domain']['name']; ?>/files/newimg/images/img/temp_connect1.png" />
		<?php endif;?>
	</div>
	
	<div style="margin-bottom:10px;margin-left:10px;text-align:center;width:600px;<?php echo ($newsletter['Newsletter']['call_color'] != '' && $newsletter['Newsletter']['call_color'] != 'FFFFFF')? 'background-color: #'. $newsletter['Newsletter']['call_color']. ';': "background-color: #7B47B2"; ?>;<?php echo ($newsletter['Newsletter']['call_text_color'] != '' && $newsletter['Newsletter']['call_text_color'] != 'FFFFFF')? 'color: #'. $newsletter['Newsletter']['call_text_color']. ';': "color: #F9F7F7"; ?>;font-size:15px;line-height:45px;">
		Call <?php if(!empty($newsletter['Newsletter']['phone_number'])) : echo $newsletter['Newsletter']['phone_number']; else : echo "1300 368 846"; endif;?> for further information or search for more fabulous offers online.
	</div>
	<?php if ($newsletter['Newsletter']['footer_disabled'] == '0') : ?>
		<div style="text-align:left;width: 560px; margin-left: 20px;color:#<?php echo $newsletter['Newsletter']['newsletter_text_color']; ?>;padding:1px;">
			<font style="font-size:12px;" face="<?php echo $newsletter['Newsletter']['font']; ?>"><?php echo $newsletter['Newsletter']['footer_content']; ?></font>
		</div>
	<?php endif; ?>
	<?php if ($this->action != 'newsletter') : ?>
		<div style="margin-bottom:10px;width: 560px; text-align:left;margin-left: 20px;<?php echo ($newsletter['Newsletter']['newsletter_text_color'] != '' && $newsletter['Newsletter']['newsletter_text_color'] != 'FFFFFF')? 'color: #'. $newsletter['Newsletter']['newsletter_text_color']. ';': "color: #E5E3E3"; ?>;">
			<a style="text-decoration:none;<?php echo ($newsletter['Newsletter']['newsletter_text_color'] != '' && $newsletter['Newsletter']['newsletter_text_color'] != 'FFFFFF')? 'color: #'. $newsletter['Newsletter']['newsletter_text_color']. ';': "color: #E5E3E3"; ?>" href="http://<?php echo $client['Domain']['name']; ?><?php echo $this->Html->url('/newsletters/unsubscribe/'. $user['User']['email']); ?>" target="newsletter">
				<b>Click here</b>
			</a> to unsubscribe.
		</div>
	<?php endif;?>
</div>