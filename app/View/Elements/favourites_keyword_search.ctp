<script>
function resetdata()
{
	document.getElementById("s_content").style.display="none";
	$.ajax({
		url: '/products/reset_data',
		type: "GET",
		success: function (data) {
			$('#searchDiv').html(data);
		}
	});
}
$(function() {
	$( "#ProductCountryId" ).change(function() {
		if($(this).val()=='Hong Kong')
			$('#stId').hide();
		else
			$('#stId').show();
		if($(this).val()!='Australia') {
			$('#pId').hide();
			$('#rId').hide();
		}else{
			$('#pId').show();
			$('#rId').show();
		}
	});
});
</script>
<div class="module2">
	<div class="leftcolumn">
	  	<div class="t3 search_content ">
	  		<div id="searchDiv">
	  		<div class="ct1" id="s_content" style="width:950px;">
				<form action="<?php echo $this->Html->url('/products/search2'); ?>" method="get">
				<table style="width:950px;">
					<tr>
						<td>
							<div style="width:230px;float:left;padding-right:10px">
								<table style="width:100%">
									<tr style="height:32px;">
										<td style="width:80px;">Country</td>
										<td>
											<?php 
											echo $this->Form->input('Product.country_id', array('type'=>'select', 'label'=>false, 'options'=>$this->Session->read('client.Countries'),'default'=>$this->Session->read('params.country_id'),'class'=>'select_input'));
											echo $this->Ajax->observeField('ProductCountryId', array('url' => '/products/update_select','update' => 'ProductSearchStateId'));
											?>
										</td>
									</tr>
									<tr style="height:32px;">
										<td style="width:80px;">Category</td>
										<td>
											<?php echo $this->Form->input('Product.category_id', array('type'=>'select', 'label'=>false, 'options'=>$this->Session->read('catlist'),'default'=>$this->Session->read('params.category_id'), 'empty'=>'Select category','class'=>'select_input'));
											echo $this->Ajax->observeField('ProductCountryId', array('url' => '/products/update_cat','update' => 'ProductCategoryId'));?>
										</td>
									</tr>
								</table>
							</div>
							<div style="width:230px;float:left;padding-right:10px">
								<table style="width:100%">
									<?php 
										$cStyle="display:none";
										if(empty($ctry)):
											if($this->Session->read('params.country_id') == '') :
												if ($this->Session->read('client.Client.country') != 'Hong Kong') :
													$cStyle="inline";
												endif;
											else:
												if($this->Session->read('params.country_id') != 'Hong Kong') :
												 	$cStyle="inline";
												endif;
											endif;
										endif;
									?>
									<tr style="height:32px;<?php echo $cStyle;?>" id="stId">
										<td style="width:70px;">State</td>
										<td>
											<?php echo $this->Form->input('Product.search_state_id', array('type'=>'select', 'label'=>false, 'options'=>$this->Session->read('statelist'),'default'=>$this->Session->read('params.search_state_id'),'empty'=>'Select State','class'=>'select_input'));?>
										</td>
									</tr>
									<tr style="height:32px;">
										<td>Keyword/s</td>
										<?php $title="Keywords refer to search terms that help you find exactly what you are looking for. For example if you want to find a pizza shop in an area then use the keyword 'pizza'. Keep refining the keywords until  you find exactly what you want."; ?>
					                   	<td> 
						                    <div title="<?php  echo $title; ?>">
						                    	<?php echo $this->Form->input('Product.keywords',array('class'=>'search_input','value'=>$this->Session->read('params.keywords'),'div'=>false,'label'=>false));  ?> 
						                    </div>
					                   </td>
									</tr>
								</table>
							</div>
							<div style="width:280px;float:left;padding-right:10px">
								<table style="width:100%">
									<?php 
										$pStyle="display:none";
										if(empty($ptry)):
											if($this->Session->read('params.country_id') == '') :
												if (($this->Session->read('client.Client.country') == 'Australia')) :
												 	$pStyle="";
												endif;
											else:
												if($this->Session->read('params.country_id') == 'Australia') :
												 	$pStyle="";
												endif;
											endif;
										endif;
									
									?>
									<tr style="height:32px;<?php echo $pStyle;?>" id="pId">
										<td style="width:140px;">Suburb / Postcode </td>
										<td> <?php echo $this->Form->input('Product.location',array('class'=>'search_input','value'=>$this->Session->read('params.location'),'div'=>false,'label'=>false)); ?> </td>
									</tr>
									<?php 
										$rStyle="display:none";
										if(empty($rtry)):
											if($this->Session->read('params.country_id') == '') :
												if ($this->Session->read('client.Client.country') == 'Australia') :
												 	$rStyle="";
												endif;
											else:
												if($this->Session->read('params.country_id') == 'Australia') :
												 	$rStyle="";
												endif;
											endif;
										endif;
									
									?>
									<tr style="height:32px;<?php echo $rStyle;?>" id="rId">
										<td>
											Radius Search
										</td>
										<td>
											<?php 
											$o= array(""=>" ","3" => "3 km", "5" => "5 km", "10" => "10 km", "15" => "15 km", "20" => "20 km", "25" => "25 km", "30" => "30 km");
											?> 
											
											<?php echo $this->Form->input('Product.distance', array('type'=>'select', 'div'=>false, 'label'=>false, 'options'=>$o,'default'=>$this->Session->read('params.distance'),'empty'=>' '));?> approx
										</td>
									</tr>
								</table>
							</div>
							<div style="float:left;">
								<table style="width:100%">
									<tr style="height:20px;">
										<td colspan="2">
											<button type="submit" class="button_style rounded_corners_button">Go !</button>
											<button type="button" style="padding:3px;margin-left:10px" class="rounded_corners_button" onclick="resetdata()">Clear</button>
										</td>
									</tr>
								</table>
							</div>
						</td>
						<td>
							<div style="position: relative; right: 15px; bottom: 15px;">
								<img src="/files/newimg/icon_close.png" onclick="SUDEffect('s_content')" style="cursor:pointer"/>
							</div>
						</td>
					</tr>
				</table>
				</form>
				<div style="height:1px"></div>
	 		</div></div>
		</div>  
	</div>
</div>	 