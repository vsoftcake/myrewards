<script>
function resetdata()
{
	document.getElementById("s_content").style.display="none";
	$.ajax({
		url: '/products/reset_data',
		type: "GET",
		success: function (data) {
			$('#searchDiv').html(data);
		}
	});
}
</script>
<div class="module2">
	<div class="leftcolumn">
		<div class="t3 search_content">
			<div id="searchDiv">
			<div class="ct1" id="s_content" style="width:950px;">
				<?php echo $this->element('keyword_search_module'); ?>
				<div style="height:1px"></div>
			</div></div> 
		</div>  
	</div>
</div>