<?php // do the replacements
$nContent = $newsletter["Newsletter"]["content"];
$nContent = str_replace("[[client_name]]", $client['Client']['name'], $nContent);
$nContent = str_replace("[[client_email]]", $client['Client']['email'], $nContent);
$nContent = str_replace("[[program_name]]", $client['Program']['name'], $nContent);
$nContent = str_replace("[[web_site]]", $client['Domain']['name'], $nContent);
$nContent = str_replace("[[first_name]]", $user['User']['first_name'], $nContent);
$nContent = str_replace("[[last_name]]", $user['User']['last_name'], $nContent);
$nContent = str_replace("[[twitter_link]]", $client['Client']['twitter_link'], $nContent);
$nContent = str_replace("[[user_name]]", $user['User']['username'], $nContent);
$nContent = str_replace("[[password]]", $user['User']['password'], $nContent);
$nContent = str_replace("[[unique_id]]", $user['User']['unique_id'], $nContent);
$newsletter["Newsletter"]["content"] = $nContent;
?>
<div id="newsletter" style="font-family: <?php echo $newsletter['Newsletter']['font']; ?>;font-size:12px;text-align:center;">
<table style="text-align:left;margin-left:auto;margin-right:auto;width:600px;">
	<tr>
		<td>
		<div id="header">
		<?php 
			 if($newsletter['Newsletter']['dashboard_id']==1) 
			 {
		?>		
				<a href="http://<?php echo $client['Domain']['name']; ?><?php echo $this->Html->url('/'); ?>" style="<?php echo ($client['Style']['link_color'] != '')? 'color: #'. $client['Style']['link_color']. ';': ''; ?>" target="newsletter"><img src="http://<?php echo $client['Domain']['name']; ?><?php echo $this->Html->url('/files/clients/newsletter_banner_image_1/'. $clientbanner['0']['ClientBanner']['newsletter_banner_image']); ?>" alt="" border="0" target="newsletter" /></a>
		<?php	 
			}
			 if($newsletter['Newsletter']['dashboard_id']==2) 
			 {
		?>
				<a href="http://<?php echo $client['Domain']['name']; ?><?php echo $this->Html->url('/'); ?>" style="<?php echo ($client['Style']['link_color'] != '')? 'color: #'. $client['Style']['link_color']. ';': ''; ?>" target="newsletter"><img src="http://<?php echo $client['Domain']['name']; ?><?php echo $this->Html->url('/files/clients/newsletter_banner_image_2/'. $clientbanner['0']['ClientBanner']['newsletter_banner_image']); ?>" alt="" border="0" target="newsletter" /></a>
		<?php	
			}
			 if($newsletter['Newsletter']['dashboard_id']==3) 
			 {
		?>
				<a href="http://<?php echo $client['Domain']['name']; ?><?php echo $this->Html->url('/'); ?>" style="<?php echo ($client['Style']['link_color'] != '')? 'color: #'. $client['Style']['link_color']. ';': ''; ?>" target="newsletter"><img src="http://<?php echo $client['Domain']['name']; ?><?php echo $this->Html->url('/files/clients/newsletter_banner_image_3/'. $clientbanner['0']['ClientBanner']['newsletter_banner_image']); ?>" alt="" border="0" target="newsletter" /></a>
		<?php
			}
		?>
				
	</div>

	<?php if ($newsletter['Newsletter']['date_disabled'] == '0') : ?>
		<div id="date"><font style="font-size:12px;" face="<?php echo $newsletter['Newsletter']['font']; ?>"><?php echo date('l j F Y', strtotime($newsletter['Newsletter']['modified'])); ?></font></div>
	<?php endif; ?>
	<?php if ($newsletter['Newsletter']['h1_disabled'] == '0') : ?>
		<div id="h1"><h1 style="font-size: 20px;<?php echo ($client['Style']['h1_color'] != '')? 'color: #'. $client['Style']['h1_color']. ';': ""; ?><?php echo ($client['Style']['h1_size'] != '')? 'size: '. $client['Style']['h1_size']. ';': "size:20px;"; ?>"><font face="<?php echo $newsletter['Newsletter']['font']; ?>"><?php echo $newsletter['Newsletter']['h1']; ?></font></h1></div>
	<?php endif; ?>
	<?php if ($newsletter['Newsletter']['h2_disabled'] == '0') : ?>
		<div id="h2"><h2 style="font-size: 18px;<?php echo ($client['Style']['h2_color'] != '')? 'color: #'. $client['Style']['h2_color']. ';': ""; ?><?php echo ($client['Style']['h2_size'] != '')? 'size: '. $client['Style']['h2_size']. ';': "size:18px;"; ?>"><font face="<?php echo $newsletter['Newsletter']['font']; ?>"><?php echo $newsletter['Newsletter']['h2']; ?></font></h2></div>
	<?php endif; ?>
	<?php if ($newsletter['Newsletter']['h3_disabled'] == '0') : ?>
		<div id="h3"><h3 style="font-size: 16px;<?php echo ($client['Style']['h3_color'] != '')? 'color: #'. $client['Style']['h3_color']. ';': ""; ?><?php echo ($client['Style']['h3_size'] != '')? 'size: '. $client['Style']['h3_size']. ';': "size:16px;"; ?>"><font face="<?php echo $newsletter['Newsletter']['font']; ?>"><?php echo $newsletter['Newsletter']['h3']; ?></font></h3></div>
	<?php endif; ?>
	<?php if ($newsletter['Newsletter']['content_disabled'] == '0') : ?>
		<div id="ggggg"><font style="font-size:12px;" face="<?php echo $newsletter['Newsletter']['font']; ?>"><?php echo $newsletter['Newsletter']['content']; ?></font></div>
	<?php endif; ?>

	<?php if ($newsletter['Newsletter']['products_short_disabled'] == '0') : ?>
	<div id="products_short">
		<h1 style="font-size: 20px;<?php echo ($client['Style']['h1_color'] != '')? 'color: #'. $client['Style']['h1_color']. ';': ""; ?><?php echo ($client['Style']['h1_size'] != '')? 'size: '. $client['Style']['h1_size']. ';': "size:20px;"; ?>"><font face="<?php echo $newsletter['Newsletter']['font']; ?>"><?php echo $newsletter['Newsletter']['products_short_title']; ?></font></h1>
		<?php if (isset($newsletter_short_products)) : ?>
		<?php foreach ($newsletter_short_products as $product) : ?>
			<?php if (trim($newsletter['Newsletter']['product_link']) == '') {
				$link = 'http://'. $client['Domain']['name']. '/products/view/'. $product['Product']['id'];
			} else {
				$link = trim($newsletter['Newsletter']['product_link']);
			}
			?>
			<a href="<?php echo $link; ?>" style="<?php echo ($client['Style']['link_color'] != '')? 'color: #'. $client['Style']['link_color']. ';': ''; ?>" target="newsletter"><font style="font-size:12px;" face="<?php echo $newsletter['Newsletter']['font']; ?>"><?php echo $product['Product']['name']; ?></font></a> <font style="font-size:12px;" face="<?php echo $newsletter['Newsletter']['font']; ?>"><?php echo $product['Product']['highlight']; ?></font><br/>
		<?php endforeach; ?>
		<?php endif ;?>
	</div>
	<?php endif; ?>

	<?php if ($newsletter['Newsletter']['products_disabled'] == '0') : ?>
	<h1 style="font-size: 20px;<?php echo ($client['Style']['h1_color'] != '')? 'color: #'. $client['Style']['h1_color']. ';': ""; ?><?php echo ($client['Style']['h1_size'] != '')? 'size: '. $client['Style']['h1_size']. ';': "size:20px;"; ?>"><font style="font-size:12px;" face="<?php echo $newsletter['Newsletter']['font']; ?>"><?php echo $newsletter['Newsletter']['products_title']; ?></font></h1>
		<div id="products">
		<?php if (isset($newsletter_categories)) : ?>
	
			<?php foreach ($newsletter_categories as $category) : ?>
				<table style="width:100%;border: 2px solid #<?php echo $client['Style']['border_color']; ?>;" cellpadding="0" cellspacing="0" border="0">
					<tr style="background-color: #<?php echo $client['Style']['tab_background_color']; ?>;color: #<?php echo $client['Style']['tab_menu_color']; ?>;">
						<td style="width:14px">&nbsp;</td>
						<td style="line-height:18px;vertical-align:middle;"><b><font style="font-size:12px;" face="<?php echo $newsletter['Newsletter']['font']; ?>"><?php echo $category['Category']['name']; ?></font></b></td>
						<td style="width:14px">&nbsp;</td>
					</tr>
					<tr style="background-color: #<?php echo $client['Style']['tab_content_background_color']; ?>;">
						<td style="width:14px">&nbsp;</td>
						<td>
							<?php if (isset($category['Product'])) : ?>
							<table style="width:100%;" id="special_offers" cellpadding="0" cellspacing="0" border="0">
							<?php $border_top = ''; ?>
							<?php foreach($category['Product'] as $product) : ?>
								<tr>
									<td style="<?php if ($border_top != '') { echo "border-top: 1px solid #". $border_top. ";"; } ?>width:1%;vertical-align:top;padding:1em 0;">
										<?php if (trim($newsletter['Newsletter']['product_link']) == '') {
											$link = 'http://'. $client['Domain']['name']. '/products/view/'. $product['Product']['id'];
										} else {
											$link = trim($newsletter['Newsletter']['product_link']);
										}
										?>
										<a href="<?php echo $link; ?>" target="newsletter">
											<font style="font-size:12px;" face="<?php echo $newsletter['Newsletter']['font']; ?>">
												<img src="http://<?php echo $client['Domain']['name']; ?><?php echo $this->Html->url('/images/product/'. $product['Product']['id']. '.'. $product['Product']['image_extension']. '/no_logo'); ?>" alt="" border="" />
											</font>
										</a>
									</td>
									<td style="padding:1em 0 1em 4px;<?php if ($border_top != '') { echo "border-top: 1px solid #". $border_top. ";"; } ?>vertical-align:top;">
										<?php if (trim($newsletter['Newsletter']['product_link']) == '') {
											$link = 'http://'. $client['Domain']['name']. '/products/view/'. $product['Product']['id'];
										} else {
											$link = trim($newsletter['Newsletter']['product_link']);
										}
										?>
										<a href="<?php echo $link; ?>"
											target="newsletter" class="item_header" style="text-decoration:none;<?php echo ($client['Style']['h4_color'] != '')? 'color: #'. $client['Style']['h4_color']. ';': ""; ?>
		<?php echo ($client['Style']['h4_size'] != '')? 'size: '. $client['Style']['h4_size']. ';': ""; ?>">
											<b>
												<font style="font-size:12px;" face="<?php echo $newsletter['Newsletter']['font']; ?>">
													<?php echo ($product['Product']['special_offer_headline'] != ''? $product['Product']['special_offer_headline']: $product['Product']['name']); ?>
												</font>
											</b>
										</a>
										<br/>
										<p>
											<font style="font-size:12px;" face="<?php echo $newsletter['Newsletter']['font']; ?>">
												<?php  if  ($product['Product']['special_offer'] == '1') : ?>
													<?php  echo $product['Product']['special_offer_body']; ?>
												<?php else : ?>
													<b><?php  echo $product['Product']['highlight']; ?></b><br/><br/>
													<?php  echo $product['Product']['details']; ?>
												<?php endif; ?>
											</font>
										</p>
										<div style="text-align:right;margin-bottom:10px;">
											<?php if (trim($newsletter['Newsletter']['product_link']) == '') {
											$link = 'http://'. $client['Domain']['name']. '/products/view/'. $product['Product']['id'];
										} else {
											$link = trim($newsletter['Newsletter']['product_link']);
										}
										?>
											<a href="<?php echo $link; ?>" style="
												border:1px solid #<?php echo $client['Style']['button_border_color']; ?>;
												background-color: #<?php echo $client['Style']['button_background_color']; ?>;
												color: #<?php echo $client['Style']['button_color']; ?>;
												text-decoration:none;
												font-size:11px
												" class="more" target="newsletter"><font style="font-size:12px;" face="<?php echo $newsletter['Newsletter']['font']; ?>">&nbsp;&nbsp;&nbsp;&nbsp;More &gt;&gt;&nbsp;&nbsp;&nbsp;&nbsp;</font></a>
										</div>
									</td>
								</tr>
								<?php $border_top = $client['Style']['border_color']; ?>
							<?php endforeach; ?>
							</table>
							<?php endif; ?>
						</td>
						<td style="width:14px">&nbsp;</td>
					</tr>
				</table>
					<br/>
				<?php endforeach; ?>
			<?php endif; ?>
		</div>
	<?php endif; ?>
	
	<?php if ($newsletter['Newsletter']['show_and_save_disabled'] == '0') : ?>
	<div id="show_and_save">
		<h1 style="font-size: 20px;<?php echo ($client['Style']['h1_color'] != '')? 'color: #'. $client['Style']['h1_color']. ';': ""; ?><?php echo ($client['Style']['h1_size'] != '')? 'size: '. $client['Style']['h1_size']. ';': "size:20px;"; ?>"><font style="font-size:12px;" face="<?php echo $newsletter['Newsletter']['font']; ?>"><?php echo $newsletter['Newsletter']['show_and_save_title']; ?></font></h1>
		<table cellpadding="0" cellspacing="0" style="width:100%">
			<tr>
				<td style="text-align:center"><a style="" href="<?php echo $newsletter['Newsletter']['show_and_save1_link']; ?>" style="<?php echo ($client['Style']['link_color'] != '')? 'color: #'. $client['Style']['link_color']. ';': ''; ?>" target="newsletter"><font style="font-size:12px;" face="<?php echo $newsletter['Newsletter']['font']; ?>"><?php echo $newsletter['Newsletter']['show_and_save1']; ?></font></a></td>
				<td style="text-align:center"><a style="" href="<?php echo $newsletter['Newsletter']['show_and_save2_link']; ?>" style="<?php echo ($client['Style']['link_color'] != '')? 'color: #'. $client['Style']['link_color']. ';': ''; ?>" target="newsletter"><font style="font-size:12px;" face="<?php echo $newsletter['Newsletter']['font']; ?>"><?php echo $newsletter['Newsletter']['show_and_save2']; ?></font></a></td>
				<td style="text-align:center"><a style="" href="<?php echo $newsletter['Newsletter']['show_and_save3_link']; ?>" style="<?php echo ($client['Style']['link_color'] != '')? 'color: #'. $client['Style']['link_color']. ';': ''; ?>" target="newsletter"><font style="font-size:12px;" face="<?php echo $newsletter['Newsletter']['font']; ?>"><?php echo $newsletter['Newsletter']['show_and_save3']; ?></font></a></td>
			</tr>
			<tr>
				<td style="text-align:center"><a style="" href="<?php echo $newsletter['Newsletter']['show_and_save4_link']; ?>" style="<?php echo ($client['Style']['link_color'] != '')? 'color: #'. $client['Style']['link_color']. ';': ''; ?>" target="newsletter"><font style="font-size:12px;" face="<?php echo $newsletter['Newsletter']['font']; ?>"><?php echo $newsletter['Newsletter']['show_and_save4']; ?></font></a></td>
				<td style="text-align:center"><a style="" href="<?php echo $newsletter['Newsletter']['show_and_save5_link']; ?>" style="<?php echo ($client['Style']['link_color'] != '')? 'color: #'. $client['Style']['link_color']. ';': ''; ?>" target="newsletter"><font style="font-size:12px;" face="<?php echo $newsletter['Newsletter']['font']; ?>"><?php echo $newsletter['Newsletter']['show_and_save5']; ?></font></a></td>
				<td style="text-align:center"><a style="" href="<?php echo $newsletter['Newsletter']['show_and_save6_link']; ?>" style="<?php echo ($client['Style']['link_color'] != '')? 'color: #'. $client['Style']['link_color']. ';': ''; ?>" target="newsletter"><font style="font-size:12px;" face="<?php echo $newsletter['Newsletter']['font']; ?>"><?php echo $newsletter['Newsletter']['show_and_save6']; ?></font></a></td>
			</tr>
		</table>
	</div>
	<?php endif; ?>

	<?php if ($newsletter['Newsletter']['whats_changed_disabled'] == '0') : ?>
	<div id="new_products">
		<h1 style="font-size: 20px;<?php echo ($client['Style']['h1_color'] != '')? 'color: #'. $client['Style']['h1_color']. ';': ""; ?><?php echo ($client['Style']['h1_size'] != '')? 'size: '. $client['Style']['h1_size']. ';': "size:20px;"; ?>"><font style="font-size:12px;" face="<?php echo $newsletter['Newsletter']['font']; ?>"><?php echo $newsletter['Newsletter']['whats_changed_title']; ?></font></h1>
		<a href="http://<?php echo $client['Domain']['name']; ?><?php echo $this->Html->url('/display/whats_new'); ?>" style="<?php echo ($client['Style']['link_color'] != '')? 'color: #'. $client['Style']['link_color']. ';': ''; ?>" target="newsletter"><font style="font-size:12px;" face="<?php echo $newsletter['Newsletter']['font']; ?>">Whats new link</font></a>
	</div>
	<?php endif; ?>
	<br/>
	<?php if ($newsletter['Newsletter']['footer_disabled'] == '0') : ?>
	<div id="footer"><font style="font-size:12px;" face="<?php echo $newsletter['Newsletter']['font']; ?>"><?php echo $newsletter['Newsletter']['footer_content']; ?></font></div>
	<?php endif; ?>

	<?php	// do not display unsubscribe info for online version
		if ($this->action != 'newsletter') :
	?>
	<div id="unsubscribe">
		<font style="font-size:12px;" face="<?php echo $newsletter['Newsletter']['font']; ?>">
			Click <a href="http://<?php echo $client['Domain']['name']; ?><?php echo $this->Html->url('/newsletters/unsubscribe/'. $user['User']['email']); ?>" style="<?php echo ($client['Style']['link_color'] != '')? 'color: #'. $client['Style']['link_color']. ';': ''; ?>" target="newsletter">here</a> to unsubscribe<br/>
			Click <a href="http://<?php echo $client['Domain']['name']; ?><?php echo $this->Html->url('/newsletters/newsletter/'.$client['Client']['id'].'/'. $newsletter['Newsletter']['id'].'/'.$newsletter['Newsletter']['dashboard_id']. '/'. $user['User']['unique_id']); ?>" style="<?php echo ($client['Style']['link_color'] != '')? 'color: #'. $client['Style']['link_color']. ';': ''; ?>" target="newsletter">here</a> for online version
		</font>
	</div>
	<?php endif; ?>
		</td>
	</tr>
</table>
</div>