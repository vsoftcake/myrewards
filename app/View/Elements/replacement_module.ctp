<?php if(($module_data['replacement']['ClientPage']['enabled'] == 1 || $module_data['replacement']['ClientPage']['enabled_override'] == 1) && ($module_data['replacement']['ClientPage']['page_id'] == 73)): ?>
	<?php echo $this->element('map_replacement'); ?> 
<?php else: ?>
	<?php echo $this->element('top_module', array('module' => 'googlemap', 'title' => 'What\'s Around You')); ?> 
<?php endif;?>