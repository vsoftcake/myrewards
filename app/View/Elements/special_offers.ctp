<?php echo $this->Html->script('jquery.cycle2.min');?>
<?php if (isset($special_offers) && count($special_offers)>0) : $i=1;?>
<div id="slideshow-1" style="position:relative">
<div style="float:left;margin-top:100px;margin-right:5px"><a href="#" class="cycle-prev"><img src="/img/br_prev.png" width="22"></a></div>
<div style="float:left" class="cycle-slideshow" data-cycle-fx="scrollHorz" data-cycle-timeout="1000000" data-cycle-speed="1000" data-cycle-slides="> div" data-cycle-pause-on-hover="true"
data-cycle-prev="#slideshow-1 .cycle-prev" data-cycle-next="#slideshow-1 .cycle-next">
	<div>
		<?php foreach($special_offers as $product) : 
		if($i==5){echo "</div><div>";$i=1;}$i++;?>
		<div align="center" id="<?php echo $product['Product']['id']; ?>" class="module37" onmouseover="changeBgImg('<?php echo $product['Product']['id']; ?>')" onmouseout="changeOldBg('<?php echo $product['Product']['id']; ?>')">
			<div  align="center" style="height:185px; width:155px; vertical-align:top;text-align:center;">
				<!-- Image code -->
				<div style="height:175px;" align="center">	
					<?php 
						$logo="no_logo";
						if ($product['Product']['display_image'] == 'Product Image') {
							$filepath = PRODUCT_IMAGE_PATH;
							$filename = $product['Product']['id']. '.'. $product['Product']['image_extension'];
						} else {
							$filepath = MERCHANT_LOGO_PATH;
							$filename = $product['Merchant']['id']. '.'. $product['Merchant']['logo_extension'];
						}
				
						if (!file_exists($filepath. $filename)) {
				
							//	try for a merchant image
							$filepath = MERCHANT_IMAGE_PATH;
							$filename = $product['Merchant']['id']. '.'. $product['Merchant']['image_extension'];
							if (!file_exists($filepath. $filename)) {
								//	try for a merchant logo
								$filepath = MERCHANT_LOGO_PATH;
								if ($logo != 'no_logo') {	//	if logo is not meant to appear, set a dummy name so it will not be found
									$filename = 'no_file';
								} else {
				
									$filename = $product['Merchant']['id']. '.'. $product['Merchant']['logo_extension'];
								}
								if (!file_exists($filepath. $filename)) {
									//	try for a client image
									$filepath = FILES_PATH. 'default_client_product_image/';
									$filename = $this->Session->read('client.Client.id'). '.'. $this->Session->read('client.Client.default_client_product_image_extension');
									if (!file_exists($filepath. $filename)) {
										//	try for a program image
										$filepath = FILES_PATH. 'default_product_image/';
										$filename = $this->Session->read('client.Program.id'). '.'. $this->Session->read('client.Program.default_product_image_extension');
										if (!file_exists($filepath. $filename)) {
											//	use the default no image image
											$filepath = WWW_ROOT. 'img/';
											$filename = 'no_image.gif';
										}
									}
								}
							}
						}
						$path = substr($filepath,strpos($filepath,'webroot')+8);
						$path_prod = $path.$filename;
					?>
					<?php
						$a=$this->ImageResize->getResizedDimensions($path_prod, 160, 120);
					?>
					<div style="height:145px">
                  	<a href="<?php echo $this->Html->url('/products/view/'. $product['Product']['id']); ?>">	
						<img align="center" style=" padding-top:36px; padding-right:7px;" width="<?php echo $a['width'];?>" height="<?php echo $a['height'];?>" src="<?php echo $this->Html->url('/'. $path_prod);?>" alt="" />
					</a></div>
					<!-- quick view -->
					<?php if($this->Session->read('client.Client.program_id')==21) : ?>
					<div id="<?php echo $product['Product']['id'].'quick'; ?>" align="center" style="margin-top:3px;display:none;">
						<?php echo $this->Html->image("/files/newimg/quick_view-1.png",array('escape' => false, 'onclick' => "productpopup(".$product['Product']['id'].")"));?>
					</div>	
					<?php else: ?>
					<div class='quckview' id="<?php echo $product['Product']['id'].'quick'; ?>" align="center" style="display:none;">
						<?php echo $this->Html->image("/files/newimg/quick_view-1.png",array('escape' => false, 'onclick' => "productpopup(".$product['Product']['id'].")"));?>
					</div>	
					<?php endif;?>
					<!-- quick view ends -->	
				</div>
				
				<!-- Ribbon code -->
				<?php if($this->Session->read('client.Client.program_id')!=21 && $this->Session->read('client.Client.program_id')!=36 && $this->Session->read('client.Client.program_id')!=37) : ?>
					<?php  if (isset($_SERVER['HTTP_USER_AGENT']) && (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 7.0') !== false)) : ?>
						<div class="offer_ribbon" style="margin-left:-150px !important"></div>
					<?php else : ?>
						<div class="offer_ribbon" style="position: absolute !important;"></div>	
					<?php endif; ?>	
				<?php endif; ?>	
				<!-- Ribbon code ends -->
				
				<?php 
				$offer = strip_tags($product['Product']['special_offer_headline']);
				$offer1 = strlen($offer);
				if($offer1>20):
					$offers  = substr($offer,0,20).'...';
				else:
					$offers  =$offer;
				endif;
				?>
				<?php 
				$pname = strip_tags($product['Product']['name']);
				if (strlen($pname)>'20') :
			  		$ptitle=mb_strcut($pname,0,20,'UTF-8').'...';
				else: 
			  		$ptitle=$pname;
				endif;
				?>									
				<?php 
				$highlight = strip_tags($product['Product']['highlight']);
				if (strlen($highlight)>'20') :
					$phighlight=mb_strcut($highlight,0,20,'UTF-8').'...';
				else:
					$phighlight=$highlight;
				endif;
				?>	
				
				<div style="font-size:13px; text-align:left; margin-left:7px;">						
					<a style="text-decoration:none" title="<?php echo $product['Product']['name'];?>" href="<?php echo $this->Html->url('/products/view/'. $product['Product']['id']. '/'.$product['MerchantAddress']['id']); ?>">
						<strong><?php echo $ptitle;?></strong>
					</a>
				</div>
				<?php if($product['Product']['special_offer_headline'] != ''):?>
				<div id="sp" title="<?php echo $product['Product']['special_offer_headline']; ?>" class="highlight" style="margin-top:5px; text-align:left; margin-left:7px;">
             		<?php echo $offers;?>
			   	 	<script type="text/javascript">document.getElementById('sp').title="<?php echo $product['Product']['special_offer_headline']; ?>"</script>
		 		</div>
				<?php else : ?>								
				<div id="sp1" title="<?php echo $product['Product']['highlight']; ?>" class="highlight" style="margin-top:5px; text-align:left; margin-left:7px;">
             		<?php echo $phighlight;?>
			   	 	<script type="text/javascript">document.getElementById('sp1').title="<?php echo $product['Product']['highlight']; ?>"</script>
		 		</div>
				<?php endif; ?>
			</div>
		</div>
		<?php endforeach; ?>
	</div>
</div>
<div style="float:left;margin-top:100px"><a href="#" class="cycle-next"><img src="/img/br_next.png" width="22"></a></div>
</div>
<?php endif;?>