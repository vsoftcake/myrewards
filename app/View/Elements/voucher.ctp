<?php echo $this->element('module2', array('module2' => 'Keyword_search', 'title' => 'Search'));?>
<script type="text/javascript">
function showWish(pid,uid)
{
	$.ajax({
		url: '/products/wishlist/'+pid+'/'+uid,
		type: "GET",
		success: function (data) {
			$('#divout').html(data);
		}
	});
}
function confirmPrint(id,pcount,ucount)
{	
	if(ucount==-1 && pcount == -1)
	{
		$.ajax({
			url: '/products/update_print_count/'+id,
			type: "GET",
			success: function (data) {}
		});
		window.print(); 
		return false;  
	}
	else if(ucount>0 && pcount>0)
	{	
		check=confirm('By clicking OK your Print Count will be decreased. Do you want to proceed?');
		if(check)
		{
			$.ajax({
				url: '/products/update_used_count/'+id,
				type: "GET",
				success: function (data) {}
			});
			window.print(); 
		}
	  	else
	  	{
	  		window.location = '/products/view/'+id;
	  	}
	}
	else if((ucount>0 && pcount==-1) || (ucount==-1 && pcount>0)) 
	{	
		check=confirm('By clicking OK your Print Count will be decreased. Do you want to proceed?');
		if(check)
		{
			$.ajax({
				url: '/products/update_used_count/'+id,
				type: "GET",
				success: function (data) {}
			});
			window.print(); 
	  	}
	  	else
	  	{
	  		window.location = '/products/view/'+id;
	  	}
	}
	return false;
}
function checkClick()
{
	var s = document.getElementById('check').value;
	if(s=="Quick Search")
	{
		document.getElementById('check').value='';
		document.myform.submit();
	}
	else
	{
		document.myform.submit();
	}
}
function checkFocus()
{
	var f = document.getElementById('check').value;
	if(f=='Quick Search')
		document.getElementById('check').value='';
}
function checkblur()
{
	var x=document.getElementById("check").value;
	if(x=='' || x==null)
		document.getElementById("check").value='Quick Search';
}
</script>
<style type="text/css">
.voucher_content {
	border-left: 2px solid #<?php echo $voucher['voucher_border_color']; ?>;
	border-right: 2px solid #<?php echo $voucher['voucher_border_color']; ?>;
	border-bottom: 0;
	border-top: 0;
	background: url('<?php echo  $this->Html->url(STYLE_WWW_PATH. 'voucher_background/'. $voucher['id']. '.'. $voucher['voucher_background_extension']); ?>') repeat-y top left;
}
</style>
<div class="search_content rounded-corners_bottom map_border" style="width:950px;padding-bottom:20px">
	<?php if (($product['Product']['used_count'] > 0 || $product['Product']['used_count'] == -1) && ($usedcount>0 || $usedcount== -1 )) : ?>
		<div style="padding-top:15px;padding-bottom:10px;float:left" class="no_print"><a style="text-decoration:none;margin-left:50px;"  href="javascript:window.history.back()"><img src="/files/newimg/back.png" alt="Back" title="Back"/></a></div>
		<div style="padding-right:10px;padding-bottom:20px; width:300px; margin-left:620px" class="no_print">
			<form action="<?php echo $this->Html->url('/products/search2'); ?>" method="get" name="myform">
				<div style="float:left;padding-top:10px;padding-right:2px;">
					<img width="25px" src="/files/newimg/magnifier.png"/>
				</div>
				<div align="center" style="padding-top: 3px; position: relative; width: 150px; height: 40px; top: 10px; float: left;z-index:1">
					<?php echo $this->Form->input('Product.keywords',array('label'=>false, 'div'=>false, 'class' => 'quick_search rounded_corners_button','id'=>'check','value'=>'Quick Search','onfocus'=>'checkFocus()','onblur'=>'checkblur()','style'=>'color:#C8D6D6;font-style:italic;')); ?>
				</div>
				<div style="top:13px;position:relative">
					<button type="submit" class="quick-search_button_style rounded_corners_button" onclick="checkClick()">Go !</button>
				</div>
			</form>
		</div>
		<div id="wishbackground" class="rounded_corners_map map_border" style="width: 830px;margin-left: 50px; padding: 10px;background-color:#ffffff;">
		<div>
			<span class="no_print">
				<div id="divout" style="font-weight: bold;float:left" class="wishlist"></div>
				<div align="right">
					<!--<button style="height: 30px; font-size: 15px; width: 150px;" class="rounded_corners_button" type="button" onclick="confirmPrint(<?php echo $usedcount;?>)">Print Coupons</button>-->
					<a href="javascript:void(0)" onclick="confirmPrint(<?php echo $product['Product']['id'].','.$product['Product']['used_count'].','.$usedcount;?>)">
						<img src="<?php echo $this->Html->url('/files/styles/icon_images/print.png'); ?>" alt="print" title="Print" />
					</a>
					<?php if  (($product['Product']['web_coupon'] == '1') && ($product['Product']['used_count'] > 0 || $product['Product']['used_count'] == -1) && ($usedcount>0 || $usedcount== -1 )) : ?>
					   <a href="javascript:void(0)" id="wlink" onclick="showWish(<?php echo $product['Product']['id'].','.$this->Session->read('user.User.id');?>)">
							<img style="padding:5px;" src="<?php echo $this->Html->url('/files/styles/icon_images/wishlist.png'); ?>" alt="Add to Favourites" title="Add to Favourites"/>
					   </a>                                                        
   					<?php endif;?>
				</div>
			</span>
		</div>	
		
    <div style="position: relative;  width: 800px;padding-bottom:30px;margin-left:5px;"> <img style="margin-top:20px" src="/files/newimg/Online_coupons4_1.jpg" height="267" width="800" alt="" border="0"> 
      <div style="position: absolute; height: 280px; left: 0; top: 0;"> 
        <table height="310">
	          <tr> 
	            <td valign="top" style="padding-top:65px;"> <div style=" width: 400px; padding-left: 30px; padding-right: 50px;"> 
	                <div style="float:left;width:170px;height:200px;"> 
				<div style="min-height:80px;overflow:hidden;">
					<?php $display_image = $product['Product']['display_image'];?>
			            	
					<?php 
					if($display_image == 'Merchant Logo') :
						$filepath = MERCHANT_LOGO_PATH;
						$filename = $product['Merchant']['id']. '.'. $product['Merchant']['logo_extension'];
						if (!file_exists($filepath. $filename)) 
						{
							$filepath = PRODUCT_IMAGE_PATH;
							$filename = $product['Product']['id']. '.'. $product['Product']['image_extension'];
							if (!file_exists($filepath. $filename)) 
							{
								$filename = 'transparent.gif';
								$filepath = '../img';
							}
						}
						$path = substr($filepath,strpos($filepath,'webroot')+8);
						$path_prod = $path.$filename;
					elseif($display_image == 'Product Image') : 
						$filepath = PRODUCT_IMAGE_PATH;
						$filename = $product['Product']['id']. '.'. $product['Product']['image_extension'];
						if (!file_exists($filepath. $filename)) 
						{
							$filepath = MERCHANT_LOGO_PATH;
							$filename = $product['Merchant']['id']. '.'. $product['Merchant']['logo_extension'];
							if (!file_exists($filepath. $filename)) 
							{
								$filename = 'transparent.gif';
								$filepath = '../img';
							}
						}
						$path = substr($filepath,strpos($filepath,'webroot')+8);
						$path_prod = $path.$filename;
					endif; 
					?>
					<?php $a=$this->ImageResize->getResizedDimensions($path_prod, 100, 100);?>
			                <img width="<?php echo $a['width'];?>" height="<?php echo $a['height'];?>" src="<?php echo $this->Html->url('/'. $path_prod); ?>" alt="" /><br/>
			                
					</div>                 
						
						<div style="padding-top:5px;">
		 					<strong><?php echo (isset($product['MerchantAddress']['name']) && $product['MerchantAddress']['name'] != ''? $product['MerchantAddress']['name']: $product['Product']['name']); ?></strong> 
			             	<br/>
			                <?php if($product[0]['count']>1):?>
			                <?php echo "Multiple Locations";?> 
			                <?php else :?>
			                <?php if(trim($product['MerchantAddress']['address1']) != ''):?>
			                <?php echo $product['MerchantAddress']['address1']; ?> 
			                <?php endif;?>
			                <?php if(trim($product['MerchantAddress']['address1']) != '' && trim($product['MerchantAddress']['address2']) != ''):?>
			                <?php echo ", ";?> 
			                <?php endif;?>
			                <?php if(trim($product['MerchantAddress']['address2']) != ''):?>
			                <?php echo $product['MerchantAddress']['address2']."<br/>"; ?> 
			                <?php endif;?>
			                <?php if(trim($product['MerchantAddress']['suburb']) != ''):?>
			                <?php echo $product['MerchantAddress']['suburb']; ?> 
			                <?php endif;?>
			                <?php if (trim($product['MerchantAddress']['state']) != '' || (trim($product['MerchantAddress']['postcode']) != '' && trim($product['MerchantAddress']['postcode']) != '0')) : ?>
			                <?php if(trim($product['MerchantAddress']['state']) != ''):?>
			                <?php echo $product['MerchantAddress']['state']; ?> 
			                <?php endif;?>
			                <?php if(trim($product['MerchantAddress']['state']) != '' && trim($product['MerchantAddress']['postcode']) != ''):?>
			                <?php echo ", ";?> 
			                <?php endif;?>
			                <?php if(trim($product['MerchantAddress']['postcode']) != ''):?>
			                <?php echo $product['MerchantAddress']['postcode']; ?> 
			                <?php endif;?>
			                <?php endif; ?>
			                <?php if(trim(strip_tags($product['MerchantAddress']['phone'])) != ''):?>
			                <br/>Tel: <?php echo $product['MerchantAddress']['phone']; ?> 
			                <?php endif;?>
			                <?php //echo (trim($product['MerchantAddress']['phone']) != ''? '<br/>Tel: '. $product['MerchantAddress']['phone']: ''); ?> 
			                <?php endif;?>
						</div>
	              </div>
	
	
	                <div style="margin-left:160px;padding-top:10px;"> 
                           
                         
                           <h3 style="margin:0px !important;text-align:center;"><?php echo strip_tags($product['Product']['highlight']);  ?></h3> 
                           
                         </div>
                         <div id="text2" title="<?php echo strip_tags($product['Product']['offer']); ?>" >   
                           <?php  if (strlen($product['Product']['offer']) > '180') { ?>
                           <?php $offer = strip_tags(substr($product['Product']['offer'],0,180)); ?>
	                  <p style="text-align:center;"><?php echo $offer.'...' ; ?></p>
                           <?php } else { ?>
                            <p style="text-align:center;"><?php echo strip_tags($product['Product']['offer']); } ?></p>
                           <script type="text/javascript">
                                  document.getElementById('text2').title="<?php echo strip_tags($product['Product']['offer']); ?>"
                            </script>
	                </div>
	                 
	              </div></td>
	            <td style="padding-top:65px;" valign="top"> <div > 
	                <strong> Member Name:</strong> <?php echo $this->Session->
	                read('user.User.first_name'). ' '. $this->Session->read('user.User.last_name'); 
	                ?><br/>
	                <strong>Membership Number:</strong> <?php echo $this->Session->read('user.User.username') ?><br/>
	                <strong>Offer ID:</strong> <?php echo $product['Product']['id']; ?><br/>
	                <strong>Client:</strong> <?php echo $this->Session->read('client.Client.name'); ?><br/>
	              </div>
	              
	              <div style="width: 285px; padding-top: 3px;"> 
	                <span style="font-size:10px"><?php echo $product['Product']['terms_and_conditions']; ?></span>
	                
	              </div></td>
	          </tr>
	          
        </table>
		<div style="position:relative;bottom:55px;float:right;margin-right:20px;#ffffff;">
		Expires:  <?php echo date("d-M-Y", strtotime('+ '.$product['Product']['coupon_expiry'].' days')); ?> </div>
      </div>
    </div>
		</div>
		
	<?php else :?>
<div>Voucher not available</div>
<?php endif;?>
</div>
