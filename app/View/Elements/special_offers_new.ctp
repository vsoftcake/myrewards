<script type="text/javascript">
function showWish(pid,uid)
{
	$.ajax({
		url: '/products/wishlist/'+pid+'/'+uid,
		type: "GET",
		success: function (data) {
			alert(data);
		}
	});
}
</script>  
<div style="float:left;width:770px;min-height: 100px; overflow: hidden;">
<?php if (isset($special_offers)) : ?>
	<?php $class_name = 'first_special_offer'; ?>
	<?php foreach($special_offers as $product) : ?>
		<div style="height:257px;width:242px;float:left;padding-top:12px;padding-left:8px;">
			<div align="center"  id="<?php echo $product['Product']['id']; ?>" class="module36" onmouseover="changeBgImg('<?php echo $product['Product']['id']; ?>')" onmouseout="changeOldBg('<?php echo $product['Product']['id']; ?>')">
				<div style=" height:185px; width:155px; vertical-align:top;text-align:center;">
					<div style="height:145px;">	
						<!--<a href="<?php echo $this->Html->url('/products/view/'. $product['Product']['id']); ?>">	
							<img align="center" style="padding-top:36px;padding-right:7px;"  src="<?php echo $this->Html->url('/images/product/'. $product['Product']['id']. '.'. $product['Product']['image_extension']. '/no_logo');?>" alt="" /> 
						</a>-->
						
						<?php $logo="no_logo";
											if ($product['Product']['display_image'] == 'Product Image') {
												$filepath = PRODUCT_IMAGE_PATH;
												$filename = $product['Product']['id']. '.'. $product['Product']['image_extension'];
											} else {
												$filepath = MERCHANT_LOGO_PATH;
												$filename = $product['Merchant']['id']. '.'. $product['Merchant']['logo_extension'];
											}
									
											if (!file_exists($filepath. $filename)) {
									
												//	try for a merchant image
												$filepath = MERCHANT_IMAGE_PATH;
												$filename = $product['Merchant']['id']. '.'. $product['Merchant']['image_extension'];
												if (!file_exists($filepath. $filename)) {
													//	try for a merchant logo
													$filepath = MERCHANT_LOGO_PATH;
													if ($logo != 'no_logo') {	//	if logo is not meant to appear, set a dummy name so it will not be found
														$filename = 'no_file';
													} else {
									
														$filename = $product['Merchant']['id']. '.'. $product['Merchant']['logo_extension'];
													}
													if (!file_exists($filepath. $filename)) {
														//	try for a client image
														$filepath = FILES_PATH. 'default_client_product_image/';
														$filename = $this->Session->read('client.Client.id'). '.'. $this->Session->read('client.Client.default_client_product_image_extension');
														if (!file_exists($filepath. $filename)) {
															//	try for a program image
															$filepath = FILES_PATH. 'default_product_image/';
															$filename = $this->Session->read('client.Program.id'). '.'. $this->Session->read('client.Program.default_product_image_extension');
															if (!file_exists($filepath. $filename)) {
																//	use the default no image image
																$filepath = WWW_ROOT. 'img/';
																$filename = 'no_image.gif';
															}
														}
													}
												}
											}
											$path = substr($filepath,strpos($filepath,'webroot')+8);
											$path_prod = $path.$filename;
										?>
									<?php
										$a=$this->ImageResize->getResizedDimensions($path_prod, 160, 120);
									?>
                  					<a href="<?php echo $this->Html->url('/products/view/'. $product['Product']['id']); ?>">	
										<!--<img align="center" style="padding-top:36px;padding-right:7px;"  src="<?php echo $this->Html->url('/images/product/'. $product['Product']['id']. '.'. $product['Product']['image_extension']. '/no_logo');?>" alt="" />-->
										<img align="center" style=" padding-top:36px; padding-right:7px;" width="<?php echo $a['width'];?>" height="<?php echo $a['height'];?>" src="<?php echo $this->Html->url('/'. $path_prod);?>" alt="" />
									</a>
					</div>
					<?php if ($page['ClientPage']['special_offers'] == '1') : ?>
						<div style="position:relative;"> 
							<img style="position:absolute;right:151px;bottom:97px;" src="<?php echo $this->Html->url('/files/newimg/hot_icon.png');?>" alt="" />
						</div>
						<?php endif; ?>
					<?php if (isset($new_products) && ($new_products!='')) : ?>
						<div style="position:relative;"> 
							<img style="position:absolute;right:151px;bottom:97px;" src="<?php echo $this->Html->url('/files/newimg/new.png');?>" alt="" />
						</div>
					<?php endif; ?>
										
					<div id="<?php echo $product['Product']['id'].'quick'; ?>" style="margin-top:6px; margin-left:8px;float:center; display:none;">
					<?php echo $this->Html->image("/files/newimg/quick_view-1.png",array('escape' => false, 'onclick' => "productpopup(".$product['Product']['id'].")"));?>
					</div>			                  
				</div>
		
				<?php 
					$offer = strip_tags($product['Product']['special_offer_headline']);
					$offer1 = strlen($offer);
					if($offer1>20):
						$offers  = substr($offer,0,20).'...';
					else:
						$offers  =$offer;
					endif;
				?>
				<?php $pname = strip_tags($product['Product']['name']);
					if (strlen($pname)>'20') :
				  		$ptitle=mb_strcut($pname,0,20,'UTF-8').'...';
					else: 
				  		$ptitle=$pname;
					 endif;
				?>	
				<?php 
					$highlight = strip_tags($product['Product']['highlight']);
					if (strlen($highlight)>'20') :
						$phighlight=mb_strcut($highlight,0,20,'UTF-8').'...';
					else:
						$phighlight=$highlight;
					endif;
				?>	
				<div style="font-size:13px; text-align:left; margin-left:7px;margin-top:5px;">						
					<a style="text-decoration:none" title="<?php echo $product['Product']['name'];?>" href="<?php echo $this->Html->url('/products/view/'. $product['Product']['id']. '/'.$product['MerchantAddress']['id']); ?>">
						<strong><?php echo $ptitle;?></strong>
					</a>
				</div>
				<?php if($product['Product']['special_offer_headline'] != ''):?>
					<div id="sp" title="<?php echo $product['Product']['special_offer_headline']; ?>" class="highlight" style="margin-top:5px; text-align:left; margin-left:7px;">
	             		<?php echo $offers;?>
				   	 	<script type="text/javascript">document.getElementById('sp').title="<?php echo $product['Product']['special_offer_headline']; ?>"</script>
			 		</div>
				<?php else : ?>								
					<div id="sp1" title="<?php echo $product['Product']['highlight']; ?>" class="highlight" style="margin-top:5px; text-align:left; margin-left:7px;">
	             		<?php echo $phighlight;?>
				   	 	<script type="text/javascript">document.getElementById('sp1').title="<?php echo $product['Product']['highlight']; ?>"</script>
			 		</div>
				<?php endif; ?>
			</div>
		</div>
	<?php endforeach; ?>
<?php endif; ?> 
</div>
<div style="float:left;padding-right:14px;text-align:center;width: 140px; padding-top: 15px;">
	<?php foreach($bannerlist as $list):?>
		<?php 
			$filepath = SKYSCRAPER_IMAGE_PATH;
			$filename = $list['Skyscraper']['id']. '.'. $list['Skyscraper']['scraper_extension'];

			$path = substr($filepath,strpos($filepath,'webroot')+8);
			$path_prod = $path.$filename;

			$a=$this->ImageResize->getResizedDimensions($path_prod, 120, 600);
		?>
		
		<a href="<?php echo $list['Skyscraper']['link_url'];?>">
			<img width="<?php echo $a['width'];?>" height="<?php echo $a['height'];?>" src="<?php echo $this->Html->url('/'. $path_prod); ?>" alt="" />
		</a>
		<!--<a href="<?php echo $list['Skyscraper']['link_url'];?>">
			<img align="center" style="vertical-align:top;" src="<?php echo $this->Html->url(SKYSCRAPER_IMAGE_WWW_PATH. $list['Skyscraper']['id']. '.'. $list['Skyscraper']['scraper_extension']); ?>" alt="" />
		</a>-->
	<?php endforeach;?>
</div>