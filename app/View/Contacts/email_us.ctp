<?php if($msg=='sent') : ?>
	<div style="padding-top:20px;padding-left:10px;width:940px;border:1px solid #DFDFDF;height:120px;">
		<h2>Thankyou!</h2>
		<br>
		<span style="font-size:14px;">Thanks for contacting us. Your email has been sent and a member of The Rewards Team will be in contact soon.</span>
	</div>
<?php elseif($msg == 'failed') : ?>
	<div style="padding-top:20px;padding-left:10px;width:940px;border:1px solid #DFDFDF;height:120px;">
		<h2>Email failed to send</h2>
	</div>
<?php endif; ?>