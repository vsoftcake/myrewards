<div style="border-bottom:0px !important">
	<?php echo $this->element('message'); ?>
	<h1 style="margin-left:10px;margin-top:10px">Feedback</h1><br/>

<form id="ContactFeedbackForm" method="post" action="/contacts/feedback">
<table border="0" cellpadding="0" cellspacing="0" class="content_table_2col" style="margin-left:10px;">
  <tr align="left" valign="middle" class="content_table_row">
    <td><p>First Name: </p></td>
    <td valign="middle"><p><?php echo $this->Form->input('Contact.firstname',array('class'=>'search_input', 'div'=>false, 'label'=>false)); ?></p></td>
	<td><?php echo $this->Form->error('Contact.firstname', array('required' => 'First Name required'), array('required' => 'First Name required')); ?></td>
  </tr>
  <tr align="left" valign="middle" class="content_table_row">
    <td><p>Last Name: </p></td>
    <td valign="middle"><p><?php echo $this->Form->input('Contact.lastname',array('class'=>'search_input', 'div'=>false, 'label'=>false)); ?></p></td>
	<td><?php echo $this->Form->error('Contact.lastname', array('required' => 'Last Name required'), array('required' => 'Last Name required')); ?></td>
  </tr>
  <tr align="left" valign="middle" class="content_table_row">
    <td><p>Email address: </p></td>
    <td valign="middle"><p><?php echo $this->Form->input('Contact.email',array('class'=>'search_input', 'div'=>false, 'label'=>false)); ?></p></td>
	<td><?php echo $this->Form->error('Contact.email', array('valid_email' => 'Invalid email address'), array('valid_email' => 'Invalid email address')); ?></td>
  </tr>
  <tr align="left" valign="middle" class="content_table_row">
    <td><p>Membership number: </p></td>
    <td valign="middle"><p><?php echo $this->Form->input('Contact.username',array('class'=>'search_input', 'div'=>false, 'label'=>false)); ?></p></td>
  </tr>
  <tr align="left" valign="middle" class="content_table_row">
    <td><p>Comments:</p></td>
    <td valign="middle"><p><textarea name="data[Contact][comments]" cols="24" id="Contactcomments"></textarea></p></td>
  </tr>
  <tr align="left" valign="middle" class="content_table_row">
    <td><p>&nbsp;</p></td>
    <td><?php echo $this->Form->submit('Submit', array('class'=>'button_search_module rounded_corners_button', 'div'=>false, 'type'=>'button', 'onclick'=>"submitFeedbackForm()"));?>
    </td>
  </tr>
</table>
</form>
</div>
<script>
function submitFeedbackForm() {
	$.ajax({
		url: '/contacts/feedback',
		data: $('#ContactFeedbackForm').serialize(),
		type: 'POST',
		success: function(data){
			$("#modalboxpopup").html(data).dialog({modal: true, "position": "top"});
		}
	});
	return false;
}
</script>