<style>
	#content
	{
		border-left:1px solid #cccccc;
		border-right:1px solid #cccccc;
		border-bottom:1px solid #cccccc;
		margin-left:20px !important;
		padding-left:18px !important;
		width:913px;
	}
	.admin_button
	{
		height: 26px;
		padding-top: 2px;
		padding-bottom: 2px;
		border-radius: 5px 5px 5px 5px;
		background-color: #0066cc;
		border: 1px solid #cccccc;
		color: #FFFFFF; 
		cursor: pointer;
		font-size:13px;
	}
	.current
	{
	    font-family: 'Droid Sans',sans-serif;
	    font-size: 12px;
	   	padding:5px 8px !important;   
	    background-color: #0066CC;
	    border: 1px solid #DFDFDF;
	    color:#ffffff;
	}
	.cpagination1
	{
		/*padding:1px 2px 1px 2px;*/
		font-family: 'Droid Sans',sans-serif;
	    font-size: 12px;
		text-decoration:none !important;
		background-color: #FFFFFF;
		padding:3px 8px;
		border: 1px solid #0066CC;
		color:#808080 !important;
	}
</style>
<script>
	function showComments()
	{

		pid = document.getElementById("CommentProgram").value;	
		id = document.getElementById('opt').value;
		country = document.getElementById("CommentCountry").value;
		if(pid!='')
		{
			$.ajax({
				url: '/admin/comments/update_comments/'+pid+'/s='+id+'/c='+country,
				type: "GET",
				success: function (data) {
					$('#comments').html(data);
				}
			});
		}
		else
		{
			alert("Please select a program");
		}
	}
	function viewComment(cid)
	{
		$.ajax({
			url: '/admin/comments/view/'+cid,
			type: "GET",
			success: function (data) {
				$('#v'+cid).html(data);
			}
		});
	}
</script>

<div class="list" id="comments">
	<table style="margin-left:-2px;border:none !important;">
		<tr>
			<td>
	            &nbsp;&nbsp;<img style="width:40px;height:40px;" src="/files/admin_home_icons/comments.png"  alt="Clients" title="Clients" />&nbsp;&nbsp;
	        </td>
	        <td> 
	            <h2><?php echo "Comments";?></h2>
	        </td>
	    </tr>        
	</table>
	
	<table style="width:918px;background-color:#cccccc;height:65px;border:1px solid #969090;">
		<tr>
			<td style="border:none !important;">
				<span style="color:red">* </span><strong>Programs List</strong> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<?php echo $this->Form->input('Comment.Program',array('type'=>'select','label'=>false,'options'=>$programs_list,'empty'=>'Select Program'));?>
			</td>
			<td style="border:none !important;">
				<strong>Sort By Status</strong>
				<select name="optname" id="opt">
					<option value=""></option>
					<option value="0">Approve</option>
					<option value="1">Approved</option>
				</select>
			</td>
			<td style="border:none !important;">
				<strong>Sort By Country</strong>
				<?php echo $this->Form->input('Comment.Country',array('type'=>'select','label'=>false,'options'=>$countries,'empty'=>' '));?>
			</td>
			<td style="border:none !important;">
				<button class="admin_button" type="submit" onclick="showComments()">Submit</button>
			</td>
		</tr>
	</table>
	
	<br>
		
	<table cellpadding="0" cellspacing="0" width="918px">
		<tr height="30">
			<th style="background-color:#cccccc;text-align:left;padding-left:5px;border-left:1px solid #969090;border-bottom:1px solid #969090;border-top:1px solid #969090;">Id</th>
			<th style="text-align:left;background-color:#cccccc;text-align:left;padding-left:5px;border-bottom:1px solid #969090;border-top:1px solid #969090;">product id</th>
			<th style="text-align:left;background-color:#cccccc;text-align:left;padding-left:5px;border-bottom:1px solid #969090;border-top:1px solid #969090;">product Name</th>
			<th style="text-align:left;background-color:#cccccc;text-align:left;padding-left:5px;border-bottom:1px solid #969090;border-top:1px solid #969090;">Merchant Name</th>
			<th style="text-align:left;background-color:#cccccc;text-align:left;border-bottom:1px solid #969090;border-top:1px solid #969090;">Posted By</th>
			<th style="text-align:left;padding-left:20px;background-color:#cccccc;text-align:left;padding-left:17px;border-bottom:1px solid #969090;border-top:1px solid #969090;">Title</th>
			<th class="actions" style="text-align:left;padding-left:50px;background-color:#cccccc;text-align:left;padding-left:4px;border-right:1px solid #969090;border-bottom:1px solid #969090;border-top:1px solid #969090;"><?php echo "Actions";?></th>
		</tr>
		<?php
		$i = 0;
		foreach ($comments as $comment): ?>
		<?php
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td style="width:20px">
				<?php echo $comment['Comment']['id'] ?>
			</td>
			<td style="width:80px">
				<?php echo $comment['Comment']['product_id'] ?>
			</td>
			<td style="width:200px">
				<?php echo $comment['Product']['name'] ?>
			</td>
			<td style="width:280px">
				<?php echo $comment['Merchant']['name'] ?>
			</td>
			<td style="width:120px">
				<?php echo $comment['User']['username'] ?>
			</td>
			<td style="width:160px;padding-left:20px">
				<?php echo $comment['Comment']['name'] ;?>
			</td>
			
			<td class="actions" style="width:300px">
			<?php if($comment['Comment']['status']==1) : echo "<span style='color:green'>Approved</span>";?>
			<?php else:?>
			<?php echo $this->Html->link(__('Approve', true), array('action'=>'approve', $comment['Comment']['id']),array('style'=>'padding-right:7px')); ?>
			<?php endif;?>
			<a href="#view" onclick="viewComment(<?php echo $comment['Comment']['id'];?>)" id="view<?php echo $comment['Comment']['id'];?>">View</a>&nbsp;&nbsp;
			<?php echo $this->Html->link(__('Delete', true), array('action'=>'delete', $comment['Comment']['id']), null, sprintf(__('Are you sure you want to delete comment \'%s\'?', true), $comment['Comment']['name'])); ?>
			<div style="padding-top:10px;" id="v<?php echo $comment['Comment']['id'] ;?>"></div>
			</td>
		</tr>
		<?php endforeach; ?>
	</table>
	
	<div class="paging" style="width:920px">
		<table style="border:none !important;padding-top:15px;float:right;">
			<tr>
				<td style="border:none !important;" valign="top">
					<?php echo $this->Paginator->first('<<', array('title'=>'first','class'=>'disabled cpagination1'));?>
					<?php echo $this->Paginator->prev('<', array('title'=>'prev','class'=>'disabled cpagination1'));?>
					<?php echo $this->Paginator->numbers(array('separator' => ' ','class'=>'cpagination1'));?>
					<?php echo $this->Paginator->next('>', array('title'=>'next','class'=>'disabled cpagination1'));?>
					<?php echo $this->Paginator->last('>>', array('title'=>'last','class'=>'disabled cpagination1'));?>
				</td>
				<td style="border:none !important;padding-left:5px;padding-top:4px;">
				<select name="page" id="selectpage" style="margin-top:-5px;">
					<?php for ($i = 1; $i <= $this->Paginator->counter(array('format' => '%pages%')); $i++):?>
					<option value="<?php echo $i;?>"<?php echo ($this->Paginator->current($params['page']) == $i) ? ' selected="selected"' : ''?>><?php echo $i ?></option>
					<?php endfor ?>
				</select>
				<script>
				$("#selectpage").change(function() {
					var url = window.location.href;
					var n = url.indexOf("/?");
					var extra = "";
					if(n>0){extra = url.substring(n,url.length);}
					window.location.href = '/admin/comments/index/page:'+$(this).val()+extra;
				});
				</script>
				</td>
			</tr>
		</table>
	</div>
</div>

