<style>
	.current
	{
	    font-family: 'Droid Sans',sans-serif;
	    font-size: 12px;
	   	padding:5px 8px !important;   
	    background-color: #0066CC;
	    border: 1px solid #DFDFDF;
	    color:#ffffff;
	}
	.cpagination1
	{
		/*padding:1px 2px 1px 2px;*/
		font-family: 'Droid Sans',sans-serif;
	    font-size: 12px;
		text-decoration:none !important;
		background-color: #FFFFFF;
		padding:3px 8px;
		border: 1px solid #0066CC;
		color:#808080 !important;
	}
</style>
<?php $this->Paginator->options(
		array(
			'update' => '#Clients',
			'url' => $this->params['pass'],
			'model'=>'Client',
			'evalScripts' => true));
	?>
	<div style="float:left;padding-right: 40px;">
		<table cellpadding="0" cellspacing="0" style="width:390px">
		<tr>
			<th style="text-align:left"><?php echo $this->Paginator->sort('id', 'Client.id');?></th>
			<th style="text-align:left"><?php echo $this->Paginator->sort('Name', 'Client.name');?></th>
			<th style="text-align:left" class="actions"><?php echo "Actions";?></th>
		</tr>
		<?php
		$i = 0;$count = 0;
		foreach ($clients as $client):
			$class = null;
			if ($i++ % 2 == 0) 
			{
				$class = ' class="altrow"';
			}
			$count++;
			if($count==11){
		?>
		</table>
	</div>
	<div>
		<table cellpadding="0" cellspacing="0" style="width:390px">
		<tr>
			<th style="text-align:left"><?php echo $this->Paginator->sort('id', 'Client.id');?></th>
			<th style="text-align:left"><?php echo $this->Paginator->sort('Name', 'Client.name');?></th>
			<th style="text-align:left" class="actions"><?php echo "Actions";?></th>
		</tr>
		   <?php } ?>  
			<tr<?php echo $class;?>>
				<td>
					<?php echo $client['Client']['id'] ?>
				</td>
				<td>
					<?php echo $client['Client']['name'] ?>
				</td>
				<td class="actions">
					<?php echo $this->Html->link(__('Edit', true), '/admin/clients/edit/'. $client['Client']['id']); ?>
				</td>
			</tr>
		<?php endforeach; ?>
		</table>
	</div>
	
	<div class="paging" style="width:822px">
		<table style="border:none !important;padding-top:25px;float:right;">
			<tr>
				<td style="border:none !important;" valign="top">
					<?php echo $this->Paginator->first('<<', array('title'=>'first','class'=>'disabled cpagination1'));?>
					<?php echo $this->Paginator->prev('<', array('title'=>'prev','class'=>'disabled cpagination1'));?>
					<?php echo $this->Paginator->numbers(array('separator' => ' ','class'=>'cpagination1'));?>
					<?php echo $this->Paginator->next('>', array('title'=>'next','class'=>'disabled cpagination1'));?>
					<?php echo $this->Paginator->last('>>', array('title'=>'last','class'=>'disabled cpagination1'));?>
				</td>
			</tr>
			<tr>
				<td style="border:none !important;padding-left:5px;padding-top:1px;">
					<p><?php echo $this->Paginator->counter(array('format' => __('Page %page% of %pages%', true)));	echo $this->Js->writeBuffer();?></p>
				</td>
			</tr>
		</table>
	</div>