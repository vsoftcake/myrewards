<style type="text/css">
	#content
	{
		border-left:1px solid #cccccc;
		border-right:1px solid #cccccc;
		border-bottom:1px solid #cccccc;
		margin-left:20px !important;
		padding-left:18px !important;
		width:913px;
	}
	.rounded-corners 
	{
		-moz-border-radius: 10px 10px 0px 0px; /* Firefox */
		-webkit-border-radius: 10px 10px 0px 0px; /* Safari, Chrome */
		border-radius: 10px 10px 0px 0px; /* CSS3 */
	}
	.tr 
	{
		height:45px;
	}
	.tr1 
	{
		height:35px;
	}
	.admin_button
	{
		height: 26px;
		padding-bottom: 2px;
		border-radius: 5px 5px 5px 5px;
		background-color: #0066cc;
		border: 1px solid #cccccc;
		color: #FFFFFF; 
		cursor: pointer;
		font-size:13px;
	}
</style>
<?php echo $this->element('tinymce');?>
<?php echo $this->Html->script('jscolor'); ?>
<script type="text/javascript">
	document.write('<style type="text/css">.tabber{display:none;}<\/style>');
	function confirmation(name,id)
    {
		var answer = confirm("Are you sure you want to delete program '<?php echo $this->Form->value('Program.name') ?>'")
		if (answer)
		{
			location.href="/admin/programs/delete/<?php echo $this->Form->value('Program.id') ?>";
		}
    }
	function dispFBShare (it, box) {
		var vis = (box.checked) ? "block" : "none";
		document.getElementById(it).style.display = vis;
	}
</script>

<div style="width:953px;margin-left:-14px;">
	<form action="<?php echo $this->Html->url('/admin/programs/edit/'. $this->Html->value('Program.id')); ?>" method="post" enctype="multipart/form-data">
		<div style="width:949px;height:50px;">
			<div style=" height: 40px; float:left; width: 608px;padding-left:37px;">
		       	<div style=" height: 40px; float:left;padding-top:5px;">
		        	<img title="Programs" alt="Programs" src="/files/admin_home_icons/programs.png" style="width:40px;height:40px;">
		        </div>
		        <div style=" height: 40px;float:left;padding-left:10px;">
		        	<h2>Edit Program <?php if($this->Form->value('Program.name')!='') : ?>- <?php echo $this->Form->value('Program.name') ?><?php endif; ?></h2>
		        </div> 
			</div>
	        <div class="actions" style="height:40px;float:left;width: 300px;">
				<ul style="padding:20px;margin-left:-28px;">
					<li style="display:inline;"><?php echo $this->Form->button('Cancel', array('onclick' => "location.href='/admin/programs/'",
					'class'=>'admin_button','style'=>'width:60px;', 'type'=>'button'));  ?></li>
					<?php if($this->Form->value('Program.id')!='') :
						$name=$this->Form->value('Program.name');$id=$this->Form->value('Program.id'); ?>
						<li style="display:inline;"><?php echo $this->Form->button('Delete Program', array('onclick' => "confirmation('$name','$id')",'style'=>'width:110px;','class'=>'admin_button','type'=>'button'));  ?></li>
		        	<?php endif;?>
			        <li style="display:inline;">
			        	<?php echo $this->Form->button('Submit', array('class'=>'admin_button','style'=>'width:60px;','type'=>'submit'));  ?>
			        </li>
		        </ul>
			</div>
		</div>

		<div class="tabber" id="mytab1">
			<div class="tabbertab"  style="min-height:220px;overflow:hidden">
	  			<h2>General Information</h2>
	    		<h4>Program General Information</h4>
	    		<?php if($this->Html->value('Program.id')==null && !empty($program_options)): ?>
	    		<div style="background-color:#EFEFEF;border:1px solid #cccccc;width:768px;min-height:40px;overflow:hidden;padding:20px">
	    	    	<table width="450">
						<tr>
							<td><strong>Copy from existing programs</strong></td>
							<td>
								<?php echo $this->Form->input('Program.copy', array('type'=>'select', 'options'=>$program_options, 'div'=>false, 'label'=>false));?>
							</td>
							<td><button type="submit" name="copy" class="admin_button">Select</button></td>
						</tr>
				  	</table>
				</div>  
				<br />
				<?php endif ; ?>
				<div style="background-color:#EFEFEF;border:1px solid #cccccc;width:775px;min-height:120px;overflow:hidden;padding:20px">
					<div style="overflow: hidden; float: left; width: 380px; min-height: 205px;">
						<table>
							<tr class="tr">
								<td style="width:100px"><strong>Name</strong></td>
								<td><?php echo $this->Form->input('Program.name', array('div'=>false,'label'=>false)); ?></td>
								<td><?php echo $this->Form->error('Program.name', array('required' => 'Name required', 'unique' => 'Name already in use'), array('required' => 'Name required')); ?></td>
							</tr>
							<tr class="tr">
								<td><strong>My Rewards Style</strong></td>
								<td>
									<?php echo $this->Form->input('Program.style_id', array('type'=>'select','options'=>$styles, 'empty'=>'','style'=>'width:230px', 'div'=>false, 'label'=>false)); ?>
								</td>
							</tr>
							<tr class="tr">
								<td><strong>Send A Friend Style</strong></td>
								<td>
									<?php echo $this->Form->input('Program.style_id_saf', array('type'=>'select','options'=>$styles, 'empty'=>'','style'=>'width:230px', 'div'=>false, 'label'=>false)); ?>
								</td>
							</tr>
							<tr class="tr">
								<td><strong>Points Style</strong></td>
								<td>
									<?php echo $this->Form->input('Program.style_id_points', array('type'=>'select','options'=>$styles, 'empty'=>'','style'=>'width:230px', 'div'=>false, 'label'=>false)); ?>
								</td>
							</tr>
							<tr class="tr">
								<td><strong>Domain</strong></td>
								<td>
									<?php echo $this->Form->input('Program.domain_id', array('type'=>'select','options'=>$domains, 'empty'=>'','style'=>'width:230px', 'div'=>false, 'label'=>false)); ?>
								</td>
							</tr>
							<tr class="tr" style="padding-top:15px;">
								<td><strong>Default offer image</strong></td>
								<td>
									<img src="<?php echo $this->Html->url(FILES_WWW_PATH. 'default_product_image/'. $this->Form->value('Program.id'). '.'. $this->Form->value('Program.default_product_image_extension')); ?>" alt="" /><br/>
									<?php echo $this->Form->file('Image.default_product_image'); ?>
								</td>
							</tr>
						</table>
					</div>
					<div style="float: left; width: 380px; min-height: 180px;overflow:hidden;">
						<table>	
							<tr class="tr">
								<td><strong>Default Country</strong></td>
								<td class="br"><?php echo $this->Form->input('Program.country', array('type'=>'select', 'options'=>$defaultcountry, 'div'=>false, 'label'=>false)); ?></td>
							</tr>
							<tr class="tr">
								<td><strong>Program Country</strong></td>
								<td class="br">
									<?php echo $this->Form->input('Country.Country', array('multiple' => 'multiple',  'empty' => ' ', 'div'=>false, 'label'=>false)); ?>
									<input type="hidden" name="data[Country][Country][]" value="" />
								</td>
							</tr>
							<tr class="tr">
								<td valign="top"><strong>Categories</strong></td>
								<td>
									<?php echo $this->Form->input('Category.Category', array('multiple' => 'multiple','style'=>'width:230px;', 'div'=>false, 'label'=>false)); ?>
									<input type="hidden" name="data[Category][Category][]" value="" />
								</td>
							</tr>
						</table>
					</div> 
				</div>
				<div align="center" style="padding-top:10px;">
					<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(1)">Next</button>
		       	</div>
			</div>
			
			<div class="tabbertab" style="min-height:158px;overflow:hidden">
		    	<h2>Links And Copyright</h2>
		     	<h4>Links And Copyright</h4>
		     	<div style="background-color:#EFEFEF;border:1px solid #cccccc;width:768px;min-height:40px;overflow:hidden;padding:20px"> 
		     		<div style="height:250px;float:left;width:300px;padding-right:25px;">
						<table>		
							<tr class="tr1">
								<td style="width:200px"><strong>Header Links Disable</strong></td>
								<td class="br"><?php echo $this->Form->input('Program.header_links_disabled', array('div'=>false,'label'=>false)); ?></td>
							</tr>
							<tr class="tr1">
								<td><strong>Footer Links Disable</strong></td>
								<td class="br"><?php echo $this->Form->input('Program.footer_links_disabled', array('div'=>false,'label'=>false)); ?></td>
							</tr>
							<tr class="tr1">
								<td><strong>Refine Search Disable</strong></td>
								<td class="br"><?php echo $this->Form->input('Program.refine_search_disabled', array('div'=>false,'label'=>false)); ?></td>
							</tr>
							<tr class="tr1">
								<td><strong>Email Alerts Disable</strong></td>
								<td class="br"><?php echo $this->Form->input('Program.email_alerts_disabled', array('div'=>false,'label'=>false)); ?></td>
							</tr>
							<tr class="tr1">
								<td><strong>Live Chat Enable</strong></td>
								<td class="br"><?php echo $this->Form->input('Program.live_chat_enabled', array('div'=>false,'label'=>false)); ?></td>
						    	</tr>
						    	<tr class="tr1">
								<td><strong>Redirect logout page</strong></td>
								<td class="br"><?php echo $this->Form->input('Program.logout_redirect', array('div'=>false,'label'=>false)); ?></td>
							</tr>
							<tr>
								<td colspan="2">
									<sub>If enabled, the user will be sent back to the external page they<br/>logged in from</sub>
								</td>
							</tr>
					 	</table>
					</div>
		     			<div>
						<table>
							
							<tr class="tr">
								<td><strong>Copyright Text</strong></td>
								<td>
									<?php echo $this->Form->input('Program.copyright_text', array('div'=>false,'label'=>false)); ?>
									&nbsp;&nbsp;<?php echo $this->Form->checkbox('Program.copyright_disabled'); ?>&nbsp;Remove
								</td>
							</tr>
							<tr class="tr">
								<td><strong>Facebook Link</strong></td>
								<td class="br">
									<?php echo $this->Form->input('Program.facebook_link', array('div'=>false,'label'=>false)); ?> 
		    						&nbsp;&nbsp;<?php echo $this->Form->checkbox('Program.facebook_disabled'); ?>&nbsp;Remove
		    					</td>
							</tr>
							<tr class="tr">
								<td><strong>Twitter Link</strong></td>
								<td class="br">
									<?php echo $this->Form->input('Program.twitter_link', array('div'=>false,'label'=>false)); ?> 
		    						&nbsp;&nbsp;<?php echo $this->Form->checkbox('Program.twitter_disabled'); ?>&nbsp;Remove
		    					</td>
							</tr>	
							<tr class="tr">
								<td><strong>Home Page</strong></td>
								<td class="br"><?php echo $this->Form->input('Program.home_page', array('div'=>false,'label'=>false)); ?></td>
							</tr>
							<tr class="tr1">
								<td style="width:150px;"><strong>Cart Enable</strong></td>
								<td class="br">
									<?php echo $this->Form->input('Program.cart_enabled',array('onclick'=>'showPaypalName()')); ?>
									<br/>
									<script>
										function showPaypalName()
										{
											if (document.getElementById('ProgramCartEnabled').checked==true)
											{
												document.getElementById('paypal').style.display='block';
												document.getElementById('currency').style.display='block';
											}
											else
											{
												document.getElementById('paypal').style.display='none';
												document.getElementById('currency').style.display='none';
											}
										}
									</script>
								</td>
							    </tr>
							    <?php 
									$mdStyle="none"; 
									if($this->data['Program']['cart_enabled'] == '0'):
										$mdStyle="display:none";
									elseif($this->data['Program']['cart_enabled'] == 1):
										$mdStyle="display:block";
									endif;
								?>	
							<tr class="tr">
								<td colspan="2">
									<div id="paypal" style=<?php echo $mdStyle;?>>
										<strong>Paypal Business Name</strong>
										<?php echo $this->Form->input('Program.paypal_business_name',array('value'=>$business_name));?>
										<br/>
										<span style="color:red">Please Provide Paypal Business Name if cart is enabled</span>
									</div>
								</td>
							</tr>
							<tr class="tr">
								<td colspan="2">
									<div id="currency" style=<?php echo $mdStyle;?>>
										<strong>Paypal Currency Code</strong>&nbsp;&nbsp;&nbsp;
										<?php 
											echo $this->Form->input('Program.paypal_currency_code', array('type' => 'select', 'options' => $codes, 'default'=>$currency_code,'div'=>false,'label'=>false)); 
										?>
										<br/>
										<span style="color:red">Please Select Currency Type if cart is enabled</span>
									</div>
								</td>
							</tr>
						</table>
					</div>
			   	</div>
				<div align="center" style="padding-top:10px;">
                	<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(0)">Previous</button>
					<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(2)">Next</button>
				</div>                      
			</div>
						
		
		<div class="tabbertab" style="min-height:100px;overflow:hidden;">
			<h4>ErrorLogin/FirstTimeLogin Text</h4>
			<div style="background-color:#EFEFEF;border:1px solid #cccccc;padding:10px;width:790px;min-height:100px;overflow:hidden">
				<table>
					<tr>
						<td><strong>Login Error Text</strong></td>

					</tr>
					<tr>
						<td>
							<?php echo $this->Form->input('Program.login_error_text', array('div'=>false,'label'=>false)); ?>
						</td>

					</tr>
				</table>
			</div>
			<br/>
			<div style="background-color:#EFEFEF;border:1px solid #cccccc;padding:10px;width:790px;min-height:100px;overflow:hidden">
				<table>
					<tr>
						<td><strong>First Time Login Text</strong></td>

					</tr>
					<tr>
						<td>
							<?php echo $this->Form->input('Program.first_time_login_txt', array('div'=>false,'label'=>false)); ?>
						</td>

					</tr>
				</table>
			</div>
			<br/>
			<?php echo $this->Form->input('Program.fb_share_disabled', array('onclick'=>"dispFBShare('fbShareText', this)")); ?> Share on Facebook
			<?php $fb_text_disp = ($this->data['Program']['fb_share_disabled']==1)?"":"none";?>
			<div id="fbShareText" style="background-color:#EFEFEF;border:1px solid #cccccc;padding:10px;width:790px;min-height:100px;overflow:hidden;display:<?php echo $fb_text_disp?>">
				<table>
					<tr>
						<td><strong>Facebook Share Description Text</strong></td>
					</tr>
					<tr>
						<td><?php echo $this->Form->input('Program.fb_description_txt', array('div'=>false,'label'=>false)); ?></td>
					</tr>
				</table>
			</div>
			<br/><?php echo $this->Form->input('Program.payroll_deduct_disabled', array('div'=>false,'label'=>false)); ?> Payroll Deduct
			<br/><?php echo $this->Form->input('Program.referral_no_disabled', array('div'=>false,'label'=>false)); ?> Referral Number
			<br/>
			<div align="center" style="padding-top:10px">
				<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(1)">Previous</button>
				<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(3)">Next</button>
			</div>
		</div>
		
		<!-- Background Images Start-->
		
		<div class="tabbertab">
			<div class="tabber" id="bimages" style="margin-left: -59px; width: 847px;">
				<h2>Background Images</h2>
				<div class="tabbertab">
					<h2>My Rewards</h2>
					<div style="background-color:#EFEFEF;border:1px solid #cccccc;width: 820px; overflow: hidden; min-height: 200px; margin-left: -19px; padding: 2px;">	 
						<table>
							<tr>
								<td class="bt br"></td>
								<td class="bt br" style="padding-top:20px;padding-left:52px;padding-right:50px;"><strong>Left</strong></td>
								<td class="bt br" style="padding-top:20px;padding-left:52px;padding-right:50px;"><strong>Right</strong></td>
							</tr>
							<tr>
								<td class="bt br" valign="top" style="padding-left:20px;"><strong>Upload Images</strong></td>
								<td class="bt br" style="padding-left:50px;padding-right:50px;">
									<?php echo $this->Form->file('Image.program_background_images_left_1'); ?><br/>
									Remove <?php echo $this->Form->checkbox('ImageDeleteLeft.program_background_images_left_1'); ?>
								</td>
								<td class="bt br" style="padding-left:50px;padding-right:50px;">
									<?php echo $this->Form->file('Image.program_background_images_right_1'); ?><br/>
									Remove <?php echo $this->Form->checkbox('ImageDeleteRight.program_background_images_right_1'); ?>
								</td>
							</tr>
							<tr>
								<td style="height:5px">&nbsp;</td>
							</tr>
							<tr> 
								<td colspan="3" style="padding-left:20px;padding-bottom:20px"><b><span style="color:red">* Left/Right Backgrount Image : Upload image only with 170px width and 700px height</span></b></td> 
							</tr>
							<tr>
								<td style="padding-left:20px;"><strong>Client background left image link</strong></td>
								<td style="padding-left:50px;">
									<?php echo $this->Form->input('Program.left_link_1', array('div'=>false,'label'=>false));?>
								</td>
							</tr>
							<tr>
								<td style="padding-left:20px;"><strong>Client background right image link</strong></td>
								<td style="padding-left:50px;">
									<?php echo $this->Form->input('Program.right_link_1', array('div'=>false,'label'=>false));?>
								</td>
							</tr>
							<tr>
								<td style="height:5px">&nbsp;</td>
							</tr>
						</table>
						<br />
						<table>
							<tr>
								<td class="bb br" colspan="2">
									<div  id="submitmrbi" style="text-align:right">
										<button class="admin_button" onclick="return displayOverrideMrBi();">Submit &amp; apply to clients...</button>
									</div>
									<div id="overridemrbi" style="display:none;border:1px solid #dfdfdf;padding:10px;">
										<table style="width:600px">
											<tr>
												<th style="font-size:13px">Items</th><th style="font-size:13px">Clients</th>
											</tr>
											<tr>
												<td style="vertical-align:top">
													<input type="hidden" name="data[ClientOverride][override_mr_bi]" value="0" id="override_enabled_mr_bi" />
													<label for="ClientOverrideLeftImage_mr"><input type="checkbox" name="data[ClientOverride][left_mr_1]" value="1" class="checkbox" id="ClientOverrideLeftMr1" /> Myrewards Left Image </label><br/>
													<label for="ClientOverrideRightImage_mr"><input type="checkbox" name="data[ClientOverride][right_mr_1]" value="1" class="checkbox" id="ClientOverrideRightMr1" /> Myrewards Right Image </label><br/>
													<label for="ClientOverrideLeftImageLink_mr"><input type="checkbox" name="data[ClientOverride][left_link_mr_1]" value="1" class="checkbox" id="ClientOverrideLeftLinkMr1" /> Myrewards Left Image Link</label><br/>
													<label for="ClientOverrideRightImageLink_mr"><input type="checkbox" name="data[ClientOverride][right_link_mr_1]" value="1" class="checkbox" id="ClientOverrideRightLinkMr1" /> Myrewards Right Image Link</label><br/>
												</td>
												<td>
													<select name="data[ClientOverideClients_mr_bi][]" multiple="multiple" id="ClientOverideClients_mr_biSelect">
														<option value="all" onclick="selectAllOverrideMRBI();">All Clients</option>
														<?php foreach ($all_clients as $client_id => $client_name) : ?>
															<option value="<?php echo $client_id; ?>" ><?php echo $client_name; ?></option>
														<?php endforeach; ?>
													</select>
												</td>
											</tr>
										</table>
										<div style="text-align:right">
											<button class="admin_button" type="submit" onclick="return confirm('Apply changes to all selected clients?');">Submit &amp; apply to clients</button>
											<button class="admin_button" onclick="return cancelOverrideMRBI();">Cancel</button>
										</div>
									</div>
									<script type="text/javascript">
										function displayOverrideMrBi() {
											$('#submitmrbi').hide();
											$('#overridemrbi').show();
											$('#override_enabled_mr_bi').val('1');
											return false;
										}

										function cancelOverrideMRBI() {
											$('#submitmrbi').show();
											$('#overridemrbi').hide();
											$('#override_enabled_mr_bi').val('0');

											return false;
										}
										var all_override_selected_mr_bi = false;
										function selectAllOverrideMRBI()
										{
											if (all_override_selected_mr_bi == false) {
												all_override_selected_mr_bi = true;
											} else {
												all_override_selected_mr_bi = false;
											}
	
											$("#ClientOverideClients_mr_biSelect option").prop('selected', all_override_selected_mr_bi);
											$("#ClientOverideClients_mr_biSelect option[value='all']").prop("selected", false);
										}
									</script>
								</td>
							</tr>
						</table>
					</div>
					<br/>
					<div align="center">
						<button type="button" class="admin_button" onclick="document.getElementById('bimages').tabber.tabShow(1)">Next</button>
					</div>
				</div>

				<div class="tabbertab">
					<h2>Send A Friend</h2>
					<div style="background-color:#EFEFEF;border:1px solid #cccccc;width: 820px; overflow: hidden; min-height: 200px; margin-left: -19px; padding: 2px;">	 
						<table>
							<tr>
								<td class="bt br"></td>
								<td class="bt br" style="padding-top:20px;padding-left:52px;padding-right:50px;"><strong>Left</strong></td>
								<td class="bt br" style="padding-top:20px;padding-left:52px;padding-right:50px;"><strong>Right</strong></td>
							</tr>
							<tr>
								<td class="bt br" valign="top" style="padding-left:20px;"><strong>Upload Images</strong></td>
								<td class="bt br" style="padding-left:50px;padding-right:50px;">
									<?php echo $this->Form->file('Image.program_background_images_left_2'); ?><br/>
									Remove <?php echo $this->Form->checkbox('ImageDeleteLeft2.program_background_images_left_2'); ?>
								</td>
								<td class="bt br" style="padding-left:50px;padding-right:50px;">
									<?php echo $this->Form->file('Image.program_background_images_right_2'); ?><br/>
									Remove <?php echo $this->Form->checkbox('ImageDeleteRight2.program_background_images_right_2'); ?>
								</td>
							</tr>
							<tr>
							    <td style="height:5px">&nbsp;</td>
							</tr>
							<tr> 
								<td colspan="3" style="padding-left:20px;padding-bottom:20px"><b><span style="color:red">* Left/Right Backgrount Image : Upload image only with 170px width and 700px height</span></b></td> 
							</tr>
							<tr>
								<td style="padding-left:20px;"><strong>Client background left image link</strong></td>
								<td style="padding-left:50px;">
									<?php echo $this->Form->input('Program.left_link_2', array('div'=>false,'label'=>false));?>
								</td>
							</tr>
							<tr>
								<td style="padding-left:20px;"><strong>Client background right image link</strong></td>
								<td style="padding-left:50px;">
									<?php echo $this->Form->input('Program.right_link_2', array('div'=>false,'label'=>false));?>
								</td>
							</tr>
							<tr>
								<td style="height:5px">&nbsp;</td>
							</tr>
						</table>
						<br/>
						<table>
							<tr>
								<td class="bb br" colspan="2">
									<div  id="submitsafbi" style="text-align:right">
										<button class="admin_button" type="button" onclick="return displayOverrideSafBi();">Submit &amp; apply to clients...</button>
									</div>
									<div id="overridesafbi" style="display:none;border:1px solid #dfdfdf;padding:10px;">
										<table style="width:600px">
											<tr>
												<th style="font-size:13px">Items</th><th style="font-size:13px">Clients</th>
											</tr>
											<tr>
												<td style="vertical-align:top">
													<input type="hidden" name="data[ClientOverride][override_saf_bi]" value="0" id="override_enabled_saf_bi" />
													<label for="ClientOverrideLeftImage_saf"><input type="checkbox" name="data[ClientOverride][left_saf_2]" value="1" class="checkbox" id="ClientOverrideLeftSaf2" /> Send A Friend Left Image </label><br/>
													<label for="ClientOverrideRightImage_saf"><input type="checkbox" name="data[ClientOverride][right_saf_2]" value="1" class="checkbox" id="ClientOverrideRightSaf2" /> Send A Friend Right Image </label><br/>
													<label for="ClientOverrideLeftImageLink_saf"><input type="checkbox" name="data[ClientOverride][left_link_saf_2]" value="1" class="checkbox" id="ClientOverrideLeftLinkSaf2" /> Send A Friend Left Image Link</label><br/>
													<label for="ClientOverrideRightImageLink_saf"><input type="checkbox" name="data[ClientOverride][right_link_saf_2]" value="1" class="checkbox" id="ClientOverrideRightLinkSaf2" /> Send A Friend Right Image Link</label><br/>
												</td>
												<td>
													<select name="data[ClientOverideClients_saf_bi][]" multiple="multiple" id="ClientOverideClients_saf_biSelect">
														<option value="all" onclick="selectAllOverrideSAFBI();">All Clients</option>
														<?php foreach ($all_clients as $client_id => $client_name) : ?>
															<option value="<?php echo $client_id; ?>" ><?php echo $client_name; ?></option>
														<?php endforeach; ?>
													</select>
												</td>
											</tr>
										</table>
										<div style="text-align:right">
											<button class="admin_button" type="submit" onclick="return confirm('Apply changes to all selected clients?');">Submit &amp; apply to clients</button>
											<button class="admin_button" onclick="return cancelOverrideSAFBI();">Cancel</button>
										</div>
									</div>
									<script type="text/javascript">
										function displayOverrideSafBi() {
											$('#submitsafbi').hide();
											$('#overridesafbi').show();
											$('#override_enabled_saf_bi').val('1');
											return false;
										}
										function cancelOverrideSAFBI() {
											$('#submitsafbi').show();
											$('#overridesafbi').hide();
											$('#override_enabled_saf_bi').val('0');
											return false;
										}
										var all_override_selected_saf_bi = false;
										function selectAllOverrideSAFBI()
										{
											if (all_override_selected_saf_bi == false) {
												all_override_selected_saf_bi = true;
											} else {
												all_override_selected_saf_bi = false;
											}
	
											$("#ClientOverideClients_saf_biSelect option").prop('selected', all_override_selected_saf_bi);
											$("#ClientOverideClients_saf_biSelect option[value='all']").prop("selected", false);
										}
									</script>
								</td>
							</tr>
						</table>
					</div>
					<br />
					<div align="center">
						<button type="button" class="admin_button" onclick="document.getElementById('bimages').tabber.tabShow(0)">Previous</button>
						<button type="button" class="admin_button" onclick="document.getElementById('bimages').tabber.tabShow(2)">Next</button>
					</div>
				</div>

				<div class="tabbertab">
					<h2>Points</h2>
						<div style="background-color:#EFEFEF;border:1px solid #cccccc;width: 820px; overflow: hidden; min-height: 200px; margin-left: -19px; padding: 2px;">
							<table>
								<tr>
									<td class="bt br"></td>
									<td class="bt br" style="padding-top:20px;padding-left:52px;padding-right:50px;"><strong>Left</strong></td>
									<td class="bt br" style="padding-top:20px;padding-left:52px;padding-right:50px;"><strong>Right</strong></td>
								</tr>
								<tr>
									<td class="bt br" valign="top" style="padding-left:20px;"><strong>Upload Images</strong></td>
									<td class="bt br" style="padding-left:50px;padding-right:50px;">
										<?php echo $this->Form->file('Image.program_background_images_left_3'); ?><br/>
										Remove <?php echo $this->Form->checkbox('ImageDeleteLeft3.program_background_images_left_3'); ?>
									</td>
									<td class="bt br" style="padding-left:50px;padding-right:50px;">
										<?php echo $this->Form->file('Image.program_background_images_right_3'); ?><br/>
										Remove <?php echo $this->Form->checkbox('ImageDeleteRight3.program_background_images_right_3'); ?>
									</td>
								</tr>
								<tr>
								     <td style="height:5px">&nbsp;</td>
								</tr>
								<tr> 
									  <td colspan="3" style="padding-left:20px;padding-bottom:20px"><b><span style="color:red">* Left/Right Backgrount Image : Upload image only with 170px width and 700px height</span></b></td> 
								</tr>
								<tr>
									<td style="padding-left:20px;"><strong>Client background left image link</strong></td>
									<td style="padding-left:50px;">
										<?php echo $this->Form->input('Program.left_link_3', array('div'=>false,'label'=>false));?>
									</td>
								</tr>
								<tr>
									<td style="padding-left:20px;"><strong>Client background right image link</strong></td>
									<td style="padding-left:50px;">
										<?php echo $this->Form->input('Program.right_link_3', array('div'=>false,'label'=>false));?>
									</td>
								</tr>
								<tr>
								     <td style="height:5px">&nbsp;</td>
								</tr>
							</table>
							<br/>

							<br/>
						<table>
							<tr>
								<td class="bb br" colspan="2">
									<div  id="submitpointsbi" style="text-align:right">
										<button class="admin_button" onclick="return displayOverridePointsBi();">Submit &amp; apply to clients...</button>
									</div>
									<div id="overridepointsbi" style="display:none;border:1px solid #dfdfdf;padding:10px;">
										<table style="width:600px">
											<tr>
												<th style="font-size:13px">Items</th><th style="font-size:13px">Clients</th>
											</tr>
											<tr>
												<td style="vertical-align:top">
													<input type="hidden" name="data[ClientOverride][override_points_bi]" value="0" id="override_enabled_points_bi" />
													<label for="ClientOverrideLeftImage_points"><input type="checkbox" name="data[ClientOverride][left_points_3]" value="1" class="checkbox" id="ClientOverrideLeftPoints3" /> Points Left Image </label><br/>
													<label for="ClientOverrideRightImage_points"><input type="checkbox" name="data[ClientOverride][right_points_3]" value="1" class="checkbox" id="ClientOverrideRightPoints3" /> Points Right Image </label><br/>
													<label for="ClientOverrideLeftImageLink_points"><input type="checkbox" name="data[ClientOverride][left_link_points_3]" value="1" class="checkbox" id="ClientOverrideLeftLinkPoints3" /> Points Left Image Link</label><br/>
													<label for="ClientOverrideRightImageLink_points"><input type="checkbox" name="data[ClientOverride][right_link_points_3]" value="1" class="checkbox" id="ClientOverrideRightLinkPoints3" /> Points Right Image Link</label><br/>
												</td>
												<td>
													<select name="data[ClientOverideClients_points_bi][]" multiple="multiple" id="ClientOverideClients_points_biSelect">
														<option value="all" onclick="selectAllOverridePOINTSBI();">All Clients</option>
														<?php foreach ($all_clients as $client_id => $client_name) : ?>
															<option value="<?php echo $client_id; ?>" ><?php echo $client_name; ?></option>
														<?php endforeach; ?>
													</select>
												</td>
											</tr>
										</table>
										<div style="text-align:right">
											<button class="admin_button" type="submit" onclick="return confirm('Apply changes to all selected clients?');">Submit &amp; apply to clients</button>
											<button class="admin_button" onclick="return cancelOverridePOINTSBI();">Cancel</button>
										</div>
									</div>
									<script type="text/javascript">
										function displayOverridePointsBi() {
											$('#submitpointsbi').hide();
											$('#overridepointsbi').show();
											$('#override_enabled_points_bi').val('1');
											return false;
										}
										function cancelOverridePOINTSBI() {
											$('#submitpointsbi').show();
											$('#overridepointsbi').hide();
											$('#override_enabled_points_bi').val('0');

											return false;
										}
										var all_override_selected_points_bi = false;
										function selectAllOverridePOINTSBI()
										{
											if (all_override_selected_points_bi == false) {
												all_override_selected_points_bi = true;
											} else {
												all_override_selected_points_bi = false;
											}
	
											$("#ClientOverideClients_points_biSelect option").prop('selected', all_override_selected_points_bi);
											$("#ClientOverideClients_points_biSelect option[value='all']").prop("selected", false);
										}
									</script>
								</td>
							</tr>
						</table>

						</div>
					<div align="center">
						<button type="button" class="admin_button" onclick="document.getElementById('bimages').tabber.tabShow(1)">Previous</button>
					</div>
				</div>
				<div align="center">
					<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(2)">Previous</button>
					<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(4)">Next</button>
				</div>
			</div>
		</div>
						
		<!-- Background Images ends -->	
		<div class="tabbertab" style="min-height:210px;overflow:hidden">
			<h2>Products</h2>
				<div class="tabber" id="mytab3" style="margin-left: -59px; width: 809px;">
				<div class="tabbertab"  style="min-height:210px;overflow:hidden" title="">
				<h2>My Rewards</h2>
				<h4>My Rewards</h4>
					<div style="background-color:#EFEFEF;border:1px solid #cccccc;width:768px;min-height:40px;overflow:hidden;padding:20px">	 
						<table style="width:750px">		
							<tr>
								<td style="width:400px"><strong>Special Offer Module - Limit</strong></td>
								<td style="width:300px"><?php echo $this->Form->input('Program.special_offer_module_limit', array('div'=>false, 'label'=>false, 'type'=>'text')); ?></td>
							</tr>
							<tr>
								<td valign="top"><strong>Special Offer Module - Products</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
								<td>
									<table width="600">
										<tr>
											<td>
												<input type="hidden" name="data[SpecialProduct][SpecialProduct][]" value="" />
												<?php foreach ($this->data['SpecialProduct'] as $product) : ?>
												<?php if($product['ProgramsSpecialProduct']['dashboard_id']==1){ ?>
													<div id="SpecialProduct<?php echo $product['id'].$product['ProgramsSpecialProduct']['dashboard_id']; ?>">
														<input type="hidden" name="data[SpecialProduct][SpecialProduct][]" value="<?php echo $product['id']; ?>" />
														<?php echo $product['id']; ?> - <?php echo $product['name']; ?> <a href="javascript:void(0)" onclick="$('#SpecialProduct<?php echo $product['id'].$product['ProgramsSpecialProduct']['dashboard_id']; ?>').remove(); return false;">Remove</a>
													</div>
												<?php } ?>
												<?php endforeach; ?>
											</td>
										</tr>
										<tr>
										<td valign="top">
										<p id="addSpecialProduct"></p>
										<a href="javascript:void(0)" onclick="return addSpecialProduct();"><strong>Add Special Offer Product</strong></a>
										</td>
										</tr>
									</table>
								<script type="text/javascript">
									function addSpecialProduct() {
										var rand = Math.round(10000000000*Math.random());
										$('<input id="sp' + rand + '" type="text" name="data[SpecialProduct][SpecialProduct][]" autocomplete="off" /><br/>').insertBefore('p#addSpecialProduct');
										$(function() {
											$( "#sp"+ rand ).autocomplete({
												source: "/admin/clients/xml_products/<?php echo $this->Html->value('Client.id')?>", delay: .25
											});
										});
										return false;
									}
								</script>
								</td>
							  </tr>
						</table>
						<br/>
						<table>
							<tr>
								<td class="bb br" colspan="2">
									<div  id="submit" style="text-align:right">
										<button class="admin_button" onclick="return displayOverride();">Submit &amp; apply to clients...</button>
									</div>
									<div id="override" style="display:none;border:1px solid #dfdfdf;padding:10px;">
										<table style="width:600px">
											<tr>
												<th style="font-size:13px">Items</th><th style="font-size:13px">Clients</th>
											</tr>
											<tr>
												<td style="vertical-align:top">
													<input type="hidden" name="data[ClientOverride][override]" value="0" id="override_enabled" />
													<label for="ClientOverrideStyle_id"><input type="checkbox" name="data[ClientOverride][style_id]" value="1"class="checkbox" id="ClientOverrideStyle_id" /> Style</label><br/>
													<label for="ClientOverrideDomain_id"><input type="checkbox" name="data[ClientOverride][domain_id]" value="1"class="checkbox" id="ClientOverrideDomain_id" /> Domain</label><br/>
													<label for="ClientOverrideSpecial_offer_module_limit"><input type="checkbox" name="data[ClientOverride][special_offer_module_limit]" value="1"class="checkbox" id="ClientOverrideSpecial_offer_module_limit" /> Special Offer Module - Limit</label><br/>
													<label for="ClientOverrideSpecialProduct"><input type="checkbox" name="data[ClientOverride][SpecialProduct]" value="1"class="checkbox" id="ClientOverrideSpecialProduct" /> Special Offer Module - Products</label><br/>
													<label for="ClientOverrideCategory"><input type="checkbox" name="data[ClientOverride][Category]" value="1"class="checkbox" id="ClientOverrideCategory" /> Categories</label><br/>
													<script>
														function showProducts()
														{
															if (document.getElementById('ClientOverrideProduct').checked==true)
																document.getElementById('filter').style.display="inline";
															else
																document.getElementById('filter').style.display='none';
														}
													</script>
													<label for="ClientOverrideHomePage"><input type="checkbox" name="data[ClientOverride][home_page]" value="1"class="checkbox" id="ClientOverrideHomePage" /> Home Page</label><br/>
													<label for="ClientOverrideLogoutRedirect"><input type="checkbox" name="data[ClientOverride][logout_redirect]" value="1"class="checkbox" id="ClientOverrideLogoutRedirect" /> Logout Redirect</label><br/>
													<label for="ClientOverrideHeaderDisable"><input type="checkbox" name="data[ClientOverride][header_links_disabled]" value="1"class="checkbox" id="ClientOverrideHeaderDisable" /> Header Links Disable</label><br/>
													<label for="ClientOverrideFooterDisable"><input type="checkbox" name="data[ClientOverride][footer_links_disabled]" value="1"class="checkbox" id="ClientOverrideFooterDisable" /> Footer Links Disable</label><br/>
													<label for="ClientOverrideRefineSearchDisable"><input type="checkbox" name="data[ClientOverride][refine_search_disabled]" value="1"class="checkbox" id="ClientOverrideRefineSearchDisable" /> Refine Search Disable</label><br/>
													<label for="ClientOverrideEmailAlertsDisable"><input type="checkbox" name="data[ClientOverride][email_alerts_disabled]" value="1"class="checkbox" id="ClientOverrideEmailAlertsDisable" /> Email Alerts Disable</label><br/>
													<label for="ClientOverrideLocation"><input type="checkbox" name="data[ClientOverride][Location]" value="1"class="checkbox" id="ClientOverrideLocation" /> Location</label><br/>
													<label for="ClientOverrideCopyrightText"><input type="checkbox" name="data[ClientOverride][copyright_text]" value="1"class="checkbox" id="ClientOverrideCopyrightText" /> Copyright Text</label><br/>
													<label for="ClientOverrideFacebookLink"><input type="checkbox" name="data[ClientOverride][facebook_link]" value="1"class="checkbox" id="ClientOverrideFacebookLink" /> Facebook Link</label><br/>
													<label for="ClientOverrideTwitterLink"><input type="checkbox" name="data[ClientOverride][twitter_link]" value="1"class="checkbox" id="ClientOverrideTwitterLink" /> Twitter Link</label><br/>
													<label for="ClientOverrideLiveChatEnable"><input type="checkbox" name="data[ClientOverride][live_chat_enabled]" value="1"class="checkbox" id="ClientOverrideLiveChatEnable" /> Live Chat</label><br/>
													<label for="ClientOverrideCartEnable"><input type="checkbox" name="data[ClientOverride][cart_enabled]" value="1"class="checkbox" id="ClientOverrideCartEnable" /> Cart</label><br/>
													<label for="ClientOverrideLoginError"><input type="checkbox" name="data[ClientOverride][login_error]" value="1"class="checkbox" id="ClientOverrideLoginError" />Login Error text</label><br/>
													<label for="ClientOverrideFirstTimeLogin"><input type="checkbox" name="data[ClientOverride][first_time_login]" value="1"class="checkbox" id="ClientOverrideFirstTimeLogin" />First Time Login Text</label><br/>
													<label for="ClientOverrideShareFacebook"><input type="checkbox" name="data[ClientOverride][share_facbook]" value="1"class="checkbox" id="ClientOverrideShareFacebook" />Share on Facebook</label><br/>
													<label for="ClientOverridePayrollDeduct"><input type="checkbox" name="data[ClientOverride][payroll_deduct]" value="1"class="checkbox" id="ClientOverridePayrollDeduct" />Payroll Deduct</label><br/>
													<label for="ClientOverrideReferralNumber"><input type="checkbox" name="data[ClientOverride][referral_number]" value="1"class="checkbox" id="ClientOverrideReferralNumber" />Referral Number</label><br/>
												</td>
												<td>
													<select name="data[ClientOverideClients][]" multiple="multiple" id="ClientOverideClientsSelect">
														<option value="all" onclick="selectAllOverride();">All Clients</option>
														<?php foreach ($all_clients as $client_id => $client_name) : ?>
															<option value="<?php echo $client_id; ?>" ><?php echo $client_name; ?></option>
														<?php endforeach; ?>
													</select>
												</td>
											</tr>
										</table>
										<div style="text-align:right">
											<button class="admin_button" type="submit" onclick="return confirm('Apply changes to all selected clients?');">Submit &amp; apply to clients</button>
											<button class="admin_button" onclick="return cancelOverride();">Cancel</button>
										</div>
									</div>
									<script type="text/javascript">
										function displayOverride() {
											$('#submit').hide();
											$('#override').show();
											$('#override_enabled').val('1');
											return false;
										}
										function cancelOverride() {
											$('#submit').show();
											$('#override').hide();
											$('#override_enabled').val('0');
											return false;
										}
										var all_override_selected = false;
										function selectAllOverride()
										{
											if (all_override_selected == false) {
												all_override_selected = true;
											} else {
												all_override_selected = false;
											}
	
											$("#ClientOverideClientsSelect option").prop('selected', all_override_selected);
											$("#ClientOverideClientsSelect option[value='all']").prop("selected", false);
										}
									</script>
								</td>
							</tr>
						</table>
					</div>
					<div align="center" style="padding-top:10px">
						<button type="button" class="admin_button" onclick="document.getElementById('mytab3').tabber.tabShow(1)">Next</button>
					</div>
				</div>
				<div class="tabbertab"  style="min-height:210px;overflow:hidden" title="">
					<h2>Send A Friend</h2>
					<h4>Send A Friend</h4>
					<div style="background-color:#EFEFEF;border:1px solid #cccccc;width:768px;min-height:40px;overflow:hidden;padding:20px">	 
						<table style="width:750px">		
							<tr>
								<td style="width:400px"><strong>Special Offer Module - Limit</strong></td>
								<td style="width:300px"><?php echo $this->Form->input('Program.special_offer_module_limit_saf', array('div'=>false,'label'=>false, 'type'=>'text')); ?></td>
							</tr>
							<tr>
								<td valign="top"><strong>Special Offer Module - Products</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
								<td>
									<table width="600">
										<tr>
											<td>
												<input type="hidden" name="data[SpecialProduct][SpecialProduct_2][]" value="" />
												<?php foreach ($this->data['SpecialProduct'] as $product) : ?>
												<?php if($product['ProgramsSpecialProduct']['dashboard_id']==2){ ?>
													<div id="SpecialProduct<?php echo $product['id'].$product['ProgramsSpecialProduct']['dashboard_id']; ?>">
														<input type="hidden" name="data[SpecialProduct][SpecialProduct_2][]" value="<?php echo $product['id']; ?>" />
														<?php echo $product['id']; ?> - <?php echo $product['name']; ?> <a href="javascript:void(0)" onclick="$('#SpecialProduct<?php echo $product['id'].$product['ProgramsSpecialProduct']['dashboard_id']; ?>').remove(); return false;">Remove</a>
													</div>
												<?php } ?>
												<?php endforeach; ?>
											</td>
										</tr>
										<tr>
										<td valign="top"><p id="addSpecialProductSAF"></p>
										<a href="javascript:void(0)" onclick="return addSpecialProductSAF();"><strong>Add Special Offer Product</strong></a>
										</td>
										</tr>
									</table>
								<script type="text/javascript">
									function addSpecialProductSAF() {
										var rand = Math.round(10000000000*Math.random());
										$('<input id="sp' + rand + '" type="text" name="data[SpecialProduct][SpecialProduct_2][]" autocomplete="off" /><br/>').insertBefore('p#addSpecialProductSAF');
										$(function() {
											$( "#sp"+ rand ).autocomplete({
												source: "/admin/clients/xml_products/<?php echo $this->Html->value('Client.id')?>", delay: .25
											});
										});
										return false;
									}
								</script>
								</td>
							</tr>
						</table>
						<br/>
						<table>
							<tr>
								<td class="bb br" colspan="2">
									<div  id="submitsaf" style="text-align:right">
										<button class="admin_button" type="button" onclick="return displayOverrideSAF();">Submit &amp; apply to clients...</button>
									</div>
									<div id="overridesaf" style="display:none;border:1px solid #dfdfdf;padding:10px;">
										<table style="width:600px">
											<tr>
												<th style="font-size:13px">Items</th><th style="font-size:13px">Clients</th>
											</tr>
											<tr>
												<td style="vertical-align:top">
													<input type="hidden" name="data[ClientOverride][override_saf]" value="0" id="override_enabled_saf" />
													<label for="ClientOverrideStyle_id_saf"><input type="checkbox" name="data[ClientOverride][style_id_saf]" value="1"class="checkbox" id="ClientOverrideStyle_id_saf" /> Send A Friend Style</label><br/>
													<label for="ClientOverrideSpecial_offer_module_limit_saf"><input type="checkbox" name="data[ClientOverride][special_offer_module_limit_saf]" value="1"class="checkbox" id="ClientOverrideSpecial_offer_module_limit_saf" /> Special Offer Module - Limit</label><br/>
													<label for="ClientOverrideSpecialProduct_saf"><input type="checkbox" name="data[ClientOverride][SpecialProduct_saf]" value="1"class="checkbox" id="ClientOverrideSpecialProduct_saf" /> Special Offer Module - Products</label><br/>
												</td>
												<td>
													<select name="data[ClientOverideClients_saf][]" multiple="multiple" id="ClientOverideClientsSafSelect">
														<option value="all" onclick="selectAllOverrideSAF();">All Clients</option>
														<?php foreach ($all_clients as $client_id => $client_name) : ?>
															<option value="<?php echo $client_id; ?>" ><?php echo $client_name; ?></option>
														<?php endforeach; ?>
													</select>
												</td>
											</tr>
										</table>
										<div style="text-align:right">
											<button class="admin_button" type="submit" onclick="return confirm('Apply changes to all selected clients?');">Submit &amp; apply to clients</button>
											<button class="admin_button" onclick="return cancelOverrideSAF();">Cancel</button>
										</div>
									</div>
									<script type="text/javascript">
										function displayOverrideSAF() {
											$('#submitsaf').hide();
											$('#overridesaf').show();
											$('#override_enabled_saf').val('1');
											return false;
										}
										function cancelOverrideSAF() {
											$('#submitsaf').show();
											$('#overridesaf').hide();
											$('#override_enabled_saf').val('0');
											return false;
										}
										var all_override_saf_selected = false;
										function selectAllOverrideSAF()
										{
											if (all_override_saf_selected == false) {
												all_override_saf_selected = true;
											} else {
												all_override_saf_selected = false;
											}
	
											$("#ClientOverideClientsSafSelect option").prop('selected', all_override_saf_selected);
											$("#ClientOverideClientsSafSelect option[value='all']").prop("selected", false);
										}
									</script>
								</td>
							</tr>
						</table>
					</div>
					<div align="center" style="padding-top:10px">
						<button type="button" class="admin_button" onclick="document.getElementById('mytab3').tabber.tabShow(0)">Previous</button>
						<button type="button" class="admin_button" onclick="document.getElementById('mytab3').tabber.tabShow(2)">Next</button>
					</div>
				</div>
				<div class="tabbertab"  style="min-height:210px;overflow:hidden" title="">
					<h2>Points</h2>
					<h4>Points</h4>
						<div style="background-color:#EFEFEF;border:1px solid #cccccc;width:768px;min-height:40px;overflow:hidden;padding:20px">	 
							<table style="width:750px">		
								<tr>
									<td style="width:400px"><strong>Special Offer Module - Limit</strong></td>
									<td style="width:300px"><?php echo $this->Form->input('Program.special_offer_module_limit_points', array('div'=>false,'label'=>false, 'type'=>'text')); ?></td>
								</tr>
								<tr>
									<td valign="top"><strong>Special Offer Module - Products</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
									<td>
										<table width="600">
											<tr>
												<td>
													<input type="hidden" name="data[SpecialProduct][SpecialProduct_3][]" value="" />
													<?php foreach ($this->data['SpecialProduct'] as $product) : ?>
													<?php if($product['ProgramsSpecialProduct']['dashboard_id']==3){ ?>
														<div id="SpecialProduct<?php echo $product['id'].$product['ProgramsSpecialProduct']['dashboard_id']; ?>">
															<input type="hidden" name="data[SpecialProduct][SpecialProduct_3][]" value="<?php echo $product['id']; ?>" />
															<?php echo $product['id']; ?> - <?php echo $product['name']; ?> <a href="javascript:void(0)" onclick="$('#SpecialProduct<?php echo $product['id'].$product['ProgramsSpecialProduct']['dashboard_id']; ?>').remove(); return false;">Remove</a>
														</div>
													<?php } ?>
													<?php endforeach; ?>
												</td>
											</tr>
											<tr>
											<td valign="top"><p id="addSpecialProductPoints"></p>
											<a href="javascript:void(0)" onclick="return addSpecialProductPoints();"><strong>Add Special Offer Product</strong></a>
											</td>
											</tr>
										</table>
									<script type="text/javascript">
										function addSpecialProductPoints() {
											var rand = Math.round(10000000000*Math.random());
											$('<input id="sp' + rand + '" type="text" name="data[SpecialProduct][SpecialProduct_3][]" autocomplete="off" /><br/>').insertBefore('p#addSpecialProductPoints');
											$(function() {
												$( "#sp"+ rand ).autocomplete({
													source: "/admin/clients/xml_products/<?php echo $this->Html->value('Client.id')?>", delay: .25
												});
											});
											return false;
										}
									</script>
									</td>
								</tr>
							</table>
							<br/>
							<table>
								<tr>
									<td class="bb br" colspan="2">
										<div  id="submitpoints" style="text-align:right">
											<button class="admin_button" type="button" onclick="return displayOverridePoints();">Submit &amp; apply to clients...</button>
										</div>
										<div id="overridepoints" style="display:none;border:1px solid #dfdfdf;padding:10px;">
											<table style="width:600px">
												<tr>
													<th style="font-size:13px">Items</th><th style="font-size:13px">Clients</th>
												</tr>
												<tr>
													<td style="vertical-align:top">
														<input type="hidden" name="data[ClientOverride][override_points]" value="0" id="override_enabled_points" />
														<label for="ClientOverrideStyle_id_points"><input type="checkbox" name="data[ClientOverride][style_id_points]" value="1"class="checkbox" id="ClientOverrideStyle_id_points" /> Send A Friend Style</label><br/>
														<label for="ClientOverrideSpecial_offer_module_limit_points"><input type="checkbox" name="data[ClientOverride][special_offer_module_limit_points]" value="1"class="checkbox" id="ClientOverrideSpecial_offer_module_limit_points" /> Special Offer Module - Limit</label><br/>
														<label for="ClientOverrideSpecialProduct_points"><input type="checkbox" name="data[ClientOverride][SpecialProduct_points]" value="1"class="checkbox" id="ClientOverrideSpecialProduct_points" /> Special Offer Module - Products</label><br/>
													</td>
													<td>
														<select name="data[ClientOverideClients_points][]" multiple="multiple" id="ClientOverideClientsPointSelect">
															<option value="all" onclick="selectAllOverridePoints();">All Clients</option>
															<?php foreach ($all_clients as $client_id => $client_name) : ?>
																<option value="<?php echo $client_id; ?>" ><?php echo $client_name; ?></option>
															<?php endforeach; ?>
														</select>
													</td>
												</tr>
											</table>
											<div style="text-align:right">
												<button class="admin_button" type="submit" onclick="return confirm('Apply changes to all selected clients?');">Submit &amp; apply to clients</button>
												<button class="admin_button" onclick="return cancelOverridePoints();">Cancel</button>
											</div>
										</div>
										<script type="text/javascript">
											function displayOverridePoints() {
												$('#submitpoints').hide();
												$('#overridepoints').show();
												$('#override_enabled_points').val('1');
												return false;
											}
											function cancelOverridePoints() {
												$('#submitpoints').show();
												$('#overridepoints').hide();
												$('#override_enabled_points').val('0');
												return false;
											}
											var all_override_points_selected = false;
											function selectAllOverridePoints()
											{
												if (all_override_points_selected == false) {
													all_override_points_selected = true;
												} else {
													all_override_points_selected = false;
												}
		
												$("#ClientOverideClientsPointSelect option").prop('selected', all_override_points_selected);
												$("#ClientOverideClientsPointSelect option[value='all']").prop("selected", false);
											}
										</script>
									</td>
								</tr>
							</table>
						</div>
						<div align="center" style="padding-top:10px">
							<button type="button" class="admin_button" onclick="document.getElementById('mytab3').tabber.tabShow(1)">Previous</button>
						</div>
					</div>
					<div align="center">
						<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(3)">Previous</button>
						<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(5)">Next</button>
					</div>
			</div>
		</div>
		<?php echo $this->Form->input('Program.id', array('type' => 'hidden')); ?>
			
			<div class="tabbertab" style="min-height:615px;overflow:hidden">
	  			<h2>Log</h2>
	    		<h4>Log Changes</h4>
	    		<div style="background-color:#EFEFEF;border:1px solid #cccccc;padding:10px;width:790px;min-height:110px;overflow:hidden">
					<table>
	    		    	<tr>
							<td class="bt bb"><strong>Notes</strong><br/><sub>for change log</sub></td>
							<td class="bt br bb">
								<?php echo $this->Form->input('Log.notes',array('rows' => '4', 'cols' => '40', 'div'=>false, 'label'=>false)); ?>
							</td>
						</tr>
					</table>
					<div align="center" style="padding-top:10px">
						<button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(4)">Previous</button>
					</div>
				</div>
				<br/>
				<div style="border:1px solid #cccccc;width:770px;min-height:105px;overflow:hidden;padding:20px;">
					<div class="list" id="log_list">
						<h3>Change Log</h3>
						<div id="Log" style="margin:0;">Retrieving changes...</div>
						<script type="text/javascript">
						$(function() {
							$.ajax({
								url: "/admin/logs/ajax_list_new/<?php echo Inflector::singularize($this->name);?>/<?php echo $this->Form->value(Inflector::singularize($this->name). '.id');?>",
								type: "GET",
								success: function (data) {
									$('#Log').html(data);
								}
							});
						});</script>
					</div>
				</div>
      		</div>
		</div>
	</form>
	<div class="tabber" id="mytab2">
			<div class="tabbertab" style="min-height:350px;overflow:hidden;padding-left:0px !important;">
			<h2>Program Pages</h2>
				<div class="tabber" style="padding-left: 5px !important; width: 860px;" id="mytab3" >
					<div class="tabbertab" style="min-height:350px;overflow:hidden;padding-left:20px !important;">
						<h2>My Rewards</h2>
						<h4>My Rewards</h4>
							<div class="categories" style="margin-right: 5px; padding: 10px; width: 540px;height:358px;">
								<?php echo $this->Tree->showPages('ProgramPage.title', $program_pages, null, '/admin/program_pages/edit/'. $this->Form->value('Program.id'). '/', false, 'Page.id', 1, null, 'enabled', array(0, null)); ?>
							</div>
							<div class="categories" style="padding: 10px; width: 250px;height:358px;margin-left: -13px;">
								<h4>Public Program Pages</h4>
								<?php echo $this->Tree->showPages('ProgramPage.title', $program_public_pages, null, '/admin/program_pages/edit/'. $this->Form->value('Program.id'). '/', false, 'Page.id', 1, null, 'enabled', array(0,null)); ?>
							</div>
							<div align="center" style="padding-top:10px">
								<button type="button" class="admin_button" onclick="document.getElementById('mytab2').tabber.tabShow(1)">Next</button>
							</div>
					</div>
					<div class="tabbertab" style="min-height:350px;overflow:hidden;padding-left:20px !important;">
						<h2>Send A Friend</h2>
						<h4>Send A Friend</h4>
							<div class="categories" style="margin-right: 5px; padding: 10px; width: 540px;height:358px;">
								
									<?php echo $this->Tree->showPages('ProgramPage.title', $program_pages_sendafriend, null, '/admin/program_pages/edit/'. $this->Form->value('Program.id'). '/', false, 'Page.id', 2, null, 'enabled', array(0, null)); ?>
							</div>
							<div class="categories" style="padding: 10px; width: 250px;height:358px;margin-left: -13px;">
								<h4>Public Program Pages</h4>
								<?php echo $this->Tree->showPages('ProgramPage.title', $program_public_pages_sendafriend, null, '/admin/program_pages/edit/'. $this->Form->value('Program.id'). '/', false, 'Page.id', 2, null, 'enabled', array(0,null)); ?>
							</div>
							<div align="center" style="padding-top:10px">
								<button type="button" class="admin_button" onclick="document.getElementById('mytab2').tabber.tabShow(1)">Next</button>
							</div>
					</div>
					<div class="tabbertab" style="min-height:350px;overflow:hidden;padding-left:20px !important;">
						<h2>Points</h2>
						<h4>Points</h4>
							<div class="categories" style="margin-right: 5px; padding: 10px; width: 540px;height:358px;">
								
									<?php echo $this->Tree->showPages('ProgramPage.title', $program_pages_points, null, '/admin/program_pages/edit/'. $this->Form->value('Program.id'). '/', false, 'Page.id', 3, null, 'enabled', array(0, null)); ?>
							</div>
							<div class="categories" style="padding: 10px; width: 250px;height:358px;margin-left: -13px;">
								<h4>Public Program Pages</h4>
								<?php echo $this->Tree->showPages('ProgramPage.title', $program_public_pages_points, null, '/admin/program_pages/edit/'. $this->Form->value('Program.id'). '/', false, 'Page.id', 3, null, 'enabled', array(0,null)); ?>
							</div>
							<div align="center" style="padding-top:10px">
								<button type="button" class="admin_button" onclick="document.getElementById('mytab2').tabber.tabShow(1)">Next</button>
							</div>
					</div>
				</div>
		</div>
			
   		<div class="tabbertab" style="min-height:150px;overflow:hidden">
		    <h2>Clients</h2>
       		<?php if ($this->Html->value('Program.id') != '0') : ?>
				<div id="Lists" style="clear:both;">&nbsp;</div>
				<div class="list">
					&nbsp;&nbsp;&nbsp;&nbsp;
					<a href="javascript:void(0)" onclick="return displaySearchClients(this);">search...</a>
					<span id="SearchClients" style="display:none;padding-left: 12px;">
						<form action="<?php echo $this->Html->url('/admin/programs/ajax_client_search'); ?>" method="get" style="display:inline;" onsubmit="return searchClients(this);" id="ClientSearchForm">
							<?php echo $this->Form->input('Client.search', array('style' => 'width:8em;font-size:10px;', 'div'=>false, 'label'=>false)); ?>
							<button style="font-size:10px;">Search</button>
						</form>
					</span>
					<div id="Clients">Retrieving clients...</div>
					<script type="text/javascript">
					$(function() {
						$.ajax({
							url: "/admin/programs/ajax_clients/<?php echo $this->Form->value('Program.id');?>",
							type: "GET",
							success: function (data) {
								$('#Clients').html(data);
							}
						});
					});</script>
				</div>
				<div style="padding-top:10px" align="center">
					<button type="button" class="admin_button" onclick="document.getElementById('mytab2').tabber.tabShow(0)">Previous</button>
					<button type="button" class="admin_button" onclick="document.getElementById('mytab2').tabber.tabShow(2)">Next</button>
				</div>
			<?php endif; ?>	
  		</div>

  		<div class="tabbertab" style="min-height:150px;overflow:hidden">
			<h2>Categories</h2>
			<?php if ($this->Html->value('Program.id') != '0') : ?>
				<div class="list" id="category_list">
					<h4>Categories</h4>
					<div id="Categories" style="margin:0;">Retrieving categories...</div>
					<script type="text/javascript">
					$(function() {
						$.ajax({
							url: "/admin/programs/ajax_categories/<?php echo $this->Form->value('Program.id');?>",
							type: "GET",
							success: function (data) {
								$('#Categories').html(data);
							}
						});
					});</script>
				</div>
				<div class="list" id="CategoryProducts"></div>
				<br>
				<div style="padding-top:10px" align="center">
					<button type="button" class="admin_button" onclick="document.getElementById('mytab2').tabber.tabShow(2)">Previous</button>
				</div>
			<?php endif; ?>
		</div>
	</div>
</div>
	<script type="Text/javascript">
		function displaySearchClients(search_link) {
			$(this).hide();
			$('#SearchClients').show();
			return false;
		}
	
		function searchClients(form) {
			var params = form.serialize();
			$.ajax({
				url: "/admin/programs/ajax_clients/<?php echo $this->Form->value('Program.id');?>?"+ params,
				type: "GET",
				success: function (data) {
					$('#Clients').html(data);
				}
			});
			return false;
		}
	
		function displaySearchProducts(search_link) {
	
			search_link.style.display = 'none';
			$('SearchProducts').style.display = 'inline';
			return false;
		}
	
		function searchProducts(form) {
			var params = form.serialize();
			$.ajax({
				url: "/admin/programs/ajax_products/<?php echo $this->Form->value('Program.id');?>?"+ params,
				type: "GET",
				success: function (data) {
					$('#Products').html(data);
				}
			});
			return false;
		}
	</script>
