<?php $this->Paginator->options(
		array(
			'update' => '#Categories',
			'evalScripts' => true,
			'url' => $this->params['pass'],
			'model'=>'Category',
			'sort' => 'Category.name'));
			
	?>
	<div style="float:left;padding-right: 40px;">
		<table cellpadding="0" cellspacing="0" style="width:390px">
			<tr>
				<th style="text-align:left"><?php echo $this->Paginator->sort('id', 'Category.id');?></th>
				<th style="text-align:left"><?php echo $this->Paginator->sort('Name', 'Category.description');?></th>
			</tr>
			<?php
			$i = 0;$count=0;
			foreach ($programs_category as $product):
				$class = null;
				if ($i++ % 2 == 0) {
					$class = ' class="altrow"';
				}
				$count++;
				if($count==11):
			?>
		</table>
	</div>
	<div>
		<table cellpadding="0" cellspacing="0" style="width:390px">
			<tr>
				<th style="text-align:left"><?php echo $this->Paginator->sort('id', 'Category.id');?></th>
				<th style="text-align:left"><?php echo $this->Paginator->sort('Name', 'Category.description');?></th>
			</tr>
			 <?php endif; ?>  
			<tr<?php echo $class;?>>
				<td>
					<?php echo $product['Category']['id'] ?>
				</td>
				<td>
					<a href="javascript:void(0)" onclick="updateCategoryProducts(<?php echo $product['Category']['id']; ?>"><?php echo $product['Category']['description'] ?></a>
				</td>
			</tr>
			<?php endforeach; ?>
		</table>
	</div>
	<div class="paging" style="width:822px">
		<table style="border:none !important;padding-top:25px;float:right;">
			<tr>
				<td style="border:none !important;" valign="top">
					<?php echo $this->Paginator->first('<<', array('title'=>'first','class'=>'disabled cpagination1'));?>
					<?php echo $this->Paginator->prev('<', array('title'=>'prev','class'=>'disabled cpagination1'));?>
					<?php echo $this->Paginator->numbers(array('separator' => ' ','class'=>'cpagination1'));?>
					<?php echo $this->Paginator->next('>', array('title'=>'next','class'=>'disabled cpagination1'));?>
					<?php echo $this->Paginator->last('>>', array('title'=>'last','class'=>'disabled cpagination1'));?>
				</td>
			</tr>
			<tr>
				<td style="border:none !important;padding-left:5px;padding-top:1px;">
					<p><?php echo $this->Paginator->counter(array('format' => __('Page %page% of %pages%', true)));	echo $this->Js->writeBuffer();?></p>
				</td>
			</tr>
		</table>
	</div>
	<script>
	function updateCategoryProducts(prdtId) {
		$.ajax({
			url: "/admin/programs/ajax_category_products/<?php echo $id;?>/"+prdtId+"/page:1/sort:Product.name/direction:asc",
			type: "GET",
			success: function (data) {
				$('#CategoryProducts').html(data);
			}
		});
		return false;
	}
	</script>
