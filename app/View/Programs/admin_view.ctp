<h2>Program - <?php echo $this->Form->input('Program.name'); ?></h2>
<div class="actions">
	<ul>
		<li><?php echo $this->Html->link('Cancel', $this->Session->read('redirect')); ?></li>
		<li><?php echo $this->Html->link(__('Delete', true), array('action'=>'delete', $this->Form->value('Program.id')), null, sprintf(__('Are you sure you want to delete Program \'%s?\'', true), $this->Form->value('Program.name'))); ?></li>
	</ul>
</div>
<div class="edit">
	<table>
		<tr>
			<td class="bt">Name</td>
			<td class="bt"><?php echo $this->Form->input('Program.name'); ?></td>
			<td class="bl"><?php echo $this->Form->error('Program.name', array('required' => 'Name required', 'unique' => 'Name already in use'), array('required' => 'Name required')); ?></td>
		</tr>
		<tr>
			<td>Style</td>
			<td class="br"><?php echo $this->Form->input('Program.style_id', array('div'=>false, 'label'=>false, 'type'=>'select', 'options'=>$styles)); ?></td>
		</tr>
		<tr>
			<td>Categories</td>
			<td>
				<?php echo $this->Form->input('Category.Category', array('multiple' => 'multiple')); ?>
				<input type="hidden" name="data[Category][Category][]" value="" />
			</td>
			<td class="bl"></td>
		</tr>
		<tr>
			<td>Default offer image</td>
			<td>
				<img src="<?php echo $this->Html->url(FILES_WWW_PATH. 'default_product_image/'. $this->Form->value('Program.id'). '.'. $this->Form->value('Program.default_product_image_extension')); ?>" alt="" /><br/>
				<?php echo $this->Form->file('Image.default_product_image'); ?>
			</td>
			<td class="bl"></td>
		</tr>
		<tr>
			<td class="bb"></td>
			<td class="bb br"><button type="submit">Submit</button>
		</tr>
	</table>
	<?php echo $this->Form->input('Program.id', array('type' => 'hidden')); ?>
	</form>
</div>
<div class="categories">
	<h4>Program Pages</h4>
	<?php echo $this->Tree->show('Page.title', $program_pages, null, '/admin/program_pages/edit/'. $this->Form->value('Program.id'). '/', false, 'Page.id'); ?>
</div>
<div class="categories">
	<h4>Public Program Pages</h4>
	<?php echo $this->Tree->show('ProgramPage.title', $program_public_pages, null, '/admin/program_pages/edit/'. $this->Form->value('Program.id'). '/', false, 'Page.id'); ?>
</div>

<div class="list">
	<h4>Clients</h4>
	<table cellpadding="0" cellspacing="0">
	<tr>
		<th><?php echo $this->Paginator->sort('id');?></th>
		<th><?php echo $this->Paginator->sort('Name', 'Client.name');?></th>
		<th class="actions"><?php echo "Actions";?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($clients as $client):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
		<tr<?php echo $class;?>>
			<td>
				<?php echo $client['Client']['id'] ?>
			</td>
			<td>
				<?php echo $client['Client']['name'] ?>
			</td>
			<td class="actions">
				<?php echo $this->Html->link('Edit', '/admin/clients/edit/'. $client['Client']['id']); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
		<div class="paging">
		<?php echo $this->Paginator->prev('<< Previous', array(), null, array('class'=>'disabled'));?>
		<?php echo $this->Paginator->numbers();?>
		<?php echo $this->Paginator->next('Next >>', array(), null, array('class'=>'disabled'));?>
	</div>
	<p><?php echo $this->Paginator->counter(array('format' => __('Page %page% of %pages%', true)));	?></p>
</div>