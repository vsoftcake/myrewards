<?php if(in_array($this->Session->read('dashboardidinsession'),$dashArray) && ($campaigns[0]['Campaign']['notification_popup']==1)) : 
echo "<table align='center' width='600'>";
			$filepath = CAMPAIGN_IMAGE_PATH;
			$filename = $campaigns[0]['Campaign']['id']. '.'. $campaigns[0]['Campaign']['notification_image_ext'];
			$path = $this->Html->url(CAMPAIGN_IMAGE_WWW_PATH.'/notification_image/'. $campaigns[0]['Campaign']['id']. '.'. $campaigns[0]['Campaign']['notification_image_ext']); 
			$path_prod = substr($path,strpos($path)+1);
			$img=$this->ImageResize->getResizedDimensions($path_prod, 600, 320);
														
			if($campaigns[0]['Campaign']['winner_message']=='F') :
				echo "<tr><td style='text-align:center'>";?>
				<img width="<?php echo $img['width'];?>" height="<?php echo $img['height'];?>" src="<?php echo $this->Html->url('/'. $path_prod);?>" style="margin:5px;"/>
				<?php echo "</td></tr>";
			else :
				echo "<tr><td>".$campaigns[0]['Campaign']['notification_message']."</td></tr>";
			endif;
			if($campaigns[0]['Campaign']['campaign_type']=='A') :
				echo "<tr><td align='center' style='padding-top:10px'>";?>
					<h2>To win the prize please provide your details by clicking
						<a href='/campaigns/details/3/<?php echo $campaigns[0]['Campaign']['id'];?>'>
							 here
						</a>
					</h2>
				<?php echo "</td></tr>";
			elseif($campaigns[0]['Campaign']['campaign_type']=='M') :
				echo "<tr><td align='center' style='padding-top:10px'>";?>
					<h2>Click <a href='/campaigns/details/6/<?php echo $campaigns[0]['Campaign']['id'];?>'>here</a> to accept your prize</h2>
			<?php echo "</td></tr>";
			endif;
			echo "</table>";
	endif; ?>