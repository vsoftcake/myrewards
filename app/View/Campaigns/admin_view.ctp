<style>
.tr 
{
	height:35px;
}

#content {
    border-bottom: 1px solid #CCCCCC;
    border-left: 1px solid #CCCCCC;
    border-right: 1px solid #CCCCCC;
    margin-left: 20px !important;
    padding-left: 15px !important;
    width: 938px;
    padding-right:0px !important;
    margin-bottom:20px !important;
}

</style>
	<table style="margin:10px;">
			<tr>
				<td style="padding-right:20px">
					<button onclick="parent.location='/admin/campaigns/edit/<?php echo $details[0]['Campaign']['id'] ?>'" class="admin_button">Edit</button>
				</td>
				<td style="padding-right:20px">
					<button onclick="parent.location='/admin/campaigns/users_participated/<?php echo $details[0]['Campaign']['id'] ?>'" class="admin_button">View Participants</button>
				</td>
				<td style="padding-right:20px">
					<button onclick="parent.location='/admin/campaigns/view_winners/<?php echo $details[0]['Campaign']['id'] ?>'" class="admin_button">View Winners</button>
				</td>
				<td style="padding-right:20px">
				<?php 
				$days = '';
				if(date('w')==0) :
					$days .= 6;
				endif;
				if(date('w') == 1) :
					$days .= 0;
				endif;
				if(date('w') == 2) :
					$days .= 1;
				endif;
				if(date('w') == 3) :
					$days .= 2;
				endif;
				if(date('w') == 4) :
					$days .= 3;
				endif;
				if(date('w') == 5) :
					$days .= 4;
				endif;
				if(date('w') == 6) :
					$days .= 5;
				endif;
				?>
				<?php if((date("Y-m-d") >= $details[0]['Campaign']['draw_start_date']) && (date("Y-m-d") <= $details[0]['Campaign']['draw_end_date']) && ($wcount < $details[0]['Campaign']['max_winners'])) :?>
					<?php if(($details[0]['Campaign']['draw_type'] == 'MD') && ($details[0]['Campaign']['active']==1) && (strpos($details[0]['Campaign']['week_days'],$days) !== false)) :?>
						<?php if($details[0]['Campaign']['campaign_type'] == 'M'): ?>
							<?php if($pcount!=''  || !empty($pcount)) : ?>
								&nbsp;&nbsp;&nbsp;
								<button onclick="parent.location='/admin/campaigns/manual_draw/<?php echo $details[0]['Campaign']['id'] ?>'" class="admin_button">Draw</button>
							<?php endif; ?>
						<?php  elseif($details[0]['Campaign']['campaign_type'] == 'A') :  ?>
							&nbsp;&nbsp;&nbsp;
							<button onclick="parent.location='/admin/campaigns/manual_draw/<?php echo $details[0]['Campaign']['id'] ?>'" class="admin_button">Draw</button>
						<?php endif; ?>
					<?php endif; ?>
				<?php endif; ?>
								
				</td>
				<td style="padding-right:20px">
					<button onclick="parent.location='/admin/campaigns/list_campaign'" class="admin_button rounded_corners_button">View Competitions List</button>
				</td>
				<td style="padding-right:20px">
					<button onclick="parent.location='/admin/campaigns/index'" class="admin_button rounded_corners_button">Back To Competition Home</button>
				</td>
			</tr>
		</table> 
	<div style="border:2px solid #DFDFDF;margin-top: 10px; padding: 10px; width: 900px;">
		<table width="100%">
			<tr>
				<td style="font-size:22px;font-weight:bold;text-align:left;">
					<?php echo $details[0]['Campaign']['name'];?>
				</td>
				<?php if((date("Y-m-d") >= $details[0]['Campaign']['entry_start_date']) && (date("Y-m-d") <= $details[0]['Campaign']['entry_end_date']) && ($count[0][0]['count']< $details[0]['Campaign']['participants_limit'])) :?>
				<td style="font-size:16px;font-weight:bold;text-align:right;">
					<?php
						$edate = explode('-',$details[0]['Campaign']['end_date']);
						$cedate = $edate[2]."-".$edate[1]."-".$edate[0];
					?>
					Competition ends on : <?php echo $cedate; ?>
				</td>
				<?php endif;?>
			</tr>
		</table>
		<br><br>
		<table width="100%">
			<?php if($details[0]['Campaign']['layout']==0) : ?>
			<tr>
				<td valign="top">
					<?php 
						$filepath = CAMPAIGN_IMAGE_PATH;
						$filename = $details[0]['Campaign']['id']. '.'. $details[0]['Campaign']['campaign_logo_extension'];
						$path = $this->Html->url(CAMPAIGN_IMAGE_WWW_PATH.'/image/'. $details[0]['Campaign']['id']. '.'. $details[0]['Campaign']['campaign_logo_extension']); 
						$path_prod = substr($path,strpos($path)+1);
						$img=$this->ImageResize->getResizedDimensions($path_prod, 780, 450);
					?>
					<img style="padding-left:20px;padding-right:20px" width="<?php echo $img['width'];?>" height="<?php echo $img['height'];?>" src="<?php echo $this->Html->url('/'. $path_prod);?>"/>
				</td>
			</tr>
			<tr>
				<td valign="top">
					<table>
			<?php else : ?>
			<tr>
				<td valign="top">
					<?php 
						$filepath = CAMPAIGN_IMAGE_PATH;
						$filename = $details[0]['Campaign']['id']. '.'. $details[0]['Campaign']['campaign_logo_extension'];
						$path = $this->Html->url(CAMPAIGN_IMAGE_WWW_PATH.'/image/'. $details[0]['Campaign']['id']. '.'. $details[0]['Campaign']['campaign_logo_extension']); 
						$path_prod = substr($path,strpos($path)+1);
						$img=$this->ImageResize->getResizedDimensions($path_prod, 380, 500);
					?>
					<img style="padding-left:20px;padding-right:20px" width="<?php echo $img['width'];?>" height="<?php echo $img['height'];?>" src="<?php echo $this->Html->url('/'. $path_prod);?>"/>
				</td>
				<td valign="top">
					<table>
			<?php endif; ?>
									<tr>
										<td valign="top">
											<?php echo $details[0]['Campaign']['description'];?>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
					
			<br><br>
			<table align="center">
				<tr>
					<?php if(!empty($details[0]['Campaign']['rules'])) : ?>
					<td style="width:150px">
						<button style="height:29px !important;" class="admin_button rounded_corners_button">Competition Rules</button>
					</td>
					<?php endif; ?>

					<?php if($details[0]['Campaign']['campaign_type']=='M') : ?>
						<?php if(empty($existing_users)) : ?>
							<?php if((date("Y-m-d") >= $details[0]['Campaign']['entry_start_date']) && (date("Y-m-d") <= $details[0]['Campaign']['entry_end_date']) && (($count[0][0]['count']< $details[0]['Campaign']['participants_limit']) || ($details[0]['Campaign']['participants_limit']==''))) :?>
							<td>
								<?php if($details[0]['Campaign']['participate_button_image_ext']=='') : ?>
										<button class="admin_button rounded_corners_button">Participate</button>
								<?php else : ?>
									<?php 
										$filepath = CAMPAIGN_IMAGE_PATH;
										$filename = $details[0]['Campaign']['id']. '.'. $details[0]['Campaign']['participate_button_image_ext'];
										$path = $this->Html->url(CAMPAIGN_IMAGE_WWW_PATH.'/participate_image/'. $details[0]['Campaign']['id']. '.'. $details[0]['Campaign']['participate_button_image_ext']); 
										$path_prod = substr($path,strpos($path)+1);
										$img=$this->ImageResize->getResizedDimensions($path_prod, 140, 35);
									?>
									<img style="padding-left:20px;padding-right:20px;padding-top:5px" width="<?php echo $img['width'];?>" height="<?php echo $img['height'];?>" src="<?php echo $this->Html->url('/'. $path_prod);?>"/>

								<?php endif; ?>
							</td>
							<?php endif; ?>
						<?php endif; ?>
					<?php endif; ?>
				</tr>
			</table>
			</div>
		<br /><br />
		<div style="border: 2px solid #dfdfdf; width: 880px; padding: 20px;">
		<table>
      		<tr>
          		<td colspan="2"><h4>Basic Details</h4><td>
      		</tr>    
      		<tr class="tr">
          		<td style="width:200px"><strong>Title of the competition</strong></td>
	          	<td><?php echo $details[0]['Campaign']['name']; ?></td>
      		</tr>
      		<tr class="tr">
          		<td><strong>Competition description</strong></td>
          		<td><?php echo $details[0]['Campaign']['description']; ?></td>
      		</tr>
      		<tr class="tr">
		         <td><strong>Competition eligibility or other rules</strong></td>
		         <td><?php echo $details[0]['Campaign']['rules']; ?></td>
		     </tr>
		     <tr class="tr">
		         <td><strong>Logo/Image image</strong></td>
		         <td>
					<?php 
						$filepath = CAMPAIGN_IMAGE_PATH;
						$filename = $details[0]['Campaign']['id']. '.'. $details[0]['Campaign']['campaign_logo_extension'];
						$path = $this->Html->url(CAMPAIGN_IMAGE_WWW_PATH.'/image/'. $details[0]['Campaign']['id']. '.'. $details[0]['Campaign']['campaign_logo_extension']); 
						$path_prod = substr($path,strpos($path)+1);
						$img=$this->ImageResize->getResizedDimensions($path_prod, 200, 200);
					?>
					<img style="padding-right:20px" width="<?php echo $img['width'];?>" height="<?php echo $img['height'];?>" src="<?php echo $this->Html->url('/'. $path_prod);?>"/>
				</td>
		     </tr>
		    
		     <tr class="tr">
		         <td><strong>Logo/Image layout</strong></td>
		         <td>
		         	<?php if($details[0]['Campaign']['layout'] == 0) : 
		         		echo "Logo/Image on top";
					else : 
					echo "Logo/Image on left";
					endif;
					?>
				</td>
		     </tr>
		     <?php if(!empty($details[0]['Campaign']['participate_button_image_ext']) || $details[0]['Campaign']['participate_button_image_ext']!='') :?>
		     <tr class="tr">
				 <td><strong>Participate button image</strong></td>
				 <td>
					<?php 
						$filepath = CAMPAIGN_IMAGE_PATH;
						$filename = $details[0]['Campaign']['id']. '.'. $details[0]['Campaign']['participate_button_image_ext'];
						$path = $this->Html->url(CAMPAIGN_IMAGE_WWW_PATH.'/participate_image/'. $details[0]['Campaign']['id']. '.'. $details[0]['Campaign']['participate_button_image_ext']); 
						$path_prod = substr($path,strpos($path)+1);
						$img=$this->ImageResize->getResizedDimensions($path_prod, 140, 35);
					?>
					<img style="padding-right:20px" width="<?php echo $img['width'];?>" height="<?php echo $img['height'];?>" src="<?php echo $this->Html->url('/'. $path_prod);?>"/>
				</td>
		     </tr>
		     <?php endif;?>
		</table>
		<br><br>
		<table>
			<tr>
          		<td><h4>Eligibilty</h4><td>
      		</tr>  
      		<tr class="tr">
      			<td style="width:200px;"><strong>Programs</strong></td>
				<td><?php echo $programsList; ?></td>
      		</tr>
      		<tr class="tr">
      			<td style="width:200px;"><strong>Clients</strong></td>
				<td><?php echo $clientsList; ?></td>
      		</tr>
		    <tr class="tr">
		    	<td style="width:200px"><strong>Competition start date</strong></td>
		        <td><?php echo $details[0]['Campaign']['start_date']; ?></td>
		    </tr>
		    <tr class="tr">
		    	<td><strong>Competition end date</strong></td>
		        <td><?php echo $details[0]['Campaign']['end_date']; ?></td>
		    </tr>
		</table>
		<br><br>
		<table>
			<tr>
          		<td><h4>Participants</h4><td>
      		</tr> 
			<tr class="tr">
		    	<td style="width:200px"><strong>Competition participation</strong></td>
		        <?php if($details[0]['Campaign']['campaign_type']=='M') :?>
		        <td><?php echo "Users should explicitly register on the competition page"; ?></td>
		        <?php else : ?>
		        <td><?php echo "Let the system choose the users as per the eligibility criteria"; ?></td>
		          <?php endif; ?>
		     </tr>
		     <?php if($details[0]['Campaign']['campaign_type']=='M') :?>
		     <tr class="tr">
		     	<td style="width:200px"><strong>Competition winner type</strong></td>
		     	<td>
		     		<?php if($details[0]['Campaign']['manual_winner_type']=='default') :
		     			echo "Default";
		     		elseif($details[0]['Campaign']['manual_winner_type']=='choose') :
		     			echo "Choose winners";
		     		endif;
		     		?>
		     	</td>
		     </tr>
		     <tr class="tr">
		     	<td style="width:200px"><strong>User participation start date</strong></td>
		     	<td><?php echo $details[0]['Campaign']['entry_start_date'];?></td>
		     </tr>
		     <tr>
		     	<td style="width:200px"><strong>User participation end date</strong></td>
		     	<td><?php echo $details[0]['Campaign']['entry_end_date'];?></td>
		     </tr>
		     <tr class="tr">
		    	<td><strong>Number of participants</strong></td>
		        <td><?php echo $details[0]['Campaign']['participants_limit']; ?></td>
		    </tr>
		    <?php endif; ?>
		    <?php if($details[0]['Campaign']['campaign_type']=='A') :?>
		     <tr class="tr">
		     	<td style="width:200px"><strong>User participation type</strong></td>
		     	<td>
		     		<?php if($details[0]['Campaign']['user_type']=='LP') :
		     			echo "Login period";
		     		elseif($details[0]['Campaign']['user_type']=='FL') :
		     			echo "First time login";
		     		elseif($details[0]['Campaign']['user_type']=='AU') :
		     			echo "All users";
		     		endif;
		     		?>
		     	</td>
		     </tr>
		     <?php if($details[0]['Campaign']['user_type']=='LP') :?>
		     <tr class="tr">
		     	<td style="width:200px"><strong>Login start date</strong></td>
		     	<td><?php echo $details[0]['Campaign']['login_start_date'];?></td>
		     </tr>
		     <tr>
		     	<td style="width:200px"><strong>Login end date</strong></td>
		     	<td><?php echo $details[0]['Campaign']['login_end_date'];?></td>
		     </tr>
		     <?php endif;?>
		    <?php endif; ?>
		    <?php if(!empty($details[0]['Campaign']['participation_message']) || $details[0]['Campaign']['participation_message']!='') : ?>
		     <tr class="tr">
		    	<td><strong>Participation message</strong></td>
		        <td><?php echo $details[0]['Campaign']['participation_message']; ?></td>
		    </tr>
		    <?php endif; ?>
		</table>	
		<br><br>
		<table>
			<tr>
          		<td><h4>Draw</h4><td>
      		</tr> 
			<tr class="tr">
		    	<td style="width:200px"><strong>Type of draw</strong></td>
		       	<td>
			<?php if($details[0]['Campaign']['draw_type'] == 'MD') : 
				echo "I would like to draw the winner(s) here manually";
			 elseif($details[0]['Campaign']['draw_type'] == 'AD') : 
				echo "Let the system draw the winner(s)";
			endif; ?>	
		       	</td>
		     </tr>
		     <tr class="tr">
		    	<td><strong>Draw start date</strong></td>
		       	<td><?php echo $details[0]['Campaign']['draw_start_date'];?></td>
		     </tr>
		     <tr class="tr">
		    	<td><strong>Draw end date</strong></td>
		       	<td><?php echo $details[0]['Campaign']['draw_end_date'];?></td>
		     </tr>
		     <tr class="tr">
		    	<td><strong>Number of winners</strong></td>
		        <td><?php echo $details[0]['Campaign']['max_winners']; ?></td>
		    </tr>
		    <tr class="tr">
		    	<td><strong>Number of winners per draw</strong></td>
		        <td><?php echo $details[0]['Campaign']['winners_per_draw']; ?></td>
		    </tr>
		</table>	
		<br><br>        
		<table>
			<tr>
          		<td><h4>Winner Notification</h4><td>
      		</tr> 
			<tr class="tr">
		    	<td style="width:200px"><strong>Notification alert</strong></td>
		       	<td>
		       		<?php 
		       			if($details[0]['Campaign']['notification_popup'] == 1 && $details[0]['Campaign']['notification_email'] == 0):
		       				echo "Popup";
		       			elseif($details[0]['Campaign']['notification_popup'] == 0 && $details[0]['Campaign']['notification_email'] == 1):
		       				echo "Email";
		       			elseif($details[0]['Campaign']['notification_popup'] == 1 && $details[0]['Campaign']['notification_email'] == 1) :
		       				echo "Popup, Email";
		       		endif; ?>
		       	</td>
		     </tr>
		     <tr class="tr">
		    	<td  valign="top" style="width:200px"><strong>Notification message</strong></td>
		       	<td>
		       		<?php 
		       			if($details[0]['Campaign']['winner_message'] == 'D'):
		       				echo "<div>Description</div><br><div>".$details[0]['Campaign']['notification_message']."</div>";
		       			elseif($details[0]['Campaign']['winner_message'] == 'F'):
		       		?>
		       		<?php 
						$filepath = CAMPAIGN_IMAGE_PATH;
						$filename = $details[0]['Campaign']['id']. '.'. $details[0]['Campaign']['notification_image_ext'];
						$path = $this->Html->url(CAMPAIGN_IMAGE_WWW_PATH.'/notification_image/'. $details[0]['Campaign']['id']. '.'. $details[0]['Campaign']['notification_image_ext']); 
						$path_prod = substr($path,strpos($path)+1);
						$img=$this->ImageResize->getResizedDimensions($path_prod, 200, 200);
						echo "<div>Flyer</div><br><div>";
					?>
					
					<img width="<?php echo $img['width'];?>" height="<?php echo $img['height'];?>" src="<?php echo $this->Html->url('/'. $path_prod);?>"/>
					
		       	<?php echo "</div>"; endif; ?>
		       	</td>
		     </tr>
		</table> 
		</div>
		<table style="margin:10px;">
			<tr>
				<td style="padding-right:20px">
					<button onclick="parent.location='/admin/campaigns/edit/<?php echo $details[0]['Campaign']['id'] ?>'" class="admin_button">Edit</button>
				</td>
				<td style="padding-right:20px">
					<button onclick="parent.location='/admin/campaigns/users_participated/<?php echo $details[0]['Campaign']['id'] ?>'" class="admin_button">View Participants</button>
				</td>
				<td style="padding-right:20px">
					<button onclick="parent.location='/admin/campaigns/view_winners/<?php echo $details[0]['Campaign']['id'] ?>'" class="admin_button">View Winners</button>
				</td>
				<td style="padding-right:20px">
					<?php if((date("Y-m-d") >= $details[0]['Campaign']['draw_start_date']) && (date("Y-m-d") <= $details[0]['Campaign']['draw_end_date']) && ($wcount < $details[0]['Campaign']['max_winners'])) :?>
						<?php if(($details[0]['Campaign']['draw_type'] == 'MD') && ($details[0]['Campaign']['active']==1) && (strpos($details[0]['Campaign']['week_days'],$days) !== false)) :?>
							<?php if($details[0]['Campaign']['campaign_type'] == 'M'): ?>
								<?php if($pcount!=''  || !empty($pcount)) : ?>
									&nbsp;&nbsp;&nbsp;
										<button onclick="parent.location='/admin/campaigns/manual_draw/<?php echo $details[0]['Campaign']['id'] ?>'" class="admin_button">Draw</button>
								<?php endif; ?>
							<?php  elseif($details[0]['Campaign']['campaign_type'] == 'A') :  ?>
								&nbsp;&nbsp;&nbsp;
									<button onclick="parent.location='/admin/campaigns/manual_draw/<?php echo $details[0]['Campaign']['id'] ?>'" class="admin_button">Draw</button>
							<?php endif; ?>
						<?php endif; ?>
					<?php endif; ?>
				</td>
				<td style="padding-right:20px">
					<button onclick="parent.location='/admin/campaigns/list_campaign'" class="admin_button rounded_corners_button">View Competitions List</button>
				</td>
				<td style="padding-right:20px">
					<button onclick="parent.location='/admin/campaigns/index'" class="admin_button rounded_corners_button">Back To Competition Home</button>
				</td>
			</tr>
		</table> 