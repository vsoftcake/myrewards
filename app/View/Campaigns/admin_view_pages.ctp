<style>
#content {
    border-bottom: 1px solid #CCCCCC;
    border-left: 1px solid #CCCCCC;
    border-right: 1px solid #CCCCCC;
    margin-left: 20px !important;
    padding-left: 15px !important;
    width: 938px;
    padding-right:0px !important;
    margin-bottom:20px !important;
}
</style>
<?php //$this->Paginator->options(array('url' => array_merge($this->params['pass'], $this->params['named']))); ?>
<div style="padding:10px;float:right">
	<button onclick="parent.location='/admin/campaigns/'" class="admin_button rounded_corners_button">Back To Competition Home</button>
	<button onclick="parent.location='/admin/campaigns/list_campaign'" class="admin_button rounded_corners_button">View Competitions List</button>
</div>
<div style="padding-top:20px;">
	<div>
	<div id="maincontent">
		<div>
			<table width="920" border="0" style="vertical-align: middle;">
	    		<tr class="produtos">
			        <td style="padding-left: 15px;">Competition Title</td>
			        <td style="padding-left: 15px;">Start Date</td>
			        <td style="padding-left: 15px;">End Date</td>
			        <td style="padding-left: 15px;">Draw Type</td>
			        <td style="padding-left: 15px;"></td>
			    </tr>
			    <?php foreach ($campaigns as $campaign): ?>
		        <tr>
			        <td style="vertical-align: middle; padding-left: 15px;"><?php echo $campaign['Campaign']['name'] ?></td>
			        <td style="vertical-align: middle; padding-left: 15px;"><?php echo $campaign['Campaign']['start_date'] ?></td>
			        <td style="vertical-align: middle; padding-left: 15px;"><?php echo $campaign['Campaign']['end_date'] ?></td>
			        <td style="vertical-align: middle; padding-left: 15px;">
			        	<?php if($campaign['Campaign']['campaign_type']=='M') : 
								echo "Manual Participation"; 
							elseif($campaign['Campaign']['campaign_type']=='A') : 
								echo "Auto Participation";	
							endif;
						?>
			        </td>
			        <td style="vertical-align: middle; padding: 10px;">
			        	<table>
							<tr>
								<td>
									<button onclick="parent.location='/admin/campaigns/users_participated/<?php echo $campaign['Campaign']['id'] ?>'" class="admin_button">View Participants</button>
								</td>
								<td>
									<button onclick="parent.location='/admin/campaigns/view_winners/<?php echo $campaign['Campaign']['id'] ?>'" class="admin_button">View Winners</button>
								</td>
								<td>
									<button onclick="parent.location='/admin/campaigns/view/<?php echo $campaign['Campaign']['id'] ?>'" class="admin_button rounded_corners_button">View</button>
								</td>
							</tr>
						</table> 
			        </td>
		    	</tr>
		    	<?php endforeach; ?>
		   </table>
		</div>
	</div>
</div>
