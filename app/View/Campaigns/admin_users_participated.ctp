<style>
#content {
    border-bottom: 1px solid #CCCCCC;
    border-left: 1px solid #CCCCCC;
    border-right: 1px solid #CCCCCC;
    margin-left: 20px !important;
    padding-left: 15px !important;
    width: 938px;
    padding-right:0px !important;
    margin-bottom:20px !important;
}
</style>
<?php $this->Paginator->options(array('url' => array_merge($this->params['pass'], $this->params['named'])));?>

	<div>
		<div style="float:left">
			<h4 style="padding-top: 10px; padding-bottom: 10px;">
				<?php echo $users_list[0]['Campaign']['name']; ?>
			</h4>
		</div>
		<div style="padding:10px;float:right">
			<button onclick="parent.location='/admin/campaigns/view/<?php echo $users_list[0]['Campaign']['id']; ?>'" class="admin_button rounded_corners_button">View</button>
			<button onclick="parent.location='/admin/campaigns/view_winners/<?php echo $users_list[0]['Campaign']['id'] ?>'" class="admin_button">View Winners</button>
			<button onclick="parent.location='/admin/campaigns/list_campaign'" class="admin_button rounded_corners_button">View Competitions List</button>
			<button onclick="parent.location='/admin/campaigns/index'" class="admin_button rounded_corners_button">Back To Competition Home</button>
	</div>
	
	<form name="winnerForm" method="post">
		<div id="maincontent">
			<div>
				<table width="920" border="0" style="vertical-align: middle;">
		    		<tr class="produtos">
		    			<?php if($users_list[0]['Campaign']['manual_winner_type'] == 'choose' ) :?>
		    			<td style="padding-left: 15px;width: 100px">Select Winners</td>
		    			<?php endif;?>
				        <td style="padding-left: 15px;">Client Name</td>
				        <td style="padding-left: 15px;">User Id</td>
				        <td style="padding-left: 15px;">User Name</td>
				        <td style="padding-left: 15px;">User Email</td>
				        <td style="padding-left: 15px; width: 20%"></td>
				    </tr>
				    <?php foreach ($users_list as $list): ?>
			        <tr>
			        	<?php if($list['Campaign']['manual_winner_type'] == 'choose' ) :?>
				        	<td style="vertical-align: middle; padding: 15px;">
					        	<?php echo $this->Form->checkbox('Campaign.check',array('value'=>$list['CampaignEntry']['user_id'])); ?>
					        </td>
				        <?php endif; ?>
				        <td style="vertical-align: middle; padding: 15px;">
				        	<?php echo $list['Client']['name']; ?>
				        </td>
				        <td style="vertical-align: middle; padding: 15px;"><?php echo $list['CampaignEntry']['user_id']; ?></td>
				        <td style="vertical-align: middle; padding: 15px;"><?php echo $list['CampaignEntry']['user_name']; ?></td>
				        <td style="vertical-align: middle; padding: 15px;"><?php echo $list['CampaignEntry']['email']; ?></td>
			    	</tr>
			    	<?php endforeach; ?>
			    	<?php if($users_list[0]['Campaign']['manual_winner_type'] == 'choose' ) :?>
				    	<tr>
				    		<td colspan="2">
				    			<button type="button" style="margin-left:15px;" class="admin_button rounded_corners_button" onclick="selectWinners(<?php echo $users_list[0]['Campaign']['id'];?>)">Choose as Winners</button>
					        </td>
						</tr>
				    <?php endif; ?>
			   </table>
			</div>
		</div>
		
		<br><br>
		<?php if(!empty($users_list)) : ?>
			<div class="paging">
		   		<table style="border:none;">
		      		<tr>
		         		<td style="border:none;">
							<?php echo $this->Paginator->first('<<', array('title'=>'first','class'=>'disabled pagination1'));?>
							<?php echo $this->Paginator->prev('<', array('title'=>'prev','class'=>'disabled pagination1'));?>
							<?php echo $this->Paginator->numbers(array('class'=>'pagination1'));?>
							<?php echo $this->Paginator->next('>', array('title'=>'next','class'=>'disabled pagination1'));?>
							<?php echo $this->Paginator->last('>>', array('title'=>'last','class'=>'disabled pagination1'));?>
			 			</td>
		      		</tr>
		  		</table>    
			</div>
		<?php endif ?>
	</form>
<script>
function selectWinners(id)
{
	var url = '/admin/campaigns/choose_winners_list/';
	var winnersList="";
	var length=document.winnerForm.elements.length;
	for(var i=0;i<length;i++)
	{
		if(document.winnerForm.elements[i].name=="data[Campaign][check]")
		{
			if(document.winnerForm.elements[i].checked)
			{
				if(winnersList.length>0)
					winnersList=winnersList+","+document.winnerForm.elements[i].value;
				else
					winnersList=winnersList+document.winnerForm.elements[i].value;
			}
		}
	}
	if(winnersList=="")
		alert("Please check to select winners");	
	else
		document.location=url+winnersList+'/'+id;
}

</script>