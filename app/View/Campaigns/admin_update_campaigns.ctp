<?php $this->Paginator->options(array('url' => array_merge($this->params['pass'], $this->params['named']))); ?>

<div id="contests">
	<div id="maincontent">
		<div>
			<table width="920" border="0" style="vertical-align: middle;">
	    		<tr class="produtos">
			        <td style="padding-left: 15px;">Competition Title</td>
			        <td style="padding-left: 15px;">Start Date</td>
			        <td style="padding-left: 15px;">End Date</td>
			        <td style="padding-left: 15px;">Draw Type</td>
			        <td style="padding-left: 15px;">Status</td>
			        <td style="padding-left: 15px;"></td>
			    </tr>
			    <?php foreach ($campaigns as $campaign): ?>
		        <tr>
			        <td style="vertical-align: middle; padding-left: 15px;"><?php echo $campaign['Campaign']['name'] ?></td>
			        <td style="vertical-align: middle; padding-left: 15px;"><?php echo $campaign['Campaign']['start_date'] ?></td>
			        <td style="vertical-align: middle; padding-left: 15px;"><?php echo $campaign['Campaign']['end_date'] ?></td>
			        <td style="vertical-align: middle; padding-left: 15px;">
			        	<?php if($campaign['Campaign']['campaign_type']=='M') : 
								echo "Manual Participation"; 
							elseif($campaign['Campaign']['campaign_type']=='A') : 
								echo "Auto Participation";	
							endif;
						?>
			        </td>
			        <td style="vertical-align: middle; padding-left: 15px;">
						<?php if($campaign['Campaign']['active']=='1') : ?> 
								<span style="padding-right:20px">Active</span>
								<?php echo $this->Html->link(__('Pause', true), array('action'=>'status', $campaign['Campaign']['id'], $campaign['Campaign']['active'])); ?>
						<?php else : ?>
								<span style="padding-right:10px">Pause</span>
								<?php echo $this->Html->link(__('Activate', true), array('action'=>'status', $campaign['Campaign']['id'], $campaign['Campaign']['active'])); ?>
						<?php endif; ?>
					</td>
			        <td style="vertical-align: middle; padding: 10px;">
			            <a href="/admin/campaigns/edit/<?php echo $campaign['Campaign']['id'] ?>"><img alt="Edit" width="16" height="16" src="/files/campaign/modify.png" title="Edit"></a>
				        &nbsp;&nbsp;&nbsp;
			           
			            <a href="/admin/campaigns/view/<?php echo $campaign['Campaign']['id'] ?>"><img src="/files/campaign/folder_explore.png/" title="View"></a>		            	
			        	&nbsp;&nbsp;&nbsp;
			        	<a href="/admin/campaigns/users_participated/<?php echo $campaign['Campaign']['id'] ?>">
							<img width="20" style="cursor:pointer" src="/files/campaign/participants.png" alt="Participants" title="Participants">
						</a>
			        	&nbsp;&nbsp;&nbsp;
			        	<a href="/admin/campaigns/view_winners/<?php echo $campaign['Campaign']['id'] ?>">
							<img width="20" title="Winners" alt="Winners" src="/files/campaign/winner.png" style="cursor:pointer">
						</a>
						
						<?php 
							$days = '';
							if(date('w')==0) :
								$days .= 6;
							endif;
							if(date('w') == 1) :
								$days .= 0;
							endif;
							if(date('w') == 2) :
								$days .= 1;
							endif;
							if(date('w') == 3) :
								$days .= 2;
							endif;
							if(date('w') == 4) :
								$days .= 3;
							endif;
							if(date('w') == 5) :
								$days .= 4;
							endif;
							if(date('w') == 6) :
								$days .= 5;
							endif;
							?>
						<?php 
							$winCount = '';$participateCount = '';
							foreach($winner_counts as $cid=>$winnerCount) : 
								if($campaign['Campaign']['id'] == $cid) : 
									$winCount = $winnerCount;
								endif;
							endforeach; 


							if($campaign['Campaign']['campaign_type'] == 'M') :
								foreach($participant_counts as $key=>$val) : 
									if($campaign['Campaign']['id'] == $key) : 
										$participateCount = $val;
									endif;
								endforeach; 
							endif;
						?>
					<?php //if((date("Y-m-d") >= $campaign['Campaign']['draw_start_date']) && (date("Y-m-d") <= $campaign['Campaign']['draw_end_date']) && $winner_counts[$campaign['Campaign']['id']]<$campaign['Campaign']['max_winners']) :?>
						<?php if((date("Y-m-d") >= $campaign['Campaign']['draw_start_date']) && (date("Y-m-d") <= $campaign['Campaign']['draw_end_date']) && ($winCount < $campaign['Campaign']['max_winners'])) :?>
							<?php if(($campaign['Campaign']['draw_type'] == 'MD') && ($campaign['Campaign']['active']==1) && (strpos($campaign['Campaign']['week_days'],$days) !== false)) :?>
								<?php if($campaign['Campaign']['campaign_type'] == 'M'): ?>
									<?php if($participateCount!=''  || !empty($participateCount)) : ?>
										&nbsp;&nbsp;&nbsp;
										<a href="/admin/campaigns/manual_draw/<?php echo $campaign['Campaign']['id'] ?>">Draw</a>
									<?php endif; ?>
								<?php  elseif($campaign['Campaign']['campaign_type'] == 'A') :  ?>
									&nbsp;&nbsp;&nbsp;
									<a href="/admin/campaigns/manual_draw/<?php echo $campaign['Campaign']['id'] ?>">Draw</a>
								<?php endif; ?>
							<?php endif; ?>
					<?php endif; ?>
			        </td>
		    	</tr>
		    	<?php endforeach; ?>
		   </table>
		</div>
	</div>
	
	<?php if(!empty($campaigns)) : ?>
			<div class="paging">
				<table style="border:none;">
			    	<tr>
			        	<td style="border:none;">
							<?php echo $this->Paginator->first('<<', array('title'=>'first','class'=>'disabled pagination1'));?>
							<?php echo $this->Paginator->prev('<', array('title'=>'prev','class'=>'disabled pagination1'));?>
							<?php echo $this->Paginator->numbers(array('class'=>'pagination1'));?>
							<?php echo $this->Paginator->next('>', array('title'=>'next','class'=>'disabled pagination1'));?>
							<?php echo $this->Paginator->last('>>', array('title'=>'last','class'=>'disabled pagination1'));?>
						 </td>
				 		<td style="border:none;">
						<select name="page" id="selectpage" style="margin-top:-5px;">
		 	 				<?php for ($i = 1; $i <= $this->Paginator->counter(array('format' => '%pages%')); $i++):?>
		 	 	   			<option value="<?php echo $i;?>"<?php echo ($this->Paginator->current($params['page']) == $i) ? ' selected="selected"' : ''?>><?php echo $i ?></option>
		 	 	  			<?php endfor ?>
						</select>
						<script>
						$("#selectpage").change(function() {
							var url = window.location.href;
							var n = url.indexOf("/?");
							var extra = "";
							if(n>0){extra = url.substring(n,url.length);}
							window.location.href = '/admin/campaigns/update_campaigns/page:'+$(this).val()+extra;
						});
						</script>
				 		</td>
			      	</tr>
			  	</table>    
			</div>
		<?php endif; ?>
</div>
<script>
	function showCampaigns()
	{
		pid = document.getElementById("CampaignProgram").value;	
		cid = document.getElementById("CampaignClient").value;	
		id = document.getElementById('opt').value;
		typeid = document.getElementById('optype').value;
		$.ajax({
			url: '/admin/campaigns/update_campaigns/c='+cid+'/s='+id+'/p='+pid+'/t='+typeid,
			type: "GET",
			success: function (data) {
				$('#campaigns').html(data);
			}
		});
	}
</script>
