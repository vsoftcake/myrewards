<style type="text/css">

	.rounded-corners 
	{
		-moz-border-radius: 10px 10px 0px 0px; /* Firefox */
		-webkit-border-radius: 10px 10px 0px 0px; /* Safari, Chrome */
		border-radius: 10px 10px 0px 0px; /* CSS3 */
	}
	.tr 
	{
		height:30px;
	}
	.tr1 
	{
		height:10px;
	}
	.checkbox input
	{
		width:10px;
		border:none !important;
	}
	.checkbox
	{
		float:left;
		padding-right:20px;
	}
	#content {
	    border-bottom: 1px solid #CCCCCC;
	    border-left: 1px solid #CCCCCC;
	    border-right: 1px solid #CCCCCC;
	    margin-left: 20px !important;
	    padding-left: 15px !important;
	    width: 938px;
	    padding-right:0px !important;
	    margin-bottom:20px !important;
}
</style>
<script type="text/javascript">
	function selectAll(chkObj)
	{
		var multi=document.getElementById('ClientClient');
		if(chkObj.checked)
			for(i=0;i<multi.options.length;i++)
			multi.options[i].selected=true;
		else
			for(i=0;i<multi.options.length;i++)
			multi.options[i].selected=false;	
	}
	function enableDate(checkbox, name1, name2)
    {
		$('#'+name1 + 'Day').prop('disabled',  !checkbox.checked);
		$('#'+name1 + 'Month').prop('disabled',  !checkbox.checked);
		$('#'+name2).prop('disabled',  !checkbox.checked);
    }

	var val = 0;
	function checkClients(id)
	{  
		if (document.getElementById(id).checked==true)
		{
			document.getElementById('prodListSearch').style.display='block';
			val = val+1;
		}
		else
		{
			val = val-1;
			if(val==0)
				document.getElementById('prodListSearch').style.display='none';
		}
	}
	
	function showWinners(id)
	{
		$.ajax({
			url: '/admin/campaigns/view_winners/'+id,
			type: "GET",
			success: function (data) {
				$('#viewwinners').html(data);
			}
		});
	}
	
	function drawWinners(id)
	{
		$.ajax({
			url: '/admin/campaigns/draw/'+id,
			type: "GET",
			success: function (data) {
				$('#drawwinners').html(data);
			}
		});
	}
	
	function selectDays(National)
	{
    	var rows = document.getElementById('days').getElementsByTagName('input');
    	for ( var i = 0; i < rows.length; i++ ) 
    	{
	       if ( rows[i] && rows[i].type == 'checkbox' && rows[i] != National ) 
			{
				if(National.checked == true)
				{
			            rows[i].checked = true;
				}
				else
				{
				   rows[i].checked = false;
				}
	        }
	    }
    	return true;
	}
	
	var count=<?php if(empty($count[0][0]['count']) || $count[0][0]['count'] == '') : echo 0; else: echo $count[0][0]['count']; endif;?>;
	function showFields()
	{
		if(count<4)
		{
			var html = '<div id="custom'+count+'" style="float: left; background-color:#EFEFEF;border:1px solid #cccccc;height: 90px; padding: 10px; margin-top: 15px; margin-bottom: 15px; margin-right: 10px;width: 350px;">';
			html += '<table>';
			html += '<tr><td width="200"><p><strong>Field Name</strong></p></td><td><input type="text" id="name'+count+'" name="data[CustomFields]['+count+'][name]" value=""></td></tr>';
			html += '<tr><td><p><strong>Field Mandatory</strong></p></td><td>';
			html += '<select name="data[CustomFields]['+count+'][mandatory]" id="mandatory'+count+'">';
			html += '<option value="0">Not Mandatory</option><option value="1">Mandatory</option>';
			html += '</select></td></tr>';
			html += '</table></div>';
			count++;
			$( html ).insertAfter( "#custom" );
		}
		else
		{
			alert('Custom fields creation limit is over');
		}		
	}
	
	function toggleSpellchecker()
	{
		var val = navigator.userAgent.toLowerCase();
	  	if(val.indexOf("msie") < 0)
	  	{
	  		tinyMCE.execCommand ('mceFocus', false, 'CampaignDescription');
			tinyMCE.execCommand('mceSpellCheck',false,'CampaignDescription');
	
			tinyMCE.execCommand ('mceFocus', false, 'CampaignRules');
			tinyMCE.execCommand('mceSpellCheck',false,'CampaignRules');
	
			tinyMCE.execCommand ('mceFocus', false, 'CampaignParticipationMessage');
			tinyMCE.execCommand('mceSpellCheck',false,'CampaignParticipationMessage');
	
			tinyMCE.execCommand ('mceFocus', false, 'CampaignNotificationMessage');
			tinyMCE.execCommand('mceSpellCheck',false,'CampaignNotificationMessage');
		}
	}	
</script>
<?php echo $this->element('tinymce');?>
       <div style="width:949px;height:50px;">
           <div style=" height: 40px; float:left; width: 608px;padding-left:37px;">
	           <div style=" height: 40px;float:left;padding-left:10px;">
	                   <h2>Edit Competition</h2>
	           </div> 
			</div>
	       	<div class="actions" style=" height: 40px; float:left;  width: 300px;">
	       		<ul style="padding-top: 20px; padding-left: 82px;">
	            	<li style="display:inline;">
	            		<button onclick="parent.location='/admin/campaigns/list_campaign'" class="admin_button rounded_corners_button" style='width:175px;'>Back To Competitions</button>
	               	</li>
	            </ul>
	        </div>
	</div>
<form action="" method="post" enctype="multipart/form-data">
	<?php echo $this->Form->input('Campaign.id', array('type' => 'hidden')); ?>
		<div class="tabber" id="mytab1">
			<div class="tabbertab"  style="min-height:600px;overflow:hidden;">
				<h2>Basic Details</h2>
			    <div class="edit" style="padding-bottom:10px;">
			    <h4>Basic Details</h4>
			    	<div style="background-color:#EFEFEF;border:1px solid #cccccc;padding-left: 30px; padding-top: 20px;padding-bottom:20px;width:790px;min-height:475px;overflow:hidden">
						<table width="800" style="border:none !important;">
							<tr class="tr1">
								<td width="300" valign="top" style="font-size:13px;"><span style="color:red">* </span><strong>What is the title of your competition?</strong></td>
							</tr>
							<tr>
								<td valign="top"><?php echo $this->Form->input('Campaign.name',array('style'=>'width:450px;', 'div'=>false, 'label'=>false)); ?></td>
								<td><?php echo $this->Form->error('Campaign.name', array('required' => 'Competition Name required','required' => 'Competition Name required')); ?></td>
							</tr>
							<tr><td style="height:15px"></td></tr>
							<tr class="tr1">
						    	<td valign="top" style="font-size:13px;"><span style="color:red">* </span><strong>Describe your competition (visible to the participants)</strong></td>
						    </tr>
						    <tr>
						      	<td><?php echo $this->Form->input('Campaign.description', array('div'=>false, 'label'=>false)); ?></td>
					       	
					       	 
						        <td>  	
						      	<?php echo $this->Form->error('Campaign.description', array('required' => 'Competition description required'), array('required' => 'Competition description required')); ?></td>
					            </td>
					       	</tr>
					       	<tr><td style="height:15px"></td></tr>
					        <tr class="tr1">
						      	<td valign="top" style="white-space:nowrap;font-size:13px;"><strong>Enter terms & conditions, eligibility, participation or other rules (visible to the participants)</strong></td>
						      	</tr>
						    <tr>
						      	<td><?php echo $this->Form->input('Campaign.rules', array('div'=>false, 'label'=>false)); ?></td>
					       </tr>
					       <tr><td style="height:15px"></td></tr>
					       <tr class="tr1">
							  <td valign="top" style="font-size:13px;"><strong>Do you want to upload an image to go with the competition description?</strong></td>
							</tr>
						    <tr>
							  <td style="width:373px">
									<?php echo $this->Form->file('Image.file'); ?><br/>
									Remove <?php echo $this->Form->checkbox('ImageDelete.file'); ?>
									<br><br>
									<div>
										For logo/image layout left upload image with width 380px and height 500px.<br>
										For logo/image layout top upload image with width 780px and height 450px.						  	
							  		</div>
							  </td>
						  </tr>
						
						<tr><td style="height:15px"></td></tr>
						<tr class="tr1">
							<td style="font-size:13px;"><strong>Where do you want the image/logo to show in relation to the text copy?</strong></td>
							</tr>
						    <tr>
							<td>
							  	<?php $layout=array('Logo/Image on top','Logo/Image on left'); ?>
							  	<?php echo $this->Form->input('Campaign.layout',array('type'=>'select','options'=>$layout, 'div'=>false, 'label'=>false)); ?>
							</td>
				      		</tr>
				      		<tr><td style="height:15px"></td></tr>
						<tr class="tr1">
							<td valign="top" style="font-size:13px;"><strong>Do you want to upload participate button image?</strong></td>
						</tr>
						<tr>
							<td style="width:373px">
								<?php echo $this->Form->file('ParticipateImage.file'); ?><br/>
								Remove <?php echo $this->Form->checkbox('ParticipateImageDelete.file'); ?>
								<br><br>
								For better appearance upload image with width 140px and height 35px
							</td>
						</tr>
						</table>
						</div>
						<br>
						<table style="border:none;">
							<tr class="tr">
								<td style="width:360px;"></td>  
								<td><button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(1)">Next</button></td>
							</tr>
				        </table>
					
               </div>
	  	    </div>
	  	    
		<div class="tabbertab"  style="min-height:500px;overflow:hidden;">
			<h2>Eligibility</h2>
			<div class="edit" style="padding-bottom:10px;">
				<h4>Eligibility</h4>
					<div style="background-color:#EFEFEF;border:1px solid #cccccc;padding-left: 30px; padding-top: 20px;padding-bottom:20px;width:790px;overflow:hidden">
							<table style="border:none !important;width:530px">
								<tr>
									<td style="font-size:13px;">
										<strong>Who should participate in this competition?</strong>
									</td>
								</tr>
								<tr>
									<td>
										<table style="border:none !important">
											<tr>
												<td valign="top">
													<strong>Programs</strong><br><br>
													<?php echo $this->Form->input('Program.Program', array('style'=>'width:250px', 'multiple' => 'multiple', 'options'=>$programlist, 'div'=>false, 'label'=>false)); ?>

													<?php 
														$options = array('url' => 'update_program_clients','update' => 'ClientClient');
													echo $this->Ajax->observeField('ProgramProgram', $options);
													?>


												</td>
												<td>
													<span style="color:red">* </span><strong>Clients</strong><br><br>
													<?php echo $this->Form->input('Client.Client', array('style'=>'width:250px','multiple' => 'multiple', 'div'=>false, 'label'=>false)); ?>
														<br>
														<sub>Select program to load clients</sub>
														<br>
														<?php echo $this->Form->checkbox('done', array('onclick' => "selectAll(this)")); ?>&nbsp;
														<span style="color:red">Check to select <b>All Clients</b></span>
												</td>
												<td style="white-space:nowrap">
												      <?php echo $this->Form->error('Client.Client', array('required' => 'Clients required','required' => 'Clients required')); ?>

												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
							<br>
							<table style="border:none !important;width:530px">
								<tr>
									<td style="font-size:13px;">
										<strong>When should your competition start and end? (visible to the participants)</strong>
									</td>
								</tr>
								<tr>
									<td>
										<table style="border:none !important;width:100%">
											<tr>
												<td><span style="color:red">* </span><strong>Competition starts from</strong></td>
											</tr>
											<tr>
												<td>
													<?php echo $this->Form->input('Campaign.start_date', array('div'=>false, 'label'=>false, 'type'=>'date', 'dateFormat'=>'DMY', 'style'=>'width:auto')); ?>
												</td>
											</tr>
											<tr>
											    <td>
											       <?php echo $this->Form->error('Campaign.start_date', array('required' => 'Start date required','todaystartdate'=> 'Start date should be not be less than today date')); ?>
											    </td>
											</tr> 

										</table>
									</td>
								</tr>
								<tr>
									<td>
										<table style="border:none !important">
											<tr>
												<td><span style="color:red">* </span><strong>Competition ends on</strong></td>
											</tr>
											<tr>
												<td>
												<?php echo $this->Form->input('Campaign.end_date', array('div'=>false, 'label'=>false, 'type'=>'date', 'dateFormat'=>'DMY', 'style'=>'width:auto')); ?>
												</td>
												<td></td>
											</tr>
											<tr>
											    <td>
											       <?php //echo $this->Form->error('Campaign.end_date', array('required' => 'End date required','enddatecheck' => 'End date should not be empty <br> End date should not less than start date')); ?>
											    </td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
							<br>
							<table style="border:none !important;width:530px">
								<tr>
									<td style="font-size:13px;">
										<strong>For what dashboards do you want this competition to appear</strong>
									</td>
								</tr>
								<tr>
									<td>
										<?php
											echo $this->Form->input('Dashboard.Dashboard',array('type'=>'select','multiple'=>'checkbox','label'=>false,'options'=>array('1' => 'My Rewards','2' => 'Send A Friend','3'=>'My Points'), 'div'=>false, 'label'=>false));
										?>
									</td>
								</tr>
																						
							</table>
							
					</div>  
					<br>
					<table style="border:none;">
						<tr class="tr">
							<td style="width:320px;"></td>
							<td><button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(0)">Previous</button></td>

							<td><button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(2)">Next</button></td>
						</tr>
				</table>

		</div>
		</div>
         
		<div class="tabbertab"  style="min-height:600px;overflow:hidden;">
			<h2>Participants</h2>
			<div class="edit" style="padding-bottom:10px;">
				<h4>Participants</h4>
					<div style="background-color:#EFEFEF;border:1px solid #cccccc;padding-left: 30px; padding-top: 20px;padding-bottom:20px;width:790px;overflow:hidden">
					<table style="width:700px;border:none !important;">
								<tr class="tr" style="font-size:13px">
									<td style="font-size:13px;"><span style="color:red">* </span><strong>How do you want the users to participate in this competition?</strong></td>
								</tr>
								<tr>
									<td>
								       <?php echo $this->Form->input('Campaign.campaign_type', array('type' => 'radio', 'options' => array('M'=>' Users should explicitly register on the competition page', 'A'=>' Let the system choose the users as per the eligibility criteria'),'class'=>'input_radio','separator'=>'<br><br>','legend'=>false, 'div'=>false, 'label'=>false));?>
								    </td>
								    <td>
								       <?php echo $this->Form->error('Campaign.campaign_type', array('required' => 'Competition type required'), array('required' => 'Competition type required')); ?>
								    </td>
								</tr>
								<tr class="tr1"><td></td></tr>
								<?php 
									$cStyle="none"; 
									$dstart = $this->data['Campaign']['campaign_type'];
									if($dstart=='M'):
										 $cStyle="inline";
									else :
										$cStyle="none";
									endif;
									
									$mdStyle="none"; 
									$md_type = $this->data['Campaign']['campaign_type'];
									if($dstart=='M'):
										 $mdStyle="inline";
									else :
										$mdStyle="none";
									endif;
								?>
									
								<tr class="tr1"><td>
									<script>
									$( "#CampaignCampaignTypeM" ).change(function() {
										if($(this).val()=='M')
										{
											$('#mds').show();
											$('#mde').show();
											$('#userType').hide();
											$('#loginPeriod').hide();
											$('#manualdrawtype').show();
										}
									});
									$( "#CampaignCampaignTypeA" ).change(function() {
										if($(this).val()=='A')
										{
											$('#mds').hide();
											$('#mde').hide();
											$('#userType').show();
											$('#loginPeriod').hide();
											$('#manualdrawtype').hide();
										}
									});
									</script></td></tr>
								<tr>
									<td>
										<div id="manualdrawtype" style="display:<?php echo $mdStyle;?>;border:none;">
											<div style="font-size:13px;"><strong>Choose winner type</strong></div><br>
											<?php echo $this->Form->input('Campaign.manual_winner_type', array('type' => 'radio', 'options' => array('default'=>'Default', 'choose'=>'Choose Winners'),'class'=>'input_radio','separator'=>'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;','legend'=>false, 'div'=>false, 'label'=>false));?>
											<br>
											<?php echo $this->Form->error('Campaign.manual_winner_type', array('required' => 'Please choose winner selection type')); ?>
										</div>
									</td>
								</tr>
								<?php 
									$drawType="false"; 
									$maxwinnersDisable="false"; 
									$perDrawDisable="false";
									$drawStartDateDisable = "false";
									$drawEndDateDisable = "false";
									$daysDisable = "inline";
									
									$type = $this->data['Campaign']['campaign_type'];
									$winner_type = $this->data['Campaign']['manual_winner_type'];
									if($type=='M' && $winner_type == 'choose'):
										 $drawType="true";
										 $maxwinnersDisable ="true";
										 $perDrawDisable = "true";
										 $drawStartDateDisable = "true";
										 $drawEndDateDisable = "true";
										 $daysDisable = "none";
									else :
										$drawType="false";
										$maxwinnersDisable ="false";
										$perDrawDisable = "false";
										$drawStartDateDisable = "false";
										$drawEndDateDisable = "false";
										$daysDisable = "inline";
									endif;
								?>
								<tr class="tr1"><td>
								<script>
								$( "#CampaignManualWinnerTypeChoose" ).change(function() {
							     	if($(this).val()=='choose')
									{
							      		$('#CampaignDrawTypeMD').prop("disabled",true);
							      		$('#CampaignDrawTypeAD').prop("disabled",true);
							      		$('#CampaignMaxWinners').prop("disabled",true);
							      		$('#CampaignWinnersPerDraw').prop("disabled",true);
							      		$('#CampaignDrawStartDateDay').prop("disabled",true);
							      		$('#CampaignDrawStartDateMonth').prop("disabled",true);
							      		$('#CampaignDrawStartDateYear').prop("disabled",true);
							      		$('#CampaignDrawEndDateDay').prop("disabled",true);
							      		$('#CampaignDrawEndDateMonth').prop("disabled",true);
							      		$('#CampaignDrawEndDateYear').prop("disabled",true);
							      		$('#daysDisable').hide();
							      	}
								});
								$( "#CampaignManualWinnerTypeDefault" ).change(function() {
							     	if($(this).val()=='default')
									{
							      		$('#CampaignDrawTypeMD').prop("disabled",false);
							      		$('#CampaignDrawTypeAD').prop("disabled",false);
							      		$('#CampaignMaxWinners').prop("disabled",false);
							      		$('#CampaignWinnersPerDraw').prop("disabled",false);
							      		$('#CampaignDrawStartDateDay').prop("disabled",false);
							      		$('#CampaignDrawStartDateMonth').prop("disabled",false);
							      		$('#CampaignDrawStartDateYear').prop("disabled",false);
							      		$('#CampaignDrawEndDateDay').prop("disabled",false);
							      		$('#CampaignDrawEndDateMonth').prop("disabled",false);
							      		$('#CampaignDrawEndDateYear').prop("disabled",false);
							      		$('#daysDisable').show();
							      	}
								});
								</script></td></tr>
								<tr>
								   <td style="font-size:13px;">
									<div id="mds" style="display:<?php echo $cStyle;?>">
									<strong>When can users start participating in this competition?</strong><br><br>
									 <span style="color:red">* </span><strong>Start date</strong>
									    &nbsp;&nbsp;
									  <?php echo $this->Form->input('Campaign.entry_start_date', array('div'=>false, 'label'=>false, 'type'=>'date', 'dateFormat'=>'DMY', 'style'=>'width:auto')); ?>
										<br>
								</div>
						   </td>
						</tr>
						<tr class="tr1"><td></td></tr>
						<tr style="font-size:13px">
						   <td>
							       <div id="mde" style="display:<?php echo $cStyle;?>">
									<span style="color:red">* </span><strong>End date</strong>
									   &nbsp;&nbsp;&nbsp;
									  <?php echo $this->Form->input('Campaign.entry_end_date', array('div'=>false, 'label'=>false, 'type'=>'date', 'dateFormat'=>'DMY', 'style'=>'width:auto')); ?>
										<br>
							      <br><div>*Participation is disabled after this day or when the total participants limit is reached, whichever comes first.</div>
						   		</div>
						   </td>
						</tr>  
								<tr class="tr1"><td></td></tr>
							</table>
							
						<?php 
							$lStyle="none"; 
							$lstart = $this->data['Campaign']['campaign_type'];
							if($lstart=='A'):
								 $lStyle="inline";
							else :
								$lStyle="none";
							endif;
						?>
						<div id="userType" style="display:<?php echo $lStyle;?>;border:none;">
						    <div style="padding-left:5px;"><span style="color:red">* </span><strong>Which users should participate the competition</strong></div>
						    <table style="border:none;">
							<tr class="tr1">
								<td>
									<?php echo $this->Form->input('Campaign.user_type', array('type' => 'radio', 'options' => array('LP'=>'Login period', 'FL'=>'First time login', 'AU'=>'All Users'),'class'=>'input_radio','separator'=>'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;','legend'=>false, 'div'=>false, 'label'=>false));?>
									<br>
										<?php echo $this->Form->error('Campaign.user_type', array('required' => 'User participant type required')); ?>
								</td>
							</tr>
							<tr class="tr1"><td></td></tr>
						    </table>
						</div>
						<script>
						$( "#CampaignUserTypeLP" ).change(function() {
							if($(this).val()=='LP')
							{
								$('#loginPeriod').show();
							}
						});
						$( "#CampaignUserTypeFL" ).change(function() {
							if($(this).val()=='FL')
							{
								$('#loginPeriod').hide();
							}
						});
						$( "#CampaignUserTypeAU" ).change(function() {
							if($(this).val()=='AU')
							{
								$('loginPeriod').hide();
							}
						});
						</script>
						<?php 
							$loginPeriod="none"; 
			 				$campaign = $this->data['Campaign']['campaign_type'];
							$type = $this->data['Campaign']['user_type'];
							if($campaign=='A' && $type=='LP'):
								$loginPeriod="inline";
							else :
								$loginPeriod="none";
							endif;
						?>    
						<div id="loginPeriod" style="display:<?php echo $loginPeriod;?>;border:none;">
							<table style="border:none;">
								<tr style="font-size:13px" class="tr1">
									<td>
										<strong>Login start date</strong>
									<br><br>
										<?php echo $this->Form->input('Campaign.login_start_date', array('div'=>false, 'label'=>false, 'type'=>'date', 'dateFormat'=>'DMY', 'style'=>'width:auto')); ?>
										<br>
									</td>
								</tr>
								<tr class="tr1"><td></td></tr>
								<tr style="font-size:13px" class="tr1">
									<td>
										<strong>Login end date</strong>
										<br><br>
										<?php echo $this->Form->input('Campaign.login_end_date', array('div'=>false, 'label'=>false, 'type'=>'date', 'dateFormat'=>'DMY', 'style'=>'width:auto')); ?>
										<br>
									</td>
								</tr>  
								<tr class="tr1"><td></td></tr>
							</table>
							</div>
							
								<table style="width:700px;border:none !important;">
									<tr class="tr">
									  <td valign="top" style="font-size:13px;">
										<strong>Do you want to limit the total number of participant entries? If so, what's the limit?</strong><br><br>
										<?php echo $this->Form->input('Campaign.participants_limit',array('style'=>'width:100px !important', 'div'=>false, 'label'=>false)); ?><br><br>
									  </td>
									</tr>
							    </table>
							   <br>
						<div>
          					<span style="color:red;font-size:13px;">* </span><strong>Participation confirmation message</strong><br>
							<br><?php echo $this->Form->input('Campaign.participation_message', array('div'=>false, 'label'=>false)); ?>
						</div>  
						    

				</div>  
				<br>     
				<table style="border:none;">
						<tr class="tr">
							<td style="width:320px;"></td>
							<td><button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(1)">Previous</button></td>

							<td><button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(3)">Next</button></td>
						</tr>
				</table>

		   </div>		       
		</div>
	  	   
		
		<div class="tabbertab"  style="min-height:300px;overflow:hidden;">
			<h2>Registration</h2>
			<h4>Registration</h4>
			<div>
				Select the fields that you want to display in the participation form to collect the details of the participant.
				<br>Select the checkbox under mandatory to make the field mandatory
			</div>
			<br>
			<div>
				<table style="width:600px;text-align:center;margin-left:100px;border:none !important;">   
					
					<tr class="tr">
						<td style="text-align:left" width="200"><strong>Form Field</strong></td>
						<td style="text-align:left"><strong>Mandatory</strong></td>
					</tr>
					<tr class="tr">
					      <td style="text-align:left"><?php echo $this->Form->checkbox('Campaign.cname', array( 'disabled' => true ,'checked'=>true)); ?>&nbsp;&nbsp;&nbsp;Name</td>
					      <td><?php echo $this->Form->checkbox('Campaign.cname_mandatory', array( 'disabled' => true ,'checked'=>true)); ?></td>
					</tr>
					<tr class="tr">
					      <td style="text-align:left"><?php echo $this->Form->checkbox('Campaign.cemail', array( 'disabled' => true ,'checked'=>true)); ?>&nbsp;&nbsp;&nbsp;E-mail</td>
					      <td><?php echo $this->Form->checkbox('Campaign.cemail_mandatory', array( 'disabled' => true ,'checked'=>true)); ?></td>
					</tr>
					<tr class="tr">
					     <td style="text-align:left"><?php echo $this->Form->checkbox('Campaign.address1'); ?>&nbsp;&nbsp;&nbsp;Address Line1</td>
					     <td><?php echo $this->Form->checkbox('Campaign.address1_mandatory'); ?></td>
					</tr>
					<tr class="tr">
					     <td style="text-align:left"><?php echo $this->Form->checkbox('Campaign.suburb'); ?>&nbsp;&nbsp;&nbsp;Suburb</td>
					     <td><?php echo $this->Form->checkbox('Campaign.suburb_mandatory'); ?></td>
					</tr>
					 <tr class="tr">
					      <td style="text-align:left"><?php echo $this->Form->checkbox('Campaign.region'); ?>&nbsp;&nbsp;&nbsp;Region</td>
					     <td><?php echo $this->Form->checkbox('Campaign.region_mandatory'); ?></td>
					</tr>
					 <tr class="tr">
					     <td style="text-align:left"><?php echo $this->Form->checkbox('Campaign.country'); ?>&nbsp;&nbsp;&nbsp;Country</td>
					     <td><?php echo $this->Form->checkbox('Campaign.country_mandatory'); ?></td>
					</tr>
					<tr class="tr">
					     <td style="text-align:left"><?php echo $this->Form->checkbox('Campaign.phone'); ?>&nbsp;&nbsp;&nbsp;Phone Number</td>
					     <td><?php echo $this->Form->checkbox('Campaign.phone_mandatory'); ?></td>
					</tr> 
					 <tr class="tr">
					     <td style="text-align:left"><?php echo $this->Form->checkbox('Campaign.address2'); ?>&nbsp;&nbsp;&nbsp;Address Line2</td>
					     <td><?php echo $this->Form->checkbox('Campaign.address2_mandatory'); ?></td>
					</tr>  
					<tr class="tr">
					      <td style="text-align:left"><?php echo $this->Form->checkbox('Campaign.city'); ?>&nbsp;&nbsp;&nbsp;City</td>
					     <td><?php echo $this->Form->checkbox('Campaign.city_mandatory'); ?></td>
					</tr>
					<tr class="tr">
					     <td style="text-align:left"><?php echo $this->Form->checkbox('Campaign.state'); ?>&nbsp;&nbsp;&nbsp;State</td>
					     <td><?php echo $this->Form->checkbox('Campaign.state_mandatory'); ?></td>
					</tr>  
					<tr class="tr">
					      <td style="text-align:left"><?php echo $this->Form->checkbox('Campaign.postcode'); ?>&nbsp;&nbsp;&nbsp;Postcode</td>
					      <td><?php echo $this->Form->checkbox('Campaign.postcode_mandatory'); ?></td>
					</tr>
					
				</table>

			   </div>
			  
							   
			<br>
			<div style="padding-top:10px"><strong>Create upto 4 custom fields to capture additional information of the participant if required.</strong></div>
			<?php if ($count[0][0]['count'] <4 ): ?>
					<div style="margin-top:10px;" ><button onclick="showFields()" type="button" class="admin_button">Add a Custom Field</button></div>
				<?php endif; ?>  
				<div>
				<?php 
				$c = 0;
				foreach($lists as $list) :?>
					<div style="float: left; background-color:#EFEFEF;border:1px solid #cccccc;height: 90px; padding: 10px; margin-top: 15px; margin-bottom: 15px; margin-right: 10px;width: 350px;">
						<table>
							<tr>
								<td style="width:200px">
									<p><strong>Field Name</strong></p>
								</td>
								<td><input type="hidden" name="data[CustomFields][<?php echo $c;?>][id]" value="<?php echo $list['CustomFields']['id'];?>">
								<input type="text" name="data[CustomFields][<?php echo $c;?>][name]" value="<?php echo $list['CustomFields']['name'];?>"></td>
							</tr>
							<tr>
								<td>
									<p><strong>Field Mandatory</strong></p>
								</td>
								<td><select name="data[CustomFields][<?php echo $c;?>][mandatory]">
								<option value="0" <?php echo $list['CustomFields']['mandatory']=='0'?'selected':'';?>>Not mandatory</option>
								<option value="1" <?php echo $list['CustomFields']['mandatory']=='1'?'selected':'';?>>Mandatory</option></select>
								</td>
							</tr>
						</table>	
				</div>
				<?php $c++;endforeach; ?>

				<div id="custom"></div>
				</div>
				 <br>
				    <table style="border:none;width:500px">
						<tr class="tr">
							<td style="width:320px;"></td>
							<td><button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(2)">Previous</button></td>

							<td><button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(4)">Next</button></td>
						</tr>
					</table>
          	</div>
          	
		<div class="tabbertab"  style="min-height:600px;overflow:hidden;">
			<h2>Draw</h2>
				<h4>Draw</h4>
			<div style="background-color:#EFEFEF;border:1px solid #cccccc;padding-left: 30px; padding-top: 20px;padding-bottom:20px;width:756px;overflow:hidden;">
				<div style="font-size:13px">
					<span style="color:red">* </span><strong>How do you want to draw winners for this competition?</strong>
					<br><br>
				       <?php echo $this->Form->input('Campaign.draw_type', array('type' => 'radio','options' => array('MD'=>'I would like to draw the winner(s) here manually', 'AD'=>'Let the system draw the winner(s)'),'class'=>'input_radio','separator'=>'<br><br>','legend'=>false, 'div'=>false, 'label'=>false));?>
				    <br>
					<?php echo $this->Form->error('Campaign.draw_type', array('required' => 'Draw type required'), array('required' => 'Draw type required')); ?>
				    </div>
				<br>
				<div style="font-size:13px">
					<span style="color:red">* </span><strong>How many total winners you would like to pick during this competition?</strong><br><br>
					<?php echo $this->Form->input('Campaign.max_winners',array('style'=>'width:100px !important','disabled' => $maxwinnersDisable, 'type'=>'text', 'div'=>false, 'label'=>false)); ?><br><br>
					<div style="white-space:nowrap;">
						<?php echo $this->Form->error('Campaign.max_winners', array('required' => 'Max winners required'), array('required' => 'Max winners required')); ?>
                    </div>
				</div>
				<div style="font-size:13px">
				   <span style="color:red">* </span><strong>How many winners you would like to pick for each draw?</strong><br><br>
					<?php echo $this->Form->input('Campaign.winners_per_draw',array('style'=>'width:100px !important','disabled'=>$perDrawDisable, 'div'=>false, 'label'=>false)); ?><br><br>
					<div style="white-space:nowrap;">
					<?php echo $this->Form->error('Campaign.winners_per_draw', array('required' => 'Winners per draw required'), array('required' => 'Winners per draw required')); ?>
				        </div>
				</div>
				<div style="font-size:13px">
					<span style="color:red">* </span><strong>When do you want to start drawing the winners?</strong>
					<br><br>
					<?php echo $this->Form->input('Campaign.draw_start_date', array('div'=>false, 'label'=>false, 'type'=>'date', 'dateFormat'=>'DMY', 'style'=>'width:auto', 'disabled'=>$drawStartDateDisable)); ?>
				</div>
				<br><br>
				<div style="font-size:13px">
					<span style="color:red">* </span><strong>When do you want to have a final drawing?</strong>
					<br><br>
					<?php echo $this->Form->input('Campaign.draw_end_date', array('div'=>false, 'label'=>false, 'type'=>'date', 'dateFormat'=>'DMY', 'style'=>'width:auto', 'disabled'=>$drawEndDateDisable)); ?>
				</div>
				<div>*Drawing will be disabled after this day or when the total winner limit is reached, whichever comes first.</div>
				<br><br>
				
				<div style="font-size:13px;display:<?php echo $daysDisable;?>;" id="daysDisable">
					<span style="color:red">* </span><strong>On what days you would like to draw the winner(s)?</strong>
					<br>
					<table style="border:none !important;" id="days">
								<tr class="tr">
									<td>
										<?php 
											if(!empty($days)) : 
												$selected1 = explode(',',$days); 
											endif;?>
										<?php echo $this->Form->input('Campaign.week_days', array('multiple' => 'checkbox','selected'=>$selected1, 'div'=>false, 'label'=>false, 
									     'options' => array('6' => 'Sunday',
												'0' => 'Monday',
												'1' => 'Tuesday',
												'2' => 'Wednesday',
												'3' => 'Thursday',
												'4' => 'Friday',
												'5' => 'Saturday')));
										?>
									</td>
									<td><?php //echo $this->Form->checkbox('Campaign.all', array('onClick' => 'selectDays(this)')); ?></td>
									<td></td>
								</tr>
								<tr>
								    <td>

								    <?php //echo $this->Form->error('Campaign.week_days', array('required' => 'Select on which day(s) you want to draw the winner(s)'), array('required' => 'Select on which day you want to draw the competition')); ?></td>
								    </td>
								</tr> 
							</table>
				</div>
		    </div>
		    <br>
		    <table style="border:none;">
						<tr class="tr">
							<td style="width:320px;"></td>
							<td><button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(3)">Previous</button></td>

							<td><button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(5)">Next</button></td>
						</tr>
			</table>

		</div>

		<div class="tabbertab"  style="min-height:500px;overflow:hidden;">
		<h2>Winner Notification</h2>
		<h4>Winner Notification</h4>   
		<div style="background-color:#EFEFEF;border:1px solid #cccccc;padding:10px 10px 10px 15px;width:756px;overflow:hidden;"> 
			<table>
				<tr class="tr"><td colspan="2" style="font-size:13px;"><span style="color:red">* </span><strong>How do you want to notify after you pick a winner?</strong></td></tr>
				<tr class="tr">
					<td><?php echo $this->Form->checkbox('Campaign.notification_popup'); ?>&nbsp;&nbsp;Popup</td>
					<td><?php echo $this->Form->checkbox('Campaign.notification_email'); ?>&nbsp;&nbsp;Email</td>
					<td> <?php //echo $this->Form->error('Campaign.notification_popup', array('required' => 'Winner alert type required')); ?> </td> 
				</tr>

				<tr class="tr"><td colspan="3" style="font-size:13px;"><span style="color:red">* </span><strong>How do you want to compose the notification message?</strong></td></tr>
				<tr class="tr">
					<td colspan="3">
						<?php 
							$fStyle="none";$dStyle="none";
							$msg = $this->data['Campaign']['winner_message'];
							if($msg == ''):
								$fStyle = "none";
								$dStyle = "none";
							elseif($msg=='F'):
								$fStyle = "block";
								$dStyle = "none";
							elseif($msg=='D'):
								$fStyle = "none";
								$dStyle = "block";
							endif;
						?>
						<script>
							function showMessageType()
							{
								if(document.getElementById('CampaignWinnerMessageF').checked==true)
								{
									document.getElementById('flyer').style.display="block";
									document.getElementById('des').style.display="none";
								}
								if(document.getElementById('CampaignWinnerMessageD').checked==true)
								{
									document.getElementById('flyer').style.display="none";
									document.getElementById('des').style.display="block";
								}
							}
						</script>

						<?php echo $this->Form->input('Campaign.winner_message', array('onclick'=>'showMessageType()', 'type' => 'radio', 'options' => array('F'=>'I want to upload a pre-designed image/html file here', 'D'=>'I want to format the message here'),'class'=>'input_radio','separator'=>'<br/>','legend'=>false, 'div'=>false, 'label'=>false));?>
						<div>	
							<?php //echo $this->Form->error('Campaign.winner_message', array('required' => 'Select how do u want to notify the winner message'), array('required' => 'Select how do u want to notify the winner message')); ?>
						</div>
						<div id="flyer" style="display:<?php echo $fStyle;?>">
							<br><span style="color:red">* </span><strong>Upload Logo/Image Notification Image</strong>
							<br>
							<?php echo $this->Form->file('FlyerNotificationImage.file'); ?><br/>
							Remove <?php echo $this->Form->checkbox('FlyerNotificationDelete.file'); ?>
						</div>
						<div id="des" style="display:<?php echo $dStyle;?>">
							<br/>
							<table>
								<tr>
									<td colspan="2">
										<span style="color:red">* </span><strong>Notification Message</strong>
									</td>
								</tr>
								<tr>
									<td>
										<?php echo $this->Form->input('Campaign.notification_message', array('div'=>false, 'label'=>false)); ?>
									</td>
									<td valign="top" style="padding-left:10px;">
										The following field replacements are available:<br/>
										[[client_name]] replaced by the name of the client<br/>
										[[member_name]] replaced by the first and last name of the user<br/>	
									</td>
								</tr>
							</table>								
						</div>
					</td>
				</tr>

				<tr class="tr"><td colspan="3"><span style="font-size:12px;font-weight:bold;">Subject for email</span></td></tr>
				<tr class="tr">
					<td colspan="3">
						<?php echo $this->Form->input('Campaign.subject',array('default'=>'Winner Confirmation','style'=>'width:450px', 'div'=>false, 'label'=>false)); ?>
					</td>
				</tr>
			</table>
		</div>
		<br>
		
		<table style="border:none;">
			<tr class="tr">
			<td style="width:320px;"></td> 
			<td><button type="button" class="admin_button" onclick="document.getElementById('mytab1').tabber.tabShow(3)">Previous</button></td> 
					<td><button type="submit" class="admin_button" onclick="toggleSpellchecker()">Submit</button></td>
		    </tr>
		</table>        
        	</div>
       
			
			</div>	 
	  	  </form>
  	     
