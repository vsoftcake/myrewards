
	<div style="float: right;padding-right:30px;padding-top: 20px;">
			<button onclick="parent.location='/campaigns/details/2/<?php echo $rules[0]['Campaign']['id']; ?>'" class="campaign_back_button_style rounded_corners_button" style="width:200px">Back To Competition View</button>
			<button onclick="parent.location='/campaigns/details'" class="campaign_back_button_style rounded_corners_button">Back To Competitions</button>
	</div>
	<div style="padding-top:40px;padding-bottom:30px;padding-left:20px;padding-right:20px;width:910px;border:1px solid #DFDFDF;">
		<h2>Competition Rules</h2>
		<?php echo $rules[0]['Campaign']['rules']; ?>
		
		<?php if($rules[0]['Campaign']['campaign_type']=='M') : ?>
			<?php if(empty($existing_users)) : ?>
				<?php if((date("Y-m-d") >= $rules[0]['Campaign']['entry_start_date']) && (date("Y-m-d") <= $rules[0]['Campaign']['entry_end_date']) && (($count[0][0]['count']< $rules[0]['Campaign']['participants_limit']) || ($rules[0]['Campaign']['participants_limit']==''))) :?>
				<div align="center">
						<?php if($rules[0]['Campaign']['participate_button_image_ext']=='') : ?>
								<button onclick="parent.location='/campaigns/details/3/<?php echo $rules[0]['Campaign']['id'];?>'" class="campaign_button_style rounded_corners_button">Participate</button>
						<?php else : ?>
							<?php 
								$filepath = CAMPAIGN_IMAGE_PATH;
								$filename = $rules[0]['Campaign']['id']. '.'. $rules[0]['Campaign']['participate_button_image_ext'];
								$path = $this->Html->url(CAMPAIGN_IMAGE_WWW_PATH.'/participate_image/'. $rules[0]['Campaign']['id']. '.'. $rules[0]['Campaign']['participate_button_image_ext']); 
								$path_prod = substr($path,strpos($path)+1);
								$img=$this->ImageResize->getResizedDimensions($path_prod, 140, 35);
							?>
							<img style="padding-left:20px;padding-right:20px;padding-top:5px" width="<?php echo $img['width'];?>" height="<?php echo $img['height'];?>" src="<?php echo $this->Html->url('/'. $path_prod);?>"/>
						
						<?php endif; ?>
				</div>
				<?php endif; ?>
			<?php endif; ?>
		<?php endif; ?>
	</div>