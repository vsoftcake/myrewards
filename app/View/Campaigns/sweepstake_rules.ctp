	<div style="float: right;padding-right:30px;padding-top: 20px;">
		<a href="/campaigns/details/2/<?php echo $rules[0]['Campaign']['id']; ?>">
			<button class="campaign_back_button_style rounded_corners_button" style="width:200px">Back To Competition View</button>
		</a>
		<a href="/campaigns/details">
			<button class="campaign_back_button_style rounded_corners_button">Back To Competitions</button>
		</a>
	</div>
	<div style="padding-top:40px;padding-bottom:30px;padding-left:20px;padding-right:20px;width:910px;border:1px solid #DFDFDF;">
		<h2>Competition Rules</h2>
		<?php echo $rules[0]['Campaign']['rules']; ?>
		
		<?php if($rules[0]['Campaign']['campaign_type']=='M') : ?>
			<?php if(empty($existing_users)) : ?>
				<?php if((date("Y-m-d") >= $rules[0]['Campaign']['entry_start_date']) && (date("Y-m-d") <= $rules[0]['Campaign']['entry_end_date']) && (($count[0][0]['count']< $rules[0]['Campaign']['participants_limit']) || ($rules[0]['Campaign']['participants_limit']==''))) :?>
				<div align="center">
					<a class="link_icons" style="text_decoration:none" href="<?php echo $this->Html->url('/campaigns/details/3/'.$rules[0]['Campaign']['id']); ?>">
						<button class="campaign_button_style rounded_corners_button">Participate</button>
					</a>
				</div>
				<?php endif; ?>
			<?php endif; ?>
		<?php endif; ?>
	</div>