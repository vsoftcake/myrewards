<script>
	function notEmpty()
  	{
		var address1 = document.forms["myForm"]["data[CampaignEntry][address1]"];
	    	var address2 = document.forms["myForm"]["data[CampaignEntry][address2]"];
	    	var suburb = document.forms["myForm"]["data[CampaignEntry][suburb]"];
	    	var city = document.forms["myForm"]["data[CampaignEntry][city]"];
	    	var region = document.forms["myForm"]["data[CampaignEntry][region]"];
	    	var pincode = document.forms["myForm"]["data[CampaignEntry][pincode]"];
	    	var country=document.forms["myForm"]["data[CampaignEntry][country]"];
	    	var state=document.forms["myForm"]["data[CampaignEntry][state]"];
	   	var email=document.forms["myForm"]["data[CampaignEntry][email]"].value;
	   	var phone = document.forms["myForm"]["data[CampaignEntry][phone]"];

  
		var atpos=email.indexOf("@");
		var dotpos=email.lastIndexOf(".");
		if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length)
	  	{
  			alert("Please enter a valid email");
  			return false;
  		}
       
  	     	if (address1 != null)
        	{
         		var address1_mandatory = <?php echo $dlist[0]['Campaign']['address1_mandatory'];?>;
         
            		if(address1_mandatory ==1)
             		{
              			var x=document.forms["myForm"]["data[CampaignEntry][address1]"].value;
                		if (x==null || x=="")
                  		{
                    			alert("Please enter address line1");
                    			return false;
                  		}
             		} 
        	}
  
	       	if (address2 != null)
	       	{ 
			var address2_mandatory = <?php echo $dlist[0]['Campaign']['address2_mandatory'];?>;
		    	if(address2_mandatory ==1)
		     	{ 
		       		var x=document.forms["myForm"]["data[CampaignEntry][address2]"].value;
			 	if (x==null || x=="")
			  	{
			    		alert("Please enter address line2");
			    		return false;
			  	}
		     	}    
	       	}
	       	
       		if (suburb != null)
        	{
         		var suburb_mandatory = <?php echo $dlist[0]['Campaign']['suburb_mandatory'];?>; 
            		if(suburb_mandatory ==1)
             		{
              			var x=document.forms["myForm"]["data[CampaignEntry][suburb]"].value;
                		if (x==null || x=="")
                  		{
                    			alert("Please enter suburb");
                    			return false;   
                  		}
             		} 
        	}
        	
         	if (city != null)
        	{
         		var city_mandatory = <?php echo $dlist[0]['Campaign']['city_mandatory'];?>; 
            		if(city_mandatory ==1)
             		{
              			var x=document.forms["myForm"]["data[CampaignEntry][city]"].value;
                		if (x==null || x=="")
                  		{
                    			alert("Please enter city");
                    			return false;
                  		}
             		} 
        	}
        
         	if (region != null)
        	{ 
         		var region_mandatory = <?php echo $dlist[0]['Campaign']['region_mandatory'];?>;
            		if(region_mandatory ==1)
             		{
              			var x=document.forms["myForm"]["data[CampaignEntry][region]"].value;
                		if (x==null || x=="")
                  		{
                    			alert("Please enter region");
                    			return false;
                  		}
             		} 
        	}
        
         	if (pincode != null)
        	{
         		var postcode_mandatory = <?php echo $dlist[0]['Campaign']['postcode_mandatory'];?>; 
            		if(postcode_mandatory ==1)
             		{
              			var x=document.forms["myForm"]["data[CampaignEntry][pincode]"].value;
                		if (x==null || x=="")
                  		{
                    			alert("Please enter postcode");
                    			return false;
                  		}
             		} 
        	}
       
       		if (country != null)
       		{
       			var country_mandatory = <?php echo $dlist[0]['Campaign']['country_mandatory'];?>;
         		if(country_mandatory ==1)
            		{
            			var x=document.forms["myForm"]["data[CampaignEntry][country]"].value;
                		if(x=="" || x==null)
                		{
	                		alert("Please select country");
	                		return false;
                		}
             		} 
        	}
        
        	if (state != null)
       		{
       			var state_mandatory = <?php echo $dlist[0]['Campaign']['state_mandatory'];?>;
         		if(state_mandatory ==1)
            		{
				var x=document.forms["myForm"]["data[CampaignEntry][state]"].value;
				if(x=="" || x==null)
				{
					alert("Please select state");
					return false;
				}
			} 
		}
        
         	if (phone != null)
	        {
	        	var phone_mandatory = <?php echo $dlist[0]['Campaign']['phone_mandatory'];?>; 
	            	if(phone_mandatory ==1)
	             	{
	              		var x=document.forms["myForm"]["data[CampaignEntry][phone]"].value;
	                	if (x==null || x=="")
			  	{
					alert("Please enter phone");
				    	return false;
			  	}
	             	} 
        	}
        
        	<?php foreach($lists as $list) :?>
	       		if(<?php echo $list['CustomFields']['mandatory'];?> == 1 && document.forms["myForm"]["data[CfContents][<?php echo $list['CustomFields']['id'];?>]"].value == "")
	       		{
	       			alert("Please enter <?php echo strtolower($list['CustomFields']['name']);?>");
	       			return false;
	        	}
        	<?php endforeach; ?>
      	
      	return true;
	}
</script>

<?php $this->Paginator->options(array('url' => array_merge($this->params['pass'], $this->params['named']))); ?>
	<?php if ($step == 2) : ?>
		<div style="padding:20px;width:910px;border:1px solid #DFDFDF;">
			<div style="float:right">
				<button onclick="parent.location='/campaigns/details'" class="campaign_back_button_style rounded_corners_button" style='width:175px'>Back To Competitions</button>
			</div>
				<br><br>
			<table width="100%">
				<tr>
					<td style="font-size:22px;font-weight:bold;text-align:left;">
						<?php echo $details[0]['Campaign']['name'];?>
					</td>
					<?php if((date("Y-m-d") >= $details[0]['Campaign']['entry_start_date']) && (date("Y-m-d") <= $details[0]['Campaign']['entry_end_date']) && ($count[0][0]['count']< $details[0]['Campaign']['participants_limit'])) :?>
					<td style="font-size:16px;font-weight:bold;text-align:right;">
						<?php
							$edate = explode('-',$details[0]['Campaign']['end_date']);
							$cedate = $edate[2]."-".$edate[1]."-".$edate[0];
						?>
						Competition ends on : <?php echo $cedate; ?>
					</td>
					<?php endif;?>
				</tr>
			</table>
			<br><br>
			<table width="100%">
				<?php if($details[0]['Campaign']['layout']==0) : ?>
				<tr>
					<td valign="top">
						<?php 
							$filepath = CAMPAIGN_IMAGE_PATH;
							$filename = $details[0]['Campaign']['id']. '.'. $details[0]['Campaign']['campaign_logo_extension'];
							$path = $this->Html->url(CAMPAIGN_IMAGE_WWW_PATH.'/image/'. $details[0]['Campaign']['id']. '.'. $details[0]['Campaign']['campaign_logo_extension']); 
							$path_prod = substr($path,strpos($path)+1);
							$img=$this->ImageResize->getResizedDimensions($path_prod, 780, 450);
						?>
						<img style="padding-left:20px;padding-right:20px" width="<?php echo $img['width'];?>" height="<?php echo $img['height'];?>" src="<?php echo $this->Html->url('/'. $path_prod);?>"/>
					</td>
				</tr>
				<tr>
					<td valign="top">
						<table>
				<?php else : ?>
				<tr>
					<td valign="top">
						<?php 
							$filepath = CAMPAIGN_IMAGE_PATH;
							$filename = $details[0]['Campaign']['id']. '.'. $details[0]['Campaign']['campaign_logo_extension'];
							$path = $this->Html->url(CAMPAIGN_IMAGE_WWW_PATH.'/image/'. $details[0]['Campaign']['id']. '.'. $details[0]['Campaign']['campaign_logo_extension']); 
							$path_prod = substr($path,strpos($path)+1);
							$img=$this->ImageResize->getResizedDimensions($path_prod, 380, 500);
						?>
						<img style="padding-left:20px;padding-right:20px" width="<?php echo $img['width'];?>" height="<?php echo $img['height'];?>" src="<?php echo $this->Html->url('/'. $path_prod);?>"/>
					</td>
					<td valign="top">
						<table>
				<?php endif; ?>
							<tr>
								<td valign="top">
									<?php echo $details[0]['Campaign']['description'];?>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<br><br>
			<table align="center">
				<tr>
					<?php if(!empty($details[0]['Campaign']['rules'])) : ?>
					<td style="width:200px">
						<button onclick="parent.location='/campaigns/campaign_rules/<?php echo $details[0]['Campaign']['id']; ?>'" style="height:29px !important;" class="campaign_back_button_style rounded_corners_button">Competition Rules</button>
					</td>
					<?php endif; ?>
					
					<?php if($details[0]['Campaign']['campaign_type']=='M') : ?>
						<?php if(empty($existing_users)) : ?>
							<?php if((date("Y-m-d") >= $details[0]['Campaign']['entry_start_date']) && (date("Y-m-d") <= $details[0]['Campaign']['entry_end_date']) && (($count[0][0]['count']< $details[0]['Campaign']['participants_limit']) || ($details[0]['Campaign']['participants_limit']==''))) :?>
							<td>
								<a class="link_icons" style="text_decoration:none" href="<?php echo $this->Html->url('/campaigns/details/3/'.$details[0]['Campaign']['id']); ?>">
									<?php if($details[0]['Campaign']['participate_button_image_ext']=='') : ?>
											<button onclick="parent.location='/campaigns/details/3/<?php echo $details[0]['Campaign']['id'];?>'" class="campaign_button_style rounded_corners_button">Participate</button>
									<?php else : ?>
										<?php 
											$filepath = CAMPAIGN_IMAGE_PATH;
											$filename = $details[0]['Campaign']['id']. '.'. $details[0]['Campaign']['participate_button_image_ext'];
											$path = $this->Html->url(CAMPAIGN_IMAGE_WWW_PATH.'/participate_image/'. $details[0]['Campaign']['id']. '.'. $details[0]['Campaign']['participate_button_image_ext']); 
											$path_prod = substr($path,strpos($path)+1);
											$img=$this->ImageResize->getResizedDimensions($path_prod, 140, 35);
										?>
										<img style="padding-left:20px;padding-right:20px;padding-top:5px" width="<?php echo $img['width'];?>" height="<?php echo $img['height'];?>" src="<?php echo $this->Html->url('/'. $path_prod);?>"/>

									<?php endif; ?>
							</td>
							<?php endif; ?>
						<?php endif; ?>
					<?php endif; ?>
				</tr>
			</table>
		</div>
	<?php elseif ($step == 3) : ?>
	<div style="padding:20px;width:910px;border:1px solid #DFDFDF;">
		<div style="width:880px">
			<div style="float:left;padding-left: 100px;">
				<h3 style="padding-left:22px;">
					<!--Fill in the form below and be automatically entered for the chance to win: <br>-->
					Required Information.
				</h3>
				<h4 style="padding-left: 22px; font-weight: normal; font-size: 12px ! important;">
					All the fields marked as * are mandatory
				</h4>
			</div>
			<?php if($dlist[0]['Campaign']['campaign_type'] == 'M') :?>
			<div style="float:right;">
				<button onclick="parent.location='/campaigns/details/2/<?php echo $dlist[0]['Campaign']['id'];?>'" class="campaign_back_button_style rounded_corners_button" style="width:200px">Back To Competition View</button>
				<button onclick="parent.location='/campaigns/details/'" class="campaign_back_button_style rounded_corners_button" style='width:175px'>Back To Competitions</button>
			</div>
			<?php endif; ?>
		</div>
		<br><br><br>
		<form name="myForm" id="UsersFirstTimeLoginForm2" action="<?php echo $this->Html->url('/campaigns/details/4/'.$dlist[0]['Campaign']['id']); ?>" method="post" class="standard_first_time_login" onSubmit="return notEmpty();">
			<div class="edit" style="padding-left: 120px; padding-top: 20px;">
				<table style="border:1px solid #EEEEEE;width:635px;padding:10px;">
					<tr>
						<td class="campaign_td" style="width: 200px; padding-left: 90px;">Your Name</td>
						<td class="campaign_td" style=""><?php echo $this->Session->read('user.User.first_name'); ?> <?php echo $this->Session->read('user.User.last_name'); ?></td>
					</tr>
					<tr><td style="height:5px" colspan="5"></td></tr>
					<tr>
						<td class="campaign_td" style="width: 200px; padding-left: 90px;">
							<span style="color:red">* </span>Your Email</td>
						<td class="campaign_td"><?php echo $this->Form->input('CampaignEntry.email', array('class'=>'campaign_input !important','style'=>'width:178px')); ?></td>
					</tr>
					<?php if($dlist[0]['Campaign']['address1']==1)  : ?>
						<tr><td style="height:5px" colspan="5"></td></tr>
						<tr>
							<td class="campaign_td" style="width: 200px; padding-left: 90px;">
							<?php if($dlist[0]['Campaign']['address1_mandatory']): ?>
								<span style="color:red">* </span>
							<?php endif; ?>Address Line1</td>
							<td class="campaign_td">
							   <textarea id="CampaignEntryAddress1" class="campaign_input !important" rows="2" cols="21" value='Form Checker' name="data[CampaignEntry][address1]"></textarea>
							</td>
						</tr>
					<?php endif; ?>		
					<?php if($dlist[0]['Campaign']['address2']==1)  : ?>
						<tr><td style="height:5px" colspan="5"></td></tr>
						<tr>
							<td class="campaign_td" style="width: 200px; padding-left: 90px;">
							<?php if($dlist[0]['Campaign']['address2_mandatory']): ?>
								<span style="color:red">* </span>
							<?php endif; ?>
							Address Line2</td>
							<td class="campaign_td">
								<?php echo $this->Form->input('CampaignEntry.address2',array('type'=>'textarea','rows' => '2', 'cols' => '21', 'class'=>'campaign_input !important')); ?>
							</td>
						</tr>
					<?php endif; ?>	
					<?php if($dlist[0]['Campaign']['suburb']==1) : ?>
					<tr><td style="height:5px" colspan="5"></td></tr>
							<tr>
								<td class="campaign_td" style="width: 200px; padding-left: 90px;">
								<?php if($dlist[0]['Campaign']['suburb_mandatory']): ?>
								<span style="color:red">* </span>
							<?php endif; ?>
								Suburb</td>
								<td class="campaign_td"><?php echo $this->Form->input('CampaignEntry.suburb', array('class'=>'campaign_input !important')); ?></td>
							<tr>
					<?php endif; ?>	
					<?php if($dlist[0]['Campaign']['city']==1) : ?>
							<tr><td style="height:5px" colspan="5"></td></tr>
							</tr>	
								<td class="campaign_td" style="width: 200px; padding-left: 90px;">
								<?php if($dlist[0]['Campaign']['city_mandatory']): ?>
								<span style="color:red">* </span>
							<?php endif; ?>
								City</td>
								<td class="campaign_td"><?php echo $this->Form->input('CampaignEntry.city', array('class'=>'campaign_input !important')); ?></td>
							</tr>
					<?php endif; ?>
					<?php if($dlist[0]['Campaign']['region']==1) : ?>			
							<tr><td style="height:5px" colspan="5"></td></tr>
							<tr>
								<td class="campaign_td" style="width: 200px; padding-left: 90px;">
								<?php if($dlist[0]['Campaign']['region_mandatory']): ?>
									<span style="color:red">* </span>
								<?php endif; ?>
								Region</td>
								<td class="campaign_td"><?php echo $this->Form->input('CampaignEntry.region', array('class'=>'campaign_input !important')); ?></td>
							</tr>
					<?php endif; ?>	
					<?php if($dlist[0]['Campaign']['postcode']==1) : ?>	
							<tr><td style="height:5px" colspan="5"></td></tr>
							<tr>	
								<td class="campaign_td" style="width: 200px; padding-left: 90px;">
								<?php if($dlist[0]['Campaign']['postcode_mandatory']): ?>
								<span style="color:red">* </span>
							<?php endif; ?>
								Postcode</td>
								<td class="campaign_td"><?php echo $this->Form->input('CampaignEntry.pincode', array('class'=>'campaign_input !important')); ?></td>
							</tr>
					<?php endif; ?>	
					<?php if($dlist[0]['Campaign']['country']==1) : ?>
						<tr><td style="height:5px" colspan="5"></td></tr>
					<tr>
						<td class="campaign_td" style="width: 200px; padding-left: 90px;">
						<?php if($dlist[0]['Campaign']['country_mandatory']): ?>
								<span style="color:red">* </span>
							<?php endif; ?>
						Country</td>
						<td class="campaign_td">
							<?php echo $this->Form->input('CampaignEntry.country', array('type'=>'select','options'=>$this->Session->read('client.Countries'), 'empty'=> 'Select Country', 'style'=>'width:180px', 'class'=>'campaign_input !important')); ?>
							<br>
							<?php echo $this->Form->error('CampaignEntry.country', array('required' => 'Please select country')); ?>
							<?php 
								$options = array('url' => '/campaigns/update_state','update' => 'CampaignEntryState');
					           	echo $this->Ajax->observeField('CampaignEntryCountry', $options);
				         	?>
						</td>
					</tr>
					<?php endif; ?>	
					<?php if($dlist[0]['Campaign']['state']==1) : ?>
						<tr><td style="height:5px" colspan="5"></td></tr>
						<tr>
							<td class="campaign_td" style="width: 200px; padding-left: 90px;">
							<?php if($dlist[0]['Campaign']['state_mandatory']): ?>
								<span style="color:red">* </span>
							<?php endif; ?>
							State</td>
							<td class="campaign_td">
								<?php echo $this->Form->input('CampaignEntry.state', array('type'=>'select','options'=>$states, 'empty'=> 'Select State', 'style'=>'width:180px', 'class'=>'campaign_input !important')); ?>
							</td>
						<td class="bl"></td>
						</tr>
					<?php endif; ?>	
					<?php if($dlist[0]['Campaign']['phone']==1) : ?>	
						<tr><td style="height:5px" colspan="5"></td></tr>
						<tr>	
							<td class="campaign_td" style="width: 200px; padding-left: 90px;">
							<?php if($dlist[0]['Campaign']['phone_mandatory']): ?>
							<span style="color:red">* </span>
						<?php endif; ?>
							phone</td>
							<td class="campaign_td"><?php echo $this->Form->input('CampaignEntry.phone', array('class'=>'campaign_input !important')); ?></td>
						</tr>
					<?php endif; ?>		
					
					<?php foreach($lists as $list) : 
						$size = $list['CustomFields']['length'];
						$mandatory = $list['CustomFields']['mandatory'];
					?>	
						<tr><td style="height:5px" colspan="5"></td></tr>
						<tr>
							<td class="campaign_td" style="width: 200px; padding-left: 90px;">
							<?php if($mandatory ==1): ?>
								<span style="color:red">* </span>
							<?php endif; ?>
							<?php echo ucwords($list['CustomFields']['name']);?></td>
							<td class="campaign_td">
								<input type="text" name="data[CfContents][<?php echo $list['CustomFields']['id'];?>]" class="campaign_input !important"/>						
						</td>
						<td class="bl"></td>
						</tr>
					<?php endforeach; ?>
					<tr><td style="height:15px" colspan="5"></td></tr>
					<tr>
						<td class="bb"></td>
						<td class="bb br">
						
							<?php echo $this->Form->submit('Submit', array('class'=>'button_search_module rounded_corners_button','style'=>'cursor:pointer', 'div'=>false ));?>
						<br/><br/>
						</td>
					</tr>
				</table>
			
				</div>
			</form>
		</div>
		<br/>
	<?php elseif($step==5) : ?>
		<?php if($type=='M') :?>
			<div style="float: right; width: 200px; padding-top: 10px;">
				<button onclick="parent.location='/campaigns/details'" class="campaign_back_button_style rounded_corners_button" style='width:175px'>Back To Competitions</button>
			</div>
		<?php endif; ?>
		<div style="padding:20px;width:910px;border:1px solid #DFDFDF;" align="center">
			
				<?php if($message!='' || !empty($message)) : ?>
					<h3><?php echo $message; ?></h3>
				<?php else : ?>
					<h3>You have succesfully participated!<br>
					Thanks for participating the competition.</h3>
				<?php endif; ?>
		</div>
	<?php elseif($step==6) : ?>
	<div style="float: right; width: 200px; padding-top: 10px;">
		<a href="">
			<button onclick="parent.location='/campaigns/details'" class="campaign_back_button_style rounded_corners_button" style='width:175px'>Back To Competitions</button>
		</a>
	</div>
	<div style="padding:20px;width:910px;border:1px solid #DFDFDF;" align="center">
		<h3>
			Sorry you have already paricipated the competition.
		</h3>
	</div>
	<?php else : ?>
		<?php if(!empty($details)) : ?>
			<div style="padding:20px;width:910px;border:1px solid #DFDFDF;">
				<div style="padding-left:20px">	
                    <h2 style="padding-left:20px">Welcome to My Rewards Competitions!</h2><br>

					<!--<form action="<?php echo $this->Html->url('/campaigns/details/1'); ?>" id="UsersFirstTimeLoginForm1" method="post" class="standard_first_time_login">-->
						<table style="width:900px">
							<tr>
								<td>
									<?php foreach($details as $detail) : ?>
										<div style="float:left;margin: 10px;  padding: 10px; width: 380px; height: 200px;border:3px solid #dfdfdf" class="rounded_corners_map">
										<table style="width:100%;height:100%;color:#808080;">
											<tr>
												<td width="140" valign="top" style="padding:5px;">
													<?php 
														$filepath = CAMPAIGN_IMAGE_PATH;
														$filename = $detail['Campaign']['id']. '.'. $detail['Campaign']['campaign_logo_extension'];
														$path = $this->Html->url(CAMPAIGN_IMAGE_WWW_PATH.'/image/'. $detail['Campaign']['id']. '.'. $detail['Campaign']['campaign_logo_extension']); 
														$path_prod = substr($path,strpos($path)+1);
														$img=$this->ImageResize->getResizedDimensions($path_prod, 130, 130);
													?>
													<?php //if (file_exists($filepath. $filename)) :?>
														<img width="<?php echo $img['width'];?>" height="<?php echo $img['height'];?>" src="<?php echo $this->Html->url('/'. $path_prod);?>" style="margin:5px;"/>
													<?php //endif; ?>
												</td>
												<td valign="top">
													<table>
														<tr>
															<td style="font-size:13px;height:30px;">
																<?php 
																	$name = substr(strip_tags($detail['Campaign']['name']),0,120);
																?>
																<b><?php echo $name;?></b>
															</td>
														</tr>
														<tr>
															<td style="font-size:12px;height:120px;">
																<?php 
																	$description = substr(strip_tags($detail['Campaign']['description']),0,220);
																?>
																	<?php echo $description;?>
																	<input type="hidden" name="data[Campaign][name1]" value="test" />
															</td>
														</tr>
														<tr>
															<td>
																<a class="link_icons" href="<?php echo $this->Html->url('/campaigns/details/2/'.$detail['Campaign']['id']); ?>">
																	<img src="/files/campaign/submit.jpg"/>		
																</a>	
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
										</div>
									<?php endforeach; ?>	
								</td>
							</tr>
						</table>
					<!--</form>-->
				</div>
			</div>
			<br><br>
			<div class="paging" style="line-height: 35px; float: right; margin: 0.5em 0.5em 1em;">
				<?php echo $this->Paginator->first('<<', array('title'=>'first','class'=>'disabled pagination'));?>
				<?php echo $this->Paginator->prev('<', array('title'=>'prev','class'=>'disabled pagination'));?>
				<?php echo $this->Paginator->numbers(array('separator' => ' ','class'=>'pagination'));?>
				<?php echo $this->Paginator->next('>', array('title'=>'next','class'=>'disabled pagination'));?>
				<?php echo $this->Paginator->last('>>', array('title'=>'last','class'=>'disabled pagination'));?>
			</div>
		<?php else : ?>
			<div style="padding-top:40px;padding-bottom:30px;padding-left:20px;padding-right:20px;width:910px;border:1px solid #DFDFDF;height:80px;">
				<div style="padding-left:300px;">
					<h2><?php echo "Currently no competitions are available";?></h2>
				</div>
			</div>
		<?php endif; ?>
	<?php endif; ?>

