<style>
#content {
    border-bottom: 1px solid #CCCCCC;
    border-left: 1px solid #CCCCCC;
    border-right: 1px solid #CCCCCC;
    margin-left: 20px !important;
    padding-left: 15px !important;
    width: 938px;
    padding-right:0px !important;
    margin-bottom:20px !important;
}
</style>
<?php $this->Paginator->options(array('url' => array_merge($this->params['pass'], $this->params['named']))); ?>

<table style="width: 920px; background-color: rgb(204, 204, 204); height: 85px; margin-top:15px; border: 1px solid rgb(150, 144, 144);">
		<tr>
			<td style="border: none ! important; padding-left: 10px; width: 220px;">
				<strong>Programs</strong> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<?php echo $this->Form->input('Campaign.program',array('type'=>'select','label'=>false,'options'=>$programs_list,'empty'=>'Select Program','style'=>'width:180px'));
				$options = array('url' => 'update_clients','update' => 'CampaignClient');
				echo $this->Ajax->observeField('CampaignProgram', $options);
				?>
			</td>
			<td style="border: none ! important; width: 300px;">
				<strong>Clients</strong> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<?php echo $this->Form->input('Campaign.client',array('type'=>'select','label'=>false,'options'=>$clients_list,'empty'=>'Select Client','style'=>'width:200px'));?>
			</td>
			<td style="border: none ! important; width: 200px;">
				<strong>Status</strong>
				<select name="status" id="opt" style="width:120px">
					<option value=""></option>
					<option value="1">Active</option>
					<option value="0">Pause</option>
				</select>
			</td>
			<td style="border: none ! important; width: 200px;">
				<strong>Competition Type</strong>
				<select name="type" id="optype" style="width:120px">
					<option value=""></option>
					<option value="M">Manual Participation</option>
					<option value="A">Auto Participation</option>
				</select>
			</td>
			<td style="border: none ! important; padding-right: 15px;">
				<button type="submit" onclick="showCampaigns()" class="admin_button"  style="margin-top:13px">Search</button>
			</td>
			<td style="border: none ! important; padding-right: 15px;">
				<button onclick="parent.location='/admin/campaigns/edit/'" class="admin_button"  style="margin-top: 13px; width: 155px; height: 26px;">Create a Competition</button>
			</td>
		</tr>
	</table>
	
<div id="campaigns" style="padding-top:20px;">
	<div>
	<div id="maincontent">
		<div>
			<table width="920" border="0" style="vertical-align: middle;">
	    		<tr class="produtos">
			        <td style="padding-left: 15px;">Competition Title</td>
			        <td style="padding-left: 15px;">Start Date</td>
			        <td style="padding-left: 15px;">End Date</td>
			        <td style="padding-left: 15px;">Participation Type</td>
			        <td style="padding-left: 15px;">Status</td>
			        <td style="padding-left: 15px;"></td>
			        <td style="padding-left: 15px;"></td>
			    </tr>
			    <?php foreach ($campaigns as $campaign): ?>
		        <tr>
			        <td style="vertical-align: middle; padding-left: 15px;"><?php echo $campaign['Campaign']['name'] ?></td>
			        <td style="vertical-align: middle; padding-left: 15px;"><?php echo $campaign['Campaign']['start_date'] ?></td>
			        <td style="vertical-align: middle; padding-left: 15px;"><?php echo $campaign['Campaign']['end_date'] ?></td>
			        <td style="vertical-align: middle; padding-left: 15px;">
			        	<?php if($campaign['Campaign']['campaign_type']=='M') : 
								echo "Manual Participation"; 
							elseif($campaign['Campaign']['campaign_type']=='A') : 
								echo "Auto Participation";	
							endif;
						?>
			        </td>
			        <td style="vertical-align: middle; padding-left: 15px;">
						<?php if($campaign['Campaign']['active']=='1') : ?> 
								<span style="padding-right:20px">Active</span>
								<?php echo $this->Html->link(__('Pause', true), array('action'=>'status', $campaign['Campaign']['id'], $campaign['Campaign']['active'])); ?>
						<?php else : ?>
								<span style="padding-right:10px">Pause</span>
								<?php echo $this->Html->link(__('Activate', true), array('action'=>'status', $campaign['Campaign']['id'], $campaign['Campaign']['active'])); ?>
						<?php endif; ?>
					</td>
			        <td style="vertical-align: middle; padding: 10px;">
			            <table>
			            	<tr>
			            		<td>
						            <a href="/admin/campaigns/edit/<?php echo $campaign['Campaign']['id'] ?>"><img alt="Edit" width="16" height="16" src="/files/campaign/modify.png" title="Edit"></a>
							        &nbsp;&nbsp;&nbsp;
							    </td>
							     <td>
								<a onclick="return confirm('Are you sure you want to delete campaign \'<?php echo $campaign['Campaign']['name'] ?>\'?');" href="/admin/campaigns/delete/<?php echo $campaign['Campaign']['id'] ?>">
									<img alt="Edit" src="/files/campaign/delete.png" title="Delete">
								</a>
								&nbsp;&nbsp;&nbsp;
							    </td>
							    <td>							       
			            			<a href="/admin/campaigns/view/<?php echo $campaign['Campaign']['id'] ?>"><img src="/files/campaign/folder_explore.png" title="View"></a>		            	
				        			&nbsp;&nbsp;&nbsp;
				        		</td>
				        		<td>
				        			<?php //if($campaign['Campaign']['campaign_type'] == 'M'): ?>
			        				<a href="/admin/campaigns/users_participated/<?php echo $campaign['Campaign']['id'] ?>">
										<img width="20" style="cursor:pointer" src="/files/campaign/participants.png" alt="Participants" title="Participants">
									</a>
									<!--&nbsp;&nbsp;&nbsp;-->
									<?php //else : ?>
									<!--&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-->
									<?php //endif; ?>
									
								</td>
								<td>
						        	<a href="/admin/campaigns/view_winners/<?php echo $campaign['Campaign']['id'] ?>">
										<img width="20" title="Winners" alt="Winners" src="/files/campaign/winner.png" style="cursor:pointer">
									</a>
									&nbsp;&nbsp;&nbsp;
								</td>
								<td>
									<?php //f($campaign['Campaign']['campaign_type'] == 'M'): ?>
										<a href="/admin/campaigns/download_participants/<?php echo $campaign['Campaign']['id'] ?>">
											Download<br>Participants
										</a>
									<?php //endif; ?>
								</td>
								<td>
									<?php 
									$days = '';
									if(date('w')==0) :
										$days .= 6;
									endif;
									if(date('w') == 1) :
										$days .= 0;
									endif;
									if(date('w') == 2) :
										$days .= 1;
									endif;
									if(date('w') == 3) :
										$days .= 2;
									endif;
									if(date('w') == 4) :
										$days .= 3;
									endif;
									if(date('w') == 5) :
										$days .= 4;
									endif;
									if(date('w') == 6) :
										$days .= 5;
									endif;
									?>
								<?php 
								   	$winCount = '';$participateCount = '';
									foreach($winner_counts as $cid=>$winnerCount) : 
										if($campaign['Campaign']['id'] == $cid) : 
											$winCount = $winnerCount;
										endif;
									endforeach; 
			
			
									if($campaign['Campaign']['campaign_type'] == 'M') :
										foreach($participant_counts as $key=>$val) : 
											if($campaign['Campaign']['id'] == $key) : 
												$participateCount = $val;
											endif;
										endforeach; 
									endif;
								?>
								
			
							    	<?php if((date("Y-m-d") >= $campaign['Campaign']['draw_start_date']) && (date("Y-m-d") <= $campaign['Campaign']['draw_end_date']) && ($winCount < $campaign['Campaign']['max_winners'])) :?>
									<?php if(($campaign['Campaign']['draw_type'] == 'MD') && ($campaign['Campaign']['active']==1) && ($campaign['Campaign']['week_days']=='' || strpos($campaign['Campaign']['week_days'],$days) !== false)) :?>
										<?php if($campaign['Campaign']['campaign_type'] == 'M' && $campaign['Campaign']['manual_winner_type'] == 'default'): ?>
											<?php if($participateCount!=''  || !empty($participateCount)) : ?>
												&nbsp;&nbsp;&nbsp;
												<a href="/admin/campaigns/manual_draw/<?php echo $campaign['Campaign']['id'] ?>">Draw</a>
											<?php endif; ?>
										<?php  elseif($campaign['Campaign']['campaign_type'] == 'A') :  ?>
											&nbsp;&nbsp;&nbsp;
											<a href="/admin/campaigns/manual_draw/<?php echo $campaign['Campaign']['id'] ?>">Draw</a>
										<?php endif; ?>
									<?php endif; ?>
								<?php endif; ?>
							</td>
						</tr>
						</table>
			        </td>
		    	</tr>
		    	<?php endforeach; ?>
		   </table>
		</div>
	</div>
</div>
<div>
	<?php if(!empty($campaigns)) : ?>
			<div class="paging">
				<table style="border:none;">
			    	<tr>
			        	<td style="border:none;">
							<?php echo $this->Paginator->first('<<', array('title'=>'first','class'=>'disabled pagination1'));?>
							<?php echo $this->Paginator->prev('<', array('title'=>'prev','class'=>'disabled pagination1'));?>
							<?php echo $this->Paginator->numbers(array('class'=>'pagination1'));?>
							<?php echo $this->Paginator->next('>', array('title'=>'next','class'=>'disabled pagination1'));?>
							<?php echo $this->Paginator->last('>>', array('title'=>'last','class'=>'disabled pagination1'));?>
						 </td>
			      	</tr>
			  	</table>    
			</div>
		<?php endif; ?>
	</div>
	
<script>
	function showCampaigns()
	{
		pid = document.getElementById("CampaignProgram").value;	
		cid = document.getElementById("CampaignClient").value;	
		id = document.getElementById('opt').value;
		typeid = document.getElementById('optype').value;
		$.ajax({
			url: '/admin/campaigns/update_campaigns/c='+cid+'/s='+id+'/p='+pid+'/t='+typeid,
			type: "GET",
			success: function (data) {
				$('#campaigns').html(data);
			}
		});
	}
</script>