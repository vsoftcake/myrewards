<style>
#content {
    border-bottom: 1px solid #CCCCCC;
    border-left: 1px solid #CCCCCC;
    border-right: 1px solid #CCCCCC;
    margin-left: 20px !important;
    padding-left: 15px !important;
    width: 938px;
    padding-right:0px !important;
    margin-bottom:20px !important;
}
</style>
<?php $this->Paginator->options(array('url' => array_merge($this->params['pass'], $this->params['named'])));?>
<div>
	<div style="padding:10px;float:right">
		<button onclick="parent.location='/admin/campaigns/download_winners/<?php echo $campaignid; ?>'" class="admin_button rounded_corners_button">
			Download Winners
		</button>
		<button onclick="parent.location='/admin/campaigns/view/<?php echo $campaignid; ?>'" class="admin_button rounded_corners_button">View</button>
		<button onclick="parent.location='/admin/campaigns/users_participated/<?php echo $campaignid; ?>'" class="admin_button">View Participants</button>
		<button onclick="parent.location='/admin/campaigns/list_campaign'" class="admin_button rounded_corners_button">View Competitions List</button>
		<button onclick="parent.location='/admin/campaigns/'" class="admin_button rounded_corners_button">Back To Competition Home</button>
	</div>
	<div id="maincontent">
		<div>
			<table width="920" border="0" style="vertical-align: middle;">
	    		<tr class="produtos">
			    	<td style="padding-left: 10px;">Competition</td>
			        <td style="padding-left: 10px;">User Name</td>
			        <td style="padding-left: 10px;">First Name</td>
			        <td style="padding-left: 10px;">Last Name</td>
			        <td style="padding-left: 10px;">Email</td>
			        <td style="padding-left: 10px;">Participation</td>
			        <td style="padding-left: 10px;">Win Date</td>
			        <td style="padding-left: 10px;"></td>
			    </tr>
			    <?php foreach ($winners as $winner): ?>
		        <tr>
			       	<td style="vertical-align: middle; padding-left: 10px;width:180px;line-height:25px"><?php echo $winner['Campaign']['name'] ?></td>
			        <td style="vertical-align: middle; padding-left: 10px;"><?php echo $winner['CampaignWinner']['winner_name'] ?></td>
			        <td style="vertical-align: middle; padding-left: 10px;"><?php echo $winner['CampaignEntry']['first_name'] ?></td>
			        <td style="vertical-align: middle; padding-left: 10px;"><?php echo $winner['CampaignEntry']['last_name'] ?></td>
			        <td style="vertical-align: middle; padding-left: 10px;"><?php echo $winner['CampaignWinner']['winner_email'] ?></td>
			        <td style="vertical-align: middle; padding-left: 10px;">
				        <?php if($winner['CampaignWinner']['campaign_type']=='M') : 
									echo "Manual Participation"; 
								elseif($winner['CampaignWinner']['campaign_type']=='A') : 
									echo "Auto Participation";	
								endif;
							?>
			       	</td>
			        <td style="vertical-align: middle; padding-left: 15px;"><?php echo $winner['CampaignWinner']['win_date'] ?></td>
		    	</tr>
		    	<?php endforeach; ?>
		   </table>
		</div>
	</div>
		
	<br><br>
	<?php if(!empty($winners)) : ?>
		<div class="paging">
			<table style="border:none;">
			<tr>
				<td style="border:none;">
						<?php echo $this->Paginator->first('<<', array('title'=>'first','class'=>'disabled pagination1'));?>
						<?php echo $this->Paginator->prev('<', array('title'=>'prev','class'=>'disabled pagination1'));?>
						<?php echo $this->Paginator->numbers(array('class'=>'pagination1'));?>
						<?php echo $this->Paginator->next('>', array('title'=>'next','class'=>'disabled pagination1'));?>
						<?php echo $this->Paginator->last('>>', array('title'=>'last','class'=>'disabled pagination1'));?>
					</td>
			</tr>
			</table>    
		</div>
	<?php endif ?>
</div>

