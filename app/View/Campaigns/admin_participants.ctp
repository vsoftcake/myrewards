<style>
	#content {
	    border-bottom: 1px solid #CCCCCC;
	    border-left: 1px solid #CCCCCC;
	    border-right: 1px solid #CCCCCC;
	    margin-left: 20px !important;
	    padding-left: 15px !important;
	    padding-right: 0 !important;
	    width: 938px;
	}
</style>
<div>
	<h3>Want to see the participants of your campaign? </h3>
	Then Select your campaign : 
	<?php 
		echo $this->Form->input('Contest.contest',array('type'=>'select','label'=>false,'options'=>$contestlist,'empty'=>' ','style'=>'width:215px'));
		$options = array('url' => 'users_participated','update' => 'participants');
		echo $this->Ajax->observeField('ContestContest', $options);
	?>
	
	<div id="participants">
		
	</div>
</div>
