<?php
class SafsInvitationController extends AppController {

	var $components = array('Cookie','File','SwiftMailer','RequestHandler','Email');
 	var $uses = array('User','Client','Program','SafsInvitation','ClientsSafsInvitation','SafsLog');

	function admin_index() {
		$conditions = array();
		if (isset($this->params['url']['data'])) {
			$this->Session->write($this->name.'.search', $this->params['url']['data']);
		}

		if ($this->Session->check($this->name.'.search')) {
			$this->request->data = $this->Session->read($this->name.'.search');
			if  (!empty($this->request->data['SafsInvitation']['search'])) {
				$conditions[] = "
				(`SafsInvitation`.`subject` LIKE  '%". $this->request->data['SafsInvitation']['search']. "%'
				OR `SafsInvitation`.`id` LIKE  '%". $this->request->data['SafsInvitation']['search']. "%')
				";
			}
		}

		$this->SafsInvitation->recursive = 0;
		$this->set('invitations', $this->paginate('SafsInvitation',$conditions));

	}


	function admin_edit($id=null) {

		if(!empty($this->request->data))
		{
			if($this->SafsInvitation->save($this->request->data))
			{
				if(!empty($id))
				{
					$this->ClientsSafsInvitation->deleteAll(array('ClientsSafsInvitation.safs_invitation_id'=>$id));

					foreach($this->request->data['Client']['Client'] as $client)
					{
						if(!empty($client))
						{
							$del = $this->ClientsSafsInvitation->deleteAll(array('ClientsSafsInvitation.client_id'=>$client));
							
							$this->request->data['ClientsSafsInvitation']['id'] = 0;
							$this->request->data['ClientsSafsInvitation']['safs_invitation_id'] = $this->SafsInvitation->id;
							$this->request->data['ClientsSafsInvitation']['client_id'] = $client;
							$this->ClientsSafsInvitation->save($this->request->data);
						}
					}
				}
				else
				{
					foreach($this->request->data['Client']['Client'] as $client)
					{
						if(!empty($client))
						{
							$del = $this->ClientsSafsInvitation->deleteAll(array('ClientsSafsInvitation.client_id'=>$client));

							$this->request->data['ClientsSafsInvitation']['id'] = 0;
							$this->request->data['ClientsSafsInvitation']['safs_invitation_id'] = $this->SafsInvitation->id;
							$this->request->data['ClientsSafsInvitation']['client_id'] = $client;
							$this->ClientsSafsInvitation->save($this->request->data);
						}
					}
				}

				$this->Session->setFlash('The Invitation has been saved');
				$this->redirect_session(array('action'=>'index'), null, true);
			}
			else
			{
				$this->Session->setFlash('The Invitation is not saved');
			}

		}
		else
		{
			$this->request->data = $this->SafsInvitation->read(null, $id);
		}

		$clientsList = $this->Client->find('list',array('conditions'=> 'Client.name!=""'));

		$clientsSafInvitation = $this->ClientsSafsInvitation->find('list',array('conditions' =>array('ClientsSafsInvitation.safs_invitation_id' => $this->request->data['SafsInvitation']['id']),'fields'=>array('ClientsSafsInvitation.id','ClientsSafsInvitation.client_id')));
		$clients= array();
		foreach ($clientsSafInvitation as $client) {
			array_push($clients,$client);
		}
		$programsList = $this->Program->find('list');
		if(!empty($clients))
		{

			$sql = "SELECT Client.program_id from clients as Client where Client.id in (".implode(',',$clients).") group by Client.program_id";
			$programsSaf = $this->Client->query($sql);
			$programs= array();
			foreach ($programsSaf as $program) {
				array_push($programs,$program['Client']['program_id']);
			}
		}

		$this->set(compact('programs','clients','clientsList','programsList'));

	}

	function admin_update_program_clients()
	{
		$pid = $this->request->data['Program']['Program'];
		$programs = $this->Program->Client->find('list', array('conditions' =>array('program_id' => $pid)));

		$this->set('program_clients',$programs);
		$this->layout = 'ajax';
	}

	function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash('Invalid id for Invitation');
			$this->redirect(array('action'=>'index'), null, true);
		}
		if ($this->SafsInvitation->delete($id)) {
			$this->ClientsSafsInvitation->deleteAll(array('ClientsSafsInvitation.safs_invitation_id'=>$id));
			$this->Session->setFlash('Safs Invitation #'.$id.' deleted');
			$this->redirect(array('action'=>'index'), null, true);
		}
	}

	function admin_saf_logs()
	{
		$conditions = array();
		if (isset($this->params['url']['data'])) {
			$this->Session->write($this->name.'.search', $this->params['url']['data']);
		}

		if ($this->Session->check($this->name.'.search')) {
			$this->request->data = $this->Session->read($this->name.'.search');
			if  (!empty($this->request->data['SafsLog']['search'])) {
				$conditions[] = "
				(`SafsLog`.`user_id` LIKE  '%". $this->request->data['SafsLog']['search']. "%')
				";
			}
		}

		$this->SafsLog->recursive = 0;
		$this->set('log_details', $this->paginate('SafsLog',$conditions));
	}

	function admin_saf_log_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash('Invalid id for SAF Log');
			$this->redirect(array('action'=>'saf_logs'), null, true);
		}
		if ($this->SafsLog->delete($id)) {
			$this->Session->setFlash('SAF Log #'.$id.' deleted');
			$this->redirect(array('action'=>'saf_logs'), null, true);
		}
	}
}
?>