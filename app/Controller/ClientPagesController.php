<?php
class ClientPagesController extends AppController {

	function admin_edit($client_id = null,  $program_id = null, $page_id = null,$dashId = null) {

		if (!empty($this->request->data)) {

			$this->ClientPage->recursive = -1;
			$old_data = $this->ClientPage->findById($this->request->data['ClientPage']['id']);

			if ($this->ClientPage->save($this->request->data)) {

				//	log the change
				$log = array(
					'id' => null,
					'foreign_id' => $this->{Inflector::singularize($this->name)}->id,
					'foreign_model' => Inflector::singularize($this->name),
					'user_id' => $this->Session->read('user.User.id'),
					'old_data' => $old_data,
					'new_data' => $this->request->data
				);
				$log['description'] = Inflector::humanize(Inflector::underscore($this->name)). ' modified';

				//	only log that the content has been changed, not the content
				if (strcmp($log['old_data']['ClientPage']['content'], $log['new_data']['ClientPage']['content']) != '0') {
					$log['notes'] = 'Content updated';
				}
				unset($log['old_data']['ClientPage']['content']);
				unset($log['new_data']['ClientPage']['content']);

				$this->save_log($log);

				$this->Session->setFlash('Page saved');
				$this->redirect('/admin/clients/edit/'. $this->request->data['ClientPage']['client_id']);
			} else {
				$this->Session->setFlash('The Clients Page could not be saved. Please, try again.');
			}
		} else {
			$this->request->data = $this->_page($page_id, $program_id, $client_id,$dashId);

			if($this->request->data['ClientPage']['enabled_override'] == 1)
				$this->request->data['ClientPage']['enabled']= '';

			if($this->request->data['ClientPage']['id'] == '')
				$this->request->data = '';

			$this->request->data['ClientPage']['client_id'] = $client_id;
			$this->request->data['ClientPage']['page_id'] = $page_id;
			$this->request->data['ClientPage']['dashboard_id'] = $dashId;
		}

		$this->ClientPage->Client->recursive = -1;
		$this->set('client', $this->ClientPage->Client->read(null, $client_id));
		$this->set('dashboard_id',$dashId);
	}
}
?>