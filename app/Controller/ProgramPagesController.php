<?php
class ProgramPagesController extends AppController {

	function admin_edit($program_id = null, $page_id = null, $dashId = null) {

		if (!empty($this->request->data)) {

			$this->ProgramPage->recursive = -1;
			$old_data = $this->ProgramPage->findById($this->request->data['ProgramPage']['id']);

			if ($this->ProgramPage->save($this->request->data)) {

				if ($this->request->data['ClientOverride']['override'] == '1' && isset($this->request->data['ClientOverride']['Page'])) {

					foreach ($this->request->data['ClientOverideClients'] as $client_id)
					{
						$client_page_id = $this->ProgramPage->Program->Client->ClientPage->find('first', array('conditions'=>array('client_id'=>$client_id, 'page_id'=> $page_id,'dashboard_id'=>$dashId),'recursive' => -1, 'fields'=>array('page_id')));

						if($client_page_id['ClientPage']['page_id'] !='' || !empty($client_page_id['ClientPage']['page_id']))
						{
							$sql = "update client_pages cp, program_pages pp
							set cp.client_id = '".$client_id."', cp.page_id = pp.page_id,
							cp.enabled_override = pp.enabled_override, cp.enabled = pp.enabled,
							cp.title_override = pp.title_override, cp.title = pp.title,
							cp.h1_override = pp.h1_override, cp.h1 = pp.h1,
							cp.h2_override = pp.h2_override, cp.h2 = pp.h2,
							cp.h3_override = pp.h3_override, cp.h3 = pp.h3,
							cp.page_title_override = pp.page_title_override, cp.page_title = pp.page_title,
							cp.meta_description_override = pp.meta_description_override, cp.meta_description = pp.meta_description,
							cp.special_offers_override = pp.special_offers_override, cp.special_offers = pp.special_offers,
							cp.whats_new_module_override = pp.whats_new_module_override, cp.whats_new_module = pp.whats_new_module,
							cp.meta_keywords_override = pp.meta_keywords_override, cp.meta_keywords = pp.meta_keywords,
							cp.meta_robots_override = pp.meta_robots_override, cp.meta_robots = pp.meta_robots,
							cp.content_override = pp.content_override, cp.content = pp.content,
							cp.created = pp.created,  cp.modified = pp.modified
							where cp.client_id ='". $client_id. "' and pp.page_id= '".$page_id."' and cp.page_id=pp.page_id
							and pp.program_id='".$program_id."' and cp.dashboard_id=pp.dashboard_id and cp.dashboard_id='".$dashId."'";
							$this->ProgramPage->Program->Client->ClientPage->query($sql);
						}
						else
						{
							$sql = "INSERT INTO client_pages (client_id, page_id, enabled_override, enabled, title_override, title, h1_override, h1,
							h2_override, h2, h3_override, h3, page_title_override, page_title, meta_description_override, meta_description,
							meta_keywords_override, meta_keywords, meta_robots_override, meta_robots, content_override, content,
							special_offers_override, special_offers, whats_new_module_override, whats_new_module, created, modified)
							SELECT '". $client_id. "', page_id, enabled_override, enabled, title_override, title, h1_override, h1,
							h2_override, h2, h3_override, h3, page_title_override, page_title, meta_description_override, meta_description,
							meta_keywords_override, meta_keywords, meta_robots_override, meta_robots, content_override, content,
							special_offers_override, special_offers, whats_new_module_override, whats_new_module, created, modified
							FROM program_pages WHERE program_pages.program_id = '". $program_id. "' and program_pages.page_id='".$page_id."' and dashboard_id='".$dashId."'";
							$this->ProgramPage->Program->Client->ClientPage->query($sql);
						}
					}
				}

				//	log the change
				$log = array(
					'id' => null,
					'foreign_id' => $this->{Inflector::singularize($this->name)}->id,
					'foreign_model' => Inflector::singularize($this->name),
					'user_id' => $this->Session->read('user.User.id'),
					'old_data' => $old_data,
					'new_data' => $this->request->data,
					'notes' => ''
				);
				$log['description'] = Inflector::humanize(Inflector::underscore($this->name)). ' modified';

				//	only log that the content has been changed, not the content
				if (strcmp($log['old_data']['ProgramPage']['content'], $log['new_data']['ProgramPage']['content']) != '0') {
					$log['notes'] = 'Content updated';
				}
				unset($log['old_data']['ProgramPage']['content']);
				unset($log['new_data']['ProgramPage']['content']);

				$this->save_log($log);

				$this->Session->setFlash('Page saved');
				$this->redirect('/admin/programs/edit/'. $this->request->data['ProgramPage']['program_id']);
			} else {
				$this->Session->setFlash('The Page could not be saved. Please, try again.');
			}
			exit();
		}
		if (empty($this->request->data)) {

			$program_pages = $this->ProgramPage->Page->bindModel(
				array(
					'hasOne' => array(
						'ProgramPage' =>array(
							'conditions' => "ProgramPage.program_id = '". $program_id. "' AND ProgramPage.dashboard_id='".$dashId."'",
							'order' => 'Page.sort, Page.title',
						)
					)
				)
			);
			$page = $this->ProgramPage->Page->find('first', array('conditions'=>array(
					"(ProgramPage.program_id = '". $program_id. "' OR ProgramPage.program_id IS NULL) AND Page.id = '". $page_id. "'")));

			$this->request->data = $this->_page($page_id, $program_id,'', $dashId);

			if($this->request->data['ProgramPage']['enabled_override'] == 1)
				$this->request->data['ProgramPage']['enabled']= '';

			if($this->request->data['ProgramPage']['id'] == '')
				$this->request->data = '';

			$this->request->data['ProgramPage']['program_id'] = $program_id;
			$this->request->data['ProgramPage']['page_id'] = $page_id;
			$this->request->data['ProgramPage']['dashboard_id'] = $dashId;
		}

		$this->ProgramPage->Program->recursive = -1;
		$this->set('program', $this->ProgramPage->Program->read(null, $program_id));
		$all_clients = $this->ProgramPage->Program->Client->find('list', array('conditions' => array('Client.program_id' => $program_id)));
		$this->set('all_clients', $all_clients);
		$this->set('dashboard_id',$dashId);
	}
}
?>