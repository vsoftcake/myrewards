<?php
class DisplaysController extends AppController{

	/**
	 * Display a standard page using data for the current client
	*/
	function display($page_name) {
		$page = $this->_clientPageNew('/display/'. $page_name, $this->Session->read('dashboardidinsession'));

		$this->set('page', $page);
		$this->loadModel("Skyscraper");
		$banner_sql = "select Skyscraper.* from skyscrapers as Skyscraper, clients_skyscrapers as ClientsSkyscraper,
		programs_skyscrapers as ProgramsScraper where ClientsSkyscraper.client_id=".$this->Session->read('client.Client.id')." and
		ProgramsScraper.program_id=".$this->Session->read('client.Client.program_id')." and Skyscraper.id=ClientsSkyscraper.skyscraper_id
		and Skyscraper.id=ProgramsScraper.skyscraper_id order by rand() limit 1";
		$banner_list = $this->Skyscraper->query($banner_sql);
		$this->set('bannerlist',$banner_list);
		if ($page['ClientPage']['special_offers'] == '1') {
			if($this->Session->read('dashboardidinsession')==1)
			{
				$sql = "SELECT DISTINCT Product. * , Merchant.*, MerchantAddress. * FROM products_addresses as ProductsAddress,
				categories as Catgeory, products AS Product
				INNER JOIN categories_products AS CategoriesProduct ON Product.id = CategoriesProduct.product_id
				INNER JOIN clients_categories AS ClientsCategory ON CategoriesProduct.category_id = ClientsCategory.category_id	AND ClientsCategory.client_id = '". $this->Session->read('client.Client.id'). "'
				INNER JOIN clients_products AS ClientsProduct ON Product.id = ClientsProduct.product_id
				LEFT JOIN merchant_addresses AS MerchantAddress ON Product.merchant_id = MerchantAddress.merchant_id
				LEFT JOIN merchants AS Merchant ON Product.merchant_id = Merchant.id
				WHERE Product.special_offer = 1 AND  Product.modified >= '". date('Y-m-d h:i:s', strtotime('-60 days')). "'
				AND Product.active = 1 AND (Product.offer_start = '0000-00-00' OR Product.offer_start <= NOW())
				AND (Product.expires = '0000-00-00' OR Product.expires >= NOW()) AND MerchantAddress.country='". $this->Session->read('client.Client.country'). "'
				AND ProductsAddress.merchant_address_id= MerchantAddress.id AND ProductsAddress.product_id=Product.id
				AND Catgeory.countries = '". $this->Session->read('client.Client.country'). "' AND Catgeory.id=CategoriesProduct.category_id
				AND (ClientsProduct.client_id = '". $this->Session->read('client.Client.id'). "' OR ClientsProduct.client_id = 0)
				AND (ClientsProduct.product_id not in (select product_id from products_safs union select product_id from products_points))";

				if (($this->Session->read('client.Client.blacklisted_product') != '')) {
					$sql .= " AND Product.id not in (". $this->Session->read('client.Client.blacklisted_product'). ") ";
				}
				$sql .=" group by Product.id ORDER BY RAND() LIMIT 12";

			}
			if($this->Session->read('dashboardidinsession')==2)
			{
				$sql = "SELECT DISTINCT Product. * , Merchant.*, MerchantAddress. *
						FROM clients_products_safs AS ClientsProductsSaf, products_addresses as ProductsAddress,
						categories as Catgeory, products AS Product
						INNER JOIN categories_products AS CategoriesProduct ON Product.id = CategoriesProduct.product_id
						INNER JOIN clients_categories AS ClientsCategory ON CategoriesProduct.category_id = ClientsCategory.category_id
						AND ClientsCategory.client_id = '". $this->Session->read('client.Client.id'). "'
						LEFT JOIN merchant_addresses AS MerchantAddress ON Product.merchant_id = MerchantAddress.merchant_id
						LEFT JOIN merchants AS Merchant ON Product.merchant_id = Merchant.id
						WHERE Product.special_offer = 1 AND  Product.modified >= '". date('Y-m-d h:i:s', strtotime('-60 days')). "'
						AND Product.active = 1 AND (Product.offer_start = '0000-00-00' OR Product.offer_start <= NOW())
						AND (Product.expires = '0000-00-00' OR Product.expires >= NOW()) AND MerchantAddress.country='". $this->Session->read('client.Client.country'). "' AND
						Product.id = ClientsProductsSaf.product_id AND (ClientsProductsSaf.client_id = '". $this->Session->read('client.Client.id'). "' OR ClientsProductsSaf.client_id = 0)
						AND ProductsAddress.merchant_address_id= MerchantAddress.id AND ProductsAddress.product_id=Product.id
						AND Catgeory.countries = '". $this->Session->read('client.Client.country'). "' AND Catgeory.id=CategoriesProduct.category_id";

						if (($this->Session->read('client.Client.blacklisted_product') != '')) {
							$sql .= " AND Product.id not in (". $this->Session->read('client.Client.blacklisted_product'). ") ";
						}

				$sql .=" group by Product.id ORDER BY RAND() LIMIT 12";

			}
			if($this->Session->read('dashboardidinsession')==3)
			{
				$sql = "SELECT DISTINCT Product. * , Merchant.*, MerchantAddress. *
						FROM clients_products_points AS ClientsProductsPoint, products_addresses as ProductsAddress,
						categories as Catgeory, products_points AS ProductsPoint, products AS Product
						INNER JOIN categories_products AS CategoriesProduct ON Product.id = CategoriesProduct.product_id
						INNER JOIN clients_categories AS ClientsCategory ON CategoriesProduct.category_id = ClientsCategory.category_id
						AND ClientsCategory.client_id = '". $this->Session->read('client.Client.id'). "'
						LEFT JOIN merchant_addresses AS MerchantAddress ON Product.merchant_id = MerchantAddress.merchant_id
						LEFT JOIN merchants AS Merchant ON Product.merchant_id = Merchant.id
						WHERE Product.special_offer = 1 AND  Product.modified >= '". date('Y-m-d h:i:s', strtotime('-60 days')). "'
						AND Product.active = 1 AND (Product.offer_start = '0000-00-00' OR Product.offer_start <= NOW())
						AND (Product.expires = '0000-00-00' OR Product.expires >= NOW()) AND MerchantAddress.country='". $this->Session->read('client.Client.country'). "' AND
						Product.id = ClientsProductsPoint.product_id AND (ClientsProductsPoint.client_id = '". $this->Session->read('client.Client.id'). "' OR ClientsProductsPoint.client_id = 0)
						AND ProductsAddress.merchant_address_id= MerchantAddress.id AND ProductsAddress.product_id=Product.id
						AND Catgeory.countries = '". $this->Session->read('client.Client.country'). "' AND Catgeory.id=CategoriesProduct.category_id
						AND ProductsPoint.product_id = ClientsProductsPoint.product_id AND ProductsPoint.active=1";

						if (($this->Session->read('client.Client.blacklisted_product') != '')) {
							$sql .= " AND Product.id not in (". $this->Session->read('client.Client.blacklisted_product'). ") ";
						}
				$sql .=" group by Product.id ORDER BY RAND() LIMIT 12";
			}

			$special_offers = $this->Domain->query($sql);
			$userid = $this->Session->read('user.User.id');
			$this->loadModel("User");
			$user_used_count = $this->User->find('first',array('conditions' => array('User.id' => $userid), 'recursive' => -1, 'fields' => array('User.used_count')));			$this->set('used_count',$user_used_count);
			$this->set('special_offers', $special_offers);
		}

		if ($page['ClientPage']['whats_new_module'] == '1') {

			// whats new  offers
			$this->Domain->Client->Product->recursive = 0;
			$this->Domain->Client->Product->bindModel(array(
				'hasOne' => array(
					'ClientsWhatsNewProduct' => array(
						'foreignKey' => 'product_id'),
					'MerchantAddress' => array(
						'conditions' => '',
						'foreignKey' => 'merchant_id',
						'localKey' => 'merchant_id')
			)));

			$sql = "SELECT Product.*, Merchant.*, ClientsWhatsNewProduct.*, MerchantAddress.*
			FROM products AS Product LEFT JOIN merchants AS Merchant ON (Product.merchant_id = Merchant.id)
			LEFT JOIN clients_whats_new_products AS ClientsWhatsNewProduct ON (ClientsWhatsNewProduct.product_id = Product.id)
			LEFT JOIN merchant_addresses AS MerchantAddress ON (MerchantAddress.merchant_id = Product.merchant_id)
			WHERE ClientsWhatsNewProduct.client_id = ".$this->Session->read('client.Client.id')." AND Product.active = 1
			AND MerchantAddress.country='". $this->Session->read('client.Client.country'). "'";
			if (($this->Session->read('client.Client.blacklisted_product') != '')) {
										$sql .= " AND Product.id not in (". $this->Session->read('client.Client.blacklisted_product'). ") ";
									}
			$sql .="group by Product.id order by rand()";

			$whats_new_offers = $this->Domain->Client->Product->query($sql);
			$this->set('whats_new_offers', $whats_new_offers);
		}
		if ($page['Page']['name'] == '/display/whats_new') {
			if($this->Session->read('dashboardidinsession')==1)			{
				$sql = "SELECT DISTINCT Product. * , Merchant.*, MerchantAddress. * FROM products_addresses as ProductsAddress,
				categories as Catgeory, products AS Product
				INNER JOIN categories_products AS CategoriesProduct ON Product.id = CategoriesProduct.product_id
				INNER JOIN clients_categories AS ClientsCategory ON CategoriesProduct.category_id = ClientsCategory.category_id
				AND ClientsCategory.client_id = '". $this->Session->read('client.Client.id'). "'
				INNER JOIN clients_products AS ClientsProduct ON Product.id = ClientsProduct.product_id
				AND (ClientsProduct.client_id = '". $this->Session->read('client.Client.id'). "' OR ClientsProduct.client_id = 0)
				LEFT JOIN merchant_addresses AS MerchantAddress ON Product.merchant_id = MerchantAddress.merchant_id
				INNER JOIN merchants AS Merchant ON Product.merchant_id = Merchant.id
				WHERE Product.created >= '". date('Y-m-d h:i:s', strtotime('-60 days')). "' AND Product.active = 1
				AND (Product.offer_start = '0000-00-00' OR Product.offer_start <= NOW()) AND (Product.expires = '0000-00-00' OR Product.expires >= NOW())
				AND MerchantAddress.country='". $this->Session->read('client.Client.country'). "'
				AND ProductsAddress.merchant_address_id= MerchantAddress.id AND ProductsAddress.product_id=Product.id
				AND Catgeory.countries = '". $this->Session->read('client.Client.country'). "' AND Catgeory.id=CategoriesProduct.category_id
				AND (ClientsProduct.product_id not in (select product_id from products_safs union select product_id from products_points))";

				if (($this->Session->read('client.Client.blacklisted_product') != '')) {
					$sql .= " AND Product.id not in (". $this->Session->read('client.Client.blacklisted_product'). ") ";						}
				$sql .=" group by Product.id ORDER BY RAND() LIMIT 51";			}

			if($this->Session->read('dashboardidinsession')==2)
			{
				$sql = "SELECT DISTINCT Product. * , Merchant.*, MerchantAddress. *
						FROM clients_products_saf as ClientsProductsSaf, products_addresses as ProductsAddress,
						categories as Catgeory, products AS Product
						INNER JOIN categories_products AS CategoriesProduct ON Product.id = CategoriesProduct.product_id
						INNER JOIN clients_categories AS ClientsCategory ON CategoriesProduct.category_id = ClientsCategory.category_id
						AND ClientsCategory.client_id = '". $this->Session->read('client.Client.id'). "'
						LEFT JOIN merchant_addresses AS MerchantAddress ON Product.merchant_id = MerchantAddress.merchant_id
						INNER JOIN merchants AS Merchant ON Product.merchant_id = Merchant.id
						WHERE Product.created >= '". date('Y-m-d h:i:s', strtotime('-60 days')). "' AND Product.active = 1
						AND (Product.offer_start = '0000-00-00' OR Product.offer_start <= NOW()) AND (Product.expires = '0000-00-00' OR Product.expires >= NOW())
						AND MerchantAddress.country='". $this->Session->read('client.Client.country'). "'
						AND Product.id = ClientsProductsSaf.product_id AND (ClientsProductsSaf.client_id = '". $this->Session->read('client.Client.id'). "' OR ClientsProductsSaf.client_id = 0)
						AND ProductsAddress.merchant_address_id= MerchantAddress.id AND ProductsAddress.product_id=Product.id
						AND Catgeory.countries = '". $this->Session->read('client.Client.country'). "' AND Catgeory.id=CategoriesProduct.category_id";

						if (($this->Session->read('client.Client.blacklisted_product') != '')) {
							$sql .= " AND Product.id not in (". $this->Session->read('client.Client.blacklisted_product'). ") ";
						}
				$sql .=" group by Product.id ORDER BY RAND() LIMIT 51";
			}
			if($this->Session->read('dashboardidinsession')==3)
			{
				$sql = "SELECT DISTINCT Product. * , Merchant.*, MerchantAddress. *
						FROM clients_products_point as ClientsProductsPoint, products_addresses as ProductsAddress,
						categories as Catgeory, products_points AS ProductsPoint, products AS Product
						INNER JOIN categories_products AS CategoriesProduct ON Product.id = CategoriesProduct.product_id
						INNER JOIN clients_categories AS ClientsCategory ON CategoriesProduct.category_id = ClientsCategory.category_id
						AND ClientsCategory.client_id = '". $this->Session->read('client.Client.id'). "'
						LEFT JOIN merchant_addresses AS MerchantAddress ON Product.merchant_id = MerchantAddress.merchant_id
						INNER JOIN merchants AS Merchant ON Product.merchant_id = Merchant.id
						WHERE Product.created >= '". date('Y-m-d h:i:s', strtotime('-60 days')). "' AND Product.active = 1
						AND (Product.offer_start = '0000-00-00' OR Product.offer_start <= NOW()) AND (Product.expires = '0000-00-00' OR Product.expires >= NOW())
						AND MerchantAddress.country='". $this->Session->read('client.Client.country'). "'
						AND Product.id = ClientsProductsPoint.product_id AND (ClientsProductsPoint.client_id = '". $this->Session->read('client.Client.id'). "' OR ClientsProductsPoint.client_id = 0)
						AND ProductsAddress.merchant_address_id= MerchantAddress.id AND ProductsAddress.product_id=Product.id
						AND Catgeory.countries = '". $this->Session->read('client.Client.country'). "' AND Catgeory.id=CategoriesProduct.category_id
						AND ProductsPoint.product_id = ClientsProductsPoint.product_id AND ProductsPoint.active=1 ";

						if (($this->Session->read('client.Client.blacklisted_product') != '')) {
							$sql .= " AND Product.id not in (". $this->Session->read('client.Client.blacklisted_product'). ") ";
						}
				$sql .=" group by Product.id ORDER BY RAND() LIMIT 51";
			}

			$new_products = $this->Domain->query($sql);

			$userid = $this->Session->read('user.User.id');
			$this->loadModel("User");
			$user_used_count = $this->User->find('first',array('conditions' => array('User.id' => $userid), 'recursive' => -1, 'fields' => array('User.used_count')));
			$this->set('used_count',$user_used_count);
			$this->set('new_products', $new_products);
		}

		if ($page['Page']['name'] == '/display/nominate_a_friend') {
				$userid= $this->Session->read('user.User.id');
				$sql = "SELECT count(*)As count FROM user_nominates As UserNominate where user_id =$userid";
				$count = $this->Domain->query($sql);
				$this->set('count', $count);
		}	}
}
?>