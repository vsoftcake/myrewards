<?php
class PagesController extends AppController {

	public $uses = array();

/**
 * Displays a view
 *
 * @param mixed What page to display
 * @access public
 */
	public function display() {
		if (!func_num_args()) {
			$this->redirect('/');
		}
		$path = func_get_args();

		if (!count($path)) {
			$this->redirect('/');
		}
		$count = count($path);
		$page = null;
		$subpage = null;
		$title = null;

		if (!empty($path[0])) {
			$page = $path[0];
		}
		if (!empty($path[1])) {
			$subpage = $path[1];
		}
		if (!empty($path[$count - 1])) {
			$title = Inflector::humanize($path[$count - 1]);
		}

		$type = $this->Session->read('dashboard');

		/* When a client has his own home page this is where customers get redirected to it after they have logged in */
		$home_page = $this->Session->read('client.Client.home_page');
		if ($page == 'home' && !empty($home_page )) {
			$this->redirect($home_page);
			exit();
		}

		/* If this domain has an independent home page for anon users then go there */
		// If not logged in
		if(!$this->Session->read('user')) {
			// If has domain home page
			$this->Domain->recursive = -1;
			$domain = $this->Domain->findByName($_SERVER['HTTP_HOST']);
			if($domain['Domain']['home_page'] != ''){
				// Then redirect to that page
				$this->redirect($domain['Domain']['home_page']);
			}
		}


		//	get the module data if needed
		$module_data = array();

		if (($page == 'myrewards' || ($page == 'home' && $this->Session->read('client.Client.dashboard') ==1)) && ($this->Session->read('user') || $this->Session->read('client.Domain.public') == '1'))
		{
			$module_data = array_merge($module_data, $this->_memberUpdateModuleData());
			$module_data = array_merge($module_data, $this->_specialOfferModuleData(1));
			$module_data = array_merge($module_data, $this->_hotOfferModuleData(1));
			$module_data = array_merge($module_data, $this->_favouritesModuleData(1));
			$module_data = array_merge($module_data, $this->_googleMapModuleData());
			$module_data = array_merge($module_data, $this->_stayTouchModuleData(1));
			$module_data = array_merge($module_data, $this->_checkOnlineModuleData(1));
			$module_data = array_merge($module_data, $this->_newsModuleData(1));
			$module_data = array_merge($module_data, $this->_replacementModuleData(1));

			$dashboardidinsession=1;
			$this->Session->write('dashboardidinsession', $dashboardidinsession);
			$style_query="SELECT style_id from clients_dashboards where client_id = '". $this->Session->read('client.Client.id'). "' and dashboard_id=1";
			$this->loadModel('Dashboard');
			$style_id_dashboard=$this->Dashboard->query($style_query);
			$style_id=$style_id_dashboard[0]['clients_dashboards']['style_id'];
			$this->Session->write('dashboard_style_id', $style_id);
		}
		else if (($page == 'send_a_friend' || ($page == 'home' && $this->Session->read('client.Client.dashboard')==2)) && ($this->Session->read('user') || $this->Session->read('client.Domain.public') == '1'))
		{
			$module_data = array_merge($module_data, $this->_memberUpdateModuleData());
			$module_data = array_merge($module_data, $this->_specialOfferModuleData(2));
			$module_data = array_merge($module_data, $this->_hotOfferModuleData(2));
			$module_data = array_merge($module_data, $this->_favouritesModuleData(2));
			$module_data = array_merge($module_data, $this->_googleMapModuleData());
			$module_data = array_merge($module_data, $this->_stayTouchModuleData(2));
			$module_data = array_merge($module_data, $this->_checkOnlineModuleData(2));
			$module_data = array_merge($module_data, $this->_newsModuleData(2));
			$dashboardidinsession=2;
			$this->Session->write('dashboardidinsession', $dashboardidinsession);
			$style_query="SELECT style_id from clients_dashboards where client_id = '". $this->Session->read('client.Client.id'). "' and dashboard_id=2";
			$this->loadModel('Dashboard');
			$style_id_dashboard=$this->Dashboard->query($style_query);
			$style_id=$style_id_dashboard[0]['clients_dashboards']['style_id'];
			$this->Session->write('dashboard_style_id', $style_id);
		}
		else if (($page == 'mypoints' || ($page == 'home' && $this->Session->read('client.Client.dashboard') ==3 )) && ($this->Session->read('user') || $this->Session->read('client.Domain.public') == '1'))
		{
			$module_data = array_merge($module_data, $this->_memberUpdateModuleData());
			$module_data = array_merge($module_data, $this->_specialOfferModuleData(3));
			$module_data = array_merge($module_data, $this->_hotOfferModuleData(3));
			$module_data = array_merge($module_data, $this->_favouritesModuleData(3));
			$module_data = array_merge($module_data, $this->_googleMapModuleData());
			$module_data = array_merge($module_data, $this->_stayTouchModuleData(3));
			$module_data = array_merge($module_data, $this->_checkOnlineModuleData(3));
			$module_data = array_merge($module_data, $this->_newsModuleData(3));
			$module_data = array_merge($module_data, $this->_userRedeemModuleData());
			$dashboardidinsession=3;
			$this->Session->write('dashboardidinsession', $dashboardidinsession);
			$style_query="SELECT style_id from clients_dashboards where client_id = '". $this->Session->read('client.Client.id'). "' and dashboard_id=3";
			$this->loadModel('Dashboard');
			$style_id_dashboard=$this->Dashboard->query($style_query);
			$style_id=$style_id_dashboard[0]['clients_dashboards']['style_id'];
			$this->Session->write('dashboard_style_id', $style_id);
		}

		$this->set('module_data', $module_data);

		//	set allowed modules array
		if ($page == 'home') {
			$allowed_modules = array();
			foreach ($this->Session->read('client.Module') as $module) {
				$allowed_modules[] = $module['title'];
				$modules_set[] = $module['id'];
			}
			$this->set('allowed_modules', $allowed_modules);
			$this->set('modules_set', $modules_set);
		}

		$this->set('subpage', $subpage);
		$this->set('title', $title);
		$this->render(join('/', $path));
		$this->set('pageName', $page);
	}

	function _memberUpdateModuleData() {

		return array('member_update' => $this->_clientPage('/display/member_update'));
	}

	function _hotOfferModuleData($id) {
		if($id==3)
		{
			$hotOffer = "SELECT Product.id, ProductsPoint.logo_extension FROM products_points AS ProductsPoint , clients_products_points AS ClientsProductsPoint,
			products AS Product,products_addresses as ProductsAddress, merchant_addresses as MerchantAddress,
			categories as Catgeory, clients_categories AS ClientsCategory, categories_products AS CategoriesProduct
			WHERE Product.active = 1 and Product.cancelled = 0 AND (Product.offer_start = '0000-00-00' OR Product.offer_start <= NOW())
			AND (Product.expires = '0000-00-00' OR Product.expires >= NOW()) and Product.id = ClientsProductsPoint.product_id
			AND (ClientsProductsPoint.client_id = '". $this->Session->read('client.Client.id'). "' OR ClientsProductsPoint.client_id = 0) and ProductsAddress.product_id=Product.id
			and ProductsAddress.merchant_address_id = MerchantAddress.id and MerchantAddress.country='".$this->Session->read('client.Client.country')."'
			And Product.id= ProductsPoint.product_id and ProductsPoint.logo_extension!=''
			AND ClientsCategory.category_id = CategoriesProduct.category_id AND CategoriesProduct.product_id=Product.id
			AND ClientsCategory.client_id = '".$this->Session->read('client.Client.id')."'
			AND Catgeory.countries = '". $this->Session->read('client.Client.country'). "' AND Catgeory.id=CategoriesProduct.category_id
			AND ProductsPoint.product_id = ClientsProductsPoint.product_id AND ProductsPoint.active=1 ";

			if (($this->Session->read('client.Client.blacklisted_product') != ''))
			{
					$hotOffer .= " AND Product.id not in (". $this->Session->read('client.Client.blacklisted_product'). ") ";
			}
			$hotOffer .= "ORDER by rand() limit 1";
		}
		if($id==2)
		{
			$hotOffer = "SELECT Product.id, ProductsSaf.logo_extension FROM products_safs AS ProductsSaf , clients_products_safs AS ClientsProductsSaf,
			products AS Product,products_addresses as ProductsAddress,merchant_addresses as MerchantAddress, clients_categories AS ClientsCategory,
			categories as Catgeory, categories_products AS CategoriesProduct
			WHERE Product.active = 1 and Product.cancelled = 0 AND (Product.offer_start = '0000-00-00' OR Product.offer_start <= NOW())
			AND (Product.expires = '0000-00-00' OR Product.expires >= NOW()) and Product.id = ClientsProductsSaf.product_id
			AND (ClientsProductsSaf.client_id = '". $this->Session->read('client.Client.id'). "' OR ClientsProductsSaf.client_id = 0) and ProductsAddress.product_id=Product.id
			and ProductsAddress.merchant_address_id = MerchantAddress.id and MerchantAddress.country='".$this->Session->read('client.Client.country')."'
			And Product.id= ProductsSaf.product_id and ProductsSaf.logo_extension!=''
			AND ClientsCategory.category_id = CategoriesProduct.category_id AND CategoriesProduct.product_id=Product.id
			AND ClientsCategory.client_id = '".$this->Session->read('client.Client.id')."'
			AND Catgeory.countries = '". $this->Session->read('client.Client.country'). "' AND Catgeory.id=CategoriesProduct.category_id ";

			if (($this->Session->read('client.Client.blacklisted_product') != ''))
			{
					$hotOffer .= " AND Product.id not in (". $this->Session->read('client.Client.blacklisted_product'). ") ";
			}
			$hotOffer .= "ORDER by rand() limit 1";
		}
		if($id==1)
		{

			$hotOffer = "SELECT Product.id, Product.hotoffer_extension FROM clients_products AS ClientsProduct, products AS Product,
			products_addresses as ProductsAddress, merchant_addresses as MerchantAddress, clients_categories AS ClientsCategory,
			categories as Catgeory, categories_products AS CategoriesProduct
			WHERE Product.active = 1 and Product.cancelled = 0 and Product.hotoffer=1
			AND (Product.offer_start = '0000-00-00' OR Product.offer_start <= NOW()) AND (Product.expires = '0000-00-00' OR Product.expires >= NOW())
			and Product.id = ClientsProduct.product_id AND (ClientsProduct.client_id = '". $this->Session->read('client.Client.id'). "' OR ClientsProduct.client_id = 0)
			and ProductsAddress.product_id=Product.id and ProductsAddress.merchant_address_id = MerchantAddress.id
			and MerchantAddress.country='".$this->Session->read('client.Client.country')."' and Product.hotoffer_extension!=''
			AND ClientsCategory.category_id = CategoriesProduct.category_id AND CategoriesProduct.product_id=Product.id
			AND ClientsCategory.client_id = '".$this->Session->read('client.Client.id')."'
			AND Catgeory.countries = '". $this->Session->read('client.Client.country'). "' AND Catgeory.id=CategoriesProduct.category_id
			AND (ClientsProduct.product_id not in (select product_id from products_safs union select product_id from products_points)) ";

			if (($this->Session->read('client.Client.blacklisted_product') != ''))
			{
					$hotOffer .= " AND Product.id not in (". $this->Session->read('client.Client.blacklisted_product'). ") ";
			}
			$hotOffer .= "ORDER by rand() limit 1";
		}
		$hotOffer = $this->Domain->Client->Product->query($hotOffer);

		 foreach($hotOffer as $pr){
				  $pid[] = $pr['Product']['id'];
				   }
		 $product_type = 'hot_Offer';
		$this->recordActivity($pid,$product_type);

		return array('hot_offers' => $hotOffer);
	}

	function _stayTouchModuleData($dashId) {
		return array('stay_touch' => $this->_clientPageNew('/display/follow_us',$dashId));
	}

	function _checkOnlineModuleData($dashId) {
		return array('check_online' => $this->_clientPageNew('/display/check_online',$dashId));
	}

	function _newsModuleData($dashId)
	{
		return array('news' => $this->_clientPageNew('/display/check_news',$dashId));
	}

	function _replacementModuleData($dashId)
	{
		return array('replacement' => $this->_clientPageNew('/display/map_replacement',$dashId));
	}

	function _googleMapModuleData()
	{
		 $google_map= $this->requestAction('/Products/googlemap');
   		//$this->set('google_map', $google_map);
		return array('google_map' => $google_map);
	}

	function _favouritesModuleData()
	{
		$sql = "SELECT Merchant.id, Merchant.logo_extension , Product.id, Product.image_extension, Product.display_image, Product.name, Product.highlight
		FROM merchant_addresses as MerchantAddress, products_addresses as ProductsAddress, clients_products AS ClientsProduct,
		merchants as Merchant, products AS Product, clients_categories AS ClientsCategory, categories_products AS CategoriesProduct
		WHERE Product.merchant_id = MerchantAddress.merchant_id and ProductsAddress.merchant_address_id=MerchantAddress.id
		and ProductsAddress.product_id=Product.id AND Product.active = 1 and Product.cancelled = 0 and Product.favourites=1
		AND (Product.offer_start = '0000-00-00' OR Product.offer_start <= NOW()) AND (Product.expires = '0000-00-00' OR Product.expires >= NOW())
		and Product.id = ClientsProduct.product_id AND (ClientsProduct.client_id = '". $this->Session->read('client.Client.id'). "' OR ClientsProduct.client_id = 0)
		and Product.merchant_id = Merchant.id and Merchant.logo_extension !='' and Merchant.mail_country='".$this->Session->read('client.Client.country')."'
		AND ClientsCategory.category_id = CategoriesProduct.category_id AND CategoriesProduct.product_id=Product.id AND ClientsCategory.client_id = '".$this->Session->read('client.Client.id')."'
		group by Product.id ORDER by rand() limit 12";

		$favouriteOffer = $this->Domain->Client->Product->Merchant->query($sql);
		  foreach($favouriteOffer as $pr){
				  $pid[] = $pr['Product']['id'];
				   }

		 $product_type = 'favourite_Offer';
		$this->recordActivity($pid,$product_type);

		return array('favourites' => $favouriteOffer);
	}

	function _specialOfferModuleData($dashboard_id)
	{
		if($dashboard_id==1)
		{
			$limit=$this->Session->read('client.Client.special_offer_module_limit');
		}
		if($dashboard_id==2)
		{
			$limit=$this->Session->read('client.Client.special_offer_module_limit_saf');
		}
		if($dashboard_id==3)
		{
			$limit=$this->Session->read('client.Client.special_offer_module_limit_points');
		}

		//	get the clients available categories
		$categories = $this->Domain->Client->ClientsCategory->find('list',
			array(
				'conditions' => array('ClientsCategory.client_id' => $this->Session->read('client.Client.id')),
				'fields' => array('ClientsCategory.category_id')
			)
		);

		$this->Domain->Client->Product->recursive = 0;
		$this->Domain->Client->Product->bindModel(array(
			'hasOne' => array(
				'ClientsSpecialProduct' => array(
					'foreignKey' => 'product_id'),
				'MerchantAddress' => array(
					'foreignKey' => false,
					'conditions' => "MerchantAddress.merchant_id = Merchant.id",
					'localKey' => 'merchant_id'),
				'ClientsProduct' => array(
					'foreignKey' => 'product_id',
					'conditions' => "(ClientsProduct.client_id = '". $this->Session->read('client.Client.id'). "' OR ClientsProduct.client_id = 0)"
				),
				'CategoriesProduct' => array(
					'foreignKey' => 'product_id',
					'conditions' => "CategoriesProduct.category_id IN ('". implode("','", $categories). "')"

				),
		)));

		//	the sub-query here prevents duplicates
		$special_offers = $this->Domain->Client->Product->find('all', array('conditions'=>array(
			"ClientsSpecialProduct.client_id = '". $this->Session->read('client.Client.id'). "'
			AND ClientsSpecialProduct.dashboard_id = $dashboard_id
			AND Product.active = 1
			AND (Product.expires = '0000-00-00' OR Product.expires >= NOW())
			AND MerchantAddress.country = '".$this->Session->read('client.Client.country')."'
			AND ClientsProduct.id IS NOT NULL
			AND CategoriesProduct.id IS NOT NULL
			AND CategoriesProduct.category_id = (SELECT min(category_id) FROM categories_products
			WHERE product_id = Product.id AND categories_products.category_id IN ('". implode("','", $categories). "')) group by Product.id "),
			"order"=>'RAND()', 'limit'=>$limit));

              foreach($special_offers as $pr){
				  $pid[] = $pr['Product']['id'];
				   }

				 $product_type = 'special_offer';
				  $this->recordActivity($pid,$product_type);


			$userid = $this->Session->read('user.User.id');
			$this->loadModel("User");
			$user_used_count = $this->User->find('first',array('conditions' => array('User.id' => $userid), 'recursive' => -1, 'fields' => array('User.used_count')));

			return array('special_offers' => $special_offers, 'used_count'=>$user_used_count);
	}

	function _whatsNewModuleData() {
		$whats_new  = $this->_clientPage('/display/whats_new_module');

		//	get the clients available categories
		$categories = $this->Domain->Client->ClientsCategory->find('list',
			array(
				'conditions' => array('ClientsCategory.client_id' => $this->Session->read('client.Client.id')),
				'fields' => array(1 => 'ClientsCategory.category_id')
			)
		);

		// whats new  offers
		$this->Domain->Client->Product->recursive = 0;
		$this->Domain->Client->Product->bindModel(array(
			'hasOne' => array(
				'ClientsWhatsNewProduct' => array(
					'foreignKey' => 'product_id'),
				'MerchantAddress' => array(
					'foreignKey' => 'merchant_id'),
					'ClientsProduct' => array(
					'foreignKey' => 'product_id',
					'conditions' => "(ClientsProduct.client_id = '". $this->Session->read('client.Client.id'). "' OR ClientsProduct.client_id = 0)"
				),
				'CategoriesProduct' => array(
					'foreignKey' => 'product_id',
					'conditions' => "CategoriesProduct.category_id IN ('". implode("','", $categories). "')"

				),
		)));
		//	the sub-query here prevents duplicates
		$whats_new_offers = $this->Domain->Client->Product->find('all', array('conditions'=>array(
			"ClientsWhatsNewProduct.client_id = '". $this->Session->read('client.Client.id'). "'
			AND Product.active = 1
			AND Product.created >= DATE_SUB(NOW(), INTERVAL 1 MONTH)
			AND (Product.expires = '0000-00-00' OR Product.expires >= NOW())
			AND ClientsProduct.id IS NOT NULL
			AND CategoriesProduct.id IS NOT NULL
			AND CategoriesProduct.category_id = (SELECT min(category_id) FROM categories_products WHERE product_id = Product.id AND categories_products.category_id IN ('". implode("','", $categories). "'))"
			, "order"=>'RAND()', 'limit'=>$this->Session->read('client.Client.whats_new_module_limit'))));

		return array('whats_new' => $whats_new, 'whats_new_offers' => $whats_new_offers);
	}

	function _userRedeemModuleData()
	{
		 $user_redeem= $this->requestAction('/Users/user_redeem');

		 $userredeempoints=$this->User->find('first',array('conditions' => array('User.id' => $this->Session->read('user.User.id')), 'recursive' => -1, 'fields' => array('User.points')));
		 $this->set('userredeempoints',$userredeempoints);
		 return array('user_redeem' => $user_redeem);
	}
}
