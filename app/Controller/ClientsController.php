<?php
class ClientsController extends AppController {

	var $components = array('File');
	var $uses = array('User', 'Client','ClientBanner','PaypalDetail','CurrencyCode','ClientsProduct','ClientsProductsSaf','ClientsProductsPoint','ClientPage','AdminTrack');

 function beforeFilter(){ 	 
     $data = $this->data;
	 $login_user = $this->Session->read('user.User.first_name').' '.$this->Session->read('user.User.last_name');  
     $this->admin_recordActivity($data,$login_user); 	 
 }

	   function admin_index() {
		
		$this->layout = 'admin';
		$name = Inflector::singularize($this->name);
		$conditions = array();

		if($this->Session->read('user.User.type')=='Client Admin')
		{
			$this->$name->recursive = 0;
			$conditions[] = "`". $name. "`.`id` = '". $this->Session->read('user.User.client_id'). "'";
			$this->set(Inflector::underscore($this->name), $this->paginate($name, $conditions));
		}
		elseif($this->Session->read('user.User.type')=='Country Admin')
		{
			$this->$name->recursive = 0;
			$conditions[] = "`". $name. "`.`country` = '". $this->Session->read('client.Client.country'). "'";
			$this->set(Inflector::underscore($this->name), $this->paginate($name, $conditions));
		}
		else
		{
			if (isset($this->params['url']['data'])) {
				$this->Session->write($name.'.search', $this->params['url']['data']);
			}

			if ($this->Session->check($name.'.search')) {
				$this->request->data = $this->Session->read($name.'.search');
				if  (!empty($this->request->data[$name]['search'])) {
					$conditions[] = "
						(`". $name. "`.`name` LIKE  '%". $this->request->data[$name]['search']. "%'
						OR `". $name. "`.`id` =  '". $this->request->data[$name]['search']. "'
						OR `". $name. "`.`description` LIKE  '%". $this->request->data[$name]['search']. "%')
					";
				}
				if  (!empty($this->request->data[$name]['program_id'])) {
					$conditions[] = "`". $name. "`.`program_id` = '". $this->request->data[$name]['program_id']. "'";
				}
			}

			$this->$name->recursive = 0;
			$this->set(Inflector::underscore($this->name), $this->paginate($name, $conditions));
		}

		$this->set('programs', $this->Client->Program->find('list'));
	}

	function admin_edit($id = null) {
   
		$this->layout = 'admin';

		if (!empty($this->request->data)) {
			
			
				if(isset($_POST['copy']))			
			{   
			
		//	echo "<pre>";
			 $this->request->data = $this->Client->read(null,$id);
			 
			$this->set('cid',$this->request->data['Client']['id']);
			 unset($this->request->data['Client']['id']);
			 unset($this->request->data['Client']['name']);
			 
			
			//print_r($this->request->data);
			 
					}else{
						
								// trim the special offer numbers down
			if (isset($this->request->data['SpecialProduct']['SpecialProduct'])) {
				foreach ($this->request->data['SpecialProduct']['SpecialProduct'] as $key => $value) {
					if ($lastchar = strpos($value, ' ')) {
						//echo $lastchar;
						$this->request->data['SpecialProduct']['SpecialProduct'][$key] = substr($value, 0, $lastchar);
					}
				}
			}
				CakeLog::write('debug-SP-', 'myArray'.print_r($this->request->data['SpecialProduct']['SpecialProduct'], true) );

			//	trim the whats new offer numbers down
			foreach ($this->request->data['WhatsNewProduct']['WhatsNewProduct'] as $key => $value) {
				$value = trim($value);
				if (strpos($value, ' ') !== false) {
					$this->request->data['WhatsNewProduct']['WhatsNewProduct'][$key] = substr($value, 0, strpos($value, ' '));
				}
			}

			//	get the image extensions
			foreach ($this->request->data['Image'] as $key => $file) {
				if ($file['name'] != '') {
					$extension = substr($file['name'], strrpos($file['name'], '.') + 1);
					$this->request->data['Client'][$key. '_extension'] = $extension;
					$this->request->data['Image'][$key]['extension'] = $extension;
				}
			}
			if ($this->request->data['ClientImage1']['file']['name'] != '') {
				$extension = substr($this->request->data['ClientImage1']['file']['name'], strrpos($this->request->data['ClientImage1']['file']['name'], '.') + 1);
				$this->request->data['Client']['mobile_membership_card_extension'] = $extension;
			}


			//	get the client details (used to delete images and in the log)
			$this->{Inflector::singularize($this->name)}->unbindModel(array('hasAndBelongsToMany' => array('Product'), 'hasMany' => array('ClientPage', 'User')));
			$client = $this->{Inflector::singularize($this->name)}->findById($id);

			//	check if the clients domain is changing, if so check for existing users in that domain
			$existing_users = false;
			if ($id && $this->request->data['Client']['domain_id'] != $client['Client']['domain_id']) {
				$sql = "SELECT User.username FROM
							users AS User
							WHERE User.domain_id = '". $this->request->data['Client']['domain_id']. "'
							AND User.username IN (
								SELECT username FROM users where client_id = '". $id. "'
							)";

				$existing_users = $this->Client->query($sql);
				$this->set('existing_users', $existing_users);
			}

			//get categories for the client before modifying catgeories list
			$beforecats = array();
			if(!empty($id))
			{
				$cat1 = $this->Client->ClientsCategory->find('all',array('conditions'=>array('client_id'=>$id),'fields'=>array('category_id')));
				foreach($cat1 as $c1)
				{
					array_push($beforecats,$c1['ClientsCategory']['category_id']);
				}

				$block = $this->Client->find('all',array('conditions'=>array('id'=>$id),'fields'=>array('blacklisted_product','program_id'),'recursive'=>-1));
			}
			if(empty($this->request->data['Client']['special_offer_module_limit_points'])){
				unset($this->request->data['Client']['special_offer_module_limit_points']);
			}
			if(empty($this->request->data['Client']['dashboard'])){
				unset($this->request->data['Client']['dashboard']);
			}

			if (empty($existing_users) && $this->Client->save($this->request->data['Client'])) {
				$clientId = $this->Client->id;
				
				
				
				  //copy client_products
				    $c_copy_id = $this->data['cid'];
					if($id==""&&$c_copy_id!="")	{	
					
		$cpdata = $this->ClientsProduct->find('list',array('fields'=>'product_id','conditions'=>array('client_id'=>$c_copy_id)));
		$cpsfdata = $this->ClientsProductsSaf->find('list',array('fields'=>'product_id','conditions'=>array('client_id'=>$c_copy_id)));
		$cppsfdata = $this->ClientsProductsPoint->find('list',array('fields'=>'product_id','conditions'=>array('client_id'=>$c_copy_id)));
		//$cpages= $this->ClientPage->find('list',array('fields'=>'product_id','conditions'=>array('client_id'=>$c_copy_id)));
		
					foreach($cpdata as $data ){
					$prid =	$data;
				    $qry = "insert into clients_products(client_id,product_id)values($clientId,$prid) ";
		        	$this->ClientsProduct->query($qry);
						}
						
					foreach($cpsfdata as $data ){
					$sprid =	$data;
				    $qry1 = "insert into clients_products_safs(client_id,product_id)values($clientId,$sprid) ";
		        	$this->ClientsProductsSaf->query($qry1);
						}	
						
						
					foreach($cppsfdata as $data ){
					$pprid =	$data;
				    $qry2 = "insert into  clients_products_points(client_id,product_id)values($clientId,$pprid) ";
		        	$this->ClientsProductsPoint->query($qry2);
						}	
						
					
			//$cpages= $this->ClientPage->find('list',array('fields'=>'product_id','conditions'=>array('client_id'=>$c_copy_id)));
		   $page = "select * from client_pages where client_id=$c_copy_id";
           $cpages = $this->ClientPage->query($page);
           $this->set('cpages',$cpages);
	

						
					foreach($cpages as $data){
					
					$page_id=	$data["client_pages"]["page_id"];
					$enabled_override=	$data["client_pages"]["enabled_override"];
					$enabled=	$data["client_pages"]["enabled"];
					$title_override =	$data["client_pages"]["title_override"];
					$title=	$data["client_pages"]["title"];
					
					$h1_override 	=	$data["client_pages"]["h1_override 	"];
					$h1=	$data["client_pages"]["h1"]; 	
					$h1=filter_var($h1, FILTER_SANITIZE_STRING);			
					$h2_override=	$data["client_pages"]["h2_override"];
					$h2=	$data["client_pages"]["h2"];
					$h3_override=	$data["client_pages"]["h3_override"];
					$h3=	$data["client_pages"]["h3"];
					$page_title_override=	$data["client_pages"]["page_title_override"];
				
					$page_title=	$data["client_pages"]["page_title"];
					$page_title=filter_var($page_title, FILTER_SANITIZE_STRING);
					$meta_description_override=	$data["client_pages"]["meta_description_override"];
					$meta_description=	$data["client_pages"]["meta_description"];
					$meta_description=	$data["client_pages"]["meta_description"];
					$meta_keywords=	$data["client_pages"]["meta_keywords"];
					$meta_robots_override=	$data["client_pages"]["meta_robots_override"];
					$meta_robots=	$data["client_pages"]["meta_robots"];
					$content_override=	$data["client_pages"]["content_override"];
				    $content=	$data["client_pages"]["content"];
					$content=filter_var($content, FILTER_SANITIZE_STRING);
					$special_offers_override=	$data["client_pages"]["special_offers_override"];
					$special_offers=	$data["client_pages"]["special_offers"];
					$whats_new_module_override=	$data["client_pages"]["whats_new_module_override"];
					$whats_new_module=	$data["client_pages"]["whats_new_module"];
					$dashboard_id=	$data["client_pages"]["dashboard_id"];
				
				   $qry1 = 'insert into client_pages(client_id,page_id,enabled_override,enabled,title_override,h1_override,h1,h2_override,h2,h3_override,h3,page_title_override,page_title,meta_description_override,meta_description,meta_keywords,meta_robots_override,meta_robots,content_override,content,special_offers_override,special_offers,whats_new_module_override,whats_new_module,dashboard_id)values("'.$clientId.'","'.$page_id.'","'.$enabled_override.'","'.$enabled.'","'.$title_override.'","'.$h1_override.'","'.$h1.'","'.$h2_override.'","'.$h2.'","'.$h3_override.'","'.$h3.'","'.$page_title_override.'","'.$page_title.'","'.$meta_description_override.'","'.$meta_description.'","'.$meta_keywords.'","'.$meta_robots_override.'","'.$meta_robots.'","'.$content_override.'","'.$content.'","'.$special_offers_override.'","'.$special_offers.'","'.$whats_new_module_override.'","'.$whats_new_module.'","'.$dashboard_id.'") ';  
				
					
		$this->ClientPage->query($qry1);
					
		
	
					
						}		
		
					
						
					}
				
				
				
				//Saving Countries
				$this->Client->ClientsCountry->deleteAll(array('client_id'=>$clientId));
				$clientCountryData = array();
				foreach($this->request->data['Country']['Country'] as $clientCountry){if(!empty($clientCountry)){
					$clientCountryData[] = array('client_id'=>$clientId, 'country_id'=>$clientCountry);
				}}
				if(!empty($clientCountryData)){$this->Client->ClientsCountry->saveMany($clientCountryData);}

				//Saving dashboards
				$this->Client->ClientsDashboard->deleteAll(array('client_id'=>$clientId));
				$clientDBData = array();
				foreach($this->request->data['Dashboard']['Dashboard'] as $clientDB){
					$clientDBData[] = array('client_id'=>$clientId, 'dashboard_id'=>$clientDB);
				}
				if(!empty($clientDBData)){$this->Client->ClientsDashboard->saveMany($clientDBData);}

				//Saving categories
				$this->Client->ClientsCategory->deleteAll(array('client_id'=>$clientId));
				$clientCategoryData = array();
				foreach($this->request->data['Category']['Category'] as $clientCategory){if(!empty($clientCategory)){
					$clientCategoryData[] = array('client_id'=>$clientId, 'category_id'=>$clientCategory);
				}}
				if(!empty($clientCategoryData)){$this->Client->ClientsCategory->saveMany($clientCategoryData);}

				//Saving special products
				$this->Client->ClientsSpecialProduct->deleteAll(array('client_id'=>$clientId));
				$clientSepPrdtData = array();
				foreach($this->request->data['SpecialProduct']['SpecialProduct'] as $clientSepPrdt){
					if(!empty($clientSepPrdt)){
						$clientSepPrdtData[] = array('client_id'=>$clientId, 'product_id'=>$clientSepPrdt, 'dashboard_id'=>1);
					}
				}

				foreach($this->request->data['SpecialProduct']['SpecialProduct_2'] as $clientSepPrdt){
					if(!empty($clientSepPrdt)){
						$clientSepPrdtData[] = array('client_id'=>$clientId, 'product_id'=>$clientSepPrdt, 'dashboard_id'=>2);
					}
				}

				foreach($this->request->data['SpecialProduct']['SpecialProduct_3'] as $clientSepPrdt){
					if(!empty($clientSepPrdt)){
						$clientSepPrdtData[] = array('client_id'=>$clientId, 'product_id'=>$clientSepPrdt, 'dashboard_id'=>3);
					}
				}
				CakeLog::write('debug-all-', 'myArray'.print_r($clientSepPrdtData, true) );

				if(!empty($clientSepPrdtData)){$this->Client->ClientsSpecialProduct->saveMany($clientSepPrdtData);}

			//insert paypal details in paypal_details table
			$paypal_details = $this->PaypalDetail->find('first',array('conditions'=>array('PaypalDetail.client_id'=>$this->Client->id)));
			if(empty($paypal_details))
			{
				$this->request->data['PaypalDetail']['client_id'] = $this->Client->id;
				$this->request->data['PaypalDetail']['program_id'] = $this->request->data['Client']['program_id'];
				$this->request->data['PaypalDetail']['paypal_business_name'] = $this->request->data['Client']['paypal_business_name'];
				$this->request->data['PaypalDetail']['paypal_currency_code'] = $this->request->data['Client']['paypal_currency_code'];
				$this->request->data['PaypalDetail']['user_id'] = $this->Session->read('user.User.id');
				$this->request->data['PaypalDetail']['created'] = date('Y-m-d h:i:s');
				$this->PaypalDetail->save($this->request->data);
			}
			else
			{
				$sql = "UPDATE paypal_details as PaypalDetail set PaypalDetail.program_id=".$this->request->data['Client']['program_id'].",
				PaypalDetail.paypal_business_name='".$this->request->data['Client']['paypal_business_name']."', paypal_currency_code ='".$this->request->data['Client']['paypal_currency_code']."',
				user_id=". $this->Session->read('user.User.id').", created='".date('Y-m-d h:i:s')."' WHERE PaypalDetail.client_id=".$this->Client->id;
				$this->PaypalDetail->query($sql);
			}

			//	save the images
				$new_images = array();

				foreach ($this->request->data['Image'] as $key => $file)
				{
					if ($file['name'] != '')
					{
						if ($key == 'newsletter_banner_image_1')
						{
							$new_images['newsletter_banner_image_1'] = $this->request->data['Image']['newsletter_banner_image_1'];
						}
						if ($key == 'newsletter_banner_image_2')
						{
							$new_images['newsletter_banner_image_2'] = $this->request->data['Image']['newsletter_banner_image_2'];
						}

						if ($key == 'newsletter_banner_image_3')
						{
							$new_images['newsletter_banner_image_3'] = $this->request->data['Image']['newsletter_banner_image_3'];
						}
						else
						{
							foreach (glob(FILES_PATH. $key. '/'. $this->Client->id. '.*') as $filename)
							{
								unlink($filename);
							}
							move_uploaded_file($file['tmp_name'], FILES_PATH. $key. '/'. $this->Client->id. '.'. $this->request->data['Client'][$key. '_extension']);
						}
					}
	 			}


				// delete unwanted images
				foreach ($this->request->data['ImageDelete'] as $key => $value) {
					if ($value == '1') {
						foreach (glob(FILES_PATH. $key. '/'. $this->Client->id. '.*') as $filename) {
							unlink($filename);
							if($key==banner_background_image_left || $key==banner_background_image_right)
							{
								$DashboardId=1;
							}
							if($key==banner_background_image_left_saf || $key==banner_background_image_right_saf)
							{
								$DashboardId=2;

							}
							if($key==banner_background_image_left_points || $key==banner_background_image_right_points)
							{
								$DashboardId=3;
							}
							if($key== 'client_background_images_left' || $key=='client_background_images_right')
							{
								$DashboardId=1;
							}
							if($key=='client_background_images_left_2' || $key=='client_background_images_right_2')
							{
								$DashboardId=2;

							}
							if($key=='client_background_images_left_3' || $key=='client_background_images_right_3')
							{
								$DashboardId=3;
							}
							$columnname=explode("_",$key);
							$column=$columnname[0].'_'.$columnname[1].'_'.$columnname[2].'_'.$columnname[3].'_extension';
							$sql = "UPDATE client_banners SET $column='' WHERE dashboard_id='$DashboardId' and client_id='$id'";
							$this->ClientBanner->query($sql);
						}
					}
				}


				// delete unwanted images - the new way

				if (isset($this->request->data['ImageDelete'])) {
					$delete_images = array();
					foreach ($this->request->data['ImageDelete'] as $key => $value) {
						if ($key == 'newsletter_banner_image_1' && $value == '1') {
							$this->File->deleteByPrefix(strtolower($this->name). '/'. $key. '/'.$this->Client->id. '_');
							$sql = "UPDATE client_banners SET newsletter_banner_image='' WHERE dashboard_id=1 and client_id='$id'";
							$this->ClientBanner->query($sql);
						}
					}
				}
				// delete unwanted images - dashboard related
				if (isset($this->request->data['ImageDelete']))
				{
					$delete_images = array();
					foreach ($this->request->data['ImageDelete'] as $key => $value)
					{
						if ($key == 'newsletter_banner_image_2' && $value == '1')
						{
							$this->File->deleteByPrefix(strtolower($this->name). '/'. $key. '/'.$this->Client->id. '_');
							$sql = "UPDATE client_banners SET newsletter_banner_image='' WHERE dashboard_id=2 and client_id='$id'";
							$this->ClientBanner->query($sql);
						}
					}
				}
				// delete unwanted images - dashboard related
				if (isset($this->request->data['ImageDelete']))
				{
					$delete_images = array();
					foreach ($this->request->data['ImageDelete'] as $key => $value)
					{
						if ($key == 'newsletter_banner_image_3' && $value == '1')
						{
							$this->File->deleteByPrefix(strtolower($this->name). '/'. $key. '/'.$this->Client->id. '_');
							$sql = "UPDATE client_banners SET newsletter_banner_image='' WHERE dashboard_id=3 and client_id='$id'";
							$this->ClientBanner->query($sql);
						}
					}
}
				// delete unwanted images
				if (isset($this->request->data['ClientImage1Delete'])) {
					$delete_images = array();
					foreach ($this->request->data['ClientImage1Delete'] as $key => $value) {
						if ($value == '1') {
							$this->File->deleteByPrefix('mobile_membership_card/'.$this->Client->id);
							$this->request->data['Client']['mobile_membership_card_extension']='';
						}
					}
				}


				// save the images - the new way
				$images = $this->File->saveImages($new_images, strtolower($this->name), $this->Client->id. '_');
				foreach ($images as $key => $image) {
					$this->request->data['Client'][$key] = $image['filename'];
				}

				if ($this->request->data['ClientImage1']['file']['name'] != '') {
					move_uploaded_file($this->request->data['ClientImage1']['file']['tmp_name'], CLIENT_MEMBERSHIP_CARD_IMAGE_PATH. $this->Client->id. '.'. $this->request->data['Client']['mobile_membership_card_extension']);
				}

				// Update styles to client dashboard table

				if(!empty($this->request->data['Client']['style_id1']))
				{
					$styleid=$this->request->data['Client']['style_id1'];
					$id=$this->request->data['Client']['id'];
					$sql = "UPDATE clients_dashboards SET style_id='$styleid' WHERE dashboard_ID='1' and client_id='$id'";
					$this->Client->query($sql);
				}
				if(!empty($this->request->data['Client']['style_id2']))
				{
					$styleid=$this->request->data['Client']['style_id2'];
					$id=$this->request->data['Client']['id'];
					$sql = "UPDATE clients_dashboards SET style_id='$styleid' WHERE dashboard_ID='2' and client_id='$id'";
					$this->Client->query($sql);
				}
				if(!empty($this->request->data['Client']['style_id3']))
				{
					$styleid=$this->request->data['Client']['style_id3'];
					$id=$this->request->data['Client']['id'];
					$sql = "UPDATE clients_dashboards SET style_id='$styleid' WHERE dashboard_ID='3' and client_id='$id'";
					$this->Client->query($sql);
                }


				//	move all the clients users to the new domain
				$sql = "UPDATE users AS User
							SET User.domain_id = '". $this->request->data['Client']['domain_id']. "'
							WHERE User.client_id = '". $this->request->data['Client']['id']. "'";
				$this->Client->query($sql);


				//	log the change
				$log = array(
					'id' => null,
					'foreign_id' => $this->{Inflector::singularize($this->name)}->id,
					'foreign_model' => Inflector::singularize($this->name),
					'user_id' => $this->Session->read('user.User.id'),
					'old_data' => $client,
					'new_data' => $this->request->data
				);
				if (empty($client)) {
					$log['description'] = Inflector::singularize($this->name). ' created';
				} else {
					$log['description'] = Inflector::singularize($this->name). ' modified';
				}

				if (isset($this->request->data['Log']['notes'])) {
					$log['notes'] = $this->request->data['Log']['notes'];
				} else {
					$log['notes'] = ''; /* Insert complains if notes is not defined*/
				}

				//	add the related changes
				foreach ($this->{Inflector::singularize($this->name)}->getAssociated('hasAndBelongsToMany') as $model)  {
					switch ($model) {		//	fields to add to the comments display
						case 'Module':
							$display = 'title';
							break;
						case 'Category' :
							$display = 'description';
						default:
							$display = 'name';
					}

					$tmp_old_related = array();
					//	deleted
					if (isset($client[$model]) && isset($this->request->data[$model][$model])) {
						foreach ($client[$model] as $model_data) {
							if (!in_array($model_data['id'], $this->request->data[$model][$model])) {
								$log['old_data'][$this->name. $model][Inflector::humanize(Inflector::underscore($model)). ': '. $model_data[$display]] = '1';
								$log['new_data'][$this->name. $model][Inflector::humanize(Inflector::underscore($model)). ': '. $model_data[$display]] = '0';
							}
							$tmp_old_related[] = $model_data['id'];
						}
						//	added
						foreach ($this->request->data[$model][$model] as $model_id) {
							if ($model_id != '' && !in_array($model_id, $tmp_old_related)) {
								$this->{Inflector::singularize($this->name)}->{$model}->recursive = -1;
								$model_data = $this->{Inflector::singularize($this->name)}->{$model}->findById($model_id);
								$log['old_data'][$this->name. $model][Inflector::humanize(Inflector::underscore($model)). ': '. $model_data[$model][$display]] = '0';
								$log['new_data'][$this->name. $model][Inflector::humanize(Inflector::underscore($model)). ': '. $model_data[$model][$display]] = '1';
							}
						}
					}
				}

				$this->save_log($log);

					if(!empty($this->request->data['Image']['newsletter_banner_image_1']['tmp_name']))
						{
							$clientbanner1 = $this->Client->ClientBanner->find('first', array('conditions' => array('ClientBanner.client_id' => $this->request->data['Client']['id'],'ClientBanner.dashboard_id' => 1), 'fields' => array('ClientBanner.id')));
							$bannerid1 = empty($clientbanner1)?0:$clientbanner1['ClientBanner']['id'];
							$this->request->data['ClientBanner']['newsletter_banner_image'] = $images['newsletter_banner_image_1']['filename'];
							$this->request->data['ClientBanner']['id'] = $bannerid1;
							$this->request->data['ClientBanner']['dashboard_id'] = 1;
							$this->Client->ClientBanner->save($this->request->data);
						}
					if(!empty($this->request->data['Image']['newsletter_banner_image_2']['tmp_name']))
						{
							$clientbanner2 = $this->Client->ClientBanner->find('first', array('conditions' => array('ClientBanner.client_id' => $this->request->data['Client']['id'],'ClientBanner.dashboard_id' => 2), 'fields' => array('ClientBanner.id')));
							$bannerid2 = empty($clientbanner2)?0:$clientbanner2['ClientBanner']['id'];
							$this->request->data['ClientBanner']['newsletter_banner_image'] = $images['newsletter_banner_image_2']['filename'];
							$this->request->data['ClientBanner']['id'] = $bannerid2;
							$this->request->data['ClientBanner']['dashboard_id'] = 2;
							$this->Client->ClientBanner->save($this->request->data);
						}
					if(!empty($this->request->data['Image']['newsletter_banner_image_3']['tmp_name']))
						{
							$clientbanner3 = $this->Client->ClientBanner->find('first', array('conditions' => array('ClientBanner.client_id' => $this->request->data['Client']['id'],'ClientBanner.dashboard_id' => 3), 'fields' => array('ClientBanner.id')));
							$bannerid3 = empty($clientbanner3)?0:$clientbanner3['ClientBanner']['id'];
							$this->request->data['ClientBanner']['newsletter_banner_image'] = $images['newsletter_banner_image_3']['filename'];
							$this->request->data['ClientBanner']['id'] = $bannerid3;
							$this->request->data['ClientBanner']['dashboard_id'] = 3;
							$this->Client->ClientBanner->save($this->request->data);
						}

					//storing the client related headerbanners and newsletterbanners for "myrewards" dashboard in client_banner table


					if(!empty($this->request->data['Image']['banner_background_image_left_1']['name']) || !empty($this->request->data['Image']['banner_background_image_right_1']['name']) )
					{
						$clientbanner = $this->Client->ClientBanner->find('first', array('conditions' => array('ClientBanner.client_id' => $this->request->data['Client']['id'],'ClientBanner.dashboard_id' => 1), 'fields' => array('ClientBanner.id,ClientBanner.newsletter_banner_image')));
						$bannerid = empty($clientbanner)?0:$clientbanner['ClientBanner']['id'];
						$backgrnd_image_left_extension=pathinfo($this->request->data['Image']['banner_background_image_left_1']['name'], PATHINFO_EXTENSION);
						$backgrnd_image_right_extension=pathinfo($this->request->data['Image']['banner_background_image_right_1']['name'], PATHINFO_EXTENSION);
						$client_id=empty($this->request->data['Client']['id'])?$this->Client->getLastInsertId():$this->request->data['Client']['id'];
						$this->request->data['ClientBanner']['id'] = $bannerid;
						$this->request->data['ClientBanner']['client_id'] = $client_id;
						$this->request->data['ClientBanner']['banner_background_image_right_extension'] = $backgrnd_image_right_extension;
						$this->request->data['ClientBanner']['banner_background_image_left_extension'] = $backgrnd_image_left_extension;
						$this->request->data['ClientBanner']['newsletter_banner_image'] = empty($clientbanner)?'':$clientbanner['ClientBanner']['newsletter_banner_image'];
						$this->request->data['ClientBanner']['dashboard_id'] = 1;
						$this->Client->ClientBanner->save($this->request->data);
						if ($backgrnd_image_left_extension != '')
						{
							move_uploaded_file($this->request->data['Image']['banner_background_image_left_1']['tmp_name'], BANNER_BACKGROUND_IMAGE_LEFT_PATH. $this->Client->id. '.'.$backgrnd_image_left_extension);
						}
						//upload the images into files/banner_background_image_right folder
						if ($backgrnd_image_right_extension != '')
						{
							move_uploaded_file($this->request->data['Image']['banner_background_image_right_1']['tmp_name'], BANNER_BACKGROUND_IMAGE_RIGHT_PATH. $this->Client->id. '.'. $backgrnd_image_right_extension);
						}

					}

					//storing the client related headerbanners and newsletterbanners for "send a friend" dashboard in client_banner table


					if(!empty($this->request->data['Image']['banner_background_image_left_2']['name']) || !empty($this->request->data['Image']['banner_background_image_right_2']['name']))
					{
						$clientbanner = $this->Client->ClientBanner->find('first', array('conditions' => array('ClientBanner.client_id' => $this->request->data['Client']['id'],'ClientBanner.dashboard_id' => 2), 'fields' => array('ClientBanner.id,ClientBanner.newsletter_banner_image')));
						$bannerid = empty($clientbanner)?0:$clientbanner['ClientBanner']['id'];
						$backgrnd_image_left_extension=pathinfo($this->request->data['Image']['banner_background_image_left_2']['name'], PATHINFO_EXTENSION);
						$backgrnd_image_right_extension=pathinfo($this->request->data['Image']['banner_background_image_right_2']['name'], PATHINFO_EXTENSION);
						$client_id=empty($this->request->data['Client']['id'])?$this->Client->getLastInsertId():$this->request->data['Client']['id'];
						$this->request->data['ClientBanner']['id'] = $bannerid;
						$this->request->data['ClientBanner']['client_id'] = $client_id;
						$this->request->data['ClientBanner']['banner_background_image_right_extension'] = $backgrnd_image_right_extension;
						$this->request->data['ClientBanner']['banner_background_image_left_extension'] = $backgrnd_image_left_extension;
						$this->request->data['ClientBanner']['newsletter_banner_image'] = empty($clientbanner)?'':$clientbanner['ClientBanner']['newsletter_banner_image'];
						$this->request->data['ClientBanner']['dashboard_id'] = 2;
						$this->Client->ClientBanner->save($this->request->data);
						//upload the images into files/banner_background_image_left folder
						if ($backgrnd_image_left_extension != '')
						{
							move_uploaded_file($this->request->data['Image']['banner_background_image_left_2']['tmp_name'], BANNER_BACKGROUND_IMAGE_LEFT_SAF_PATH. $this->Client->id. '.'.$backgrnd_image_left_extension);
						}
						//upload the images into files/banner_background_image_right folder
						if ($backgrnd_image_right_extension != '')
						{
							move_uploaded_file($this->request->data['Image']['banner_background_image_right_2']['tmp_name'], BANNER_BACKGROUND_IMAGE_RIGHT_SAF_PATH. $this->Client->id. '.'. $backgrnd_image_right_extension);
						}
					}

					//storing the client related headerbanners and newsletterbanners for "points" dashboard in client_banner table

					if(!empty($this->request->data['Image']['banner_background_image_left_3']['name']) || !empty($this->request->data['Image']['banner_background_image_right_3']['name']))
					{
						$clientbanner = $this->Client->ClientBanner->find('first', array('conditions' => array('ClientBanner.client_id' => $this->request->data['Client']['id'],'ClientBanner.dashboard_id' => 3), 'fields' => array('ClientBanner.id,ClientBanner.newsletter_banner_image')));
						$bannerid = empty($clientbanner)?0:$clientbanner['ClientBanner']['id'];
						$backgrnd_image_left_extension=pathinfo($this->request->data['Image']['banner_background_image_left_3']['name'], PATHINFO_EXTENSION);
						$backgrnd_image_right_extension=pathinfo($this->request->data['Image']['banner_background_image_right_3']['name'], PATHINFO_EXTENSION);
						$client_id=empty($this->request->data['Client']['id'])?$this->Client->getLastInsertId():$this->request->data['Client']['id'];
						$this->request->data['ClientBanner']['id'] = $bannerid;
						$this->request->data['ClientBanner']['client_id'] = $client_id;
						$this->request->data['ClientBanner']['banner_background_image_right_extension'] = $backgrnd_image_right_extension;
						$this->request->data['ClientBanner']['banner_background_image_left_extension'] = $backgrnd_image_left_extension;
						$this->request->data['ClientBanner']['newsletter_banner_image'] = empty($clientbanner)?'':$clientbanner['ClientBanner']['newsletter_banner_image'];
						$this->request->data['ClientBanner']['dashboard_id'] = 3;
						$this->Client->ClientBanner->save($this->request->data);
						if ($backgrnd_image_left_extension != '')
						{
							move_uploaded_file($this->request->data['Image']['banner_background_image_left_3']['tmp_name'], BANNER_BACKGROUND_IMAGE_LEFT_POINTS_PATH. $this->Client->id. '.'.$backgrnd_image_left_extension);
						}
						//upload the images into files/banner_background_image_right folder
						if ($backgrnd_image_right_extension != '')
						{
							move_uploaded_file($this->request->data['Image']['banner_background_image_right_3']['tmp_name'], BANNER_BACKGROUND_IMAGE_RIGHT_POINTS_PATH. $this->Client->id. '.'. $backgrnd_image_right_extension);
						}
					}
					if(!empty($this->request->data['Client']['banner_option_1']) || !empty($this->request->data['Client']['banner_background_color_1']) || !empty($this->request->data['Client']['banner_left_align_1']) || !empty($this->request->data['Client']['banner_right_align_1']) || !empty($this->request->data['Client']['banner_left_repeat_1']) || !empty($this->request->data['Client']['banner_right_repeat_1']) )
					{

						$client_id=empty($this->request->data['Client']['id'])?$this->Client->getLastInsertId():$this->request->data['Client']['id'];
						$clientbanner = $this->Client->ClientBanner->find('first', array('conditions' => array('ClientBanner.client_id' => $client_id,'ClientBanner.dashboard_id' => 1), 'fields' => array('ClientBanner.id, ClientBanner.banner_background_image_left_extension, ClientBanner.banner_background_image_right_extension,ClientBanner.newsletter_banner_image')));
						$bannerid = empty($clientbanner)?0:$clientbanner['ClientBanner']['id'];


						$this->request->data['ClientBanner']['id'] = $bannerid;
						$this->request->data['ClientBanner']['client_id'] = $client_id;
						$this->request->data['ClientBanner']['dashboard_id'] = 1;
						$this->request->data['ClientBanner']['banner_option'] = $this->request->data['Client']['banner_option_1'];
						$this->request->data['ClientBanner']['banner_background_color'] = $this->request->data['Client']['banner_background_color_1'];
						$this->request->data['ClientBanner']['banner_left_align'] = $this->request->data['Client']['banner_left_align_1'];
						$this->request->data['ClientBanner']['banner_right_align'] = $this->request->data['Client']['banner_right_align_1'];
						$this->request->data['ClientBanner']['banner_left_repeat'] = $this->request->data['Client']['banner_left_repeat_1'];
						$this->request->data['ClientBanner']['banner_right_repeat'] = $this->request->data['Client']['banner_right_repeat_1'];
						$this->request->data['ClientBanner']['banner_background_image_right_extension'] = empty($clientbanner)?'':$clientbanner['ClientBanner']['banner_background_image_right_extension'];
						$this->request->data['ClientBanner']['banner_background_image_left_extension'] = empty($clientbanner)?'':$clientbanner['ClientBanner']['banner_background_image_left_extension'];
						$this->request->data['ClientBanner']['newsletter_banner_image'] = empty($clientbanner)?'':$clientbanner['ClientBanner']['newsletter_banner_image'];
						$this->Client->ClientBanner->save($this->request->data['ClientBanner']);

					}
					if(!empty($this->request->data['Client']['banner_option_2']) || !empty($this->request->data['Client']['banner_background_color_2']) || !empty($this->request->data['Client']['banner_left_align_2']) || !empty($this->request->data['Client']['banner_right_align_2']) || !empty($this->request->data['Client']['banner_left_repeat_2']) || !empty($this->request->data['Client']['banner_right_repeat_2']))
					{
						$client_id=empty($this->request->data['Client']['id'])?$this->Client->getLastInsertId():$this->request->data['Client']['id'];
						$clientbanner = $this->Client->ClientBanner->find('first', array('conditions' => array('ClientBanner.client_id' => $client_id,'ClientBanner.dashboard_id' => 2), 'fields' => array('ClientBanner.id, ClientBanner.banner_background_image_left_extension, ClientBanner.banner_background_image_right_extension,ClientBanner.newsletter_banner_image')));
						$bannerid = empty($clientbanner)?0:$clientbanner['ClientBanner']['id'];


						$this->request->data['ClientBanner']['id'] = $bannerid;
						$this->request->data['ClientBanner']['client_id'] = $client_id;
						$this->request->data['ClientBanner']['dashboard_id'] = 2;
						$this->request->data['ClientBanner']['banner_option'] = $this->request->data['Client']['banner_option_2'];
						$this->request->data['ClientBanner']['banner_background_color'] = $this->request->data['Client']['banner_background_color_2'];
						$this->request->data['ClientBanner']['banner_left_align'] = $this->request->data['Client']['banner_left_align_2'];
						$this->request->data['ClientBanner']['banner_right_align'] = $this->request->data['Client']['banner_right_align_2'];
						$this->request->data['ClientBanner']['banner_left_repeat'] = $this->request->data['Client']['banner_left_repeat_2'];
						$this->request->data['ClientBanner']['banner_right_repeat'] = $this->request->data['Client']['banner_right_repeat_2'];
						$this->request->data['ClientBanner']['banner_background_image_right_extension'] = empty($clientbanner)?'':$clientbanner['ClientBanner']['banner_background_image_right_extension'];
						$this->request->data['ClientBanner']['banner_background_image_left_extension'] = empty($clientbanner)?'':$clientbanner['ClientBanner']['banner_background_image_left_extension'];
						$this->request->data['ClientBanner']['newsletter_banner_image'] = empty($clientbanner)?'':$clientbanner['ClientBanner']['newsletter_banner_image'];
						$this->Client->ClientBanner->save($this->request->data['ClientBanner']);

					}
					if(!empty($this->request->data['Client']['banner_option_3']) || !empty($this->request->data['Client']['banner_background_color_3']) || !empty($this->request->data['Client']['banner_left_align_3']) || !empty($this->request->data['Client']['banner_right_align_3']) || !empty($this->request->data['Client']['banner_left_repeat_3']) || !empty($this->request->data['Client']['banner_right_repeat_3']))
					{
						$client_id=empty($this->request->data['Client']['id'])?$this->Client->getLastInsertId():$this->request->data['Client']['id'];
						$clientbanner = $this->Client->ClientBanner->find('first', array('conditions' => array('ClientBanner.client_id' => $client_id,'ClientBanner.dashboard_id' => 3), 'fields' => array('ClientBanner.id, ClientBanner.banner_background_image_left_extension, ClientBanner.banner_background_image_right_extension,ClientBanner.newsletter_banner_image')));
						$bannerid = empty($clientbanner)?0:$clientbanner['ClientBanner']['id'];


						$this->request->data['ClientBanner']['id'] = $bannerid;
						$this->request->data['ClientBanner']['client_id'] = $client_id;
						$this->request->data['ClientBanner']['dashboard_id'] = 3;
						$this->request->data['ClientBanner']['banner_option'] = $this->request->data['Client']['banner_option_3'];
						$this->request->data['ClientBanner']['banner_background_color'] = $this->request->data['Client']['banner_background_color_3'];
						$this->request->data['ClientBanner']['banner_left_align'] = $this->request->data['Client']['banner_left_align_3'];
						$this->request->data['ClientBanner']['banner_right_align'] = $this->request->data['Client']['banner_right_align_3'];
						$this->request->data['ClientBanner']['banner_left_repeat'] = $this->request->data['Client']['banner_left_repeat_3'];
						$this->request->data['ClientBanner']['banner_right_repeat'] = $this->request->data['Client']['banner_right_repeat_3'];
						$this->request->data['ClientBanner']['banner_background_image_right_extension'] = empty($clientbanner)?'':$clientbanner['ClientBanner']['banner_background_image_right_extension'];
						$this->request->data['ClientBanner']['banner_background_image_left_extension'] = empty($clientbanner)?'':$clientbanner['ClientBanner']['banner_background_image_left_extension'];
						$this->request->data['ClientBanner']['newsletter_banner_image'] = empty($clientbanner)?'':$clientbanner['ClientBanner']['newsletter_banner_image'];
						$this->Client->ClientBanner->save($this->request->data['ClientBanner']);

					}

					//background images for myrewards
										if(!empty($this->request->data['ClientBanner']['left_link_1']) || !empty($this->request->data['ClientBanner']['right_link_1']) || !empty($this->request->data['Image']['client_background_images_left_1']['name']) || !empty($this->request->data['Image']['client_background_images_right_1']['name']))
										{
											$client_id=empty($this->request->data['Client']['id'])?$this->Client->getLastInsertId():$this->request->data['Client']['id'];
											$clientbanner = $this->Client->ClientBanner->find('first',
														array('conditions'=>array('ClientBanner.client_id' => $client_id, 'ClientBanner.dashboard_id' => 1),
															  'fields' => array('ClientBanner.id, ClientBanner.banner_background_image_left_extension,
																				 ClientBanner.banner_background_image_right_extension,ClientBanner.newsletter_banner_image,
																				 ClientBanner.client_background_images_left_extension,
																				 ClientBanner.client_background_images_right_extension,
																				 ClientBanner.left_link, ClientBanner.right_link')));
											$bannerid = empty($clientbanner)?0:$clientbanner['ClientBanner']['id'];

											$this->request->data['ClientBanner']['id'] = $bannerid;
											$this->request->data['ClientBanner']['client_id'] = $client_id;
											$this->request->data['ClientBanner']['dashboard_id'] = 1;
											$this->request->data['ClientBanner']['banner_option'] = $this->request->data['Client']['banner_option_1'];
											$this->request->data['ClientBanner']['banner_background_color'] = $this->request->data['Client']['banner_background_color_1'];
											$this->request->data['ClientBanner']['banner_left_align'] = $this->request->data['Client']['banner_left_align_1'];
											$this->request->data['ClientBanner']['banner_right_align'] = $this->request->data['Client']['banner_right_align_1'];
											$this->request->data['ClientBanner']['banner_left_repeat'] = $this->request->data['Client']['banner_left_repeat_1'];
											$this->request->data['ClientBanner']['banner_right_repeat'] = $this->request->data['Client']['banner_right_repeat_1'];
											$this->request->data['ClientBanner']['banner_background_image_right_extension'] = empty($clientbanner)?'':$clientbanner['ClientBanner']['banner_background_image_right_extension'];
											$this->request->data['ClientBanner']['banner_background_image_left_extension'] = empty($clientbanner)?'':$clientbanner['ClientBanner']['banner_background_image_left_extension'];
											$this->request->data['ClientBanner']['newsletter_banner_image'] = empty($clientbanner)?'':$clientbanner['ClientBanner']['newsletter_banner_image'];

											$extension_left=pathinfo($this->request->data['Image']['client_background_images_left_1']['name'], PATHINFO_EXTENSION);
											$extension_right=pathinfo($this->request->data['Image']['client_background_images_right_1']['name'], PATHINFO_EXTENSION);

											$this->request->data['ClientBanner']['client_background_images_left_extension'] = empty($extension_left)?$clientbanner['ClientBanner']['client_background_images_left_extension']:$extension_left;
											$this->request->data['ClientBanner']['client_background_images_right_extension'] = empty($extension_right)?$clientbanner['ClientBanner']['client_background_images_right_extension']:$extension_right;

											$this->request->data['ClientBanner']['left_link'] = empty($this->request->data['ClientBanner']['left_link_1'])?$clientbanner['ClientBanner']['left_link']:$this->request->data['ClientBanner']['left_link_1'];
											$this->request->data['ClientBanner']['right_link'] = empty($this->request->data['ClientBanner']['right_link_1'])?$clientbanner['ClientBanner']['right_link']:$this->request->data['ClientBanner']['right_link_1'];
											$this->request->data['ClientBanner']['modified'] = date('Y-m-d h:i:s');
											$this->Client->ClientBanner->save($this->request->data);

											if ($extension_left != '') {
												//$path = CLIENT_BACKGROUND_IMAGE_LEFT_PATH. $this->Client->id. '.'.$clientbanner['ClientBanner']['client_background_images_left_extension'];
												move_uploaded_file($this->request->data['Image']['client_background_images_left_1']['tmp_name'], CLIENT_BACKGROUND_IMAGE_LEFT_PATH. $this->Client->id. '.'.$extension_left);
											}

											if ($extension_right != '') {
												move_uploaded_file($this->request->data['Image']['client_background_images_right_1']['tmp_name'], CLIENT_BACKGROUND_IMAGE_RIGHT_PATH. $this->Client->id. '.'.$extension_right);
											}
										}

										//background images for send a friend
										if(!empty($this->request->data['ClientBanner']['left_link_2']) || !empty($this->request->data['ClientBanner']['right_link_2']) || !empty($this->request->data['Image']['client_background_images_left_2']['name']) || !empty($this->request->data['Image']['client_background_images_right_2']['name']))
										{
											$client_id=empty($this->request->data['Client']['id'])?$this->Client->getLastInsertId():$this->request->data['Client']['id'];
											$clientbanner = $this->Client->ClientBanner->find('first',
														array('conditions'=>array('ClientBanner.client_id' => $client_id, 'ClientBanner.dashboard_id' => 2),
															  'fields' => array('ClientBanner.id, ClientBanner.banner_background_image_left_extension,
																				 ClientBanner.banner_background_image_right_extension,ClientBanner.newsletter_banner_image,
																				 ClientBanner.client_background_images_left_extension,
																				 ClientBanner.client_background_images_right_extension,
																				 ClientBanner.left_link, ClientBanner.right_link')));
											$bannerid = empty($clientbanner)?0:$clientbanner['ClientBanner']['id'];

											$this->request->data['ClientBanner']['id'] = $bannerid;
											$this->request->data['ClientBanner']['client_id'] = $client_id;
											$this->request->data['ClientBanner']['dashboard_id'] = 2;
											$this->request->data['ClientBanner']['banner_option'] = $this->request->data['Client']['banner_option_2'];
											$this->request->data['ClientBanner']['banner_background_color'] = $this->request->data['Client']['banner_background_color_2'];
											$this->request->data['ClientBanner']['banner_left_align'] = $this->request->data['Client']['banner_left_align_2'];
											$this->request->data['ClientBanner']['banner_right_align'] = $this->request->data['Client']['banner_right_align_2'];
											$this->request->data['ClientBanner']['banner_left_repeat'] = $this->request->data['Client']['banner_left_repeat_2'];
											$this->request->data['ClientBanner']['banner_right_repeat'] = $this->request->data['Client']['banner_right_repeat_2'];
											$this->request->data['ClientBanner']['banner_background_image_right_extension'] = empty($clientbanner)?'':$clientbanner['ClientBanner']['banner_background_image_right_extension'];
											$this->request->data['ClientBanner']['banner_background_image_left_extension'] = empty($clientbanner)?'':$clientbanner['ClientBanner']['banner_background_image_left_extension'];
											$this->request->data['ClientBanner']['newsletter_banner_image'] = empty($clientbanner)?'':$clientbanner['ClientBanner']['newsletter_banner_image'];

											$extension_left=pathinfo($this->request->data['Image']['client_background_images_left_2']['name'], PATHINFO_EXTENSION);
											$extension_right=pathinfo($this->request->data['Image']['client_background_images_right_2']['name'], PATHINFO_EXTENSION);

											$this->request->data['ClientBanner']['client_background_images_left_extension'] = empty($extension_left)?$clientbanner['ClientBanner']['client_background_images_left_extension']:$extension_left;
											$this->request->data['ClientBanner']['client_background_images_right_extension'] = empty($extension_right)?$clientbanner['ClientBanner']['client_background_images_right_extension']:$extension_right;

											$this->request->data['ClientBanner']['left_link'] = empty($this->request->data['ClientBanner']['left_link_2'])?$clientbanner['ClientBanner']['left_link']:$this->request->data['ClientBanner']['left_link_2'];
											$this->request->data['ClientBanner']['right_link'] = empty($this->request->data['ClientBanner']['right_link_2'])?$clientbanner['ClientBanner']['right_link']:$this->request->data['ClientBanner']['right_link_2'];

											$this->request->data['ClientBanner']['modified'] = date('Y-m-d h:i:s');
											$this->Client->ClientBanner->save($this->request->data);

											if ($extension_left != '') {
												move_uploaded_file($this->request->data['Image']['client_background_images_left_2']['tmp_name'], CLIENT_BACKGROUND_IMAGE_LEFT_2_PATH. $this->Client->id. '.'.$extension_left);
											}

											if ($extension_right != '') {
												move_uploaded_file($this->request->data['Image']['client_background_images_right_2']['tmp_name'], CLIENT_BACKGROUND_IMAGE_RIGHT_2_PATH. $this->Client->id. '.'.$extension_right);
											}
										}

										//background images for points
										if(!empty($this->request->data['ClientBanner']['left_link_3']) || !empty($this->request->data['ClientBanner']['right_link_3']) || !empty($this->request->data['Image']['client_background_images_left_3']['name']) || !empty($this->request->data['Image']['client_background_images_right_3']['name']))
										{
											$client_id=empty($this->request->data['Client']['id'])?$this->Client->getLastInsertId():$this->request->data['Client']['id'];
											$clientbanner = $this->Client->ClientBanner->find('first',
														array('conditions'=>array('ClientBanner.client_id' => $client_id, 'ClientBanner.dashboard_id' => 3),
															  'fields' => array('ClientBanner.id, ClientBanner.banner_background_image_left_extension,
																				 ClientBanner.banner_background_image_right_extension,ClientBanner.newsletter_banner_image,
																				 ClientBanner.client_background_images_left_extension,
																				 ClientBanner.client_background_images_right_extension,
																				 ClientBanner.left_link, ClientBanner.right_link')));
											$bannerid = empty($clientbanner)?0:$clientbanner['ClientBanner']['id'];

											$this->request->data['ClientBanner']['id'] = $bannerid;
											$this->request->data['ClientBanner']['client_id'] = $client_id;
											$this->request->data['ClientBanner']['dashboard_id'] = 3;
											$this->request->data['ClientBanner']['banner_option'] = $this->request->data['Client']['banner_option_3'];
											$this->request->data['ClientBanner']['banner_background_color'] = $this->request->data['Client']['banner_background_color_3'];
											$this->request->data['ClientBanner']['banner_left_align'] = $this->request->data['Client']['banner_left_align_3'];
											$this->request->data['ClientBanner']['banner_right_align'] = $this->request->data['Client']['banner_right_align_3'];
											$this->request->data['ClientBanner']['banner_left_repeat'] = $this->request->data['Client']['banner_left_repeat_3'];
											$this->request->data['ClientBanner']['banner_right_repeat'] = $this->request->data['Client']['banner_right_repeat_3'];
											$this->request->data['ClientBanner']['banner_background_image_right_extension'] = empty($clientbanner)?'':$clientbanner['ClientBanner']['banner_background_image_right_extension'];
											$this->request->data['ClientBanner']['banner_background_image_left_extension'] = empty($clientbanner)?'':$clientbanner['ClientBanner']['banner_background_image_left_extension'];
											$this->request->data['ClientBanner']['newsletter_banner_image'] = empty($clientbanner)?'':$clientbanner['ClientBanner']['newsletter_banner_image'];

											$extension_left=pathinfo($this->request->data['Image']['client_background_images_left_3']['name'], PATHINFO_EXTENSION);
											$extension_right=pathinfo($this->request->data['Image']['client_background_images_right_3']['name'], PATHINFO_EXTENSION);

											$this->request->data['ClientBanner']['client_background_images_left_extension'] = empty($extension_left)?$clientbanner['ClientBanner']['client_background_images_left_extension']:$extension_left;
											$this->request->data['ClientBanner']['client_background_images_right_extension'] = empty($extension_right)?$clientbanner['ClientBanner']['client_background_images_right_extension']:$extension_right;

											$this->request->data['ClientBanner']['left_link'] = empty($this->request->data['ClientBanner']['left_link_3'])?$clientbanner['ClientBanner']['left_link']:$this->request->data['ClientBanner']['left_link_3'];
											$this->request->data['ClientBanner']['right_link'] = empty($this->request->data['ClientBanner']['right_link_3'])?$clientbanner['ClientBanner']['right_link']:$this->request->data['ClientBanner']['right_link_3'];

											$this->request->data['ClientBanner']['modified'] = date('Y-m-d h:i:s');
											$this->Client->ClientBanner->save($this->request->data);

											if ($extension_left != '') {
												move_uploaded_file($this->request->data['Image']['client_background_images_left_3']['tmp_name'], CLIENT_BACKGROUND_IMAGE_LEFT_3_PATH. $this->Client->id. '.'.$extension_left);
											}

											if ($extension_right != '') {
												move_uploaded_file($this->request->data['Image']['client_background_images_right_3']['tmp_name'], CLIENT_BACKGROUND_IMAGE_RIGHT_3_PATH. $this->Client->id. '.'.$extension_right);
											}
					}

				//inserting the special products of client into clients_special_products where dashboard_id=2
/*
						if (!empty($this->request->data['SpecialProduct']['SpecialProduct_2']))
						{
							$errors=array();
							foreach ($this->request->data['SpecialProduct']['SpecialProduct_2'] as $key => $value)
							 {
								if(!empty($value))
								{
									$specialproductid = $this->Client->ClientsSpecialProduct->find('first', array('conditions' => array('ClientsSpecialProduct.product_id' => $value,'ClientsSpecialProduct.dashboard_id' => 2), 'fields' => array('ClientsSpecialProduct.id')));
									$specialid = empty($specialproductid)?0:$specialproductid['ClientsSpecialProduct']['id'];
									$this->loadModel("ProductsSaf");
									$productssafid=$this->ProductsSaf->find('first',array('conditions'=>array('ProductsSaf.product_id'=>current(explode(" ", $value))), 'fields' => array('ProductsSaf.product_id')));
									if(!empty($productssafid))
									{
										$this->request->data['ClientsSpecialProduct']['id']=0;
										$this->request->data['ClientsSpecialProduct']['client_id']=$this->request->data['Client']['id'];
										$this->request->data['ClientsSpecialProduct']['product_id']=current(explode(" ", $value));
										$this->request->data['ClientsSpecialProduct']['dashboard_id']=2;
										$this->Client->ClientsSpecialProduct->save($this->request->data);
									}
									else
									{
										$errors[]= $value;
									}
								}
							 }
						}
*/
						//inserting the special products of client into clients_special_products where dashboard_id=3
/*
						if (!empty($this->request->data['SpecialProduct']['SpecialProduct_3']))
						{
							foreach ($this->request->data['SpecialProduct']['SpecialProduct_3'] as $key => $value)
							 {
								if(!empty($value))
								{
									$specialproductid = $this->Client->ClientsSpecialProduct->find('first', array('conditions' => array('ClientsSpecialProduct.product_id' => $value,'ClientsSpecialProduct.dashboard_id' => 2), 'fields' => array('ClientsSpecialProduct.id')));
									$specialid = empty($specialproductid)?0:$specialproductid['ClientsSpecialProduct']['id'];
									$this->loadModel("ProductsPoint");
									$productspointid=$this->ProductsPoint->find('first',array('conditions'=>array('ProductsPoint.product_id'=>current(explode(" ", $value))), 'fields' => array('ProductsPoint.product_id')));
									if(!empty($productspointid))
									{
										$this->request->data['ClientsSpecialProduct']['id']=0;
										$this->request->data['ClientsSpecialProduct']['client_id']=$this->request->data['Client']['id'];
										$this->request->data['ClientsSpecialProduct']['product_id']=current(explode(" ", $value));
										$this->request->data['ClientsSpecialProduct']['dashboard_id']=3;
										$this->Client->ClientsSpecialProduct->save($this->request->data);
									}
									else
									{
										$errors[]= $value;
									}
								}
							 }
						}
*/
						if(!empty($errors))
						{
							$var=implode(",",$errors);
							$this->Session->setFlash(''.$var.' has not been saved.');
						}
						else
						{
							$this->Session->setFlash('The Client has been saved');
						}
						$this->redirect_session(array('action'=>'index'), null, true);
			} else {
				$this->Session->setFlash('Please correct the errors below.');

				$special_products =$this->Client->Product->find('all',array('conditions'=>array("Product.id IN ('". implode("','", $this->request->data['SpecialProduct']['SpecialProduct']). "')"), 'recursive' => -1));

				$this->request->data['SpecialProduct'] = array();
				foreach ($special_products as $product) {
					$this->request->data['SpecialProduct'][] = $product['Product'];
				}

				$whats_new_products = $this->Client->Product->find('all',array('conditions'=>array("Product.id IN ('". implode("','", $this->request->data['WhatsNewProduct']['WhatsNewProduct']). "')"),'recursive'=>-1));

				$this->request->data['WhatsNewProduct'] = array();
				foreach ($whats_new_products as $product) {
					$this->request->data['WhatsNewProduct'][] = $product['Product'];
				}
			}
		} 
							 
				
							 
							 
							 }else {

			//	for adding a new client
			if ($id == 0) {
				$this->Client->Program->recursive = 1;
				$this->Client->Program->unbindModel(array('belongsTo' => array('Style','Voucher'), 'hasMany' => array('Client', 'ProgramPage'), 'hasAndBelongsToMany' => array('Product')));
				$program = $this->Client->Program->findById($this->params['url']['data']['Client']['program_id']);

				$client['Client'] = $program['Program'];

				//	set some defaults
				$client['Client']['program_id'] = $client['Client']['id'];
				$client['Client']['id'] = 0;
				unset($client['Client']['name']);
				$client['Category'] = $program['Category'];
				$client['SpecialProduct'] = $program['SpecialProduct'];
				$client['Module'] = $program['Module'];

				$this->request->data = $client;

			} else {

				$this->Client->recursive = 1;
				$this->Client->unbindModel(array('hasMany' => array('User', 'ClientPage'), 'hasAndBelongsToMany' => array('Product')));
				$this->Client->id = $id;
				$this->request->data = $this->Client->read(null, $id);
			}
			//debug($this->request->data);
			$this->_setRedirect();

		}
		//dashboard related styles and client related dashboard
		$dashboard_styles = array();
		$client_style_type=array();
		$this->loadModel("Style");

		foreach($this->request->data['Dashboard'] as $dashboard)
		{
			$dashboard_styles[$dashboard['id']] = $dashboard['ClientsDashboard']['style_id'];
			$client_style_type[$dashboard['id']] = $this->Style->find('first',array('conditions'=>array('id'=>$dashboard_styles[$dashboard['id']]),'recursive'=>-1));
		}

		$this->set('dashboard_styles',$dashboard_styles);
		$this->set('styles_client',$client_style_type);

		$modules = $this->Client->Module->find('list');
		$this->Client->Category->recursive = -1;
		$categories = $this->Client->Category->find('list', array('order'=>'description, name', 'fields'=>'id,description'));

		$countries = $this->Client->Country->find('list');
		$defaultcountry = $this->Client->Country->find('list', array('fields' => 'Country.name,Country.name'));

		$this->loadModel('Style');
		$programs = $this->Client->Program->find('list');
		$styles = $this->Style->find('list');
		$vouchers = $this->Client->Voucher->find('list');
		$domains = $this->Client->Domain->find('list');
		$aligns = $this->Client->getEnumValues('banner_align');
		$repeats = $this->Client->getEnumValues('banner_repeat');

		//here we are passing dashboard_id's into _pages method like _pages(,,,1,0),_pages(,,,2,0),_pages(,,,3,0) to set client pages with respective dashbaords
		//here last number 0(zero) is indicating the public pages and 1 indicating private pages

		$this->set('client_pages', $this->_pages($this->request->data['Client']['program_id'], $this->request->data['Client']['id'], true,1,0));
		$this->set('client_public_pages', $this->_pages($this->request->data['Client']['program_id'], $this->request->data['Client']['id'],true,1,1));
		$this->set('client_pages_sendafriend', $this->_pages($this->request->data['Client']['program_id'], $this->request->data['Client']['id'],true,2, 0));
		$this->set('client_public_pages_sendafriend', $this->_pages($this->request->data['Client']['program_id'], $this->request->data['Client']['id'], true,2,1));
		$this->set('client_pages_points', $this->_pages($this->request->data['Client']['program_id'], $this->request->data['Client']['id'], true,3,0));
		$this->set('client_public_pages_point', $this->_pages($this->request->data['Client']['program_id'], $this->request->data['Client']['id'], true,3,1));


		//	default the client to all modules & categories
		if (!$id && empty($this->request->data['Client']['id'])) {
				$this->request->data['Module']['Module'] = $modules;
				$this->request->data['Category']['Category'] = $categories;
				$this->request->data['Country']['Country'] = $countries;
		}
		$this->loadModel("Style");
				$styles_client = $this->Style->find('first',array('conditions'=>array('id'=>$this->request->data['Client']['style_id']),'recursive'=>-1));

		$this->set('style_client',$styles_client);
		$this->set(compact('defaultcountry','countries','categories','modules','programs','styles','vouchers','pages','domains', 'aligns','repeats'));

		// dashboard data
		$this->loadModel("ClientBanner");
		$sql1 = "SELECT * FROM client_banners where client_id='".$this->request->data['Client']['id']."' and dashboard_id=1";
		$dashboard = $this->Client->ClientBanner->query($sql1);
		$this->set('dashboard',$dashboard);

		//dashboard 2
		$sql2 = "SELECT * FROM client_banners where client_id='".$this->request->data['Client']['id']."' and dashboard_id=2";
		$dashboard2 = $this->ClientBanner->query($sql2);
		$this->set('dashboard2',$dashboard2);

		// dashboard 3
		$sql3 = "SELECT * FROM client_banners where client_id='".$this->request->data['Client']['id']."' and dashboard_id=3";
		$dashboard3 = $this->ClientBanner->query($sql3);
		$this->set('dashboard3',$dashboard3);

		$codes = $this->CurrencyCode->find('list');
		$this->set('codes',$codes);

		$business_name = $this->PaypalDetail->find('first',array('conditions'=>array('PaypalDetail.client_id'=>$this->request->data['Client']['id'])));
		$this->set('business_name',$business_name['PaypalDetail']['paypal_business_name']);
		$this->set('currency_code',$business_name['PaypalDetail']['paypal_currency_code']);
	}

	function admin_delete($id = null) {

		$this->layout = 'admin';

		if (!$id) {
			$this->Session->setFlash('Invalid id for Client');
			$this->redirect(array('action'=>'index'), null, true);
		}


		//	cannot delete a client if the client has users
		if ($this->Client->User->find('count', array('conditions'=>array('client_id' => $id)))  != '0') {
			$this->Session->setFlash('Client still has users, remove them from the client before deleting. <a href="'. $this->base. '/admin/users/?data%5BUser%5D%5Bsearch%5D=&data%5BUser%5D%5Bclient_id%5D='. $id.'">View users for this client</a>');
			$this->redirect(array('action'=>'index'), null, true);
		}

		// cannot delete a client if it is the default for a domain
		$domain  = $this->Client->Domain->findByDefaultClientId($id);
		if ($domain) {
			$this->Session->setFlash('Client is the default for Domain <a href="'. $this->base. '/admin/domains/edit/'. $domain['Domain']['id']. '">'. $domain['Domain']['name']. '</a>. Change the default client before deleting.');
			$this->redirect(array('action'=>'index'), null, true);
		}

		//	predelete the clients products as the delete function deletes them one at a time
		$this->Client->query("DELETE FROM clients_products WHERE client_id = '". $id. "'");

		if ($this->Client->delete($id)) {

			// delete unneded files
			$delete_images = array('banner', 'banner_background_image_left', 'banner_backgorund_image_right', 'banner_background_image_left_saf', 'banner_backgorund_image_right_saf', 'banner_background_image_left_points', 'banner_backgorund_image_right_points', 'client_logo', 'default_product_image');
			foreach ($delete_images as $image) {
				$this->File->deleteByPrefix($image. '/'.$id. '.');
			}

			$this->Session->setFlash('Client deleted');
			$this->redirect(array('action'=>'index'), null, true);
		}


	}

	/**
	 * Return a html list of products for an ajax call
	 */
	function admin_ajax_products($id) {

		$this->layout = 'ajax';

		$data = $this->paginate('ClientsProduct',"Client.id = '". $id. "' AND Product.id IS NOT NULL");

		$this->set('clients_product', $data);
		$this->set('id', $id);
	}

	/**
	 * Return an xml list of products
	 *
	 * @param	int	$id	client id
	 * @param	int	$limit	number or rows to return
	*/
	function admin_xml_products($id, $limit = 10) {

		$this->layout = 'ajax';

		$conditions = "(ClientsProduct.client_id = '". $id. "' OR ClientsProduct.client_id=0)";
		if (isset($_GET['term'])) {
			$conditions .= "AND Product.id LIKE '".$_GET['term']."%'";
		}

		App::import("Model", "ClientsProduct");
		$this->ClientsProduct = new ClientsProduct();
		$data = $this->ClientsProduct->find('all', array('conditions'=>$conditions, 'fields'=>'Product.id,Product.name', 'order'=>'Product.name', 'limit'=>$limit));

		$this->set('products', $data);

	}

	function admin_xml_products_points($id, $limit = 10)
		{
			$this->layout = 'ajax';
			$conditions = "Product.active = 1 AND (Product.expires = '0000-00-00' OR Product.expires >= NOW()) AND ClientsProductsPoint.product_id LIKE '".$_GET['term']."%'";
			if (isset($_GET['term']))
			{
				$conditions .= "AND Product.id LIKE '". $_GET['term']. "%'";
			}
			$this->loadModel("ClientsProductsPoint");
			$data = $this->ClientsProductsPoint->find('all', array('conditions'=>$conditions, 'fields'=>'DISTINCT Product.id,Product.name', 'order'=>'Product.name', 'limit'=>$limit));

			$this->set('products', $data);
		}

		function admin_xml_products_saf($id, $limit = 10)
		{
			$this->layout = 'ajax';
			$conditions = "Product.active = 1 AND (Product.expires = '0000-00-00' OR Product.expires >= NOW()) AND ClientsProductsSaf.product_id LIKE '".$_GET['term']."%'";
			if (isset($_GET['term']))
			{
				$conditions .= "AND Product.id LIKE '". $_GET['term']. "%'";
			}
			$this->loadModel("ClientsProductsSaf");
			$data = $this->ClientsProductsSaf->find('all', array('conditions'=>$conditions, 'fields'=>'DISTINCT Product.id,Product.name', 'order'=>'Product.name', 'limit'=>$limit));

			$this->set('products', $data);
	}
/**
	 * Return an HTML snippet of client comments
	*/
	function admin_ajax_comments($id = null) {

		$this->layout = 'ajax';

		$comments = $this->Client->ClientComment->findAllByClientId($id);

		$this->set('comments', $comments);
		$this->set('id', $id);
	}

	/**
	 * Save a new comment
	*/
	function admin_ajax_comment_save($id,$desc,$notes) {

		$this->layout = 'ajax';

		$this->request->data['ClientComment']['user_id'] = $this->Session->read('user.User.id');
		$this->request->data['ClientComment']['username'] = $this->Session->read('user.User.username');
		$this->request->data['ClientComment']['client_id'] = $id;
		
		$this->request->data['ClientComment']['description'] = $desc;	
		$this->request->data['ClientComment']['notes'] = $notes;
		
		if ($this->Client->ClientComment->save($this->request->data)) {

		} else {
			$this->set('error', true);
		}

		$this->set('id', $id);
	}

	/**
	 * Delete a  comment
	*/
	function admin_ajax_comment_delete($id, $comment_id) {

		$this->layout = 'ajax';

		$this->Client->ClientComment->delete($comment_id);

		$this->set('id', $id);
	}

	function admin_update_style($dahboard=null)
	{
		$selected_style= $dahboard==3?$this->request->data['Client']['style_id3']:($dahboard==2?$this->request->data['Client']['style_id2']:$this->request->data['Client']['style_id1']);
		$defaultstylesql ="SELECT distinct Style.tab_background_color, Style.button_border_color ,Style.h1_color FROM styles AS Style, clients as Client where
		Style.id='".$selected_style."'";

		$this->loadModel("Style");
		$cstyles = $this->Style->query($defaultstylesql);
		$this->set('cstyles',$cstyles);
        $this->layout = 'blank';
	}
}
?>