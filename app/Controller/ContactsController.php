<?php
class ContactsController extends AppController {

	var $helpers = array('Html', 'Form','Ajax');
	var $uses = array('Contact','Domain');
	var $components = array('SwiftMailer', 'MailQueue','RequestHandler');

	function contact_us() {
		$client= $this->Session->read('client.Client');
		if(empty($this->request->data)){
			$user = $this->Session->read('user.User');
			if(!empty($user)){
				//prepopulate the user information
				$this->request->data['Contact']['firstname'] = $user['first_name'];
				$this->request->data['Contact']['lastname'] = $user['last_name'];
				$this->request->data['Contact']['email'] = $user['email'];
				$this->request->data['Contact']['username'] = $user['username'];
				$this->request->data['Contact']['user_id'] = $user['id'];
			}else{
				$this->request->data['Contact']['user_id'] = 0;
			}

		}else{
			//save the form into the database
			$this->request->data['Contact']['client_id'] = $client['id'];

			//excluding all data that will be stored into the database directly
			$exclude = array('client_id', 'user_id', 'form_type', 'email');
			$typeArray = array();
			$dataArray = array();
			$key =0;
			foreach($this->request->data['Contact'] as $type=>$data){
				if(in_array($type,$exclude)) continue;
				$typeArray[$key] = $type;
				$dataArray[$key] = $data;
				$key++;
			}


			if(!empty($typeArray)){
				$this->request->data['Contact']['type'] = serialize($typeArray);
				$this->request->data['Contact']['data'] = serialize($dataArray);
			}

			if($this->Contact->save($this->request->data)){
				//send the email to the client
				$email = $client['email'];
				$this->set('contact',$this->request->data);
				$this->set('typeArray',$typeArray);
				$this->set('dataArray',$dataArray);
				if($this->SwiftMailer->connect()) {
					$this->SwiftMailer->addTo('to', $email);
					$this->SwiftMailer->addTo('from',WEB_MAIL_FROM);

					if(!$this->SwiftMailer->sendView('Contact Us', 'contact', 'html')) {
						echo "The mailer failed to Send. Errors:";
						die(debug($this->SwiftMailer->errors()));
					}

				} else {
					echo "The mailer failed to connect. Errors:";
					die(debug($this->SwiftMailer->errors()));
				}
				echo '<div align="center" style="padding-top:35px;"> Thank you for contacting '. $client['name'].'.<br> Your email has been sent and you will be contacted shortly by a rewards team member. Have a great day!</div>';
				die;$this->autoRender=false;
			} else {
				$this->Session->setFlash('Please correct the errors below.');
			}


		}

	}

	function feedback() {
		$client= $this->Session->read('client.Client');
		if(empty($this->request->data)){
			$user = $this->Session->read('user.User');
			//print_r($client);
			if(!empty($user)){
				//prepopulate the user information
				$this->request->data['Contact']['firstname'] = $user['first_name'];
				$this->request->data['Contact']['lastname'] = $user['last_name'];
				$this->request->data['Contact']['email'] = $user['email'];
				$this->request->data['Contact']['username'] = $user['username'];
				$this->request->data['Contact']['user_id'] = $user['id'];
			}else{
				$this->request->data['Contact']['user_id'] = 0;
			}

		}else{
			//save the form into the database
			$this->request->data['Contact']['client_id'] = $client['id'];

			//excluding all data that will be stored into the database directly
			$exclude = array('client_id', 'user_id', 'form_type', 'email');
			$typeArray = array();
			$dataArray = array();
			$key =0;
			foreach($this->request->data['Contact'] as $type=>$data){
				if(in_array($type,$exclude)) continue;
				$typeArray[$key] = $type;
				$dataArray[$key] = $data;
				$key++;
			}


			if(!empty($typeArray)){
				$this->request->data['Contact']['type'] = serialize($typeArray);
				$this->request->data['Contact']['data'] = serialize($dataArray);
			}

			if($this->Contact->save($this->request->data)){
				//send the email to the client
				$email = $client['email'];
				$this->set('contact',$this->request->data);
				$this->set('typeArray',$typeArray);
				$this->set('dataArray',$dataArray);
				if($this->SwiftMailer->connect()) {
					$this->SwiftMailer->addTo('to', $email);
					$this->SwiftMailer->addTo('from',WEB_MAIL_FROM);

					if(!$this->SwiftMailer->sendView('Contact Us', 'contact', 'html')) {
						echo "The mailer failed to Send. Errors:";
						die(debug($this->SwiftMailer->errors()));
					} else {
						echo '<div align="center" style="padding-top:20px;">Email sent successfully</div>';
					}

				} else {
					echo "The mailer failed to connect. Errors:";
					die(debug($this->SwiftMailer->errors()));
				}
				echo '<div align="center" style="padding-top:10px;">Thank you for your feedback</div>';die;
			} else {
				$this->Session->setFlash('Please correct the errors below.');
			}
		}
	}

	function email_us() {
		if(!empty($_REQUEST['emailto'])){
		$email = $_REQUEST['emailto'];
		$fname = $_REQUEST['first_name'];
		$lname = $_REQUEST['last_name'];
		$mnumber = $_REQUEST['membership_number'];
		$emailfrom = $_REQUEST['email_address'];
		$tel = $_REQUEST['telephone'];
		$comments = $_REQUEST['more_info_comments'];
		$returnUrl = $_REQUEST['returnurl'];

		$body = '';

		$body .= "First name : ".$fname."\n\n";
		$body .= "Last name :".$lname."\n\n";
		$body .= "Membership Number : ".$mnumber."\n\n";
		$body .= "Email : ". $emailfrom."\n\n";
		$body .= "Telephone : ".$tel."\n\n";
		$body .= "Comments : ". $comments;

		App::import("Vendor",'Swift');
		App::import("Vendor",'Swift/Connection/NativeMail');

		$swift = new Swift(new Swift_Connection_NativeMail());
		$message =& new Swift_Message("Email from the website", $body);

		if ($swift->send($message, $email, $_REQUEST['email_address']))
		{
			$msg = 'sent';
		}
		else
		{
			$msg = 'failed';
		}
		$this->set('msg',$msg);

		if(!empty($returnUrl))
		{
			$dm = $_SERVER['HTTP_HOST'];

			$old_data = $this->Domain->find('all',array('conditions'=>array('Domain.name'=>$dm),'recursive'=>-1));
			$this->redirect('http://'.$old_data[0]['Domain']['name'].$old_data[0]['Domain']['home_page'].'/'.$returnUrl);
		}
		}else{
			$this->redirect(Controller::referer());
		}
	}
}
?>