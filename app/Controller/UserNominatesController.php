<?php
class UserNominatesController extends AppController {

	var $components = array('Cookie', 'SwiftMailer', 'MailQueue','Csv','RequestHandler');
	var $uses = array('User','Client','UserNominate','Domain');

	function add()
	{
		$userid= $this->Session->read('user.User.id');
		$clientid= $this->Session->read('user.Client.id');
		$userName = "";
		foreach ($this->request->data['UserNominate'] as $key => $value)
		{
			$userName .= strlen($userName)>0 ? ", ":"";
			$userName .= $value['firstname'];
			$this->UserNominate->create();
			$value['user_id'] = $userid;
			$value['approved'] = 0;
			$value['clientid'] = $clientid;
			$this->UserNominate->save(array('UserNominate' => $value));
		}
		echo "<div style='padding:25px;'><p><b>Email sent!</b></p><p>A welcome email has just be sent to <b>".$userName."</b></p>
		<p>Soon they will be able to start using the program benefits and reaping the rewards!</p></div>";
		$this->autoRender=false;
	}

	function admin_index() {

		$this->set('clients', $this->User->Client->find('list'));
		if (isset($this->params['url']['data'])) {
			$this->Session->write($this->name.'.search_index', $this->params['url']['data']);
		}
		if ($this->Session->check($this->name.'.search_index')) {
			$this->request->data = $this->Session->read($this->name.'.search_index');
			$conditions = array();
			if  (!empty($this->request->data['UserNominate']['clientid'])) {
				$conditions[] = "`UserNominate`.`clientid` = '". $this->request->data['UserNominate']['clientid']. "'";
			}
		}
		else {
			$conditions = array();
		}
		$this->paginate = array('UserNominate' => array('limit' => 30));
		$this->set('nominate', $this->paginate('UserNominate',$conditions));
	}

	function admin_approve($id)
	{
		$domain = $_SERVER['HTTP_HOST'];
		$this->set('domain', $domain);
		$approve = $this->UserNominate->findById($id);

		$client_nominee = $this->Client->find('all',array('conditions'=>array('Client.id' => $approve['UserNominate']['clientid']),'fields'=>'country', 'recursive'=> -1));

		$default_country = $client_nominee[0]['Client']['country'];
		if($default_country=='Australia')
			$client = $this->Client->find('all',array('conditions'=>array('Client.id' => 1018), 'recursive'=> -1));
		elseif($default_country=='India')
			$client = $this->Client->find('all',array('conditions'=>array('Client.id' => 1408), 'recursive'=> -1));

		$clientid= $client[0]['Client']['id'];
		$domainid = $client[0]['Client']['domain_id'];

		$domain = $this->Domain->findById($domainid);
		$username_541=$id;
		$data = null;
		$old_data = $this->User->findById($approve['UserNominate']['user_id']);

		$unique_id = md5(uniqid(rand(), true));
		$sql = "INSERT INTO users (client_id, domain_id, username, email, first_name, last_name, state, newsletter, registered,
		unique_id, expires, created, modified) VALUES ('".$clientid."', '".$domainid."', '".$username_541."' ,
		'".$approve['UserNominate']['email']."', '".$approve['UserNominate']['firstname']."', '".$approve['UserNominate']['lastname']."', '',
		1, 0, '".$unique_id."', '".$old_data['User']['expires']."', NOW(), NOW()) ";

		$this->User->query($sql);
		$new_user = $this->User->findByUsername($username_541);
		$this->set('new_user',$new_user);
		$this->set('nominate_user',$old_data);
		$this->set('nominate_domain',$domain);
		$this->set('nominate_client',$client);

		if( $approve['UserNominate']['email'] !='')
		{
			if($this->SwiftMailer->connect())
			{
				$this->SwiftMailer->addTo('to', $approve['UserNominate']['email'], $approve['UserNominate']['firstname'].' '.$approve['UserNominate']['lastname']);
				$this->SwiftMailer->addTo('from',$client[0]['Client']['email'],$client[0]['Client']['name']);
				if(!$this->SwiftMailer->sendView('Invitation from '. $old_data['User']['email'], 'family_invite', 'html'))
				{
					echo "Mail not sent. Errors: ";
					die(debug($this->SwiftMailer->errors()));
				}
			}
			else
			{
				echo "The mailer failed to connect. Errors: ";
				die(debug($this->SwiftMailer->errors()));
			}
			$this->Session->setFlash('Your invitation has been sent to  '. $approve['UserNominate']['email'] .'');
			$this->UserNominate->updateAll(array('approved'=>1), array('UserNominate.id'=>$id));
			$this->redirect($_SERVER["HTTP_REFERER"]);
		}
	}


	function admin_delete($id)
	{
		if ($this->UserNominate->delete($id))
		{
			$this->Session->setFlash('User deleted');
			$this->redirect($_SERVER["HTTP_REFERER"]);
		}
	}
	function admin_delete_selected($list)
	{
		$nominates_list = explode(',',$list);
		foreach($nominates_list as $id)
		{
			$this->UserNominate->delete($id);
		}
		$this->redirect($_SERVER["HTTP_REFERER"]);
	}
	function admin_unapproved()
	{
		$conditions = array();

		$conditions = " UserNominate.approved =0 ";
		$this->paginate = array('UserNominate' => array('limit' => 30));
		$this->set('nominate', $this->paginate('UserNominate',$conditions));
	}

	function admin_bulk_approvals($list)
	{
		$domain = $_SERVER['HTTP_HOST'];
		$this->set('domain', $domain);
		$nominates_list = explode(',',$list);
		foreach($nominates_list as $id)
		{
			$approve = $this->UserNominate->findById($id);

			$client_nominee = $this->Client->find('all',array('conditions'=>array('Client.id' => $approve['UserNominate']['clientid']),'fields'=>'country', 'recursive'=> -1));

			$default_country = $client_nominee[0]['Client']['country'];

			if($default_country=='Australia')
				$client = $this->Client->find('all',array('conditions'=>array('Client.id' => 1018), 'recursive'=> -1));

			$clientid= $client[0]['Client']['id'];
			$domainid = $client[0]['Client']['domain_id'];

			$domain = $this->Domain->findById($domainid);
			$username_541=$id;
			$data = null;

			$old_data = $this->User->findById($approve['UserNominate']['user_id']);

			$unique_id = md5(uniqid(rand(), true));
			$sql = "INSERT INTO users (client_id, domain_id, username, email, first_name, last_name, state, newsletter, registered,
			unique_id, expires, created, modified) VALUES ('".$clientid."', '".$domainid."', '".$username_541."' ,
			'".$approve['UserNominate']['email']."', '".$approve['UserNominate']['firstname']."', '".$approve['UserNominate']['lastname']."', '',
			1, 0, '".$unique_id."', '".$old_data['User']['expires']."', NOW(), NOW()) ";

			$this->User->query($sql);
			$new_user = $this->User->findByUsername($username_541);
			$this->set('nominate_user',$old_data);

			if( $approve['UserNominate']['email'] !='' && $approve['UserNominate']['id'] == $id)
			{
				$message = "";
				$subject ="Invitation from ". $old_data['User']['email'];

				$message.= "<p>Invitation email</p><p>Hi ".$new_user['User']['first_name'] ." ". $new_user['User']['last_name'].",</p>";
				$message.="<p>".$old_data['User']['first_name']. " ". $old_data['User']['last_name']." has invited you to enjoy a family
				membership with ".$client[0]['Client']['name']. ". By accepting the invitation and completing your details you will also
				receive unlimited web access to ".$client[0]['Client']['name']." . You can also receive your own membership card and a
				benefits book for free *.</p>";
				$message.="<p>Your membership number: ". $new_user['User']['username']."<br/></p>";
				$message.="<p>To accept the invitation <a href='http://".$domain['Domain']['name']."/users/first_time_login/'> click here.</a></p>";
				$message.="<p>Use the membership number provided to create a First Time Login.<br/></p>";
				$message.="<p>* To order your free membership book and card please search for 'Free My Rewards Coupon Book & Card' once you have
				logged in. The coupon book and card is free + $9.95 postage and handling.</p>";

				$headers  = "From: '".$client[0]['Client']['name']."' <".$client[0]['Client']['email'].">\r\n";
				$headers .= "Content-type: text/html\r\n";
				$mail = mail($approve['UserNominate']['email'], $subject, $message, $headers);

				$this->UserNominate->updateAll(array('approved'=>1), array('UserNominate.id'=>$approve));
			}
		}
		$this->Session->setFlash('Invitation has been sent to approved users');
		$this->redirect($_SERVER["HTTP_REFERER"]);
	}
}
?>