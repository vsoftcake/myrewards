<?php
class PointsController extends AppController {

	var $components = array('File','Cookie', 'SwiftMailer', 'MailQueue','Csv','RequestHandler');
	var $uses = array('User', 'Client','Program','Merchant','Product','ProductsPoint','ClientsProductsPoint','CartProduct','Cart','ClientsSpecialProduct');

	function admin_index()
	{
		$programs= $this->Program->find("list");
		$this->set('programs', $programs);
		// Search Results
		if(isset($this->params['url']['data']))
		{
			$product= $this->params['url']['data'];
			$sql1 = " and ProductsPoint.product_id=".$product['Product']['search'];
			$sql = "SELECT * FROM products as Product , products_points as ProductsPoint where ProductsPoint.product_id=Product.id and
			(ProductsPoint.product_id LIKE '%".$product['Product']['search']."%' OR Product.name LIKE '%".$product['Product']['search']."%')";
			$searchproducts=$this->ProductsPoint->query($sql);
			$this->set("searchproducts",$searchproducts);
		}else{
			// index page product list in cart products table
			$this->paginate = array('ProductsPoint' => array('limit' => 20));
			$products = $this->paginate('ProductsPoint', array('search' =>array('sql' => $sql1)));
		}
		$this->set("products",$products);

		//setting all clients redeemed history into points log tab
		$sqlquery ="select User.username,User.points as userpoints,RedeemTracker.id, RedeemTracker.user_id,RedeemTracker.product_id,RedeemTracker.points,RedeemTracker.activity,RedeemTracker.activity_date,Products.name from users as User,redeem_trackers as RedeemTracker LEFT JOIN products as Products ON RedeemTracker.product_id=Products.id where User.id=RedeemTracker.user_id and RedeemTracker.activity='Product/s Redeemed.' order by RedeemTracker.id desc";
		$this->loadModel("RedeemTracker");
		$redeemdetails = $this->RedeemTracker->query($sqlquery);
		$this->set('redeem_history',$redeemdetails);

		// index page product list in cart products table
		//$sql = "SELECT * FROM products as Product , cart_products as CartProduct where CartProduct.product_id=Product.id";
		//$this->paginate = array('Cart' => array('limit' => 20));
		//$cartproducts = $this->paginate('Cart');
		//$this->set("cartproducts",$cartproducts);

		// New orders
		$sql = "SELECT * FROM carts as Cart, cart_items as CartItem, cart_products as CartProduct, cart_addresses as CartAddress, products as Product
		where Cart.id = CartItem.cart_id and Cart.address_id = CartAddress.id and Product.id=CartProduct.product_id and CartItem.product_id=CartProduct.product_id
		and CartProduct.product_type = 'Points' and (CartItem.cart_status='Pending' or CartItem.cart_status='Paid') order by CartItem.order_id desc";
		$cart=$this->Cart->query($sql);
		//print_r($cart);
		$this->set("carts",$cart);

		//Shipped orders
		$sql = "SELECT * FROM carts as Cart, cart_items as CartItem, cart_products as CartProduct, cart_addresses as CartAddress,products as Product
		where Cart.id = CartItem.cart_id and Cart.address_id = CartAddress.id and Product.id=CartProduct.product_id and CartProduct.product_type = 'Points' and
		CartItem.product_id=CartProduct.product_id AND CartItem.cart_status='Shipped' order by CartItem.order_id desc";
		$shipped_cart=$this->Cart->query($sql);
		//print_r($shipped_cart);
		$this->set("shipped_cart",$shipped_cart);

		//Get total count of products of an order for Shipped status
		$shipped_total_count_sql = "SELECT CartItem.order_id, count(CartItem.order_id) as count1 FROM carts as Cart, cart_items as CartItem, cart_products as CartProduct, cart_addresses as CartAddress,products as Product
		where Cart.id = CartItem.cart_id and Cart.address_id = CartAddress.id and Product.id=CartProduct.product_id and CartProduct.product_type = 'Points' and
		CartItem.product_id=CartProduct.product_id AND CartItem.cart_status='Shipped' and CartItem.mail_sent=0 group by CartItem.order_id";
		$shipped_total_count=$this->Cart->query($shipped_total_count_sql);

		$this->set("shipped_total_count",$shipped_total_count);

		//Get total count of products of an order
		$total_count_sql = "SELECT CartItem.order_id, count(CartItem.order_id) as count2 FROM carts as Cart, cart_items as CartItem, cart_products as CartProduct, cart_addresses as CartAddress,products as Product
		where Cart.id = CartItem.cart_id and Cart.address_id = CartAddress.id and Product.id=CartProduct.product_id and CartProduct.product_type = 'Points' and
		CartItem.product_id=CartProduct.product_id and CartItem.mail_sent=0 group by CartItem.order_id";
		$total_count=$this->Cart->query($total_count_sql);

		$this->set("total_count",$total_count);

		//Completed orders
		$sql = "SELECT * FROM carts as Cart, cart_items as CartItem, cart_products as CartProduct, cart_addresses as CartAddress,products as Product
		where Cart.id = CartItem.cart_id and Cart.address_id = CartAddress.id and Product.id=CartProduct.product_id and CartProduct.product_type = 'Points' and
		CartItem.product_id=CartProduct.product_id AND CartItem.cart_status='Completed' order by CartItem.order_id desc";
		$complted_cart=$this->Cart->query($sql);
		$this->set("complted_cart",$complted_cart);

	}
	function admin_update_program_clients()
	{
		$pid = $this->request->data['Program']['Program'];
		$programs = $this->Program->Client->find('list', array('conditions' =>array('program_id' => $pid)));

		$this->set('program_clients',$programs);
        $this->layout = 'ajax';
	}
	function admin_updatepoints()
	{
		$points=$this->params['form']['client_points'];
		$clientList = implode(',',$this->request->data['Client']['Client']);
		$clients_list= substr($clientList, 0, -1);
		$user_points_update_query="UPDATE users  SET points = points + ".$points." WHERE client_id in (".$clients_list.")";
		$this->User->query($user_points_update_query);
		$this->Session->setFlash('The Points has been added to users');
		$this->redirect_session(array('action'=>'index'), null, true);
		exit();
	}
	function admin_updateproductpoints()
	{
		$points=$this->params['form']['product_points'];
		$merchant_ids = implode(',',$this->request->data['Merchant']['Merchant']);
		$merchant_list= substr($merchant_ids, 0, -1);
		$product_list_query="select id from products where merchant_id in (".$merchant_list.")";
		$product_list = $this->Product->query($product_list_query);
		$product_list_data=array();
		foreach($product_list as $product_list_data )
		{
			$product_points_update_query="UPDATE products  SET points = ".$points." WHERE id=".$product_list_data['products']['id'];
			$this->User->query($user_points_update_query);

		}
		$this->Session->setFlash('The Points has been added to Products');
		$this->redirect_session(array('action'=>'index'), null, true);
		exit();
	}
	function admin_edit($id=null) {

		if (!empty($this->request->data)) {

			$this->request->data['ProductsPoint']['product_id']=current(explode(" ", $this->request->data['ProductsPoint']['product_id']));
			$prdt = $this->ProductsPoint->find('first', array('conditions' => array('product_id'=>$this->request->data['ProductsPoint']['product_id'])));
			if(empty($prdt))
			{
				$msg = "";
				if (isset($this->request->data['ProductsPoint']['id']) && $this->request->data['ProductsPoint']['id']>0 && isset($this->request->data['ProductsPoint']['points']))
					$msg = "updated";
				elseif (isset($this->request->data['ProductsPoint']['id']))

				$this->request->data['ProductsPoint']['id'] = 0;
				if ($this->request->data['ProductsPoint']['file']['name'] != '')
				{
					$logo_extension = substr($this->request->data['ProductsPoint']['file']['name'], strrpos($this->request->data['ProductsPoint']['file']['name'], '.') + 1);
					$this->request->data['ProductsPoint']['logo_extension'] = $logo_extension;
				}

				if ($this->request->data['ProductsPoint']['file']['name'] != '')
				{
					move_uploaded_file($this->request->data['ProductsPoint']['file']['tmp_name'], PRODUCT_HOT_OFFER_IMAGE_POINTS_PATH. $this->request->data['ProductsPoint']['product_id']. '.'. $logo_extension);
				}

				$this->ProductsPoint->save($this->request->data);

				$prId = $this->request->data['ProductsPoint']['product_id'];
				$this->ClientsProductsPoint->deleteAll(array('product_id'=>$prId));
				if($this->request->data['ProductsPoint']['product_type'] == '-1'){
					$this->ClientsProductsPoint->save(array('product_id'=>$prId, 'client_id'=>0));
				}else{
					//Save clients products points
					$prClientData = array();
					foreach($this->request->data['Client']['Client'] as $prClient){
						if(!empty($prClient)){
							$prClientData[] = array('product_id'=>$prId, 'client_id'=>$prClient);
						}
					}
					if(!empty($prClientData)){$this->ClientsProductsPoint->saveMany($prClientData);}
				}

				if ($msg == 'updated')
				{
					$this->Session->setFlash('The product is successfully updated!!');
				}
				else
				{
					$this->Session->setFlash('The product is successfully added!!');
				}

			}
			else{
				$data=$this->request->data;
				if ($this->request->data['ProductsPoint']['file']['name'] != '')
				{
					$logo_extension = substr($this->request->data['ProductsPoint']['file']['name'], strrpos($this->request->data['ProductsPoint']['file']['name'], '.') + 1);
					$this->request->data['ProductsPoint']['logo_extension'] = $logo_extension;
					$sql = "UPDATE products_points set logo_extension='$logo_extension',points=".$data['ProductsPoint']['points'].",
					active =".$data['ProductsPoint']['active']." where product_id=".$data['ProductsPoint']['product_id'];
					$this->ProductsPoint->query($sql);
				}
				if ($this->request->data['ProductsPoint']['file']['name'] == '')
				{
					$logo_extension = substr($this->request->data['ProductsPoint']['file']['name'], strrpos($this->request->data['ProductsPoint']['file']['name'], '.') + 1);
					$this->request->data['ProductsPoint']['logo_extension'] = $logo_extension;
					$sql = "UPDATE products_points set points=".$data['ProductsPoint']['points'].", active =".$data['ProductsPoint']['active']."
					where product_id=".$data['ProductsPoint']['product_id'];
					$this->ProductsPoint->query($sql);
				}

				if ($this->request->data['ProductsPoint']['file']['name'] != '') {
					move_uploaded_file($this->request->data['ProductsPoint']['file']['tmp_name'], PRODUCT_HOT_OFFER_IMAGE_POINTS_PATH. $this->request->data['ProductsPoint']['product_id']. '.'. $logo_extension);
				}

				$prId = $this->request->data['ProductsPoint']['product_id'];
				$this->ClientsProductsPoint->deleteAll(array('product_id'=>$prId));
				if($this->request->data['ProductsPoint']['product_type'] == '-1'){
					$this->ClientsProductsPoint->save(array('product_id'=>$prId, 'client_id'=>0));
				}else{
					//Save clients products points
					$prClientData = array();
					foreach($this->request->data['Client']['Client'] as $prClient){
						if(!empty($prClient)){
							$prClientData[] = array('product_id'=>$prId, 'client_id'=>$prClient);
						}
					}
					if(!empty($prClientData)){$this->ClientsProductsPoint->saveMany($prClientData);}
				}

				// delete hotoffer image
				if (isset($this->request->data['ProductsPointImage1Delete'])) {
					foreach ($this->request->data['ProductsPointImage1Delete'] as $key => $value) {
						if ($value == '1') {
							$this->File->deleteByPrefix('hot_offer_img_points/'.$data['ProductsPoint']['product_id']);
							$delExtsql = "UPDATE products_points set logo_extension='' where product_id=".$data['ProductsPoint']['product_id'];
							$this->ProductsPoint->query($delExtsql);
						}
					}
				}
			}

		    // insert into cart products table
			$product_points=$this->request->data['ProductsPoint']['points'];
			$quanity=$this->request->data['CartProduct']['product_quantity'];
			$product_type='Points';
			$this->request->data['ProductsPoint']['product_id']=current(explode(" ", $this->request->data['ProductsPoint']['product_id']));
			$product_id = $this->CartProduct->find('first', array('conditions' => array('CartProduct.product_type'=>'Points','CartProduct.product_id'=>$this->request->data['ProductsPoint']['product_id']),'fields' => array('CartProduct.product_id','CartProduct.product_quantity')));
			if (empty($product_id))
			{

				$sql = "Insert into cart_products (product_id, product_quantity, product_type,  product_points) values (".$this->request->data['ProductsPoint']['product_id'].", ".$quanity.", '".$product_type."', '".$product_points."')";
				$this->CartProduct->query($sql);

			}
			else
			{

				$sql = "UPDATE cart_products set product_quantity = ".$quanity.",product_points = ".$product_points." WHERE product_type='Points' and product_id =".$this->request->data['ProductsPoint']['product_id'];
				$this->CartProduct->query($sql);
			}
			$this->Session->setFlash('The product is successfully updated!!');
			$this->redirect('/admin/points');
		}
		else {
			$this->request->data = $this->ProductsPoint->read(null, $id);
			if(!empty($id)){
	 			$sql = "SELECT * FROM cart_products as CartProduct WHERE CartProduct.product_type='Points' AND CartProduct.product_id =".$this->request->data['ProductsPoint']['product_id'];
	 			$product_id = $this->CartProduct->query($sql);
			}
		}

		$clientsList = $this->Client->find('list',array('conditions'=> 'Client.name!=""'));
		$clientsPoint = $this->ClientsProductsPoint->find('list',array('conditions' =>array('ClientsProductsPoint.product_id' => $this->request->data['ProductsPoint']['product_id']),'fields'=>array('ClientsProductsPoint.id','ClientsProductsPoint.client_id')));
		$clients= array();
			foreach ($clientsPoint as $client) {
			   array_push($clients,$client);
			}

		if(!empty($clients))
		{
			$programsList = $this->Product->Program->find('list');

			$sql = "SELECT Client.program_id from clients as Client where Client.id in (".implode(',',$clients).") group by Client.program_id";
			$programsPoint = $this->Client->query($sql);
			$programs= array();$program_ids = array();
				foreach ($programsPoint as $program) {
				   array_push($programs,$program['Client']['program_id']);
				   $program_ids[] = $program['Client']['program_id'];
				}
			$this->set(compact('programsList','programs'));

			$allclients = $this->Client->find('all', array('conditions' =>array('program_id' =>$program_ids ),'order'=>'Client.program_id, Client.name','fields'=>array('Client.id','Client.name','Client.program_id','Program.name'),'recursive'=>0));

			$this->set('allclients',$allclients);

		}

		$this->set(compact('clients','clientsList'));

		$this->set("productid",$product_id);

	}
	function admin_delete($id = null)
	 {
			if (!$id)
			{
				$this->Session->setFlash('Invalid id for Product');
				$this->redirect(array('action'=>'index'), null, true);
			}
			$sql = "SELECT product_id FROM products_points as ProductsPoint where ProductsPoint.id=".$id;
					$productid=$this->ProductsPoint->query($sql);
					$product_id=$productid[0]['ProductsPoint']['product_id'];
					$logo_extension=$productid[0]['ProductsPoint']['logo_extension'];
					$condition = array('ClientsProductsPoint.product_id' => $product_id);

			$file=new File(WWW_ROOT ."/files/hot_offer_img_points/".$product_id.".".$logo_extension);
			$file->delete();

			$this->ClientsProductsPoint->deleteAll($condition,false);

			$query="delete from clients_special_products where product_id=".$product_id." and dashboard_id='3' and client_id=".$this->Session->read('user.User.client_id');
			$this->ClientsSpecialProduct->query($query);
			if ($this->ProductsPoint->delete($id))
			{
						$this->CartProduct->delete($id,array('conditions'=>array('CartProduct.product_id'=>$id,'CartProduct.Product_type'=>'Points')));
						$this->Session->setFlash('Product deleted successfully');
						$this->redirect(array('action'=>'index'), null, true);
			}
	}

	/**
	 * Return an xml list of products
	 *
	 * @param	int	$id	client id
	 * @param	int	$limit	number or rows to return
	*/
	function admin_xml_products($limit = 10) {

		$this->layout = 'ajax';

		$conditions = "Product.active = 1 AND (Product.expires = '0000-00-00' OR Product.expires >= NOW()) AND ClientsProductsPoint.product_id LIKE '". $this->params['url']['s']. "%'";
		if (isset($this->params['url']['s'])) {
			$conditions = "Product.id LIKE '". $this->params['url']['s']. "%'";
		}

		$data = $this->Product->find('all', array('conditions'=>$conditions, 'fields'=>'Product.id,Product.name', 'order'=>'Product.name', 'limit'=>$limit));

		$this->set('products', $data);

	}

	function admin_products()
	{
		$pid = $_GET['term'];
		if (isset($pid)) {
			$conditions = array("Product.active = 1 AND (Product.expires = '0000-00-00' OR Product.expires >= NOW())  AND Product.id LIKE '%$pid%'");
		}
		else{
			$conditions = array("Product.active = 1 AND (Product.expires = '0000-00-00' OR Product.expires >= NOW())");
		}
		$data = $this->Product->find('all',array('conditions' => $conditions,'fields'=>array('Product.id','Product.name','Product.product_type'),'recursive'=>-1));
		$this->set('products', $data);
	}

	function admin_get_product_clients($productId)
	{
		$this->layout = 'ajax';
		$programsList = $this->Product->Program->find('list');
		$programs_ids = $this->Product->Program->ProgramsProduct->find('all',array('conditions' =>array('ProgramsProduct.product_id' => $productId),
																				'fields'=>array('ProgramsProduct.program_id'),
																				'recursive'=>0));
		$programs = array();
		foreach($programs_ids as $program)
		{
			$programs[] = $program['ProgramsProduct']['program_id'];
		}

		$clientsPoint = $this->Product->Client->ClientsProduct->find('list', array('conditions' =>array('ClientsProduct.product_id' =>$productId),
																			  'fields'=>array('ClientsProduct.id','ClientsProduct.client_id'),
																			  'recursive'=>0));
		$clients= array();
			foreach ($clientsPoint as $client) {
			   array_push($clients,$client);
			}

		$allclients = $this->Client->find('all', array('conditions' =>array('program_id' =>$programs ),'order'=>'Client.program_id, Client.name','fields'=>array('Client.id','Client.name','Client.program_id','Program.name'),'recursive'=>0));

		$this->set('allclients',$allclients);

		$this->set(compact('clients','programs','programsList'));
	}

	function admin_load_clients($list)
	{
		$this->layout = 'blank';
		$this->loadModel('Program');
		if(!empty($list))
		{
			$clients = $this->Client->find('list',array('conditions'=>array('Client.program_id'=>$list),'order'=>'Client.name'));
			$this->set('clientsList',$clients);

			$programname = $this->Program->find('first',array('conditions'=>array('Program.id'=>$list), 'recursive'=>-1, 'fields'=>array('Program.name')));
			$this->set('program_name',$programname['Program']['name']);
			$this->set('pid',$list);
			$this->set('value',$value);

		}
	}
}
?>