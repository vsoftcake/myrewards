<?php
class AppController extends Controller {

	public $helpers = array('Js', 'Html', 'Tree', 'GoogleMapV3', 'GoogleMap', 'ImageResize', 'Javascript', 'Ajax');
	public $uses = array('Permission', 'Domain', 'Log');

	var $return_disabled = true;

	function beforeFilter() {

	  	$this->redir_err();
		$qstr = strtolower($_SERVER['REDIRECT_QUERY_STRING']);
		$qstr = urldecode($qstr);
		if(strpos($qstr,'sleep(') !== false){
			$this->redirect('/');
		}

		//	load the stylesheet for the given url, if the user is not logged in
		if (!$this->Session->check('client.Client')) {

			$client = false;
			$this->Domain->recursive = 0;
			$domain = $this->Domain->findByName($_SERVER['HTTP_HOST']);

			//	if there is no domain try for a www domain, and redirect there, otherwise display a 404
			if (!$domain) {
				$domain = $this->Domain->findByName( 'www.'. $_SERVER['HTTP_HOST'] );
				if ($domain) {
					header('http/1.0 301 Moved Permanently');
					$this->redirect('http://www.'. $_SERVER['HTTP_HOST']. $_SERVER['REQUEST_URI']);
					exit();
				} else {
					header("http/1.0 404 not found");
					echo '<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">
					<html><head>
					<title>Oops!!</title>
					</head><body>
					</br></br>
					<h1>Not Found:</h1></br></br>
					<p>Oops , this is awkward . We have logged the error and will attend , <b> meanwhile click DASHBOARD in menu bar </b></p>
					</body></html>';
					exit();
				}
			} else {
				$this->Domain->Client->recursive = -1;

				$client = $this->Domain->Client->findById( $domain['Domain']['default_client_id'] );
				if (!$this->Session->check('dashboardidinsession')) {
					$this->Session->write('dashboardidinsession', $client['Client']['dashboard']);
				}
				//	if there is no default client, dispaly an error
				if (!$client) {
					header("http/1.0 404 not found");
					echo '<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">
					<html><head>
					<title>Oops!!</title>
					</head><body>
					</br></br>
					<h1>Not Found:</h1></br></br>
					<p>Oops , this is awkward . We have logged the error and will attend , <b> meanwhile click DASHBOARD in menu bar </b></p>
					</body></html>';
					exit();
				} else {
					$client = $this->getClient($client['Client']['id'], $client['Client']['dashboard']);

					$this->Session->write('client', $client);
					$this->Session->write('client'.$client['Client']['dashboard'], $client);
				}
			}
		}

		$page = $this->_checkAccess();
		$this->page = $page;
		$this->_logUserAction($page);

		$this->set('page', $page);

		//	while we are here, set the page title
		if (isset($page['Page']['page_title'])) {
			$this->set('title_for_layout', $page['ClientPage']['page_title']);
		}

		// load the admin layout if we are looking at an admin page
		if (preg_match('/\/admin/i', str_replace($this->base,null, $_SERVER["REQUEST_URI"]))) {
			$this->layout = "admin";
		}

		// default page style
		if (!$this->Session->check('dashboard_style_id')) {
			$style_query="SELECT style_id from clients_dashboards where client_id = '". $this->Session->read('client.Client.id'). "' and dashboard_id=1";
			$this->loadModel('Dashboard');
			$style_id_dashboard=$this->Dashboard->query($style_query);
			$style_id=$style_id_dashboard[0]['clients_dashboards']['style_id'];
			$this->Session->write('dashboard_style_id', $style_id);
		}
	}

	/**
	 * Check if the requested page is available to the currently logged in user, if not, redirect to the login page
	*/
	function _checkAccess() {

		//	first get the page
		//	get the requested page details

		$request = explode('/', str_replace($this->base,null, $_SERVER["REQUEST_URI"]));

		if (!isset($request[1])) {
			$page_name = '/';
		} elseif (!isset($request[2])) {
			$page_name = '/'. $request[1];
		} else {
			$page_name = '/'. $request[1]. '/'. $request[2];
		}

		$page = $this->_clientPage($page_name);

		if (preg_match('/\/admin/i', str_replace($this->base,null, $_SERVER["REQUEST_URI"])) || ($this->Session->read('client.Domain.public') == '0' && (!$page || $page['Page']['public'] == '0'))) {
			// Make the requested alias string
			$alias = $this->name. '.' .$this->action;
			if (!empty($this->params['pass'])) {
				$alias.= '.'. implode('.', $this->params['pass']);
			}

			// Make the alias wild cards
			$aliases[] = $this->name. '.*';
			$aliases[] = $this->name. '.' .$this->action. '.*';

			// Get the users security level
			if ($this->Session->check('user.User.type')) {
				$group = $this->Session->read('user.User.type');

			} else {
				$group = 'Public';
			}

			// Check the access
			if($group=='Member' && $alias=='Pages.display.admin')
			{
				$where = "(Permission.group = '*' OR Permission.group = '". $group. "')
				AND (Permission.alias = '*' OR Permission.alias = '". $alias. "' ";
			}
			else
			{
				$where = "(Permission.group = '*' OR Permission.group = '". $group. "')
					AND (Permission.alias = '*' OR Permission.alias = '". $alias. "' ";
				foreach ($aliases as $alias) {
					$where .= "OR Permission.alias = '". $alias. "' ";
				}
			}

			$where .= ")";

			$permission = $this->Permission->find('all', array('conditions'=>$where));

			// Redirect if no access or not logged in
			if (empty($permission)) {

				if (!$this->Session->check('user.User.id')) {
					$this->Session->write('redirect', 'http://'. $_SERVER['HTTP_HOST']. $_SERVER['REQUEST_URI']);
					$this->redirect('/users/login');
					exit();
				} else {
					if (preg_match('/\/admin/i', str_replace($this->base,null, $_SERVER["REQUEST_URI"])) && $this->Session->read('user.User.type')!="Administrator") {
						$this->Session->setFlash($page_name);
						$this->redirect('/pages/no_access/');
						exit();
					}
				}
			}
		}

		return $page;

	}

	/**
	 * Log a users action in the useraction table, only log specified pages
	 *
	 * @param array $page array of a pages details
 	*/
	function _logUserAction($page) {

		if ($page && $page['Page']['log'] == '1') {

			$from = FULL_BASE_URL.$this->here;
			$data = array(
				'UserAction' => array(
					'page_id' => $page['Page']['id'],
					'url' => $from,
					'remote_ip' => $_SERVER['REMOTE_ADDR']
				)
			);

			if ($this->Session->check('Config.userAgent')) {
				$data['UserAction']['session_id'] = $this->Session->read('Config.userAgent');
			}

			if ($this->Session->check('user.User.id')) {
				$data['UserAction']['user_id'] = $this->Session->read('user.User.id');
			}

			if ($this->Session->check('client.Client.id')) {
				$data['UserAction']['client_id'] = $this->Session->read('client.Client.id');
			}

			//	get the product id, if it is in the url
			if (strpos($data['UserAction']['url'], '/products/voucher/') !== false) {
				$product_id = substr($data['UserAction']['url'], strpos($data['UserAction']['url'], '/products/voucher/') + strlen('/products/voucher/'));
				if (strpos($product_id, '/') !== false) {
					$product_id = substr($product_id, 0, strpos($product_id, '/'));
				}
				$data['UserAction']['product_id'] = $product_id;
			} else if (strpos($data['UserAction']['url'], '/products/view/') !== false) {
				$product_id = substr($data['UserAction']['url'], strpos($data['UserAction']['url'], '/products/view/') + strlen('/products/view/'));
				if (strpos($product_id, '/') !== false) {
					$product_id = substr($product_id, 0, strpos($product_id, '/'));
				}
				$data['UserAction']['product_id'] = $product_id;
				/* Dont record if it has expired */
				$the_product = $this->Domain->Client->Product->findById($product_id);
				if($the_product['Product']['expires'] != '0000-00-00' and strtotime($the_product['Product']['expires']) < mktime()){
					$data['UserAction']['product_id'] = 0;
				}
			}

			$this->Domain->Client->User->UserAction->save($data);

		}
	}

	/**
	 * Return a client with their associated details
	*/
	function getClient($client_id,$dashid=null) {

		$this->Domain->Client->recursive = 1;
		$this->Domain->Client->unbindModel(array('hasAndBelongsToMany' => array('Product', 'Category'),'hasMany' => array('User', 'ClientPage')));
		$client = $this->Domain->Client->findById($client_id);

		$client_country = $this->Domain->query("select `country` from `clients` where `id`=".$client_id);
		$client_country = $client_country['0']['clients']['country'];

		//categories for myrewards
		$sql = "SELECT DISTINCT Category.* FROM categories AS Category
				LEFT JOIN clients_categories AS ClientsCategory ON Category.id = ClientsCategory.category_id
				WHERE ClientsCategory.client_id = '". $client_id ."' and Category.countries = '".$client_country."'
				order by Category.name";

		$categories = $this->Domain->query($sql);

		$client['Categories'] = array();
				foreach ($categories as $category) {
					$client['Categories'][$category['Category']['id']] = $category['Category']['name'];
		}

		//categories for send a friend
		$sqlSaf = "SELECT DISTINCT Category.* FROM categories AS Category
				LEFT JOIN clients_categories AS ClientsCategory ON Category.id = ClientsCategory.category_id
				LEFT JOIN categories_products AS CategoriesProduct ON ClientsCategory.category_id = CategoriesProduct.category_id
				LEFT JOIN clients_products_safs AS ClientsProductsSaf ON ClientsProductsSaf.client_id = '". $client_id. "'
				AND CategoriesProduct.product_id = ClientsProductsSaf.product_id
				WHERE ClientsCategory.client_id = '". $client_id ."' and Category.countries = '".$client_country."'
				AND ClientsProductsSaf.id IS NOT NULL order by Category.name";

		$categories_saf = $this->Domain->query($sqlSaf);

		$client['CategoriesSAF'] = array();
				foreach ($categories_saf as $category) {
					$client['CategoriesSAF'][$category['Category']['id']] = $category['Category']['name'];
		}

		//categories for points
		$sqlPoints = "SELECT DISTINCT Category.* FROM categories AS Category
				LEFT JOIN clients_categories AS ClientsCategory ON Category.id = ClientsCategory.category_id
				LEFT JOIN categories_products AS CategoriesProduct ON ClientsCategory.category_id = CategoriesProduct.category_id
				LEFT JOIN clients_products_points AS ClientsProductsPoint ON ClientsProductsPoint.client_id = '". $client_id. "'
				AND CategoriesProduct.product_id = ClientsProductsPoint.product_id
				WHERE ClientsCategory.client_id = '". $client_id ."' and Category.countries = '".$client_country."'
				AND ClientsProductsPoint.id IS NOT NULL order by Category.name";

		$categories_points = $this->Domain->query($sqlPoints);

		$client['CategoriesPoints'] = array();
				foreach ($categories_points as $category) {
					$client['CategoriesPoints'][$category['Category']['id']] = $category['Category']['name'];
		}

		$client['Page'] = $this->getClientPages($client,$dashid);

		$this->Domain->Client->Category->recursive = -1;
		//$client['CategoryTree'] = $this->Domain->Client->Category->findAllThreaded("Category.deleted != 1 AND Category.id IN ('". implode("','", array_keys($client['Categories'])). "')", array('id', 'parent_id', 'name'), 'Category.sort, Category.name', 'DISTINCT Category.name');

		$countrysql ="SELECT Country.id, Country.name  FROM countries AS Country
					JOIN clients_countries AS ClientsCountry ON (ClientsCountry.client_id IN (".$client_id.") AND ClientsCountry.country_id = Country.id)
					UNION
					SELECT Country.id, Country.name FROM countries AS Country, clients AS Client where Client.id = ".$client_id."
					AND Client.country = Country.name";

		$countries = $this->Domain->query($countrysql);
		$client['Countries'] = array();
		foreach ($countries as $country) {
			foreach($country as $cc)
			{
				$client['Countries'][$cc['name']] = $cc['name'];
			}
		}

		 $statesql1 ="SELECT State.state_cd, State.state_name FROM states AS State, countries as Country, clients as Client,
		    merchant_addresses as MerchantAddress, products as Product where State.country_cd = Country.id and Country.name='".$client_country."'
		    and MerchantAddress.merchant_id = Product.merchant_id and MerchantAddress.state = State.state_cd
		    and Client.id = ".$client_id." order by State.state_name";

	    	$this->loadModel("State");
			$states = $this->State->query($statesql1);

			$client['States'] = array();
			foreach ($states as $state) {
				foreach($state as $st)
				{
					$client['States'][$st['state_cd']] = $st['state_name'];
				}
			}

		// reindex the client pages
		if (isset($client['ClientPage'])) {
			$tmp_array = array();
			foreach ($client['ClientPage'] as $client_page) {
				$tmp_array[$client_page['id']] = $client_page;
			}
			$client['ClientPage'] = $tmp_array;
		}

		return $client;
	}

	/**
	 * Return an array of pages for the given client
	 *
	*/
	function getClientPages($client,$dashid=null) {
		//	public or logged in pages?
		if ($this->Session->check('user') || (isset($client['Domain']['public']) && $client['Domain']['public'] == '1')) {
			$public = 0;
		} else {
			$public = 1;
		}

		$pages = $this->_pages($client['Client']['program_id'], $client['Client']['id'], false,$dashid,$public);		//	get the tree of available pages for this client

		return $pages;
	}

	/**
	 * These functions deal with getting the data for the client and program pages,
	 * Use them to get all the pages, then override the settings for clients and programs
	*/

	/**
	 * Return an array of pages
	 *
	 * @param	int	$program_id
	 * @param	int	$client_id
	 * @param	bool	$return_disabled		decide if disabled items are returned
	 * @param	int	$public 						return public items if true, otherwise member only items
	*/
	function _pages($program_id = null, $client_id = null, $return_disabled = true,$dash_id,$public = 0) {

		$this->Domain->Client->Program->ProgramPage->Page->bindModel(
			array(
				'hasOne' => array(
					'ProgramPage' =>array(
						'conditions' => "ProgramPage.program_id = '". $program_id. "'  AND ProgramPage.dashboard_id = '". $dash_id. "'",
						'order' => 'Page.sort, Page.title',
					),
					'ClientPage' =>array(
						'conditions' => "ClientPage.client_id = '". $client_id. "' AND ClientPage.dashboard_id = '". $dash_id. "'",
						'order' => 'Page.sort, Page.title',
					)
				)
			)
		);

		$pages = $this->Domain->Client->Program->ProgramPage->Page->find('threaded', array('conditions'=>array(
			"(ProgramPage.program_id = '". $program_id. "' OR ProgramPage.program_id IS NULL)
			 AND (ClientPage.client_id = '". $client_id. "' OR ClientPage.client_id IS NULL)
			 AND (ClientPage.dashboard_id = '". $dash_id. "' OR ClientPage.dashboard_id IS NULL)
			 AND (ProgramPage.dashboard_id = '". $dash_id. "' OR ProgramPage.dashboard_id IS NULL)
			 AND Page.public = '". $public. "'"),
			 'fields'=>array(
			 'Page.id','Page.parent_id','Page.name','Page.title','Page.enabled',
			 'ClientPage.id','ClientPage.title','ClientPage.title_override','ClientPage.enabled','ClientPage.enabled_override',
			 'ProgramPage.id','ProgramPage.title','ProgramPage.enabled','ProgramPage.title_override','ProgramPage.enabled_override',
			 ))
		);

		$this->return_disabled = $return_disabled;

		$pages =  $this->_PageDefaults($pages);

		return $pages;
	}


	/**
	 * Return a single page
	*/
	function _page($page_id, $program_id = null, $client_id = null,$dashId) {

		$this->Domain->Client->Program->ProgramPage->Page->bindModel(
			array(
				'hasOne' => array(
					'ProgramPage' =>array(
						'conditions' => "ProgramPage.program_id = '". $program_id. "' AND ProgramPage.dashboard_id = '". $dashId. "'",
						'order' => 'Page.sort, Page.title',
					),
					'ClientPage' =>array(
						'conditions' => "ClientPage.client_id = '". $client_id. "' AND ClientPage.dashboard_id = '". $dashId. "'",
						'order' => 'Page.sort, Page.title',
					)
				)
			)
		);
		$page = $this->Domain->Client->Program->ProgramPage->Page->find('first', array('conditions'=>array(
			"(ProgramPage.program_id = '". $program_id. "' OR ProgramPage.program_id IS NULL)
			 AND (ClientPage.client_id = '". $client_id. "' OR ClientPage.client_id IS NULL)
			 AND (ClientPage.dashboard_id = '". $dashId. "' OR ClientPage.dashboard_id IS NULL)
			 AND (ProgramPage.dashboard_id = '". $dashId. "' OR ProgramPage.dashboard_id IS NULL)
			 AND Page.id = '". $page_id. "'"
		)));

		return $this->_PageDefault($page, $client_id);
	}

	/**
	 * Return a single page for the current client
	 *
	 * @param str $page_name Name of the page in the pages table
	*/
	function _clientPage($page_name) {



		$this->Domain->Client->Program->ProgramPage->Page->bindModel(
			array(
				'hasOne' => array(
					'ProgramPage' =>array(
						'conditions' => "ProgramPage.program_id = '". $this->Session->read('client.Client.program_id'). "'",
						'order' => 'Page.sort, Page.title',
					),
					'ClientPage' =>array(
						'conditions' => "ClientPage.client_id = '". $this->Session->read('client.Client.id'). "'",
						'order' => 'Page.sort, Page.title',
					)
				)
			)
		);

		$page = $this->Domain->Client->Program->ProgramPage->Page->find('first', array('conditions'=>array(
			"(ProgramPage.program_id = '". $this->Session->read('client.Client.program_id'). "' OR ProgramPage.program_id IS NULL)
			 AND (ClientPage.client_id = '". $this->Session->read('client.Client.id'). "' OR ClientPage.client_id IS NULL) AND
			 Page.name = '". $page_name. "'"
		)));

		return $this->_PageDefault($page);
	}
	function _clientPageNew($page_name,$dashId) {



			$this->Domain->Client->Program->ProgramPage->Page->bindModel(
				array(
					'hasOne' => array(
						'ProgramPage' =>array(
							'conditions' => "ProgramPage.program_id = '". $this->Session->read('client.Client.program_id'). "'",
							'order' => 'Page.sort, Page.title',
						),
						'ClientPage' =>array(
							'conditions' => "ClientPage.client_id = '". $this->Session->read('client.Client.id'). "' AND ClientPage.dashboard_id = '". $dashId. "' ",
							'order' => 'Page.sort, Page.title',
						)
					)
				)
			);

			$page = $this->Domain->Client->Program->ProgramPage->Page->find('first', array('conditions'=>array(
				"(ProgramPage.program_id = '". $this->Session->read('client.Client.program_id'). "' OR ProgramPage.program_id IS NULL)
				 AND (ClientPage.client_id = '". $this->Session->read('client.Client.id'). "' OR ClientPage.client_id IS NULL) AND
				 ClientPage.dashboard_id = '".$dashId."' AND Page.name = '". $page_name. "'"
			)));

			return $this->_PageDefault($page);
		}


	/**
	 * Modify the pages array to use default values from Page controller if required
	*/
	function _pageDefaults($pages) {

		foreach ($pages as $key => $page) {

			$page = $this->_pageDefault($page);

			if ($this->return_disabled || $page['ClientPage']['enabled'] == '1') {
				$pages[$key] = $this->_pageDefault($page);

			} else {
				unset($pages[$key]);
			}
		}
		return $pages;
	}

	function _pageDefault($page, $client_id = false) {

		if ($page) {
			// add defaults if there is no record for entrys
			$fields = array('enabled', 'title', 'h1', 'h2', 'h3', 'content', 'special_offers', 'page_title', 'meta_description', 'meta_keywords', 'meta_robots','whats_new_module');

			if ($client_id) {
				$this->Domain->Client->recursive = 0;
				$client = $this->Domain->Client->findById( $client_id );
				if (!empty($client)) {
					$replacements = array(
						'[[client_name]]' => $client['Client']['name'],
						'[[client_email]]' => $client['Client']['email'],
						'[[program_name]]' => $client['Program']['name']
					);
				} else {
					$replacements = array(
						'[[client_name]]' => '',
						'[[client_email]]' => '',
						'[[program_name]]' => ''
					);
				}
			} else {
				$replacements = array(
					'[[client_name]]' => $this->Session->read('client.Client.name'),
					'[[client_email]]' => $this->Session->read('client.Client.email'),
					'[[program_name]]' => $this->Session->read('client.Program.name')
				);
			}
			foreach ($fields as $field) {

				if (isset($page['Page'][$field])) {
					if (!isset($page['ProgramPage'][$field. '_override']) || $page['ProgramPage'][$field. '_override'] == '' || $page['ProgramPage'][$field. '_override'] == '1') {
						$page['ProgramPage'][$field] = $page['Page'][$field];
					}
					if (!isset($page['ClientPage'][$field. '_override']) || $page['ClientPage'][$field. '_override'] == '' || $page['ClientPage'][$field. '_override'] == '1') {
						$page['ClientPage'][$field] = $page['ProgramPage'][$field];
					}

					if (!isset($page['ClientPage'][$field. '_override']) || $page['ClientPage'][$field. '_override'] == '') {

						$page['ClientPage'][$field. '_override'] = 1;
					}

					$page['ClientPage'][$field] = str_replace(array_keys($replacements), $replacements, $page['ClientPage'][$field]);
				}

			}

			//	always set a navigation title value
			if ($page['ProgramPage']['title'] == '') {
				$page['ProgramPage']['title'] = $page['Page']['title'];
			}
			if ($page['ClientPage']['title'] == '') {
				$page['ClientPage']['title'] = $page['ProgramPage']['title'];
			}

			// dont forget the kids
			if (isset($page['children'])) {
				$page['children'] = $this->_pageDefaults($page['children'], $client_id);
			}
		}
		return $page;
	}

	/**
	 * Return products for the given client
	 *
	  * @param		int	$client_id	id of the client to search for
	  * @param		str	$conditions	string of conditions to search for
	  * @param		str	$order	string of SQL order statement
	  * @param		int	$limit	number to limit search by
	  * @param		bool	$primary_address	weather to limit the address to the primary only
	*/
	function _clientProducts($client_id, $conditions = null, $order = 'RAND()', $limit = 10, $primary_address=true) {

		$sql = "SELECT DISTINCT Product.*, MerchantAddress.* FROM products AS Product
						LEFT JOIN merchants AS Merchant ON Product.merchant_id = Merchant.id
						LEFT JOIN merchant_addresses AS MerchantAddress ON Merchant.id = MerchantAddress.merchant_id ";
		if ($primary_address) {
			$sql .= "AND MerchantAddress.primary = 1 ";
		}
		$sql .= "	LEFT JOIN categories_products AS CategoriesProduct ON Product.id = CategoriesProduct.product_id
						LEFT JOIN clients_categories AS ClientsCategory ON CategoriesProduct.category_id = ClientsCategory.category_id AND ClientsCategory.client_id = '". $client_id. "'
						LEFT JOIN clients_products AS ClientsProduct ON Product.id = ClientsProduct.product_id AND (ClientsProduct.client_id = '". $client_id. "' OR ClientsProduct.client_id = 0)
						WHERE CategoriesProduct.id IS NOT NULL
						AND ClientsCategory.id IS NOT NULL
						AND ClientsProduct.id IS NOT NULL
						AND
						";
		$sql .= $conditions;
		if (!empty($order)) {
			$sql .= ' ORDER BY '. $order;
		}
		if (is_numeric($limit)) {
			$sql .= ' LIMIT '. $limit;
		}

		$products = $this->Domain->query($sql);
		return $products;
	}

	/**
	 * Return an array of whats new products
	 *
	 * @param int $client_id Id of the client to retreive products for
	 * @papam int $limit number of products to return, if null do not limit
	*/
	function _whatsNewProducts($client_id, $limit=null) {

		$sql = "SELECT DISTINCT Product. * , Merchant.*, MerchantAddress.* FROM products AS Product
					INNER JOIN clients_whats_new_products AS ClientsWhatsNewProduct ON Product.id = ClientsWhatsNewProduct.product_id AND ClientsWhatsNewProduct.client_id = '". $client_id. "'
					INNER JOIN categories_products AS CategoriesProduct ON Product.id = CategoriesProduct.product_id
					INNER JOIN clients_categories AS ClientsCategory ON CategoriesProduct.category_id = ClientsCategory.category_id AND ClientsCategory.client_id = '". $client_id. "'
					INNER JOIN clients_products AS ClientsProduct ON Product.id = ClientsProduct.product_id AND (ClientsProduct.client_id = '". $client_id. "' OR ClientsProduct.client_id = 0)
					LEFT JOIN merchant_addresses AS MerchantAddress ON Product.merchant_id = MerchantAddress.merchant_id  AND MerchantAddress.`primary` = 1
					LEFT JOIN merchants AS Merchant ON Product.merchant_id = Merchant.id
					WHERE Product.active = 1
					AND Product.created >= DATE_SUB(NOW(), INTERVAL 1 MONTH)
					AND (Product.expires = '0000-00-00' OR Product.expires >= NOW())
					ORDER BY RAND() ";
		if ($limit) {
			$sql .= " LIMIT ". $limit;
		}

		return $this->Domain->Client->Product->query($sql);
	}

	/**
	 * set the redirect url
	*/
	function _setRedirect() {
		if (isset($_SERVER['HTTP_REFERER']) && $_SERVER['HTTP_REFERER'] != 'http://'. $_SERVER['HTTP_HOST']. $_SERVER['REQUEST_URI']) {
			$this->Session->write('redirect', $_SERVER['HTTP_REFERER']);
		}
	}

	/**
	 * redirect to the session redirect if specified, otherwise redirect to the given url
	*/
	function redirect_session($redirect, $status = null, $exit = false) {

		if ($this->Session->read('redirect')) {
			$redirect = $this->Session->read('redirect');
			$this->Session->delete('redirect');
		}

		$this->redirect($redirect, $status, $exit);
	}

	/**
	 * Save some log details
	 *	@param	array	array of log data
	 * @param	bool		if true, check the related values
	*/
	function save_log($data, $hasMany=false) {

		$data['id'] = false;
		$this->Log->save(array('Log' => $data));
		if (isset($data['foreign_model']) && isset($data['old_data'][$data['foreign_model']]) && isset($data['new_data'][$data['foreign_model']])) {
			foreach ($data['new_data'] as $foreign_model => $new_data) {
				foreach ($new_data as $key => $value) {
					if (!is_array($value) && isset($data['old_data'][$foreign_model][$key])  && ($data['old_data'][$foreign_model][$key] != $data['new_data'][$foreign_model][$key])) {

						$this->Log->LogChange->save(array(
							'id' => '',
							'log_id' => $this->Log->id,
							'key' => $key,
							'foreign_model' => $foreign_model,
							'old_value' => $data['old_data'][$foreign_model][$key],
							'new_value' => $data['new_data'][$foreign_model][$key]
						));
					}
				}
			}
		}
	}

			public function recordActivity($prdts,$action){

					    $this->loadModel('Track');
					    $pages = str_replace("/index.php",'', $_SERVER['PHP_SELF']);
					    $pages = explode("/", $pages);
						$content = array();
						$cid = $_SESSION['client']['Client']['id'];
						foreach($prdts as $pr){
						if($pr['id']!=""){
						$products[] = array('Track'=>array('product_id'=>$pr,'key'=>$_GET['key'],'state'=>$_GET['state'],'ctry'=>$_GET['ctry'],'cat'=>$_GET['cat'] ,'model'=>$this->params['controller'], 'action'=>$action,'user_ip'=> $_SERVER['REMOTE_ADDR'],'user_browser'=> $_SERVER['HTTP_USER_AGENT'],'clicked_from'=>$this->params['controller'].'/'.$this->params['action'],'uid'=> $_SESSION['user']['User']['id'],'client_id'=>$cid));
							}
						}
						if($products!=""){
						$this->Track->saveAll($products);
						return true;
						}



				}


					public function admin_recordActivity($post_data,$login_user){

					    $this->loadModel('AdminTrack');
					    $pages = str_replace("/index.php",'', $_SERVER['PHP_SELF']);
					    $pages = explode("/", $pages);

						$change_data = json_encode($post_data);

						$data = array('AdminTrack'=>array('model'=>$this->params['controller'], 'user_ip'=> $_SERVER['REMOTE_ADDR'],'user_browser'=> $_SERVER['HTTP_USER_AGENT'],'clicked_from'=>$this->params['controller'].'/'.$this->params['action'],'uid'=> $_SESSION['user']['User']['id'],'login_user'=>$login_user,'change_data'=>"$change_data"));

					 $this->AdminTrack->saveAll($data);

						}


			public function redir_err(){

			$req =$this->viewPath;
			if($req=='Errors'){
				if($this->Session->read('dashboardidinsession')==1){
					$this->redirect('/myrewards');
				}

				if($this->Session->read('dashboardidinsession')==2){
					$this->redirect('/send_a_friend');
				}

				if($this->Session->read('dashboardidinsession')==3){
					$this->redirect('/mypoints');
				}

			}

		}







}
