<?php
class LogsController extends AppController {

	public $uses = array('Log');
	/*
	 * Return a ajax page of log items
	 *
	 * @param	str	$foreign_model model we are getting for
	 * @param	int	$foreign_model (optional) id of the model, if false, return empth log results
	*/
	function admin_ajax_list_new($foreign_model, $foreign_id = false) {
			$this->layout = 'ajax';

			$this->Log->recursive = 1;
			$conditions = array('foreign_id' => $foreign_id, 'foreign_model' => $foreign_model);

			$this->set('logs', $this->paginate('Log', $conditions));
	}

	/*
	 * Return a ajax list of changes for a log entry
	*/
	function admin_ajax_changes($id) {
		$this->layout = 'ajax';

		$log_changes = $this->Log->LogChange->findAllByLogId($id);
		$this->set('log_changes', $log_changes);
	}
}
?>