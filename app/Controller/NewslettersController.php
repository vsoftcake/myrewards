<?php
class NewslettersController extends AppController {

	var $components = array( 'SwiftMailer', 'MailQueue','File');

	function admin_index() {
		$name = Inflector::singularize($this->name);
		$conditions = array();

		if (!empty($this->request->data[$name]['search'])) {
			$this->Session->write($name.'.search', $this->request->data[$name]['search']);
		}
		$show =0;
		if ($this->Session->check($name.'.search')) {
			$this->request->data[$name]['search'] = $this->Session->read($name.'.search');
			$show=1;
		}

		if  (!empty($this->request->data[$name]['search']) && is_numeric($this->request->data[$name]['search'])) {
			$conditions = array("Program.id"=>$this->request->data[$name]['search'], "Newsletter.delete"=>0);
		}else{
			$conditions = array();
		}

		$programs = $this->Newsletter->Program->find("list");
		$this->set('programs',$programs);
		$this->set('show',$show);

		if(!empty($conditions)) {
			$this->Newsletter->recursive = 0;
			$newsletters = $this->Newsletter->find('all', array('conditions'=>$conditions, 'order'=>'Newsletter.modified desc'));

			foreach ($newsletters as $key => $newsletter) {

				$this->Newsletter->NewslettersEmail->recursive = -1;
				$newsletter_emails = $this->Newsletter->NewslettersEmail->find("all", array("conditions"=>array("newsletter_id"=>$newsletter['Newsletter']['id']), "group"=>"status", "fields"=>"status, count(id) as count"));
				foreach ($newsletter_emails as $status) {
					$newsletters[$key]['Total'][$status['NewslettersEmail']['status']] = $status[0]['count'];
				}

				$newsletters[$key]['Client']['count'] = $this->Newsletter->NewslettersClient->find("count", array("conditions"=>array("newsletter_id"=>$newsletter['Newsletter']['id'])));
			}
			$this->set(Inflector::underscore($this->name), $newsletters);
		}
	}

	function admin_edit($id = null){

		if (!empty($this->request->data)) {

			//copy newsletter
			if((isset($_POST['copy']) && !empty($this->request->data['Newsletter']['copy'])) || (isset($_POST['view_copy']))){

					if(!isset($_POST['view_copy']))
					{
						$id = $this->request->data['Newsletter']['copy'];
					}
					$this->Newsletter->unbindModel(array('hasMany' => array('NewslettersEmail')));
					$this->request->data = $this->Newsletter->read(null,$id);
					unset($this->request->data['Newsletter']['id']);
					unset($this->request->data['Newsletter']['status']);
					unset($this->request->data['Newsletter']['name']);

					if ($this->Session->check('Newsletter.search')) {
							$program_id = $this->Session->read('Newsletter.search');
							if($id==null){
								//come up a newsletter list under this program
								$conditions_newsletter= "(`Newsletter`.`program_id` =  '". $program_id . "' AND `Newsletter`.`delete` = 0)";
								$newsletter_options = $this->Newsletter->find("list",array('conditions'=>$conditions_newsletter));
								$this->set("newsletter_options", $newsletter_options);
							}
							//come up a client list related to the program_id
							$conditions_client['conditions'] = "(`Client`.`program_id` =  '". $program_id . " And Client.newsletter  = 1')";
							$client_options = $this->Newsletter->Client->find('list',$conditions_client);
							$this->set("clients", $client_options);
							$this->request->data['Newsletter']['program_id'] = $program_id;
					}else{
							$this->set("newsletter_options", array());
							$this->set("clients", $this->Newsletter->Client->find("list"));
					}


					if(empty($this->request->data['Newsletter']['user_id'])) {
						$this->request->data['Newsletter']['user_id'] = $this->Session->read('user.User.id');
					}

					//change data format to match the product_short layout
					if(!empty($this->request->data['NewslettersShortproduct'])){
						foreach($this->request->data['NewslettersShortproduct'] as $key=>$short_product){
							$category_id = $short_product['category_id'];
							$products_short['NewsletterCategory'][$category_id]['category_id'] = $category_id;
							$products_short['NewsletterCategory'][$category_id]['NewsletterOffer'][$key]['NewsletterOffer']['offer_id'] = $short_product['product_id'];
							$products_short['NewsletterCategory'][$category_id]['NewsletterOffer'][$key]['NewsletterOffer']['id'] = $short_product['id'];
						}
						$this->set("product_shorts",$products_short);
					}

					//change data format to match the product_short layout
					if(!empty($this->request->data['NewslettersProduct'])){
						foreach($this->request->data['NewslettersProduct'] as $key=>$product){
							$category_id = $product['category_id'];
							$products['NewsletterCategory'][$category_id]['category_id'] = $category_id;
							$products['NewsletterCategory'][$category_id]['NewsletterOffer'][$key]['NewsletterOffer']['offer_id'] = $product['product_id'];
							$products['NewsletterCategory'][$category_id]['NewsletterOffer'][$key]['NewsletterOffer']['id'] = $product['id'];
						}
						$this->set("products",$products);
					}

					//get all related categories
					$this->Newsletter->Program->unbindModel(
						array(
						'hasMany' => array('Client', 'ProgramPage'),
						'hasAndBelongsToMany' => array('Product', 'SpecialProduct','WhatsNewProduct','Module'),
						)
					);
					$programs = $this->Newsletter->Program->read(null, $program_id);
					if (!empty($programs['Category'])) {
						 $categories = Set::combine($programs['Category'] ,'{n}.id' , '{n}.description', null);
					}
					//print_r($categories);
					$this->set('categories',$categories);

					//	get newsletter states
					$this->loadModel('Country');
					$countries = $this->Country->find('list');

					$this->set('newsletter_countries',$countries);

					$statesql1 ="SELECT State.state_cd, State.state_name FROM states AS State, newsletter_states  as NewsletterState
								where State.country_cd = NewsletterState.country and NewsletterState.newsletter_id='".$id."'
								order by State.state_name";

						$this->loadModel("State");
						$states_list = $this->State->query($statesql1);

						$states = array();
						foreach ($states_list as $state) {
							foreach($state as $st)
							{
								$states[$st['state_cd']]= $st['state_name'];
							}
						}
					$this->set('states',$states);

					$selected_newsletter_states = array();
					$selected_newsletter_country = '';
					if (isset($this->request->data['NewsletterState'])) {
						foreach ($this->request->data['NewsletterState'] as $newsletter_state) {
							$selected_newsletter_states[$newsletter_state['state']] = $newsletter_state['state'];
							$selected_newsletter_country= $newsletter_state['country'];
						}
					}
					$this->set('selected_newsletter_states', $selected_newsletter_states);
					$this->set('selected_newsletter_country', $selected_newsletter_country);

					$this->loadModel("Dashboard");
					$this->set('dashboard', $this->Dashboard->find('superlist',array('fields'=>array('Dashboard.id','Dashboard.type'),'separator'=>' * ')));
					// lookup options
					$this->set('fonts', $this->Newsletter->getEnumValues('font'));
			}else{
				if($id ==null) {
					$this->request->data['Newsletter']['id'] = "";
				}

				if ($this->request->data['Image1']['file']['name'] != '') {
					$extension = substr($this->request->data['Image1']['file']['name'], strrpos($this->request->data['Image1']['file']['name'], '.') + 1);
					$this->request->data['Newsletter']['image1_ext'] = $extension;
				}
				if ($this->request->data['Image2']['file']['name'] != '') {
					$extension = substr($this->request->data['Image2']['file']['name'], strrpos($this->request->data['Image2']['file']['name'], '.') + 1);
					$this->request->data['Newsletter']['image2_ext'] = $extension;
				}
				if ($this->request->data['Image3']['file']['name'] != '') {
					$extension = substr($this->request->data['Image3']['file']['name'], strrpos($this->request->data['Image3']['file']['name'], '.') + 1);
					$this->request->data['Newsletter']['image3_ext'] = $extension;
				}
				if ($this->request->data['Image4']['file']['name'] != '') {
					$extension = substr($this->request->data['Image4']['file']['name'], strrpos($this->request->data['Image4']['file']['name'], '.') + 1);
					$this->request->data['Newsletter']['image4_ext'] = $extension;
				}

				//save newsletter
				if($this->Newsletter->save($this->request->data)){
					$newLtrId = $this->Newsletter->id;
					//Save categories products
					$this->Newsletter->NewslettersClient->deleteAll(array('newsletter_id'=>$newLtrId));
					$newLtrClientData = array();
					foreach($this->request->data['Client']['Client'] as $newsLtrClient){
						if(!empty($newsLtrClient)){
							$newLtrClientData[] = array('newsletter_id'=>$newLtrId, 'client_id'=>$newsLtrClient);
						}
					}
					if(!empty($newLtrClientData)){
						$this->Newsletter->NewslettersClient->saveMany($newLtrClientData);
					}

						if(empty($this->request->data['Newsletter']['id'])) {
							$this->request->data['Newsletter']['id'] = $this->Newsletter->getLastInsertID();
						}
						if (isset($this->request->data['ImageDelete1']))
						{
							if ($this->request->data['ImageDelete1']['file'] == '1') {
								$this->File->deleteByPrefix('newsletters/image1/'.$this->Newsletter->id);
								$this->request->data['Newsletter']['image1_ext']='';
								$this->Newsletter->save($this->request->data);
							}
						}

						if (isset($this->request->data['ImageDelete2']))
						{
							if ($this->request->data['ImageDelete2']['file'] == '1') {
								$this->File->deleteByPrefix('newsletters/image2/'.$this->Newsletter->id);
								$this->request->data['Newsletter']['image2_ext']='';
								$this->Newsletter->save($this->request->data);
							}
						}

						if (isset($this->request->data['ImageDelete3']))
						{
							if ($this->request->data['ImageDelete3']['file'] == '1') {
								$this->File->deleteByPrefix('newsletters/image31/'.$this->Newsletter->id);
								$this->request->data['Newsletter']['image3_ext']='';
								$this->Newsletter->save($this->request->data);
							}
						}

						if (isset($this->request->data['ImageDelete4']))
						{
							if ($this->request->data['ImageDelete4']['file'] == '1') {
								$this->File->deleteByPrefix('newsletters/image4/'.$this->Newsletter->id);
								$this->request->data['Newsletter']['image4_ext']='';
								$this->Newsletter->save($this->request->data);
							}
						}

						if ($this->request->data['Image1']['file']['name'] != '')
						{
							move_uploaded_file($this->request->data['Image1']['file']['tmp_name'], NEWSLETTER_IMAGE1_PATH. $this->Newsletter->id. '.'. $this->request->data['Newsletter']['image1_ext']);
						}

						if ($this->request->data['Image2']['file']['name'] != '')
						{
							move_uploaded_file($this->request->data['Image2']['file']['tmp_name'], NEWSLETTER_IMAGE2_PATH. $this->Newsletter->id. '.'. $this->request->data['Newsletter']['image2_ext']);
						}

						if ($this->request->data['Image3']['file']['name'] != '')
						{
							move_uploaded_file($this->request->data['Image3']['file']['tmp_name'], NEWSLETTER_IMAGE3_PATH. $this->Newsletter->id. '.'. $this->request->data['Newsletter']['image3_ext']);
						}

						if ($this->request->data['Image4']['file']['name'] != '')
						{
							move_uploaded_file($this->request->data['Image4']['file']['tmp_name'], NEWSLETTER_IMAGE4_PATH. $this->Newsletter->id. '.'. $this->request->data['Newsletter']['image4_ext']);
						}

						//delete all data related
						$sql = "DELETE FROM newsletters_short_products WHERE newsletter_id = '".$this->request->data['Newsletter']['id']. "'";
						$this->Newsletter->query($sql);

						$sql = "DELETE FROM newsletters_products WHERE newsletter_id = '". $this->request->data['Newsletter']['id']. "'";
						$this->Newsletter->query($sql);

						$sql = "DELETE FROM newsletter_states WHERE newsletter_id = '". $this->request->data['Newsletter']['id']. "'";
						$this->Newsletter->query($sql);

						if (isset($this->request->data['NewsletterState']))
						{
							$country = $this->request->data['NewsletterState']['country'];
							foreach ($this->request->data['NewsletterState']['state'] as $state) {
								$this->Newsletter->NewsletterState->save(array('id' => '', 'newsletter_id' => $this->request->data['Newsletter']['id'], 'state' => $state, 'country' => $country));
							}
						}

						//change the data format for short product
						if(!empty($this->request->data['NewslettersShortProduct'])){
							$NewslettersShortProduct = array();
							foreach($this->request->data['NewslettersShortProduct'] as $key => $product) {
								if(!empty($product)){
									$data['NewslettersShortProduct']['id'] = '';
									$data['NewslettersShortProduct']['product_id'] = $product;
									$data['NewslettersShortProduct']['newsletter_id'] = trim($this->request->data['Newsletter']['id']);
									$this->Newsletter->NewslettersShortProduct->save($data);
								}
							}
						}

						$prodIds = "";

						//change the data format to product
						if(!empty($this->request->data['Product'])){
							foreach($this->request->data['Product']['NewsletterCategory'] as $category){
								if(!empty($category ['NewsletterOffer'])){
									foreach($category ['NewsletterOffer'] as $offer){
										if(is_numeric($offer) && is_numeric($category['category_id'])){
											$this->request->data['NewslettersProduct']['id'] = 0;
											$this->request->data['NewslettersProduct']['category_id'] = $category['category_id'];
											$this->request->data['NewslettersProduct']['product_id'] = trim($offer);
											$this->request->data['NewslettersProduct']['newsletter_id'] = trim($this->request->data['Newsletter']['id']);
											$this->Newsletter->NewslettersProduct->save($this->request->data);
											$prodIds .= strlen($prodIds)>0?",".$this->request->data['NewslettersProduct']['product_id']:$this->request->data['NewslettersProduct']['product_id'];
										}
									}
								}
							}
						}
							CakeLog::debug("-prods-$prodIds");


						if($this->request->data['Newsletter']['limit_products_to_state'] == 1)
						{
							$delSql = "DELETE from newsletters_temp_clients WHERE newsletter_id=".$this->request->data['Newsletter']['id'];
							$this->Newsletter->NewslettersTempClient->query($delSql);

							$delProdSql = "DELETE from newsletters_temp_states WHERE newsletter_id=".$this->request->data['Newsletter']['id'];
							$this->Newsletter->NewslettersTempState->query($delProdSql);

							$clientIds = "0";
							foreach($this->request->data['Client']['Client'] as $client)
							{
								$clientIds .= (strlen($clientIds) && $client>0)>0?",".$client:$client;
							}

							$prodIds = !empty($prodIds)?$prodIds:"''";
							$national_qry = "SELECT b.client_id FROM products a, clients_products b WHERE a.id=b.product_id AND a.available_national=1
							AND b.client_id IN ($clientIds) AND b.product_id IN ($prodIds) group by b.client_id";
							$clientNationalList = $this->Newsletter->query($national_qry);
							CakeLog::debug("-national_qry-$national_qry");

							$nationalClients = array();
							foreach($clientNationalList as $clientNational) {
								array_push($nationalClients,$clientNational['b']['client_id']);
							}

							$client_qry = "SELECT DISTINCT client_id FROM clients_products WHERE client_id IN ($clientIds) AND product_id IN ($prodIds)";
							$tempClients = $this->Newsletter->query($client_qry);
							CakeLog::debug("-clients-$client_qry");

							$client_array = array();
							foreach($tempClients as $tempClient)
							{
								$tempClientId = $tempClient['clients_products']['client_id'];
								if($tempClientId==0){
									foreach($this->request->data['Client']['Client'] as $client){
										if(!empty($client) && !in_array($client, $client_array)){
											$prdtNational = in_array($client,$nationalClients)?1:0;
											$this->request->data['NewslettersTempClient']['id'] = 0;
											$this->request->data['NewslettersTempClient']['client_id'] = $client;
											$this->request->data['NewslettersTempClient']['newsletter_id'] = trim($this->request->data['Newsletter']['id']);
											$this->request->data['NewslettersTempClient']['available_national'] = $prdtNational;
											$this->Newsletter->NewslettersTempClient->save($this->request->data);
											array_push($client_array, $client);
										}
									}
								}elseif(!in_array($tempClientId, $client_array)){
									$prdtNational = in_array($tempClientId,$nationalClients)?1:0;
									$this->request->data['NewslettersTempClient']['id'] = 0;
									$this->request->data['NewslettersTempClient']['client_id'] = $tempClientId;
									$this->request->data['NewslettersTempClient']['newsletter_id'] = trim($this->request->data['Newsletter']['id']);
									$this->request->data['NewslettersTempClient']['available_national'] = $prdtNational;
									$this->Newsletter->NewslettersTempClient->save($this->request->data);
									array_push($client_array, $tempClientId);
								}
							}

							// Selecting the newsletter states

							$states_qry = "SELECT b.client_id, c.state FROM products a, clients_products b, merchant_addresses c
							WHERE a.id=b.product_id AND a.merchant_id = c.merchant_id
							AND c.state != '' AND a.id IN ($prodIds) AND b.client_id IN ($clientIds) group by b.client_id, c.state";

							$tempStates = $this->Newsletter->query($states_qry);

							$client_array = array();
							foreach($tempStates as $tempState)
							{
								$tempClientId = $tempState['b']['client_id'];
								if($tempClientId==0){
									foreach($this->request->data['Client']['Client'] as $client){
										if(!empty($client) && !in_array($client, $client_array)){
											$this->request->data['NewslettersTempState']['id'] = 0;
											$this->request->data['NewslettersTempState']['newsletter_id'] = trim($this->request->data['Newsletter']['id']);
											$this->request->data['NewslettersTempState']['client_id'] = $client;
											$this->request->data['NewslettersTempState']['state'] = $tempState['c']['state'];
											$this->Newsletter->NewslettersTempState->save($this->request->data);
											array_push($client_array, $client);
										}
									}
								}else { // if(!in_array($tempClientId, $client_array))
									$prdtNational = in_array($tempClientId,$nationalClients)?1:0;
									$this->request->data['NewslettersTempState']['id'] = 0;
									$this->request->data['NewslettersTempState']['client_id'] = $tempClientId;
									$this->request->data['NewslettersTempState']['newsletter_id'] = trim($this->request->data['Newsletter']['id']);
									$this->request->data['NewslettersTempState']['state'] = $tempState['c']['state'];
									$this->Newsletter->NewslettersTempState->save($this->request->data);
									array_push($client_array, $tempClientId);
								}
							}

						}


						if(!empty($_POST['send'])){
								$this->redirect('preview/'.$this->request->data['Newsletter']['id']);
								exit;
						}else{
								$this->Session->setFlash('The Newsletter has been saved');
								$this->redirect(array('action'=>'index'), null, true);
								exit;
						}
				} else {
						$this->Session->setFlash('Please correct the errors below');
						if ($this->Session->check('Newsletter.search')) {
								$program_id = $this->Session->read('Newsletter.search');
								//come up a client list related to the program_id
								$conditions_client['conditions'] = "(`Client`.`program_id` =  '". $program_id . " And Client.newsletter  = 1')";
								$client_options = $this->Newsletter->Client->find('list',$conditions_client);
								$this->set("clients", $client_options);
								$this->request->data['Newsletter']['program_id'] = $program_id;
						}else{
								$this->set("newsletter_options", array());
								$this->set("clients", $this->Newsletter->Client->find("list"));
						}


						if(empty($this->request->data['Newsletter']['user_id'])) $this->request->data['Newsletter']['user_id'] = $this->Session->read('user.User.id');

						//change the data format for short product
						if(!empty($this->request->data['NewslettersShortProduct'])){
							$NewslettersShortProduct = array();
							foreach($this->request->data['NewslettersShortProduct'] as $key => $product) {
								$this->request->data['NewslettersShortProduct'][$key]  = array('product_id' => $product);
							}
						}


						//change the data format to product
						if(!empty($this->request->data['Product'])){
								foreach($this->request->data['Product']['NewsletterCategory'] as $category){
									if(!empty($category ['NewsletterOffer'])){
										foreach($category ['NewsletterOffer'] as $key=>$offer){
											if(is_numeric($offer) && is_numeric($category['category_id'])){
												$category_id = $category['category_id'];
												$products['NewsletterCategory'][$category_id]['category_id'] = $category_id;
												$products['NewsletterCategory'][$category_id]['NewsletterOffer'][$key]['NewsletterOffer']['offer_id'] = $offer;
												$products['NewsletterCategory'][$category_id]['NewsletterOffer'][$key]['NewsletterOffer']['id'] = 0;
											}
										}
									}
								}
								$this->set("products",$products);
						}

						//	change the data format for newsletter states

						if (isset($this->request->data['NewsletterState'])) {
							foreach ($this->request->data['NewsletterState']['state'] as $newsletter_state) {
								$this->request->data['NewsletterState'][] = array('state' => $newsletter_state);
							}
							unset($this->request->data['NewsletterState']['state']);
						}

						//get all related categories
						$this->Newsletter->Program->unbindModel(
							array(
							'hasMany' => array('Client', 'ProgramPage'),
							'hasAndBelongsToMany' => array('Product', 'SpecialProduct','WhatsNewProduct','Module'),
							)
						);
						$programs = $this->Newsletter->Program->read(null, $program_id);
						if (!empty($programs['Category'])) {
							 $categories = Set::combine($programs['Category'] ,'{n}.id' , '{n}.description', null);
						}
						//print_r($categories);
						$this->set('categories',$categories);
				}
			}
		}else{
			$this->request->data['Newsletter']['id'] = $id;

			$this->request->data = $this->Newsletter->read(null,$id);

			if ($this->Session->check('Newsletter.search')) {
					$program_id = $this->Session->read('Newsletter.search');
					if($id==null){
						//come up a newsletter list under this program
						$conditions_newsletter= "(`Newsletter`.`program_id` =  '". $program_id . "' AND `Newsletter`.`delete` = 0)";
						$newsletter_options = $this->Newsletter->find("list",array('conditions'=>$conditions_newsletter));
						$this->set("newsletter_options", $newsletter_options);
					}
					//come up a client list related to the program_id
					$conditions_client['conditions'] = "(`Client`.`program_id` =  '". $program_id . " And Client.newsletter  = 1')";
					$client_options = $this->Newsletter->Client->find('list',$conditions_client);
					$this->set("clients", $client_options);
					$this->request->data['Newsletter']['program_id'] = $program_id;
			}else{
					$this->set("newsletter_options", array());
					$this->set("clients", $this->Newsletter->Client->find("list"));
			}


			if(empty($this->request->data['Newsletter']['user_id'])) $this->request->data['Newsletter']['user_id'] = $this->Session->read('user.User.id');

			//change data format to match the product_short layout
			if(!empty($this->request->data['NewslettersShortproduct'])){
				foreach($this->request->data['NewslettersShortproduct'] as $key=>$short_product){
					$category_id = $short_product['category_id'];
					$products_short['NewsletterCategory'][$category_id]['category_id'] = $category_id;
					$products_short['NewsletterCategory'][$category_id]['NewsletterOffer'][$key]['NewsletterOffer']['offer_id'] = $short_product['product_id'];
					$products_short['NewsletterCategory'][$category_id]['NewsletterOffer'][$key]['NewsletterOffer']['id'] = $short_product['id'];
				}
				$this->set("product_shorts",$products_short);
			}

			//change data format to match the product_short layout
			if(!empty($this->request->data['NewslettersProduct'])){
				foreach($this->request->data['NewslettersProduct'] as $key=>$product){
					$category_id = $product['category_id'];
					$products['NewsletterCategory'][$category_id]['category_id'] = $category_id;
					$products['NewsletterCategory'][$category_id]['NewsletterOffer'][$key]['NewsletterOffer']['offer_id'] = $product['product_id'];
					$products['NewsletterCategory'][$category_id]['NewsletterOffer'][$key]['NewsletterOffer']['id'] = $product['id'];
				}
				$this->set("products",$products);
			}

			//get all related categories
			$this->Newsletter->Program->unbindModel(
				array(
				'hasMany' => array('Client', 'ProgramPage'),
				'hasAndBelongsToMany' => array('Product', 'SpecialProduct','WhatsNewProduct','Module'),
				)
			);
			$programs = $this->Newsletter->Program->read(null, $program_id);
			if (!empty($programs['Category'])) {
				 $categories = Set::combine($programs['Category'] ,'{n}.id' , '{n}.description', null);
			}

			$this->set('categories',$categories);
		}

		$this->loadModel('Country');
		$countries = $this->Country->find('list');

		$this->set('newsletter_countries',$countries);

		//	get newsletter states
		//$this->set('states', $this->Newsletter->NewsletterState->getEnumValues('state'));
		$statesql1 ="SELECT State.state_cd, State.state_name FROM states AS State, newsletter_states  as NewsletterState
					where State.country_cd = NewsletterState.country and NewsletterState.newsletter_id='".$id."'
					order by State.state_name";
			CakeLog::debug("-statesql1-$statesql1");

			$this->loadModel("State");
			$states_list = $this->State->query($statesql1);

			$states = array();
			foreach ($states_list as $state) {
				foreach($state as $st)
				{
					$states[$st['state_cd']]= $st['state_name'];
				}
			}
		$this->set('states',$states);
		//	get newsletter states
		//$this->set('states', $this->Newsletter->NewsletterState->getEnumValues('state'));
		$selected_newsletter_states = array();
		$selected_newsletter_country = '';
		if (isset($this->request->data['NewsletterState'])) {
			foreach ($this->request->data['NewsletterState'] as $newsletter_state) {
				$selected_newsletter_states[$newsletter_state['state']] = $newsletter_state['state'];
				$selected_newsletter_country= $newsletter_state['country'];
			}
		}
		$this->set('selected_newsletter_states', $selected_newsletter_states);
		$this->set('selected_newsletter_country', $selected_newsletter_country);

		// lookup options
		$this->set('fonts', $this->Newsletter->getEnumValues('font'));

		$this->loadModel("Dashboard");
		$this->set('dashboard', $this->Dashboard->find('superlist',array('fields'=>array('Dashboard.id','Dashboard.type'),'separator'=>' * ')));


	}

	function admin_delete($id){
		$newsletter['Newsletter']['id'] = $id;
		$newsletter['Newsletter']['delete'] =1;
		if($this->Newsletter->save($newsletter)){
				$this->Session->setFlash('The Newsletter has been deleted');
				$this->redirect(array('action'=>'index'), null, true);
		}else{
				$this->Session->setFlash('The Newsletter can not be deleted');
				$this->redirect(array('action'=>'index'), null, true);
		}
	}

			/**
	 * Cancel a sent newsletter
	 *
	 * @param	int	$newsletter_id		id of the sent_newsletter to try to cancel
	*/
	function admin_cancel($newsletter_id) {

		$this->Newsletter->query("
			UPDATE newsletters_emails
			SET status = 'canceled'
			WHERE newsletter_id = '". $newsletter_id. "'
			AND (status = 'queued' OR status = 'sending')
		");

		$query = "UPDATE newsletters
			SET status = 'canceled'
			WHERE id = '". $newsletter_id. "' ";


		$this->Newsletter->query($query);

		$this->redirect('/admin/newsletters/index/');
	}

		/*
	 * dispaly a newsletter for a user
	 *
	 * @param	int  $id newsletter id
	 * @param str $unique_user_id unique id of the user to dispaly the newsletter for
	*/
	function admin_preview($id){

		if(!empty($this->request->data)){
			if(isset($_POST['next'])){
				$this->redirect('confirm/'.$id.'/'.$this->request->data['NewslettersClient']['client_id']);
			}
			if(isset($_POST['previous'])){
				$this->redirect('edit/'.$id);
			}
			if(isset($_POST['cancel'])){
				$this->redirect('index');
			}
			if(isset($_POST['view_copy'])){
				$this->redirect('edit/'.$id);
			}
		}

		$newsletter = $this->Newsletter->read(null, $id);
		if (!empty($newsletter['Client'])) {
			 $clients = Set::combine($newsletter['Client'] ,'{n}.id' , '{n}.name', null);
			 $clients1 = Set::combine($newsletter['Client'] , '{n}.email', null);
			 $this->set('clients',$clients);
			 $this->set('first_client',current(array_keys($clients)));
		}
		$this->set('newsletter',$newsletter);
		if  ($newsletter['Newsletter']['limit_users_to_state'] == '1')
		{
			$states = $this->Newsletter->NewsletterState->find('list', array('conditions' => array('NewsletterState.newsletter_id' => $id), 'fields' => array('NewsletterState.state', 'NewsletterState.state'), 'order' => 'NewsletterState.state'));
			$countries = $this->Newsletter->NewsletterState->find('list', array('conditions' => array('NewsletterState.newsletter_id' => $id), 'fields' => array('NewsletterState.country', 'NewsletterState.country'), 'order' => 'NewsletterState.country'));
		}

		/*else {
		$statesql ="SELECT State.state_cd, State.state_name FROM states AS State, countries as Country where State.country_cd = Country.id
	   	and Country.name='".$this->Session->read('user.User.country')."'";

    	$this->loadModel("State");
		$state = $this->State->query($statesql);

    	$states = array();
		foreach ($state as $state1) {
			$states[$state1['State']['state_cd']] = $state1['State']['state_cd'];
		}

		$states = $this->Newsletter->User->getEnumValues('state');
		}*/
		if(empty($newsletter['Newsletter']['email_id']))
			$from_email = current(array_keys($clients1));
		else
			$from_email = $newsletter['Newsletter']['email_id'];
		$this->set('from_email', $from_email);
		if(!empty($states))
		{
			$this->set('states', $states);
		}
		if(!empty($countries))
		{
			$this->set('countries',$countries);
		}

		if (!empty($states)) {
			$this->set('first_state', current($states));
		} else {
			$this->set('first_state', '');
		}
	}

	function admin_view($id){

		if(!empty($this->request->data)){
			if(isset($_POST['next'])){
				$this->redirect('confirm/'.$id.'/'.$this->request->data['NewslettersClient']['client_id']);
			}
			if(isset($_POST['previous'])){
				$this->redirect('edit/'.$id);
			}
			if(isset($_POST['cancel'])){
				$this->redirect('index');
			}
		}

		$newsletter = $this->Newsletter->read(null, $id);

		if (!empty($newsletter['Client'])) {
			 $clients = Set::combine($newsletter['Client'] ,'{n}.id' , '{n}.name', null);
			 $clients1 = Set::combine($newsletter['Client'] , '{n}.email', null);
			 $this->set('clients',$clients);
			 $this->set('first_client',current(array_keys($clients)));
		}
		$this->set('newsletter',$newsletter);
		if  ($newsletter['Newsletter']['limit_users_to_state'] == '1') {
			$states = $this->Newsletter->NewsletterState->find('list', array('conditions' => array('NewsletterState.newsletter_id' => $id), 'fields' => array('NewsletterState.state', 'NewsletterState.state'), 'order' => 'NewsletterState.state'));
			$countries = $this->Newsletter->NewsletterState->find('list', array('conditions' => array('NewsletterState.newsletter_id' => $id), 'fields' => array('NewsletterState.country', 'NewsletterState.country'), 'order' => 'NewsletterState.country'));
		}

		if(empty($newsletter['Newsletter']['email_id']))
			$from_email = current(array_keys($clients1));
		else
			$from_email = $newsletter['Newsletter']['email_id'];
		$this->set('from_email', $from_email);
		$this->set('states', $states);
		if (!empty($states)) {
			$this->set('first_state', current($states));
		} else {
			$this->set('first_state', '');
		}
	}
	/**
	 * confirm before sending
	 */
	function admin_confirm($newsletter_id) {
		set_time_limit(900);
		ini_set('memory_limit', '1024M');

		if(!empty($this->request->data)){
			if(isset($_POST['next'])){
				$this->redirect('send/'.$newsletter_id);
			}elseif(isset($_POST['previous'])){
				$this->redirect('preview/'.$newsletter_id);
			}
		}

		$newsletter = $this->Newsletter->read(null, $newsletter_id);

		$total_users = 0;

		$conditions = "";$clientCounts = array();
		if ($newsletter['Newsletter']['limit_users_to_state'] == '1') {
			$newsletter_states = $this->Newsletter->NewsletterState->find('list', array('conditions' => array('NewsletterState.newsletter_id' => $newsletter_id), 'fields' => array('NewsletterState.state', 'NewsletterState.state'), 'order' => 'NewsletterState.state'));
			$conditions = " AND u.state IN ('". implode("','", $newsletter_states). "')";
		}
		CakeLog::debug("-nstates-$conditions");
		if ($newsletter['Newsletter']['limit_products_to_state'] == '1')
		{
			if($this->Newsletter->NewslettersProduct->find('count', array('conditions'=>array("NewslettersProduct.newsletter_id = ".$newsletter_id))) > 0)
			{
				$sql = "(SELECT u.client_id, count(*) as userCnt FROM newsletters_temp_clients c, newsletters_temp_states s, users u
				WHERE c.newsletter_id = ".$newsletter_id." AND c.available_national=0 AND s.newsletter_id = c.newsletter_id
				AND u.client_id=c.client_id AND u.client_id=s.client_id AND u.state = s.state AND u.email <> '' AND u.email <> '@'
				AND u.newsletter = 1 AND u.registered = 1".$conditions." group by u.client_id)
				UNION
				(SELECT u.client_id, count(*) as userCnt FROM newsletters_temp_clients c, users u
				WHERE c.newsletter_id = ".$newsletter_id." AND c.available_national=1 AND u.client_id=c.client_id
				AND u.email <> '' AND u.email <> '@' AND u.newsletter = 1 AND u.registered = 1".$conditions." group by u.client_id)";CakeLog::debug("111$sql");

				$cntList = $this->Newsletter->query($sql);
				foreach($cntList as $list) {
					$clientCounts[$list[0]['client_id']] = $list[0]['userCnt'];
				}
						CakeLog::debug("-limit_products_to_state-$sql");

			}
			else
			{
				$sql = "SELECT u.client_id, count(*) as userCnt FROM users u, newsletters_clients c
				WHERE u.client_id = c.client_id AND c.newsletter_id = ".$newsletter_id." AND u.email <> ''
				AND u.email <> '@' AND u.newsletter = 1 AND u.registered = 1".$conditions." group by u.client_id";CakeLog::debug("222$sql");

				$cntList = $this->Newsletter->query($sql);
				foreach($cntList as $list) {
					$clientCounts[$list['u']['client_id']] = $list[0]['userCnt'];
				}
				CakeLog::debug("-NO limit_products_to_state-$sql");

			}
		}
		else
		{
			$sql = "SELECT u.client_id, count(*) as userCnt FROM users u, newsletters_clients c
			WHERE u.client_id = c.client_id AND c.newsletter_id = ".$newsletter_id." AND u.email <> ''
			AND u.email <> '@' AND u.newsletter = 1 AND u.registered = 1".$conditions." group by u.client_id";

			$cntList = $this->Newsletter->query($sql);
			foreach($cntList as $list) {
				$clientCounts[$list['u']['client_id']] = $list[0]['userCnt'];
			}
									CakeLog::debug("ELSE-limit_products_to_state-$sql");

		}

		foreach ($newsletter['Client'] as $key => $newsletter_client) {

			$userCnt = array_key_exists($newsletter_client['id'], $clientCounts)?$clientCounts[$newsletter_client['id']]:0;

			$newsletter['NewslettersClient'][$key]['users'] = $userCnt;

			$this->Newsletter->Client->recursive = -1;
			$client = $this->Newsletter->Client->find('first', array('conditions'=>array('Client.id' => $newsletter_client['id']), 'fields'=>array('name')));
			$newsletter['NewslettersClient'][$key]['Client'] = $client['Client'];
			$total_users += $userCnt;
		}
		$this->set('newsletter', $newsletter);
		$this->set('total_users', $total_users);

	}



	/*
	 * Send newsletter
	 *
	 * @param	str	$email			address to send to
	 * @param	int	$newsletter_id	newsletter id
	 * @param	int	$client_id		used to get the banner and offers for the client
	 */
	function admin_send($newsletter_id) {

		set_time_limit(900);
		ini_set('memory_limit', '1024M');

		$this->Newsletter->unbindModel(array('hasMany' => array('NewslettersEmail')));
		$newsletter = $this->Newsletter->findById($newsletter_id);

		//	delete existing emails, if they exist
		$this->Newsletter->NewslettersEmail->query("DELETE FROM newsletters_emails WHERE newsletter_id = '". $newsletter_id. "'");

		$conditions = "";$selectSql = "";
		if ($newsletter['Newsletter']['limit_users_to_state'] == '1') {
			$newsletter_states = $this->Newsletter->NewsletterState->find('list', array('conditions' => array('NewsletterState.newsletter_id' => $newsletter_id), 'fields' => array('NewsletterState.state', 'NewsletterState.state'), 'order' => 'NewsletterState.state'));
			$conditions = " AND u.state IN ('". implode("','", $newsletter_states). "')";
		}
		if ($newsletter['Newsletter']['limit_products_to_state'] == '1')
		{
			if($this->Newsletter->NewslettersProduct->find('count', array('conditions'=>array("NewslettersProduct.newsletter_id = ".$newsletter_id))) > 0)
			{
				$selectSql = "FROM ((SELECT u.id, u.client_id, u.first_name, u.last_name, u.email FROM newsletters_temp_clients c, newsletters_temp_states s, users u
				WHERE c.newsletter_id = ".$newsletter_id." AND c.available_national=0 AND s.newsletter_id = c.newsletter_id
				AND u.client_id=c.client_id AND u.client_id=s.client_id AND u.state = s.state AND u.email <> '' AND u.email <> '@'
				AND u.newsletter = 1 AND u.registered = 1".$conditions.")
				UNION ALL
				(SELECT u.id, u.client_id, u.first_name, u.last_name, u.email FROM newsletters_temp_clients c, users u
				WHERE c.newsletter_id = ".$newsletter_id." AND c.available_national=1 AND u.client_id=c.client_id
				AND u.email <> '' AND u.email <> '@' AND u.newsletter = 1 AND u.registered = 1".$conditions.")) as u";
			}
			else
			{
				$selectSql = "FROM users u, newsletters_clients c
				WHERE u.client_id = c.client_id AND c.newsletter_id = ".$newsletter_id." AND u.email <> ''
				AND u.email <> '@' AND u.newsletter = 1 AND u.registered = 1".$conditions;
			}
		}
		else
		{
			$selectSql = "FROM users u, newsletters_clients c
			WHERE u.client_id = c.client_id AND c.newsletter_id = ".$newsletter_id." AND u.email <> ''
			AND u.email <> '@' AND u.newsletter = 1 AND u.registered = 1".$conditions;
		}



		$sql = "INSERT INTO newsletters_emails (user_id, newsletter_id, client_id, to_name, to_email, time_to_send)
				SELECT u.id, ".$newsletter_id.", u.client_id, CONCAT(u.first_name, ' ', u.last_name), u.email,
				ADDTIME(NOW(), '0:10:00') ".$selectSql;

				//." GROUP BY u.id, u.state, u.email";
	//	CakeLog::write('debug-sql-', 'myArray'.print_r($sql, true) );
	//	die();

		$this->Newsletter->query($sql);

		$delSql = "DELETE from newsletters_temp_clients WHERE newsletter_id=".$newsletter_id;
		$this->Newsletter->NewslettersTempClient->query($delSql);

		$delProdSql = "DELETE from newsletters_temp_states WHERE newsletter_id=".$newsletter_id;
		$this->Newsletter->NewslettersTempState->query($delProdSql);

		$this->Newsletter->save(array('id' => $newsletter_id, 'status' => 'sending', 'user_id' => $this->Session->read('user.User.id')));

		//redirect to the index page and showing the result
		$this->Session->setFlash('The Newsletter has been added to the queue');

		$this->redirect(array('action'=>'index'), null, true);

	}

	/*
	 * dispaly a newsletter for a user
	 *
	 * @param	int  $id newsletter id
	 * @param str $unique_user_id unique id of the user to dispaly the newsletter for
	*/
	function newsletter($client_id,$id,$dashboard_id=null, $unique_user_id = false) {
		if (empty($unique_user_id)) {
			$this->Session->setFlash('Newsletter not found');
			$this->redirect('/');
		}

		$this->Newsletter->Client->User->recursive = -1;
		$user = $this->Newsletter->Client->User->findByUniqueId($unique_user_id);
		if (empty($user)) {
			$this->setFlash('Newsletter not found');
			$this->redirect('/');
		}

		$this->_newsletter($id, $client_id,$dashboard_id, $user);
	}

	/*
	 * dispaly a newsletter for a client
	 *
	 * @param	int  $id newsletter id
	 * @param int  $client_id  id of the client to dispaly the newsletter for
	 * @param str  $state  state to dispaly the newsletter for
	*/
	function admin_newsletter($id, $client_id, $dashboard_id=null, $state = false) {

		$user = $this->Session->read('user');
		$user['User']['state'] = $state;

		$this->_newsletter($id, $client_id, $dashboard_id, $user);
	}

	/**
	 * Generate the newsletter
	 *
	* @param mixed newsletter Arry of newsletter information or newsletter id
	 * @param mixed client Array of client information or client id
	 * @param array user Array of user information
	 * @param	bool	limit_products_to_state	string australian state, ov
	*/
	function _newsletter($newsletter, $client, $dashboard_id=null, $user) {

		if (!is_array($newsletter)) {
			$this->Newsletter->recursive = 0;
			$newsletter = $this->Newsletter->findById($newsletter);
		}

		$this->pageTitle = $newsletter['Newsletter']['page_title'];

		if (!is_array($client)) {
			$this->Newsletter->Client->recursive = 0;
			$client = $this->Newsletter->Client->findById($client);
		}

		//finding newsletter banners mapped with dashboard id and client id in clientbanners table
		$this->loadModel("ClientBanner");
		$this->set('clientbanner',$this->ClientBanner->find('all',array('conditions'=>array('ClientBanner.client_id'=>$client['Client']['id'],'ClientBanner.dashboard_id'=>$dashboard_id),'recursive'=>0)));

		$this->layout = 'newsletter';

		//	get the categories and products
		$sql = "SELECT DISTINCT Category.* FROM categories AS Category
					LEFT JOIN newsletters_products AS NewslettersProduct ON NewslettersProduct.newsletter_id = '". $newsletter['Newsletter']['id']. "' AND NewslettersProduct.category_id = Category.id
					WHERE NewslettersProduct.id IS NOT NULL
					ORDER BY NewslettersProduct.id";
		$newsletter_categories = $this->Newsletter->query($sql);
		foreach ($newsletter_categories as $key => $category) {
			$sql = "SELECT DISTINCT Product.*, Merchant.* FROM products AS Product
						LEFT JOIN merchants AS Merchant ON Merchant.id = Product.merchant_id
						INNER JOIN newsletters_products AS NewslettersProduct ON NewslettersProduct.product_id = Product.id AND NewslettersProduct.newsletter_id = '". $newsletter['Newsletter']['id']. "' AND NewslettersProduct.category_id =  '". $category['Category']['id']. "'
						INNER JOIN categories_products AS CategoriesProduct ON Product.id = CategoriesProduct.product_id
						INNER JOIN clients_categories AS ClientsCategory ON CategoriesProduct.category_id = ClientsCategory.category_id AND ClientsCategory.client_id = '". $client['Client']['id']. "'
						INNER JOIN clients_products ON Product.id = clients_products.product_id AND (clients_products.client_id = '". $client['Client']['id']. "' or clients_products.client_id = 0)
						AND clients_products.blocked=0 AND clients_products.id IS NOT NULL
						ORDER BY NewslettersProduct.id";
			$products = $this->Newsletter->query($sql);

			//	if we are limiting to a users state, check products state availability
			if ($newsletter['Newsletter']['limit_products_to_state'] == '1' && $user['User']['state'] != '') {
				foreach ($products as $key2 => $product) {
					//	always allow national and products available to the users state

					if ($product['Product']['available_national'] == 1 || $product['Product']['available_international'] == 1 ) {
						continue;
					}

					$sql = "SELECT COUNT(*) AS count FROM merchant_addresses AS MerchantAddress, products_addresses as ProductsAddress
					WHERE MerchantAddress.merchant_id =".$product['Product']['merchant_id']." AND ProductsAddress.merchant_address_id= MerchantAddress.id
					AND state = '".$user['User']['state']."'  ";

					$merchant_address_count = $this->Newsletter->Client->Product->Merchant->MerchantAddress->query($sql);

					if ($merchant_address_count[0][0]['count'] == '0')
					{
						unset($products[$key2]);
					}

				}
			}

			if (empty($products)) {
				unset($newsletter_categories[$key]);
			} else {
				$newsletter_categories[$key]['Product'] = $products;
			}
		}

		//	get the short product list
		$sql = "SELECT DISTINCT Product. * FROM products AS Product
					INNER JOIN newsletters_short_products AS NewslettersShortProduct ON NewslettersShortProduct.product_id = Product.id AND NewslettersShortProduct.newsletter_id = '". $newsletter['Newsletter']['id']. "'
					INNER JOIN categories_products AS CategoriesProduct ON Product.id = CategoriesProduct.product_id
					INNER JOIN clients_categories AS ClientsCategory ON CategoriesProduct.category_id = ClientsCategory.category_id AND ClientsCategory.client_id = '". $client['Client']['id']. "'
					INNER JOIN clients_products ON Product.id = clients_products.product_id AND (clients_products.client_id = '". $client['Client']['id']. "' or clients_products.client_id = 0)
					AND clients_products.blocked=0 AND clients_products.id IS NOT NULL
					ORDER BY NewslettersShortProduct.id
					";
		$newsletter_short_products = $this->Newsletter->query($sql);

		//	if we are limiting to a users state, check products state availability
		if ($newsletter['Newsletter']['limit_products_to_state'] == '1' && $user['User']['state'] != '') {
			foreach ($newsletter_short_products as $key => $product) {
				//	always allow national and products available to the users state
				if ($product['Product']['available_national'] == 1 || $product['Product']['available_international'] == 1) {
					continue;
				}

				$sql = "SELECT COUNT(*) AS count FROM merchant_addresses AS MerchantAddress, products_addresses as ProductsAddress
				WHERE MerchantAddress.merchant_id =".$product['Product']['merchant_id']." AND ProductsAddress.merchant_address_id= MerchantAddress.id
				AND state = '".$user['User']['state']."'  ";

				$merchant_address_count = $this->Newsletter->Client->Product->Merchant->MerchantAddress->query($sql);

				if ($merchant_address_count[0][0]['count'] == '0')
				{
					unset($products[$key2]);
				}

			}
		}

		$this->set('newsletter', $newsletter);
		$this->newsletter = $newsletter;

		$this->set('newsletter_categories', $newsletter_categories);
		$this->set('newsletter_short_products', $newsletter_short_products);

		$this->ctgrs = $newsletter_categories;
		$this->newsletterstate = $newsletter;

		$style_query="SELECT style_id from clients_dashboards where client_id = '". $client['Client']['id']. "' and dashboard_id=".$dashboard_id;
		$this->loadModel('Dashboard');
		$dashboard_style_id=$this->Dashboard->query($style_query);
		$style_id=$dashboard_style_id[0]['clients_dashboards']['style_id'];

		App::import('Model','Style');
		$this->Style = new Style();
		$this->Style->unbindModel(array('hasMany' => array('ClientDashboard', 'Program')));
		$style = $this->Style->findById($style_id);
		$client['Style'] = $style['Style'];

		$this->set('client', $client);
		$this->client = $client;

		$this->set('user', $user);

	}


	/*
	 * Send a test email
	 *
	 * @param	str	$email			address to send to
	 * @param	int	$newsletter_id	newsletter id
	 * @param	int	$client_id		used to get the banner and offers for the client
	 * @param	str	$state			limit the products to a state
	 */
	function admin_send_test($from_email, $email, $newsletter_id, $client_id,$dashboard_id=null, $state=false) {

		$user = $this->Session->read('user');
		$user['User']['state'] = $state;
		$this->autoRender = false;

		$this->_newsletter($newsletter_id, $client_id,$dashboard_id, $user);

		if($this->SwiftMailer->connect()) {
			if(empty($from_email))
				$from_email = $this->client['Client']['email'];
			$this->SwiftMailer->addTo('to', $email);
			$this->SwiftMailer->addTo('from',$from_email,str_replace('@', 'at ', $this->client['Client']['name']));
			$this->SwiftMailer->layout = 'newsletter';

			$this->newsletter['Newsletter']['subject'] = str_replace('[[client_name]]', $this->client['Client']['name'], $this->newsletter['Newsletter']['subject']);
			$this->newsletter['Newsletter']['subject'] = str_replace('[[client_email]]', $from_email, $this->newsletter['Newsletter']['subject']);
			$this->newsletter['Newsletter']['subject'] = str_replace('[[program_name]]', $this->client['Program']['name'], $this->newsletter['Newsletter']['subject']);
			$this->newsletter['Newsletter']['subject'] = str_replace('[[web_site]]', $this->client['Domain']['name'], $this->newsletter['Newsletter']['subject']);
			$this->newsletter['Newsletter']['subject'] = str_replace('[[first_name]]', $user['User']['first_name'], $this->newsletter['Newsletter']['subject']);
			$this->newsletter['Newsletter']['subject'] = str_replace('[[last_name]]', $user['User']['last_name'], $this->newsletter['Newsletter']['subject']);
			$this->newsletter['Newsletter']['subject'] = str_replace('[[twitter_link]]', $this->client['Client']['twitter_link'], $this->newsletter['Newsletter']['subject']);
			$this->newsletter['Newsletter']['subject'] = str_replace('[[user_name]]', $user['User']['username'], $this->newsletter['Newsletter']['subject']);
			$this->newsletter['Newsletter']['subject'] = str_replace('[[password]]', $user['User']['password'], $this->newsletter['Newsletter']['subject']);
			$this->newsletter['Newsletter']['subject'] = str_replace('[[unique_id]]', $user['User']['unique_id'], $this->newsletter['Newsletter']['subject']);

			if(!$this->SwiftMailer->sendView($this->newsletter['Newsletter']['subject'], 'newsletter', 'both')) {
				echo "The mailer failed to Send. Errors:";
				die(debug($this->SwiftMailer->errors()));
			} else {
				echo 'sent';die;
			}

		} else {
			echo "The mailer failed to connect. Errors:";
			die(debug($this->SwiftMailer->errors()));
		}
	}

		/**
	 * Batch send some of the emails
	 *
	 * @param int $timeout	time in seconds to run the script, allow some space here for up to 100 emails to be sent AFTER this timeout,  default 60 seconds
	*/
	function send($timeout = 60) {
		$timeout = 900; /* Fix this to be 5 minutes */
		$time_limit = 900;

		set_time_limit($time_limit);
		ini_set('memory_limit', '128M');

		if ($timeout < $time_limit - 30) {
			$timeout = time() + $timeout;
		} else {
			$timeout = time() + $time_limit - 30;
		}
		echo 'start '. date('y-m-d h:i:s', time()). "<br/>\n";
		$this->Newsletter->unbindModel(array('hasMany' => array('NewslettersEmail')));
		$sent_newsletters = $this->Newsletter->findAllByStatus('sending');

		foreach ($sent_newsletters as $sent_newsletter) {

			$this->set('newsletter', $sent_newsletter);
			$from_email = "";

			$sent_newsletter_emails = false;
			//loop for each client

			foreach($sent_newsletter['Client'] as $sent_client) {

				if (time() > $timeout) {
					break;
				}

				$this->Newsletter->Client->recursive = 0;
				$client = $this->Newsletter->Client->findById($sent_client['id']);

				if(empty($sent_newsletter['Newsletter']['email_id']))
					$from_email = $client['Client']['email'];
				else
					$from_email = $sent_newsletter['Newsletter']['email_id'];

				if (!$client) {
					continue;
				}
				$this->set('client', $client);

				do {

					if (time() > $timeout) {
						break 2;
					}

					//	get emails 100 at a time
					$this->Newsletter->NewslettersEmail->recursive = 0;
					$this->Newsletter->NewslettersEmail->bindModel(array('belongsTo' => array('User')));
					$sent_newsletter_emails = $this->Newsletter->NewslettersEmail->find('all', array('conditions'=>array(
						"NewslettersEmail.client_id = '".$client['Client']['id']. "' AND newsletter_id = '".$sent_newsletter['Newsletter']['id']."' AND status = 'queued' AND time_to_send <= '". date('Y-m-d H:i:s', time()). "'",
						), 'limit'=>100));

echo '<br> ####### news letters ########### <br>'.count($sent_newsletter_emails);
echo '##### client id'.$client['Client']['id'].'#######<br>';

					foreach ($sent_newsletter_emails as $sent_newsletter_email) {

						if (time() > $timeout) {
							break 2;
						}

						//	only try valid emails addresses
						if(!preg_match("/[a-zA-Z0-9_\\.-]+@[a-zA-Z0-9_\\.-]+/", $sent_newsletter_email['NewslettersEmail']['to_email'])) {
							$data['NewslettersEmail']  = array(
								'id' => $sent_newsletter_email['NewslettersEmail']['id'],
								'status' => 'failed',
								'fail_reason' => 'Invalid email address'
							);
							$this->Newsletter->NewslettersEmail->save($data);

						} else {
							$this->_newsletter($sent_newsletter, $client,$sent_newsletter['Newsletter']['dashboard_id'], $sent_newsletter_email);

							//code to check and send mails to the users having products or not

							if($this->newsletterstate['Newsletter']['limit_products_to_state'] == 1 &&
							$this->Newsletter->NewslettersProduct->find('count', array('conditions'=>array("NewslettersProduct.newsletter_id = ".$sent_newsletter['Newsletter']['id']))) > 0)
							{
								$prods =array();

								foreach($this->ctgrs as $s)
								{

									$prods=$s['Product'];
								}

								if(empty($prods))
								{
									$data['NewslettersEmail']  = array(
										'id' => $sent_newsletter_email['NewslettersEmail']['id'],
										'status' => 'failed',
										'fail_reason' => 'No products'
									);
									$this->Newsletter->NewslettersEmail->save($data);
								}
								else
								{
									$this->SwiftMailer->resetTo();
									$this->SwiftMailer->layout = 'newsletter';

									if($this->SwiftMailer->connect())
									{
										$this->SwiftMailer->addTo('to', $sent_newsletter_email['NewslettersEmail']['to_email']);
										$this->SwiftMailer->addTo('from',$from_email,$this->client['Client']['name']);

										$this->newsletter['Newsletter']['subject'] = str_replace('[[client_name]]', $this->client['Client']['name'], $this->newsletter['Newsletter']['subject']);
										$this->newsletter['Newsletter']['subject'] = str_replace('[[client_email]]', $from_email, $this->newsletter['Newsletter']['subject']);
										$this->newsletter['Newsletter']['subject'] = str_replace('[[program_name]]', $this->client['Program']['name'], $this->newsletter['Newsletter']['subject']);
										$this->newsletter['Newsletter']['subject'] = str_replace('[[web_site]]', $this->client['Domain']['name'], $this->newsletter['Newsletter']['subject']);
										$this->newsletter['Newsletter']['subject'] = str_replace('[[first_name]]', $sent_newsletter_email['User']['first_name'], $this->newsletter['Newsletter']['subject']);
										$this->newsletter['Newsletter']['subject'] = str_replace('[[last_name]]', $sent_newsletter_email['User']['last_name'], $this->newsletter['Newsletter']['subject']);
										$this->newsletter['Newsletter']['subject'] = str_replace('[[twitter_link]]', $this->client['Client']['twitter_link'], $this->newsletter['Newsletter']['subject']);
										$this->newsletter['Newsletter']['subject'] = str_replace('[[user_name]]', $sent_newsletter_email['User']['username'], $this->newsletter['Newsletter']['subject']);
										$this->newsletter['Newsletter']['subject'] = str_replace('[[password]]', $sent_newsletter_email['User']['password'], $this->newsletter['Newsletter']['subject']);
										$this->newsletter['Newsletter']['subject'] = str_replace('[[unique_id]]', $sent_newsletter_email['User']['unique_id'], $this->newsletter['Newsletter']['subject']);

										//echo "Email address: ". $sent_newsletter_email['NewslettersEmail']['to_email']. " <br/>\n";
										if(!$this->SwiftMailer->sendView1($this->newsletter['Newsletter']['subject'], 'newsletter', 'both')) {
										echo "Email address: ". $sent_newsletter_email['NewslettersEmail']['to_email']. " <br/>\n";
											//echo "The mailer failed to Send. Errors: <br/>\n";
											//echo "Email address: ". $sent_newsletter_email['NewslettersEmail']['to_email']. " <br/>\n";
											//debug($this->SwiftMailer->errors());

											$data = array(
												'NewslettersEmail' => array(
													'id'	=>	$sent_newsletter_email['NewslettersEmail']['id'],
													'retry_count'	=>	$sent_newsletter_email['NewslettersEmail']['retry_count'] + 1
												)
											);

											//	try again later
											//	first make the date now
											if (strtotime($sent_newsletter_email['NewslettersEmail']['time_to_send']) <= time()) {
												$sent_newsletter_email['NewslettersEmail']['time_to_send'] = date("Y-m-d H:i:s", time());
											}

											$time_to_send = strtotime($sent_newsletter_email['NewslettersEmail']['time_to_send']);
											//	send again in 30 mins
											$time_to_send = (pow(2, $sent_newsletter_email['NewslettersEmail']['retry_count']) * 1800) + strtotime($sent_newsletter_email['NewslettersEmail']['time_to_send']);

											$data['NewslettersEmail']['time_to_send'] = date("Y-m-d H:i:s", $time_to_send);

											//	if the email has failed 5 times, give up
											if  ($sent_newsletter_email['NewslettersEmail']['retry_count']  > 1) {
												$data['NewslettersEmail']['status'] = 'failed';
												$data['NewslettersEmail']['fail_reason'] = $this->SwiftMailer->errors();

											}

										}
										else
										{
											$data = array(
												'NewslettersEmail' => array(
													'id'	=>	$sent_newsletter_email['NewslettersEmail']['id'],
													'status'	=>	'sent',
													'sent_time' => date("Y-m-d H:i:s"),
												)
											);
										}

										$this->Newsletter->NewslettersEmail->save($data);

									}
									else
									{
										echo "The mailer failed to connect. Errors:";
										die(debug($this->SwiftMailer->errors()));
									}
								}
							}
							else
							{
								$this->SwiftMailer->resetTo();
								$this->SwiftMailer->layout = 'newsletter';

								if($this->SwiftMailer->connect()) {
									$this->SwiftMailer->addTo('to', $sent_newsletter_email['NewslettersEmail']['to_email']);
									$this->SwiftMailer->addTo('from',$from_email,$this->client['Client']['name']);

									$this->newsletter['Newsletter']['subject'] = str_replace('[[client_name]]', $this->client['Client']['name'], $this->newsletter['Newsletter']['subject']);
									$this->newsletter['Newsletter']['subject'] = str_replace('[[client_email]]', $from_email, $this->newsletter['Newsletter']['subject']);
									$this->newsletter['Newsletter']['subject'] = str_replace('[[program_name]]', $this->client['Program']['name'], $this->newsletter['Newsletter']['subject']);
									$this->newsletter['Newsletter']['subject'] = str_replace('[[web_site]]', $this->client['Domain']['name'], $this->newsletter['Newsletter']['subject']);
									$this->newsletter['Newsletter']['subject'] = str_replace('[[first_name]]', $sent_newsletter_email['User']['first_name'], $this->newsletter['Newsletter']['subject']);
									$this->newsletter['Newsletter']['subject'] = str_replace('[[last_name]]', $sent_newsletter_email['User']['last_name'], $this->newsletter['Newsletter']['subject']);
									$this->newsletter['Newsletter']['subject'] = str_replace('[[twitter_link]]', $this->client['Client']['twitter_link'], $this->newsletter['Newsletter']['subject']);
									$this->newsletter['Newsletter']['subject'] = str_replace('[[user_name]]', $sent_newsletter_email['User']['username'], $this->newsletter['Newsletter']['subject']);
									$this->newsletter['Newsletter']['subject'] = str_replace('[[password]]', $sent_newsletter_email['User']['password'], $this->newsletter['Newsletter']['subject']);
									$this->newsletter['Newsletter']['subject'] = str_replace('[[unique_id]]', $sent_newsletter_email['User']['unique_id'], $this->newsletter['Newsletter']['subject']);

									//echo "Email address: ". $sent_newsletter_email['NewslettersEmail']['to_email']. " <br/>\n";
									if(!$this->SwiftMailer->sendView1($this->newsletter['Newsletter']['subject'], 'newsletter', 'both')) {
									echo "Email address: ". $sent_newsletter_email['NewslettersEmail']['to_email']. " <br/>\n";
										//echo "The mailer failed to Send. Errors: <br/>\n";
										//echo "Email address: ". $sent_newsletter_email['NewslettersEmail']['to_email']. " <br/>\n";
										//debug($this->SwiftMailer->errors());

										$data = array(
											'NewslettersEmail' => array(
												'id'	=>	$sent_newsletter_email['NewslettersEmail']['id'],
												'retry_count'	=>	$sent_newsletter_email['NewslettersEmail']['retry_count'] + 1
											)
										);

										//	try again later
										//	first make the date now
										if (strtotime($sent_newsletter_email['NewslettersEmail']['time_to_send']) <= time()) {
											$sent_newsletter_email['NewslettersEmail']['time_to_send'] = date("Y-m-d H:i:s", time());
										}

										$time_to_send = strtotime($sent_newsletter_email['NewslettersEmail']['time_to_send']);
										//	send again in 30 mins
										$time_to_send = (pow(2, $sent_newsletter_email['NewslettersEmail']['retry_count']) * 1800) + strtotime($sent_newsletter_email['NewslettersEmail']['time_to_send']);

										$data['NewslettersEmail']['time_to_send'] = date("Y-m-d H:i:s", $time_to_send);

										//	if the email has failed 5 times, give up
										if  ($sent_newsletter_email['NewslettersEmail']['retry_count']  > 1) {
											$data['NewslettersEmail']['status'] = 'failed';
											$data['NewslettersEmail']['fail_reason'] = $this->SwiftMailer->errors();

										}

									}
									else
									{
										$data = array(
											'NewslettersEmail' => array(
												'id'	=>	$sent_newsletter_email['NewslettersEmail']['id'],
												'status'	=>	'sent',
												'sent_time' => date("Y-m-d H:i:s"),
											)
										);
									}

									$this->Newsletter->NewslettersEmail->save($data);

								}
								else
								{
									echo "The mailer failed to connect. Errors:";
									die(debug($this->SwiftMailer->errors()));
								}
							}
						}
					}

					//	get the number of emails to send
					$this->Newsletter->NewslettersEmail->recursive = 0;
					$this->Newsletter->NewslettersEmail->bindModel(array('belongsTo' => array('User')));
					$sent_newsletter_email_count = $this->Newsletter->NewslettersEmail->find('count', array('conditions'=>array(
						"NewslettersEmail.client_id = '".$client['Client']['id']. "' AND newsletter_id = '".$sent_newsletter['Newsletter']['id']."' AND status = 'queued' AND time_to_send <= '". date('Y-m-d H:i:s', time()). "'"
					)));
					}
				 while($sent_newsletter_email_count > 0);
			}


			$this->Newsletter->NewslettersEmail->recursive = 0;
			$sent_newsletter_email_count = $this->Newsletter->NewslettersEmail->find('count', array('conditions'=>array("newsletter_id = '".$sent_newsletter['Newsletter']['id']."' AND status = 'queued' ")));

			if ($sent_newsletter_email_count == '0') {
				$data = array('Newsletter' => array('id' => $sent_newsletter['Newsletter']['id'], 'status' => 'sent'));
				$this->Newsletter->save($data);
			}


		}

		echo ' <br/>finish '. date('y-m-d h:i:s', time());

		$this->layout = 'blank';
	}


	/**
	 * Unsubscribe an email address
	 *
	 * @param	str	email	Email address to unsubscribe
	*/
	function unsubscribe($email=false) {

		if (isset($this->request->data['User']['email'])) {
			if (empty($this->request->data['User']['email'])) {
				$this->set('error', true);
				return;
			}

			$this->Newsletter->User->recursive = -1;
			$users = $this->Newsletter->User->findAllByEmail($this->request->data['User']['email']);
			if (empty($users)) {
				$this->set('error', true);
				return;
			} else {
				foreach ($users as $user) {
					$old_data = $user;
					$user['User']['newsletter'] = '0';
					$this->Newsletter->User->save($user);
					//	log the change
					$log = array(
						'id' => null,
						'foreign_id' => $this->Newsletter->User->id,
						'foreign_model' => 'User',
						'user_id' => $user['User']['id'],
						'old_data' => $old_data,
						'new_data' => $user
					);
					$log['description'] = 'User unsubscribed from newsletter';
					$this->save_log($log);
				}
				$this->redirect('/newsletters/unsubscribe_done');
			}

		} else {
			$this->request->data['User']['email'] = $email;
		}

	}

	function admin_update_newsletter_states()
	{
		$cid =  $this->request->data['NewsletterState']['country'];
		$this->loadModel("State");
		$statesql = "SELECT State.state_cd, State.state_name FROM states AS State WHERE State.country_cd = '".$cid."'";
		$state_result = $this->State->query($statesql);
		$states = array();
		foreach ($state_result as $states1) {
			foreach($states1 as $cc)
			{
				$states[$cc['state_cd']] = $cc['state_name'];
			}
		}

		$this->set('states',$states);
		$this->layout = 'ajax';
	}

	function unsubscribe_done()
	{
		$message = "Your request to unsubscribe newsletter has been processed...";
		$this->set('msg', $message);
	}

}
?>
