<?php
class StylesController extends AppController {

	public $components = array('File');


	/**
	 * Display the stylesheet for a style
	 */
	function sheet($id) {
		$this->layout = 'stylesheet';
		$this->Style->recursive = 0;
		$this->set('style', $this->Style->read(null, $id));
	}

	function admin_index() {

		$name = Inflector::singularize($this->name);
		$conditions = array();
		
		if($this->Session->read('user.User.type')=='Client Admin')
		{
			$this->loadModel('StyleAdmin');
			$sql1 = " WHERE Client.id=ClientsDashboard.client_id and ClientsDashboard.style_id=Style.id and
			ClientsDashboard.client_id=".$this->Session->read('user.User.client_id')." group by Style.id ";
		
			$this->set(Inflector::underscore($this->name), $this->paginate('StyleAdmin', array('search' =>array('sql' => $sql1))));
		}
		elseif($this->Session->read('user.User.type')=='Country Admin')
		{
			$this->loadModel('StyleAdmin');
			$sql1 = " WHERE Client.id=ClientsDashboard.client_id and ClientsDashboard.style_id=Style.id and
			Client.country = '".$this->Session->read('client.Client.country')."' group by Style.id ";
		
			$this->set(Inflector::underscore($this->name), $this->paginate('StyleAdmin', array('search' =>array('sql' => $sql1))));
		}
		else
		{
			if (isset($this->params['url']['data']))
			{
				$this->Session->write($name.'.search', $this->params['url']['data']);
			}

			if ($this->Session->check($name.'.search')) {
				$this->request->data = $this->Session->read($name.'.search');
				if  (!empty($this->request->data[$name]['search'])) {
					$conditions[] = "
						(`". $name. "`.`name` LIKE  '%". $this->request->data[$name]['search']. "%'
						OR `". $name. "`.`id` =  '". $this->request->data[$name]['search']. "')
					";
				}
			}

			$this->$name->recursive = 0;
			$this->set(Inflector::underscore($this->name), $this->paginate($name, $conditions));
		}
	}

	function admin_edit($id = null) {

		if (!empty($this->request->data)) {
			if(isset($_POST['copy']))
			{
				$this->request->data = $this->Style->read(null,$id);
				unset($this->request->data['Style']['id']);
				unset($this->request->data['Style']['name']);
			}
			else
			{
				$this->Style->recursive = -1;

				$old_data = $this->Style->findById($id);

				if ($this->Style->save($this->request->data)) {

					//	log the change
					$log = array(
						'id' => null,
						'foreign_id' => $this->Style->id,
						'foreign_model' => 'Style',
						'user_id' => $this->Session->read('user.User.id'),
						'old_data' => $old_data,
						'new_data' => $this->request->data
					);

					if (empty($old_data)) {
						$log['description'] = 'Style created';
					} else {
						$log['description'] = 'Style modified';
					}

					if (isset($this->request->data['Log']['notes'])) {
						$log['notes'] = $this->request->data['Log']['notes'];
					}

					$this->save_log($log);

					$this->Session->setFlash('The Style has been saved');
					$this->redirect_session(array('action'=>'index'), null, true);
				} else {
					$this->Session->setFlash('Please correct the errors below.');
				}
			}
		}
		else {
			$this->request->data = $this->Style->read(null, $id);

			$this->_setRedirect();
		}
		$font_styles = array('Arial, Helvetica, sans-serif' => 'Arial, Helvetica, sans-serif','Bookman Old Style, serif'=>'Bookman Old Style, serif', 'Comic Sans MS'=>'Comic Sans MS', 'Courier New, Courier, mono'=>'Courier New, Courier, mono', 'Droid Sans, sans-serif'=>'Droid Sans, sans-serif','Geneva, Arial, Helvetica, sans-serif'=>'Geneva, Arial, Helvetica, sans-serif', 'Georgia, Times New Roman, Times, serif'=>'Georgia, Times New Roman, Times, serif', 'Lucida Grande, sans-serif'=>'Lucida Grande, sans-serif', 'Myriad,Trebuchet MS,Lucida Sans Unicode'=> 'Myriad,Trebuchet MS,Lucida Sans Unicode', 'Times New Roman, Times, serif' => 'Times New Roman, Times, serif', 'Trebuchet MS, Helvetica, sans-serif'=>'Trebuchet MS, Helvetica, sans-serif', 'Verdana, Arial, Helvetica, sans-serif'=>'Verdana, Arial, Helvetica, sans-serif');
		$this->set('font_styles',$font_styles);
		$this->set('default_font_styles','Droid Sans, sans-serif');
		$this->set('default_tab_font_styles','Droid Sans, sans-serif');
		$this->set('default_menu_font_styles','Myriad,Trebuchet MS ,Lucida Sans Unicode');
	}

	function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash('Invalid id for Style');
			$this->redirect(array('action'=>'index'), null, true);
		}elseif ($this->Style->delete($id)) {
			$this->Session->setFlash('Style deleted');
			$this->redirect(array('action'=>'index'), null, true);
		}
	}
}
?>