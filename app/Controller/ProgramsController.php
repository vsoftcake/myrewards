<?php
class ProgramsController extends AppController {

	public $components = array('File');
	public $uses = array('ProgramsProduct', 'ProgramsCategory', 'Program', 'Client');

	function admin_index() {
		$this->Program->recursive = 0;
		$this->set('programs', $this->paginate('Program'));
	}

	function admin_edit($id = null) {

		//	updating the clients products takes some time
		set_time_limit('60');
		ini_set( 'memory_limit', '-1' );

		//reset the sub item searches
		$this->Session->delete($this->name.'.search');

		if (!empty($this->request->data)) {

			if((isset($_POST['copy']) && !empty($this->request->data['Program']['copy'])) ){

					$id = $this->request->data['Program']['copy'];

					$this->request->data = $this->Program->read(null,$id);
					unset($this->request->data['Program']['id']);
					unset($this->request->data['Program']['name']);

					if($id==null)
					{
						$program_options = $this->Program->find('list');
					}

			}
			else
			{
			//	get the image extensions
			foreach ($this->request->data['Image'] as $key => $file) {
				if ($file['name'] != '') {
					$extension = substr($file['name'], strrpos($file['name'], '.') + 1);
					$this->request->data['Program'][$key. '_extension'] = $extension;
				}
			}

			$this->{Inflector::singularize($this->name)}->unbindModel(array('hasAndBelongsToMany' => array('Product'), 'hasMany' => array('ProgramPage')));
			$old_data = $this->{Inflector::singularize($this->name)}->findById($id);

			//	fix up the special products and whats new products
			foreach ($this->request->data['SpecialProduct']['SpecialProduct'] as $key => $value) {
				$value = trim($value);
				if (strpos($value, ' ') === true) {
					$this->request->data['SpecialProduct']['SpecialProduct'][$key] = substr($value, 0, strpos($value, ' '));
				}
			}

			foreach ($this->request->data['WhatsNewProduct']['WhatsNewProduct'] as $key => $value) {
				$value = trim($value);
				if (strpos($value, ' ') !== false) {
					$this->request->data['WhatsNewProduct']['WhatsNewProduct'][$key] = substr($value, 0, strpos($value, ' '));
				}
			}

			$cat1 = $this->Program->ProgramsCategory->find('all',array('conditions'=>array('program_id'=>$id),'fields'=>array('category_id')));
			$bcat = array();
			foreach($cat1 as $c1)
			{
				array_push($bcat,$c1['ProgramsCategory']['category_id']);
			}
			
			if(empty($this->request->data['Program']['special_offer_module_limit'])){
				unset($this->request->data['Program']['special_offer_module_limit']);
			}
			if(empty($this->request->data['Program']['special_offer_module_limit_saf'])){
				unset($this->request->data['Program']['special_offer_module_limit_saf']);
			}
			if(empty($this->request->data['Program']['special_offer_module_limit_points'])){
				unset($this->request->data['Program']['special_offer_module_limit_points']);
			}
			if(empty($this->request->data['Program']['style_id'])){
				unset($this->request->data['Program']['style_id']);
			}
			if(empty($this->request->data['Program']['style_id_saf'])){
				unset($this->request->data['Program']['style_id_saf']);
			}
			if(empty($this->request->data['Program']['style_id_points'])){
				unset($this->request->data['Program']['style_id_points']);
			}
			if(empty($this->request->data['Program']['domain_id'])){
				unset($this->request->data['Program']['domain_id']);
			}

			if ($this->Program->save($this->request->data['Program'])) {
				
				$program_id=$this->Program->id;
				//Saving Countries
				$this->Program->ProgramsCountry->deleteAll(array('program_id'=>$program_id));
				$programCountryData = array();
				foreach($this->request->data['Country']['Country'] as $programCountry){if(!empty($programCountry)){
					$programCountryData[] = array('program_id'=>$program_id, 'country_id'=>$programCountry);
				}}
				if(!empty($programCountryData)){$this->Program->ProgramsCountry->saveMany($programCountryData);}
				
				//Saving categories
				$this->Program->ProgramsCategory->deleteAll(array('program_id'=>$program_id));
				$programCategoryData = array();
				foreach($this->request->data['Category']['Category'] as $programCategory){if(!empty($programCategory)){
					$programCategoryData[] = array('program_id'=>$program_id, 'category_id'=>$programCategory);
				}}
				if(!empty($programCategoryData)){$this->Program->ProgramsCategory->saveMany($programCategoryData);}
				
				//Saving special products
				$this->Program->ProgramsSpecialProduct->deleteAll(array('program_id'=>$program_id));
				$programSepPrdtData = array();
				foreach($this->request->data['SpecialProduct']['SpecialProduct'] as $programSepPrdt){if(!empty($programSepPrdt)){
					$programSepPrdtData[] = array('program_id'=>$program_id, 'product_id'=>$programSepPrdt, 'dashboard_id'=>1);
				}}
				foreach($this->request->data['SpecialProduct']['SpecialProduct_2'] as $programSepPrdt){if(!empty($programSepPrdt)){
					$programSepPrdtData[] = array('program_id'=>$program_id, 'product_id'=>$programSepPrdt, 'dashboard_id'=>2);
				}}
				foreach($this->request->data['SpecialProduct']['SpecialProduct_3'] as $programSepPrdt){if(!empty($programSepPrdt)){
					$programSepPrdtData[] = array('program_id'=>$program_id, 'product_id'=>$programSepPrdt, 'dashboard_id'=>3);
				}}
				if(!empty($programSepPrdtData)){$this->Program->ProgramsSpecialProduct->saveMany($programSepPrdtData);}
				/* Saving banner data in program banners */
					//background images for myrewards
					if($this->request->data['ClientOverride']['override_mr_bi'] == '1' && isset($this->request->data['ClientOverideClients_mr_bi']))
					{
						if ($this->request->data['ClientOverride']['left_mr_1'] ==1 )
						{
							if ($this->request->data['ImageDeleteLeft']['program_background_images_left_1'] == 1) {
								$delete_images = array();
								foreach ($this->request->data['ImageDeleteLeft'] as $key => $value) {
									if ($value == '1') {
										$this->File->deleteByPrefix('program_background_images_left/'.$program_id);
										$sql = "UPDATE program_banners SET program_background_images_left_extension='' WHERE dashboard_id=1 and program_id=".$program_id;
										$this->Program->ProgramBanner->query($sql);
									}
								}
							}
							else
							{
								$clientbanner = $this->Program->ProgramBanner->find('first',
										array('conditions'=>array('ProgramBanner.program_id' => $program_id, 'ProgramBanner.dashboard_id' => 1),
												'fields' => array('ProgramBanner.id, ProgramBanner.program_background_images_left_extension')));
								$bannerid = empty($clientbanner)?0:$clientbanner['ProgramBanner']['id'];

								$this->request->data['ProgramBanner']['id'] = $bannerid;
								$this->request->data['ProgramBanner']['program_id'] = $program_id;
								$this->request->data['ProgramBanner']['dashboard_id'] = 1;
								$extension_left=pathinfo($this->request->data['Image']['program_background_images_left_1']['name'], PATHINFO_EXTENSION);

								$this->request->data['ProgramBanner']['program_background_images_left_extension'] = $extension_left;
								$this->request->data['ProgramBanner']['modified'] = date('Y-m-d h:i:s');
								$this->Program->ProgramBanner->save($this->request->data);

								if ($extension_left != '') {
									copy($this->request->data['Image']['program_background_images_left_1']['tmp_name'], PROGRAM_BACKGROUND_IMAGE_LEFT_PATH. $program_id. '.'.$extension_left);
								}
							}
						}
						if ($this->request->data['ClientOverride']['right_mr_1'] ==1 )
						{
							if ($this->request->data['ImageDeleteRight']['program_background_images_right_1'] == 1) {
								$delete_images = array();
								foreach ($this->request->data['ImageDeleteRight'] as $key => $value) {
									if ($value == '1') {
										$this->File->deleteByPrefix('program_background_images_right/'.$program_id);
										$sql = "UPDATE program_banners SET program_background_images_right_extension='' WHERE dashboard_id=1 and program_id=".$program_id;
										$this->Program->ProgramBanner->query($sql);
									}
								}
							}
							else
							{
								$clientbanner = $this->Program->ProgramBanner->find('first',
										array('conditions'=>array('ProgramBanner.program_id' => $program_id, 'ProgramBanner.dashboard_id' => 1),
												'fields' => array('ProgramBanner.id, ProgramBanner.program_background_images_right_extension')));
								$bannerid = empty($clientbanner)?0:$clientbanner['ProgramBanner']['id'];

								$this->request->data['ProgramBanner']['id'] = $bannerid;
								$this->request->data['ProgramBanner']['program_id'] = $program_id;
								$this->request->data['ProgramBanner']['dashboard_id'] = 1;
								$extension_right=pathinfo($this->request->data['Image']['program_background_images_right_1']['name'], PATHINFO_EXTENSION);

								$this->request->data['ProgramBanner']['program_background_images_right_extension'] = $extension_right;
								$this->request->data['ProgramBanner']['modified'] = date('Y-m-d h:i:s');
								$this->Program->ProgramBanner->save($this->request->data);

								if ($extension_right != '') {
									copy($this->request->data['Image']['program_background_images_right_1']['tmp_name'], PROGRAM_BACKGROUND_IMAGE_RIGHT_PATH. $program_id. '.'.$extension_right);
								}
							}
						}
						if ($this->request->data['ClientOverride']['left_link_mr_1'] ==1 )
						{
							$clientbanner = $this->Program->ProgramBanner->find('first',
									array('conditions'=>array('ProgramBanner.program_id' => $program_id, 'ProgramBanner.dashboard_id' => 1),
											'fields' => array('ProgramBanner.id, ProgramBanner.left_link')));
							$bannerid = empty($clientbanner)?0:$clientbanner['ProgramBanner']['id'];

							$this->request->data['ProgramBanner']['id'] = $bannerid;
							$this->request->data['ProgramBanner']['program_id'] = $program_id;
							$this->request->data['ProgramBanner']['dashboard_id'] = 1;

							$this->request->data['ProgramBanner']['left_link'] =$this->request->data['Program']['left_link_1'];
							$this->request->data['ProgramBanner']['modified'] = date('Y-m-d h:i:s');
							$this->Program->ProgramBanner->save($this->request->data);
						}
						if ($this->request->data['ClientOverride']['right_link_mr_1'] ==1 )
						{
							$clientbanner = $this->Program->ProgramBanner->find('first',
									array('conditions'=>array('ProgramBanner.program_id' => $program_id, 'ProgramBanner.dashboard_id' => 1),
											'fields' => array('ProgramBanner.id, ProgramBanner.right_link')));
							$bannerid = empty($clientbanner)?0:$clientbanner['ProgramBanner']['id'];

							$this->request->data['ProgramBanner']['id'] = $bannerid;
							$this->request->data['ProgramBanner']['program_id'] = $program_id;
							$this->request->data['ProgramBanner']['dashboard_id'] = 1;
							$this->request->data['ProgramBanner']['right_link'] = $this->request->data['Program']['right_link_1'];
							$this->request->data['ProgramBanner']['modified'] = date('Y-m-d h:i:s');
							$this->Program->ProgramBanner->save($this->request->data);

						}
					}

					//background images for send a friend
					 if ($this->request->data['ClientOverride']['override_saf_bi'] == '1' && isset($this->request->data['ClientOverideClients_saf_bi']))
					{
						if ($this->request->data['ClientOverride']['left_saf_2'] ==1 )
						{
							if ($this->request->data['ImageDeleteLeft2']['program_background_images_left_2']==1) {
								$delete_images = array();
								foreach ($this->request->data['ImageDeleteLeft2'] as $key => $value) {
									if ($value == '1') {
										$this->File->deleteByPrefix('program_background_images_left_2/'.$program_id);
										$sql = "UPDATE program_banners SET program_background_images_left_extension='' WHERE dashboard_id=2 and program_id=".$program_id;
										$this->Program->ProgramBanner->query($sql);
									}
								}
							}
							else
							{
								$clientbanner = $this->Program->ProgramBanner->find('first',
										array('conditions'=>array('ProgramBanner.program_id' => $program_id, 'ProgramBanner.dashboard_id' => 2),
												'fields' => array('ProgramBanner.id, ProgramBanner.program_background_images_left_extension')));
								$bannerid = empty($clientbanner)?0:$clientbanner['ProgramBanner']['id'];

								$this->request->data['ProgramBanner']['id'] = $bannerid;
								$this->request->data['ProgramBanner']['program_id'] = $program_id;
								$this->request->data['ProgramBanner']['dashboard_id'] = 2;
								$extension_left=pathinfo($this->request->data['Image']['program_background_images_left_2']['name'], PATHINFO_EXTENSION);

								$this->request->data['ProgramBanner']['program_background_images_left_extension'] = $extension_left;
								$this->request->data['ProgramBanner']['modified'] = date('Y-m-d h:i:s');
								$this->Program->ProgramBanner->save($this->request->data);

								if ($extension_left != '') {
									copy($this->request->data['Image']['program_background_images_left_2']['tmp_name'], PROGRAM_BACKGROUND_IMAGE_LEFT_2_PATH. $program_id. '.'.$extension_left);
								}
							}
						}
						if ($this->request->data['ClientOverride']['right_saf_2'] ==1 )
						{
							if ($this->request->data['ImageDeleteRight2']['program_background_images_right_2'] == 1) {
								$delete_images = array();
								foreach ($this->request->data['ImageDeleteRight2'] as $key => $value) {
									if ($value == '1') {
										$this->File->deleteByPrefix('program_background_images_right_2/'.$program_id);
										$sql = "UPDATE program_banners SET program_background_images_right_extension='' WHERE dashboard_id=2 and program_id=".$program_id;
										$this->Program->ProgramBanner->query($sql);
									}
								}
							}
							else
							{
								$clientbanner = $this->Program->ProgramBanner->find('first',
										array('conditions'=>array('ProgramBanner.program_id' => $program_id, 'ProgramBanner.dashboard_id' => 2),
												'fields' => array('ProgramBanner.id, ProgramBanner.program_background_images_right_extension')));
								$bannerid = empty($clientbanner)?0:$clientbanner['ProgramBanner']['id'];

								$this->request->data['ProgramBanner']['id'] = $bannerid;
								$this->request->data['ProgramBanner']['program_id'] = $program_id;
								$this->request->data['ProgramBanner']['dashboard_id'] = 2;
								$extension_right=pathinfo($this->request->data['Image']['program_background_images_right_2']['name'], PATHINFO_EXTENSION);

								$this->request->data['ProgramBanner']['program_background_images_right_extension'] =$extension_right;
								$this->request->data['ProgramBanner']['modified'] = date('Y-m-d h:i:s');
								$this->Program->ProgramBanner->save($this->request->data);

								if ($extension_right != '') {
									copy($this->request->data['Image']['program_background_images_right_2']['tmp_name'], PROGRAM_BACKGROUND_IMAGE_RIGHT_2_PATH. $program_id. '.'.$extension_right);
								}
							}
						}
						if ($this->request->data['ClientOverride']['left_link_saf_2'] ==1 )
						{
							$clientbanner = $this->Program->ProgramBanner->find('first',
									array('conditions'=>array('ProgramBanner.program_id' => $program_id, 'ProgramBanner.dashboard_id' => 2),
											'fields' => array('ProgramBanner.id, ProgramBanner.left_link, ProgramBanner.right_link')));
							$bannerid = empty($clientbanner)?0:$clientbanner['ProgramBanner']['id'];

							$this->request->data['ProgramBanner']['id'] = $bannerid;
							$this->request->data['ProgramBanner']['program_id'] = $program_id;
							$this->request->data['ProgramBanner']['dashboard_id'] = 2;

							$this->request->data['ProgramBanner']['left_link'] = $this->request->data['Program']['left_link_2'];
							$this->request->data['ProgramBanner']['modified'] = date('Y-m-d h:i:s');
							$this->Program->ProgramBanner->save($this->request->data);
						}
						if ($this->request->data['ClientOverride']['right_link_saf_2'] ==1 )
						{
							$clientbanner = $this->Program->ProgramBanner->find('first',
									array('conditions'=>array('ProgramBanner.program_id' => $program_id, 'ProgramBanner.dashboard_id' => 2),
											'fields' => array('ProgramBanner.id, ProgramBanner.right_link')));
							$bannerid = empty($clientbanner)?0:$clientbanner['ProgramBanner']['id'];

							$this->request->data['ProgramBanner']['id'] = $bannerid;
							$this->request->data['ProgramBanner']['program_id'] = $program_id;
							$this->request->data['ProgramBanner']['dashboard_id'] = 2;

							$this->request->data['ProgramBanner']['right_link'] = $this->request->data['Program']['right_link_2'];
							$this->request->data['ProgramBanner']['modified'] = date('Y-m-d h:i:s');
							$this->Program->ProgramBanner->save($this->request->data);
						}
					}

					//background images for points
					if ($this->request->data['ClientOverride']['override_points_bi'] == '1' && isset($this->request->data['ClientOverideClients_points_bi']))
					{
						if ($this->request->data['ClientOverride']['left_points_3'] ==1 )
						{
							if ($this->request->data['ImageDeleteLeft3']['program_background_images_left_3']==1) {
								$delete_images = array();
								foreach ($this->request->data['ImageDeleteLeft3'] as $key => $value) {
									if ($value == '1') {
										$this->File->deleteByPrefix('program_background_images_left_3/'.$program_id);
										$sql = "UPDATE program_banners SET program_background_images_left_extension='' WHERE dashboard_id=3 and program_id=".$program_id;
										$this->Program->ProgramBanner->query($sql);
									}
								}
							}
							else
							{
								$clientbanner = $this->Program->ProgramBanner->find('first',
										array('conditions'=>array('ProgramBanner.program_id' => $program_id, 'ProgramBanner.dashboard_id' => 3),
												'fields' => array('ProgramBanner.id, ProgramBanner.program_background_images_left_extension')));
								$bannerid = empty($clientbanner)?0:$clientbanner['ProgramBanner']['id'];

								$this->request->data['ProgramBanner']['id'] = $bannerid;
								$this->request->data['ProgramBanner']['program_id'] = $program_id;
								$this->request->data['ProgramBanner']['dashboard_id'] = 3;
								$extension_left=pathinfo($this->request->data['Image']['program_background_images_left_3']['name'], PATHINFO_EXTENSION);

								$this->request->data['ProgramBanner']['program_background_images_left_extension'] = $extension_left;

								$this->request->data['ProgramBanner']['modified'] = date('Y-m-d h:i:s');
								$this->Program->ProgramBanner->save($this->request->data);

								if ($extension_left != '') {
									copy($this->request->data['Image']['program_background_images_left_3']['tmp_name'], PROGRAM_BACKGROUND_IMAGE_LEFT_3_PATH. $program_id. '.'.$extension_left);
								}
							}
						}
						if ($this->request->data['ClientOverride']['right_points_3'] ==1 )
						{
							if ($this->request->data['ImageDeleteRight3']['program_background_images_right_3'] == 1) {
								$delete_images = array();
								foreach ($this->request->data['ImageDeleteRight3'] as $key => $value) {
									if ($value == '1') {
										$this->File->deleteByPrefix('program_background_images_right_3/'.$program_id);
										$sql = "UPDATE program_banners SET program_background_images_right_extension='' WHERE dashboard_id=3 and program_id=".$program_id;
										$this->Program->ProgramBanner->query($sql);
									}
								}
							}
							else
							{
								$clientbanner = $this->Program->ProgramBanner->find('first',
										array('conditions'=>array('ProgramBanner.program_id' => $program_id, 'ProgramBanner.dashboard_id' => 3),
												'fields' => array('ProgramBanner.id, ProgramBanner.program_background_images_right_extension')));
								$bannerid = empty($clientbanner)?0:$clientbanner['ProgramBanner']['id'];

								$this->request->data['ProgramBanner']['id'] = $bannerid;
								$this->request->data['ProgramBanner']['program_id'] = $program_id;
								$this->request->data['ProgramBanner']['dashboard_id'] = 3;
								$extension_right=pathinfo($this->request->data['Image']['program_background_images_right_3']['name'], PATHINFO_EXTENSION);

								$this->request->data['ProgramBanner']['program_background_images_right_extension'] = $extension_right;

								$this->request->data['ProgramBanner']['modified'] = date('Y-m-d h:i:s');
								$this->Program->ProgramBanner->save($this->request->data);

								if ($extension_right != '') {
									copy($this->request->data['Image']['program_background_images_right_3']['tmp_name'], PROGRAM_BACKGROUND_IMAGE_RIGHT_3_PATH. $program_id. '.'.$extension_right);
								}
							}
						}
						if ($this->request->data['ClientOverride']['left_link_points_3'] ==1 )
						{
							$clientbanner = $this->Program->ProgramBanner->find('first',
									array('conditions'=>array('ProgramBanner.program_id' => $program_id, 'ProgramBanner.dashboard_id' => 3),
											'fields' => array('ProgramBanner.id, ProgramBanner.left_link')));
							$bannerid = empty($clientbanner)?0:$clientbanner['ProgramBanner']['id'];

							$this->request->data['ProgramBanner']['id'] = $bannerid;
							$this->request->data['ProgramBanner']['program_id'] = $program_id;
							$this->request->data['ProgramBanner']['dashboard_id'] = 3;
							$this->request->data['ProgramBanner']['left_link'] = $this->request->data['Program']['left_link_3'];
							$this->request->data['ProgramBanner']['modified'] = date('Y-m-d h:i:s');
							$this->Program->ProgramBanner->save($this->request->data);
						}
						if ($this->request->data['ClientOverride']['right_link_points_3'] ==1 )
						{
							$clientbanner = $this->Program->ProgramBanner->find('first',
									array('conditions'=>array('ProgramBanner.program_id' => $program_id, 'ProgramBanner.dashboard_id' => 3),
											'fields' => array('ProgramBanner.id, ProgramBanner.right_link')));
							$bannerid = empty($clientbanner)?0:$clientbanner['ProgramBanner']['id'];

							$this->request->data['ProgramBanner']['id'] = $bannerid;
							$this->request->data['ProgramBanner']['program_id'] = $program_id;
							$this->request->data['ProgramBanner']['dashboard_id'] = 3;

							$this->request->data['ProgramBanner']['right_link'] = $this->request->data['Program']['right_link_3'];
							$this->request->data['ProgramBanner']['modified'] = date('Y-m-d h:i:s');
							$this->Program->ProgramBanner->save($this->request->data);
						}

					}


				/* saving data ends */

				$cat = $this->Program->ProgramsCategory->find('all',array('conditions'=>array('program_id'=>$program_id),'fields'=>array('category_id')));
				$acat = array();$acats = array();
				foreach($cat as $c)
				{
					array_push($acat,$c['ProgramsCategory']['category_id']);
				}
				$acats = implode(",",$acat);

				$delete = array_diff($bcat, $acat);
				$new = array_diff($acat, $bcat);

				if(count($delete)>0 || count($new)>0)
				{
					$update_delete_product = "update products set modified=NOW() where id in
					(select a.product_id from programs_products a where a.program_id=".$program_id.")";
					$this->Program->Product->query($update_delete_product);

					$del_sql = "DELETE from programs_products where program_id=".$program_id;
					$this->Program->ProgramsProduct->query($del_sql);

					$insert_sql = "INSERT INTO programs_products(`program_id`,`modified`, `product_id`) SELECT distinct ".$program_id.", NOW(),  a.product_id from categories_products a where a.category_id in (".$acats.") ";
					$this->Program->ProgramsProduct->query($insert_sql);

					$update_product = "update products set modified=NOW() where id in (select distinct a.product_id from categories_products a where a.category_id in (".$acats."))";
					$this->Program->Product->query($update_product);
				}

				//	save the images
				foreach ($this->request->data['Image'] as $key => $file) {
					if ($file['name'] != '') {
						move_uploaded_file($file['tmp_name'], FILES_PATH. $key. '/'. $program_id. '.'. $this->request->data['Program'][$key. '_extension']);
					}
				}

				//	save for the clients, if set
				if ($this->request->data['ClientOverride']['override'] == '1' && isset($this->request->data['ClientOverideClients'])) {
					$this->loadModel("Client");
					$this->loadModel("ClientsSpecialProduct");
					$this->loadModel("Dashboard");
					$this->loadModel('PaypalDetail');
					foreach ($this->request->data['ClientOverideClients'] as $client_id) {

						$this->Program->Client->unbindModel(array('hasAndBelongsToMany' => array('Product'), 'hasMany' => array('ClientPage', 'User')));
						$old_client_data = $this->Program->Client->findById($client_id);

						$data = array();
						$data['Client']['id'] = $client_id;
						if ($this->request->data['ClientOverride']['style_id']==1)
						{
							$this->loadModel("Dashboard");
							$sql = "update clients_dashboards set style_id='".$this->request->data['Program']['style_id']."' where client_id='".$client_id."' and dashboard_id=1";
							$this->Dashboard->query($sql);
						}
						if (isset($this->request->data['ClientOverride']['voucher_id'])) {
							$data['Client']['voucher_id'] = $this->request->data['Program']['voucher_id'];
						}
						if (isset($this->request->data['ClientOverride']['domain_id'])) {
							$data['Client']['domain_id'] = $this->request->data['Program']['domain_id'];
						}
						if (isset($this->request->data['ClientOverride']['special_offer_module_limit'])) {
							$data['Client']['special_offer_module_limit'] = $this->request->data['Program']['special_offer_module_limit'];
						}

						if (isset($this->request->data['ClientOverride']['whats_new_module_limit'])) {
							$data['Client']['whats_new_module_limit'] = $this->request->data['Program']['whats_new_module_limit'];
						}

						$models = array();
						//	categories
						if (isset($this->request->data['ClientOverride']['Category'])) {
							//Saving categories
							$this->Program->Client->ClientsCategory->deleteAll(array('client_id'=>$client_id));
							$clientCategoryData = array();
							foreach($this->request->data['Category']['Category'] as $clientCategory){
								if(!empty($clientCategory)){
									$clientCategoryData[] = array('client_id'=>$client_id, 'category_id'=>$clientCategory);
								}
							}
							if(!empty($clientCategoryData)){$this->Program->Client->ClientsCategory->saveMany($clientCategoryData);}
							$models[] = 'Category';
						}

						// modules
						if (isset($this->request->data['ClientOverride']['Module'])) {
							$data['Module'] = $this->request->data['Module'];
							$models[] = 'Module';
						}

						if ($this->request->data['ClientOverride']['SpecialProduct']==1)
						{
							foreach (array_slice($this->request->data['SpecialProduct']['SpecialProduct'], 1) as $specialproducts)
							{
								if(!empty($specialproducts)){
									$clientbanner = $this->ClientsSpecialProduct->find('first', array('conditions' => array('ClientsSpecialProduct.client_id' => $client_id,'ClientsSpecialProduct.product_id' => $specialproducts,'ClientsSpecialProduct.dashboard_id' => 1), 'fields' => array('ClientsSpecialProduct.id')));
									$clientspecialproductid = empty($clientbanner)?0:$clientbanner['ClientsSpecialProduct']['id'];
	
									$this->request->data['ClientsSpecialProduct']['id'] = $clientspecialproductid;
									$this->request->data['ClientsSpecialProduct']['client_id'] = $client_id;
									$this->request->data['ClientsSpecialProduct']['product_id'] = $specialproducts;
									$this->request->data['ClientsSpecialProduct']['dashboard_id'] = 1;
									$this->ClientsSpecialProduct->save($this->request->data);
								}
							}

						}

						// What's new offer module products
						if (isset($this->request->data['ClientOverride']['WhatsNewProduct'])) {
							$data['WhatsNewProduct'] = $this->request->data['WhatsNewProduct'];
							$models[] = 'WhatsNewProduct';
						}

						//Home Page
						if(isset($this->request->data['ClientOverride']['home_page'])){
							$data['Client']['home_page'] = $this->request->data['Program']['home_page'];
						}

						//Logout redirect
						if(isset($this->request->data['ClientOverride']['logout_redirect'])){
							$data['Client']['logout_redirect'] = $this->request->data['Program']['logout_redirect'];
						}

						//Live Chat enable
						if(isset($this->request->data['ClientOverride']['live_chat_enabled'])){
							$data['Client']['live_chat_enabled'] = $this->request->data['Program']['live_chat_enabled'];
						}

						//Cart enable
						if(isset($this->request->data['ClientOverride']['cart_enabled'])){
							$data['Client']['cart_enabled'] = $this->request->data['Program']['cart_enabled'];

							//insert paypal details in paypal_details table
							$paypal_details = $this->PaypalDetail->find('first',array('conditions'=>array('PaypalDetail.program_id'=>$program_id,'PaypalDetail.client_id'=>$client_id)));
							if(empty($paypal_details))
							{
								$this->PaypalDetail->create();
								$this->request->data['PaypalDetail']['client_id'] = $client_id;
								$this->request->data['PaypalDetail']['program_id'] = $program_id;
								$this->request->data['PaypalDetail']['paypal_business_name'] = $this->request->data['Program']['paypal_business_name'];
								$this->request->data['PaypalDetail']['paypal_currency_code'] = $this->request->data['Program']['paypal_currency_code'];
								$this->request->data['PaypalDetail']['user_id'] = $this->Session->read('user.User.id');
								$this->request->data['PaypalDetail']['created'] = date('Y-m-d h:i:s');
								$this->PaypalDetail->save($this->request->data);
							}
							else
							{
								$sql = "UPDATE paypal_details as PaypalDetail set PaypalDetail.paypal_business_name='".$this->request->data['Program']['paypal_business_name']."',
								paypal_currency_code = '".$this->request->data['Program']['paypal_currency_code']."', user_id=". $this->Session->read('user.User.id').",
								created='".date('Y-m-d h:i:s')."' WHERE PaypalDetail.program_id=".$program_id." and PaypalDetail.client_id=".$client_id;
								$this->PaypalDetail->query($sql);
							}
						}

						//Header links disable
						if(isset($this->request->data['ClientOverride']['header_links_disabled'])){
							$data['Client']['header_links_disabled'] = $this->request->data['Program']['header_links_disabled'];
						}

						//footer links disable
						if(isset($this->request->data['ClientOverride']['footer_links_disabled'])){
							$data['Client']['footer_links_disabled'] = $this->request->data['Program']['footer_links_disabled'];
						}

						//Refine Search disable
						if(isset($this->request->data['ClientOverride']['refine_search_disabled'])){
							$data['Client']['refine_search_disabled'] = $this->request->data['Program']['refine_search_disabled'];
						}

						//Email Alerts disable
						if(isset($this->request->data['ClientOverride']['email_alerts_disabled'])){
							$data['Client']['email_alerts_disabled'] = $this->request->data['Program']['email_alerts_disabled'];
						}

						//Copyright Text disable
						if (isset($this->request->data['ClientOverride']['copyright_text']))
						{
							if($this->request->data['Program']['copyright_disabled']==1)
								$data['Client']['copyright_disabled'] = 1;
							else
								$data['Client']['copyright_disabled'] = 0;

							$data['Client']['copyright_text'] = $this->request->data['Program']['copyright_text'];
						}

						//Facebook Link disable
						if (isset($this->request->data['ClientOverride']['facebook_link']))
						{
							if($this->request->data['Program']['facebook_disabled']==1)
								$data['Client']['facebook_disabled'] = 1;
							else
								$data['Client']['facebook_disabled'] = 0;

							$data['Client']['facebook_link'] = $this->request->data['Program']['facebook_link'];
						}

						//Twitter Link disable
					   	if (isset($this->request->data['ClientOverride']['twitter_link']))
					   	{
					   		if($this->request->data['Program']['twitter_disabled']==1)
								$data['Client']['twitter_disabled'] = 1;
							else
					 			$data['Client']['twitter_disabled'] = 0;

							$data['Client']['twitter_link'] = $this->request->data['Program']['twitter_link'];
						}

						//Error Login text
						if(isset($this->request->data['ClientOverride']['login_error'])){
							$data['Client']['login_error_text'] = $this->request->data['Program']['login_error_text'];
						}

						//First Time Login text
						if(isset($this->request->data['ClientOverride']['first_time_login'])){
							$data['Client']['first_time_login_txt'] = $this->request->data['Program']['first_time_login_txt'];
						}

						//Share on Facebook
						if(isset($this->request->data['ClientOverride']['share_facbook'])){
							$data['Client']['fb_share_disabled'] = $this->request->data['Program']['fb_share_disabled'];
							$data['Client']['fb_description_txt'] = $this->request->data['Program']['fb_description_txt'];
						}

						//Payroll Deduct
						if(isset($this->request->data['ClientOverride']['payroll_deduct'])){
							$data['Client']['payroll_deduct_disabled'] = $this->request->data['Program']['payroll_deduct_disabled'];
						}

						//Referral Number
						if(isset($this->request->data['ClientOverride']['referral_number'])){
							$data['Client']['referral_no_disabled'] = $this->request->data['Program']['referral_no_disabled'];
						}

						//	log the client change
						$log = array(
							'id' => null,
							'foreign_id' => $client_id,
							'foreign_model' => 'Client',
							'user_id' => $this->Session->read('user.User.id'),
							'old_data' => $old_client_data,
							'new_data' => $data
						);

						$log['description'] = 'Client updated from program';
						$log['notes'] = '';

						//	add the related changes
						foreach ($models as $model)  {
							switch ($model) {		//	fields to add to the comments display
								case 'Module':
									$display = 'title';
									break;
								case 'Category' :
									$display = 'description';
								default:
									$display = 'name';
							}

							$tmp_old_related = array();
							if (isset($old_client_data[$model]) && isset($this->request->data[$model][$model])) {
								//	deleted
								foreach ($old_client_data[$model] as $model_data) {
									if (!in_array($model_data['id'], $this->request->data[$model][$model])) {
										$log['old_data']['Clients'. $model][Inflector::humanize(Inflector::underscore($model)). ': '. $model_data[$display]] = '1';
										$log['new_data']['Clients'. $model][Inflector::humanize(Inflector::underscore($model)). ': '. $model_data[$display]] = '0';
									}
									$tmp_old_related[] = $model_data['id'];
								}
								//	added
								foreach ($this->request->data[$model][$model] as $model_id) {
									if ($model_id != '' && !in_array($model_id, $tmp_old_related)) {
										$this->Program->Client->{$model}->recursive = -1;
										$model_data = $this->Program->Client->{$model}->findById($model_id);
										$log['old_data']['Clients'. $model][Inflector::humanize(Inflector::underscore($model)). ': '. $model_data[$model][$display]] = '0';
										$log['new_data']['Clients'. $model][Inflector::humanize(Inflector::underscore($model)). ': '. $model_data[$model][$display]] = '1';
									}
								}
							}
						}


						// products
						if (isset($this->request->data['ClientOverride']['Product'])) {

							if($this->request->data['ClientOverride']['exclude_product'] == 0 || (empty($this->request->data['ClientOverride']['exclude_product']) || $this->request->data['ClientOverride']['exclude_product']==''))
							{
								$update_delete_product = "update products set modified=NOW() where id in
								(select product_id from (SELECT clients_products.product_id FROM clients_products
								INNER JOIN products on products.id = clients_products.product_id
								WHERE clients_products.client_id = '". $client_id. "'
								AND products.assignment_override = 0 and (products.specific_to_client=0 || products.specific_to_client=1 || products.specific_to_client=2)) as tmptable)";
								$this->Program->Product->query($update_delete_product);

								$sql = "DELETE clients_products
								FROM clients_products
								INNER JOIN products on products.id = clients_products.product_id
								WHERE clients_products.client_id = '". $client_id. "'
								AND products.assignment_override = 0 and (products.specific_to_client=0 || products.specific_to_client=1 || products.specific_to_client=2)";
								$this->Program->query($sql);

								$sql = "INSERT INTO clients_products (client_id, product_id)
								SELECT '". $client_id. "', product_id FROM programs_products
								INNER JOIN products on products.id = programs_products.product_id
								WHERE programs_products.program_id = '". $program_id. "'
								AND products.assignment_override = 0 and
								(products.specific_to_client=0 || products.specific_to_client=1 || products.specific_to_client=2)
								AND programs_products.product_id in (SELECT categories_products.product_id
								FROM programs_categories, categories_products WHERE programs_categories.category_id=categories_products.category_id
								AND programs_categories.program_id= '". $program_id. "')";
								$this->Program->query($sql);


								$blockSql = "SELECT blacklisted_product from clients where id=".$client_id;
								$list = $this->Program->Client->query($blockSql);
								if(!empty($list[0]['clients']['blacklisted_product'])){
									$sql1 = "update clients_products set blocked=1, modified=NOW() where product_id in (".$list[0]['clients']['blacklisted_product'].")
									and client_id=".$client_id;
									$this->Program->query($sql1);
								}

								$update_product = "update products set modified=NOW() where id in
								(select product_id from (SELECT programs_products.product_id FROM programs_products
								INNER JOIN products on products.id = programs_products.product_id
								WHERE programs_products.program_id = '". $program_id. "'
								AND products.assignment_override = 0 and
								(products.specific_to_client=0 || products.specific_to_client=1 || products.specific_to_client=2)) as tmptable)";
								$this->Program->Product->query($update_product);
							}
							elseif($this->request->data['ClientOverride']['exclude_product'] == 1)
							{
								$update_delete_product = "update products set modified=NOW() where id in
								(select product_id from (SELECT clients_products.product_id FROM clients_products
								INNER JOIN products on products.id = clients_products.product_id
								WHERE clients_products.client_id = '". $client_id. "'
								AND products.assignment_override = 0 and products.specific_to_client=0) as tmptable)";
								$this->Program->Product->query($update_delete_product);

								$sql = "DELETE clients_products
								FROM clients_products
								INNER JOIN products on products.id = clients_products.product_id
								WHERE clients_products.client_id = '". $client_id. "'
								AND products.assignment_override = 0 and products.specific_to_client=0";
								$this->Program->query($sql);

								$sql = "INSERT INTO clients_products (client_id, product_id)
								SELECT '". $client_id. "', product_id
								FROM programs_products
								INNER JOIN products on products.id = programs_products.product_id
								WHERE programs_products.program_id = '". $program_id. "'
								AND products.assignment_override = 0 and products.specific_to_client=0
								AND programs_products.product_id in (SELECT categories_products.product_id
								FROM programs_categories, categories_products WHERE programs_categories.category_id=categories_products.category_id
								AND programs_categories.program_id= '". $program_id. "' )";
								$this->Program->query($sql);

								$blockSql = "SELECT blacklisted_product from clients where id=".$client_id;
								$list = $this->Program->Client->query($blockSql);
								$sql1 = "update clients_products set blocked=1, modified=NOW() where product_id in (".$list[0]['clients']['blacklisted_product'].")
								and client_id=".$client_id;
								$this->Program->query($sql1);

								$update_product = "update products set modified=NOW() where id in
								(select product_id from (SELECT programs_products.product_id FROM programs_products
								INNER JOIN products on products.id = programs_products.product_id
								WHERE programs_products.program_id = '". $program_id. "'
								AND products.assignment_override = 0 and products.specific_to_client=0) as tmptable)";
								$this->Program->Product->query($update_product);
							}
							elseif($this->request->data['ClientOverride']['exclude_product'] == 2)
							{
								$update_delete_product = "update products set modified=NOW() where id in
								(select product_id from (SELECT clients_products.product_id FROM clients_products
								INNER JOIN products on products.id = clients_products.product_id
								WHERE clients_products.client_id = '". $client_id. "'
								AND products.assignment_override = 0 and (products.specific_to_client=0 || products.specific_to_client=2)) as tmptable)";
								$this->Program->Product->query($update_delete_product);

								$sql = "DELETE clients_products
								FROM clients_products
								INNER JOIN products on products.id = clients_products.product_id
								WHERE clients_products.client_id = '". $client_id. "'
								AND products.assignment_override = 0 and (products.specific_to_client=0 || products.specific_to_client=2)
								AND programs_products.product_id in (SELECT categories_products.product_id
								FROM programs_categories, categories_products WHERE programs_categories.category_id=categories_products.category_id
								AND programs_categories.program_id= '". $program_id. "' )";
								$this->Program->query($sql);

								$sql = "INSERT INTO clients_products (client_id, product_id)
								SELECT '". $client_id. "', product_id
								FROM programs_products
								INNER JOIN products on products.id = programs_products.product_id
								WHERE programs_products.program_id = '". $program_id. "'
								AND products.assignment_override = 0 and (products.specific_to_client=0 || products.specific_to_client=2)";
								$this->Program->query($sql);

								$blockSql = "SELECT blacklisted_product from clients where id=".$client_id;
								$list = $this->Program->Client->query($blockSql);
								$sql1 = "update clients_products set blocked=1, modified=NOW() where product_id in (".$list[0]['clients']['blacklisted_product'].")
								and client_id=".$client_id;
								$this->Program->query($sql1);

								$update_product = "update products set modified=NOW() where id in
								(select product_id from (SELECT programs_products.product_id FROM programs_products
								INNER JOIN products on products.id = programs_products.product_id
								WHERE programs_products.program_id = '". $program_id. "'
								AND products.assignment_override = 0 and (products.specific_to_client=0 || products.specific_to_client=2)) as tmptable)";
								$this->Program->Product->query($update_product);
							}


							$log['notes'] = 'Products updated to match Program';
						}

						//Location

						if (isset($this->request->data['ClientOverride']['Location']))
						{
							$sql = "DELETE clients_countries
										FROM clients_countries
										WHERE clients_countries.client_id = '". $client_id. "'";
							$this->Program->query($sql);

							$sql = "INSERT INTO clients_countries (client_id, country_id)
										SELECT '". $client_id. "', country_id
										FROM programs_countries
										WHERE programs_countries.program_id = '". $program_id. "'";
							$this->Program->query($sql);

							$country_sql = "select country from programs where id=". $program_id;
							$country =  $this->Program->query($country_sql);
							$sql = "update clients set country='".$country[0]['programs']['country']."' where id='".$client_id."'";
							$this->Program->query($sql);

							$log['notes'] = 'Countries updated to match Program';
						}

						$this->save_log($log);

						$this->Program->Client->save($data);

					}

				}
				if ($this->request->data['ClientOverride']['override_saf'] == '1' && isset($this->request->data['ClientOverideClients_saf']))
				{
					$this->loadModel("Client");
					$this->loadModel("ClientsSpecialProduct");
					$this->loadModel("Dashboard");
					foreach ($this->request->data['ClientOverideClients_saf'] as $client_id)
					{
						$data = array();
						$data['Client']['id'] = $client_id;
						if ($this->request->data['ClientOverride']['style_id_saf']==1)
						{

							$sql = "update clients_dashboards set style_id='".$this->request->data['Program']['style_id_saf']."' where client_id='".$client_id."' and dashboard_id=2";
							$this->Dashboard->query($sql);
						}
						if ($this->request->data['ClientOverride']['special_offer_module_limit_saf']==1)
						{
							$sql = "update clients set special_offer_module_limit_saf='".$this->request->data['Program']['special_offer_module_limit_saf']."' where id='".$client_id."'";
							$this->Client->query($sql);
						}
						// special offer module products
						if ($this->request->data['ClientOverride']['SpecialProduct_saf']==1)
						{

							foreach (array_slice($this->request->data['SpecialProduct']['SpecialProduct_2'], 1) as $specialproducts)
							{
								$clientbanner = $this->ClientsSpecialProduct->find('first', array('conditions' => array('ClientsSpecialProduct.client_id' => $client_id,'ClientsSpecialProduct.product_id' => $specialproducts,'ClientsSpecialProduct.dashboard_id' => 2), 'fields' => array('ClientsSpecialProduct.id')));
								$clientspecialproductid = empty($clientbanner)?0:$clientbanner['ClientsSpecialProduct']['id'];

								$this->request->data['ClientsSpecialProduct']['id'] = $clientspecialproductid;
								$this->request->data['ClientsSpecialProduct']['client_id'] = $client_id;
								$this->request->data['ClientsSpecialProduct']['product_id'] = $specialproducts;
								$this->request->data['ClientsSpecialProduct']['dashboard_id'] = 2;
								$this->ClientsSpecialProduct->save($this->request->data);
							}

						}
					 }
				 }
				if ($this->request->data['ClientOverride']['override_points'] == '1' && isset($this->request->data['ClientOverideClients_points']))
				 {
						$this->loadModel("Client");
						$this->loadModel("ClientsSpecialProduct");
						$this->loadModel("Dashboard");
						foreach ($this->request->data['ClientOverideClients_points'] as $client_id)
						{
							$data = array();
							$data['Client']['id'] = $client_id;
							if ($this->request->data['ClientOverride']['style_id_points']==1)
							{

								$sql = "update clients_dashboards set style_id='".$this->request->data['Program']['style_id_points']."' where client_id='".$client_id."' and dashboard_id=3";
								$this->Dashboard->query($sql);
							}
							if ($this->request->data['ClientOverride']['special_offer_module_limit_points']==1)
							{
								$sql = "update clients set special_offer_module_limit_points='".$this->request->data['Program']['special_offer_module_limit_points']."' where id='".$client_id."'";
								$this->Client->query($sql);
							}
							// special offer module products
							if ($this->request->data['ClientOverride']['SpecialProduct_points']==1)
							{

								foreach (array_slice($this->request->data['SpecialProduct']['SpecialProduct_3'], 1) as $specialproducts)
								{
									$clientbanner = $this->ClientsSpecialProduct->find('first', array('conditions' => array('ClientsSpecialProduct.client_id' => $client_id,'ClientsSpecialProduct.product_id' => $specialproducts,'ClientsSpecialProduct.dashboard_id' => 3), 'fields' => array('ClientsSpecialProduct.id')));
									$clientspecialproductid = empty($clientbanner)?0:$clientbanner['ClientsSpecialProduct']['id'];

									$this->request->data['ClientsSpecialProduct']['id'] = $clientspecialproductid;
									$this->request->data['ClientsSpecialProduct']['client_id'] = $client_id;
									$this->request->data['ClientsSpecialProduct']['product_id'] = $specialproducts;
									$this->request->data['ClientsSpecialProduct']['dashboard_id'] = 3;
									$this->ClientsSpecialProduct->save($this->request->data);
								}

							}
						 }
					 }


					//saving background images and links for clients of myrewards dashboard.
					 if ($this->request->data['ClientOverride']['override_mr_bi'] == '1' && isset($this->request->data['ClientOverideClients_mr_bi']))
					 {
						$this->loadModel("Client");
						$this->loadModel("ClientBanner");
						$this->loadModel("Dashboard");
						foreach ($this->request->data['ClientOverideClients_mr_bi'] as $client_id)
						{
							if ($this->request->data['ClientOverride']['left_mr_1'] ==1 )
							{
								if ($this->request->data['ImageDeleteLeft']['program_background_images_left_1'] == 1)  {
									$delete_images = array();
									foreach ($this->request->data['ImageDeleteLeft'] as $key => $value) {
										if ($value == '1') {
											$this->File->deleteByPrefix('client_background_images_left/'.$client_id);
											$sql = "UPDATE client_banners SET client_background_images_left_extension='' WHERE dashboard_id=1 and client_id=".$client_id;
											$this->ClientBanner->query($sql);
										}
									}
								}
								else
								{
									$clientbanner = $this->Client->ClientBanner->find('first',
											array('conditions'=>array('ClientBanner.client_id' => $client_id, 'ClientBanner.dashboard_id' => 1),
													'fields' => array('ClientBanner.id, ClientBanner.client_background_images_left_extension')));
									$bannerid = empty($clientbanner)?0:$clientbanner['ClientBanner']['id'];

									$backgrnd_image_left_extension=pathinfo($this->request->data['Image']['program_background_images_left_1']['name'], PATHINFO_EXTENSION);

									$this->request->data['ClientBanner']['id'] = $bannerid;
									$this->request->data['ClientBanner']['client_id'] = $client_id;
									$this->request->data['ClientBanner']['dashboard_id'] = 1;
									$this->request->data['ClientBanner']['client_background_images_left_extension'] = $backgrnd_image_left_extension;

									$this->Client->ClientBanner->save($this->request->data);
									if ($backgrnd_image_left_extension != '')
									{
										//copy($this->request->data['Image']['program_background_images_left_1']['tmp_name'], CLIENT_BACKGROUND_IMAGE_LEFT_PATH. $client_id. '.'.$backgrnd_image_left_extension);
										copy(PROGRAM_BACKGROUND_IMAGE_LEFT_PATH. $program_id. '.'.$backgrnd_image_left_extension, CLIENT_BACKGROUND_IMAGE_LEFT_PATH. $client_id. '.'.$backgrnd_image_left_extension);
									}
								}
							}

							if($this->request->data['ClientOverride']['right_mr_1'] ==1)
							{
								if ($this->request->data['ImageDeleteRight']['program_background_images_right_1'] == 1) {
									$delete_images = array();
									foreach ($this->request->data['ImageDeleteRight'] as $key => $value) {
										if ($value == '1') {
											$this->File->deleteByPrefix('client_background_images_right/'.$client_id);
											$sql = "UPDATE client_banners SET client_background_images_right_extension='' WHERE dashboard_id=1 and client_id=".$client_id;
											$this->ClientBanner->query($sql);
										}
									}
								}
								else
								{
									$clientbanner = $this->Client->ClientBanner->find('first',
											array('conditions'=>array('ClientBanner.client_id' => $client_id, 'ClientBanner.dashboard_id' => 1),
													'fields' => array('ClientBanner.id, ClientBanner.client_background_images_right_extension')));
									$bannerid = empty($clientbanner)?0:$clientbanner['ClientBanner']['id'];

									$backgrnd_image_right_extension=pathinfo($this->request->data['Image']['program_background_images_right_1']['name'], PATHINFO_EXTENSION);

									$this->request->data['ClientBanner']['id'] = $bannerid;
									$this->request->data['ClientBanner']['client_id'] = $client_id;
									$this->request->data['ClientBanner']['dashboard_id'] = 1;
									$this->request->data['ClientBanner']['client_background_images_right_extension'] = $backgrnd_image_right_extension;

									$this->Client->ClientBanner->save($this->request->data);
									if ($backgrnd_image_right_extension != '')
									{
										//copy($this->request->data['Image']['program_background_images_right_1']['tmp_name'], CLIENT_BACKGROUND_IMAGE_RIGHT_PATH. $client_id. '.'.$backgrnd_image_right_extension);
										copy(PROGRAM_BACKGROUND_IMAGE_RIGHT_PATH. $program_id. '.'.$backgrnd_image_right_extension, CLIENT_BACKGROUND_IMAGE_RIGHT_PATH. $client_id. '.'.$backgrnd_image_right_extension);
									}
								}
							}

							if( $this->request->data['ClientOverride']['left_link_mr_1']==1 )
							{
								$clientbanner = $this->Client->ClientBanner->find('first',
										array('conditions'=>array('ClientBanner.client_id' => $client_id, 'ClientBanner.dashboard_id' => 1),
												'fields' => array('ClientBanner.id, ClientBanner.left_link')));
								$bannerid = empty($clientbanner)?0:$clientbanner['ClientBanner']['id'];

								$this->request->data['ClientBanner']['id'] = $bannerid;
								$this->request->data['ClientBanner']['client_id'] = $client_id;
								$this->request->data['ClientBanner']['dashboard_id'] = 1;
								$this->request->data['ClientBanner']['left_link'] = $this->request->data['Program']['left_link_1'];
								$this->request->data['ClientBanner']['modified'] = date('Y-m-d h:i:s');
								$this->Client->ClientBanner->save($this->request->data);
							}

							if($this->request->data['ClientOverride']['right_link_mr_1']==1)
							{
								$clientbanner = $this->Client->ClientBanner->find('first',
										array('conditions'=>array('ClientBanner.client_id' => $client_id, 'ClientBanner.dashboard_id' => 1),
												'fields' => array('ClientBanner.id, ClientBanner.right_link')));
								$bannerid = empty($clientbanner)?0:$clientbanner['ClientBanner']['id'];

								$this->request->data['ClientBanner']['id'] = $bannerid;
								$this->request->data['ClientBanner']['client_id'] = $client_id;
								$this->request->data['ClientBanner']['dashboard_id'] = 1;
								$this->request->data['ClientBanner']['right_link'] = $this->request->data['Program']['right_link_1'];
								$this->request->data['ClientBanner']['modified'] = date('Y-m-d h:i:s');
								$this->Client->ClientBanner->save($this->request->data);
							}
						}
					 }

					 //saving background images and links for clients of send a friend dashboard.
					 if ($this->request->data['ClientOverride']['override_saf_bi'] == '1' && isset($this->request->data['ClientOverideClients_saf_bi']))
					 {
						$this->loadModel("Client");
						$this->loadModel("ClientBanner");
						$this->loadModel("Dashboard");
						foreach ($this->request->data['ClientOverideClients_saf_bi'] as $client_id)
						{
							if ($this->request->data['ClientOverride']['left_saf_2'] ==1 )
							{
								if ($this->request->data['ImageDeleteLeft2']['program_background_images_left_2']==1) {
									$delete_images = array();
									foreach ($this->request->data['ImageDeleteLeft2'] as $key => $value) {
										if ($value == '1') {
											$this->File->deleteByPrefix('client_background_images_left_2/'.$client_id);
											$sql = "UPDATE client_banners SET client_background_images_left_extension='' WHERE dashboard_id=2 and client_id=".$client_id;
											$this->ClientBanner->query($sql);
										}
									}
								}
								else
								{
									$clientbanner = $this->Client->ClientBanner->find('first',
											array('conditions'=>array('ClientBanner.client_id' => $client_id, 'ClientBanner.dashboard_id' => 2),
													'fields' => array('ClientBanner.id, ClientBanner.client_background_images_left_extension')));
									$bannerid = empty($clientbanner)?0:$clientbanner['ClientBanner']['id'];

									$backgrnd_image_left_extension=pathinfo($this->request->data['Image']['program_background_images_left_2']['name'], PATHINFO_EXTENSION);
									$this->request->data['ClientBanner']['id'] = $bannerid;
									$this->request->data['ClientBanner']['client_id'] = $client_id;
									$this->request->data['ClientBanner']['dashboard_id'] = 2;
									$this->request->data['ClientBanner']['client_background_images_left_extension'] = $backgrnd_image_left_extension;

									$this->Client->ClientBanner->save($this->request->data);
									if ($backgrnd_image_left_extension != '')
									{
										//copy($this->request->data['Image']['program_background_images_left_2']['tmp_name'], CLIENT_BACKGROUND_IMAGE_LEFT_2_PATH. $client_id. '.'.$backgrnd_image_left_extension);
										copy(PROGRAM_BACKGROUND_IMAGE_LEFT_2_PATH. $program_id. '.'.$backgrnd_image_left_extension, CLIENT_BACKGROUND_IMAGE_LEFT_2_PATH. $client_id. '.'.$backgrnd_image_left_extension);
									}
								}
							}

							if($this->request->data['ClientOverride']['right_saf_2'] ==1)
							{
								if ($this->request->data['ImageDeleteRight2']['program_background_images_right_2'] == 1) {
									$delete_images = array();
									foreach ($this->request->data['ImageDeleteRight2'] as $key => $value) {
										if ($value == '1') {
											$this->File->deleteByPrefix('client_background_images_right_2/'.$client_id);
											$sql = "UPDATE client_banners SET client_background_images_right_extension='' WHERE dashboard_id=2 and client_id=".$client_id;
											$this->ClientBanner->query($sql);
										}
									}
								}
								else
								{
									$clientbanner = $this->Client->ClientBanner->find('first',
											array('conditions'=>array('ClientBanner.client_id' => $client_id, 'ClientBanner.dashboard_id' => 2),
													'fields' => array('ClientBanner.id, ClientBanner.client_background_images_right_extension')));
									$bannerid = empty($clientbanner)?0:$clientbanner['ClientBanner']['id'];

									$backgrnd_image_right_extension=pathinfo($this->request->data['Image']['program_background_images_right_2']['name'], PATHINFO_EXTENSION);
									$this->request->data['ClientBanner']['id'] = $bannerid;
									$this->request->data['ClientBanner']['client_id'] = $client_id;
									$this->request->data['ClientBanner']['dashboard_id'] = 2;
									$this->request->data['ClientBanner']['client_background_images_right_extension'] = $backgrnd_image_right_extension;

									$this->Client->ClientBanner->save($this->request->data);
									if ($backgrnd_image_right_extension != '')
									{
										//copy($this->request->data['Image']['program_background_images_right_2']['tmp_name'], CLIENT_BACKGROUND_IMAGE_RIGHT_2_PATH. $client_id. '.'.$backgrnd_image_right_extension);
										copy(PROGRAM_BACKGROUND_IMAGE_RIGHT_2_PATH. $program_id. '.'.$backgrnd_image_right_extension, CLIENT_BACKGROUND_IMAGE_RIGHT_2_PATH. $client_id. '.'.$backgrnd_image_right_extension);
									}
								}

							}

							if( $this->request->data['ClientOverride']['left_link_saf_2']==1 )
							{
								$clientbanner = $this->Client->ClientBanner->find('first',
										array('conditions'=>array('ClientBanner.client_id' => $client_id, 'ClientBanner.dashboard_id' => 2),
												'fields' => array('ClientBanner.id, ClientBanner.left_link')));
								$bannerid = empty($clientbanner)?0:$clientbanner['ClientBanner']['id'];

								$this->request->data['ClientBanner']['id'] = $bannerid;
								$this->request->data['ClientBanner']['client_id'] = $client_id;
								$this->request->data['ClientBanner']['dashboard_id'] = 2;
								$this->request->data['ClientBanner']['left_link'] = $this->request->data['Program']['left_link_2'];
								$this->request->data['ClientBanner']['modified'] = date('Y-m-d h:i:s');
								$this->Client->ClientBanner->save($this->request->data);
							}

							if($this->request->data['ClientOverride']['right_link_saf_2']==1)
							{
								$clientbanner = $this->Client->ClientBanner->find('first',
										array('conditions'=>array('ClientBanner.client_id' => $client_id, 'ClientBanner.dashboard_id' => 2),
												'fields' => array('ClientBanner.id, ClientBanner.right_link')));
								$bannerid = empty($clientbanner)?0:$clientbanner['ClientBanner']['id'];
								$this->request->data['ClientBanner']['id'] = $bannerid;
								$this->request->data['ClientBanner']['client_id'] = $client_id;
								$this->request->data['ClientBanner']['dashboard_id'] = 2;
								$this->request->data['ClientBanner']['right_link'] = $this->request->data['Program']['right_link_2'];
								$this->request->data['ClientBanner']['modified'] = date('Y-m-d h:i:s');
								$this->Client->ClientBanner->save($this->request->data);
							}
						}
					 }

					 //saving background images and links for clients of points dashboard.
					 if ($this->request->data['ClientOverride']['override_points_bi'] == '1' && isset($this->request->data['ClientOverideClients_points_bi']))
					 {
						$this->loadModel("Client");
						$this->loadModel("ClientBanner");
						$this->loadModel("Dashboard");
						foreach ($this->request->data['ClientOverideClients_points_bi'] as $client_id)
						{
							if ($this->request->data['ClientOverride']['left_points_3'] ==1 )
							{
								if ($this->request->data['ImageDeleteLeft3']['program_background_images_left_3']==1) {
									$delete_images = array();
									foreach ($this->request->data['ImageDeleteLeft3'] as $key => $value) {
										if ($value == '1') {
											$this->File->deleteByPrefix('client_background_images_left_3/'.$client_id);
											$sql = "UPDATE client_banners SET client_background_images_left_extension='' WHERE dashboard_id=3 and client_id=".$client_id;
											$this->ClientBanner->query($sql);
										}
									}
								}
								else
								{
									$clientbanner = $this->Client->ClientBanner->find('first',
											array('conditions'=>array('ClientBanner.client_id' => $client_id, 'ClientBanner.dashboard_id' => 3),
													'fields' => array('ClientBanner.id, ClientBanner.client_background_images_left_extension')));
									$bannerid = empty($clientbanner)?0:$clientbanner['ClientBanner']['id'];

									$backgrnd_image_left_extension=pathinfo($this->request->data['Image']['program_background_images_left_3']['name'], PATHINFO_EXTENSION);
									$this->request->data['ClientBanner']['id'] = $bannerid;
									$this->request->data['ClientBanner']['client_id'] = $client_id;
									$this->request->data['ClientBanner']['dashboard_id'] = 3;
									$this->request->data['ClientBanner']['client_background_images_left_extension'] = $backgrnd_image_left_extension;

									$this->Client->ClientBanner->save($this->request->data);
									if ($backgrnd_image_left_extension != '')
									{
										//copy($this->request->data['Image']['program_background_images_left_3']['tmp_name'], CLIENT_BACKGROUND_IMAGE_LEFT_3_PATH. $client_id. '.'.$backgrnd_image_left_extension);
										copy(PROGRAM_BACKGROUND_IMAGE_LEFT_3_PATH. $program_id. '.'.$backgrnd_image_left_extension, CLIENT_BACKGROUND_IMAGE_LEFT_3_PATH. $client_id. '.'.$backgrnd_image_left_extension);
									}
								}
							}

							if($this->request->data['ClientOverride']['right_points_3'] ==1)
							{
								if ($this->request->data['ImageDeleteRight3']['program_background_images_right_3'] == 1) {;
									$delete_images = array();
									foreach ($this->request->data['ImageDeleteRight3'] as $key => $value) {
										if ($value == '1') {
											$this->File->deleteByPrefix('client_background_images_right_3/'.$client_id);
											$sql = "UPDATE client_banners SET client_background_images_right_extension='' WHERE dashboard_id=3 and client_id=".$client_id;
											$this->ClientBanner->query($sql);
										}
									}
								}
								else
								{
									$clientbanner = $this->Client->ClientBanner->find('first',
											array('conditions'=>array('ClientBanner.client_id' => $client_id, 'ClientBanner.dashboard_id' => 3),
													'fields' => array('ClientBanner.id, ClientBanner.client_background_images_right_extension')));
									$bannerid = empty($clientbanner)?0:$clientbanner['ClientBanner']['id'];

									$backgrnd_image_right_extension=pathinfo($this->request->data['Image']['program_background_images_right_3']['name'], PATHINFO_EXTENSION);
									$this->request->data['ClientBanner']['id'] = $bannerid;
									$this->request->data['ClientBanner']['client_id'] = $client_id;
									$this->request->data['ClientBanner']['dashboard_id'] = 3;
									$this->request->data['ClientBanner']['client_background_images_right_extension'] = $backgrnd_image_right_extension;

									$this->Client->ClientBanner->save($this->request->data);
									if ($backgrnd_image_right_extension != '')
									{
										copy(PROGRAM_BACKGROUND_IMAGE_RIGHT_3_PATH. $program_id. '.'.$backgrnd_image_right_extension, CLIENT_BACKGROUND_IMAGE_RIGHT_3_PATH. $client_id. '.'.$backgrnd_image_right_extension);
									}
								}

							}

							if( $this->request->data['ClientOverride']['left_link_points_3']==1 )
							{
								$clientbanner = $this->Client->ClientBanner->find('first',
										array('conditions'=>array('ClientBanner.client_id' => $client_id, 'ClientBanner.dashboard_id' => 3),
												'fields' => array('ClientBanner.id, ClientBanner.left_link')));
								$bannerid = empty($clientbanner)?0:$clientbanner['ClientBanner']['id'];

								$this->request->data['ClientBanner']['id'] = $bannerid;
								$this->request->data['ClientBanner']['client_id'] = $client_id;
								$this->request->data['ClientBanner']['dashboard_id'] = 3;
								$this->request->data['ClientBanner']['left_link'] = $this->request->data['Program']['left_link_3'];
								$this->request->data['ClientBanner']['modified'] = date('Y-m-d h:i:s');
								$this->Client->ClientBanner->save($this->request->data);
							}

							if($this->request->data['ClientOverride']['right_link_points_3']==1)
							{
								$clientbanner = $this->Client->ClientBanner->find('first',
										array('conditions'=>array('ClientBanner.client_id' => $client_id, 'ClientBanner.dashboard_id' => 3),
												'fields' => array('ClientBanner.id, ClientBanner.right_link')));
								$bannerid = empty($clientbanner)?0:$clientbanner['ClientBanner']['id'];
								$this->request->data['ClientBanner']['id'] = $bannerid;
								$this->request->data['ClientBanner']['client_id'] = $client_id;
								$this->request->data['ClientBanner']['dashboard_id'] = 3;
								$this->request->data['ClientBanner']['right_link'] = $this->request->data['Program']['right_link_3'];
								$this->request->data['ClientBanner']['modified'] = date('Y-m-d h:i:s');
								$this->Client->ClientBanner->save($this->request->data);
							}
						}
					 }

				//	log the change
				$log = array(
					'id' => null,
					'foreign_id' => $this->{Inflector::singularize($this->name)}->id,
					'foreign_model' => Inflector::singularize($this->name),
					'user_id' => $this->Session->read('user.User.id'),
					'old_data' => $old_data,
					'new_data' => $this->request->data
				);
				if (empty($old_data)) {
					$log['description'] = Inflector::singularize($this->name). ' created';
				} else {
					$log['description'] = Inflector::singularize($this->name). ' modified';
				}

				if (isset($this->request->data['Log']['notes'])) {
					$log['notes'] = $this->request->data['Log']['notes'];
				}

				//	add the related changes
				foreach ($this->Program->getAssociated('hasAndBelongsToMany') as $model)  {
					switch ($model) {		//	fields to add to the comments display
						case 'Module':
							$display = 'title';
							break;
						case 'Category' :
							$display = 'description';
						default:
							$display = 'name';
					}

					$tmp_old_related = array();
					//	deleted
					if (isset($old_data[$model]) && isset($this->request->data[$model][$model])) {
						foreach ($old_data[$model] as $model_data) {
							if (!in_array($model_data['id'], $this->request->data[$model][$model])) {
								$log['old_data']['Programs'. $model][Inflector::humanize(Inflector::underscore($model)). ': '. $model_data[$display]] = '1';
								$log['new_data']['Programs'. $model][Inflector::humanize(Inflector::underscore($model)). ': '. $model_data[$display]] = '0';
							}
							$tmp_old_related[] = $model_data['id'];
						}
						//	added
						foreach ($this->request->data[$model][$model] as $model_id) {
							if ($model_id != '' && !in_array($model_id, $tmp_old_related)) {
								$this->Program->{$model}->recursive = -1;
								$model_data = $this->Program->{$model}->findById($model_id);
								$log['old_data']['Programs'. $model][Inflector::humanize(Inflector::underscore($model)). ': '. $model_data[$model][$display]] = '0';
								$log['new_data']['Programs'. $model][Inflector::humanize(Inflector::underscore($model)). ': '. $model_data[$model][$display]] = '1';
							}
						}
					}
				}

				$this->save_log($log);

				//inserting the special products of programs into programs_special_products where dashboard_id=2
				if (!empty($this->request->data['SpecialProduct']['SpecialProduct_2']))
				{
					foreach ($this->request->data['SpecialProduct']['SpecialProduct_2'] as $key => $value)
					 {
						if(!empty($value))
						{
							$specialproductid = $this->Program->ProgramsSpecialProduct->find('first', array('conditions' => array('ProgramsSpecialProduct.program_id' => $value,'ProgramsSpecialProduct.dashboard_id' => 2), 'fields' => array('ProgramsSpecialProduct.id')));
							$specialid = empty($specialproductid)?0:$specialproductid['ProgramsSpecialProduct']['id'];

							$this->request->data['ProgramsSpecialProduct']['id']=0;
							$this->request->data['ProgramsSpecialProduct']['program_id']=$program_id;
							$this->request->data['ProgramsSpecialProduct']['product_id']=$value;
							$this->request->data['ProgramsSpecialProduct']['dashboard_id']=2;
							$this->Program->ProgramsSpecialProduct->save($this->request->data);
						}
					 }
				}
				//inserting the special products of programs into programs_special_products where dashboard_id=3
				if (!empty($this->request->data['SpecialProduct']['SpecialProduct_3']))
				{
					foreach ($this->request->data['SpecialProduct']['SpecialProduct_3'] as $key => $value)
					 {
						if(!empty($value))
						{
							$specialproductid = $this->Program->ProgramsSpecialProduct->find('first', array('conditions' => array('ProgramsSpecialProduct.program_id' => $value,'ProgramsSpecialProduct.dashboard_id' => 2), 'fields' => array('ProgramsSpecialProduct.id')));
							$specialid = empty($specialproductid)?0:$specialproductid['ProgramsSpecialProduct']['id'];

							$this->request->data['ProgramsSpecialProduct']['id']=0;
							$this->request->data['ProgramsSpecialProduct']['program_id']=$program_id;
							$this->request->data['ProgramsSpecialProduct']['product_id']=$value;
							$this->request->data['ProgramsSpecialProduct']['dashboard_id']=3;
							$this->Program->ProgramsSpecialProduct->save($this->request->data);
						}
					 }
				}


				$this->Session->setFlash('The Program has been saved');
				$this->redirect_session(array('action'=>'index'), null, true);
			} else {
				$this->Session->setFlash('Please correct the errors below.');
			}
			}

		} else {
			$this->Program->recursive = 1;

			$this->Program->unbindModel(array('hasMany' => array('ProgramPage','Client'), 'hasAndBelongsToMany' => array('Product')));
			$this->request->data = $this->Program->read(null, $id);

					if($id==null){
						//come up a program list
						$program_options = $this->Program->find("list");
						$this->set("program_options", $program_options);
					}

			$this->_setRedirect();
		}

		$this->Program->Category->recursive = -1;
		$categories = $this->Program->Category->find('list', array('order'=>'description, name', 'fields'=>'id,description'));

		$this->Program->Style->recursive = -1;
		$styles = $this->Program->Style->find("list");
		$this->Program->Voucher->recursive = -1;
		$vouchers = $this->Program->Voucher->find("list");
		$this->Program->Domain->recursive = -1;
		$domains = $this->Program->Domain->find("list");
		$this->Program->Module->recursive = -1;
		$modules = $this->Program->Module->find("list");

		//here we are passing dashboard_id's into _pages method like _pages(,,,1,0),_pages(,,,2,0),_pages(,,,3,0) to set client pages with respective dashbaords
		//here last number 0(zero) is indicating the public pages and 1 indicating private pages
		$this->set('program_pages', $this->_pages($this->request->data['Program']['id'], null, true, 1, 0));
		$this->set('program_public_pages', $this->_pages($this->request->data['Program']['id'], null, true, 1, 1));
		$this->set('program_pages_sendafriend', $this->_pages($this->request->data['Program']['id'], null, true, 2, 0));
		$this->set('program_public_pages_sendafriend', $this->_pages($this->request->data['Program']['id'], null, true, 2, 1));
		$this->set('program_pages_points', $this->_pages($this->request->data['Program']['id'], null, true, 3, 0));
		$this->set('program_public_pages_points', $this->_pages($this->request->data['Program']['id'], null, true, 3, 1));

		//	clients list
		$this->Program->Client->recursive = -1;
		$clients = $this->paginate('Client',array('Client.program_id' => $id));
		$this->set('clients', $clients);

		// all clients for this program
		$this->Program->Client->recursive = -1;
		$all_clients = $this->Program->Client->find('list', 	array('conditions' => array('Client.program_id' => $id)));
		$this->set('all_clients', $all_clients);

		$countries = $this->Program->Country->find('list');
		$defaultcountry = $this->Program->Country->find('list', array('fields' => 'Country.name,Country.name'));

		$this->set(compact('categories','styles','vouchers','domains','modules','countries','defaultcountry'));

		$this->loadModel('CurrencyCode');
		$codes = $this->CurrencyCode->find('list');
		$this->set('codes',$codes);

		$this->loadModel('PaypalDetail');
		$business_name = $this->PaypalDetail->find('first',array('conditions'=>array('PaypalDetail.client_id'=>$this->request->data['Program']['id'])));
		$this->set('business_name',$business_name['PaypalDetail']['paypal_business_name']);
		$this->set('currency_code',$business_name['PaypalDetail']['paypal_currency_code']);
	}

	function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash('Invalid id for Program');
			$this->redirect(array('action'=>'index'), null, true);
		}
		if ($this->Program->delete($id)) {
			$this->Session->setFlash('Program #'.$id.' deleted');
			$this->redirect(array('action'=>'index'), null, true);
		}
	}

	/**
	 * return a list of products for a given program and category
	 */
	function ajax_category_products($id, $category_id) {

		$this->layout = 'blank';

		$sql = "SELECT Product.* FROM products AS Product
					LEFT JOIN categories_products AS CategoriesProduct ON Product.id = CategoriesProduct.product_id
					LEFT JOIN programs_products AS ProgramsProduct ON Product.id = ProgramsProduct.product_id
					WHERE CategoriesProduct.category_id = '". $category_id. "'
					AND ProgramsProduct.program_id = '". $id. "'";
		$products = $this->Program->query($sql);
		$this->set('products', $products);
	}

	/**
	 * Return a html list of products for an ajax call
	 *
	 * @param	int	$id	program id
	 */
	function admin_ajax_products($id) {

		$this->layout = 'ajax';

		$name = 'Product';
		$conditions = array();

		if (isset($this->params['url']['data'])) {
			$this->Session->write($this->name.'.search.'.$name, $this->params['url']['data']);
		}

		if ($this->Session->check($this->name.'.search.'.$name)) {
			$this->request->data = $this->Session->read($this->name.'.search.'.$name);
			if  (!empty($this->request->data[$name]['search'])) {
				$conditions[] = "
					(`". $name. "`.`name` LIKE  '%". $this->request->data[$name]['search']. "%'
					OR `". $name. "`.`id` =  '". $this->request->data[$name]['search']. "')
				";
			}
		}
		$conditions[] = "`". 'Program'. "`.`id` = '". $id. "'";

		$this->Program->$name->recursive = 0;

		$this->set('programs_product', $this->paginate('ProgramsProduct', $conditions));

		$this->set('id', $id);

	}

	/**
	 * Return a html list of categories for an ajax call
	 */
	function admin_ajax_categories($id) {

		$this->layout = 'ajax';

		$name = 'Category';
		$conditions = array();

		if (isset($this->params['url']['data'])) {
			$this->Session->write($this->name.'.search.'.$name, $this->params['url']['data']);
		}

		if ($this->Session->check($this->name.'.search.'.$name)) {
			$this->request->data = $this->Session->read($this->name.'.search.'.$name);
			if  (!empty($this->request->data[$name]['search'])) {
				$conditions[] = "
					(`". $name. "`.`name` LIKE  '%". $this->request->data[$name]['search']. "%'
					OR `". $name. "`.`id` =  '". $this->request->data[$name]['search']. "')
				";
			}
		}
		$conditions[] = "`". 'Program'. "`.`id` = '". $id. "'";

		$this->Program->$name->recursive = 0;

		$this->set('programs_category', $this->paginate('ProgramsCategory', $conditions));

		$this->set('id', $id);

	}

	/**
	 * Return a html list of products for an ajax call
	 *
	 * @param	int	$id	program id

	 */
	function admin_ajax_category_products($id, $category_id) {

		$this->layout = 'ajax';

		$this->Program->Product->bindModel(array('hasOne' => array(
			'ProgramsProduct' => array(
				'conditions' => 'ProgramsProduct.program_id = '. $id,
			),
			'CategoriesProduct' => array(
				'conditions' => 'CategoriesProduct.category_id = '. $category_id,
			)
		)), false	);

		$conditions[] = 'ProgramsProduct.id IS NOT NULL';
		$conditions[] = 'CategoriesProduct.id IS NOT NULL';
		$this->Program->Product->recursive = 0;
		$this->set('products', $this->paginate('Product', $conditions));

		$this->set('id', $id);
		$this->Program->Category->recursive = 0;
		$this->set('category', $this->Program->Category->findById($category_id));

	}

	/**
	 * Return a html list of clients for an ajax call
	 */
	function admin_ajax_clients($id) {

		$this->layout = 'ajax';

		$name = 'Client';
		$conditions = array();

		if (isset($this->params['url']['data'])) {
			$this->Session->write($this->name.'.search.'.$name, $this->params['url']['data']);
		}

		if ($this->Session->check($this->name.'.search.'.$name)) {
			$this->request->data = $this->Session->read($this->name.'.search.'.$name);
			if  (!empty($this->request->data[$name]['search'])) {
				$conditions[] = "
					(`". $name. "`.`name` LIKE  '%". $this->request->data[$name]['search']. "%'
					OR `". $name. "`.`id` =  '". $this->request->data[$name]['search']. "')
				";
			}
		}
		$conditions[] = "`". $name. "`.`program_id` = '". $id. "'";
		$this->Program->$name->recursive = 0;

		$this->set(Inflector::pluralize(Inflector::underscore($name))	, $this->paginate($name, $conditions));

		$this->set('id', $id);
	}

	/**
	 * Return an xml list of products
	 *
	 * @param	int	$id	client id
	 * @param	int	$limit	number or rows to return
	*/
	function admin_xml_products($id, $limit = 10) {

		$this->layout = 'ajax';

		$conditions = "ProgramsProduct.program_id = '". $id. "'";
		if (isset($this->params['url']['s'])) {
			$conditions .= "AND Product.id LIKE '". $this->params['url']['s']. "%'";
		}

		$data = $this->Program->ProgramsProduct->find('all', array('conditions'=>$conditions, 'fields'=>'Product.id,Product.name', 'order'=>'Product.name', 'limit'=>$limit));

		$this->set('products', $data);

	}

}
?>