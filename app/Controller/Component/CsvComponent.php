<?php 

class CSVComponent extends Component {

	var $lf = "\n";
	
	/*
 	 *  Parse a CSV data to a associated 2D array
	 * @param	str $data data containing csv
	 * @return array array of csv data
	 */
	function file_to_array($file,$head=true,$delim=",",$len=2000) {
		$return = false;
		$handle = fopen($file, "r");
		if ($head) {
			$header = fgetcsv($handle, $len, $delim);
		}
		
		while (($data = fgetcsv($handle, $len, $delim)) !== FALSE) {
			if ($head AND isset($header)) {
				foreach ($header as $key=>$heading) {
					$row[trim($heading)]=(isset($data[$key])) ? trim($data[$key]) : '';
				}
				$return[]=$row;
			} else {
				$return[]=$data;
			}
		}
		fclose($handle);
		
		return $return;
	}
}