<?php
/*
 * SwiftMailer Component Original By othman ouahbi.
 * Ported for Swift Mailer 3 By Wouter Verweirder
 * comments, bug reports are welcome wouter AT aboutme DOT be
 * @author Wouter Verweirder
 * @author othman ouahbi aka CraZyLeGs
 * @version 0.2
 * @license MIT
 */

class SwiftMailerComponent extends Object {
	var $controller = false;
	var $mailer = null;
	var $connection = MAIL_CONNECTION; // sendmail, native

	var $smtp_host = SMTP_SERVER; // null auto detect
	//var $smtp_host = null; // null auto detect
	var $smtp_port = false; // false to let the mailer choose. default 25, 465 for ssl.
	var $smtp_type = 'open'; // open, ssl, tls

	var $username = SMTP_USER;
	var $password = SMTP_PASS;

	var $message = null; //reference to a Swift_Message object
	var $subject = '';
	var $recipients = null;
	var $from = null;

	var $layout = 'swift_email';

	var $email_views_dir = 'swift_emails';

	var $sendmail_cmd = false; // false: SWIFT_AUTO_DETECT, 'default': '/usr/sbin/sendmail -bs' etc..

	function startup(& $controller) {
		$this->controller = & $controller;
	}

	function connect() {
		switch ($this->connection) {
			case 'smtp' :
				$this->connect_smtp();
				break;
			case 'sendmail' :
				$this->connect_sendmail();
				break;
			case 'native' :
			default :
				$this->connect_native();
				break;
		}
		return true;
	}

	function connect_native() {
		App::import('Vendor', 'Swift', array('file'=>'Swift.php'));
		App::import('Vendor', 'NativeMail', array('file'=>'Swift'.DS.'Connection'.DS.'NativeMail.php'));

		$this->mailer = new Swift(new Swift_Connection_NativeMail());
	}

	function connect_sendmail() {
		App::import('Vendor', 'Swift', array('file'=>'Swift.php'));
		App::import('Vendor', 'Sendmail', array('file'=>'Swift'.DS.'Connection'.DS.'Sendmail.php'));

		if ($this->sendmail_cmd == false) {
			$this->sendmail_cmd = Swift_Connection_Sendmail::AUTO_DETECT;
		}
		elseif ($this->sendmail_cmd == 'default') {
			$this->sendmail_cmd = '/usr/sbin/sendmail -bs';
		}

		$this->mailer = new Swift(new Swift_Connection_Sendmail($this->sendmail_cmd));
	}

	function connect_smtp() {
		App::import('Vendor', 'Swift', array('file'=>'Swift.php'));
		App::import('Vendor', 'SMTP', array('file'=>'Swift'.DS.'Connection'.DS.'SMTP.php'));

		$this->log =& Swift_LogContainer::getLog();
		$this->log->setLogLevel(2);

		// SWIFT_AUTO_DETECT
		if (is_null($this->smtp_host)) {
			$this->smtp_host = Swift_Connection_SMTP::AUTO_DETECT;
		}

		if (is_null($this->smtp_port)) {
			$this->smtp_port = Swift_Connection_SMTP::AUTO_DETECT;
		}

		$ssl_types = array('open'=>Swift_Connection_SMTP::ENC_OFF,'ssl'=>Swift_Connection_SMTP::ENC_SSL,'tls'=>Swift_Connection_SMTP::ENC_TLS);

		if(in_array($this->smtp_type, array ('open','ssl','tls')))
		{
			$ssl_type = $ssl_types[$this->smtp_type];
		}else
		{
			$ssl_type = $ssl_types['open'];
		}

		$this->mailer = new Swift(new Swift_Connection_SMTP($this->smtp_host, $this->smtp_port, $ssl_type));
	}

	function auth() {
		//disabled for now
		return false;
	}

	function errors() {

		if (!empty($this->log)) {
		 	return $this->log->dump(true);
		} else {
			return null;
		}
	}

	function transactions() {
		//disabled for now
		//return $this->mailer->transactions;
		return null;
	}

	function close() {
		//disabled for now
		//$this->mailer->close();
	}

	/*
	 * description:
	 * Renders a body view located in the emails dir.
	 * if html, wraps it with a layout and embeds images that have the embed="swift" attribute
	 * strip tags if plain.
	 */
	function viewBody($name, $type = 'both', $return = false) {
		switch ($type) {
			case 'both' :
				$plain = true;
				$html = true;
				break;
			case 'html' :
				$html = true;
				break;
			case 'plain' :
				$plain = true;
				break;
			default :
				return;
				break;
		}

		$this->message =& new Swift_Message($this->subject);

		if (isset ($plain)) {
			$view = APP.'View'.DS.$this->email_views_dir.DS.$name.'.thtml';
			$old_layout = $this->controller->layout;
			$this->controller->layout = '';
			ob_start();
			$plain_msg = $this->controller->render($view);
			ob_end_clean();
			$this->controller->layout = $old_layout;
		}

		if (isset ($html)) {
			$name .= "_html";
			$view = APP.'View'.DS.$this->email_views_dir.DS.$name.'.thtml';
			$old_layout = $this->controller->layout;
			ob_start();
			$html_msg = $this->controller->render($view, $this->layout);
			ob_end_clean();
			$html_msg = $this->replaceIMG($html_msg);
			$this->controller->layout = $old_layout;
		}

		switch ($type) {
			case 'both' :
				if ($return) {
					return array (
						$plain_msg,
						$html_msg
					);
				}
				$this->message->attach(new Swift_Message_Part($html_msg, "text/html"));
				$this->message->attach(new Swift_Message_Part($plain_msg, "text/plain"));
				break;
			case 'html' :
				if ($return) {
					return $html_msg;
				}
				$this->message->attach(new Swift_Message_Part($html_msg, "text/html"));
				break;
			case 'plain' :
				if ($return) {
					return $plain_msg;
				}
				$this->message->attach(new Swift_Message_Part($plain_msg, "text/plain"));
				break;
		}
	}

	function replaceIMG($msg) {
		$matches = array ();
		$files = array ();
		if (preg_match_all('#<img.*src=\"(.*?)\".*?\/>#', $msg, $matches)) {
			for ($i = 0; $i < count($matches[0]); $i++) {
				$pos = strpos($matches[0][$i], 'embed="swift"');
				if ($pos !== false) {
					$file = substr($matches[1][$i], strrpos($matches[1][$i], '/') + 1);
					if (array_key_exists($file, $files)) {
						$replace = $files[$file];
					} else {
						//$replace = $this->mailer->addImage(WWW_ROOT . 'img' . DS . $file);
						$replace = $message->attach(new Swift_Message_Image(new Swift_File(WWW_ROOT . 'img' . DS . $file)));
						$files[$file] = $replace;
					}

					$msg = str_replace($matches[1][$i], $replace, $msg);
				}
			}
		}
		return $msg;
	}

	/*
	 * description:
	 * Wraps the body with a layout, strips tags if not html
	 */
	function wrapBody($msg, $type = 'plain', $return = false) {
		$view = APP.DS.'View'.DS.$this->email_views_dir.DS.'default.thtml';

		$this->controller->set('swiftMailer_data', $msg);

		ob_start();
		$msg = $this->controller->render($view, $this->layout);
		ob_end_clean();

		if ($type != 'html') {
			$msg = strip_tags($msg);
		}

		if ($return) {
			return $msg;
		}

		$this->message =& new Swift_Message($this->subject);
		if($type == 'html'){
			$this->message->attach(new Swift_Message_Part($msg, "text/plain"));
		}else{
			$this->message->attach(new Swift_Message_Part($msg, "text/html"));
		}
	}

	// original idea Tommy0
	function addTo($type, $address, $name = false) {
		if(empty($this->recipients)){
			$this->recipients =& new Swift_RecipientList();
		}
		switch($type){
			case 'to':
				$this->recipients->addTo($address, $name);
				break;
			case 'from':
				$this->from =& new Swift_Address($address, $name);
				break;
			case 'cc':
				$this->recipients->addCc($address, $name);
				break;
			case 'bcc':
				$this->recipients->addBcc($address, $name);
				break;
		}
	}

	function resetTo() {
		if(!empty($this->recipients)){
			$this->recipients =& new Swift_RecipientList();
		}
	}

	// original idea Tommy0
	function send($subject) {
		//subject updaten
		$this->message->setSubject($subject);
		//
		if (!empty ($this->username) && !$this->auth()) {
			return false;
		}

		if ($this->mailer->send($this->message, $this->recipients, $this->from)) {
			$this->close();
			return true;
		}
		return false;
	}

	function sendWrap($subject, $body, $type = 'plain') {
		$this->wrapBody($body, $type);

		return $this->send($subject);
	}

	function sendView($subject, $view, $type = 'plain') {
		$this->viewBody($view, $type);

		return $this->send($subject);
	}
	function sendView1($subject, $view, $type = 'plain') {
		$this->viewBody($view, $type);
		return $this->send1($subject);
	}

	function send1($subject) {
		//subject updaten
		$this->message->setSubject($subject);
		//
		if (!empty ($this->username) && !$this->auth()) {
			return false;
		}

		try{
			echo $this->mailer->send($this->message, $this->recipients, $this->from);
			return true;
		} catch (Exception $e) {
    		echo 'Caught exception: ',  $e->getMessage(), "\n";
		}
		
		return false;
	}
}
?>