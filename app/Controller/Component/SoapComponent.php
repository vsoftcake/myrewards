<?php
/**
 * Class SoapComponent
 *
 * Generate WSDL and handle SOAP calls
 */
class SoapComponent extends Component
{
    var $params = array();
    var $components = array('RequestHandler');
    function initialize($controller)
    {
        $this->params = $controller->params;
    }
    /**
     * Get WSDL for specified model.
     *
     * @param string $modelClass : model name in camel case
     * @param string $serviceMethod : method of the controller that will handle SOAP calls
     */
    function getWSDL($modelId, $serviceMethod = 'call')
    {
        //header('Content-Type: text/xml');
        $this->RequestHandler->respondAs('xml');
		$modelClass = $this->__getModelClass($modelId);
        $expireTime = '+1 year';
        $cachePath = CACHE. $modelClass . '.wsdl';
        // Check cache if exist
        //$wsdl = cache($cachePath, null, $expireTime);


        // Generate WSDL if not cached
        return file_get_contents($cachePath);
    }
    /**
     * Handle SOAP service call
     *
     * @param string $modelId : underscore notation of the called model
     *                          without _service ending
     * @param string $wsdlMethod : method of the controller that will generate the WSDL
     */
    function handle($modelId, $wsdlMethod = 'wsdl')
    {
    	App::import("Model", "UserService");
		//header('Content-Type: text/xml');
		$this->RequestHandler->respondAs('xml');
		$modelClass = $this->__getModelClass($modelId);
        $wsdlCacheFile = CACHE . $modelClass . '.wsdl';
        $srv = new SoapServer($wsdlCacheFile);
        $srv->setClass("UserService");
        $srv->handle();
    }
    /**
     * Get model class for specified model id
     *
     * @access private
     * @return string : the model id
     */
    function __getModelClass($modelId)
    {
        return (Inflector::camelize($modelId) . 'Service');
    }
    /**
     * Get model id for specified model class
     *
     * @access private
     * @return string : the model id
     */
    function __getModelId($modelClass)
    {
        return Inflector::underscore(substr($class, 0, -7));
    }
    /**
     * Get model file for specified model id
     *
     * @access private
     * @return string : the filename
     */
    function __getModelFile($modelId)
    {
        $modelDir = dirname(dirname(dirname(__FILE__))) . DS . 'models';
        return $modelDir . DS . $modelId . '_service.php';
    }
}
?>