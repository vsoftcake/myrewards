<?php 

class fileComponent extends Component {

	/**
	 * save image files to the server
	 *
	 * @param	array	$images array of image information on the server
	 * @param	str		$folder	folder name in files path to save images in 
	 * @param	str		$prefix	prefix to assign to all images
	*/
	function saveImages($images, $folder = null, $prefix = null) {
	 
	 	if ($folder != '') {
			$folder .= '/';
		}
			
		foreach ($images as $key => $file) {
			
			if ($file['name'] != '') {
				
				$file['filename'] = md5(uniqid(rand(), true)). '.'. substr($file['name'], strrpos($file['name'], '.') + 1);
				$file_folder = FILES_PATH. $folder. $key. '/';
				
				if (!file_exists($file_folder)) {
					mkdir($file_folder, 0777, true);
					chmod($file_folder, 0777);
				}
				
				//	delete the old image 
				$this->deleteByPrefix($folder. $key. '/'. $prefix);
				
				move_uploaded_file($file['tmp_name'], $file_folder. $prefix. $file['filename']);
				chmod($file_folder. $prefix. $file['filename'], 0777);
				$images[$key]['filename'] = $prefix. $file['filename'];
			} else {
				unset($images[$key]);
			}
		}
		
		return $images;
	}
	
	/**
	 * delete images by prefix
	 *
	 * @param	array	$prefix of the images to delete
	 */
	function deleteByPrefix($prefix = null) {
		
		if (empty($prefix)) {
			return;
		}
		
		foreach (glob(FILES_PATH. $prefix. '*') as $filename) {
			unlink($filename);
		}
	}
	
}