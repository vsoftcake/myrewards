<?php 

class MailQueueComponent extends Object {
	
	var $controller = false;
	var $mailer = null;
	var $message = false;
	var $recipients = false;
	var $log;
	
	function startup(& $controller) {
		$this->controller = & $controller;
	}

	function connect() {
		App::import('Vendor', 'Swift', array('file'=>'Swift.php'));
		App::import('Vendor', 'SMTP', array('file'=>'Swift'.DS.'Connection'.DS.'SMTP.php'));
		
		$this->mailer = new Swift(new Swift_Connection_SMTP(SMTP_SERVER));
		
		$this->log = Swift_LogContainer::getLog();
		$this->log->setLogLevel(2);
	}

	function setMessage($subject, $body) {
		$this->message = new Swift_Message($subject, $body, "text/html");
	}

	function addTo($email, $name) {
		if (!$this->recipients) {
			$this->recipients = new Swift_RecipientList();
		}
		$this->recipients->addTo($email, $name);
	}
	
	function flushTo() {
		$this->recipients = new Swift_RecipientList();
	}
	
	function batchSend($email, $name) {
		
		if ($this->message && $this->recipients) {
			$this->mailer->batchSend($this->message, $this->recipients, new Swift_Address($email, $name));
		}
	}
	
	function log() {
		return $this->log->dump(true);
	}
	
	function getFailedRecipients() {
		return $this->log->getFailedRecipients();
	}

}
?>