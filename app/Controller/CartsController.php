<?php
class CartsController extends AppController
{
	var $components = array('File','SwiftMailer','RequestHandler','AuthorizeNet');
	var $uses = array('User','Client','Cart','CartProduct','CartItem','CartAddress','Product','RedeemTracker','ShippingCategory');

	function add_cart($productId)
	{
		$this->check_query_numeric($productId);
		$userId =$this->Session->read('user.User.id');

		$sql = "SELECT * FROM carts as Cart WHERE Cart.user_id =".$userId." And Cart.cart_status ='New'";
		$result = $this->Cart->CartItem->query($sql);

		if (empty($result))
		{
			$sql = "SELECT Cart.id FROM carts as Cart, cart_items as CartItem WHERE Cart.id = CartItem.cart_id
			and CartItem.product_id = ".$productId." AND Cart.user_id =".$userId." And Cart.cart_status ='New'";

			$cartlist = $this->Cart->CartItem->query($sql);

			if (empty($cartlist))
			{
				// put the product in cart table
				$this->request->data['Cart']['user_id'] = $userId;
				$this->request->data['Cart']['cart_date'] = date('Y-m-d h:i:s');
				$this->request->data['Cart']['cart_status'] = 'New';
				if($this->Cart->save($this->request->data['Cart']))
				{
					$cartId = $this->Cart->id;
					$this->request->data['CartItem']['cart_id'] = $cartId;
					$this->request->data['CartItem']['user_id'] = $userId;
					$this->request->data['CartItem']['product_id'] = $productId;
					$this->request->data['CartItem']['product_qty'] = 1;
					$this->request->data['CartItem']['product_type'] ='Pay';
					$this->Cart->CartItem->save($this->request->data['CartItem']);
				}
			}
		}
		else {
			$sql = "SELECT Cart.id FROM carts as Cart, cart_items as CartItem
			WHERE Cart.id = CartItem.cart_id and CartItem.product_id = ".$productId." AND Cart.user_id =".$userId." And Cart.cart_status ='New'
			and CartItem.cart_status is Null and CartItem.product_type <> 'Points' and CartItem.product_qty >0";

			$cartlist = $this->Cart->CartItem->query($sql);

			if (empty($cartlist))
			{
				$cartId = $result[0]['Cart']['id'];
				$this->request->data['CartItem']['cart_id'] = $cartId;
				$this->request->data['CartItem']['user_id'] = $userId;
				$this->request->data['CartItem']['product_id'] = $productId;
				$this->request->data['CartItem']['product_qty'] = 1;
				$this->request->data['CartItem']['product_type'] ='Pay';
				$this->Cart->CartItem->save($this->request->data['CartItem']);
			}
		}

		$sql = "SELECT * FROM carts as Cart, cart_products as CartProduct, cart_items as CartItem, products as Product
		WHERE Cart.cart_status='New' And Cart.id = CartItem.cart_id AND CartItem.product_id=CartProduct.product_id AND Product.id=CartItem.product_id
		AND CartItem.cart_status is Null AND CartItem.product_type=CartProduct.product_type and (CartProduct.product_quantity > 0 OR CartProduct.product_quantity=-1) AND
		CartItem.product_type='pay' and Cart.user_id=".$userId." ORDER BY CartProduct.product_shipping_type desc, CartProduct.product_shipping_cost desc";
		$cartlist = $this->Cart->query($sql);

		$this->set('cartDetails',$cartlist);

		//max shipping cost for shipping type as Group Shipping
		$gsc_sql = "SELECT max(CartProduct.product_shipping_cost) as max_group_ship_cost FROM carts as Cart, cart_products as CartProduct, cart_items as CartItem, products as Product
		WHERE Cart.cart_status='New' And Cart.id = CartItem.cart_id AND CartItem.product_id=CartProduct.product_id AND Product.id=CartItem.product_id
		AND CartItem.cart_status is Null AND CartItem.product_type=CartProduct.product_type and (CartProduct.product_quantity > 0 or CartProduct.product_quantity=-1)
		AND Cart.user_id=".$userId." and CartProduct.product_shipping_type=3 and CartProduct.product_type='Pay'";
		$group_shipping_cost = $this->Cart->query($gsc_sql);

		$this->set('max_group_ship_cost',$group_shipping_cost[0][0]['max_group_ship_cost']);

		$shipping_category = $this->ShippingCategory->find('all');
		$this->set('shippingCategory',$shipping_category);
	}

	function show_cart()
	{
		$userId =$this->Session->read('user.User.id');

		$sql = "";
		$sql.= "SELECT * FROM carts as Cart, cart_products as CartProduct, cart_items as CartItem, products as Product
		WHERE Cart.cart_status='New' And Cart.id = CartItem.cart_id AND CartItem.product_id=CartProduct.product_id AND Product.id=CartItem.product_id
		AND CartItem.cart_status is Null AND CartItem.product_type=CartProduct.product_type and (CartProduct.product_quantity > 0 or CartProduct.product_quantity=-1)
		AND Cart.user_id=$userId";

		if($this->Session->read('dashboardidinsession') == 1)
			$sql.= " AND CartItem.product_type='pay' ORDER BY CartProduct.product_shipping_type desc, CartProduct.product_shipping_cost desc";
		if($this->Session->read('dashboardidinsession') == 3)
			$sql.= " AND CartItem.product_type='points'";

		$cartlist = $this->Cart->query($sql);
		$this->set('cartDetails',$cartlist);

		if($this->Session->read('dashboardidinsession') == 1)
		{
			//max shipping cost for shipping type as Group Shipping
			$gsc_sql = "SELECT max(CartProduct.product_shipping_cost) as max_group_ship_cost FROM carts as Cart, cart_products as CartProduct, cart_items as CartItem, products as Product
			WHERE Cart.cart_status='New' And Cart.id = CartItem.cart_id AND CartItem.product_id=CartProduct.product_id AND Product.id=CartItem.product_id
			AND CartItem.cart_status is Null AND CartItem.product_type=CartProduct.product_type and (CartProduct.product_quantity > 0 or CartProduct.product_quantity=-1)
			AND Cart.user_id=".$userId." and CartProduct.product_shipping_type=3 and CartProduct.product_type='Pay'";
			$group_shipping_cost = $this->Cart->query($gsc_sql);

			$this->set('max_group_ship_cost',$group_shipping_cost[0][0]['max_group_ship_cost']);

			$shipping_category = $this->ShippingCategory->find('all');
			$this->set('shippingCategory',$shipping_category);

			$sql = "SELECT count(CartProduct.product_id) as count, CartProduct.product_shipping_type, CartItem.smart_shipping_cost
			FROM carts as Cart, cart_products as CartProduct, cart_items as CartItem, products as Product
			WHERE Cart.cart_status='New' And Cart.id = CartItem.cart_id AND CartItem.product_id=CartProduct.product_id AND Product.id=CartItem.product_id
			AND CartItem.cart_status is Null AND CartItem.product_type=CartProduct.product_type
			and (CartProduct.product_quantity > 0 or CartProduct.product_quantity=-1) AND Cart.user_id=".$userId." and CartProduct.product_shipping_type=4";

			$count = $this->CartProduct->query($sql);
			$this->set('counts',$count);


		}
	}

	function update_cart($cartId,$pid,$cart_item_id,$qty,$shippingtype,$max_group_ship_cost)
	{
		$this->check_query_numeric($cartId,$pid,$cart_item_id,$qty,$shippingtype);
		//updating cart table with new request quantity
		$sql = "UPDATE cart_items set product_qty = ".$qty." where cart_id=".$cartId." and product_id=".$pid." and id=$cart_item_id and user_id=".$this->Session->read('user.User.id');
		$this->Cart->CartItem->query($sql);

		// Getting total amount of new request quantity
		$sql = "SELECT CartItem.product_qty,CartProduct.product_price,CartProduct.product_shipping_cost FROM carts as Cart, cart_items as CartItem,cart_products as CartProduct
		where Cart.id = CartItem.cart_id and Cart.cart_status='New' and CartItem.product_id=CartProduct.product_id AND CartItem.product_type=CartProduct.product_type and CartItem.id=$cart_item_id and CartProduct.product_id=".$pid;
		$prod_list= $this->Cart->query($sql);
		if($shippingtype == 1)
		{
			$total =(($prod_list[0]['CartItem']['product_qty'] * $prod_list[0]['CartProduct']['product_price'])+$prod_list[0]['CartProduct']['product_shipping_cost']);
		}
		if($shippingtype == 2)
		{
			$total =($prod_list[0]['CartItem']['product_qty'] * ($prod_list[0]['CartProduct']['product_price'] + $prod_list[0]['CartProduct']['product_shipping_cost']));
		}
		if($shippingtype == 3)
		{
			if($max_group_ship_cost == $prod_list[0]['CartProduct']['product_shipping_cost'])
			{
				$total =(($prod_list[0]['CartItem']['product_qty'] * $prod_list[0]['CartProduct']['product_price'])+$max_group_ship_cost);
			}
			else
			{
				$total =($prod_list[0]['CartItem']['product_qty'] * $prod_list[0]['CartProduct']['product_price']);
			}
		}
		if($shippingtype == 4)
		{
			$total =$prod_list[0]['CartItem']['product_qty'] * $prod_list[0]['CartProduct']['product_price'];
		}
		$this->set("total",$total);
	}

	function grand_total($max_group_ship_cost)
	{
		// Getting total amount of cart
		$sql = "SELECT CartProduct.product_shipping_cost, CartProduct.product_shipping_type as stype, (CartItem.product_qty * CartProduct.product_price) as total,
		(case when CartProduct.product_shipping_type=2 then CartProduct.product_shipping_cost*CartItem.product_qty else CartProduct.product_shipping_cost end) as shippingcost
		FROM carts as Cart, cart_items as CartItem,cart_products as CartProduct where Cart.id = CartItem.cart_id and Cart.cart_status='New'
		and CartItem.cart_status is Null and CartItem.product_id=CartProduct.product_id AND CartProduct.product_type = CartItem.product_type
		AND CartItem.product_type='Pay' and Cart.user_id=".$this->Session->read('user.User.id')." ORDER BY
		CartProduct.product_shipping_type desc, CartProduct.product_shipping_cost desc ";

		$grand=$this->Cart->query($sql);

		$sql1 = "SELECT count(CartProduct.product_id) as count, CartProduct.product_shipping_type, CartItem.smart_shipping_cost
				FROM carts as Cart, cart_products as CartProduct, cart_items as CartItem, products as Product
				WHERE Cart.cart_status='New' And Cart.id = CartItem.cart_id AND CartItem.product_id=CartProduct.product_id AND Product.id=CartItem.product_id
				AND CartItem.cart_status is Null AND CartItem.product_type=CartProduct.product_type
				and (CartProduct.product_quantity > 0 or CartProduct.product_quantity=-1) AND Cart.user_id=".$this->Session->read('user.User.id')." and CartProduct.product_shipping_type=4";

		$count = $this->CartProduct->query($sql1);
		$grand_total=0;$shipping_count = 0;

		foreach($grand as $details)
		{
			if($details['CartProduct']['stype']==3)
			{
				if($max_group_ship_cost == $details['CartProduct']['product_shipping_cost'])
					$total=$details[0]['total']+$max_group_ship_cost;
				else
					$total=$details[0]['total'];
			}
			elseif($details['CartProduct']['stype']==4)
			{
				if($shipping_count != $details['CartProduct']['stype'])
					$total=$details[0]['total']+$count[0]['CartItem']['smart_shipping_cost'];
				else
					$total=$details[0]['total'];
			}
			else
			{
				$total=$details[0]['total']+$details[0]['shippingcost'];
			}
			$shipping_count = $details['CartProduct']['stype'];
			$grand_total += $total;

		}
		$this->set("grandtotal",$grand_total);
		$this->layout = 'ajax';
	}

	function smart_grand_total($shipcost, $max_group_ship_cost)
	{
		$userId =$this->Session->read('user.User.id');

		$sql = "SELECT CartProduct.product_id, CartItem.id
		FROM carts as Cart, cart_products as CartProduct, cart_items as CartItem, products as Product
		WHERE Cart.cart_status='New' And Cart.id = CartItem.cart_id AND CartItem.product_id=CartProduct.product_id AND Product.id=CartItem.product_id
		AND CartItem.cart_status is Null AND CartItem.product_type=CartProduct.product_type
		and (CartProduct.product_quantity > 0 or CartProduct.product_quantity=-1) AND Cart.user_id=".$userId." and CartProduct.product_shipping_type=4";
		$smartproducts = $this->Cart->query($sql);

		foreach($smartproducts as $product)
		{
			$sql = "UPDATE cart_items set smart_shipping_cost = ".$shipcost." where product_id=".$product['CartProduct']['product_id']."
			and id=".$product['CartItem']['id']." and user_id=".$this->Session->read('user.User.id');
			$this->Cart->CartItem->query($sql);
		}

		// Getting total amount of cart
		$sql = "SELECT CartProduct.product_shipping_cost, CartProduct.product_shipping_type as stype, (CartItem.product_qty * CartProduct.product_price) as total,
		(case when CartProduct.product_shipping_type=2 then CartProduct.product_shipping_cost*CartItem.product_qty else CartProduct.product_shipping_cost end) as shippingcost
		FROM carts as Cart, cart_items as CartItem,cart_products as CartProduct where Cart.id = CartItem.cart_id and Cart.cart_status='New'
		and CartItem.cart_status is Null and CartItem.product_id=CartProduct.product_id AND CartProduct.product_type = CartItem.product_type
		AND CartItem.product_type='Pay' and Cart.user_id=".$this->Session->read('user.User.id')." ORDER BY
		CartProduct.product_shipping_type desc, CartProduct.product_shipping_cost desc ";

		$grand=$this->Cart->query($sql);
		$grand_total=0;$shipping_count = 0;
		foreach($grand as $details)
		{
			if($details['CartProduct']['stype']==3)
			{
				if($shipping_count != $details['CartProduct']['stype'])
					$total=$details[0]['total']+$max_group_ship_cost;
				else
					$total=$details[0]['total'];
			}
			else
			{
				$total=$details[0]['total']+$details[0]['shippingcost'];
			}
			$grand_total += $total;
			$shipping_count = $details['CartProduct']['stype'];
		}
		$grand_total = $grand_total + $shipcost;

		$this->set("grandtotal",$grand_total);
		$this->layout = 'ajax';
	}

	function order_summary($cid)
	{
		$userId =$this->Session->read('user.User.id');
		$id=$this->request->data['CartAddress']['id'];
		$cartid = $this->request->data['CartAddress']['cartid'];
		$status = $this->request->data['CartAddress']['status'];

		$this->request->data['CartAddress']['username']=$this->Session->read('user.User.username');
		$this->request->data['CartAddress']['member_name']=$this->Session->read('user.User.first_name').' '.$this->Session->read('user.User.last_name');

		if(empty($id))
		{
			if($status == 'order')
			{
				$caId = $this->Cart->find('all',array('conditions'=>array('Cart.id'=>$cid),'fields'=>array('Cart.address_id')));
				$caddId = $caId[0]['Cart']['address_id'];
			}
			elseif($status == 'new')
			{
				$this->request->data['CartAddress']['cart_id']=$cartid;
				$this->request->data['CartAddress']['user_id']=$userId;
				$this->request->data['CartAddress']['date']=date('Y-m-d H:i:s');
				$this->request->data['CartAddress']['last_update']=date('Y-m-d H:i:s');
				$this->CartAddress->save($this->request->data['CartAddress']);
				$caddId =  $this->CartAddress->id;

				$sql = "UPDATE carts set address_id = ".$caddId." where user_id=".$this->Session->read('user.User.id')." and id = ".$cartid;
				$this->Cart->query($sql);
			}
		}
		else
		{
			$this->request->data['CartAddress']['cart_id']=$cartid;
			$this->CartAddress->save($this->request->data['CartAddress']);

			$sql = "UPDATE carts set address_id = ".$id." where cart_status='New' and user_id=".$userId." and id =".$cartid;
			$this->Cart->query($sql);

			$caddId = $id;
		}

		$sql = "";
		$sql.= "SELECT * FROM carts as Cart, cart_items as CartItem, cart_products as CartProduct,products as Product
		where Cart.id =CartItem.cart_id and CartItem.product_id=CartProduct.product_id and CartProduct.product_id=Product.id
		and Cart.cart_status='New' and (CartProduct.product_quantity > 0 OR CartProduct.product_quantity=-1) AND CartItem.cart_status is Null
		AND CartItem.product_type=CartProduct.product_type and Cart.user_id=$userId";
		if($this->Session->read('dashboardidinsession') == 1)
			$sql.= " AND CartItem.product_type='pay' ORDER BY CartProduct.product_shipping_type desc, CartProduct.product_shipping_cost desc";
		if($this->Session->read('dashboardidinsession') == 3)
			$sql.= " AND CartItem.product_type='points'";
		$cartlist = $this->Cart->query($sql);

		$this->set('cartDetails',$cartlist);

		if($this->Session->read('dashboardidinsession') == 1)
		{
			//max shipping cost for shipping type as Group Shipping
			$gsc_sql = "SELECT max(CartProduct.product_shipping_cost) as max_group_ship_cost FROM carts as Cart, cart_items as CartItem, cart_products as CartProduct,products as Product
			where Cart.id =CartItem.cart_id and CartItem.product_id=CartProduct.product_id and CartProduct.product_id=Product.id
			and Cart.cart_status='New' and (CartProduct.product_quantity > 0 OR CartProduct.product_quantity=-1) AND CartItem.cart_status is Null
			AND CartItem.product_type=CartProduct.product_type and Cart.user_id=".$userId." and CartProduct.product_shipping_type=3 and CartProduct.product_type='Pay'";

			$group_shipping_cost = $this->Cart->query($gsc_sql);

			$this->set('max_group_ship_cost',$group_shipping_cost[0][0]['max_group_ship_cost']);

			$sql = "SELECT count(CartProduct.product_id) as count, CartProduct.product_shipping_type, CartItem.smart_shipping_cost
			FROM carts as Cart, cart_products as CartProduct, cart_items as CartItem, products as Product
			WHERE Cart.cart_status='New' And Cart.id = CartItem.cart_id AND CartItem.product_id=CartProduct.product_id AND Product.id=CartItem.product_id
			AND CartItem.cart_status is Null AND CartItem.product_type=CartProduct.product_type
			and (CartProduct.product_quantity > 0 or CartProduct.product_quantity=-1) AND Cart.user_id=".$userId." and CartProduct.product_shipping_type=4";

			$count = $this->CartProduct->query($sql);
			$this->set('counts',$count);

			$shipping_category = $this->ShippingCategory->find('all');
			$this->set('shippingCategory',$shipping_category);
		}

		$sql = "SELECT * FROM carts as Cart, cart_addresses as CartAddress where CartAddress.id=Cart.address_id
		and Cart.user_id= $userId and Cart.cart_status='New' and CartAddress.id=".$caddId;
		$useraddress = $this->CartAddress->query($sql);
		$this->set('useraddress',$useraddress);
	}

	function proceed_payment()
	{
		$userId = $this->Session->read('user.User.id');

		$sql = "SELECT * FROM carts as Cart, cart_items as CartItem, cart_products as CartProduct, products as Product
		where Cart.id = CartItem.cart_id and CartItem.product_id=CartProduct.product_id and CartItem.product_type=CartProduct.product_type AND CartItem.cart_status is Null
		and CartProduct.product_id=Product.id and Cart.cart_status='New' AND Cart.user_id=".$userId;

		if($this->Session->read('dashboardidinsession') == 1)
			$sql.= " AND CartItem.product_type='Pay' ORDER BY CartProduct.product_shipping_type desc, CartProduct.product_shipping_cost desc";
		if($this->Session->read('dashboardidinsession') == 3)
			$sql.= " AND CartItem.product_type='Points'";

		$cartlist = $this->Cart->query($sql);
		$this->set('cartlist',$cartlist);

		if($this->Session->read('dashboardidinsession') == 1)
		{
			//max shipping cost for shipping type as Group Shipping
			$gsc_sql = "SELECT max(CartProduct.product_shipping_cost) as max_group_ship_cost
			FROM carts as Cart, cart_items as CartItem, cart_products as CartProduct, products as Product
			where Cart.id = CartItem.cart_id and CartItem.product_id=CartProduct.product_id and CartItem.product_type=CartProduct.product_type AND CartItem.cart_status is Null
			and CartProduct.product_id=Product.id and Cart.cart_status='New' AND Cart.user_id=".$userId."
			and CartProduct.product_shipping_type=3 and CartProduct.product_type='Pay'";
			$group_shipping_cost = $this->Cart->query($gsc_sql);

			$this->set('max_group_ship_cost',$group_shipping_cost[0][0]['max_group_ship_cost']);

			$sql = "SELECT count(CartProduct.product_id) as count, CartProduct.product_shipping_type, CartItem.smart_shipping_cost
			FROM carts as Cart, cart_products as CartProduct, cart_items as CartItem, products as Product
			WHERE Cart.cart_status='New' And Cart.id = CartItem.cart_id AND CartItem.product_id=CartProduct.product_id AND Product.id=CartItem.product_id
			AND CartItem.cart_status is Null AND CartItem.product_type=CartProduct.product_type
			and (CartProduct.product_quantity > 0 or CartProduct.product_quantity=-1) AND Cart.user_id=".$userId." and CartProduct.product_shipping_type=4";

			$count = $this->CartProduct->query($sql);
			$this->set('counts',$count);
		}

		// total cart product points
		$sql = "SELECT sum((CartItem.product_qty * CartProduct.product_points)) as total
		FROM carts as Cart, cart_items as CartItem, cart_products as CartProduct,products as Product
		where Cart.id = CartItem.cart_id and CartItem.product_id=CartProduct.product_id and CartProduct.product_id=Product.id
		and Cart.cart_status='New' and CartProduct.product_type='Points' and CartItem.product_type=CartProduct.product_type AND CartItem.cart_status is Null And Cart.user_id=$userId ";

		$pointslist = $this->Cart->query($sql);
		$this->set('pointslist',$pointslist);

		// Get total points from user
		$total_user_points_sql = "SELECT points FROM users where id=".$userId;
		$userpoints= $this->User->query($total_user_points_sql);
		$total_user_points=$userpoints['0']['users']['points'];
		$this->set('total_user_points',$total_user_points);

		$this->set('cartid',$cartlist[0]['Cart'] ['id']);

		// get grand total of products
		$sql = "SELECT (CartItem.product_qty * CartProduct.product_price) as total,
		(case when CartProduct.product_shipping_type=1 then CartProduct.product_shipping_cost*CartItem.product_qty else CartProduct.product_shipping_cost end) as shippingcost
		FROM carts as Cart, cart_items as CartItem,cart_products as CartProduct where Cart.id = CartItem.cart_id and Cart.cart_status='New'
		and CartItem.product_id=CartProduct.product_id and Cart.id=".$cartlist[0]['Cart'] ['id'];

		$grand=$this->Cart->query($sql);
		$grand_total=0;
		foreach($grand as $details)
		{
			$total=$details[0]['total']+$details[0]['shippingcost'];
			$grand_total += $total;
		}

		$this->set('total',$grand_total);

		$sql = "SELECT * FROM carts as Cart, cart_addresses as CartAddress where CartAddress.id=Cart.address_id
		and Cart.user_id= $userId and Cart.cart_status='New' and CartAddress.id=".$cartlist[0]['Cart'] ['id'];
		$useraddress = $this->CartAddress->query($sql);
		$this->set('useraddress',$useraddress);
	}

	function invoice($uid)
	{
		$cartid = $_POST['custom'];
		$status = addslashes($_POST['payment_status']);
		$tx_id = $_POST['txn_id'];
		$date=$data['created'] = date('Y-m-d H:m:s', strtotime('+1 week'));
		$today = date("Y-m-d");

		if($status == 'Completed')
			$status = 'Paid';
		$sql = "SELECT * FROM carts as Cart, cart_items as CartItem, cart_products as CartProduct,products as Product
		where Cart.id = CartItem.cart_id and CartItem.product_id=CartProduct.product_id and CartItem.product_type=CartProduct.product_type
		AND CartItem.cart_status is Null
		and CartProduct.product_id=Product.id and Cart.cart_status='New' AND Cart.user_id=".$uid." and CartItem.product_type='pay'
		ORDER BY CartProduct.product_shipping_type desc, CartProduct.product_shipping_cost desc ";
		$cartlist = $this->Cart->query($sql);

		$maxordId = $this->CartItem->find('all',array('fields'=>array('MAX(CartItem.order_id) as maxid'),'recursive'=>-1));
		$maxOrder = $maxordId[0][0]['maxid'] +1;

		$gsc_sql = "SELECT max(CartProduct.product_shipping_cost) as max_group_ship_cost FROM carts as Cart, cart_products as CartProduct, cart_items as CartItem,
		products as Product
		WHERE Cart.cart_status='New' And Cart.id = CartItem.cart_id AND CartItem.product_id=CartProduct.product_id AND Product.id=CartItem.product_id
		AND CartItem.cart_status is Null AND CartItem.product_type=CartProduct.product_type and (CartProduct.product_quantity > 0 or CartProduct.product_quantity=-1)
		AND Cart.user_id=".$uid." and CartProduct.product_shipping_type=3 and CartProduct.product_type='Pay'";
		$group_shipping_cost = $this->Cart->query($gsc_sql);

		$sql = "SELECT CartItem.smart_shipping_cost
		FROM carts as Cart, cart_products as CartProduct, cart_items as CartItem, products as Product
		WHERE Cart.cart_status='New' And Cart.id = CartItem.cart_id AND CartItem.product_id=CartProduct.product_id AND Product.id=CartItem.product_id
		AND CartItem.cart_status is Null AND CartItem.product_type=CartProduct.product_type
		and (CartProduct.product_quantity > 0 or CartProduct.product_quantity=-1) AND Cart.user_id=".$uid." and CartProduct.product_type='Pay'
		and CartProduct.product_shipping_type=4";

		$count = $this->CartProduct->query($sql);

		$shipping_count = 0;
		foreach($cartlist as $list)
		{
			$qty = $list['CartItem']['product_qty'];
			$price = $list['CartProduct']['product_price'];

			if($list['CartProduct']['product_shipping_type']==1)
			{
				$ship_cost = $list['CartProduct']['product_shipping_cost'];
				$total = (($qty * $price) + $list['CartProduct']['product_shipping_cost']);
			}
			if($list['CartProduct']['product_shipping_type']==2)
			{
				$ship_cost = $list['CartProduct']['product_shipping_cost'];
				$total = ($qty * ($price + $list['CartProduct']['product_shipping_cost']));
			}

			if($list['CartProduct']['product_shipping_type']==3)
			{
				if($shipping_count != $list['CartProduct']['product_shipping_type'])
				{
					$ship_cost = $group_shipping_cost[0][0]['max_group_ship_cost'];
					$total = (($qty * $price) + $group_shipping_cost[0][0]['max_group_ship_cost']);
				}
				else
				{
					$ship_cost = "0.00";
					$total = ($qty * $price);
				}
			}

			if($list['CartProduct']['product_shipping_type']==4)
			{
				if($shipping_count != $list['CartProduct']['product_shipping_type'])
				{
					$ship_cost = $count[0]['CartItem']['smart_shipping_cost'];
					$total = (($qty * $price) + $count[0]['CartItem']['smart_shipping_cost']);
				}
				else
				{
					$ship_cost = "0.00";
					$total = ($qty * $price);
				}

			}

			$sql = "UPDATE cart_items CartItem set CartItem.order_id = ".$maxOrder.", CartItem.total=".$total.", CartItem.ship_cost=".$ship_cost." WHERE CartItem.id = ".$list['CartItem']['id'];
			$this->CartItem->query($sql);

			if($list['CartProduct']['product_quantity']>0 && $list['CartProduct']['product_quantity']!=-1)
			{
				$qty = $list['CartProduct']['product_quantity'] - $list['CartItem']['product_qty'];

				$sql1 = "UPDATE cart_items as CartItem, cart_products as CartProduct set CartProduct.product_quantity = ".$qty."
				WHERE CartProduct.product_id=CartItem.product_id AND CartProduct.product_id=".$list['CartProduct']['product_id']."
				AND CartProduct.product_type='pay' AND CartItem.id = ".$list['CartItem']['id'];

				$this->CartProduct->query($sql1);
			}
			$shipping_count = $list['CartProduct']['product_shipping_type'];
		}

		$sql = "UPDATE carts set cart_status ='".$status."' where id=".$cartid;
		$this->Cart->query($sql);

		$sql="UPDATE cart_items set cart_status ='".$status."',order_date='".$today."' where cart_id=".$cartid." and order_id=".$maxOrder;
		$this->Cart->query($sql);

		$this->autoRender = false;
	}

	function success($cartid=null, $userId)
	{
		$this->check_query_numeric($cartid);

		$sql = "SELECT * FROM carts as Cart, cart_items as CartItem, cart_products as CartProduct, cart_addresses as CartAddress,products as Product
		where Cart.id = CartItem.cart_id and Cart.address_id = CartAddress.id and CartItem.product_id=Product.id and CartItem.product_id=CartProduct.product_id
		and (CartItem.cart_status='Pending' or CartItem.cart_status='Paid') and CartItem.product_type='Pay' and CartItem.product_type=CartProduct.product_type
		And CartItem.order_date = '".$today."' AND Cart.user_id=$userId and Cart.id =".$cartid;
		$sql.=" ORDER BY CartProduct.product_shipping_type desc, CartProduct.product_shipping_cost desc";

		$cartlist = $this->Cart->query($sql);

		$this->set('cartlist',$cartlist);

		//max shipping cost for shipping type as Group Shipping
		$gsc_sql = "SELECT max(CartProduct.product_shipping_cost) as max_group_ship_cost FROM carts as Cart, cart_items as CartItem, cart_products as CartProduct, cart_addresses as CartAddress,products as Product
		where Cart.id = CartItem.cart_id and Cart.address_id = CartAddress.id and CartItem.product_id=Product.id and CartItem.product_id=CartProduct.product_id
		and CartItem.product_type=CartProduct.product_type AND CartItem.cart_id=".$cartid." and CartItem.order_id=".$cartlist[0]['CartItem']['order_id']."
		and CartProduct.product_shipping_type=3 and CartProduct.product_type='Pay'";

		$group_shipping_cost = $this->Cart->query($gsc_sql);

		$this->set('max_group_ship_cost',$group_shipping_cost[0][0]['max_group_ship_cost']);

		$sql = "SELECT count(CartProduct.product_id) as count, CartProduct.product_shipping_type, CartItem.smart_shipping_cost
		FROM carts as Cart, cart_items as CartItem, cart_products as CartProduct, cart_addresses as CartAddress,products as Product
		where Cart.id = CartItem.cart_id and Cart.address_id = CartAddress.id and CartItem.product_id=Product.id and CartItem.product_id=CartProduct.product_id
		and (CartItem.cart_status='Pending' or CartItem.cart_status='Paid') and CartItem.product_type='Pay' and CartItem.product_type=CartProduct.product_type
		And CartItem.order_date = '".$today."' AND Cart.user_id=$userId and Cart.id =".$cartid." and CartProduct.product_shipping_type=4";

		$count = $this->CartProduct->query($sql);
		$this->set('counts',$count);

		$tomail=$cartlist[0]['CartAddress']['email'];
		$this->set('domain', $this->Session->read('client.Domain.name'));

		$this->SwiftMailer->layout = 'shopping_cart';
		if($this->SwiftMailer->connect()) {
			$this->SwiftMailer->addTo('to', ''.$tomail.'');
			$this->SwiftMailer->addTo('from','support@therewardsteam.com');
			if(!$this->SwiftMailer->sendView('Your Myrewards Order:'. $cartlist[0]['Cart']['id'], 'shopping_cart', 'html')){
				//echo "Payment error.Mail not sent.";
				//die(debug($this->SwiftMailer->errors()));
				$this->redirect('/products/shop_now/');
			}
			else {
				$this->Session->setFlash('Your cart is successfully updated!!');
				$this->redirect('/products/shop_now/');
			}
		}
	}

	/*
	 * Shopping Cart End
	*/

	/*
	 * Points Begin
	 */
	function redeem_cart_points($productId)
	{
		$this->check_query_numeric($productId);
		$userId =$this->Session->read('user.User.id');

		// Get total points from user
		$total_user_points_sql = "SELECT points FROM users where id=".$userId;
		$userpoints= $this->User->query($total_user_points_sql);
		$total_user_points=$userpoints['0']['users']['points'];

		$product_points_sql = "SELECT product_points FROM cart_products where product_id=".$productId;
		$prodpoints= $this->CartProduct->query($product_points_sql);
		$product_points=$prodpoints['0']['cart_products']['product_points'];


		$sql = "SELECT * FROM carts as Cart WHERE Cart.user_id =".$userId." And Cart.cart_status ='New'";
		$result = $this->Cart->CartItem->query($sql);

		if ($total_user_points > 0 && ($total_user_points > $product_points)) {

			if (empty($result))
			{
				$sql = "SELECT Cart.id FROM carts as Cart, cart_items as CartItem WHERE Cart.id = CartItem.cart_id
				and CartItem.product_id = ".$productId." AND Cart.user_id =".$userId." And Cart.cart_status ='New'";

				$cartlist = $this->Cart->CartItem->query($sql);

				if (empty($cartlist))
				{
					// put the product in cart table
					$this->request->data['Cart']['user_id'] = $userId;
					$this->request->data['Cart']['cart_date'] = date('Y-m-d h:i:s');
					$this->request->data['Cart']['cart_status'] = 'New';
					if($this->Cart->save($this->request->data))
					{
						$cartId = $this->Cart->id;
						$this->request->data['CartItem']['cart_id'] = $cartId;
						$this->request->data['CartItem']['user_id'] = $userId;
						$this->request->data['CartItem']['product_id'] = $productId;
						$this->request->data['CartItem']['product_qty'] = 1;
						$this->request->data['CartItem']['product_type'] ='Points';
						$this->Cart->CartItem->save($this->request->data);
					}
				}
			}
			else {

				/*$sql = "SELECT Cart.id FROM carts as Cart, cart_items as CartItem , cart_products as CartProduct WHERE Cart.id = CartItem.cart_id
					and CartItem.product_id = ".$productId." AND Cart.user_id =".$userId." And Cart.cart_status ='New' and CartItem.cart_status is Null and
				CartProduct.product_quantity >0";*/
				$sql = "SELECT Cart.id FROM carts as Cart, cart_items as CartItem WHERE Cart.id = CartItem.cart_id
				and CartItem.product_id = ".$productId." AND Cart.user_id =".$userId." And Cart.cart_status ='New' and CartItem.cart_status is Null
				and CartItem.product_qty >0";
				$carts = $this->Cart->CartItem->query($sql);

				if (empty($carts))
				{
					$cartId = $result[0]['Cart']['id'];
					$this->request->data['CartItem']['cart_id'] = $cartId;
					$this->request->data['CartItem']['user_id'] = $userId;
					$this->request->data['CartItem']['product_id'] = $productId;
					$this->request->data['CartItem']['product_qty'] = 1;
					$this->request->data['CartItem']['product_type'] ='Points';
					$this->Cart->CartItem->save($this->request->data);
				}
			}
			$sql = "SELECT * FROM carts as Cart, cart_products as CartProduct, cart_items as CartItem, products as Product
			WHERE Cart.cart_status='New' And Cart.id = CartItem.cart_id AND CartItem.product_id=CartProduct.product_id AND Product.id=CartItem.product_id
			AND CartItem.cart_status is Null AND CartItem.product_type=CartProduct.product_type
			and (CartProduct.product_quantity > 0 OR CartProduct.product_quantity=-1) AND CartItem.product_type='points' and Cart.user_id=$userId";
			$cartlist = $this->Cart->query($sql);

			$this->set('cartDetails',$cartlist);
		}

		else
		{
			echo "<div style='color: red; padding: 50px 0px 50px 90px;'><strong>Sorry, You Don't Have Sufficient Points To Redeem This Product.<strong></div>";

		}
	}

	function update_cart_points($cartId,$pid,$cart_item_id,$qty,$shippingtype)
	{
		$this->check_query_numeric($cartId,$pid,$cart_item_id,$qty,$shippingtype);
		//updating cart table with new request quantity
		$sql = "UPDATE cart_items set product_qty = ".$qty." where cart_id=".$cartId." and product_id=".$pid." and id=$cart_item_id and user_id=".$this->Session->read('user.User.id');
		$this->Cart->CartItem->query($sql);

		// Getting total amount of new request quantity

		$sql = "SELECT CartItem.product_qty,CartProduct.product_points,CartProduct.product_shipping_cost FROM carts as Cart, cart_items as CartItem,cart_products as CartProduct
		where Cart.id = CartItem.cart_id and Cart.cart_status='New' and CartItem.cart_status is null AND CartItem.product_type=CartProduct.product_type and CartItem.id=$cart_item_id and CartItem.product_id=CartProduct.product_id and CartProduct.product_id=".$pid;
		$prod_list= $this->Cart->query($sql);
		$total =($prod_list[0]['CartItem']['product_qty'] * $prod_list[0]['CartProduct']['product_points']);
		$this->set("total",$total);
	}

	function grand_total_points()
	{
		// Getting total amount of cart
		$sql = "SELECT (CartItem.product_qty * CartProduct.product_points) as total
		FROM carts as Cart, cart_items as CartItem,cart_products as CartProduct where Cart.id = CartItem.cart_id and Cart.cart_status='New'
		and CartItem.product_id=CartProduct.product_id AND CartProduct.product_type = CartItem.product_type and CartItem.cart_status is Null
		AND CartItem.product_type='points' and Cart.user_id=".$this->Session->read('user.User.id');

		$grand=$this->Cart->query($sql);
		$grand_total=0;
		foreach($grand as $details)
		{
			$total=$details[0]['total'];
			$grand_total += $total;
		}

		$this->set("grandtotal",$grand_total);
		$this->layout = 'ajax';
	}


	/*
	 * Relative to both shopping cart and points
	 */
	function remove_cart($cartid,$pid,$cartitemid)
	{
		$this->check_query_numeric($cartid,$pid,$cartitemid);
		$sql="delete from cart_items where product_id=$pid and cart_id=$cartid and id=$cartitemid and user_id=".$this->Session->read('user.User.id');
		$grand=$this->Cart->query($sql);
		$this->redirect('/carts/show_cart/');
		$this->Session->setFlash('Product deleted from cart');
	}

	function check_out()
	{
		$sql = "SELECT * FROM cart_addresses as CartAddress where CartAddress.user_id=".$this->Session->read('user.User.id')." order by modified desc limit 2";
		$user_address=$this->CartAddress->query($sql);
		$this->set("user_address",$user_address);
		$userid=$this->Session->read('user.User.id');

		//Getting List of products
		$sql = "";
		$sql.= "SELECT *  FROM carts as Cart, cart_items as CartItem, cart_products as CartProduct,products as Product
		where Cart.id =CartItem.cart_id and Product.id=CartItem.product_id and CartItem.product_id=CartProduct.product_id
		and Cart.cart_status='New' AND CartItem.cart_status is Null AND CartItem.product_type=CartProduct.product_type
		and (CartProduct.product_quantity > 0 OR CartProduct.product_quantity=-1) and Cart.user_id= $userid";

		if($this->Session->read('dashboardidinsession') == 1)
			$sql.= " AND CartItem.product_type='pay' ORDER BY CartProduct.product_shipping_type desc, CartProduct.product_shipping_cost desc";
		if($this->Session->read('dashboardidinsession') == 3)
			$sql.= " AND CartItem.product_type='points'";

		$cart=$this->Cart->query($sql);
		$this->set("cart",$cart);

		if($this->Session->read('dashboardidinsession') == 1)
		{
			//max shipping cost for shipping type as Group Shipping
			$gsc_sql = "SELECT max(CartProduct.product_shipping_cost) as max_group_ship_cost FROM carts as Cart, cart_items as CartItem, cart_products as CartProduct,products as Product
			where Cart.id =CartItem.cart_id and Product.id=CartItem.product_id and CartItem.product_id=CartProduct.product_id
			and Cart.cart_status='New' AND CartItem.cart_status is Null AND CartItem.product_type=CartProduct.product_type
			and (CartProduct.product_quantity > 0 OR CartProduct.product_quantity=-1) and Cart.user_id=".$userid."
			and CartProduct.product_shipping_type=3 and CartProduct.product_type='Pay'";

			$group_shipping_cost = $this->Cart->query($gsc_sql);

			$this->set('max_group_ship_cost',$group_shipping_cost[0][0]['max_group_ship_cost']);

			$sql = "SELECT count(CartProduct.product_id) as count, CartProduct.product_shipping_type, CartItem.smart_shipping_cost
			FROM carts as Cart, cart_products as CartProduct, cart_items as CartItem, products as Product
			WHERE Cart.cart_status='New' And Cart.id = CartItem.cart_id AND CartItem.product_id=CartProduct.product_id AND Product.id=CartItem.product_id
			AND CartItem.cart_status is Null AND CartItem.product_type=CartProduct.product_type
			and (CartProduct.product_quantity > 0 or CartProduct.product_quantity=-1) AND Cart.user_id=".$userid." and CartProduct.product_shipping_type=4";

			$count = $this->CartProduct->query($sql);
			$this->set('counts',$count);
		}

		// getting counties list
		$this->loadModel('Country');
		$countries = $this->Country->find('list', array('fields' => 'Country.name, Country.name'));

		$this->set('countries',$countries);

		$address = $this->CartAddress->find('all',array('conditions'=>array('CartAddress.id'=>$cart[0]['Cart']['address_id'])));
		$this->set('address',$address);

		$defaultstatesql ="SELECT State.state_cd, State.state_name FROM states AS State, countries as Country where State.country_cd = Country.id
		and Country.name='".$address[0]['CartAddress']['country']."'";

		$this->loadModel("State");
		$stateslist = $this->State->query($defaultstatesql);
		$states = array();
		foreach ($stateslist as $state) {
			$states[$state['State']['state_cd']] = $state['State']['state_name'];
		}
		$this->set('states',$states);
	}

	function update_online_request_state($country=null)
	{
		$selected_country= addslashes($this->request->data['CartAddress']['country']);
		if($country!=null){
			$selected_country=addslashes($country);
		}
		$defaultstatesql ="SELECT State.state_cd, State.state_name FROM states AS State, countries as Country where State.country_cd = Country.id
		and Country.name='".$selected_country."'";

		$this->loadModel("State");
		$cstates = $this->State->query($defaultstatesql);

		$ctrystate = array();
		foreach ($cstates as $ctrystates) {
			$ctrystate[$ctrystates['State']['state_cd']] = $ctrystates['State']['state_name'];
		}

		$this->set('cstates',$ctrystate);
		$this->layout = 'ajax';
	}

	function redeem_points()
	{
		$uid = $this->Session->read('user.User.id');
		$sql = "SELECT * FROM carts as Cart, cart_items as CartItem, cart_products as CartProduct,products as Product
		where Cart.id = CartItem.cart_id and CartItem.product_id=CartProduct.product_id and CartItem.product_type=CartProduct.product_type AND CartItem.cart_status is Null
		and CartProduct.product_id=Product.id and Cart.cart_status='New' AND Cart.user_id=".$uid." and CartItem.product_type='Points'";
		$cartlist = $this->Cart->query($sql);

		$maxordId = $this->CartItem->find('all',array('fields'=>array('MAX(CartItem.order_id) as maxid'),'recursive'=>-1));
		$maxOrder = $maxordId[0][0]['maxid'] +1;

		foreach($cartlist as $list)
		{
			$sql = "UPDATE cart_items CartItem set CartItem.order_id = ".$maxOrder." WHERE CartItem.id = ".$list['CartItem']['id'];
			$this->CartItem->query($sql);

			if($list['CartProduct']['product_quantity']!=-1)
			{
				$points = $list['CartProduct']['product_quantity'] - $list['CartItem']['product_qty'];
				$sql1 = "UPDATE cart_products CartProduct set CartProduct.product_quantity = ".$points." WHERE CartProduct.id = ".$list['CartProduct']['id'];
				$this->CartProduct->query($sql1);
			}
		}

		$redeem_product_points=$_POST['points'];
		$cart_id=$_POST['cartid'];

		$today = date("Y-m-d");
		// Get total points from user
		$total_user_points_sql = "SELECT points FROM users where id=".$uid;
		$userpoints= $this->User->query($total_user_points_sql);
		$total_user_points=$userpoints['0']['users']['points'];
		$this->set('total_user_points',$total_user_points);

		$user_redem_rem_points=($total_user_points-$redeem_product_points);


		$redeem_user_update_query = "UPDATE users  SET points = ".$user_redem_rem_points." WHERE id=".$uid;
		$this->User->query($redeem_user_update_query);

		// update cart items table
		$sql = "update cart_items a, cart_products b set a.cart_status='Pending', a.payment_type='Points',a.order_date = '".$today."'
		where a.cart_id=$cart_id and a.product_id=b.product_id and b.product_type='Points' and a.order_id=".$maxOrder;
		$this->Cart->query($sql);

		// update cart table
		$sql = "update carts a, cart_items b set a.cart_status='Pending' where a.id=b.cart_id and b.id=$cart_id and b.cart_status is not null";

		$this->Cart->query($sql);

		// get redeemed product details
		$sql = "select a.product_qty,c.name,c.id, b.product_points*a.product_qty as tot from cart_items a, cart_products b, products c where a.cart_id=$cart_id and a.product_type=b.product_type
		and a.product_id=b.product_id and c.id=b.product_id And a.order_date = '".$today."' and b.product_type='Points' and a.order_id=".$maxOrder;

		$redeem_product_list=$this->Cart->query($sql);


		// get redeemer details
		$redeem_user = $this->User->find('first',array('conditions' => array('User.id' => $uid), 'recursive' => -1));

		$redeem_user_points=$redeem_user['User']['points'];
		$redeem_client = $this->User->Client->find('first',array('conditions' => array('Client.id' => $redeem_user['User']['client_id']), 'recursive' => -1, 'fields' => array('Client.email')));
 
		// Log
		foreach($redeem_product_list as $list)
		{
			$user_points_tracker_query = "INSERT INTO redeem_trackers ( user_id, product_id,points,quantity,activity,description,activity_date )
			VALUES ('".$uid."','".$list['c']['id']."',".$list[0]['tot'].",'".$list['a']['product_qty']."','Product/s Redeemed.','".$list['c']['name']."', CURDATE())";
			$this->RedeemTracker->query($user_points_tracker_query);
		}

		$this->set('redeem_user',$redeem_user);
		$this->set('redeem_product_list',$redeem_product_list);
		$this->set('redeem_product_points',$redeem_product_points);
       $this->set('user_redem_rem_points',$user_redem_rem_points);
	   
		//sending mail to client admin of user
		$adminmail=$redeem_client['Client']['email'];

		$this->SwiftMailer->layout = 'redeem_points';
		if($this->SwiftMailer->connect()) {
			$this->SwiftMailer->addTo('to', ''.$adminmail.'');
			$this->SwiftMailer->addTo('from','support@therewardsteam.com');
			if(!$this->SwiftMailer->sendView('Redeemed Product Details', 'redeem_points_admin_mail', 'html')){
				echo "Mail not sent.";
				die(debug($this->SwiftMailer->errors()));
			}
		}

		//sending mail to user who is redeemed the product
		$usermail = $redeem_user['User']['email'];
		if($this->SwiftMailer->connect()) {
			$this->SwiftMailer->addTo('to', ''.$usermail.'');
			$this->SwiftMailer->addTo('from','support@therewardsteam.com');
			if(!$this->SwiftMailer->sendView('Redeemed Product Details', 'redeem_points_user_mail', 'html')){
				echo "Mail not sent.";
				die(debug($this->SwiftMailer->errors()));
			}
		}

		$this->Session->setFlash('This Cart has been Redeemed');
		$this->redirect('/carts/cart_info/');

	}

	function address_details($id)
	{
		$this->check_query_numeric($id);
		$this->layout = 'ajax';
		$sql = "SELECT * FROM cart_addresses where id=".$id;
		$user_address=$this->Cart->query($sql);
		echo json_encode($user_address[0]['cart_addresses']);
		die;
	}

	function cart_info()
	{
		$userId =$this->Session->read('user.User.id');
		$sql = "";
		$sql.= "SELECT * FROM carts as Cart, cart_items as CartItem, cart_products as CartProduct, cart_addresses as CartAddress,products as Product
		where Cart.id = CartItem.cart_id and Cart.address_id = CartAddress.id and Product.id=CartItem.product_id and CartItem.product_type=CartProduct.product_type and
		CartItem.product_id=CartProduct.product_id and (CartItem.cart_status='pending' or  CartItem.cart_status='Paid'or CartItem.cart_status='Shipped' or CartItem.cart_status='completed')
		AND Cart.user_id=".$userId;

		if($this->Session->read('dashboardidinsession') == 1)
			$sql.= " AND CartItem.product_type='pay'";
		if($this->Session->read('dashboardidinsession') == 3)
			$sql.= " AND CartItem.product_type='points'";

		$sql.=" ORDER BY CartItem.order_id desc";
		if($this->Session->read('dashboardidinsession') == 1)
			$sql.=" ,CartProduct.product_shipping_type, CartProduct.product_shipping_cost desc";
		$cartlist = $this->Cart->query($sql);
		$this->set('cartlist',$cartlist);

		if($this->Session->read('dashboardidinsession') == 1)
		{
			$sql = "SELECT count(CartProduct.product_id) as count, CartProduct.product_shipping_type, CartItem.smart_shipping_cost
			FROM carts as Cart, cart_products as CartProduct, cart_items as CartItem, products as Product
			WHERE Cart.cart_status='New' And Cart.id = CartItem.cart_id AND CartItem.product_id=CartProduct.product_id AND Product.id=CartItem.product_id
			AND CartItem.cart_status is Null AND CartItem.product_type=CartProduct.product_type
			and (CartProduct.product_quantity > 0 or CartProduct.product_quantity=-1) AND Cart.user_id=".$userId." and CartProduct.product_shipping_type=4";

			$count = $this->CartProduct->query($sql);
			$this->set('counts',$count);
		}
	}


	function order_show($id,$Productid)
	{
		$this->check_query_numeric($id,$Productid);
		$userId=$this->Session->read('user.User.id');
		$sql = "SELECT * FROM carts as Cart, cart_items as CartItem, cart_products as CartProduct, cart_addresses as CartAddress, products as Product
		where Cart.id = CartItem.cart_id and Cart.address_id = CartAddress.id and Product.id=CartProduct.product_id and CartItem.product_id=CartProduct.product_id
		and CartItem.product_type=CartProduct.product_type AND CartItem.product_id=$Productid and CartItem.id =".$id;

		$cartlist = $this->Cart->query($sql);
		$this->set('cartlist',$cartlist);

		if(($cartlist[0]['CartProduct']['product_type']=='Pay') && ($cartlist[0]['CartProduct']['product_shipping_type'] == 3))
		{
			$sql = "SELECT CartProduct.product_shipping_cost FROM carts as Cart, cart_items as CartItem, cart_products as CartProduct
			where Cart.id = CartItem.cart_id and CartItem.product_id=CartProduct.product_id and CartItem.product_type=CartProduct.product_type and CartProduct.product_type='Pay'
			AND CartProduct.product_shipping_type = 3 and Cart.user_id=".$userId." and CartItem.order_id=".$cartlist[0]['CartItem']['order_id'];

			$list = $this->Cart->query($sql);
			$listarray = array();
			foreach($list as $lists)
			{
				array_push($listarray,$lists['CartProduct']['product_shipping_cost']);
			}
			$shippingcost = '';
			if(count($list)>1)
			{
				if($cartlist[0]['CartProduct']['product_shipping_cost']>=max($listarray))
				{
					$shippingcost = $cartlist[0]['CartProduct']['product_shipping_cost'];
				}
				else
				{
					$shippingcost = "Free";
				}
			}
			else
			{
				$shippingcost = $cartlist[0]['CartProduct']['product_shipping_cost'];
			}
			$this->set('shipping_cost',$shippingcost);
		}

		$this->layout = 'admin';

	}

	function cart_show_order($id,$Productid)
	{
		$this->check_query_numeric($id,$Productid);
		$userId=$this->Session->read('user.User.id');
		$sql = "SELECT * FROM carts as Cart, cart_items as CartItem, cart_products as CartProduct, cart_addresses as CartAddress, products as Product
		where Cart.id = CartItem.cart_id and Cart.address_id = CartAddress.id and Product.id=CartProduct.product_id and CartItem.product_id=CartProduct.product_id
		and CartItem.product_type=CartProduct.product_type AND CartItem.product_id=$Productid and CartItem.id =".$id;

		$cartlist = $this->Cart->query($sql);
		$this->set('cartlist',$cartlist);

		if(($cartlist[0]['CartProduct']['product_type']=='Pay') && ($cartlist[0]['CartProduct']['product_shipping_type'] == 3))
		{
			$sql = "SELECT CartProduct.product_shipping_cost FROM carts as Cart, cart_items as CartItem, cart_products as CartProduct
			where Cart.id = CartItem.cart_id and CartItem.product_id=CartProduct.product_id and CartItem.product_type=CartProduct.product_type and CartProduct.product_type='Pay'
			AND CartProduct.product_shipping_type = 3 and Cart.user_id=".$userId." and CartItem.order_id=".$cartlist[0]['CartItem']['order_id'];

			$list = $this->Cart->query($sql);
			$listarray = array();
			foreach($list as $lists)
			{
				array_push($listarray,$lists['CartProduct']['product_shipping_cost']);
			}
			$shippingcost = '';
			if(count($list)>1)
			{
				if($cartlist[0]['CartProduct']['product_shipping_cost']>=max($listarray))
				{
					$shippingcost = $cartlist[0]['CartProduct']['product_shipping_cost'];
				}
				else
				{
					$shippingcost = "Free";
				}
			}
			else
			{
				$shippingcost = $cartlist[0]['CartProduct']['product_shipping_cost'];
			}
			$this->set('shipping_cost',$shippingcost);
		}

	}

	function delete($id)
	{
		$this->CartItem->delete($id);
		$this->Session->setFlash('Cart is successfully deleted.');
		$this->redirect(Controller::referer());
	}

	function admin_index() {

		// Search Results
		if(isset($this->params['url']['data']))
		{
			$product= $this->params['url']['data'];
			$sql = "SELECT * FROM products as Product , cart_products as CartProduct where CartProduct.product_id=Product.id
			and CartProduct.product_type='Pay' and (CartProduct.product_id='".$product['Product']['search']."' OR Product.name LIKE '%".$product['Product']['search']."%')";
			$searchproducts=$this->Cart->query($sql);
			$this->set("searchproducts",$searchproducts);
		}else{
			$this->paginate = array('Cart' => array('limit' => 20));
			$products = $this->paginate('Cart', array('search' =>array('sql' => $sql)));
			$this->set("products",$products);
		}

		// New orders
		$sql = "SELECT * FROM carts as Cart, cart_items as CartItem, cart_products as CartProduct, cart_addresses as CartAddress, products as Product
		where Cart.id = CartItem.cart_id and Cart.address_id = CartAddress.id and Product.id=CartProduct.product_id and CartItem.product_id=CartProduct.product_id
		AND CartItem.product_type='Pay' AND CartItem.product_type=CartProduct.product_type and (CartItem.cart_status='Pending' or CartItem.cart_status='Paid')
		order by CartItem.order_id desc";
		$cart=$this->Cart->query($sql);
		$this->set("carts",$cart);

		//Shipped orders
		$sql = "SELECT * FROM carts as Cart, cart_items as CartItem, cart_products as CartProduct, cart_addresses as CartAddress,products as Product
		where Cart.id = CartItem.cart_id and Cart.address_id = CartAddress.id and Product.id=CartProduct.product_id and CartItem.product_id=CartProduct.product_id
		AND CartItem.product_type='Pay' AND CartItem.product_type=CartProduct.product_type and CartItem.cart_status='Shipped' order by CartItem.order_id desc";
		$shipped_cart=$this->Cart->query($sql);
		//print_r($shipped_cart);
		$this->set("shipped_cart",$shipped_cart);

		//Get total count of products of an order for Shipped orders to send mail
		$shipped_total_count_sql = "SELECT CartItem.order_id, count(CartItem.order_id) as count1 FROM carts as Cart, cart_items as CartItem, cart_products as CartProduct, cart_addresses as CartAddress,products as Product
		where Cart.id = CartItem.cart_id and Cart.address_id = CartAddress.id and Product.id=CartProduct.product_id and CartItem.product_id=CartProduct.product_id
		AND CartItem.product_type='Pay' AND CartItem.product_type=CartProduct.product_type and CartItem.cart_status='Shipped'
		and CartItem.mail_sent=0 group by CartItem.order_id desc";
		$shipped_total_count=$this->Cart->query($shipped_total_count_sql);
		$this->set("shipped_total_count",$shipped_total_count);

		//Get total count of products of an order to send mail
		$total_count_sql = "SELECT CartItem.order_id, count(CartItem.order_id) as count2 FROM carts as Cart, cart_items as CartItem, cart_products as CartProduct, cart_addresses as CartAddress,products as Product
		where Cart.id = CartItem.cart_id and Cart.address_id = CartAddress.id and Product.id=CartProduct.product_id and CartItem.product_id=CartProduct.product_id
		AND CartItem.product_type='Pay' AND CartItem.product_type=CartProduct.product_type and CartItem.mail_sent=0 group by CartItem.order_id";;
		$total_count=$this->Cart->query($total_count_sql);
		$this->set("total_count",$total_count);

		//Completed orders
		$sql = "SELECT * FROM carts as Cart, cart_items as CartItem, cart_products as CartProduct, cart_addresses as CartAddress,products as Product
		where Cart.id = CartItem.cart_id and Cart.address_id = CartAddress.id and Product.id=CartProduct.product_id and CartItem.product_id=CartProduct.product_id
		AND CartItem.product_type='Pay' AND CartItem.cart_status='Completed' order by CartItem.order_id desc";
		$complted_cart=$this->Cart->query($sql);
		$this->set("complted_cart",$complted_cart);
	}

	function update_order_status($id=null,$productid=null)
	{
		$this->check_query_numeric($id,$productid);
		if (!empty($this->request->data))
		{

			$date=$data['created'] = date('Y-m-d H:m:s', strtotime('+1 week'));
			$shipping_date=date('Y-m-d H:m:s');
			if( (date("d") != $this->request->data['CartItem']['completed_date']['day']))
			{
				$completed_date= $this->request->data['CartItem']['completed_date']['year']."-".$this->request->data['CartItem']['completed_date']['month']."-".$this->request->data['CartItem']['completed_date']['day'];
			}
			else
			{
				$completed_date=$this->request->data['CartItem']['completed_date']=$date;
			}
			$courier_name=$this->request->data['CartItem']['courier_name'];
			$courier_number=$this->request->data['CartItem']['courier_number'];
			$tracking_link=$this->request->data['CartItem']['tracking_link'];
			$status="Shipped";
			$courier_name=$this->request->data['CartItem']['courier_name'];
			$product_id=$_POST['productid'];
			$cart_id=$_POST['cartid'];

			$sql = "UPDATE cart_items set cart_status ='$status',shipping_date='$shipping_date',completed_date='$completed_date',courier_name='$courier_name',courier_number='$courier_number',tracking_link='$tracking_link'
			where id=$cart_id and product_id=$product_id ";

			$this->CartItem->query($sql);

			$sql = "SELECT * FROM carts as Cart, cart_items as CartItem, cart_products as CartProduct, cart_addresses as CartAddress,products as Product
			where Cart.id = CartItem.cart_id and Cart.address_id = CartAddress.id and Product.id=CartItem.product_id and CartItem.cart_status='Shipped'
			and CartItem.product_id=$product_id and CartItem.product_id=CartProduct.product_id AND CartItem.id =".$cart_id;

			$cartlist = $this->Cart->query($sql);

			$this->set('cartlist',$cartlist);

			$total=($cartlist[0]['CartItem']['product_qty']* ($cartlist[0]['CartProduct']['product_price'] + $cartlist[0]['CartProduct']['product_shipping_cost']));
			$this->set('total',$total);

			//order ID
			$this->set('orderID',$id);

			/*$tomail=$cartlist[0]['CartAddress']['email'];

			$this->SwiftMailer->layout = 'shipping_mail';
			if($this->SwiftMailer->connect()) {
			$this->SwiftMailer->addTo('to', ''.$tomail.'');
			$this->SwiftMailer->addTo('from','support@therewardsteam.com');
			if(!$this->SwiftMailer->sendView('Myrewards: One of your order '.$cartlist[0]['CartItem']['order_id'].' has been shipped', 'shipping_mail', 'html')){
			echo "Mail not sent. Errors: ";
			die(debug($this->SwiftMailer->errors()));
			}
			} else {
			echo "The mailer failed to connect. Errors: ";
			die(debug($this->SwiftMailer->errors()));
			}$this->Session->setFlash('The product status is successfully updated!! and sent mail');*/

			$this->Session->setFlash('The product status is successfully updated!!');

			if($cartlist[0]['CartItem']['product_type'] == 'Points')
			{
				$this->redirect('/admin/points/');
			}
			elseif($cartlist[0]['CartItem']['product_type'] == 'Pay')
			{
				$this->redirect('/admin/carts/');
			}
			//$this->redirect($_SERVER["HTTP_REFERER"]);
			//echo "<p style='padding:30px';><strong>The product status is successfully updated!! and sent mail.</strong></p>";
		}else {
			$this->set('id',$id);
			$this->set('productid',$productid);
		}
		$this->layout = 'admin';
	}

	function update_shipping_status($id,$productid,$modify)
	{
		$this->check_query_numeric($id,$productid);
		$sql = "UPDATE cart_items set cart_status ='Completed',completed_date=now() where id=$id and product_id=$productid";
		$this->Cart->query($sql);
		$this->Session->setFlash('The product status is updated.');

	}

	function admin_edit($id=null) {
		$this->check_query_numeric_admin($id);
		if (!empty($this->request->data)) {
			$this->request->data['CartProduct']['product_id']=current(explode(" ", $this->request->data['CartProduct']['product_id']));
			$prdt = $this->CartProduct->find('first', array('conditions' => array('product_id'=>$this->request->data['CartProduct']['product_id'],'product_type'=>'Pay')));
			if(empty($prdt))
			{
				$msg = "";
				if (isset($this->request->data['CartProduct']['id']) && $this->request->data['CartProduct']['id']>0)
					$msg = "updated";
				elseif (isset($this->request->data['CartProduct']['id']))
				$this->request->data['CartProduct']['id'] = 0;
				$this->request->data['CartProduct']['product_type'] = 'Pay';
				if($this->request->data['CartProduct']['product_shipping_type'] == 4)
				{
					$this->request->data['CartProduct']['product_shipping_cost'] = 0;
				}
				if(empty($this->request->data['CartProduct']['product_quantity'])) {
					unset($this->request->data['CartProduct']['product_quantity']);
				}
				$this->CartProduct->save($this->request->data);

				if ($msg == 'updated') {
					$this->Session->setFlash('The product is successfully updated!!');
				} else {
					$this->Session->setFlash('The product is successfully added!!');
				}
				$this->redirect('/admin/carts');
			}else{
				if($this->request->data['CartProduct']['product_shipping_type'] == 4)
				{
					$this->request->data['CartProduct']['product_shipping_cost'] = 0;
				}
				if(empty($this->request->data['CartProduct']['product_quantity'])) {
					unset($this->request->data['CartProduct']['product_quantity']);
				}
				$this->request->data['CartProduct']['id'] = $prdt["CartProduct"]["id"];
				$this->CartProduct->save($this->request->data);
				$this->Session->setFlash('The product is successfully updated!!');
				$this->redirect('/admin/carts');
			}
		}else {
			$this->request->data = $this->CartProduct->read(null, $id);
		}

		$shipping_cats = $this->ShippingCategory->find('list');
		$this->set('stype',$shipping_cats);
	}

	function admin_delete($id = null) {
		$this->check_query_numeric_admin($id);
		if (!$id) {
			$this->Session->setFlash('Invalid id for Product');
			$this->redirect(array('action'=>'index'), null, true);
		}
		if ($this->CartProduct->delete($id)) {
			$this->Session->setFlash('Product #'.$id.' deleted');
			$this->redirect(array('action'=>'index'), null, true);
		}
	}

	/*function order_invoice($cartitemid,$productid)
	 {
	$sql = "SELECT * FROM carts as Cart, cart_items as CartItem, cart_products as CartProduct, cart_addresses as CartAddress,products as Product
	where Cart.id = CartItem.cart_id and Cart.address_id = CartAddress.id and CartItem.product_id=Product.id and CartItem.product_id=CartProduct.product_id
	and CartItem.product_type=CartProduct.product_type AND CartItem.product_id=$productid and CartItem.id =".$cartitemid;

	$cartlist = $this->Cart->query($sql);
	$this->set('cartlist',$cartlist);
	$total=($cartlist[0]['CartItem']['product_qty']* ($cartlist[0]['CartProduct']['product_price'] + $cartlist[0]['CartProduct']['product_shipping_cost']));
	$this->set('total',$total);
	}*/

	function order_invoice($cartid, $orderId)
	{
		$this->check_query_numeric($cartid);
		$sql = "";
		$sql.= "SELECT * FROM carts as Cart, cart_items as CartItem, cart_products as CartProduct, cart_addresses as CartAddress,products as Product
		where Cart.id = CartItem.cart_id and Cart.address_id = CartAddress.id and CartItem.product_id=Product.id and CartItem.product_id=CartProduct.product_id
		and CartItem.product_type=CartProduct.product_type AND CartItem.cart_id=".$cartid." and CartItem.order_id=".$orderId;

		if($this->Session->read('dashboardidinsession') == 1)
			$sql.= " AND CartItem.product_type='pay'";
		if($this->Session->read('dashboardidinsession') == 3)
			$sql.= " AND CartItem.product_type='points'";

		$cartlist = $this->Cart->query($sql);
		$this->set('cartlist',$cartlist);

		if($this->Session->read('dashboardidinsession') == 1)
		{
			//max shipping cost for shipping type as Group Shipping
			$gsc_sql = "SELECT max(CartProduct.product_shipping_cost) as max_group_ship_cost FROM carts as Cart, cart_items as CartItem, cart_products as CartProduct, cart_addresses as CartAddress,products as Product
			where Cart.id = CartItem.cart_id and Cart.address_id = CartAddress.id and CartItem.product_id=Product.id and CartItem.product_id=CartProduct.product_id
			and CartItem.product_type=CartProduct.product_type AND CartItem.cart_id=".$cartid." and CartItem.order_id=".$orderId."
			and CartProduct.product_shipping_type=3 and CartProduct.product_type='Pay'";

			$group_shipping_cost = $this->Cart->query($gsc_sql);

			$this->set('max_group_ship_cost',$group_shipping_cost[0][0]['max_group_ship_cost']);

			$sql = "SELECT count(CartProduct.product_id) as count, CartProduct.product_shipping_type, CartItem.smart_shipping_cost
			FROM carts as Cart, cart_items as CartItem, cart_products as CartProduct, cart_addresses as CartAddress,products as Product
			where Cart.id = CartItem.cart_id and Cart.address_id = CartAddress.id and CartItem.product_id=Product.id
			and CartItem.product_id=CartProduct.product_id and CartItem.product_type=CartProduct.product_type AND CartItem.cart_id=".$cartid."
			and CartItem.order_id=".$orderId." and CartProduct.product_shipping_type=4";

			$count = $this->CartProduct->query($sql);
			$this->set('counts',$count);
		}
	}

	function admin_xml_products($limit = 10) {

		$this->layout = 'ajax';
		if (isset($_GET['term'])) {
			$conditions = array("Product.active = 1 AND (Product.expires = '0000-00-00' OR Product.expires >= NOW()) AND Product.id LIKE '".$_GET['term']. "%'");
		}

		$data = $this->Product->find('all', array('fields'=>'Product.id, Product.name', 'conditions'=>$conditions, 'order'=>'Product.name', 'limit'=>$limit));

		$this->set('products', $data);

	}

	function check_query_numeric($id, $id1=null, $id2=null, $id3=null, $id4=null)
	{
		if((!empty($id) && !is_numeric($id)) || (!empty($id) && is_numeric($id) && $id <0)) {
			$this->redirect('/');
		}elseif((!empty($id1) && !is_numeric($id1)) || (!empty($id1) && is_numeric($id1) && $id1 <0)) {
			$this->redirect('/');
		}elseif(!empty($id2) && !is_numeric($id2)) {
			$this->redirect('/');
		}elseif(!empty($id3) && !is_numeric($id3)) {
			$this->redirect('/');
		}elseif(!empty($id4) && !is_numeric($id4)) {
			$this->redirect('/');
		}
	}

	function check_query_numeric_admin($id)
	{
		if((!empty($id) && !is_numeric($id)) || (!empty($id) && is_numeric($id) && $id <0)) {
			$this->redirect('/admin');
		}
	}

	function send_shipping_status_mail($orderid)
	{
		$this->autoRender=false;
		$this->layout='ajax';
		$sql = "SELECT * FROM carts as Cart, cart_items as CartItem, cart_products as CartProduct, cart_addresses as CartAddress,products as Product
		where Cart.id = CartItem.cart_id and Cart.address_id = CartAddress.id and Product.id=CartItem.product_id and CartItem.product_id=CartProduct.product_id
		and CartItem.product_type=CartProduct.product_type AND CartItem.order_id =".$orderid;

		$cartlist = $this->Cart->query($sql);
		$this->set('cartlist',$cartlist);

		$update = "UPDATE carts as Cart, cart_items as CartItem, cart_products as CartProduct set CartItem.mail_sent=1
		where Cart.id = CartItem.cart_id and CartItem.product_id=CartProduct.product_id and CartItem.product_type=CartProduct.product_type AND CartItem.order_id=".$orderid;
		$this->CartItem->query($update);

		/*$this->set('domain',$this->Session->read('client.Domain.name'));
		 $this->set('clientid',$this->Session->read('client.Client.id'));
		$this->set('clientname',$this->Session->read('client.Client.name'));
		$this->set('extn',$this->Session->read('client.Client.client_logo_extension'));*/

		$userClient = $this->User->find('first',array('conditions'=>array('User.id'=>$cartlist[0]['CartItem']['user_id']),'recursive'=>-1));

		$this->Client->unbindModel(array('belongsTo' => array('Program','Voucher')));
		$list = $this->Client->find('first',array('conditions'=>array('Client.id'=>$userClient['User']['client_id']),'recursive'=>0));

		$this->set('domain',$list['Domain']['name']);
		$this->set('clientid',$list['Client']['id']);
		$this->set('clientname',$list['Client']['name']);
		$this->set('extn',$list['Client']['client_logo_extension']);

		$tomail=$cartlist[0]['CartAddress']['email'];

		$this->SwiftMailer->layout = 'shipping_mail';
		if($this->SwiftMailer->connect()) {
			$this->SwiftMailer->addTo('to', ''.$tomail.'');
			$this->SwiftMailer->addTo('from','support@therewardsteam.com');
			if(!$this->SwiftMailer->sendView('Your order '.$cartlist[0]['CartItem']['order_id'].' has been shipped', 'shipping_mail', 'html')){
				echo "Mail not sent. Errors: ";
				die(debug($this->SwiftMailer->errors()));
			}
		} else {
			echo "The mailer failed to connect. Errors: ";
			die(debug($this->SwiftMailer->errors()));
		}

	}
}
?>