/**
 * Some method description.
 *
 * @param Model $Model The model to use.
 * @param array $array Some array value.
 * @param callable $callback Some callback.
 * @param boolean $boolean Some boolean value.
 */

<?php
class Advt4FreeController extends AppController {

	var $components = array('Cookie', 'SwiftMailer', 'MailQueue','Csv','RequestHandler');
	var $uses = array('Advt4FreeSearch','Merchant','MerchantAddress','Product','ProductsAddress','Category','CategoriesProduct','Advt4Free','Country');


	function admin_index()
	{
		$conditions = array();

		$conditions[] = "`Advt4Free`.`merchant_approved` = 'YES'";

		$this->paginate = array('Advt4Free' => array('limit' => 30));
		$this->set('advt4free', $this->paginate('Advt4Free',$conditions));

		$countries = $this->Country->find('list', array('fields' => 'Country.name, Country.name'));
		$this->set('countries',$countries);
	}

	function admin_view($id) {

		$edit = $this->Advt4Free->findById($id);
		$this->set('edit', $edit);
	}

	function admin_approve($id) {

		$created = date("Y-m-d H:i:s");


		$approve = $this->Advt4Free->findById($id);
		if (strstr($approve['Advt4Free']['business_category'] ,"Web Coupon.")) {
	    	$webcoupon='1';
		}
		else
		{
			 $webcoupon='0';
		}
		$this->set('new_user',$approve);

					##### Merchants Table ###################
					$this->request->data['Merchant']['name'] = $approve['Advt4Free']['company_name'];
					$this->request->data['Merchant']['contact_first_name'] =$approve['Advt4Free']['contact_name'];
					$this->request->data['Merchant']['contact_position'] =$approve['Advt4Free']['position'];
					$this->request->data['Merchant']['mail_address1'] = $approve['Advt4Free']['postal_address'];
					$this->request->data['Merchant']['mail_suburb'] = $approve['Advt4Free']['suburb'];
					$this->request->data['Merchant']['mail_state'] = $approve['Advt4Free']['state'];
					$this->request->data['Merchant']['mail_postcode'] = $approve['Advt4Free']['p_code'];
					$this->request->data['Merchant']['mail_country'] = $approve['Advt4Free']['country'];
					$this->request->data['Merchant']['email'] = $approve['Advt4Free']['email'];
					$this->request->data['Merchant']['phone'] = $approve['Advt4Free']['phone'];
					$this->request->data['Merchant']['mobile'] = $approve['Advt4Free']['mobile'];
					$this->request->data['Merchant']['logo_extension'] = $approve['Advt4Free']['logo_extension'];
					$this->request->data['Merchant']['created'] = $created;
					$this->Merchant->save($this->request->data);

					$merchantid = $this->Merchant->getLastInsertId();

					$logo=$merchantid.'.'.$approve['Advt4Free']['logo_extension'];
					$newname="/files/merchant_logo/$logo";
					$mlogo=$approve['Advt4Free']['company_name'].'.'.$approve['Advt4Free']['logo_extension'];
		            copy("advt4free/logos/$mlogo", ".$newname");

					#################### MerchantAddress ################
		            $this->request->data['MerchantAddress']['merchant_id'] = $merchantid;
					$this->request->data['MerchantAddress']['name'] = $approve['Advt4Free']['trading_name'];
					$this->request->data['MerchantAddress']['address1'] = $approve['Advt4Free']['trading_address'];
					$this->request->data['MerchantAddress']['suburb'] = $approve['Advt4Free']['suburb2'];
					$this->request->data['MerchantAddress']['state'] = $approve['Advt4Free']['state2'];
					$this->request->data['MerchantAddress']['country'] =$approve['Advt4Free']['country'];
					$this->request->data['MerchantAddress']['postcode'] = $approve['Advt4Free']['p_code2'];
					$this->request->data['MerchantAddress']['phone'] = $approve['Advt4Free']['trading_phone'];
					$this->request->data['MerchantAddress']['created'] = $created;
					$this->MerchantAddress->save($this->request->data);

					#################### Product Table ################

					if(!empty($approve['Advt4Free']['offer_details']))
					{
						$highlight =$approve['Advt4Free']['offer_details'];
					}
					else
					{
						$highlight =$approve['Advt4Free']['discount_offer3'].'% off';
					}

					$prod_sql = "INSERT INTO products (merchant_id,name,details,highlight,text,terms_and_conditions,link1,created,
					web_coupon,offer,modified) VALUES (".$merchantid.",'".addslashes($approve['Advt4Free']['trading_name'])."',
					'".addslashes($approve['Advt4Free']['offer_details2'])."','".addslashes($highlight)."','".addslashes($approve['Advt4Free']['about_business'])."',
					'".addslashes($approve['Advt4Free']['terms_conditions'])."','".$approve['Advt4Free']['web']."','".$created."',".$webcoupon.",
					'".addslashes($approve['Advt4Free']['coupondetails'])."','')";
					$this->Product->query($prod_sql);


					#################### Product Addresses table ################

					$productid = $this->Product->findByMerchantId($merchantid);
					$merchantadderssid = $this->MerchantAddress->findByMerchantId($merchantid);

					$sql = "INSERT INTO products_addresses (product_id,merchant_address_id) VALUES (".$productid['Product']['id'].",".$merchantadderssid['MerchantAddress']['id'].")";
					$this->ProductsAddress->query($sql);

					#################### Product Tracker table ################
					$this->loadModel("ProductsTracker");
					$sql = "INSERT INTO products_trackers(`product_id`,`clicks`,`prints`) VALUES (".$productid['Product']['id'].",0,0 )";
					$this->ProductsTracker->query($sql);

					#################### Getting Category ID ################

					$CATID =$this->Category->query("SELECT id FROM categories AS Category where name='".$approve['Advt4Free']['category']."' and countries='".$approve['Advt4Free']['country']."'");

					#################### CategoriesProduct table ################

					$this->request->data['CategoriesProduct']['category_id'] = $CATID[0]['Category']['id'];
					$this->request->data['CategoriesProduct']['product_id'] = $productid['Product']['id'];
					$this->CategoriesProduct->save($this->request->data);

					########################### Sending conformation mail to merchant #####################

				if( $approve['Advt4Free']['email'] !='') {

							$this->SwiftMailer->layout = 'merchant_invite';
							if($this->SwiftMailer->connect()) {
								$this->SwiftMailer->addTo('to', $approve['Advt4Free']['email']);
								$this->SwiftMailer->addTo('from','noreply@therewardsteam.com');
								if(!$this->SwiftMailer->sendView('Invitation from My Rewards', 'merchant_invite', 'html')){
									echo "Mail not sent. Errors: ";
									die(debug($this->SwiftMailer->errors()));
								}
							} else {
								echo "The mailer failed to connect. Errors: ";
								die(debug($this->SwiftMailer->errors()));
							}
						$this->Session->setFlash('Your invitation has been sent to  '. $approve['Advt4Free']['email'] .'');
						$this->Advt4Free->updateAll(array('approved'=>1,'admin_modified'=>"'".$created."'"), array('Advt4Free.id'=>$id));
					    $this->redirect($_SERVER["HTTP_REFERER"]);
					}
					$this->autoRender=false;
	}

	function admin_reject($id) {
		$created = date("Y-m-d H:i:s");
		$approve = $this->Advt4Free->findById($id);

		$this->set('new_user',$approve);

		################ Sending rejected mail ##################################

		if( $approve['Advt4Free']['email'] !='') {

							$this->SwiftMailer->layout = 'reject_invite';
							if($this->SwiftMailer->connect()) {
								$this->SwiftMailer->addTo('to', $approve['Advt4Free']['email']);
								$this->SwiftMailer->addTo('from','noreply@myrewards.com.au');
								if(!$this->SwiftMailer->sendView('Notice from My Rewards', 'reject_invite', 'html')){
									echo "Mail not sent. Errors: ";
									die(debug($this->SwiftMailer->errors()));
								}
							} else {
								echo "The mailer failed to connect. Errors: ";
								die(debug($this->SwiftMailer->errors()));
							}
						$this->Session->setFlash('Rejected Mail has been sent to  '. $approve['Advt4Free']['email'] .'');
						$this->Advt4Free->updateAll(array('approved'=>2,'admin_modified'=>"'".$created."'"), array('Advt4Free.id'=>$id));
					    $this->redirect($_SERVER["HTTP_REFERER"]);

					}


	}
	function admin_delete($id)
	{
		if ($this->Advt4Free->delete($id))
		{
			$this->Session->setFlash('Merchant deleted');
			$this->redirect($_SERVER["HTTP_REFERER"]);
		}
	}

	function admin_merchants_not_approved()
	{
		$conditions = array();

		$conditions[] = "`Advt4Free`.`merchant_approved` = 'NO'";

		$this->paginate = array('Advt4Free' => array('limit' => 30));

		$this->set('advt4free', $this->paginate('Advt4Free',$conditions));
		$countries = $this->Country->find('list', array('fields' => 'Country.name, Country.name'));
		$this->set('countries',$countries);
	}

	function admin_merchant_approve($advtid)
	{
		$sql = "UPDATE advt4_frees as Advt4Free set Advt4Free.merchant_approved = 'YES', merchant_modified='".date('Y-m-d h:i:s')."' WHERE Advt4Free.id=".$advtid;
		$this->Advt4Free->query($sql);

		$this->redirect('/admin/advt_4_free/');
	}

	function admin_update_states()
	{
		$selected_country= $this->request->data['Advt4Free']['country'];
		$defaultstatesql_query ="SELECT State.state_cd, State.state_name FROM states AS State, countries as Country
		where State.country_cd = Country.id and Country.name='".$selected_country."'";

		$this->loadModel("State");
		$cstates_data = $this->State->query($defaultstatesql_query);

		$ctrystate_data = array();
		foreach ($cstates_data as $ctrystates_data) {
			$ctrystate_data[$ctrystates_data['State']['state_cd']] = $ctrystates_data['State']['state_name'];
		}

		$this->set('states',$ctrystate_data);
		$this->layout = 'ajax';
	}

	function admin_search($country,$state)
	{
		$sql1 = " where 1=1 ";
		if($country<>'ctry=')
		{
			$country = substr($country,5);
			$sql1.= " and Advt4Free.country='".$country."' ";
		}

		if($state<>'st=')
		{
			$state = substr($state,3);
			$sql1.= " and Advt4Free.state='".$state."' ";
		}
		$sql1.=" and Advt4Free.merchant_approved='YES'";
		$this->paginate = array('Advt4FreeSearch' => array('limit' => 30));
		$advt4frees = $this->paginate('Advt4FreeSearch', array('search' =>array('sql' => $sql1)));

		if($country == 'ctry=')
			$country = '';
		if($state == 'st=')
			$state = '';

		$this->set('advt4free',$advt4frees);
		$countries = $this->Country->find('list', array('fields' => 'Country.name, Country.name'));
		$this->set('countries',$countries);
		$this->set('country',$country);

		$query ="SELECT State.state_cd, State.state_name FROM states AS State, countries as Country
		where State.country_cd = Country.id and Country.name='".$country."'";

		$this->loadModel("State");
		$cstates_data = $this->State->query($query);

		$ctrystate_data = array();
		foreach ($cstates_data as $ctrystates_data) {
			$ctrystate_data[$ctrystates_data['State']['state_cd']] = $ctrystates_data['State']['state_name'];
		}

		$this->set('states',$ctrystate_data);
		$this->set('state',$state);
	}

	function admin_search_not_approved($country,$state)
	{
		$sql1 = " where 1=1 ";
		if($country<>'ctry=')
		{
			$country = substr($country,5);
			$sql1.= " and Advt4Free.country='".$country."' ";
		}

		if($state<>'st=')
		{
			$state = substr($state,3);
			$sql1.= " and Advt4Free.state='".$state."' ";
		}
		$sql1.=" and Advt4Free.merchant_approved='NO'";
		$this->paginate = array('Advt4FreeSearch' => array('limit' => 30));
		$advt4frees = $this->paginate('Advt4FreeSearch', array('search' =>array('sql' => $sql1)));

		if($country == 'ctry=')
			$country = '';
		if($state == 'st=')
			$state = '';

		$this->set('advt4free',$advt4frees);
		$countries = $this->Country->find('list', array('fields' => 'Country.name, Country.name'));
		$this->set('countries',$countries);
		$this->set('country',$country);

		$query ="SELECT State.state_cd, State.state_name FROM states AS State, countries as Country
		where State.country_cd = Country.id and Country.name='".$country."'";

		$this->loadModel("State");
		$cstates_data = $this->State->query($query);

		$ctrystate_data = array();
		foreach ($cstates_data as $ctrystates_data) {
			$ctrystate_data[$ctrystates_data['State']['state_cd']] = $ctrystates_data['State']['state_name'];
		}

		$this->set('states',$ctrystate_data);
		$this->set('state',$state);
	}
}
?>