<?php
class ProductsController extends AppController {

	public $uses = array('Product', 'Suburb', 'Postcode', 'Neighbour', 'ProductList', 'ProductList2', 'Wishlist', 'User', 'Detail', 'RedeemTracker',
			'User', 'Client', 'ProductDeal', 'ProductsPoint', 'ProductsSaf', 'CartProduct','Track');
	public $components = array('File','RequestHandler','PanaceaSms');

	public $paginate = array('ProductList' => array('limit' => SEARCH_RESULTS_PER_PAGE));



	/**
	* Google map
	**/
	function googlemap($name)
	{
		$name = addslashes($name);
		if (!empty($name))
   		{
	   		$location= explode(",", $name);
	   		$count =  count($location);

	   		$third = trim($location[2]);
	   		$ctry = trim($location[1]);
	   		$add = trim($location[0]);
	   		$count=count($location);

	   		if ($count==1)
	   		{
		   		$sql = "SELECT distinct Product.name, Product.id, Product.highlight, Product.details, MerchantAddress.latitude,
		   		MerchantAddress.longitude, MerchantAddress.id
		   		FROM products AS Product, merchant_addresses AS MerchantAddress, merchants as Merchant , states as State,
		   		clients_products as ClientProduct, clients as Client
		   		where Product.merchant_id = MerchantAddress.merchant_id AND Product.merchant_id = Merchant.id  AND Product.available_national = '0'
		   		and (round(MerchantAddress.latitude)!=0 || round(MerchantAddress.longitude)!=0) AND Product.available_international = '0'";
		   		if(strtolower($add)!='hong kong')
		   		{
		   			$sql.="and MerchantAddress.state=State.state_cd
		   			AND ((MerchantAddress.country= '$add') OR (MerchantAddress.state ='$add' or State.state_name = '$add' or State.state_cd = '$add')
		   			OR (MerchantAddress.suburb = '$add' OR MerchantAddress.city = '$add')) ";
	   			}
	   			else if(strtolower($add)=='hong kong')
	   			{
	   				$sql.="and ((MerchantAddress.country= '$add') OR (MerchantAddress.suburb = '$add' OR MerchantAddress.city = '$add')) ";
	   			}
		   		$sql.="and MerchantAddress.latitude is not null and MerchantAddress.longitude is not null
		   		and Product.active = 1 and Product.cancelled = 0 AND (Product.expires = '0000-00-00' OR Product.expires >= NOW())
		   		and Product.id=ClientProduct.product_id and ClientProduct.client_id='". $this->Session->read('client.Client.id') ."'
		   		and Client.id=ClientProduct.client_id group by MerchantAddress.latitude, MerchantAddress.longitude order by rand() limit 100" ;

				$google_map = $this->Domain->Client->Product->Merchant->MerchantAddress->query($sql);
				$this->set('google_map', array('google_map' => $google_map));
   			}
   			else if ($count==3)
	   		{
	   			$google_map = array();
		   		$sql = "SELECT distinct Product.name, Product.id, Product.highlight, Product.details, MerchantAddress.latitude,
		   		MerchantAddress.longitude, MerchantAddress.id
		   		FROM products AS Product, merchant_addresses AS MerchantAddress, merchants as Merchant , states as State,
		   		clients_products as ClientProduct, clients as Client
		   		where Product.merchant_id = MerchantAddress.merchant_id AND Product.merchant_id = Merchant.id AND
		   		(round(MerchantAddress.latitude)!=0 || round(MerchantAddress.longitude)!=0) and Product.available_national = '0' AND Product.available_international = '0'";
		   		if(strtolower($third)!='hong kong')
		   		{
		   			$sql.="and MerchantAddress.state=State.state_cd
		   			AND ((MerchantAddress.country= '$third') and (MerchantAddress.state ='$ctry' or State.state_name = '$ctry' or State.state_cd = '$ctry')
		   			and (MerchantAddress.suburb = '$add' OR MerchantAddress.city = '$add')) ";
	   			}
	   			else if(strtolower($third)=='hong kong')
	   			{
	   				$sql.="and ((MerchantAddress.country= '$third')
	   				and ((MerchantAddress.suburb = '$add, $ctry' OR MerchantAddress.city = '$add, $ctry')
	   				or (MerchantAddress.suburb = '$add,$ctry' OR MerchantAddress.city = '$add,$ctry')))";
	   			}
		   		$sql.="and MerchantAddress.latitude is not null and MerchantAddress.longitude is not null
		   		and Product.active = 1 and Product.cancelled = 0 AND (Product.expires = '0000-00-00' OR Product.expires >= NOW())
		   		and Product.id=ClientProduct.product_id and ClientProduct.client_id='". $this->Session->read('client.Client.id') ."'
		   		and Client.id=ClientProduct.client_id group by MerchantAddress.latitude, MerchantAddress.longitude order by rand() limit 100" ;

				$google_map = $this->Domain->Client->Product->Merchant->MerchantAddress->query($sql);
				if(empty($google_map))
				{
					$sql = "SELECT distinct Product.name, Product.id, Product.highlight, Product.details, MerchantAddress.latitude,
			   		MerchantAddress.longitude, MerchantAddress.id
			   		FROM products AS Product, merchant_addresses AS MerchantAddress, merchants as Merchant , states as State,
			   		clients_products as ClientProduct, clients as Client
			   		where Product.merchant_id = MerchantAddress.merchant_id AND Product.merchant_id = Merchant.id
			   		and (round(MerchantAddress.latitude)!=0 || round(MerchantAddress.longitude)!=0) AND Product.available_national = '0' AND Product.available_international = '0' ";
			   		if(strtolower($third)!='hong kong')
			   		{
			   			$sql.=" AND ((MerchantAddress.country= '$third') and (MerchantAddress.suburb = '$add, $ctry' OR MerchantAddress.city = '$add, $ctry')) ";
		   			}
		   			else if(strtolower($third)=='hong kong')
		   			{
		   				$sql.="and ((MerchantAddress.country= '$third')
		   				and ((MerchantAddress.suburb = '$add, $ctry' OR MerchantAddress.city = '$add, $ctry')
		   				or (MerchantAddress.suburb = '$add,$ctry' OR MerchantAddress.city = '$add,$ctry')))";
		   			}
			   		$sql.="and MerchantAddress.latitude is not null and MerchantAddress.longitude is not null
			   		and Product.active = 1 and Product.cancelled = 0 AND (Product.expires = '0000-00-00' OR Product.expires >= NOW())
			   		and Product.id=ClientProduct.product_id and ClientProduct.client_id='". $this->Session->read('client.Client.id') ."'
			   		and Client.id=ClientProduct.client_id group by MerchantAddress.latitude, MerchantAddress.longitude order by rand() limit 100" ;

					$google_map = $this->Domain->Client->Product->Merchant->MerchantAddress->query($sql);
				}
				$this->set('google_map', array('google_map' => $google_map));
   			}
	   		else if($count==2)
	   		{
	   			$google_map =array();
		   		$sql = "SELECT distinct Product.name, Product.id, Product.highlight, Product.details, MerchantAddress.latitude,
		   		MerchantAddress.longitude, MerchantAddress.id
		   		FROM products AS Product, merchant_addresses AS MerchantAddress, merchants as Merchant , states as State,
		   		clients_products as ClientProduct, clients as Client
		   		where Product.merchant_id = MerchantAddress.merchant_id AND Product.merchant_id = Merchant.id
		   		and (round(MerchantAddress.latitude)!=0 || round(MerchantAddress.longitude)!=0)
		   		AND Product.available_national = '0' AND Product.available_international = '0' ";
		   		if(strtolower($add)!='hong kong' && strtolower($ctry)!='hong kong')
		   		{
		   			$sql.="and MerchantAddress.state=State.state_cd
		   			AND ((MerchantAddress.country= '$ctry') and (MerchantAddress.state ='$add' or State.state_name = '$add' or State.state_cd = '$add')
		   			or (MerchantAddress.suburb = '$add' or MerchantAddress.city = '$add')) ";
		   		}
		   		else if(strtolower($add)=='hong kong' || strtolower($ctry)=='hong kong')
	   			{
	   				$sql.="and ((MerchantAddress.country= '$ctry')
	   				and ((MerchantAddress.suburb = '$add' OR MerchantAddress.city = '$add')
	   				or (MerchantAddress.suburb = '$ctry' OR MerchantAddress.city = '$ctry')) ";
	   			}
		   		$sql.="and MerchantAddress.latitude is not null and MerchantAddress.longitude is not null and Product.active = 1
		   		and Product.cancelled = 0 AND (Product.expires = '0000-00-00' OR Product.expires >= NOW()) and Product.id=ClientProduct.product_id
		   		and ClientProduct.client_id='". $this->Session->read('client.Client.id') ."' and Client.id=ClientProduct.client_id
		   		group by MerchantAddress.latitude, MerchantAddress.longitude order by rand() limit 100" ;

				$google_map = $this->Domain->Client->Product->Merchant->MerchantAddress->query($sql);

				if(empty($google_map))
				{
					$sql = "SELECT distinct Product.name, Product.id, Product.highlight, Product.details, MerchantAddress.latitude,
					MerchantAddress.longitude, MerchantAddress.id  FROM products AS Product, merchant_addresses AS MerchantAddress, merchants as Merchant ,
					states as State, clients_products as ClientProduct, clients as Client
					where Product.merchant_id = MerchantAddress.merchant_id
					and (round(MerchantAddress.latitude)!=0 || round(MerchantAddress.longitude)!=0)
					AND Product.merchant_id = Merchant.id AND Product.available_national = '0' AND Product.available_international = '0' ";
			   		if(strtolower($add)!='hong kong' && strtolower($ctry)!='hong kong')
			   		{
			   			$sql.=" AND ((MerchantAddress.country= '$ctry') and (MerchantAddress.suburb = '$add' or MerchantAddress.city = '$add')) ";
			   		}
			   		else if(strtolower($add)=='hong kong' || strtolower($ctry)=='hong kong')
		   			{
		   				$sql.="AND ((MerchantAddress.country= '$ctry') or (MerchantAddress.suburb = '$add' or MerchantAddress.city = '$add')) ";;
		   			}
			   		$sql.="and MerchantAddress.latitude is not null and MerchantAddress.longitude is not null and Product.active = 1
			   		and Product.cancelled = 0 AND (Product.expires = '0000-00-00' OR Product.expires >= NOW())
			   		and Product.id=ClientProduct.product_id and ClientProduct.client_id='". $this->Session->read('client.Client.id') ."'
			   		and Client.id=ClientProduct.client_id group by MerchantAddress.latitude, MerchantAddress.longitude order by rand() limit 100" ;
			   		$google_map = $this->Domain->Client->Product->Merchant->MerchantAddress->query($sql);

				}
				$this->set('google_map', array('google_map' => $google_map));
   			}
   			else
   			{
   				$wctry = substr($name,0,strrpos($name,','));
				$suburb1 = substr($name,0,strrpos($wctry,','));

				$lastcountry = $location[$count-1];
	   			$laststate = $location[$count-2];

   				$sql = "SELECT distinct Product.name, Product.id, Product.highlight, Product.details, MerchantAddress.latitude,
		   		MerchantAddress.longitude, MerchantAddress.id
		   		FROM products AS Product, merchant_addresses AS MerchantAddress, merchants as Merchant , states as State,
		   		clients_products as ClientProduct, clients as Client
		   		where Product.merchant_id = MerchantAddress.merchant_id
		   		and (round(MerchantAddress.latitude)!=0 || round(MerchantAddress.longitude)!=0)
		   		AND Product.merchant_id = Merchant.id AND Product.available_national = '0' AND Product.available_international = '0' ";
		   		if(strtolower(trim($lastcountry))!='hong kong')
		   		{
		   			$sql.="and MerchantAddress.state=State.state_cd AND ((MerchantAddress.country= '".trim($lastcountry)."')
		   			and (MerchantAddress.state ='".trim($laststate)."' or State.state_name = '".trim($laststate)."' or State.state_cd = '".trim($laststate)."')
		   			and (MerchantAddress.suburb = '$suburb1' OR MerchantAddress.city = '$suburb1')) ";
	   			}
	   			else if(strtolower(trim($lastcountry))=='hong kong')
	   			{
	   				$sql.="and ((MerchantAddress.country= '".trim($lastcountry)."')
	   				and (MerchantAddress.suburb = '$suburb1' OR MerchantAddress.city = '$suburb1'))";
	   			}
		   		$sql.="and MerchantAddress.latitude is not null and MerchantAddress.longitude is not null
		   		and Product.active = 1 and Product.cancelled = 0 AND (Product.expires = '0000-00-00' OR Product.expires >= NOW())
		   		and Product.id=ClientProduct.product_id and ClientProduct.client_id='". $this->Session->read('client.Client.id') ."'
		   		and Client.id=ClientProduct.client_id group by MerchantAddress.latitude, MerchantAddress.longitude order by rand() limit 100" ;

				$google_map = $this->Domain->Client->Product->Merchant->MerchantAddress->query($sql);
				$this->set('google_map', array('google_map' => $google_map));
   			}
   		}
        $this->layout = 'blank';
	}

	/**
	 * View a product
	 *
	 * @param	int	$id	product id
	 * @param	int	$merchant_address_id	id of the merchant address to display, if null, display the default address, if 'all', display a list of address suburbs
	*/
	function view($id=null,$string, $merchant_address_id=null) {
		$this->check_query_numeric($id, $merchant_address_id);
	    $this->set('keyword',$string);  
		
		if ($id) {

			$dashboardid=$this->Session->read('dashboardidinsession');
			$sql = "SELECT DISTINCT Product.*, MerchantAddress.*, Merchant.* FROM merchant_addresses as MerchantAddress,
			products_addresses as ProductsAddress, merchants as Merchant, products AS Product
			INNER JOIN clients_products AS ClientsProduct ON Product.id = ClientsProduct.product_id
			AND (ClientsProduct.client_id = '". $this->Session->read('client.Client.id'). "' OR ClientsProduct.client_id = 0)
 			WHERE Product.merchant_id = MerchantAddress.merchant_id and ProductsAddress.product_id=Product.id
			AND ProductsAddress.merchant_address_id=MerchantAddress.id and Product.id = '". $id. "' AND Product.active = 1
			and Product.merchant_id= Merchant.id AND (Product.expires = '0000-00-00' OR Product.expires >= NOW()) ";

			if (($this->Session->read('client.Client.blacklisted_product') != '')) {
				$sql .= " AND Product.id not in (". $this->Session->read('client.Client.blacklisted_product'). ") ";
			}
			$sql .= " group by Product.id";

			$products = $this->Product->query($sql);

			$sql="select Product.id from cart_products as CartProduct, products as Product where Product.id=CartProduct.product_id
			AND (CartProduct.product_quantity >0 OR CartProduct.product_quantity =-1) AND CartProduct.product_type='Pay' and CartProduct.active=1 and Product.id=".$id ;
			$cartproduct = $this->Product->query($sql);

			if ($merchant_address_id == '')
			{
				 $merchant_address1 = $products[0]['MerchantAddress']['address1'];
				 $merchant_address2 = $products[0]['MerchantAddress']['address2'];
				 $mail_suburb=$products[0]['MerchantAddress']['suburb'];
				 $mail_state=$products[0]['MerchantAddress']['state'];
				 $mail_country=$products[0]['MerchantAddress']['country'];
			} else {
				$sql = "SELECT MerchantAddress.* FROM merchant_addresses as MerchantAddress where id='".$merchant_address_id."' ";

				$prod = $this->Product->query($sql);

				$merchant_address1 = $prod[0]['MerchantAddress']['address1'];
				$merchant_address2 = $prod[0]['MerchantAddress']['address2'];
				$mail_suburb=$prod[0]['MerchantAddress']['suburb'];
				$mail_state=$prod[0]['MerchantAddress']['state'];
				$mail_country=$prod[0]['MerchantAddress']['country'];
			}

			if($merchant_address1!='do not send' && $merchant_address1!='')
				$address1=strip_tags($merchant_address1);
				$address11=preg_replace("/[^a-zA-Z0-9s]/", " ", $address1);
			if($merchant_address2!='do not send' && $merchant_address2!='')
				$address2=strip_tags($merchant_address2);
				$address22=preg_replace("/[^a-zA-Z0-9s]/", " ", $address2);
			if($mail_suburb!='do not send' && $mail_suburb!='')
		 		$suburb=$mail_suburb;
		 	if($mail_country!='do not send' && $mail_country!='')
				$country=$mail_country;
			if($mail_state!='do not send' && $mail_state!='')
				$state_code=$mail_state;
				$sql="Select state_name FROM states as State where state_cd='$state_code'";
				$state_query = $this->Product->query($sql);
				$state= $state_query[0]['State']['state_name'];

			 	$addressfinal =$address11.'+'.$address22.'+'.$suburb.'+'.$state.'+'.$country;

			  	$finalURL = 'http://maps.google.com/maps/api/geocode/json?address='.str_replace(" ","+",$addressfinal).'&sensor=false';
			 	$geocode=file_get_contents($finalURL);

	          	$output= json_decode($geocode);

	        	$latitude = $output->results[0]->geometry->location->lat;
	        	$longitude = $output->results[0]->geometry->location->lng;

		    	if ($latitude==''||$longitude==''){
		      		$addressfinal =$suburb.'+'.$state.'+'.$country;

			  		$finalURL = 'http://maps.google.com/maps/api/geocode/json?address='.str_replace(" ","+",$addressfinal).'&sensor=false';
					$geocode=file_get_contents($finalURL);

	          		$output= json_decode($geocode);

	          		$latitude = $output->results[0]->geometry->location->lat;
	         		$longitude = $output->results[0]->geometry->location->lng;
		      	}

		      	if ($latitude==''||$longitude==''){
					$addressfinal =$state.'+'.$country;

					$finalURL = 'http://maps.google.com/maps/api/geocode/json?address='.str_replace(" ","+",$addressfinal).'&sensor=false';
					$geocode=file_get_contents($finalURL);

					$output= json_decode($geocode);

					$latitude = $output->results[0]->geometry->location->lat;
					$longitude = $output->results[0]->geometry->location->lng;
	      		}

	      		if ($latitude==''||$longitude==''){
					$addressfinal = $country;

					$finalURL = 'http://maps.google.com/maps/api/geocode/json?address='.str_replace(" ","+",$addressfinal).'&sensor=false';
					$geocode=file_get_contents($finalURL);

					$output= json_decode($geocode);

					$latitude = $output->results[0]->geometry->location->lat;
					$longitude = $output->results[0]->geometry->location->lng;
	      		}

			$productpoints =$this->ProductsPoint->find('first',array('conditions'=>array('ProductsPoint.product_id'=>$id),'recursive'=> -1));
			$this->set('productpoints', $productpoints);
		} else if (!empty($this->request->data['Product']['selected'])) {
			$sql = "SELECT DISTINCT Product.*, Merchant.* FROM products AS Product
					LEFT JOIN merchants AS Merchant ON Product.merchant_id = Merchant.id
					WHERE Product.id IN ('". implode("','", $this->request->data['Product']['selected']). "')
					AND Product.active = 1
					AND (Product.expires = '0000-00-00' OR Product.expires >= NOW())";
			$products = $this->Product->query($sql);
		} else {
			$this->redirect('/');
			exit();
		}

		foreach ($products as $key => $product) {
			// get the count of addresses
			$countSql = "SELECT COUNT(*) AS count FROM merchant_addresses AS MerchantAddress, products_addresses AS ProductsAddress
			WHERE MerchantAddress.merchant_id = ".$product['Product']['merchant_id']." and ProductsAddress.merchant_address_id=MerchantAddress.id
			and ProductsAddress.product_id=".$id;

			$location_count = $this->Product->Merchant->MerchantAddress->query($countSql);

			$mersql = "SELECT MerchantAddress.* from merchant_addresses as MerchantAddress, products_addresses as ProductsAddress WHERE ";
			if (!empty($merchant_address_id) && $merchant_address_id != 'all')
			{
				$mersql.="MerchantAddress.id=".$merchant_address_id;
			}
			else
			{
				$mersql.="MerchantAddress.merchant_id=".$product['Product']['merchant_id']." and MerchantAddress.id=ProductsAddress.merchant_address_id
				and ProductsAddress.product_id=".$id;
			}

			$merchant_address = $this->Product->Merchant->MerchantAddress->query($mersql);

			if (empty($merchant_address) || $merchant_address[0]['MerchantAddress']['merchant_id'] != $product['Product']['merchant_id'])
			{
				$mersql = "SELECT MerchantAddress.* from merchant_addresses as MerchantAddress, products_addresses as ProductsAddress WHERE
				MerchantAddress.merchant_id=".$product['Product']['merchant_id']." and MerchantAddress.id=ProductsAddress.merchant_address_id
				and ProductsAddress.product_id=".$id;

				$merchant_address = $this->Product->Merchant->MerchantAddress->query($mersql);
			}

			$product['MerchantAddress'] = $merchant_address[0]['MerchantAddress'];
			$product['location_count'] = $location_count[0][0]['count'];
			$products[$key] = $product;

			//	update the product view count
			$data = array('Product' => array('id' => $product['Product']['id'], 'views' => $product['Product']['views'] + 1));
			$this->Product->save($data);

		}
		$product_message = '';
		if(empty($products)) {
			$product_message = 'This offer is no longer available, for the latest available '.$this->Session->read('client.Client.name').' offers please search your program web site';
		}

		$this->loadModel("Skyscraper");

		$banner_sql = "select Skyscraper.* from skyscrapers as Skyscraper, clients_skyscrapers as ClientsSkyscraper,
		programs_skyscrapers as ProgramsScraper where ClientsSkyscraper.client_id=".$this->Session->read('client.Client.id')." and
		ProgramsScraper.program_id=".$this->Session->read('client.Client.program_id')." and Skyscraper.id=ClientsSkyscraper.skyscraper_id
		and Skyscraper.id=ProgramsScraper.skyscraper_id order by rand() limit 1";

		$banner_list = $this->Skyscraper->query($banner_sql);

		$this->set('bannerlist',$banner_list);

		$userid = $this->Session->read('user.User.id');
		$this->loadModel("User");

		$cat_list = "SELECT DISTINCT Category.name,Category.logo_extension, Category.id FROM categories as Category, categories_products AS CategoriesProduct, products AS Product
						LEFT JOIN merchants AS Merchant ON Product.merchant_id = Merchant.id
						WHERE Product.id = '". $id. "'
						AND Product.active = 1
						AND (Product.expires = '0000-00-00' OR Product.expires >= NOW())
						and Category.id = CategoriesProduct.category_id and Product.id = CategoriesProduct.product_id";
		$categories = $this->Product->query($cat_list);

		$sql="Select state_name FROM states as State where state_cd='".$products[0]['MerchantAddress']['state']."'";
		$state_query = $this->Product->query($sql);
		$state= $state_query[0]['State']['state_name'];

		$user_used_count = $this->User->find('first',array('conditions' => array('User.id' => $userid), 'recursive' => -1, 'fields' => array('User.used_count')));

		$this->loadModel("ProductsTracker");
		$print_count = $this->ProductsTracker->find('first',array('conditions' => array('ProductsTracker.product_id' => $id), 'fields' => array('ProductsTracker.product_id','ProductsTracker.clicks')));
		$sql = "update products_trackers as ProductsTracker set ProductsTracker.clicks =".$print_count['ProductsTracker']['clicks']."+1 where ProductsTracker.product_id=".$print_count['ProductsTracker']['product_id'];
		$this->ProductsTracker->query($sql);

		$this->loadModel("UserProductView");
		$print_count1 = $this->UserProductView->find('first',array('conditions' => array('UserProductView.product_id' => $id,'UserProductView.user_id' =>$this->Session->read('user.User.id')), 'fields' => array('UserProductView.product_id','UserProductView.user_id','UserProductView.clicks')));

		if(($print_count1['UserProductView']['user_id']==$this->Session->read('user.User.id')) && ($print_count1['UserProductView']['product_id']==$id))
	    {
			$sql = "update user_product_views as UserProductView set UserProductView.clicks =".$print_count1['UserProductView']['clicks']."+1
			where UserProductView.product_id=".$print_count1['UserProductView']['product_id']. " and UserProductView.user_id=".$this->Session->read('user.User.id');

			$this->UserProductView->query($sql);
		}
		else
	   	{
		    $this->request->data['UserProductView']['username'] = $this->Session->read('user.User.username');
		    $this->request->data['UserProductView']['user_id'] = $this->Session->read('user.User.id');
			$this->request->data['UserProductView']['product_id'] = $id;
			$this->request->data['UserProductView']['clicks'] = 1;
			$this->UserProductView->save($this->request->data);
		}

		$this->loadModel("Comment");
		$sql1 = "Select Comment.* FROM comments AS Comment where product_id = '". $id. "' AND status = '1'";
       	$comments = $this->Comment->query($sql1);

       	$this->loadModel("User");
		$user_id = $this->User->find('first',array('conditions'=>array('id'=>$comments[0]['Comment']['user_id']),'recursive'=> -1));

		$this->loadModel('Country');
		$countries = $this->Country->find('list', array('fields' => array('Country.name')));

		$this->set('countries',$countries);

       	$this->set('users',$user_id);
		$this->set('comments',$comments);
		$this->set('usedcount', $user_used_count['User']['used_count']);
		$this->set('product_message', $product_message);
		$this->set('products', $products);
		$this->set('latitude', $latitude);
		$this->set('longitude', $longitude);
		$this->set('addressfinal', $addressfinal);
		$this->set('categories_list',$categories);
		$this->set('state',$state);
		$this->set('cartproduct',$cartproduct);
	}

	/**
	 * View a product voucher, like view, but looks different
	 *
	 * @param	int	$id	product id
	 * @param	int	$merchant_address_id	id of the merchant address to display, if null, display the default address
	*/
	function voucher($id, $merchant_address_id=null) {

		$this->check_query_numeric($id, $merchant_address_id);
		$sql = "SELECT DISTINCT Product.*, Merchant.* FROM products AS Product
				INNER JOIN clients_products AS ClientsProduct ON Product.id = ClientsProduct.product_id AND (ClientsProduct.client_id = '". $this->Session->read('client.Client.id'). "' OR ClientsProduct.client_id = 0)
				LEFT JOIN merchants AS Merchant ON Product.merchant_id = Merchant.id
				WHERE Product.id = '". $id. "'
				AND Product.web_coupon = 1
				AND Product.active = 1
				AND (Product.expires = '0000-00-00' OR Product.expires >= NOW())
				LIMIT 1";
		$products = $this->Product->query($sql);
		if (isset($products[0])) {
			$product = $products[0];
		} else {
			$product = false;
		}

		$voucher = $this->Session->read('client.Voucher');

		if ($product) {
			// get the count of addresses
			$location_count = $this->Product->Merchant->MerchantAddress->find('count', array('conditions' => array('merchant_id' => $product['Product']['merchant_id'])));

			if (!empty($merchant_address_id)) {
				$conditions = array('MerchantAddress.id' => $merchant_address_id);
			} else {
				$conditions = array('MerchantAddress.merchant_id' => $product['Product']['merchant_id']);
			}

			$merchant_address = $this->Product->Merchant->MerchantAddress->find('first', array('conditions' => $conditions));
			$product['MerchantAddress'] = $merchant_address['MerchantAddress'];
		}

		$userid = $this->Session->read('user.User.id');
		$this->loadModel("User");

		$user_used_count = $this->User->find('first',array('conditions' => array('User.id' => $userid), 'recursive' => -1, 'fields' => array('User.used_count')));

		$this->set('usedcount',$user_used_count['User']['used_count']);
		$this->set('product', $product);
		$this->set('voucher', $voucher);
	}

	function update_used_count($product_id)
	{
		$this->check_query_numeric($product_id);
		$product_used_count = $this->Product->find('first',array('conditions' => array('Product.id' => $product_id), 'recursive' => -1, 'fields' => array('Product.used_count')));
		if($product_used_count['Product']['used_count'] > 0 && $product_used_count['Product']['used_count'] != -1)
		{
			$data = array('Product' => array('id' => $product_id, 'used_count' => $product_used_count['Product']['used_count'] - 1));
			$this->Product->save($data);
		}

		$this->loadModel("ProductsTracker");
		$print_count = $this->ProductsTracker->find('first',array('conditions' => array('ProductsTracker.product_id' => $product_id), 'fields' => array('ProductsTracker.product_id','ProductsTracker.prints')));

		$sql = "update products_trackers as ProductsTracker set ProductsTracker.prints =".$print_count['ProductsTracker']['prints']."+1 where ProductsTracker.product_id=".$product_id;
		$this->ProductsTracker->query($sql);

		$this->loadModel("UserProductView");
		$view_count = $this->UserProductView->find('first',array('conditions' => array('UserProductView.product_id' => $product_id,'UserProductView.user_id' =>$this->Session->read('user.User.id')), 'fields' => array('UserProductView.product_id','UserProductView.user_id','UserProductView.prints')));

		if(($view_count['UserProductView']['user_id']==$this->Session->read('user.User.id')) && ($view_count['UserProductView']['product_id']==$product_id))
		{
			$sql = "update user_product_views as UserProductView set UserProductView.prints =".$view_count['UserProductView']['prints']."+1
			where UserProductView.product_id=".$view_count['UserProductView']['product_id']. " and UserProductView.user_id=".$this->Session->read('user.User.id');

			$this->UserProductView->query($sql);
		}
		else
	   	{
		    $this->request->data['UserProductView']['username'] = $this->Session->read('user.User.username');
		    $this->request->data['UserProductView']['user_id'] = $this->Session->read('user.User.id');
			$this->request->data['UserProductView']['product_id'] = $product_id;
			$this->request->data['UserProductView']['prints'] = 1;
			$this->UserProductView->save($this->request->data);
		}

		$userid = $this->Session->read('user.User.id');
		$this->loadModel("User");

		$user_used_count = $this->User->find('first',array('conditions' => array('User.id' => $userid), 'recursive' => -1, 'fields' => array('User.used_count')));
		if($user_used_count['User']['used_count'] > 0 && $user_used_count['User']['used_count'] != -1)
		{
			$count_data = array('User' => array('id' => $userid, 'used_count' => $user_used_count['User']['used_count'] - 1));
			$this->User->save($count_data);
		}
		$this->autoRender=false;
	}

	/*
	 * Updates prints count for tracking
	 */
	function update_print_count($product_id)
	{
		$this->check_query_numeric($product_id);

		$this->loadModel("ProductsTracker");
		$print_count = $this->ProductsTracker->find('first',array('conditions' => array('ProductsTracker.product_id' => $product_id), 'fields' => array('ProductsTracker.product_id','ProductsTracker.prints')));

		$sql = "update products_trackers as ProductsTracker set ProductsTracker.prints =".$print_count['ProductsTracker']['prints']."+1 where ProductsTracker.product_id=".$product_id;
		$this->ProductsTracker->query($sql);

		$this->loadModel("UserProductView");
		$view_count = $this->UserProductView->find('first',array('conditions' => array('UserProductView.product_id' => $product_id,'UserProductView.user_id' =>$this->Session->read('user.User.id')), 'fields' => array('UserProductView.product_id','UserProductView.user_id','UserProductView.prints')));

		if(($view_count['UserProductView']['user_id']==$this->Session->read('user.User.id')) && ($view_count['UserProductView']['product_id']==$product_id))
	    {
			$sql = "update user_product_views as UserProductView set UserProductView.prints =".$view_count['UserProductView']['prints']."+1
			where UserProductView.product_id=".$view_count['UserProductView']['product_id']. " and UserProductView.user_id=".$this->Session->read('user.User.id');

			$this->UserProductView->query($sql);
		}
		else
	   	{
		    $this->request->data['UserProductView']['username'] = $this->Session->read('user.User.username');
		    $this->request->data['UserProductView']['user_id'] = $this->Session->read('user.User.id');
			$this->request->data['UserProductView']['product_id'] = $product_id;
			$this->request->data['UserProductView']['prints'] = 1;
			$this->UserProductView->save($this->request->data);
		}

		$this->autoRender=false;
	}

	/*
	 *	return a snipet of product merchant addresses
	 *
	 *	@param	int	$id		id of the product to display addresses for
	 *	@param	int	$merchant_id		id of the merchant to display addresses for
	 * @param	int	$address_id		id of the address to exclude
	*/
	function ajax_addresses($id, $merchant_id, $address_id = null) {
		$this->check_query_numeric($id,$merchant_id, $address_id);
		$this->Product->Merchant->MerchantAddress->recursive = 0;

		$sql = "SELECT MerchantAddress.* FROM merchant_addresses AS MerchantAddress, products_addresses as ProductsAddress
		WHERE MerchantAddress.merchant_id = '".$merchant_id."' AND MerchantAddress.id != '".$address_id."'
		and MerchantAddress.id = ProductsAddress.merchant_address_id and ProductsAddress.product_id= ".$id." ORDER BY MerchantAddress.suburb ASC";

		$addresses = $this->Product->Merchant->MerchantAddress->query($sql);

		$this->set('merchant_addresses',$addresses);
		$this->set('product_id', $id);

		$this->layout = 'blank';
	}


	/**
	 * Search for offers
	 *
	 * @param	int	$type	the type of search: keyword , location or category
	*/
	function search2($type=null) {

		if (!empty($_GET['data']['Product'])) {
			foreach ($_GET['data']['Product'] as $key => $value) {
				$this->request->params['named'][$key] = $value;
			}
		}

		//	default search method
		if (!isset($this->params['named']['sort'])) {
			$search_sort = 'default';
		}
		else
		{
			$search_sort = addslashes($this->params['named']['sort']);
		}

		//	set the form values again
		foreach ($this->params['named'] as $key => $value) {
			$this->request->data['Product'][$key] = $value;
		}

		$type = 'keyword';

		if (!empty($type)) {
             //  exact or like  -- condition
			if (isset($this->params['named']['search_mode_id']) && $this->params['named']['search_mode_id'] != '')
			{
				$search_mode =  addslashes(addslashes($this->params['named']['search_mode_id']));
			} else {$search_mode = 'E';}
            // search particular state or default all states
            if (isset($this->params['named']['search_state_id']) && $this->params['named']['search_state_id'] != '')
            {
                $search_state = addslashes(addslashes($this->params['named']['search_state_id']));
            } else { $search_state = '';}

			if (isset($this->params['named']['location'])) {
				$search_location = addslashes(addslashes($this->params['named']['location']));
			} else {
				$search_location = '';
			}

			if (isset($this->params['named']['keywords'])) {
				$search_keyword = addslashes(addslashes($this->params['named']['keywords']));
			} else {
				$search_keyword = '';
			}

			$this->Product->unbindModel(array('belongsTo' => array('Merchant'), 'hasAndBelongsToMany' => array('Client')));

			$dashboardid=$this->Session->read('dashboardidinsession');

			if($dashboardid == 3)
			{
				$sql =" INNER JOIN categories_products AS CategoriesProduct ON Product.id = CategoriesProduct.product_id
				INNER JOIN clients_categories AS ClientsCategory ON CategoriesProduct.category_id = ClientsCategory.category_id AND ClientsCategory.client_id = '". $this->Session->read('client.Client.id'). "'
				WHERE Product.merchant_id = MerchantAddress.merchant_id and ProductsAddress.merchant_address_id=MerchantAddress.id
				and Category.id = CategoriesProduct.category_id and ProductsAddress.product_id=Product.id AND Product.active = 1 and Product.cancelled = 0 and Product.merchant_id = Merchant.id
				AND (Product.offer_start = '0000-00-00' OR Product.offer_start <= NOW()) AND (Product.expires = '0000-00-00' OR Product.expires >= NOW())
				and Product.id = ClientsProductsPoint.product_id AND ProductsPoint.product_id = ClientsProductsPoint.product_id AND ProductsPoint.active=1
				AND (ClientsProductsPoint.client_id = '". $this->Session->read('client.Client.id'). "' OR ClientsProductsPoint.client_id = 0)";
			}
			if($dashboardid == 2)
			{
				$sql =" INNER JOIN categories_products AS CategoriesProduct ON Product.id = CategoriesProduct.product_id
				INNER JOIN clients_categories AS ClientsCategory ON CategoriesProduct.category_id = ClientsCategory.category_id AND ClientsCategory.client_id = '". $this->Session->read('client.Client.id'). "'
				WHERE Product.merchant_id = MerchantAddress.merchant_id and ProductsAddress.merchant_address_id=MerchantAddress.id
				and Category.id = CategoriesProduct.category_id and ProductsAddress.product_id=Product.id AND Product.active = 1 and Product.cancelled = 0 and Product.merchant_id = Merchant.id
				AND (Product.offer_start = '0000-00-00' OR Product.offer_start <= NOW()) AND (Product.expires = '0000-00-00' OR Product.expires >= NOW()) and Product.id = ClientsProductsSaf.product_id
				AND (ClientsProductsSaf.client_id = '". $this->Session->read('client.Client.id'). "' OR ClientsProductsSaf.client_id = 0)";
			}
			if($dashboardid == 1)
			{
				$sql =" INNER JOIN categories_products AS CategoriesProduct ON Product.id = CategoriesProduct.product_id
				INNER JOIN clients_categories AS ClientsCategory ON CategoriesProduct.category_id = ClientsCategory.category_id AND ClientsCategory.client_id = '". $this->Session->read('client.Client.id'). "'
				WHERE Product.merchant_id = MerchantAddress.merchant_id and ProductsAddress.merchant_address_id=MerchantAddress.id
				and Category.id = CategoriesProduct.category_id and ProductsAddress.product_id=Product.id AND Product.active = 1 and Product.cancelled = 0 and Product.merchant_id = Merchant.id
				AND (Product.offer_start = '0000-00-00' OR Product.offer_start <= NOW()) AND (Product.expires = '0000-00-00' OR Product.expires >= NOW()) and Product.id = ClientsProduct.product_id
				AND (ClientsProduct.client_id = '". $this->Session->read('client.Client.id'). "' OR ClientsProduct.client_id = 0)
				AND ClientsProduct.product_id not in (select product_id from products_points) AND ClientsProduct.product_id not in (select product_id from products_safs)";
			}
			if (($this->Session->read('client.Client.blacklisted_product') != ''))
			{
							$sql .= " AND Product.id not in (". $this->Session->read('client.Client.blacklisted_product'). ") ";
			}

			if (isset($this->params['named']['category_id']) && $this->params['named']['category_id'] != '' && is_numeric($this->params['named']['category_id'])) {
							$sql .= " AND CategoriesProduct.category_id = '". $this->params['named']['category_id']. "' ";
							$this->Product->Category->recurisve = 0;
							$this->set('category', $this->Product->Category->findById($this->params['named']['category_id']));
			}
				//Modified Code for Keyword
				if($search_keyword != '')
				{
					if($search_mode == 'E')
					{
					// Search for exact condition  - anywhere in the following fields
                    // Match-Against statement doesn't work accordingly when words are enclosed in double quotes
					   $sql .= " AND
								( Product.keyword LIKE '%". $search_keyword. "%'
								OR Product.name LIKE '%". $search_keyword. "%'
								OR MerchantAddress.name LIKE '%". $search_keyword. "%'
								OR Product.details LIKE '%". $search_keyword. "%'
								OR Product.highlight LIKE '%". $search_keyword. "%'
								OR Product.`text` LIKE '%". $search_keyword. "%'
								OR Product.special_offer_headline LIKE '%". $search_keyword. "%'
								OR Product.special_offer_body LIKE '%". $search_keyword. "%' ) ";
								//break;
	            	}
					else
					{
						//Search for Like condition

						$kt=split(' ' ,$search_keyword);
						while(list($key,$val)=each($kt))
						{
							if($val<>' ' and strlen($val) > 0)
							{
								 $sql .= " AND
									( Product.keyword LIKE '%". $val. "%'
									OR Product.name LIKE '%". $val. "%'
									OR MerchantAddress.name LIKE '%". $val. "%'
									OR Product.details LIKE '%". $val. "%'
									OR Product.highlight LIKE '%". $val. "%'
									OR Product.`text` LIKE '%". $val. "%'
									OR Product.special_offer_headline LIKE '%". $val. "%'
									OR Product.special_offer_body LIKE '%". $val. "%' ) " ;
							}
						 }
					}
				}
					//Modified Code for Location

					$found = false;
					$postcode = false;

					//	state search
					$states = array('act' => 'ACT', 'nsw' => 'NSW', 'vic' => 'VIC', 'tas' => 'TAS', 'sa' => 'SA', 'wa' => 'WA', 'nt' => 'NT', 'qld' => 'QLD',
										'australian capitol territory' => 'ACT', 'new south wales' => 'NSW', 'victoria' => 'VIC', 'tasmania' => 'TAS', 'south australia' => 'SA',
										'western australia' => 'WA', 'northern territory' => 'NT', 'queensland' => 'QLD',
										'a.c.t.' => 'ACT', 'n.s.w.' => 'NSW', 'vic.' => 'VIC', 'tas.' => 'TAS', 's.a.' => 'SA', 'w.a.' => 'WA', 'n.t.' => 'NT', 'qld.' => 'QLD'
					);

					if ($search_location == '') {
						$found = true;
					} else if (isset($states[strtolower($search_location)])) {
						$postcode = false;
						$found = true;
					//	postcode search
					}
					elseif ($search_location!='')
					{
						if(strpos($search_location,'-'))
						{
							$suburb_txt = trim(substr($search_location, strpos($search_location,'-')+1));
							$search_location = $suburb_txt;
						}
						if(is_numeric(trim($search_location)))
							$postcode = $search_location;
						else
						{
							$postcode_sql = "select Postcode.postcode from postcodes as Postcode where Postcode.suburb='".$search_location."'";
							$code = $this->Postcode->query($postcode_sql);

							$postcodes = array();

							foreach($code as $cd)
							{
								array_push($postcodes, $cd['Postcode']['postcode']);
							}
							$postcode = $postcodes;
						}
						$include_postcode = $postcode;
						$found = true;

					//	suburb search
					} else {

						$suburbs = $this->Suburb->find('all', array('conditions'=>array("suburb LIKE '". $search_location. "'")));

						if ($suburbs) {
							foreach ($suburbs as $suburb) {
								$postcode[] = $suburb['Suburb']['postcode'];
							}
							$include_postcode = $postcode[0];
							$found = true;
						}

					}
						$radius = $this->params['named']['distance'];
						if(isset($radius) && $radius!='' && is_numeric($radius))
						{
							if(is_array($include_postcode))
								$ref=mysql_query("SELECT `lat`,`lon` FROM `postcodes` WHERE `postcode` in (".implode(',',$include_postcode).")");
							else
								$ref=mysql_query("SELECT `lat`,`lon` FROM `postcodes` WHERE `postcode`  =  {$include_postcode}");

							//$ref=mysql_query("SELECT `lat`,`lon` FROM `postcodes` WHERE `postcode`='{$include_postcode}'");
							if(mysql_num_rows($ref)){
								$res=mysql_fetch_assoc($ref);
								$lat=$res["lat"];
								$lon=$res["lon"];
								$ref2= mysql_query("SELECT `postcode` FROM `postcodes` WHERE (POW((69.1*(`lon`-\"$lon\")*cos($lat/57.3)),\"2\")+POW((69.1*(`lat`-\"$lat\")),\"2\"))<($radius*$radius)");
								$postcode=array();
								if(mysql_num_rows($ref2)){
									while($res2=mysql_fetch_assoc($ref2)){
										array_push($postcode,$res2["postcode"]);
										//$postcode = $res2["postcode"];
									}
								}
							}
						}

					$sql .= " AND (Product.available_international = 1 OR ((1=1 ";

					if ($search_location == '' && $search_state == ''){}
					else if ($search_location == '' && $search_state != ''){
						$sql .= " AND (Product.available_national = 1 OR MerchantAddress.state = '". $search_state ."') ";
					}
					else if ($search_location != '') {
						//check postcode / postcodes
						if (is_array($postcode)) {
							$i = 1;
							foreach ($postcode as $value){
								if($i == 1)
								{
									 $sql .= " AND (MerchantAddress.postcode = '". $value. "' ";
								}
								else
								{
									 $sql .= " OR MerchantAddress.postcode = '". $value. "' ";
								}
								$i = $i + 1;
							}
							$sql .= ") ";
						}
						else	{
							 if ($postcode){
							 $sql .= " AND MerchantAddress.postcode = '". $postcode. "' ";
							 }
						}
						if ($search_state != ''){
							$sql .= "  AND ( Product.available_national = 1 OR MerchantAddress.state = '". $search_state ."') ";
						}

					}

					$sql .= ") ";
					if (!$found) {
						$sql .= " AND 1!=1 ";
					}


			if (isset($this->params['named']['country_id'])) {
					$sql .= " AND (MerchantAddress.country= '". addslashes($this->params['named']['country_id']). "')))";
			}
			else
			{
				$sql .= " AND (MerchantAddress.country='".$this->Session->read('client.Client.country')."')))";
			}

			$this->paginate = array('ProductList2' => array('limit' => SEARCH_RESULTS_PER_PAGE),array ('search' =>array('dashboardid'=>$dashboardid )));
			$products = $this->paginate('ProductList2', array('search' =>array('sql' => $sql, 'keyword' => $search_keyword,'dashboardid'=>$dashboardid ), 'sort' => $search_sort));

			if ($search_sort != 'suburb') {
				foreach ($products as $key => $product) {
					$address_count = $this->Product->Merchant->MerchantAddress->find('count', array('conditions'=>array('MerchantAddress.merchant_id' => $product['Product']['merchant_id'])));

					if ($address_count == '0') {
						$products[$key]['MerchantAddress'] = 'none';
					} else if ($address_count > 1) {
						$products[$key]['MerchantAddress'] = 'multiple';
					} else {
						$merchant_address = $this->Product->Merchant->MerchantAddress->find('first', array('conditions'=>array('MerchantAddress.merchant_id' => $product['Product']['merchant_id'])));
						$products[$key]['MerchantAddress'] = $merchant_address['MerchantAddress'];
					}
				}
			}


			//get country selected from drop down
			$ctry= addslashes($this->params['named']['country_id']);

			//first time param value will be empty so get country from session
	        if(empty($ctry))
	        	$ctry = $this->Session->read('client.Client.country');

	       	// Query to get categories array based on selected country from drop down

	    	$catsql ="SELECT DISTINCT Category.* FROM categories AS Category
	    	LEFT JOIN clients_categories AS ClientsCategory ON Category.id = ClientsCategory.category_id
	    	WHERE ClientsCategory.client_id = '". $this->Session->read('client.Client.id') ."' and Category.countries = '".$ctry."'
	    	order by Category.name";

	    	$this->loadModel("Category");
			$ctgry = $this->Category->query($catsql);

	    	$countrycat = array();
			foreach ($ctgry as $category1) {
				$countrycat[$category1['Category']['id']] = $category1['Category']['name'];
			}

			// Query to get states array based on selected country from drop down

		    $statesql ="SELECT State.state_cd, State.state_name FROM states AS State, countries as Country, clients as Client,
		    merchant_addresses as MerchantAddress, products as Product where State.country_cd = Country.id and Country.name='".$ctry."'
		    and MerchantAddress.merchant_id = Product.merchant_id and MerchantAddress.state = State.state_cd
		    and Client.id = ".$this->Session->read('client.Client.id')." order by State.state_name";

	    	$this->loadModel("State");
			$state = $this->State->query($statesql);

	    	$statectry = array();
			foreach ($state as $state1) {
				$statectry[$state1['State']['state_cd']] = $state1['State']['state_name'];
			}

			if(!empty($this->params['named']['country_id']))
			{
				$this->Session->write('statelist', $statectry);
				$this->Session->write('catlist', $countrycat);
			}
			$this->Session->write('params', $this->params['named']);


			$this->loadModel("Skyscraper");
			$banner_sql = "select Skyscraper.* from skyscrapers as Skyscraper, clients_skyscrapers as ClientsSkyscraper,
			programs_skyscrapers as ProgramsScraper where ClientsSkyscraper.client_id=".$this->Session->read('client.Client.id')." and
			ProgramsScraper.program_id=".$this->Session->read('client.Client.program_id')." and Skyscraper.id=ClientsSkyscraper.skyscraper_id
			and Skyscraper.id=ProgramsScraper.skyscraper_id order by rand() limit 1";

			$banner_list = $this->Skyscraper->query($banner_sql);

			$this->set('bannerlist',$banner_list);

			$this->set('statelist',$statectry);
			$this->set('categorylist',$countrycat);
			$this->set('products', $products);

			$this->set('whats_new_products', $this->_whatsNewProducts($this->Session->read('client.Client.id'), $this->Session->read('client.Client.whats_new_module_limit')));

			$this->set('sorts', array('default' => 'Default', 'name' => 'Name', 'suburb' => 'Suburb', 'latest' => 'Latest Offers', 'state' => 'By State'));

			$this->set('named_params', $this->params['named']);

		}

		$userid = $this->Session->read('user.User.id');
		$this->loadModel("User");

		$user_used_count = $this->User->find('first',array('conditions' => array('User.id' => $userid), 'recursive' => -1, 'fields' => array('User.used_count')));

		$this->set('usedcount', $user_used_count['User']['used_count']);
		$this->set('type', $type);

		$this->loadModel('Country');
		$countries = $this->Country->find('list', array('fields' => array('Country.name')));

		$this->set('countries',$countries);

	}

	/*
	 * Populate states using ajax for country in search
	 */
	function update_select()
    {
	   	$selected_country= $this->request->data['Product']['country_id'];
    	$defaultstatesql ="SELECT State.state_cd, State.state_name FROM states AS State, countries as Country, clients as Client,
    	merchant_addresses as MerchantAddress, products as Product where State.country_cd = Country.id and
    	MerchantAddress.merchant_id = Product.merchant_id and MerchantAddress.state = State.state_cd
    	and Country.name='".$selected_country."' and Client.id = ".$this->Session->read('client.Client.id')." order by State.state_name";

    	$this->loadModel("State");
		$cstates = $this->State->query($defaultstatesql);

    	$ctrystate = array();
		foreach ($cstates as $ctrystates) {
			$ctrystate[$ctrystates['State']['state_cd']] = $ctrystates['State']['state_name'];
		}

        $this->set('cstates',$ctrystate);
        $this->layout = 'ajax';
    }

    /*
	 * Populate categories using ajax for country in search
	 */
	function update_cat()
    {
	    $selected_ctry= $this->request->data['Product']['country_id'];
    	$defaultcatsql ="SELECT DISTINCT Category.* FROM categories AS Category
    	LEFT JOIN clients_categories AS ClientsCategory ON Category.id = ClientsCategory.category_id
    	WHERE ClientsCategory.client_id = '". $this->Session->read('client.Client.id') ."' and Category.countries = '".$selected_ctry."'
    	order by Category.name";

    	$this->loadModel("Category");
		$ccat = $this->Category->query($defaultcatsql);

    	$ctrycat = array();
		foreach ($ccat as $cat) {
			$ctrycat[$cat['Category']['id']] = $cat['Category']['name'];
		}

        $this->set('ctry_cat',$ctrycat);
        $this->layout = 'ajax';
    }

    /*
	 * generate merchant addresses states in products admin
	 */
	function admin_ajax_states($country,$state,$adid)
    {
    	$selected_country = $country;
    	$defaultstatesql ="SELECT State.state_cd, State.state_name FROM states AS State, countries as Country, clients as Client where State.country_cd = Country.id
		and Country.name='".$selected_country."'";

    	$this->loadModel("State");
		$cstates = $this->State->query($defaultstatesql);

    	$ctrystate = array();
		foreach ($cstates as $ctrystates) {
			$ctrystate[$ctrystates['State']['state_cd']] = $ctrystates['State']['state_name'];
		}
		$this->set('stateid',$state);
		$this->set('adId',$adid);
        $this->set('cstates',$ctrystate);
        $this->layout = 'ajax';
    }

	/*
	 * report of products for easyfuel
	*/
	function easyfuel() {

		$this->loadModel('Country');
		$countries = $this->Country->find('list', array('fields' => array('Country.name')));

		$this->set('countries',$countries);


        $statesql ="SELECT State.state_cd, State.state_name FROM states AS State, countries as Country, clients as Client where State.country_cd = Country.id
	   	and Country.name='".$this->Session->read('user.User.country')."'";

    	$this->loadModel("State");
		$state = $this->State->query($statesql);

    	$states = array();
		foreach ($state as $state1) {
			$states[$state1['State']['state_cd']] = $state1['State']['state_name'];
		}

		$this->set('states',$states);


		//$states = $this->Product->Merchant->MerchantAddress->getEnumValues('state');
		//unset($states['']);
		//$this->set('states', $states);

	}

	function easyfuel_states()
	{
		$selected_country= $this->request->data['Product']['country'];
		//$selected_country = 'India';
    	$defaultstatesql ="SELECT State.state_cd, State.state_name FROM states AS State, countries as Country, clients as Client where State.country_cd = Country.id
	   	and Country.name='".$selected_country."'";

    	$this->loadModel("State");
		$cstates = $this->State->query($defaultstatesql);

    	$ctrystate = array();
		foreach ($cstates as $ctrystates) {
			$ctrystate[$ctrystates['State']['state_cd']] = $ctrystates['State']['state_name'];
		}

        $this->set('cstates',$ctrystate);
        $this->layout = 'ajax';
	}

	function easyfuel_list() {

		if (isset($this->request->data['Product']['state'])) {

			//	fuel products
			$sql = "SELECT DISTINCT Product. * , Merchant.*, MerchantAddress. * FROM products AS Product
						INNER JOIN categories_products AS CategoriesProduct ON Product.id = CategoriesProduct.product_id
						INNER JOIN clients_categories AS ClientsCategory ON CategoriesProduct.category_id = ClientsCategory.category_id AND ClientsCategory.client_id = '". $this->Session->read('client.Client.id'). "'
						INNER JOIN clients_products AS ClientsProduct ON Product.id = ClientsProduct.product_id AND (ClientsProduct.client_id = '". $this->Session->read('client.Client.id'). "' OR ClientsProduct.client_id = 0)
						INNER JOIN categories AS Category ON CategoriesProduct.category_id = Category.id AND Category.description = 'EasyFuel Fuel'
						LEFT JOIN merchant_addresses AS MerchantAddress ON Product.merchant_id = MerchantAddress.merchant_id
						LEFT JOIN merchants AS Merchant ON Product.merchant_id = Merchant.id

						WHERE Product.active = 1
						AND (Product.expires = '0000-00-00' OR Product.expires >= NOW())
						AND MerchantAddress.state = '". $this->request->data['Product']['state']. "'
						ORDER BY MerchantAddress.suburb, MerchantAddress.name";
			$fuel_products = $this->Product->query($sql);
			$fuel_suburbs = array();
			foreach ($fuel_products as $product) {
				$fuel_suburbs[strtoupper($product['MerchantAddress']['suburb'])][] = $product;
			}

			$fuel_suburbs = array_chunk($fuel_suburbs, (ceil(count($fuel_suburbs) /2)), true);

			$this->set('fuel_suburbs', $fuel_suburbs);

			//	retail products
			$sql = "SELECT DISTINCT Product. * , Merchant.*, MerchantAddress. * FROM products AS Product
						INNER JOIN categories_products AS CategoriesProduct ON Product.id = CategoriesProduct.product_id
						INNER JOIN clients_categories AS ClientsCategory ON CategoriesProduct.category_id = ClientsCategory.category_id AND ClientsCategory.client_id = '". $this->Session->read('client.Client.id'). "'
						INNER JOIN clients_products AS ClientsProduct ON Product.id = ClientsProduct.product_id AND (ClientsProduct.client_id = '". $this->Session->read('client.Client.id'). "' OR ClientsProduct.client_id = 0)
						INNER JOIN categories AS Category ON CategoriesProduct.category_id = Category.id AND Category.description != 'Easyretail fuel'
						LEFT JOIN merchant_addresses AS MerchantAddress ON Product.merchant_id = MerchantAddress.merchant_id
						LEFT JOIN merchants AS Merchant ON Product.merchant_id = Merchant.id

						WHERE Product.active = 1
						AND (Product.expires = '0000-00-00' OR Product.expires >= NOW())
						AND MerchantAddress.state = '". $this->request->data['Product']['state']. "'
						ORDER BY MerchantAddress.suburb, MerchantAddress.name";
			$retail_products = $this->Product->query($sql);
			$retail_suburbs = array();
			foreach ($retail_products as $product) {
				$retail_suburbs[strtoupper($product['MerchantAddress']['suburb'])][] = $product;
			}

			$retail_suburbs = array_chunk($retail_suburbs, (ceil(count($retail_suburbs) /2)), true);

			$this->set('retail_suburbs', $retail_suburbs);

			$this->set('state', $this->request->data['Product']['state']);
		}
	}

	/*
	 * report of show_and_save products
	*/
	function show_and_save() {

		$this->loadModel('Country');
		$countries = $this->Country->find('list', array('fields' => array('Country.name')));

		$this->set('countries',$countries);


        $statesql ="SELECT State.state_name, State.state_cd FROM states AS State, countries as Country, clients as Client where State.country_cd = Country.id
	   	and Country.name='".$this->Session->read('user.User.country')."'";

    	$this->loadModel("State");
		$state = $this->State->query($statesql);

    	$states = array();
		foreach ($state as $state1) {
			$states[$state1['State']['state_cd']] = $state1['State']['state_name'];
		}

		$this->set('states',$states);
		//$states = $this->Product->Merchant->MerchantAddress->getEnumValues('state');
		//unset($states['']);
		//$this->set('states', $states);

	}

	function show_and_save_report() {

		if (isset($this->request->data['Product']['state'])) {

			$sql = "SELECT DISTINCT Product. * , Merchant.*, MerchantAddress. *, Category.* FROM products AS Product
						INNER JOIN categories_products AS CategoriesProduct ON Product.id = CategoriesProduct.product_id
						INNER JOIN clients_categories AS ClientsCategory ON CategoriesProduct.category_id = ClientsCategory.category_id AND ClientsCategory.client_id = '". $this->Session->read('client.Client.id'). "'
						INNER JOIN clients_products AS ClientsProduct ON Product.id = ClientsProduct.product_id AND (ClientsProduct.client_id = '". $this->Session->read('client.Client.id'). "' OR ClientsProduct.client_id = 0)
						INNER JOIN categories AS Category ON CategoriesProduct.category_id = Category.id
						LEFT JOIN merchant_addresses AS MerchantAddress ON Product.merchant_id = MerchantAddress.merchant_id
						LEFT JOIN merchants AS Merchant ON Product.merchant_id = Merchant.id
						WHERE Product.active = 1
						AND Product.show_and_save = 1
						AND (Product.expires = '0000-00-00' OR Product.expires >= NOW())
						AND (
							MerchantAddress.state = '". $this->request->data['Product']['state']. "'
							OR Product.available_national = 1
							OR Product.available_". strtolower($this->request->data['Product']['state']). " = 1
						)
						ORDER BY Category.name, MerchantAddress.name";

			$products = $this->Product->query($sql);
			$categories = array();
			foreach ($products as $product) {
				$categories[strtoupper($product['Category']['name'])][] = $product;
			}
			if (!empty($categories)) {
				$categories = array_chunk($categories, (ceil(count($categories) /2)), true);
			}

			$this->set('categories', $categories);

			$this->set('state', $this->request->data['Product']['state']);
		}
	}

	   function admin_index() {

			$conditions = array();
			if (isset($this->params['url']['data'])) {
				$this->Session->write($this->name.'.search', $this->params['url']['data']);
			}

			if ($this->Session->check($this->name.'.search')) {
				$this->data = $this->Session->read($this->name.'.search');
			
				if (!empty($this->data['Product']['search'])){
								
				$conditions[] = "((`Product`.`name` LIKE  '%". $this->data['Product']['search']. "%'	OR `Product`.`id` LIKE  '%". $this->data['Product']['search']. "%') AND `Product`.`active`='".$this->data['Product']['active']."'  )";
				}
				else{	
				$conditions[] = "(`Product`.`active` = '".$this->data['Product']['active']."')";
				}			
			}
	     	$this->Product->recursive = 0;
			$prdts = $this->paginate('Product',$conditions);
		   	$this->set('products', $prdts);
		}


	/**
	 * Edit or create a product
	 *
	 * @param	int	id product id, if empty create a new product
	 * @param	int	$merchant_id id of the merchant if creating a new product
	 * @param	int	$category_id	id of category to assign this product to if creating a new product. This will automatically select all clients and programs for that category. note this is passed as a passedArgs array item
	*/
	function admin_edit($id = null, $merchant_id = null) {

		$this->check_query_numeric_admin($id, $merchant_id);

		if(!empty($this->params['named']['merchant_id']))
			$param_merchant_id = $this->params['named']['merchant_id'];

		if (!empty($this->request->data)) {

			if(isset($_POST['copy']))
			{
				$this->request->data = $this->Product->read(null,$id);
				unset($this->request->data['Product']['id']);
				unset($this->request->data['Product']['name']);
			}
			else
			{
				if ($this->request->data['Product']['expires']['day'] == '' || $this->request->data['Product']['expires']['month'] == '' || $this->request->data['Product']['expires']['year'] == '') {
					$this->request->data['Product']['expires']['day'] = '00';
					$this->request->data['Product']['expires']['month'] = '00';
					$this->request->data['Product']['expires']['year'] = '0000';
				}

				if ($this->request->data['Product']['offer_start']['day'] == '' || $this->request->data['Product']['offer_start']['month'] == '' || $this->request->data['Product']['offer_start']['year'] == '') {
					$this->request->data['Product']['offer_start']['day'] = date("d");
					$this->request->data['Product']['offer_start']['month'] = date("m");
					$this->request->data['Product']['offer_start']['year'] = date("Y");
				}


				//	get the product image extension
				if ($this->request->data['ProductImage']['file']['name'] != '') {
					$extension = substr($this->request->data['ProductImage']['file']['name'], strrpos($this->request->data['ProductImage']['file']['name'], '.') + 1);
					$this->request->data['Product']['image_extension'] = $extension;
				}

				if ($this->request->data['ProductImage1']['file']['name'] != '') {
					$extension = substr($this->request->data['ProductImage1']['file']['name'], strrpos($this->request->data['ProductImage1']['file']['name'], '.') + 1);
					$this->request->data['Product']['hotoffer_extension'] = $extension;
				}

				if ($this->request->data['ProductAppImage']['file']['name'] != '') {
					$extension = substr($this->request->data['ProductAppImage']['file']['name'], strrpos($this->request->data['ProductAppImage']['file']['name'], '.') + 1);
					$this->request->data['Product']['app_image_extension'] = $extension;
				}

				//$this->Product->recursive = -1;
				$old_data = $this->Product->findById($this->request->data['Product']['id']);

				if(!empty($id))
				{
					$clients1 = $this->Product->Program->Client->ClientsProduct->find('all', array('conditions' =>array('product_id' =>$id),'fields'=>array('client_id')));
					$bclients = array();
					foreach($clients1 as $c1)
					{
						array_push($bclients,$c1['ClientsProduct']['client_id']);
					}
				}
				if(empty($this->request->data['Product']['price'])){
					$this->request->data['Product']['price']=0;
				}
				if(empty($this->request->data['Product']['quantity'])){
					$this->request->data['Product']['quantity']=0;
				}

				if ($this->Product->save($this->request->data['Product']))
				{
					$prId = $this->Product->id;
					//Save categories products
					$this->Product->CategoriesProduct->deleteAll(array('product_id'=>$prId));
					$prCategoryData = array();
					foreach($this->request->data['Category']['Category'] as $prCategory){if(!empty($prCategory)){
						$prCategoryData[] = array('product_id'=>$prId, 'category_id'=>$prCategory);
					}}
					if(!empty($prCategoryData)){$this->Product->CategoriesProduct->saveMany($prCategoryData);}
					//Save product addresses
					$this->Product->ProductsAddress->deleteAll(array('product_id'=>$prId));
					$prAddressData = array();
					foreach($this->request->data['MerchantAddress']['MerchantAddress'] as $prAddress){if(!empty($prAddress)){
						$prAddressData[] = array('product_id'=>$prId, 'merchant_address_id'=>$prAddress);
					}}
					if(!empty($prAddressData)){$this->Product->ProductsAddress->saveMany($prAddressData);}

					$this->Product->ProgramsProduct->deleteAll(array('product_id'=>$prId));
					$this->Product->ClientsProduct->deleteAll(array('product_id'=>$prId));

					if($this->request->data['Product']['product_type'] == '-1'){
						$this->Product->ClientsProduct->save(array('product_id'=>$prId, 'client_id'=>0));
					}else{
						//Save programs products
						$prProgramData = array();
						foreach($this->request->data['Program']['Program'] as $prProgram){if(!empty($prProgram)){
							$prProgramData[] = array('product_id'=>$prId, 'program_id'=>$prProgram);
						}}
						if(!empty($prProgramData)){$this->Product->ProgramsProduct->saveMany($prProgramData);}

						//Save clients products
						$prClientData = array();
						foreach($this->request->data['Client']['Client'] as $prClient){if(!empty($prClient)){
							$prClientData[] = array('product_id'=>$prId, 'client_id'=>$prClient);
						}}
						if(!empty($prClientData)){$this->Product->ClientsProduct->saveMany($prClientData);}
					}
					/*if($this->request->data['Product']['active']==0) {
						// create a new cURL resource
						$ch = curl_init();

						// set URL and other appropriate options
						curl_setopt($ch, CURLOPT_URL, "http://www.myrewards.com.au:8983/solr/core0/update?stream.body=<delete><query>id:".$this->request->data['Product']['id']."</query></delete>&commit=true");
						curl_setopt($ch, CURLOPT_HEADER, 0);
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

						// grab URL and pass it to the browser
						curl_exec($ch);

						// close cURL resource, and free up system resources
						curl_close($ch);
					}else{
						$aclients = $this->request->data['Client']['Client'];
						$dclients = array_diff($bclients, $aclients);
						foreach($dclients as $dclient) {
							// create a new cURL resource
							$ch = curl_init();

							// set URL and other appropriate options
							curl_setopt($ch, CURLOPT_URL, "http://www.myrewards.com.au:8983/solr/core0/update?stream.body=<delete><query>(id:".$id.")AND(clientId:".$dclient.")</query></delete>&commit=true");
							curl_setopt($ch, CURLOPT_HEADER, 0);
							curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

							// grab URL and pass it to the browser
							curl_exec($ch);

							// close cURL resource, and free up system resources
							curl_close($ch);
						}
					}*/

				//inserting product into products_tracker table

				$this->loadModel("ProductsTracker");
				$prod = $this->ProductsTracker->findByProductId($this->Product->id);

				if(empty($prod))
				{
					$this->request->data['ProductsTracker']['product_id'] = $this->Product->id;
					$this->request->data['ProductsTracker']['clicks'] = 0;
					$this->request->data['ProductsTracker']['prints'] = 0;
					$this->ProductsTracker->save($this->request->data);
				}

				$this->loadModel("CartProduct");

				if($this->request->data['Product']['online_purchase'] ==1)
				{
					$this->request->data['CartProduct']['product_id'] = $this->Product->id;
					$this->request->data['CartProduct']['name'] = $this->request->data['Product']['name'];
					$this->request->data['CartProduct']['details'] = $this->request->data['Product']['details'];
					$this->request->data['CartProduct']['highlight'] = $this->request->data['Product']['highlight'];
					$this->request->data['CartProduct']['price'] = $this->request->data['Product']['price'];
					$this->request->data['CartProduct']['quantity'] = $this->request->data['Product']['quantity'];
					$this->request->data['CartProduct']['image_extension'] = 'gif';
					$this->request->data['CartProduct']['display_image'] = $this->request->data['Product']['display_image'];
					$this->request->data['CartProduct']['active'] = $this->request->data['Product']['active'];
					$this->request->data['CartProduct']['pd_expires'] = $this->request->data['Product']['expires'];
					$this->CartProduct->save($this->request->data);
				}

				// delete unwanted images
					if (isset($this->request->data['ImageDelete'])) {
						$delete_images = array();
						foreach ($this->request->data['ImageDelete'] as $key => $value) {
							if ($value == '1') {
								$this->File->deleteByPrefix('product_image/'.$this->Product->id);
								$this->request->data['Product']['image_extension']='';
								$this->Product->save($this->request->data["Product"]);
							}
						}
					}

				// delete unwanted images
					if (isset($this->request->data['ProductImage1Delete'])) {
						$delete_images = array();
						foreach ($this->request->data['ProductImage1Delete'] as $key => $value) {
							if ($value == '1') {
								$this->File->deleteByPrefix('hot_offer_image/'.$this->Product->id);
								$this->request->data['Product']['hotoffer_extension']='';
								$this->Product->save($this->request->data["Product"]);

								$del_deals1 = "DELETE from product_deals where product_id=".$id." AND dashboard_id=1";
								$this->Product->ProductDeal->query($del_deals1);
							}
						}

					}
					if (isset($this->request->data['ProductImage2Delete'])) {
						$delete_images = array();
						foreach ($this->request->data['ProductImage2Delete'] as $key => $value) {
							if ($value == '1') {
								$this->File->deleteByPrefix('hot_offer_img_saf/'.$this->Product->id);
								$this->request->data['Product']['hotoffer_extension']='';
								$this->Product->save($this->request->data["Product"]);

								$del_deals2 = "DELETE from product_deals where product_id=".$id." AND dashboard_id=2";
								$this->Product->ProductDeal->query($del_deals2);
							}
						}

					}
					if (isset($this->request->data['ProductImage3Delete'])) {
						$delete_images = array();
						foreach ($this->request->data['ProductImage3Delete'] as $key => $value) {
							if ($value == '1') {
								$this->File->deleteByPrefix('hot_offer_img_points/'.$this->Product->id);
								$this->request->data['Product']['hotoffer_extension']='';
								$this->Product->save($this->request->data["Product"]);

								$del_deals3 = "DELETE from product_deals where product_id=".$id." AND dashboard_id=3";
								$this->Product->ProductDeal->query($del_deals3);
							}
						}

					}


				// delete app image
					if (isset($this->request->data['ImageAppDelete'])) {
						$delete_images = array();
						foreach ($this->request->data['ImageAppDelete'] as $key => $value) {
							if ($value == '1') {
								$this->File->deleteByPrefix('app_image/'.$this->Product->id);
								$this->request->data['Product']['app_image_extension']='';
								$this->Product->save($this->request->data["Product"]);
							}
						}
					}

				//	save the uploaded files
					if ($this->request->data['ProductImage']['file']['name'] != '') {
						move_uploaded_file($this->request->data['ProductImage']['file']['tmp_name'], PRODUCT_IMAGE_PATH. $this->Product->id. '.'. $this->request->data['Product']['image_extension']);
					}

					if ($this->request->data['ProductImage1']['file']['name'] != '') {
						move_uploaded_file($this->request->data['ProductImage1']['file']['tmp_name'], PRODUCT_HOT_OFFER_IMAGE_PATH. $this->Product->id. '.'. $this->request->data['Product']['hotoffer_extension']);
					}


					if ($this->request->data['ProductAppImage']['file']['name'] != '') {
						move_uploaded_file($this->request->data['ProductAppImage']['file']['tmp_name'], APP_IMAGE_PATH. $this->Product->id. '.'. $this->request->data['Product']['app_image_extension']);
					}
					if ($this->request->data['ProductImageDeal1']['file']['name'] != '') {
						move_uploaded_file($this->request->data['ProductImageDeal1']['file']['tmp_name'], PRODUCT_HOT_OFFER_IMAGE_PATH. $this->Product->id. '.'. $extension1);
					}

					if ($this->request->data['ProductImageDeal2']['file']['name'] != '') {
						move_uploaded_file($this->request->data['ProductImageDeal2']['file']['tmp_name'], PRODUCT_HOT_OFFER_IMAGE_SAF_PATH. $this->Product->id. '.'. $extension2);
					}

					if ($this->request->data['ProductImageDeal3']['file']['name'] != '') {
						move_uploaded_file($this->request->data['ProductImageDeal3']['file']['tmp_name'], PRODUCT_HOT_OFFER_IMAGE_POINTS_PATH. $this->Product->id. '.'. $extension3);
					}

					// get the updated record
						$new_data = $this->Product->findById($this->request->data['Product']['id']);
					//	log the change
						$log = array(
							'id' => null,
							'foreign_id' => $this->{Inflector::singularize($this->name)}->id,
							'foreign_model' => Inflector::singularize($this->name),
							'user_id' => $this->Session->read('user.User.id'),
							'old_data' => $old_data,
							'new_data' => $new_data
						);
						$log['description'] = Inflector::humanize(Inflector::underscore($this->name)). ' modified';
						$log['notes'] = ' ';
						$this->save_log($log);

					$this->Session->setFlash('The Product has been saved');
					$this->redirect_session(array('action'=>'index'), null, true);
					exit();
				}
				else
				{
					$this->Session->setFlash('Please correct the errors below.');
				}
			}
		}
		else{
			$this->Product->recursive = 1;
			$this->request->data = $this->Product->read(null, $id);
			$this->_setRedirect();
		}
		//	get the merchant if specified
		if (empty($merchant_id)) {
			$merchant_id = $this->request->data['Merchant']['id'];
		}

		//	select the category, programs and clients for the category if set
		if (empty($id) && isset($this->passedArgs['category_id'] )) {
			$this->Product->Category->recursive = -1;
			$category = $this->Product->Category->findById($this->passedArgs['category_id']);
			if ($category) {
				$this->request->data['Category'][] = $category['Category'];
				$programs = $this->Product->Program->ProgramsCategory->findAll(array('category_id' => $this->passedArgs['category_id']));
				foreach ($programs as $program) {
					$this->request->data['Program'][] = $program['Program'];
				}
				$clients = $this->Product->Client->ClientsCategory->findAll(array('category_id' => $this->passedArgs['category_id']));
				foreach ($clients as $client) {
					$this->request->data['Client'][] = $client['Client'];
				}
			}
		}
		$this->Product->recursive = 0;
		$this->set('related_products', $this->paginate('Product', "Product.merchant_id = '". $merchant_id. "' AND Product.id != '". $id. "'"));

		$this->set('search_weights', $this->Product->getEnumValues('search_weight'));

		/*
		 * Loading clients list for a specific program
		 */
		$plist = $this->Product->Program->ProgramsProduct->findAllByProductId($id);
		$program_ids = array();
		foreach ($plist as $list) {
			$program_ids[] = $list['ProgramsProduct']['program_id'];
		}

		$this->Product->unbindModel(array('belongsTo' => array('Domain','Voucher')));
		$clients = $this->Product->Program->Client->find('all', array('conditions' =>array('program_id' =>$program_ids ),'order'=>'Client.program_id, Client.name','fields'=>array('Client.id','Client.name','Client.program_id','Program.name'),'recursive'=>0));

		$clientsList = $this->Product->Program->Client->ClientsProduct->find('all', array('conditions' =>array('product_id' =>$id),'fields'=>array('id','client_id')));
		$client_ids = array();
		foreach ($clientsList as $list) {
			$client_ids[] = $list['ClientsProduct']['client_id'];
		}

		/*
		 * loading merchant addresses for the merchant
		 */

		if(empty($param_merchant_id) && isset($this->request->data['Merchant']['id']))
		{
			$merchant_addresses = $this->Product->Merchant->MerchantAddress->find('superlist', array('conditions' =>array('merchant_id' => $this->request->data['Merchant']['id']),'fields'=>array('MerchantAddress.id','MerchantAddress.name','MerchantAddress.address1','MerchantAddress.address2','MerchantAddress.suburb','MerchantAddress.state','MerchantAddress.country'),'separator'=>' '));
		}
		else if(!empty($param_merchant_id))
		{
			$merchant_addresses = $this->Product->Merchant->MerchantAddress->find('superlist', array('conditions' =>array('merchant_id' => $param_merchant_id),'fields'=>array('MerchantAddress.id','MerchantAddress.name','MerchantAddress.address1','MerchantAddress.address2','MerchantAddress.suburb','MerchantAddress.state','MerchantAddress.country'),'separator'=>' '));
		}
		else {
			$merchant_addresses = array();
		}

		/*
		 * Setting checked product deals
		 */
		$deals =$this->Product->ProductDeal->find('all',array('conditions'=>array('ProductDeal.product_id' => $id),'recursive'=>-1, 'fields'=>array('ProductDeal.dashboard_id')));
		$deal_ids = array();
		foreach ($deals as $list) {
			$deal_ids[] = $list['ProductDeal']['dashboard_id'];
		}
		$this->set('deals',$deal_ids);

		/*
		 * Setting default merchant
		 */
		if(!empty($param_merchant_id))
			$this->set('mer_id',$param_merchant_id);
		else
			$this->set('mer_id',$this->request->data['Merchant']['id']);

		$this->Product->Category->recursive = -1;
		$categories = $this->Product->Category->find('list', array('order'=>'description, name', 'fields'=>'id,description'));
		$programs = $this->Product->Program->find('list');
		$merchantslist = $this->Product->Merchant->find('list',array('order'=>'Merchant.name','conditions'=>array('Merchant.name!=""')));

		$this->set('merchantslist',$merchantslist);
		$this->set('programsList',$program_ids);
		$this->set('clientsList',$client_ids);
		$this->set(compact('clients','categories','programs','merchant_addresses'));
		$this->set('display_images', $this->Product->getEnumValues('display_image'));
	}

	function admin_update_merchantaddresses()
	{
		$ma_id = $this->request->data['Product']['merchant_id'];
		$merchant_addresses = $this->Product->Merchant->MerchantAddress->find('superlist', array('conditions' =>array('merchant_id' => $ma_id),'fields'=>array('MerchantAddress.id','MerchantAddress.name','MerchantAddress.address1','MerchantAddress.address2','MerchantAddress.suburb','MerchantAddress.state','MerchantAddress.country'),'separator'=>' '));
		$this->set('merchant_addresses',$merchant_addresses);
        $this->layout = 'ajax';
	}

	function admin_update_program_clients()
	{
		$pid = $this->request->data['Program']['Program'];
		$programs = $this->Product->Program->Client->find('list', array('conditions' =>array('program_id' => $pid)));

		$this->set('program_clients',$programs);
        $this->layout = 'ajax';
	}

	/*
	 * Populate states using ajax for country in admin
	 */
	function admin_update_state_data()
    {
	   	$selected_country_data= $this->request->data['Product']['product_country'];
    	$defaultstatesql_query ="SELECT State.state_cd, State.state_name FROM states AS State, countries as Country, clients as Client
    	where State.country_cd = Country.id and Country.name='".$selected_country_data."'";

    	$this->loadModel("State");
		$cstates_data = $this->State->query($defaultstatesql_query);

    	$ctrystate_data = array();
		foreach ($cstates_data as $ctrystates_data) {
			$ctrystate_data[$ctrystates_data['State']['state_cd']] = $ctrystates_data['State']['state_name'];
		}

        $this->set('ctrystates',$ctrystate_data);
        $this->layout = 'ajax';
    }

	function admin_delete($id = null) {
		$this->check_query_numeric_admin($id);
		if (!$id) {
			$this->Session->setFlash('Invalid id for Product');
			$this->redirect(array('action'=>'index'), null, true);
		}
		if ($this->Product->delete($id)) {

			$pid = $this->CartProduct->find('first',array('conditions'=>array('CartProduct.product_id'=>$id), 'fields'=>array('CartProduct.id')));
			$this->CartProduct->delete($pid['CartProduct']['id']);

			// create a new cURL resource
			/*$ch = curl_init();

			// set URL and other appropriate options
			curl_setopt($ch, CURLOPT_URL, "http://myrewards.com.au:8983/solr/core0/update?stream.body=<delete><query>id:".$id."</query></delete>&commit=true");
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

			// grab URL and pass it to the browser
			curl_exec($ch);

			// close cURL resource, and free up system resources
			curl_close($ch);*/

			$this->Session->setFlash('Product #'.$id.' deleted');
			$this->redirect(array('action'=>'index'), null, true);
		}
	}

	function wishlist($pid, $uid)
	{
		$this->check_query_numeric($pid, $uid);
		if(!empty($pid) && !empty($uid))
		{
			$prod_list = $this->Wishlist->find('all',array('conditions'=>array('Wishlist.user_id'=>$uid, 'Wishlist.product_id'=>$pid),'fields'=>array('Wishlist.product_id')));

			if(empty($prod_list[0]['Wishlist']['product_id']))
			{
				$wdata['Wishlist']['product_id'] = $pid;
				$wdata['Wishlist']['user_id'] = $uid;
				$this->Wishlist->save($wdata);
				$product_used_count = $this->Product->find('first',array('conditions' => array('Product.id' => $pid), 'recursive' => -1, 'fields' => array('Product.used_count')));
				if($product_used_count['Product']['used_count'] > 0 && $product_used_count['Product']['used_count'] != -1)
				{
					$data = array('Product' => array('id' => $pid, 'used_count' => $product_used_count['Product']['used_count'] - 1));
					$this->Product->save($data);
				}
				$this->loadModel("ProductsTracker");
				$product_print_count = $this->ProductsTracker->find('first',array('conditions' => array('ProductsTracker.product_id' => $pid), 'recursive' => -1, 'fields' => array('ProductsTracker.product_id','ProductsTracker.prints')));

				$sql = "update products_trackers as ProductsTracker set
				ProductsTracker.prints =".$product_print_count['ProductsTracker']['prints']."+1 where ProductsTracker.product_id=".$pid;
				$this->ProductsTracker->query($sql);

				$userid = $this->Session->read('user.User.id');
				$this->loadModel("User");

				$user_used_count = $this->User->find('first',array('conditions' => array('User.id' => $uid), 'recursive' => -1, 'fields' => array('User.used_count')));
				if($user_used_count['User']['used_count'] > 0 && $user_used_count['User']['used_count'] != -1)
				{
					$count_data = array('User' => array('id' => $uid, 'used_count' => $user_used_count['User']['used_count'] - 1));
					$this->User->save($count_data);
				}
				$t = "This Product has been added to My Favourites";
			}
			else
			{
				$t ="This Product is already added to My Favourites";
			}
		}
		echo $t;
		$this->layout = 'ajax';
		$this->autoRender = false;
	}

	function view_wishlist()
	{
		$sql1 = "where Wishlist.user_id='".$this->Session->read('user.User.id')."' and Wishlist.product_id=Product.id
		and Product.merchant_id = MerchantAddress.merchant_id and ProductsAddress.merchant_address_id=MerchantAddress.id
		and ProductsAddress.product_id=Product.id and Product.web_coupon = 1 AND Product.active = 1
		AND (Product.expires = '0000-00-00' OR Product.expires >= NOW()) and Product.id = ClientsProduct.product_id
		and Product.merchant_id=Merchant.id AND (ClientsProduct.client_id = '".$this->Session->read('client.Client.id')."' OR ClientsProduct.client_id = 0)
		group by Product.id , MerchantAddress.merchant_id";

		$this->paginate = array('Wishlist' => array('limit' => 5));
		$wish_list = $this->paginate('Wishlist', array('search' =>array('sql' => $sql1)));

		$userid = $this->Session->read('user.User.id');
		$this->loadModel("User");

		$user_used_count = $this->User->find('first',array('conditions' => array('User.id' => $userid), 'recursive' => -1, 'fields' => array('User.used_count')));

		$this->set('usedcount',$user_used_count['User']['used_count']);

		$this->set('wish_list',$wish_list);
	}

	function remove_wishlist($product_id, $user_id)
	{
		$this->check_query_numeric($product_id, $user_id);
		$this->Wishlist->query("delete from wishlists where product_id=".$product_id." and user_id=".$user_id);
		$product_used_count = $this->Product->find('first',array('conditions' => array('Product.id' => $product_id), 'recursive' => -1, 'fields' => array('Product.used_count')));
		if($product_used_count['Product']['used_count'] > 0 && $product_used_count['Product']['used_count'] != -1)
		{
			$data = array('Product' => array('id' => $product_id, 'used_count' => $product_used_count['Product']['used_count'] + 1));
			$this->Product->save($data);
		}

		$userid = $this->Session->read('user.User.id');
		$this->loadModel("User");

		$user_used_count = $this->User->find('first',array('conditions' => array('User.id' => $user_id), 'recursive' => -1, 'fields' => array('User.used_count')));
		if($user_used_count['User']['used_count'] > 0 && $user_used_count['User']['used_count'] != -1)
		{
			$count_data = array('User' => array('id' => $user_id, 'used_count' => $user_used_count['User']['used_count'] + 1));
			$this->User->save($count_data);
		}
		$this->Session->setFlash('Product deleted from Your Favourites');
		$this->redirect('/products/view_wishlist/');
	}

	function clear_wishlist()
	{
		$this->Wishlist->query("delete from wishlists");
		$this->Session->setFlash('Your Favourites is cleared');
		$this->redirect('/products/view_wishlist/');
	}

	function reset_data()
		{
			$ctry = $this->Session->read('client.Client.country');

	       	// Query to get categories array based on selected country from drop down

	    	$catsql ="SELECT DISTINCT Category.* FROM categories AS Category
	    	LEFT JOIN clients_categories AS ClientsCategory ON Category.id = ClientsCategory.category_id
	    	AND CategoriesProduct.product_id = ClientsProduct.product_id
	    	WHERE ClientsCategory.client_id = '". $this->Session->read('client.Client.id') ."' and Category.countries = '".$ctry."'";

	    	$this->loadModel("Category");
			$ctgry = $this->Category->query($catsql);

	    	$countrycat = array();
			foreach ($ctgry as $category1) {
				$countrycat[$category1['Category']['id']] = $category1['Category']['name'];
			}

			// Query to get states array based on selected country from drop down

		    $statesql ="SELECT State.state_cd, State.state_name FROM states AS State, countries as Country, clients as Client,
		    merchant_addresses as MerchantAddress, products as Product where State.country_cd = Country.id and Country.name='".$ctry."'
		    and MerchantAddress.merchant_id = Product.merchant_id and MerchantAddress.state = State.state_cd
		    and Client.id = ".$this->Session->read('client.Client.id');

	    	$this->loadModel("State");
			$state = $this->State->query($statesql);

	    	$statectry = array();
			foreach ($state as $state1) {
				$statectry[$state1['State']['state_cd']] = $state1['State']['state_name'];
			}

			$this->set('statelist',$statectry);
			$this->set('categorylist',$countrycat);
		 	$this->layout = 'ajax';
	}

	function autocomplete ()
	{
		$first='';$second='';$third='';$fourth='';

		$search = addslashes(trim($_GET['term']));
		$explode=explode(",",$search);
		$count=count($explode);
		$message = "";

		if ($search)  {
			if ($count == 1)
			{
				$sql="Select distinct MerchantAddress.suburb,MerchantAddress.state,MerchantAddress.country from merchant_addresses as MerchantAddress,
				products as Product, merchants as Merchant, clients_products as ClientProduct, clients as Client
				where MerchantAddress.suburb like '".$search."%' and Product.merchant_id=Merchant.id and Product.active=1
				and Product.cancelled = 0 AND (Product.expires = '0000-00-00' OR Product.expires >= NOW()) and Merchant.id=MerchantAddress.merchant_id
				and Product.id=ClientProduct.product_id and ClientProduct.client_id='". $this->Session->read('client.Client.id') ."'
				and Client.id=ClientProduct.client_id";
				$first= $this->Product->query($sql);

				$sql2="Select distinct MerchantAddress.city,MerchantAddress.state,MerchantAddress.country from merchant_addresses as MerchantAddress,
				products as Product, merchants as Merchant, clients_products as ClientProduct, clients as Client
				where  MerchantAddress.city like '".$search."%' and Product.merchant_id=Merchant.id and Product.active=1
				and Product.cancelled = 0 AND (Product.expires = '0000-00-00' OR Product.expires >= NOW()) and Merchant.id=MerchantAddress.merchant_id
				and Product.id=ClientProduct.product_id and ClientProduct.client_id='". $this->Session->read('client.Client.id') ."'
				and Client.id=ClientProduct.client_id";
				$second= $this->Product->query($sql2);

				$sql3="Select distinct MerchantAddress.state,MerchantAddress.country from merchant_addresses as MerchantAddress,
				products as Product, merchants as Merchant, clients_products as ClientProduct, clients as Client
				where MerchantAddress.state like '".$search."%' and Product.merchant_id=Merchant.id and Product.active=1
				and Product.cancelled = 0 AND (Product.expires = '0000-00-00' OR Product.expires >= NOW()) and Merchant.id=MerchantAddress.merchant_id
				and Product.id=ClientProduct.product_id and ClientProduct.client_id='". $this->Session->read('client.Client.id') ."'
				and Client.id=ClientProduct.client_id";
				$third= $this->Product->query($sql3);

				$sql4="Select distinct MerchantAddress.country from merchant_addresses as MerchantAddress,
				products as Product, merchants as Merchant, clients_products as ClientProduct, clients as Client
				where MerchantAddress.country like '".$search."%' and Product.merchant_id=Merchant.id and Product.active=1
				and Product.cancelled = 0 AND (Product.expires = '0000-00-00' OR Product.expires >= NOW()) and Merchant.id=MerchantAddress.merchant_id
				and Product.id=ClientProduct.product_id and ClientProduct.client_id='". $this->Session->read('client.Client.id') ."'
				and Client.id=ClientProduct.client_id";
				$fourth= $this->Product->query($sql4);
			}
			else if ($count == 2)
			{
				$sql="Select distinct MerchantAddress.suburb,MerchantAddress.state,MerchantAddress.country from merchant_addresses as MerchantAddress,
				products as Product, merchants as Merchant, clients_products as ClientProduct, clients as Client
				where MerchantAddress.suburb like '".trim($explode[0])."%' AND MerchantAddress.state like '".trim($explode[1])."%'
				and Product.merchant_id=Merchant.id and Product.active=1
				and Product.cancelled = 0 AND (Product.expires = '0000-00-00' OR Product.expires >= NOW()) and Merchant.id=MerchantAddress.merchant_id
				and Product.id=ClientProduct.product_id and ClientProduct.client_id='". $this->Session->read('client.Client.id') ."'
				and Client.id=ClientProduct.client_id";
				$first= $this->Product->query($sql);

				$sql2="Select distinct MerchantAddress.city,MerchantAddress.state,MerchantAddress.country from merchant_addresses as MerchantAddress,
				products as Product, merchants as Merchant, clients_products as ClientProduct, clients as Client
				where  MerchantAddress.city like '".trim($explode[0])."%' AND MerchantAddress.state like '".trim($explode[1])."%'
				and Product.merchant_id=Merchant.id and Product.active=1
				and Product.cancelled = 0 AND (Product.expires = '0000-00-00' OR Product.expires >= NOW()) and Merchant.id=MerchantAddress.merchant_id
				and Product.id=ClientProduct.product_id and ClientProduct.client_id='". $this->Session->read('client.Client.id') ."'
				and Client.id=ClientProduct.client_id";
				$second= $this->Product->query($sql2);

				$sql3="Select distinct MerchantAddress.state,MerchantAddress.country from merchant_addresses as MerchantAddress,
				products as Product, merchants as Merchant, clients_products as ClientProduct, clients as Client
				where MerchantAddress.state like '".trim($explode[0])."%' AND MerchantAddress.country like '".trim($explode[1])."%'
				and Product.merchant_id=Merchant.id and Product.active=1
				and Product.cancelled = 0 AND (Product.expires = '0000-00-00' OR Product.expires >= NOW()) and Merchant.id=MerchantAddress.merchant_id
				and Product.id=ClientProduct.product_id and ClientProduct.client_id='". $this->Session->read('client.Client.id') ."'
				and Client.id=ClientProduct.client_id";
				$third= $this->Product->query($sql3);
			}
			else if ($count == 3)
			{
				$sql="Select distinct MerchantAddress.suburb,MerchantAddress.state,MerchantAddress.country from merchant_addresses as MerchantAddress,
				products as Product, merchants as Merchant, clients_products as ClientProduct, clients as Client
				where MerchantAddress.suburb like '".trim($explode[0])."%' AND MerchantAddress.state like '".trim($explode[1])."%'
				AND MerchantAddress.country like '".trim($explode[2])."%' and Product.merchant_id=Merchant.id and Product.active=1
				and Product.cancelled = 0 AND (Product.expires = '0000-00-00' OR Product.expires >= NOW()) and Merchant.id=MerchantAddress.merchant_id
				and Product.id=ClientProduct.product_id and ClientProduct.client_id='". $this->Session->read('client.Client.id') ."'
				and Client.id=ClientProduct.client_id";
				$first= $this->Product->query($sql);

				$sql2="Select distinct MerchantAddress.city,MerchantAddress.state,MerchantAddress.country from merchant_addresses as MerchantAddress,
				products as Product, merchants as Merchant, clients_products as ClientProduct, clients as Client
				where  MerchantAddress.city like '".trim($explode[0])."%' AND MerchantAddress.state like '".trim($explode[1])."%'
				AND MerchantAddress.country like '".trim($explode[2])."%' and Product.merchant_id=Merchant.id and Product.active=1
				and Product.cancelled = 0 AND (Product.expires = '0000-00-00' OR Product.expires >= NOW()) and Merchant.id=MerchantAddress.merchant_id
				and Product.id=ClientProduct.product_id and ClientProduct.client_id='". $this->Session->read('client.Client.id') ."'
				and Client.id=ClientProduct.client_id";
				$second= $this->Product->query($sql2);
			}
		}

		if(empty($first) && empty($second) && empty($third) && empty($fourth))
		{
			$message = "No Offers Found";
		}else{
			$message = "";
		}
		$this->set('msg',$message);
		$this->set('posts',$first);
		$this->set('posts2',$second);
		$this->set('posts3',$third);
		$this->set('posts4',$fourth);
	}

	function products_expire()
	{
		$prods = array();
		$sql = "select id, name, expires, active, cancelled from products as Product where Product.expires< curdate()
		and Product.expires != 0000-00-00 and ((Product.active=1 and Product.cancelled =0) or (Product.active=0 and Product.cancelled =0)
		or (Product.active=1 and Product.cancelled =1))";
		$products = $this->Product->query($sql);
		foreach($products as $product)
		{
			$update_sql = "update products as Product set Product.cancelled=1, Product.active=0 where Product.id=".$product['Product']['id'];
			$prods = $this->Product->query($update_sql);
		}
		$this->autoRender=false;
	}

	function search_products ()
		{
			App::import('Vendor','Service', array('file'=>'Solr'.DS.'Service.php'));

			App::import("Model", "ClientsCategory");
			$this->ClientsCategory = new ClientsCategory();
			$this->ClientsCategory->unbindModel(array('belongsTo' => array('Client')));
			$clientCategories = $this->ClientsCategory->find("all", array("conditions"=>array("ClientsCategory.client_id"=>$this->Session->read('client.Client.id')),
					"fields"=>"Category.name"));
			$catQuery = "";
			foreach($clientCategories as $clientCategory){
				$catQuery .= strlen($catQuery)>0?" OR ":"";
				$catQuery .= 'categoryNm:"'.$clientCategory["Category"]["name"].'"';
			}
			App::import("Model", "ClientsCountry");
			$this->ClientsCountry = new ClientsCountry();
			$this->ClientsCountry->bindModel(array( 'hasOne' => array('Country'=>array('foreignKey'=>false, 'conditions'=>'ClientsCountry.country_id=Country.id'))));
			$clientCountries = $this->ClientsCountry->find("all", array("conditions"=>array("ClientsCountry.client_id"=>$this->Session->read('client.Client.id')),
					"fields"=>"Country.name"));
			$countryQuery = 'maCountryStr:"'.urldecode($this->Session->read('client.Client.country')).'"';
			foreach($clientCountries as $clientCountry){
				$countryQuery .= strlen($countryQuery)>0?" OR ":"";
				$countryQuery .= 'maCountryStr:"'.$clientCountry["Country"]["name"].'"';
			}

			$dashboardid=$this->Session->read('dashboardidinsession');
			$corePath = 'core0/';
			if($dashboardid == 2){
				$corePath = 'core2/';
			}else if($dashboardid == 3){
				$corePath = 'core3/';
			}

			$solr = new Apache_Solr_Service('localhost', '8983', '/solr', false, $corePath);

			if (!$solr->ping())
			    $this->redirect ( "/" );

			$keyword = trim ($_POST['keyword']) != '' ? urldecode ($_POST['keyword']) : '';

			if(isset($_GET['key']))
				$keyword = trim ($_GET['key']) != '' ? urldecode ($_GET['key']) : '';

			$conditions = array();
			array_push($conditions, '(clientId:'.$this->Session->read('client.Client.id').' OR clientId:0)');
			if(!empty($catQuery)){
				array_push($conditions, '('.$catQuery.')');
			}
			if(!empty($countryQuery)){
				array_push($conditions, '('.$countryQuery.')');
			}

			if(isset($_GET['cat']))
			{
				$cat = trim ($_GET['cat']) != '' ? urldecode ($_GET['cat']) : '';
				array_push($conditions, 'category:"'.urldecode($cat).'"');
			}
			if(isset($_GET['merchant']))
			{
				$merchant = trim ($_GET['merchant']) != '' ? urldecode ($_GET['merchant']) : '';
				array_push($conditions, 'merchant:"'.urldecode($merchant).'"');
			}
			if(isset($_GET['ctry']))
			{
				$ctry = trim ($_GET['ctry']) != '' ? urldecode ($_GET['ctry']) : '';
				array_push($conditions, 'maCountryStr:"'.urldecode($ctry).'"');
			}
			if(isset($_GET['state']))
			{
				$state = trim ($_GET['state']) != '' ? urldecode ($_GET['state']) : '';
				array_push($conditions, 'maStateStr:"'.urldecode($state).'"');
			}
			if(isset($_GET['suburb']))
			{
				$suburb = trim ($_GET['suburb']) != '' ? urldecode ($_GET['suburb']) : '';
				array_push($conditions, 'maSuburb:"'.urldecode($suburb).'"');
			}

			$additionalParameters = array('fq' => $conditions, 'facet'=>'true', 'facet.field'=>array('categoryNm','merchantNm','maCountryStr','maStateStr','maSuburb'),'facet.mincount'=>1,'qf' => 'name prdtHighlight prdtDetails prdtKeyword prdtText prdtSpclOffer prdtSpclOfferHeadline prdtSpclOfferBody maSuburb maPostCode category merchant prdtHotOffer prdtFavourites maCountry maState','bf' => 'prdtSearchWeight merchantSearchWeight categorySearchWeight', 'defType' => 'dismax');

			$sortoption = $_GET['sort'];
			if(isset($sortoption) && $sortoption!='default')
			{
				$sort = $sortoption. ' asc';
				$additionalParameters['sort'] = $sort;
			}

			$per_page= 15;
			if($_GET['page']>1)
			{
				$page = (!isset($_GET["page"]) ? 1 : $_GET["page"]);
				$limit =15;
				$offset = ($page * $limit) - $limit;
			}
			else
			{
				$offset = 0;
				$limit = 15;
			}
			$query=$keyword."";
			$res = $solr->search($query, $offset, $limit,$additionalParameters);

			$jsondata = $res->getRawResponse();
			$array = json_decode($jsondata, true);

			$adCnt = $array['response']['numFound'];
			if($adCnt>0)
			{
				$products = $array['response']['docs'];
			}

			foreach ($products as $key => $product)
			{
				$address_count = $this->Product->Merchant->MerchantAddress->find('count', array('conditions' => array('MerchantAddress.merchant_id' => $product['merchantId'])));

				if ($address_count == '0') {
					$products[$key]['MerchantAddress'] = 'none';
				} else if ($address_count > 1) {
					$products[$key]['MerchantAddress'] = 'multiple';
				} else {
					$merchant_address = $this->Product->Merchant->MerchantAddress->find('first', array('conditions' => array('MerchantAddress.merchant_id' => $product['merchantId'])));
					$products[$key]['MerchantAddress'] = $merchant_address['MerchantAddress'];
				}
			}

             //tracks products
               foreach($products as $pr){		
				      $pid[] = $pr['id'];						  
				   } 
				   
				   $product_type = 'SOLR';			   
				   $this->recordActivity($pid,$product_type);	 	
      

			$categories = $array['facet_counts']['facet_fields']['categoryNm'];
			$merchants = $array['facet_counts']['facet_fields']['merchantNm'];
			$country = $array['facet_counts']['facet_fields']['maCountryStr'];
			$stateCount = $array['facet_counts']['facet_fields']['maStateStr'];
			$suburbCount = $array['facet_counts']['facet_fields']['maSuburb'];

			$this->loadModel("Skyscraper");
			$banner_sql = "select Skyscraper.* from skyscrapers as Skyscraper, clients_skyscrapers as ClientsSkyscraper,
			programs_skyscrapers as ProgramsScraper where ClientsSkyscraper.client_id=".$this->Session->read('client.Client.id')." and
			ProgramsScraper.program_id=".$this->Session->read('client.Client.program_id')." and Skyscraper.id=ClientsSkyscraper.skyscraper_id
			and Skyscraper.id=ProgramsScraper.skyscraper_id order by rand() limit 1";

			$banner_list = $this->Skyscraper->query($banner_sql);

			$this->set('bannerlist',$banner_list);

			$userid = $this->Session->read('user.User.id');
			$this->loadModel("User");

			$user_used_count = $this->User->find('first',array('conditions' => array('User.id' => $userid), 'recursive' => -1, 'fields' => array('User.used_count')));

			$this->set('usedcount', $user_used_count['User']['used_count']);

			$this->set('catCounts',$categories);
			$this->set('products',$products);
			$this->set('keyword',$keyword);
			$this->set('merchants',$merchants);
			$this->set('countries',$country);
			$this->set('states',$stateCount);
			$this->set('suburbs',$suburbCount);
			$this->set('sort',$sortoption);
			$this->set('count',$adCnt);
			$this->set('perpage',$per_page);
			if(!isset($_GET['page']))
				$_GET['page'] = 1;
			$this->set('page1',$_GET['page']);
			if(!empty($ctry))
				$this->set('ctry',$ctry);
			if(!empty($state))
				$this->set('state',$state);
			if(!empty($merchant))
				$this->set('merchant',$merchant);
			if(!empty($cat))
				$this->set('cat',$cat);
			if(!empty($suburb))
				$this->set('suburb',$suburb);
		}

		function redeemlist($pid, $uid)
		{
			$this->check_query_numeric($pid, $uid);
			if(!empty($pid) && !empty($uid))
				{
					$redeem_product_query ="select Product.name,ProductsPoint.points from products Product,products_points ProductsPoint where Product.id=ProductsPoint.product_id and Product.id=".$pid;
					$redeem_product=$this->ProductsPoint->query($redeem_product_query);
					$redeem_user = $this->User->find('first',array('conditions' => array('User.id' => $uid), 'recursive' => -1, 'fields' => array('User.points','User.client_id','User.username')));

					$redeem_product_points=$redeem_product['0']['ProductsPoint']['points'];
					$redeem_user_points=$redeem_user['User']['points'];
					$redeem_client = $this->User->Client->find('first',array('conditions' => array('Client.id' => $redeem_user['User']['client_id']), 'recursive' => -1, 'fields' => array('Client.email')));


					if($redeem_user_points >= $redeem_product_points)
					{ /*
						$user_redem_rem_points=$redeem_user_points-$redeem_product_points;
						$redeem_tracker_query = "INSERT INTO redeem_trackers ( user_id, product_id,points,activity,activity_date) VALUES ( $uid, $pid,$redeem_product_points,'Product Redeemed',CURDATE())";
						$this->RedeemTracker->query($redeem_tracker_query);
						$redeem_user_update_query = "UPDATE users  SET points = ".$user_redem_rem_points." WHERE id=".$uid;
						$this->RedeemTracker->query($redeem_user_update_query);

						//sending mail to client admin of user
						$subject = "Redeemed Product Details";
						$message.="Dear Admin , ".$redeem_user['User']['username']." has redeemed the product with following Details!<br>";
						$message.="User Name :".$redeem_user['User']['username']."<br>";
						$message.="Product Name :".$redeem_product['0']['Product']['name']."<br>";
						$message.="Product Points :".$redeem_product_points."<br>";
						$message.="<p>Regards,</p><p>Team support@therewardsteam.com </p>";
						$headers  = "From: support@therewardsteam.com\r\n";
						$headers .= "Content-type: text/html\r\n";
						$mail = mail($redeem_client['Client']['email'], $subject, $message, $headers);

						//sending mail to user who is redeemed the product
						$usermessage.="Dear ".$redeem_user['User']['username']." ,You are redeemed the product with following Details!<br>";
						$usermessage.="User Name :".$redeem_user['User']['username']."<br>";
						$usermessage.="Product Name :".$redeem_product['0']['Product']['name']."<br>";
						$usermessage.="Product Points :".$redeem_product_points."<br>";
						$usermessage.="<p>Regards,</p><p>Team support@therewardsteam.com </p>";
						$mail = mail($redeem_user['User']['email'], $subject, $usermessage, $headers);



						$t = "This Product has been Redeemed";
					*/
					}
					else
					{
						$t ="You do not have sufficient amount of points";
					}
				}
					$this->set('text1',$t);
					$this->layout = 'ajax';
					//$this->autoRender = false;
		}


		function auto_complete_keyword()
		{
			App::import('Vendor','Service', array('file'=>'Solr'.DS.'Service.php'));

			$solr = new Apache_Solr_Service('localhost', '8983', '/solr', false, 'core1/');

			if (!$solr->ping())
			    $this->redirect ( "/" );

			$conditions = array();
			array_push($conditions, 'clientId:'.$this->Session->read('client.Client.id'));

			$keyword = $_GET['term'];
			$additionalParameters = array( 'fq' => $conditions);

			$offset = 0;
			$limit = 15;


			$query="(name:\"".$keyword."\" OR merchant:\"".$keyword."\")";

			$res = $solr->search($query, $offset, $limit, $additionalParameters);
			$jsondata = $res->getRawResponse();
			$array = json_decode($jsondata, true);

			$adCnt = $array['response']['numFound'];
			if($adCnt>0)
			{
				$products = $array['response']['docs'];
			}
			$this->set('products', $products);
			$this->set('kword',$keyword);
			$this->layout = 'ajax';
	}

	function sendsms($id,$numbers,$code)
	{
		if(!empty($id) && is_numeric($id))
		{
			$prodDetails = $this->Product->find('first',array('conditions'=>array('Product.id'=>$id), 'fields'=>array('Product.name','Product.highlight')));

			$NewHighlight= $prodDetails['Product']['highlight'];
			$ProductName= $prodDetails['Product']['name'];

		    $sessionUserName = $this->Session->read('user.User.username');
		    $expiry= date("d-m-Y", strtotime('+ 30 days'));
		    $message = 'MID:'.$sessionUserName.', Offer:'.$ProductName.','.$NewHighlight.', Ends:'.$expiry;

			$sessionClientId=$this->Session->read('user.Client.id');
			$sessionUserId = $this->Session->read('user.User.id');  //retrieving current logged user id from the session
	        $sessionClientCountry=$this->Session->read('client.Client.country'); //retrieving current logged client country from the session
	        $sessionClientName=$this->Session->read('client.Client.name');
			$users=$this->User->find('all',
	            array('conditions'=>array('User.id'=>$sessionUserId), 'fields'=>array('User.id','User.mobile', 'User.username','User.type'), 'recursive'=>-1));

		    foreach($users as $user)
		    {
		    	$username=$user['User']['username'];
	            $userMobile= $user['User']['mobile'];
	            $userType=$user['User']['type'];
			}

			$number_array=array();
			$number = preg_replace( '/\s+/', '', $numbers);

	        $number_array=explode(",", $number);
	        $narray = array();
	        foreach($number_array as $number) {
	        	array_push($narray, $code.$number);
	        }

	        if(!empty($number))
	        {
	                $fromNumber = $sessionClientName;
		        $this->PanaceaSms->username = "testsample"; /* This is your Panacea Mobile username */
		        $this->PanaceaSms->password = "infozeal"; /* This is your Panacea Mobile password */
		        $this->PanaceaSms->delivery_report_mask = 31; /* This is for all statuses */
		        $smsid= $this->PanaceaSms-> generateMessageId();

	                $this->PanaceaSms->delivery_report_url = Router::url(array('action' => 'receive_delivery_report'), true);

	                if($this->PanaceaSms->Send($narray, $message,$fromNumber))
	                {
	                $status = "sent";
	                $this->request->data['Detail']['client_id'] = $sessionClientId;
	                $this->request->data['Detail']['product_id'] = $id;
	                $this->request->data['Detail']['user_name'] = $username;
			        $this->request->data['Detail']['user_type'] = $userType;
			        $this->request->data['Detail']['message'] = $message;
			        $this->request->data['Detail']['status'] = $status;
			        $this->request->data['Detail']['mobile_no'] = $number;
			        $this->request->data['Detail']['smsid'] = $smsid;
			        $this->request->data['Detail']['userid'] = $sessionUserId;
			        $this->request->data['Detail']['modified'] =  date('Y-m-d h:i:s');

	                $this->Detail->save($this->request->data);
	               	$this->Session->setFlash("Message Sent Successfully");
				}
			}

			$this->autoRender = false;
	       	// $this->redirect( '/products/search2' );
	        $this->redirect($_SERVER["HTTP_REFERER"]);
		}
		else {
			$this->redirect('/');
		}

	}

	function sendsmsSolr($id,$numbers,$code,$key)
	{
		if(!empty($id) && is_numeric($id))
		{
			$prodDetails = $this->Product->find('first',array('conditions'=>array('Product.id'=>$id), 'fields'=>array('Product.name','Product.highlight')));

			$NewHighlight= $prodDetails['Product']['highlight'];
			$ProductName= $prodDetails['Product']['name'];

			$sessionUserName = $this->Session->read('user.User.username');
			$expiry= date("d-m-Y", strtotime('+ 30 days'));
			$message = 'MID:'.$sessionUserName.', Offer:'.$ProductName.','.$NewHighlight.', Ends:'.$expiry;

			$sessionClientId=$this->Session->read('user.Client.id');
			$sessionUserId = $this->Session->read('user.User.id');  //retrieving current logged user id from the session
			$sessionClientCountry=$this->Session->read('client.Client.country'); //retrieving current logged client country from the session
			$sessionClientName=$this->Session->read('client.Client.name');
			$users=$this->User->find('all',
			array('conditions'=>array('User.id'=>$sessionUserId), 'fields'=>array('User.id','User.mobile', 'User.username','User.type'), 'recursive'=>-1));

			foreach($users as $user)
			{
				$username=$user['User']['username'];
				$userMobile= $user['User']['mobile'];
				$userType=$user['User']['type'];
			}

			$number_array=array();

			$number = preg_replace( '/\s+/', '', $numbers);
			$number_array=explode(",", $number);
			$narray = array();
			foreach($number_array as $number) {
				array_push($narray, $code.$number);
			}

			if(!empty($number))
			{
				$fromNumber = $sessionClientName;
				$this->PanaceaSms->username = "testsample"; /* This is your Panacea Mobile username */
				$this->PanaceaSms->password = "infozeal"; /* This is your Panacea Mobile password */
				$this->PanaceaSms->delivery_report_mask = 31; /* This is for all statuses */
				$smsid= $this->PanaceaSms-> generateMessageId();

				$this->PanaceaSms->delivery_report_url = Router::url(array('action' => 'receive_delivery_report'), true);

				if($this->PanaceaSms->Send($narray, $message,$fromNumber))
				{
					$status = "sent";
					$this->request->data['Detail']['client_id'] = $sessionClientId;
					$this->request->data['Detail']['product_id'] = $id;
					$this->request->data['Detail']['user_name'] = $username;
					$this->request->data['Detail']['user_type'] = $userType;
					$this->request->data['Detail']['message'] = $message;
					$this->request->data['Detail']['status'] = $status;
					$this->request->data['Detail']['mobile_no'] = $number;
					$this->request->data['Detail']['smsid'] = $smsid;
					$this->request->data['Detail']['userid'] = $sessionUserId;
					$this->request->data['Detail']['modified'] =  date('Y-m-d h:i:s');

					$this->Detail->save($this->request->data);
					$this->Session->setFlash("Message Sent Successfully");
				}
			}
			$urlKey = '/products/search_products/?key='.$key;
			$this->autoRender = false;
			$this->redirect($urlKey);
		//$this->redirect($_SERVER["HTTP_REFERER"]);
		}
		else {
			$this->redirect('/');
		}
	}

	function receive_delivery_report()
	{
		$details = $this->PanaceaSms->getDeliveryReport();
		if($details !== FALSE)
		{
        	/* original to, from, message and status returned in an array */
        }
	}

	function add_to_cart($prodId)
		{
			$prodDetails = $this->Product->find('first',array('conditions'=>array('Product.id'=>$prodId),'recursive'=>-1));

			$clientId = $this->Session->read('client.Client.id');
			$userId = $this->Session->read('user.User.id');
			$merchantId = $prodDetails['Product']['merchant_id'];
			$pname = $prodDetails['Product']['name'];

			$this->request->data['ProductsCart']['product_id'] = $prodId;
			$this->request->data['ProductsCart']['merchant_id'] = $merchantId;
			$this->request->data['ProductsCart']['user_id'] = $userId;
			$this->request->data['ProductsCart']['client_id'] = $clientId;
			$this->request->data['ProductsCart']['quantity'] = 1;

			$prod_list = $this->Product->ProductsCart->find('first',array('conditions'=>array('ProductsCart.product_id'=>$prodId,'ProductsCart.user_id'=>$userId),'fields'=>array('ProductsCart.product_id')));

			if(empty($prod_list['ProductsCart']['product_id']))
			{
				$this->Product->ProductsCart->save($this->request->data);

				$add = "This Product has been added to Cart";
			}
			else
			{
				$add ="This Product is already added to Cart";
			}
			$this->set('text',$add);
			$this->layout = 'ajax';

		}

		function show_cart()
		{
			$prodCart = $this->Product->ProductsCart->find('all',array('conditions'=>array('ProductsCart.user_id'=>$this->Session->read('user.User.id'),'ProductsCart.checkout'=>0),'recursive'=>0));
			$this->set('cartDetails',$prodCart);

			$prodOrders = $this->Product->ProductsCart->find('all',array('conditions'=>array('ProductsCart.user_id'=>$this->Session->read('user.User.id'),'ProductsCart.checkout'=>1),'recursive'=>0));
			$this->set('cartOrders',$prodOrders);

			$this->Session->write('cartProducts',$prodCart);
		}

		function update_cart($id,$qty)
		{
			$sql = "UPDATE products_carts as ProductsCart set ProductsCart.quantity = ".$qty." where ProductsCart.product_id=".$id." and ProductsCart.user_id=".$this->Session->read('user.User.id');
			$this->Product->ProductsCart->query($sql);

			$prod_list = $this->Product->ProductsCart->find('first',array('conditions'=>array('ProductsCart.product_id'=>$id),'fields'=>array('Product.price','ProductsCart.quantity'),'recursive'=>0));

			$total = $prod_list['ProductsCart']['quantity'] * $prod_list['Product']['price'];
			$this->set("total",$total);
			$this->layout = 'ajax';
		}

		function remove_cart($pid)
		{
			$this->Product->ProductsCart->query("delete from products_carts where product_id=".$pid." and user_id=".$this->Session->read('user.User.id'));
			$this->Session->setFlash('Product deleted from cart');
			$this->redirect('/products/show_cart/');
		}

		function clear_cart()
		{
			$this->Product->ProductsCart->query("delete from products_carts");
			$this->Session->setFlash('Your Cart is cleared');
			$this->redirect('/products/show_cart/');
		}
		function online_request()
		{
			$this->loadModel('Country');
			$countries = $this->Country->find('list', array('fields' => array('Country.name')));

			$this->set('countries',$countries);
		}
		function checkout()
		{
			//$url = $_SERVER['HTTP_REFERER'];
			$products = $this->Session->read('cartProducts');

			//$surl = $this->request->data['ProductsCart']['url'];
			$email = $this->request->data['ProductsCart']['email'];
			$address1 = $this->request->data['ProductsCart']['address1'];
			$address2 = $this->request->data['ProductsCart']['address2'];
			$mobile = $this->request->data['ProductsCart']['mobile'];
			$country = $this->request->data['ProductsCart']['country'];
			$state = $this->request->data['ProductsCart']['state'];

			$subject_user = "Test Your Online Request";
			$message_user = "<p>Dear ".$this->Session->read('user.User.first_name')." ".$this->Session->read('user.User.last_name').",</p>";
			$message_user.= "<p>You have requested the below products: </p>";

			$subject_admin = "Test Users Online Request";
			$message_admin = "<p>Dear Admin!, </p>";
			$message_admin.= "<p>".$this->Session->read('user.User.first_name')." ".$this->Session->read('user.User.last_name')." has requested the below products: </p>";
			$message_admin.= "<p><strong>User Name : </strong>".$this->Session->read('user.User.username')."</p> ";

			foreach($products as $product)
			{
				$sql = "UPDATE products_carts as ProductsCart set address1 ='".$address1."', address2 = '".$address2."', mobile =".$mobile.",
				country = '".$country."', state = '".$state."', email = '".$email."', ProductsCart.checkout = 1 WHERE ProductsCart.product_id = ".$product['Product']['id']."
				and ProductsCart.user_id = ".$this->Session->read('user.User.id')." and ProductsCart.client_id = ".$this->Session->read('client.Client.id');

				$this->Product->ProductsCart->query($sql);

				$plist = $this->Product->ProductsCart->find('first',array('conditions'=>array('ProductsCart.product_id'=>$product['Product']['id']),'recursive'=>0));

				if($plist['Product']['quantity']>0 && ($plist['Product']['quantity'] >= $plist['ProductsCart']['quantity']))
				{
					$updateSql = "UPDATE products as Product set quantity=".($plist['Product']['quantity'] - $plist['ProductsCart']['quantity'])." WHERE Product.id=".$product['Product']['id'];
					$this->Product->query($updateSql);
				}

				$message_user.= "<p><strong>Product Name : </strong>".$product['Product']['name']." </p> ";
				$message_user.= "<p><strong>Hightlight : </strong>".$product['Product']['highlight']. "</p> ";
				$message_user.= "<p><strong>Quantity : </strong>".$plist['ProductsCart']['quantity']." </p> ";
				$message_user.= "<p><strong>Ordered Date : </strong>".date('Y-m-d', strtotime($plist['ProductsCart']['modified']))." </p><hr>";


				$message_admin.= "<p><strong>Product Name : </strong>".$product['Product']['name']." </p> ";
				$message_admin.= "<p><strong>Hightlight : </strong>".$product['Product']['highlight']. "</p> ";
				$message_admin.= "<p><strong>Quantity : </strong>".$plist['ProductsCart']['quantity']." </p> ";
				$message_admin.= "<p><strong>Ordered Date : </strong>".date('Y-m-d', strtotime($plist['ProductsCart']['modified']))." </p><hr>";

			}

			$message_user.="<p>Regards,</p><p>Team ".$this->Session->read('client.Client.name')."</p>";

			$headers  = "From: kiran.ganta@infozeal.com\r\n";
			$headers .= "Content-type: text/html\r\n";
			mail($email, $subject_user, $message_user, $headers);

			$message_admin.="<p>Regards,</p><p>".$this->Session->read('user.User.first_name')." ".$this->Session->read('user.User.last_name')."</p>";
			$headers1  = "From: ".$email."\r\n";
			$headers1 .= "Content-type: text/html\r\n";
			mail('kiran.ganta@infozeal.com', $subject_admin, $message_admin, $headers1);

			$this->redirect('/products/show_cart/');
		}

		function online_request_solr($pid,$keyword)
		{
			$url = $_SERVER['HTTP_REFERER'];

			if(isset($this->request->data['ProductsCart']['online']))
			{
				$surl = $this->request->data['ProductsCart']['url'];
				$prodDetails = $this->Product->find('first',array('conditions'=>array('Product.id'=>$pid),'recursive'=>-1));

				$clientId = $this->Session->read('client.Client.id');
				$userId = $this->Session->read('user.User.id');
				$merchantId = $prodDetails['Product']['merchant_id'];
				$email = $this->request->data['ProductsCart']['email'];
				$pname = $prodDetails['Product']['name'];

				$this->request->data['ProductsCart']['product_id'] = $pid;
				$this->request->data['ProductsCart']['merchant_id'] = $merchantId;
				$this->request->data['ProductsCart']['user_id'] = $userId;
				$this->request->data['ProductsCart']['client_id'] = $clientId;

				if($this->Product->ProductsCart->save($this->request->data))
				{
					$subject_user = "Test Your Online Request";
					$message_user = "You have requested the product ".$pname;

					$headers  = "From: kiran.ganta@infozeal.com\r\n";
					$headers .= "Content-type: text/html\r\n";
					$mail = mail($email, $subject_user, $message_user, $headers);

					$subject_admin = "Test Users Online Request";
					$message_admin = "The user ".$this->Session->read('user.User.username')." has requested the product ".$pname;

					$headers1  = "From: ".$email."\r\n";
					$headers1 .= "Content-type: text/html\r\n";
					$mail = mail('kiran.ganta@infozeal.com', $subject_admin, $message_admin, $headers1);
				}
				$urlKey = $surl.'/?key='.$keyword;
				$this->redirect($surl);
			}
			$this->loadModel('Country');
			$countries = $this->Country->find('list', array('fields' => array('Country.name')));

			$this->set('countries',$countries);
			$this->set('pid',$pid);
			$this->set('sendUrl',$url);
		}

		function update_online_request_state()
		{
			$selected_country= $this->request->data['ProductsCart']['country'];
			$defaultstatesql ="SELECT State.state_cd, State.state_name FROM states AS State, countries as Country where State.country_cd = Country.id
			and Country.name='".$selected_country."'";

			$this->loadModel("State");
			$cstates = $this->State->query($defaultstatesql);

			$ctrystate = array();
			foreach ($cstates as $ctrystates) {
				$ctrystate[$ctrystates['State']['state_cd']] = $ctrystates['State']['state_name'];
			}

			$this->set('cstates',$ctrystate);
			$this->layout = 'ajax';
    }

	function quick_view($id=null,$string)
	{
		$this->set('keyword',$string); 	
		$this->check_query_numeric($id);
		if($id != null)
		{
			$this->Product->unbindModel(array('hasAndBelongsToMany' => array('Client', 'Program')));

			$product = $this->Product->find('first', array('conditions'=>array('Product.id'=>$id)));

			$address_count = $this->Product->Merchant->MerchantAddress->find('count', array('conditions'=>
					array('MerchantAddress.merchant_id' => $product['Product']['merchant_id'])));

			if ($address_count == '0') {
				$product['address'] = 'none';
			} else if ($address_count > 1) {
				$product['address'] = 'multiple';
			} else {
				$product['address'] = '';
				$merchant_address = $this->Product->Merchant->MerchantAddress->find('all', array('conditions'=>array('MerchantAddress.merchant_id' => $product['Product']['merchant_id'])));
				$product['MerchantAddress'] = $merchant_address['MerchantAddress'];
			}

			$sql="select Product.id from cart_products as CartProduct, products as Product where Product.id=CartProduct.product_id
			AND (CartProduct.product_quantity >0 OR CartProduct.product_quantity =-1) AND CartProduct.product_type='Pay' and CartProduct.active=1 and Product.id=".$id ;
			$cartproduct = $this->Product->query($sql);

			$pointssql="select points from products_points where product_id=".$id;
			$productpoints=$this->ProductsPoint->query($pointssql);

			$this->set('product',$product);
			$this->set('cartproduct',$cartproduct);
			$this->set('productpoints',$productpoints);
		}
	}

	/**
	 * Search for offers
	 *
	 * @param	int	$type	the type of search: keyword , location or category
	 */

	function shop_now($type=null) {

		if (!empty($_GET['data']['Product'])) {
			foreach ($_GET['data']['Product'] as $key => $value) {
				$this->request->params['named'][$key] = $value;
			}
		}

		//	default search method
		if (!isset($this->params['named']['sort'])) {
			$search_sort = 'default';
		}
		else
		{
			$search_sort = addslashes($this->params['named']['sort']);
		}
		//	set the form values again
		foreach ($this->params['named'] as $key => $value) {
			$this->request->data['Product'][$key] = $value;
		}

		$type = 'keyword';

		if (!empty($type)) {
			//  exact or like  -- condition
			if (isset($this->params['named']['search_mode_id']) && $this->params['named']['search_mode_id'] != '')
			{
				$search_mode = addslashes(addslashes($this->params['named']['search_mode_id']));
			} else {$search_mode = 'E';
			}
			// search particular state or default all states
			if (isset($this->params['named']['search_state_id']) && $this->params['named']['search_state_id'] != '')
			{
				$search_state = addslashes(addslashes($this->params['named']['search_state_id']));
			} else { $search_state = '';
			}

			if (isset($this->params['named']['location'])) {
				$search_location = addslashes(addslashes($this->params['named']['location']));
			} else {
				$search_location = '';
			}

			if (isset($this->params['named']['keywords'])) {
				$search_keyword = addslashes(addslashes($this->params['named']['keywords']));
			} else {
				$search_keyword = '';
			}

			$this->Product->unbindModel(array('belongsTo' => array('Merchant'), 'hasAndBelongsToMany' => array('Client')));

			$dashboardid=$this->Session->read('dashboardidinsession');

			if($dashboardid == 1)
			{
				$sql =" INNER JOIN cart_products as CartProduct ON Product.id = CartProduct.product_id
				INNER JOIN categories_products AS CategoriesProduct ON Product.id = CategoriesProduct.product_id
				INNER JOIN clients_categories AS ClientsCategory ON CategoriesProduct.category_id = ClientsCategory.category_id AND ClientsCategory.client_id = '". $this->Session->read('client.Client.id'). "'
				WHERE Product.merchant_id = MerchantAddress.merchant_id and ProductsAddress.merchant_address_id=MerchantAddress.id
				and Category.id = CategoriesProduct.category_id and ProductsAddress.product_id=Product.id AND Product.active = 1 and Product.cancelled = 0 and Product.merchant_id = Merchant.id
				AND (Product.offer_start = '0000-00-00' OR Product.offer_start <= NOW()) AND (Product.expires = '0000-00-00' OR Product.expires >= NOW()) and Product.id = ClientsProduct.product_id
				AND CartProduct.product_type = 'Pay'
				AND CartProduct.active = 1
				AND (ClientsProduct.client_id = '". $this->Session->read('client.Client.id'). "' OR ClientsProduct.client_id = 0)";
				//AND ClientsProduct.product_id not in (select product_id from products_points) AND ClientsProduct.product_id not in (select product_id from products_safs)";
			}
			if (($this->Session->read('client.Client.blacklisted_product') != ''))
			{
				$sql .= " AND Product.id not in (". $this->Session->read('client.Client.blacklisted_product'). ") ";
			}

			if (isset($this->params['named']['category_id']) && $this->params['named']['category_id'] != '' && is_numeric($this->params['named']['category_id'])) {
				$sql .= " AND CategoriesProduct.category_id = '". $this->params['named']['category_id']. "' ";
				$this->Product->Category->recurisve = 0;
				$this->set('category', $this->Product->Category->findById($this->params['named']['category_id']));
			}
			//Modified Code for Keyword
			if($search_keyword != '')
			{
				if($search_mode == 'E')
				{
					// Search for exact condition  - anywhere in the following fields
					// Match-Against statement doesn't work accordingly when words are enclosed in double quotes
					$sql .= " AND
					( Product.keyword LIKE '%". $search_keyword. "%'
					OR Product.name LIKE '%". $search_keyword. "%'
					OR MerchantAddress.name LIKE '%". $search_keyword. "%'
					OR Product.details LIKE '%". $search_keyword. "%'
					OR Product.highlight LIKE '%". $search_keyword. "%'
					OR Product.`text` LIKE '%". $search_keyword. "%'
					OR Product.special_offer_headline LIKE '%". $search_keyword. "%'
					OR Product.special_offer_body LIKE '%". $search_keyword. "%' ) ";
					//break;
				}
				else
				{
					//Search for Like condition

					$kt=split(' ' ,$search_keyword);
					while(list($key,$val)=each($kt))
					{
						if($val<>' ' and strlen($val) > 0)
						{
							$sql .= " AND
							( Product.keyword LIKE '%". $val. "%'
							OR Product.name LIKE '%". $val. "%'
							OR MerchantAddress.name LIKE '%". $val. "%'
							OR Product.details LIKE '%". $val. "%'
							OR Product.highlight LIKE '%". $val. "%'
							OR Product.`text` LIKE '%". $val. "%'
							OR Product.special_offer_headline LIKE '%". $val. "%'
							OR Product.special_offer_body LIKE '%". $val. "%' ) " ;
						}
					}
				}
			}
			//Modified Code for Location

			$found = false;
			$postcode = false;

			//	state search
			$states = array('act' => 'ACT', 'nsw' => 'NSW', 'vic' => 'VIC', 'tas' => 'TAS', 'sa' => 'SA', 'wa' => 'WA', 'nt' => 'NT', 'qld' => 'QLD',
					'australian capitol territory' => 'ACT', 'new south wales' => 'NSW', 'victoria' => 'VIC', 'tasmania' => 'TAS', 'south australia' => 'SA',
					'western australia' => 'WA', 'northern territory' => 'NT', 'queensland' => 'QLD',
					'a.c.t.' => 'ACT', 'n.s.w.' => 'NSW', 'vic.' => 'VIC', 'tas.' => 'TAS', 's.a.' => 'SA', 'w.a.' => 'WA', 'n.t.' => 'NT', 'qld.' => 'QLD'
			);

			if ($search_location == '') {
				$found = true;
			} else if (isset($states[strtolower($search_location)])) {
				$postcode = false;
				$found = true;
				//	postcode search
			}
			elseif ($search_location!='')
			{
				if(strpos($search_location,'-'))
				{
					$suburb_txt = trim(substr($search_location, strpos($search_location,'-')+1));
					$search_location = $suburb_txt;
				}
				if(is_numeric(trim($search_location)))
					$postcode = $search_location;
				else
				{
					$postcode_sql = "select Postcode.postcode from postcodes as Postcode where Postcode.suburb='".$search_location."'";
					$code = $this->Postcode->query($postcode_sql);

					$postcodes = array();

					foreach($code as $cd)
					{
						array_push($postcodes, $cd['Postcode']['postcode']);
					}
					$postcode = $postcodes;
				}

				$include_postcode = $postcode;
				$found = true;

				//	suburb search
			} else {

				$suburbs = $this->Suburb->find('all', array('conditions'=>array("suburb LIKE '". $search_location. "'")));

				if ($suburbs) {
					foreach ($suburbs as $suburb) {
						$postcode[] = $suburb['Suburb']['postcode'];
					}
					$include_postcode = $postcode[0];
					$found = true;
				}

			}
			// Implement the surrounding suburbs logic
			$radius = $this->params['named']['distance'];
			if(isset($radius) && $radius!='' && is_numeric($radius))
			{
				if(is_array($include_postcode))
					$ref=mysql_query("SELECT `lat`,`lon` FROM `postcodes` WHERE `postcode` in (".implode(',',$include_postcode).")");
				else
					$ref=mysql_query("SELECT `lat`,`lon` FROM `postcodes` WHERE `postcode`  =  {$include_postcode}");

				if(mysql_num_rows($ref)){
					$res=mysql_fetch_assoc($ref);
					$lat=$res["lat"];
					$lon=$res["lon"];
					$ref2= mysql_query("SELECT `postcode` FROM `postcodes` WHERE (POW((69.1*(`lon`-\"$lon\")*cos($lat/57.3)),\"2\")+POW((69.1*(`lat`-\"$lat\")),\"2\"))<($radius*$radius)");
					$postcode=array();
					if(mysql_num_rows($ref2)){
						while($res2=mysql_fetch_assoc($ref2)){
							array_push($postcode,$res2["postcode"]);
							//$postcode = $res2["postcode"];
						}
					}
				}
			}
			//}

			$sql .= " AND (Product.available_international = 1 OR ((1=1 ";
			//$sql .= " AND (Product.available_international = 1 OR ((Product.available_national = 1 ";

			if ($search_location == '' && $search_state == ''){}
			else if ($search_location == '' && $search_state != ''){
				//check vic state where prod is available
				//check the merchant address as well for the state

				//$sql .= " AND ((Product.available_" . strtolower($search_state) . " = 1 AND MerchantAddress.primary = 1) " ;

				//$sql .= "  OR MerchantAddress.state = '". $search_state ."' ";
				$sql .= " AND (Product.available_national = 1 OR MerchantAddress.state = '". $search_state ."') ";

			}
			else if ($search_location != '') {
				//check postcode / postcodes
				if (is_array($postcode)) {
					$i = 1;
					foreach ($postcode as $value){
						if($i == 1)
						{
							$sql .= " AND (MerchantAddress.postcode = '". $value. "' ";
						}
						else
						{
							$sql .= " OR MerchantAddress.postcode = '". $value. "' ";
						}
						$i = $i + 1;
					}
					$sql .= ") ";
				}
				else	{
					if ($postcode){
						$sql .= " AND MerchantAddress.postcode = '". $postcode. "' ";
					}
				}
				if ($search_state != ''){
					//$sql .= "  AND MerchantAddress.state = '". $search_state ."' ";
					$sql .= "  AND ( Product.available_national = 1 OR MerchantAddress.state = '". $search_state ."') ";
				}

			}

			$sql .= ") ";
			if (!$found) {
				$sql .= " AND 1!=1 ";
			}


			if (isset($this->params['named']['country_id']) ) {
				$sql .= " AND (MerchantAddress.country= '". addslashes($this->params['named']['country_id']). "')))";
			}
			else
			{
				$sql .= " AND (MerchantAddress.country='".$this->Session->read('client.Client.country')."')))";
			}

			$this->paginate = array('ProductList2' => array('limit' => SEARCH_RESULTS_PER_PAGE),array ('search' =>array('dashboardid'=>$dashboardid )));
			$products = $this->paginate('ProductList2', array('search' =>array('sql' => $sql, 'keyword' => $search_keyword,'dashboardid'=>$dashboardid ), 'sort' => $search_sort));

			if ($search_sort != 'suburb') {
				foreach ($products as $key => $product) {
					$address_count = $this->Product->Merchant->MerchantAddress->find('count', array('conditions'=>array('MerchantAddress.merchant_id' => $product['Product']['merchant_id'])));

					if ($address_count == '0') {
						$products[$key]['MerchantAddress'] = 'none';
					} else if ($address_count > 1) {
						$products[$key]['MerchantAddress'] = 'multiple';
					} else {
						$merchant_address = $this->Product->Merchant->MerchantAddress->find('first', array('conditions'=>array('MerchantAddress.merchant_id' => $product['Product']['merchant_id'])));
						$products[$key]['MerchantAddress'] = $merchant_address['MerchantAddress'];
					}
				}
			}


			//get country selected from drop down
			$ctry= addslashes($this->params['named']['country_id']);

			//first time param value will be empty so get country from session
			if(empty($ctry))
				$ctry = $this->Session->read('client.Client.country');

			// Query to get categories array based on selected country from drop down

			$catsql ="SELECT DISTINCT Category.* FROM categories AS Category
			LEFT JOIN clients_categories AS ClientsCategory ON Category.id = ClientsCategory.category_id
			LEFT JOIN categories_products AS CategoriesProduct ON ClientsCategory.category_id = CategoriesProduct.category_id
			WHERE (ClientsCategory.client_id = '". $this->Session->read('client.Client.id') ."') and Category.countries = '".$ctry."'
			order by Category.name";

			$this->loadModel("Category");
			$ctgry = $this->Category->query($catsql);

			$countrycat = array();
			foreach ($ctgry as $category1) {
				$countrycat[$category1['Category']['id']] = $category1['Category']['name'];
			}

			// Query to get states array based on selected country from drop down

			$statesql ="SELECT State.state_cd, State.state_name FROM states AS State, countries as Country, clients as Client,
			merchant_addresses as MerchantAddress, products as Product where State.country_cd = Country.id and Country.name='".$ctry."'
			and MerchantAddress.merchant_id = Product.merchant_id and MerchantAddress.state = State.state_cd
			and Client.id = ".$this->Session->read('client.Client.id')." order by State.state_name";

			$this->loadModel("State");
			$state = $this->State->query($statesql);

			$statectry = array();
			foreach ($state as $state1) {
				$statectry[$state1['State']['state_cd']] = $state1['State']['state_name'];
			}

			if(!empty($this->params['named']['country_id']))
			{
				$this->Session->write('statelist', $statectry);
				$this->Session->write('catlist', $countrycat);
			}
			$this->Session->write('params', $this->params['named']);


			$this->loadModel("Skyscraper");
			$banner_sql = "select Skyscraper.* from skyscrapers as Skyscraper, clients_skyscrapers as ClientsSkyscraper,
			programs_skyscrapers as ProgramsScraper where ClientsSkyscraper.client_id=".$this->Session->read('client.Client.id')." and
			ProgramsScraper.program_id=".$this->Session->read('client.Client.program_id')." and Skyscraper.id=ClientsSkyscraper.skyscraper_id
			and Skyscraper.id=ProgramsScraper.skyscraper_id order by rand() limit 1";

			$banner_list = $this->Skyscraper->query($banner_sql);

			$this->set('bannerlist',$banner_list);

			$this->set('statelist',$statectry);
			$this->set('categorylist',$countrycat);
			$this->set('products', $products);

			$this->set('whats_new_products', $this->_whatsNewProducts($this->Session->read('client.Client.id'), $this->Session->read('client.Client.whats_new_module_limit')));

			$this->set('sorts', array('default' => 'Default', 'name' => 'Name', 'suburb' => 'Suburb', 'latest' => 'Latest Offers', 'state' => 'By State'));

			$this->set('named_params', $this->params['named']);

		}

		$userid = $this->Session->read('user.User.id');
		$this->loadModel("User");

		$user_used_count = $this->User->find('first',array('conditions' => array('User.id' => $userid), 'recursive' => -1, 'fields' => array('User.used_count')));

		$this->set('usedcount', $user_used_count['User']['used_count']);
		$this->set('type', $type);

		$this->loadModel('Country');
		$countries = $this->Country->find('list', array('fields' => array('Country.name')));

		$this->set('countries',$countries);

	}

	public function get_postcodes()
	{
		$suburb_code = $_GET['term'];
		if(!is_numeric($suburb_code))
			$postcodes = $this->Postcode->find('all',array('conditions'=>array("suburb LIKE '%". $suburb_code. "%'"),'fields'=>array('suburb','postcode')));
		$this->set('code',$postcodes);
	}

	function check_query_numeric($id, $id1=null, $id2=null)
	{
		if((!empty($id) && !is_numeric($id)) || (!empty($id) && is_numeric($id) && $id <0)) {
			$this->redirect('/');
		}elseif((!empty($id1) && !is_numeric($id1)) || (!empty($id1) && is_numeric($id1) && $id1 <0)) {
			$this->redirect('/');
		}
		elseif(!empty($id2) && !is_numeric($id2)) {
			$this->redirect('/');
		}
	}

	function check_query_numeric_admin($id, $id1=null)
	{
		if((!empty($id) && !is_numeric($id)) || (!empty($id) && is_numeric($id) && $id <0)) {
			$this->redirect('/admin');
		}elseif(!empty($id1) && !is_numeric($id1)) {
			$this->redirect('/admin');
		}
	}

	function admin_load_clients($list, $value)
		{
			$this->layout = 'blank';
			$this->loadModel('Program');
			if(!empty($list))
			{
				$clients = $this->Client->find('list',array('conditions'=>array('Client.program_id'=>$list),'order'=>'Client.name'));
				$this->set('clientsList',$clients);

				$programname = $this->Program->find('first',array('conditions'=>array('Program.id'=>$list), 'recursive'=>-1, 'fields'=>array('Program.name')));
				$this->set('program_name',$programname['Program']['name']);
				$this->set('pid',$list);
				$this->set('value',$value);

			}
	}










}
?>