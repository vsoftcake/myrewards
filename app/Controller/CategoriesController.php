<?php
class CategoriesController extends AppController {

	var $uses = array('Category', 'CategoryProductList','CategoriesProduct');
	var $components = array('File');

	var $paginate = array('CategoryProductList' => array('limit' => SEARCH_RESULTS_PER_PAGE));

	function view($id = null) {

		//	id required, and must be available to this client
		if ($id == null  || !in_array($id, array_keys($this->Session->read('client.Categories')))) {
			$this->redirect('/');
			exit();
		}

		$this->Category->unbindModel(array('hasAndBelongsToMany' => array('Client', 'Program', 'Product')), false);
		$category = $this->Category->findById($id);
		$this->set('category', $category);

		$conditions = array(
			'Client' => array(
				'id' => $this->Session->read('client.Client.id')
			),
			'Category' => array(
				'id' => $id
			)
		);

		$conditions['Product']['special_offer'] = 1;
		$this->paginate = array('CategoryProductList' => array('limit' => SPECIAL_OFFERS_PER_PAGE));
		$special_offers = $this->paginate('CategoryProductList',$conditions);

		$conditions['Product']['special_offer'] = 0;
		$this->paginate = array('CategoryProductList' => array('limit' => SEARCH_RESULTS_PER_PAGE));
		$products = $this->paginate('CategoryProductList',$conditions);

		if (empty($special_offers)) {
			for ($i =0; $i < 5; $i++) {
				if (isset($products[$i])) {
					$special_offers[] = $products[$i];
					unset($products[$i]);
				}
			}
		}

		$this->set('products', $products);
		$this->set('special_offers', $special_offers);

		$sub_categories = $this->Category->findAllByParentId($category['Category']['id']);
		$this->set('sub_categories', $sub_categories);

	}

	function admin_index($id = null) {
		$this->Category->recursive = 0;
		$categories = $this->Category->find('threaded', array('conditions'=>array('Category.deleted !='=>1), 'order'=>'Category.description, Category.name'));
		$this->set('categories', $categories);

		if ($id) {
			$this->Category->recursive = -1;
			$category = $this->Category->read(null, $id);
			$this->set('category', $category);
		}

	}

	function admin_view($id = null) {
		if (!$id && empty($this->request->data)) {
			$this->Session->setFlash('Invalid Category');
			$this->redirect(array('action'=>'index'), null, true);
		}

		$this->layout = 'blank';

		$this->set('data', $this->Category->read(null, $id));

		$conditions = array();
		$this->set('products', $this->paginate($this->Category->Product,$conditions));
	}

	function admin_add() {
		if (!empty($this->request->data)) {
			if (isset($this->request->data['CategoryLogo']))
			{
				//	get the image extensions
				if ($this->request->data['CategoryLogo']['file']['name'] != '') {
					$extension = substr($this->request->data['CategoryLogo']['file']['name'], strrpos($this->request->data['CategoryLogo']['file']['name'], '.') + 1);
					$this->request->data['Category']['logo_extension'] = $extension;
				}
			}
			if(empty($this->request->data['Category']['parent_id'])){
				unset($this->request->data['Category']['parent_id']);
			}
			if(empty($this->request->data['Category']['sort'])){
				unset($this->request->data['Category']['sort']);
			}

			if ($this->Category->save($this->request->data)) {

				if (isset($this->request->data['CategoryLogo']))
				{
					//	save the uploaded files
					if ($this->request->data['CategoryLogo']['file']['name'] != '') {
						move_uploaded_file($this->request->data['CategoryLogo']['file']['tmp_name'], CATEGORY_LOGO_PATH. $this->Category->id. '.'. $this->request->data['Category']['logo_extension']);
					}
				}

				$this->Session->setFlash('The Category has been created');
				$this->redirect(array('action'=>'index'), null, true);
			} else {
				$this->Session->setFlash('Please correct the errors below.');
			}
		}
		$this->loadModel("Country");
		$country = $this->Country->find('list', array('fields' => 'Country.name,Country.name'));

		$this->Category->recursive = 0;
		$categories = $this->Category->find('list', array('order'=>'description, name', 'fields'=>'id,description'));
		$this->set('categories', $categories);
		$this->set('country_list', $country);
		$this->set('search_weights', $this->Category->getEnumValues('search_weight'));
	}

	function admin_edit($id = null) {
		if (!$id && empty($this->request->data)) {
			$this->Session->setFlash('Invalid Category');
			$this->redirect(array('action'=>'index'), null, true);
		}

		if (!empty($this->request->data)) {
			if (isset($this->request->data['CategoryLogo']))
			{
				//	get the image extensions
				if ($this->request->data['CategoryLogo']['file']['name'] != '') {
					$extension = substr($this->request->data['CategoryLogo']['file']['name'], strrpos($this->request->data['CategoryLogo']['file']['name'], '.') + 1);
					$this->request->data['Category']['logo_extension'] = $extension;
				}
			}

			$this->Category->unbindModel(array('hasAndBelongsToMany' => array('Product')));
			$old_data = $this->Category->findById($id);

			if ($this->Category->save($this->request->data['Category'])) {
				$categoryId = $this->Category->id;
				//Saving clients
				$this->Category->ClientsCategory->deleteAll(array('category_id'=>$categoryId));
				$categoryClientData = array();
				foreach($this->request->data['Client']['Client'] as $categoryClient){if(!empty($categoryClient)){
					$categoryClientData[] = array('category_id'=>$categoryId, 'client_id'=>$categoryClient);
				}}
				if(!empty($categoryClientData)){$this->Category->ClientsCategory->saveMany($categoryClientData);}
				
				//Saving programs
				$this->Category->ProgramsCategory->deleteAll(array('category_id'=>$categoryId));
				$categoryProgramData = array();
				foreach($this->request->data['Program']['Program'] as $categoryProgram){if(!empty($categoryProgram)){
					$categoryProgramData[] = array('category_id'=>$categoryId, 'program_id'=>$categoryProgram);
				}}
				if(!empty($categoryProgramData)){$this->Category->ProgramsCategory->saveMany($categoryProgramData);}

			// delete unwanted images of merchant logo
				if (isset($this->request->data['CategoryLogoImageDelete'])) {
					$delete_images = array();
					foreach ($this->request->data['CategoryLogoImageDelete'] as $key => $value) {
						if ($value == '1') {
							$this->File->deleteByPrefix('category_logo/'.$this->Category->id);
							$this->request->data['Category']['logo_extension']='';
							$this->Category->save($this->request->data["Category"]);
						}
					}
				}

				if (isset($this->request->data['CategoryLogo']))
				{
						//	save the uploaded files
						if ($this->request->data['CategoryLogo']['file']['name'] != '') {
							move_uploaded_file($this->request->data['CategoryLogo']['file']['tmp_name'], CATEGORY_LOGO_PATH. $this->Category->id. '.'. $this->request->data['Category']['logo_extension']);
						}
				}

				//	log the change
				$log = array(
					'id' => null,
					'foreign_id' => $this->{Inflector::singularize($this->name)}->id,
					'foreign_model' => Inflector::singularize($this->name),
					'user_id' => $this->Session->read('user.User.id'),
					'old_data' => $old_data,
					'new_data' => $this->request->data,
					'notes' => ''
				);
				$log['description'] = Inflector::humanize(Inflector::underscore($this->name)). ' modified';

				//	add the related changes
				foreach ($this->{Inflector::singularize($this->name)}->getAssociated('hasAndBelongsToMany') as $model)  {
					switch ($model) {		//	fields to add to the comments display
						default:
							$display = 'name';
					}

					$tmp_old_related = array();
					//	deleted
					if (isset($old_data[$model]) && isset($this->request->data[$model][$model])) {
						foreach ($old_data[$model] as $model_data) {
							if (!in_array($model_data['id'], $this->request->data[$model][$model])) {
								$log['old_data'][$this->name. $model][Inflector::humanize(Inflector::underscore($model)). ': '. $model_data[$display]] = '1';
								$log['new_data'][$this->name. $model][Inflector::humanize(Inflector::underscore($model)). ': '. $model_data[$display]] = '0';
							}
							$tmp_old_related[] = $model_data['id'];
						}
						//	added
						foreach ($this->request->data[$model][$model] as $model_id) {
							if ($model_id != '' && !in_array($model_id, $tmp_old_related)) {
								$this->{Inflector::singularize($this->name)}->{$model}->recursive = -1;
								$model_data = $this->{Inflector::singularize($this->name)}->{$model}->findById($model_id);
								$log['old_data'][$this->name. $model][Inflector::humanize(Inflector::underscore($model)). ': '. $model_data[$model][$display]] = '0';
								$log['new_data'][$this->name. $model][Inflector::humanize(Inflector::underscore($model)). ': '. $model_data[$model][$display]] = '1';
							}
						}
					}
				}

				$this->save_log($log);

				$this->Session->setFlash('The Category has been saved');
				$this->redirect_session(array('action'=>'index'), null, true);
			} else {
				$this->Session->setFlash('Please correct the errors below.');
			}
		} else {
			$this->request->data = $this->Category->read(null, $id);
			$this->_setRedirect();
		}
		$clients = $this->Category->Client->find('list');
		$programs = $this->Category->Program->find('list');
		$this->loadModel("Country");
		$country = $this->Country->find('list', array('fields' => 'Country.name,Country.name'));
		$this->set(compact('clients','programs','categories','country'));
		$this->set('search_weights', $this->Category->getEnumValues('search_weight'));
	}

	function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash('Invalid id for Category');
			$this->redirect(array('action'=>'index'), null, true);
		}

		//	test for related items
		//	products
		$products = $this->Category->CategoriesProduct->find('count', array('conditions'=>array('category_id' => $id)));
		if ($products > 0) {
			$this->Session->setFlash('Please remove all products from the category before deleting.');
			$this->redirect('/admin/categories/edit/'. $id);
		}

		//	programs
		$programs = $this->Category->ProgramsCategory->find('count', array('conditions'=>array('category_id' => $id)));
		if ($programs > 0) {
			$this->Session->setFlash('Please remove all programs from the category before deleting.');
			$this->redirect('/admin/categories/edit/'. $id);
		}

		//	clients
		$clients = $this->Category->ClientsCategory->find('count', array('conditions'=>array('category_id' => $id)));
		if ($clients > 0) {
			$this->Session->setFlash('Please remove all clients from the category before deleting.');
			$this->redirect('/admin/categories/edit/'. $id);
		}

		if ($this->Category->delete($id)) {
			$this->Session->setFlash('Category #'.$id.' deleted');
			$this->redirect(array('action'=>'index'), null, true);
		}
	}

	/**
	 * Return a html list of products for an ajax call
	 */
	function admin_ajax_products($id) {

		$this->layout = 'ajax';

		$data = $this->paginate('CategoriesProduct',array('Category.id' => $id));

		$this->set('products', $data);
		$this->set('id', $id);
	}

	/**
	 * Return a html list of programs for an ajax call
	 */
	function admin_ajax_programs($id) {

		$this->layout = 'ajax';

		$sql = "SELECT Program.*, ProgramsCategory.category_id
					FROM programs AS Program
					LEFT JOIN programs_categories AS ProgramsCategory ON Program.id = ProgramsCategory.program_id AND ProgramsCategory.category_id = '". $id. "'
					ORDER BY Program.name";
		$data = $this->Category->query($sql);

		$this->set('programs', $data);
		$this->set('id', $id);
	}

	/**
	 * Return a html list of programs for an ajax call
	 */
	function admin_ajax_program_clients($id, $program_id) {

		$this->layout = 'ajax';

		$sql = "SELECT Client.*, ClientsCategory.category_id
					FROM clients AS Client
					LEFT JOIN clients_categories AS ClientsCategory ON Client.id = ClientsCategory.client_id AND ClientsCategory.category_id = '". $id. "'
					WHERE Client.program_id = '". $program_id. "'
					ORDER BY Client.name";
		$data = $this->Category->query($sql);

		$this->set('clients', $data);
		$this->set('id', $id);
		$this->Category->Program->recursive = -1;
		$this->set('program', $this->Category->Program->findById($program_id));
	}

	/**
	 * Update the categories clients and programs, also assigns products in the category
	*/
	function admin_ajax_programs_clients_update($id) {
		set_time_limit('180');
		$this->layout = 'ajax';

		$products = $this->Category->CategoriesProduct->find('list',
			array(
				'conditions' => array('CategoriesProduct.category_id' => $id, 'Product.assignment_override' => '0'),
				'recursive' => 1,
				'fields' => array('Product.id', 'Product.id')
				)
		);

		if (!empty($this->request->data)) {
			if (isset($this->request->data['ClientsCategory'])) {
				//	remove all clients
				$this->Category->ClientsCategory->deleteAll("category_id = '". $id. "' AND client_id IN ('". implode("','", array_keys($this->request->data['ClientsCategory']['id'])). "')");
				foreach ($this->request->data['ClientsCategory']['id'] as $client_id => $enabled) {
					// remove all clients products
					$this->Category->Client->ClientsProduct->deleteAll("ClientsProduct.client_id = '". $client_id. "' AND ClientsProduct.product_id IN ('". implode("','",$products). "')");
					if ($enabled == '1') {
						$this->Category->ClientsCategory->save(array('ClientsCategory' => array('id' => null, 'category_id' => $id, 'client_id' =>  $client_id)));
						$insert_array = array();
						foreach ($products as $product_id) {
							// this is too slow. using sql extended insert instead  $this->Category->Client->ClientsProduct->save(array('ClientsProduct' => array('id' => '', 'client_id' => $client_id, 'product_id' => $product_id)));
							$insert_array[] = "('". $client_id. "', '". $product_id. "')";
						}
						$sql = "INSERT INTO clients_products (client_id, product_id) VALUES ". implode(',', $insert_array);
						$this->Category->Client->ClientsProduct->query($sql);
					}
				}
			}
			if (isset($this->request->data['ProgramsCategory'])) {
				$this->Category->ProgramsCategory->deleteAll("category_id = '". $id. "' AND program_id IN ('". implode("','", array_keys($this->request->data['ProgramsCategory']['id'])). "')");
				foreach ($this->request->data['ProgramsCategory']['id'] as $program_id => $enabled) {
					// remove all programs products
					$this->Category->Program->ProgramsProduct->deleteAll("ProgramsProduct.program_id = '". $program_id. "' AND ProgramsProduct.product_id IN ('". implode("','",$products). "')");
					if ($enabled == '1') {
						$this->Category->ProgramsCategory->save(array('ProgramsCategory' => array('id' => null, 'category_id' => $id, 'program_id' =>  $program_id)));
						$insert_array = array();
						foreach ($products as $product_id) {
							$insert_array[] = "('". $program_id. "', '". $product_id. "')";
						}
						$sql = "INSERT INTO programs_products (program_id, product_id) VALUES ". implode(',', $insert_array);
						$this->Category->Program->ProgramsProduct->query($sql);
						/*
						foreach ($products as $product_id) {
							$this->Category->Program->ProgramsProduct->save(array('ProgramsProduct' => array('id' => '', 'program_id' => $program_id, 'product_id' => $product_id)));
						}*/
					}
				}
			}
		}
	}
}
?>
