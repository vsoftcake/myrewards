<?php
class CommentsController extends AppController
{
	var $components = array('File','RequestHandler');

    function add_comments($prodid)
    {
		if(!empty($this->request->data))
		{
			$this->request->data['Comment']['product_id'] = $prodid;
			$this->request->data['Comment']['user_id'] = $this->Session->read('user.User.id');
			if ($this->Comment->save($this->request->data))
			{
				$this->Session->setFlash('Thank you for submitting your comment. We will review your post shortly.');
				$this->request->data['Comment']['name'] = "";
				$this->request->data['Comment']['comments'] = "";
			}
			else
			{
				$this->Session->setFlash('<div>In the title please summarise your comment.</div><br/><div style="padding-left:24px">Please give a description in the comments area.</div><br/>');
			}
		}
		$this->set('prodid',$prodid);
	}

	function admin_index()
	{
		$conditions = array();
		/*if (isset($this->params['url']['data'])) {
			$this->Session->write($this->name.'.search', $this->params['url']['data']);
		}

		if ($this->Session->check($this->name.'.search')) {
			$this->request->data = $this->Session->read($this->name.'.search');
			if  (!empty($this->request->data['Comment']['search'])) {
				$conditions[] = "
					(`Comment`.`name` LIKE  '%". $this->request->data['Comment']['search']. "%'
					OR `Comment`.`id` LIKE  '%". $this->request->data['Comment']['search']. "%')
				";
			}
		}

		$this->Comment->recursive = 0;
		$this->set('comments', $this->paginate('Comment',$conditions));*/

		$sql1 = " WHERE Comment.product_id=Product.id and Merchant.id=Product.merchant_id and User.id=Comment.user_id group by Comment.id";

		$this->paginate = array('Comment' => array('limit' => 20));
		$comments = $this->paginate('Comment', array('search' =>array('sql' => $sql1)));
		$this->set('comments',$comments);
		
		$this->loadModel("Country");
		$countries = $this->Country->find('all',array('fields'=>'name,name'));
		
		$ctry = array();
		foreach($countries as $country)
		{
			$ctry[$country['Country']['name']] = $country['Country']['name'];
		}
		$this->set('countries',$ctry);
		
		$this->loadModel("Program");
		$plist = $this->Program->find('list');
		$this->set('programs_list',$plist);

	}

	function admin_approve($cid)
	{
		$comment_status = $this->Comment->find('first',array('conditions' => array('Comment.id' => $cid), 'fields' => array('Comment.status','Comment.name')));
		if($comment_status['Comment']['status']== 0 )
		{
			$status = array('Comment' => array('id' => $cid, 'status' => 1));
			$this->Comment->save($status);
			$this->Session->setFlash('The comment #'. $comment_status['Comment']['name']. ' is approved');
		}
		$this->autoRender=false;

		$this->redirect('/admin/comments/index');
	}
	function admin_delete($id = null)
	{
		if (!$id)
		{
			$this->Session->setFlash('Invalid id for Comment');
			$this->redirect(array('action'=>'index'), null, true);
		}
		if ($this->Comment->delete($id))
		{
			$this->Session->setFlash('The comment #'.$id.' deleted');
			$this->redirect(array('action'=>'index'), null, true);
		}
	}

	function admin_view($id=null)
	{
		if ($id)
		{
			$views = $this->Comment->find('all', array('conditions' => array('Comment.id' => $id)));
		}
		$this->set('views', $views);
		$this->layout = 'ajax';
	}

	function admin_update_comments($pid,$sort,$country)
	{
		$this->loadModel("ProgramsProduct");
		$this->loadModel("CommentList");
		
		$selectSql = " SELECT Comment.*, Merchant.name, Product.name, User.username, User.first_name, User.last_name 
        FROM comments as Comment, merchants as Merchant, programs_products as ProgramsProduct, products as Product, users as User";
        
		$countSql = "SELECT count(Distinct Comment.id) as count FROM comments as Comment, merchants as Merchant, users as User, 
		programs_products as ProgramsProduct, products as Product"; 
		
		$sql1 = " WHERE Comment.product_id=ProgramsProduct.product_id and ProgramsProduct.program_id=".$pid." and Product.id = Comment.product_id
		and Product.merchant_id=Merchant.id and User.id=Comment.user_id ";
		
		if($country<>'c=')
		{
			$country = substr($country,2);
			$selectSql.= ", merchant_addresses as MerchantAddress  ";
			$countSql.= ", merchant_addresses as MerchantAddress  ";
			$sql1.=" and Product.merchant_id=MerchantAddress.merchant_id and MerchantAddress.country='".$country."' ";
		}
		if($sort<>'s=')
		{	
			$sort = substr($sort,2);
			$sql1.=" and Comment.status=".$sort." ";
		}
		else
			$sql1.=" and (Comment.status='0' or Comment.status=1) ";

		$sql1.="  group by Comment.id ";
		
		$this->paginate = array('CommentList' => array('limit' => 20));
		$comments = $this->paginate('CommentList', array('search' =>array('select'=>$selectSql, 'count'=>$countSql, 'sql' => $sql1)));
		
		if($sort == 's=')
			$sort = '';
		$this->set('products',$comments);
		$this->set('sortby',$sort);
		$this->set('country',$country);
		
		$this->loadModel("Program");
		$plist = $this->Program->find('list');
		$this->set('programs_list',$plist);
		
		$this->loadModel("Country");
		$countries = $this->Country->find('all',array('fields'=>'name,name'));
		
		$ctry = array();
		foreach($countries as $country)
		{
			$ctry[$country['Country']['name']] = $country['Country']['name'];
		}
		$this->set('countries',$ctry);
		
		$this->set('program_id',$pid);
		
		
	}
}