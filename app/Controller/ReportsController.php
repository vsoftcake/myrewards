<?php
class ReportsController extends AppController {

	var $helpers = array('Csv' );
	var $uses = array('UserAction','Client','User','Country','UserProductView','Product','ClientUserList','Cart','CartItem','CartProduct','Advt4Free');

	/*function __construct() {
		ini_set('memory_limit', '400M');
		set_time_limit('240');
		parent::__construct();
	}*/

	function admin_index() {

	}

	/*
	 *	export the subreport
	 */
	function admin_subreport_export($report_name, $file_num){
		ini_set('max_execution_time', 0);
		$this->layout = "blank";
		$sql = $this->Session->read('Report.all_sql');

		$file_name_array = array(
								'user_actions_per_client' => '_create_uapc_report_array',
								'all_users'=>'_create_ru_report_array',
								'registered_users'=>'_create_ru_report_array',
								'unregistered_users'=>'_create_ru_report_array',
								'products' => '_create_prod_report_array',
								'merchants' =>'_create_marc_report_array',
								'merchant_addresses' =>'_create_madd_report_array',
								'user_actions' =>'_create_ua_report_array',
								'user_statistics' =>'_create_uf_report_array',
								'users_with_prefix'=>'_create_ru_with_prefix_report_array',
								'clients_users_not_logged' =>'_create_not_logged_report_array',
								'logged_on_users' =>'_create_logged_on_users_report_array',
								'multiple_logins_by_member' =>'_create_multiple_logins_by_member_array',
								'actions_by_membership_number' =>'_create_multiple_logins_by_member_array',
								'product_availability' =>'_create_product_availability_array',
								'client_user_export' =>'_create_client_user_selected_report_array',
								'product_views_clients'=>'_create_actions_by_product_views_clients_array',
								'cart_products'=>'_create_cart_products_report_array',
								'advt4free_merchant'=>'_create_advt4_frees_report_array',
		);

		$start = ($file_num -1) * MAX_REPORT_ROW_NUM;

		$sql .= " LIMIT $start, ".MAX_REPORT_ROW_NUM;

		$reports  = $this->UserAction->query($sql);

		if($report_name == 'product_views_clients')
		{
			$client = 	$this->Session->read('Report.client_name');
			$reports = $this->$file_name_array[$report_name]($reports, $client );
		}
		else
		{
			$reports = $this->$file_name_array[$report_name]($reports);
		}

		$this->set('reports',$reports);

		$this->set('name',$report_name);

		$this->set('row_num', MAX_REPORT_ROW_NUM);
	}

	/*
	 * User actions per client Report Page
	 *
	 */
	function admin_user_actions_pc() {

		$first_day_of_last_month = date("Y-m-d",mktime(0,0,1,date("m") -1,1,date("Y"))) ;
		$start_year = date("Y",mktime(0,0,1,date("m") -1,1,date("Y"))) ;
		$last_day_of_last_month  = date("Y-m-d",mktime(0,0,1,date("m"),1,date("Y")) - 3600 * 24);
		$end_year  =  date("Y",mktime(0,0,1,date("m"),1,date("Y")) - 3600 * 24);

		$this->set('start_default',$first_day_of_last_month);
		$this->set('end_default',$last_day_of_last_month);
		$this->set('start_year',$start_year);
		$this->set('end_year',$end_year);

		$this->Client->Category->recursive = -1;
		$categories = $this->Client->Category->find('list', array('order'=>'description, name', 'fields'=>'id,description'));

		$this->Client->Program->recursive = -1;
		$programs= $this->Client->Program->find("list");

		//	create the programs_clients array
		$programs_clients = array();
		foreach ($programs as $program_id => $value) {
			$programs_clients[$program_id] = $this->Client->find("list", array('conditions' => "Client.program_id = '". $program_id. "'"));
		}

		$countries = $this->Country->find('list', array('fields' => array('Country.name','Country.name')));
		$states = array();

		$this->set('programs_clients', $programs_clients);

		$this->Client->recursive = -1;
		$clients= $this->Client->find("list",array('order'=>'Client.name','conditions'=>array('Client.name!=""')));

		$this->set(compact('categories','programs','clients','countries','states'));
	}

	/*
	 * Export User actions per client Report
	 *
	 */
	function admin_user_actions_pc_export(){

		$this->layout = "blank";
		ini_set('max_execution_time', 0);

		//create reports
		if(!empty($this->request->data)){
			$start_date = $this->request->data['start_year']['year'].'-'.$this->request->data['Report']['start_date']['month'].'-'.$this->request->data['Report']['start_date']['day'];
			$end_date = $this->request->data['end_year']['year'].'-'.$this->request->data['Report']['end_date']['month'].'-'.$this->request->data['Report']['end_date']['day'];
			$all_sql = "SELECT DISTINCT ct.name, ua.created, ua.url, cg.name, p1.title, p1.parent_id, p2.title, u.country, u.state ";
			$count_sql = "SELECT COUNT(DISTINCT ua.id) as total ";
			$sql = "	FROM user_actions as ua
							INNER JOIN users as u ON u.id = ua.user_id
							LEFT JOIN categories_products as cp ON cp.product_id = ua.product_id
							LEFT JOIN clients as ct ON ct.id = ua.client_id
							LEFT JOIN categories as cg ON cg.id = cp.category_id
							INNER JOIN pages AS p1 ON p1.id = ua.page_id
							LEFT JOIN pages AS p2 ON p1.parent_id = p2.id
							WHERE ua.created >= '{$start_date}' AND DATE(ua.created) <='{$end_date}' ";
			$start = 0;
			$sql .= " AND EXISTS (SELECT category_id FROM clients_categories AS cc WHERE cc.client_id = ua.client_id AND cc.category_id = cg.id) ";


			//	limit to clients
			if(empty($this->request->data['Report']['client'])){
				$this->request->data['Report']['client'] = array();
			}
			$sql .= " AND ua.client_id IN ('". implode("','", $this->request->data['Report']['client']). "') ";

			//	limit to categories
			if(!empty($this->request->data['Report']['category'])){
				if ($this->request->data['Report']['category'][0] != '') {
					$sql .= " AND cp.category_id IN ('". implode("','", $this->request->data['Report']['category']). "') ";
				}
			}

			if(!empty($this->request->data['Report']['country'])){
				$sql.= " and u.country = '".$this->request->data['Report']['country']."' ";
			}
			if($this->request->data['Report']['country'] !='Hong Kong')
			{
				if(!empty($this->request->data['Report']['state'])){
					$sql.= " and u.state = '".$this->request->data['Report']['state']."' ";
				}
			}

			$all_sql .= $sql;
			$count_sql .= $sql;

			$counts = $this->UserAction->query($count_sql);

			$this->set('row_num', $counts[0][0]['total']);
			$this->set('name','user_actions_per_client');

			// echo $all_sql;
			// exit();


			if($counts[0][0]['total'] > MAX_REPORT_ROW_NUM){
				//
				$this->layout = "admin";
				$this->Session->write('Report.all_sql',$all_sql);

			}else{

				$reports  = $this->UserAction->query($all_sql);

				$reports = $this->_create_uapc_report_array($reports);

				$this->set('reports',$reports);

			}
		}
	}


	/**
	 * Create User actions per client Report
	 */
	function _create_uapc_report_array($reports) {

		foreach ($reports as $key => $report) {

			if($report['p1']['parent_id'] == 0){
				$section = $report['p1']['title'];
				$subpage = "";
			}else{
				$section = $report['p2']['title'];
				$subpage = $report['p1']['title'];
			}


			$reports[$key]['Export'] = array(
				'Client Name' 		=>	$report['ct']['name'],
				'Date'		=>	date('Y-m-d', strtotime($report['ua']['created'])),
				'Section'		=>	$section,
				'Action or Subpage'		=>	$subpage,
				'Category' => $report['cg']['name'],
				'Country'=> $report['u']['country'],
				'State'=> $report['u']['state']
			);
		}

		return $reports;
	}


	/*
	 * Users per client
	 *
	 */
	function admin_users_per_client() {

		$this->Client->Program->recursive = -1;
		$programs= $this->Client->Program->find("list");

		$this->Client->recursive = -1;
		$clients= $this->Client->find("list",array('order'=>'Client.name','conditions'=>array('Client.name!=""')));

		//	create the programs_clients array
		$programs_clients = array();
		foreach ($programs as $program_id => $value) {
			$programs_clients[$program_id] = $this->Client->find("list", array('conditions' => "Client.program_id = '". $program_id. "'"));
		}
		$this->set('programs_clients', $programs_clients);

		$countries = $this->Country->find('list', array('fields' => array('Country.name','Country.name')));
		$states = array();
		$this->set(compact('programs','clients','countries','states'));
		$this->set('registered',1);
	}


	/*
	 * Export users per client
	 *
	 */
	function admin_users_per_client_export(){
		$this->layout = "blank";
		ini_set('max_execution_time', 0);

		if(!empty($this->request->data)){
			$select = "SELECT ct.id, ct.name, COUNT(ct.name) AS num FROM clients as ct
								LEFT JOIN users as us ON ct.id = us.client_id ";

			//	limit to clients
			if(empty($this->request->data['Report']['client'])){
				$this->request->data['Report']['client'] = array();
			}
			$where = " WHERE ct.id IN ('". implode("','", $this->request->data['Report']['client']). "') ";

			if(!empty($this->request->data['Report']['country'])){
				$where.= " and us.country = '".$this->request->data['Report']['country']."' ";
			}
			if($this->request->data['Report']['country'] !='Hong Kong')
			{
				if(!empty($this->request->data['Report']['state'])){
					$where.= " and us.state = '".$this->request->data['Report']['state']."' ";
				}
			}

			$group =" Group BY ct.id ORDER BY ct.name ";

			$all_users  = $this->User->query($select. $where. $group);
			$reg_users  = $this->User->query($select. $where. ' AND registered = 1 '. $group);
			$unreg_users  = $this->User->query($select. $where. ' AND registered = 0 '. $group);

			$reg_clients = array();
			foreach ($reg_users as $client) {
				$reg_clients[$client['ct']['id']] = $client;
			}

			$unreg_clients = array();
			foreach ($unreg_users as $client) {
				$unreg_clients[$client['ct']['id']] = $client;
			}

			$export_report = array();
			foreach ($all_users as $key => $row) {
				$export_report[$key]['Export'] = array(
						'Client Name' 		=>	$row['ct']['name'],
						'Registered'			=>	isset($reg_clients[$row['ct']['id']])? $reg_clients[$row['ct']['id']][0]['num']: 0,
						'Unregistered'		=>	isset($unreg_clients[$row['ct']['id']])? $unreg_clients[$row['ct']['id']][0]['num']: 0,
						'Total'					=>	$row[0]['num'],
				);
			}

			$this->set('reports',$export_report);
		}
	}

	/*
	 * Total product views
	 *
	 */
	function admin_product_views() {

		$first_day_of_last_month = date("Y-m-d",mktime(0,0,1,date("m") -1,1,date("Y"))) ;
		$start_year = date("Y",mktime(0,0,1,date("m") -1,1,date("Y"))) ;
		$last_day_of_last_month  = date("Y-m-d",mktime(0,0,1,date("m"),1,date("Y")) - 3600 * 24);
		$end_year  =  date("Y",mktime(0,0,1,date("m"),1,date("Y")) - 3600 * 24);

		$this->set('start_default',$first_day_of_last_month);
		$this->set('end_default',$last_day_of_last_month);
		$this->set('start_year',$start_year);
		$this->set('end_year',$end_year);

		$this->Client->Category->recursive = -1;
		$categories = $this->Client->Category->find('list', array('order'=>'description, name', 'fields'=>'id,description'));

		$this->Client->Program->recursive = -1;
		$programs= $this->Client->Program->find("list");

		//	create the programs_clients array
		$programs_clients = array();
		foreach ($programs as $program_id => $value) {
			$programs_clients[$program_id] = $this->Client->find("list", array('conditions' => "Client.program_id = '". $program_id. "'"));
		}
		$this->set('programs_clients', $programs_clients);

		$this->Client->recursive = -1;
		$clients= $this->Client->find("list",array('order'=>'Client.name','conditions'=>array('Client.name!=""')));

		$this->set(compact('categories','programs','clients'));
	}

	/*
	 * Total product views export
	 */
	function admin_product_views_export() {

		$this->layout = "blank";
		ini_set('max_execution_time', 0);

		if(!empty($this->request->data)){
			$products = $this->UserAction->query("
				SELECT Product.id, Product.name, Category.name, count(UserAction.product_id) as count, ProductsTracker.clicks, ProductsTracker.prints
				FROM products_trackers as ProductsTracker, user_actions AS UserAction
				INNER JOIN products AS Product ON UserAction.product_id = Product.id
				INNER JOIN pages AS Page ON UserAction.page_id = Page.id
				LEFT JOIN categories_products AS CategoriesProduct ON CategoriesProduct.product_id = Product.id
				LEFT JOIN categories AS Category ON Category.id = CategoriesProduct.category_id
				WHERE Page.name = '/products/view'
				AND Category.id IN ('". implode("','", $this->request->data['Report']['category']). "')
				AND UserAction.created >= '". $this->request->data['start_year']['year'].'-'.$this->request->data['Report']['start_date']['month'].'-'.$this->request->data['Report']['start_date']['day']. "'
				AND UserAction.created <= '". $this->request->data['end_year']['year'].'-'.$this->request->data['Report']['end_date']['month'].'-'.$this->request->data['Report']['end_date']['day']. "'
				AND ProductsTracker.product_id=Product.id
				GROUP BY Product.id
			");

			$export_report = array();
			foreach ($products as $key => $product) {
				$export_report[$key]['Export'] = array(
					'Product id' 		=>	$product['Product']['id'],
					'Product name' 		=>	$product['Product']['name'],
					'Product category'  => $product['Category']['name'],
					'Views' 			=>	$product[0]['count'],
					'Clicks'			=> $product['ProductsTracker']['clicks'],
					'Prints'			=> $product['ProductsTracker']['prints']
				);
			}
			$this->set('reports',$export_report);
		}
	}

	/*
	 * Export registered users reports
	 *
	 */
	function admin_export_users($registered='all') {

		$this->Client->Program->recursive = -1;
		$programs= $this->Client->Program->find("list");

		$this->Client->recursive = -1;
		$clients= $this->Client->find("list",array('order'=>'Client.name','conditions'=>array('Client.name!=""')));

		//	create the programs_clients array
		$programs_clients = array();
		foreach ($programs as $program_id => $value) {
			$programs_clients[$program_id] = $this->Client->find("list", array('conditions' => "Client.program_id = '". $program_id. "'"));
		}

		$countries = $this->Country->find('list', array('fields' => array('Country.name','Country.name')));

		$states = array();

		$start_default = date("Y-m-d",mktime(0,0,1,date("m") -1,1,date("Y"))) ;
		$start_year = date("Y",mktime(0,0,1,date("m") -1,1,date("Y"))) ;
		$end_default  = date("Y-m-d",mktime(0,0,1,date("m"),1,date("Y")) - 3600 * 24);
		$end_year  =  date("Y",mktime(0,0,1,date("m"),1,date("Y")) - 3600 * 24);

		$this->set('programs_clients', $programs_clients);

		$this->set(compact('programs','clients','countries','states','start_default','end_default','start_year','end_year'));
		$this->set('registered', $registered);
	}

	/*
	 * Export Registered users report
	 *
	 */
	function admin_users_export($registered = 'all'){

		$this->layout = "blank";
		ini_set('max_execution_time', 0);
		//create reports
		if(!empty($this->request->data)){
		    $start_date = $this->request->data['start_year']['year'].'-'.$this->request->data['Report']['start_date']['month'].'-'.$this->request->data['Report']['start_date']['day'];
			$end_date = $this->request->data['end_year']['year'].'-'.$this->request->data['Report']['end_date']['month'].'-'.$this->request->data['Report']['end_date']['day'];

			$all_sql = "SELECT DISTINCT ct.id, ct.name, us.* ";
			$count_sql = "SELECT count(DISTINCT us.id) as total ";

			$sql ="FROM users as us
						LEFT JOIN clients as ct ON ct.id = us.client_id ";

			//	limit to clients
			if(empty($this->request->data['Report']['client'])){
				$this->request->data['Report']['client'] = array();
			}
			$sql .= " WHERE us.client_id IN ('". implode("','", $this->request->data['Report']['client']). "') ";
			$sql .= "And us.created >= '{$start_date}' AND DATE(us.created) <='{$end_date}'";
			switch ($registered ) {
				case "registered":
					$sql .= " AND registered = 1 ";
					$this->set('name','registered_users');
					break;
				case "unregistered":
					$sql .= " AND registered = 0 ";
					$this->set('name','unregistered_users');
					break;
				default :
					$this->set('name','all_users');
			}

			if(!empty($this->request->data['Report']['country'])){
				$sql.= " and us.country = '".$this->request->data['Report']['country']."' ";
			}
			if($this->request->data['Report']['country'] !='Hong Kong')
			{
				if(!empty($this->request->data['Report']['state'])){
					$sql.= " and us.state = '".$this->request->data['Report']['state']."' ";
				}
			}
			$all_sql .= $sql;
			$count_sql .= $sql;

			$counts = $this->UserAction->query($count_sql);

			$this->set('row_num', $counts[0][0]['total']);

			if($counts[0][0]['total'] > MAX_REPORT_ROW_NUM) {
				$this->layout = "admin";
				$this->Session->write('Report.all_sql',$all_sql);

			}else{

				$reports  = $this->UserAction->query($all_sql);

				$reports = $this->_create_ru_report_array($reports);

				$this->set('reports',$reports);

			}
		}
	}


	/**Create Registered users report
	 *
	 */
	function _create_ru_report_array($reports) {

		$tmp_reports = array();

		foreach ($reports as $key => $report) {
			$tmp_reports[$key]['Export']['client_id'] = $report['ct']['id'];
			$tmp_reports[$key]['Export']['client'] = $report['ct']['name'];
			foreach ($report['us'] as $col => $val) {
				$tmp_reports[$key]['Export'][$col] = $val;
			}
		}
		return $tmp_reports;
	}

	/*
	 * Export Products Report
	 *
	 */
	function admin_products() {
		$first_day_of_last_month = date("Y-m-d",mktime(0,0,1,date("m") -1,1,date("Y"))) ;
		$start_year = date("Y",mktime(0,0,1,date("m") -1,1,date("Y"))) ;
		$last_day_of_last_month  = date("Y-m-d",mktime(0,0,1,date("m"),1,date("Y")) - 3600 * 24);
		$end_year  =  date("Y",mktime(0,0,1,date("m"),1,date("Y")) - 3600 * 24);

		$this->set('start_default',$first_day_of_last_month);
		$this->set('end_default',$last_day_of_last_month);
		$this->set('start_year',$start_year);
		$this->set('end_year',$end_year);

		$this->Client->Category->recursive = -1;
		$categories = $this->Client->Category->find('list', array('order'=>'description, name', 'fields'=>'id,description'));

		$this->Client->Program->recursive = -1;
		$programs= $this->Client->Program->find("list");

		$this->Client->recursive = -1;
		$clients= $this->Client->find("list",array('order'=>'Client.name','conditions'=>array('Client.name!=""')));

		//	create the programs_clients array
		$programs_clients = array();
		foreach ($programs as $program_id => $value) {
			$programs_clients[$program_id] = $this->Client->find("list", array('conditions' => "Client.program_id = '". $program_id. "'"));
		}

		//	create the programs_categories array
		$programs_categories = array();
		/*foreach ($programs as $program_id => $value) {
		$programs_categories[$program_id] = $this->Client->Category->ProgramsCategory->find('list', array('fields' => array('Category.id', 'Category.name'),
		'conditions' => array("ProgramsCategory.program_id = '". $program_id. "'"),'recursive' => 0 ));
		}*/

		$countries = $this->Country->find('list', array('fields' => array('Country.name','Country.name')));

		$states = array();
		$this->set('programs_clients', $programs_clients);
		$this->set('programs_categories', $programs_categories);

		$this->set(compact('categories','programs','clients','countries','states'));
	}

	/*
	 * Export Products Report
	 *
	 */
	function admin_products_export(){

		$this->layout = "blank";
		ini_set('max_execution_time', 0);
		if(!empty($this->request->data)){

			$all_sql = "SELECT DISTINCT products.*, merchants.*, merchant_addresses.*, categories.name ";
			$count_sql = "SELECT count(DISTINCT products.id) as total ";

			$sql = " FROM products  ";
			$sql .=" LEFT JOIN merchants ON merchants.id = products.merchant_id  ";
			$sql.=" LEFT JOIN merchant_addresses ON merchant_addresses.merchant_id = products.merchant_id  ";
			$sql.=" LEFT JOIN categories_products ON categories_products.product_id = products.id
						LEFT JOIN categories ON categories.id = categories_products.category_id
						LEFT JOIN clients_products  ON clients_products.product_id = products.id
						LEFT JOIN clients ON clients.id = clients_products.client_id
						WHERE 1=1 ";

			if(($this->request->data['Report']['product_active']==1) )
			{
				$sql.= " AND products.active = 1 ";
			}
			if(($this->request->data['Report']['product_expire']==1) )
			{
				$sql.= " AND products.active = 0 ";
			}
			if(($this->request->data['Report']['product_cancelled']==1))
			{
				$sql.= " AND products.cancelled = 1 ";
			}
			if(($this->request->data['Report']['product_uncancelled']==1))
			{
				$sql.= " AND products.cancelled = 0 ";
			}

			$clients = implode("','", $this->request->data['Report']['client']);
			$clients = strlen($clients)>0?$clients.",0":"0";
			//	limit to clients
			if(!empty($this->request->data['Report']['client'])){
				$sql .= " AND clients_products.client_id IN ('". $clients. "') OR clients_products.client_id=0 ";
			}


			//	limit to categories
			if(!empty($this->request->data['Report']['category'])){
				if ($this->request->data['Report']['category'][0] != '') {
					$sql .= " AND categories_products.category_id IN ('". implode("','", $this->request->data['Report']['category']). "') ";
				}
			}

			//created from
			if(isset($this->request->data['created_from_year'])){
				$created_from = $this->request->data['created_from_year']['year'].'-'.$this->request->data['Report']['created_from']['month'].'-'.$this->request->data['Report']['created_from']['day'];
				$sql.= " AND products.created >= '$created_from' ";

			}

			//created to
			if(isset($this->request->data['created_to_year'])){
				$created_to = $this->request->data['created_to_year']['year'].'-'.$this->request->data['Report']['created_to']['month'].'-'.$this->request->data['Report']['created_to']['day'];
				$sql.= " AND DATE(products.created) <= '$created_to' ";
			}

			//modified from
			if(isset($this->request->data['modified_from_year'])){
				$modified_from = $this->request->data['modified_from_year']['year'].'-'.$this->request->data['Report']['modified_from']['month'].'-'.$this->request->data['Report']['modified_from']['day'];
				$sql.= " AND products.modified >= '$modified_from' ";
			}

			//modified to
			if(isset($this->request->data['modified_to_year'])){
				$modified_to = $this->request->data['modified_to_year']['year'].'-'.$this->request->data['Report']['modified_to']['month'].'-'.$this->request->data['Report']['modified_to']['day'];
				$sql.= " AND DATE(products.modified) <= '$modified_to' ";
			}

			if(!empty($this->request->data['Report']['country'])){
				$sql.= " and merchant_addresses.country = '".$this->request->data['Report']['country']."' ";
			}
			if($this->request->data['Report']['country'] !='Hong Kong')
			{
				if(!empty($this->request->data['Report']['state'])){
					$sql.= " and merchant_addresses.state = '".$this->request->data['Report']['state']."' ";
				}
			}

            $sql .= " GROUP BY products.id";
			$all_sql .= $sql;
			$count_sql .= $sql;

			$counts = $this->UserAction->query($count_sql);


			$this->set('row_num', $counts[0][0]['total']);

			$this->set('name','products');

			if($counts[0][0]['total'] > MAX_REPORT_ROW_NUM){

				$this->layout = "admin";
				$this->Session->write('Report.all_sql',$all_sql);

			}else{
				$reports  = $this->UserAction->query($all_sql);
				$tempreport;

				ini_set( 'memory_limit', '-1' );
				$otherreport = $this->_create_prod_report_array($reports);
				//array_($tempreport,$otherreport);

				$this->set('reports',$otherreport);
			}
		}
	}

	function admin_product_availability_export()
	{
    	$this->layout = "blank";
		ini_set('max_execution_time', 0);
		$count_sql = "select sum(total) from (SELECT count(Product.id) as total ";
		$all_sql ="SELECT Product.id, Product.name, Client.id,Client.name";

		$sql = " FROM clients_products AS ClientsProduct
		LEFT JOIN clients AS Client ON (ClientsProduct.client_id = Client.id)
		LEFT JOIN products AS Product ON (ClientsProduct.product_id = Product.id)
		WHERE (ClientsProduct.client_id = ".$this->request->data['Report']['clients']." OR ClientsProduct.client_id = 0) ";
		$sql.=" group by Product.name";
	    $all_sql .= $sql;
	    $count_sql .= $sql;
	    $count_sql .=" ) as t";

	    $counts = $this->Product->query($count_sql);

        $this->set('row_num', $counts[0][0]['sum(total)']);
        $this->set('name','product_availability');

        if($counts[0][0]['sum(total)'] >MAX_REPORT_ROW_NUM){
		$this->layout = "admin";
        $this->Session->write('Report.all_sql',$all_sql);
        }else{
        $reports  = $this->Product->query($all_sql);
    	$reports = $this->_create_product_availability_array($reports);
        $this->set('reports',$reports);
      }

	       }
	function _create_product_availability_array($reports)
	    {
	      foreach ($reports as $key => $report)
	       {
	                            $product_id= $report['Product']['id'];
	                $product_name=$report['Product']['name'];
	                $client_id= $report['Client']['id'];
	                $client_name= $report['Client']['name'];

	                       $reports[$key]['Export'] = array(
	                               'Product_id'   =>  $product_id,
	                           'Product_name' =>  $product_name,
	                           'Client_id'    =>  $client_id,
	                           'Client_name'  =>  $client_name);
	               }

	               return $reports;

	   }

	/**Create products Report
	 *
	 */
	function _create_prod_report_array($rep) {

				foreach ($rep as $key => $report) {

					$treport[$key]['products']['text'] = $rep[$key]['products']['text'];
					$treport[$key]['products']['details'] = $rep[$key]['products']['details'];
					$treport[$key]['products']['special_offer_body'] =$rep[$key]['products']['special_offer_body'];
					$treport[$key]['products']['keyword'] = $rep[$key]['products']['keyword'];
					$treport[$key]['products']['terms_and_conditions'] = $rep[$key]['products']['terms_and_conditions'];


					foreach($report['products'] as $field => $product){

						$treport[$key]['Export']['Product.'.$field] = preg_replace("/&#?[a-z0-9]{2,8};/i","",strip_tags($product));

					}

					//if($include_merchant ==1){
						foreach($report['merchants'] as $field => $product){

							$treport[$key]['Export']['Merchant.'.$field] = $product;
						}
					//}

					//if($include_address ==1){
						foreach($report['merchant_addresses'] as $field => $product){

							$treport[$key]['Export']['MerchantAddress.'.$field] = $product;
						}
					//}
				foreach($report['categories'] as $field => $product){

							$treport[$key]['Export']['Category.'.$field] = $product;
						}


				}

				return $treport;
	}

	/*
	 * Product availability report
	 *
	 */
	function admin_product_availability() {
		$first_day_of_last_month = date("Y-m-d",mktime(0,0,1,date("m") -1,1,date("Y"))) ;
		$start_year = date("Y",mktime(0,0,1,date("m") -1,1,date("Y"))) ;
		$last_day_of_last_month  = date("Y-m-d",mktime(0,0,1,date("m"),1,date("Y")) - 3600 * 24);
		$end_year  =  date("Y",mktime(0,0,1,date("m"),1,date("Y")) - 3600 * 24);

		$this->set('start_default',$first_day_of_last_month);
		$this->set('end_default',$last_day_of_last_month);
		$this->set('start_year',$start_year);
		$this->set('end_year',$end_year);

		$this->Client->Category->recursive = -1;
		$categories = $this->Client->Category->find('list', array('order'=>'description, name', 'fields'=>'id,description'));

		$this->Client->Program->recursive = -1;
		$programs= $this->Client->Program->find("list");
		$clientslist = array();
		$this->set('clientslist',$clientslist);
		$this->set(compact('categories','programs'));
	}

	function admin_clients()
    {
    	$selected_program= $this->request->data['Report']['program_id'];

		$sql = "select Client.id, Client.name from clients as Client where Client.program_id=".$selected_program." order by Client.name";
    	$clients = $this->Client->query($sql);
		$clientslist = array();

		foreach ($clients as $client) {
			$clientslist[$client['Client']['id']] = $client['Client']['name'];
		}

        $this->set('list',$clientslist);

        $this->layout = 'ajax';
    }

	/*
	 * Export Merchant Addresses Report
	 *
	 */
	function admin_merchants() {
		$first_day_of_last_month = date("Y-m-d",mktime(0,0,1,date("m") -1,1,date("Y"))) ;
		$start_year = date("Y",mktime(0,0,1,date("m") -1,1,date("Y"))) ;
		$last_day_of_last_month  = date("Y-m-d",mktime(0,0,1,date("m"),1,date("Y")) - 3600 * 24);
		$end_year  =  date("Y",mktime(0,0,1,date("m"),1,date("Y")) - 3600 * 24);

		$this->set('start_default',$first_day_of_last_month);
		$this->set('end_default',$last_day_of_last_month);
		$this->set('start_year',$start_year);
		$this->set('end_year',$end_year);

		$this->Client->Category->recursive = -1;
		$categories = $this->Client->Category->find('list', array('order'=>'description, name', 'fields'=>'id,description'));

		$this->Client->Program->recursive = -1;
		$programs= $this->Client->Program->find("list");

		$this->Client->recursive = -1;
		$clients= $this->Client->find("list",array('order'=>'Client.name','conditions'=>array('Client.name!=""')));

		//	create the programs_clients array
		$programs_clients = array();
		foreach ($programs as $program_id => $value) {
			$programs_clients[$program_id] = $this->Client->find("list", array('conditions' => "Client.program_id = '". $program_id. "'"));
		}

		$countries = $this->Country->find('list', array('fields' => array('Country.name','Country.name')));
		$states = array();
		$this->set('programs_clients', $programs_clients);

		$this->set(compact('categories','programs','clients','countries','states'));
	}

	/*
	 * Export Merchant Addresses Report
	 *
	 */
	function admin_merchants_export(){
		$this->layout = "blank";
		ini_set('max_execution_time', 0);
		//create reports
		if(!empty($this->request->data)){

			$all_sql = "SELECT DISTINCT merchants.* ";
			$count_sql = "SELECT count(DISTINCT merchants.id) as total ";

			$sql =" FROM merchants  ";

			$sql.=" 	LEFT JOIN products ON merchants.id = products.merchant_id
						LEFT JOIN categories_products ON categories_products.product_id = products.id
						LEFT JOIN categories ON categories.id = categories_products.category_id
						LEFT JOIN clients_products  ON clients_products.product_id = products.id
						LEFT JOIN clients ON clients.id = clients_products.client_id
						WHERE 1=1 ";

			//limit to active or inactive merchants
			if($this->request->data['Report']['merchant_active']==1 && $this->request->data['Report']['merchant_inactive']==0)
			{
				$sql.= " AND products.active = 1 AND products.cancelled = 0 ";
			}
			if($this->request->data['Report']['merchant_active']==0 && $this->request->data['Report']['merchant_inactive']==1)
			{
				$sql.= " AND products.active = 0 AND products.cancelled = 1 ";
			}
			if(($this->request->data['Report']['merchant_active']==1 && $this->request->data['Report']['merchant_inactive']==1) || ($this->request->data['Report']['merchant_active']==0 && $this->request->data['Report']['merchant_inactive']==0))
			{
				$sql.= " AND (products.active = 0 OR products.active = 1 OR products.cancelled = 0 OR products.cancelled = 1) ";
			}

			//	limit to clients
			if(!empty($this->request->data['Report']['client'])){
				$clients = implode("','", $this->request->data['Report']['client']);
				$clients = strlen($clients)>0?$clients.",0":"0";
				$sql .= " AND clients_products.client_id IN ('". $clients. "') OR clients_products.client_id=0 ";
			}


			//	limit to categories
			if(!empty($this->request->data['Report']['category'])){
				if ($this->request->data['Report']['category'][0] != '') {
					$sql .= " AND categories_products.category_id IN ('". implode("','", $this->request->data['Report']['category']). "') ";
				}
			}

			if(!empty($this->request->data['Report']['country'])){
				$sql.= " and merchants.mail_country = '".$this->request->data['Report']['country']."' ";
			}
			if($this->request->data['Report']['country'] !='Hong Kong')
			{
				if(!empty($this->request->data['Report']['state'])){
					$sql.= " and merchants.mail_state = '".$this->request->data['Report']['state']."' ";
				}
			}

			//created from
			if(isset($this->request->data['created_from_year'])){
				$created_from = $this->request->data['created_from_year']['year'].'-'.$this->request->data['Report']['created_from']['month'].'-'.$this->request->data['Report']['created_from']['day'];
				$sql.= " AND merchants.created >= '$created_from' ";

			}

			//created to
			if(isset($this->request->data['created_to_year'])){
				$created_to = $this->request->data['created_to_year']['year'].'-'.$this->request->data['Report']['created_to']['month'].'-'.$this->request->data['Report']['created_to']['day'];
				$sql.= " AND DATE(merchants.created) <= '$created_to' ";
			}

			//modified from
			if(isset($this->request->data['modified_from_year'])){
				$modified_from = $this->request->data['modified_from_year']['year'].'-'.$this->request->data['Report']['modified_from']['month'].'-'.$this->request->data['Report']['modified_from']['day'];
				$sql.= " AND merchants.modified >= '$modified_from' ";
			}

			//modified to
			if(isset($this->request->data['modified_to_year'])){
				$modified_to = $this->request->data['modified_to_year']['year'].'-'.$this->request->data['Report']['modified_to']['month'].'-'.$this->request->data['Report']['modified_to']['day'];
				$sql.= " AND DATE(merchants.modified) <= '$modified_to' ";
			}

			$all_sql .= $sql;
			$count_sql .= $sql;

			$counts = $this->UserAction->query($count_sql);

			$this->set('row_num', $counts[0][0]['total']);
			$this->set('name','merchants');

			if($counts[0][0]['total'] > MAX_REPORT_ROW_NUM){
				//
				$this->layout = "admin";
				$this->Session->write('Report.all_sql',$all_sql);

			}else{

				$reports  = $this->UserAction->query($all_sql);

				$reports = $this->_create_marc_report_array($reports);

				$this->set('reports',$reports);

			}

		}
	}



	/**Create Merchant Report
	 *
	 */
	function _create_marc_report_array($reports) {

		foreach ($reports as $key => $report) {

			foreach($report['merchants'] as $field => $product){
				$reports[$key]['Export']['Merchant.'.$field] = $product;
			}

		}

		return $reports;
	}

	/*
	 * Export Merchant Addresses Report
	 *
	 */
	function admin_merchant_addresses() {
		$first_day_of_last_month = date("Y-m-d",mktime(0,0,1,date("m") -1,1,date("Y"))) ;
		$start_year = date("Y",mktime(0,0,1,date("m") -1,1,date("Y"))) ;
		$last_day_of_last_month  = date("Y-m-d",mktime(0,0,1,date("m"),1,date("Y")) - 3600 * 24);
		$end_year  =  date("Y",mktime(0,0,1,date("m"),1,date("Y")) - 3600 * 24);

		$this->set('start_default',$first_day_of_last_month);
		$this->set('end_default',$last_day_of_last_month);
		$this->set('start_year',$start_year);
		$this->set('end_year',$end_year);

		$this->Client->Category->recursive = -1;
		$categories = $this->Client->Category->find('list', array('order'=>'description, name', 'fields'=>'id,description'));

		$this->Client->Program->recursive = -1;
		$programs= $this->Client->Program->find("list");

		$this->Client->recursive = -1;
		$clients= $this->Client->find("list",array('order'=>'Client.name','conditions'=>array('Client.name!=""')));

		//	create the programs_clients array
		$programs_clients = array();
		foreach ($programs as $program_id => $value) {
			$programs_clients[$program_id] = $this->Client->find("list", array('conditions' => "Client.program_id = '". $program_id. "'"));
		}
		$countries = $this->Country->find('list', array('fields' => array('Country.name','Country.name')));
		$states = array();
		$this->set('programs_clients', $programs_clients);

		$this->set(compact('categories','programs','clients','countries','states'));
	}

	/*
	 * Export Merchant Addresses Report
	 *
	 */
	function admin_merchant_addresses_export(){
		$this->layout = "blank";
		ini_set('max_execution_time', 0);
		//create reports
		if(!empty($this->request->data)){

			$all_sql = "SELECT DISTINCT merchants.*, merchant_addresses.* ";
			$count_sql = "SELECT count(DISTINCT merchant_addresses.id) as total ";
			$sql = " FROM merchants  ";

			$sql.=" 	LEFT JOIN merchant_addresses ON merchants.id = merchant_addresses.merchant_id
								LEFT JOIN products ON merchants.id = products.merchant_id
								LEFT JOIN categories_products ON categories_products.product_id = products.id
								LEFT JOIN categories ON categories.id = categories_products.category_id
								LEFT JOIN clients_products  ON clients_products.product_id = products.id
								LEFT JOIN clients ON clients.id = clients_products.client_id WHERE
								products.active = 1 and products.cancelled = 0 and 1=1
								";

			//	limit to clients
			if(!empty($this->request->data['Report']['client'])){
				$clients = implode("','", $this->request->data['Report']['client']);
				$clients = strlen($clients)>0?$clients.",0":"0";
				$sql .= " AND clients_products.client_id IN ('". $clients. "') ";
			}


			//	limit to categories
			if(!empty($this->request->data['Report']['category'])){
				if ($this->request->data['Report']['category'][0] != '') {
					$sql .= " AND categories_products.category_id IN ('". implode("','", $this->request->data['Report']['category']). "') ";
				}
			}

			if(!empty($this->request->data['Report']['country'])){
				$sql.= " and merchant_addresses.country = '".$this->request->data['Report']['country']."' ";
			}
			if($this->request->data['Report']['country'] !='Hong Kong')
			{
				if(!empty($this->request->data['Report']['state'])){
					$sql.= " and merchant_addresses.state = '".$this->request->data['Report']['state']."' ";
				}
			}

			//created from
			if(isset($this->request->data['created_from_year'])){
				$created_from = $this->request->data['created_from_year']['year'].'-'.$this->request->data['Report']['created_from']['month'].'-'.$this->request->data['Report']['created_from']['day'];
				$sql.= " AND (merchants.created >= '$created_from' OR merchant_addresses.created >= '$created_from')";

			}

			//created to
			if(isset($this->request->data['created_to_year'])){
				$created_to = $this->request->data['created_to_year']['year'].'-'.$this->request->data['Report']['created_to']['month'].'-'.$this->request->data['Report']['created_to']['day'];
				$sql.= " AND (DATE(merchants.created) <= '$created_to' OR DATE(merchant_addresses.created) <= '$created_to')";
			}

			//modified from
			if(isset($this->request->data['modified_from_year'])){
				$modified_from = $this->request->data['modified_from_year']['year'].'-'.$this->request->data['Report']['modified_from']['month'].'-'.$this->request->data['Report']['modified_from']['day'];
				$sql.= " AND (merchants.modified >= '$modified_from' OR merchant_addresses.modified >= '$modified_from') ";
			}

			//modified to
			if(isset($this->request->data['modified_to_year'])){
				$modified_to = $this->request->data['modified_to_year']['year'].'-'.$this->request->data['Report']['modified_to']['month'].'-'.$this->request->data['Report']['modified_to']['day'];
				$sql.= " AND (DATE(merchants.modified) <= '$modified_to' OR DATE(merchant_addresses.modified) <= '$modified_to')";
			}

			$all_sql .= $sql;
			$count_sql .= $sql;

			$counts = $this->UserAction->query($count_sql);

			$this->set('row_num', $counts[0][0]['total']);
			$this->set('name','merchant_addresses');

			if($counts[0][0]['total'] > MAX_REPORT_ROW_NUM){
				//
				$this->layout = "admin";
				$this->Session->write('Report.all_sql',$all_sql);

			}else{

				$reports  = $this->UserAction->query($all_sql);

				$reports = $this->_create_madd_report_array($reports);

				$this->set('reports',$reports);

			}

		}
	}



	/**Create Merchant Addresses Report
	 *
	 */
	function _create_madd_report_array($reports) {

		foreach ($reports as $key => $report) {

			foreach($report['merchants'] as $field => $product){
				$reports[$key]['Export']['Merchant.'.$field] = $product;
			}

			foreach($report['merchant_addresses'] as $field => $product){
				$reports[$key]['Export']['MerchantAddress.'.$field] = $product;
			}

		}

		return $reports;
	}


	/*
	 * User actions Reports
	 *
	 */
	function admin_user_actions() {
		$first_day_of_last_month = date("Y-m-d",mktime(0,0,1,date("m") -1,1,date("Y"))) ;
		$start_year = date("Y",mktime(0,0,1,date("m") -1,1,date("Y"))) ;
		$last_day_of_last_month  = date("Y-m-d",mktime(0,0,1,date("m"),1,date("Y")) - 3600 * 24);
		$end_year  =  date("Y",mktime(0,0,1,date("m"),1,date("Y")) - 3600 * 24);

		$this->set('start_default',$first_day_of_last_month);
		$this->set('end_default',$last_day_of_last_month);
		$this->set('start_year',$start_year);
		$this->set('end_year',$end_year);

		$this->Client->Category->recursive = -1;
		$categories = $this->Client->Category->find('list', array('order'=>'description, name', 'fields'=>'id,description'));

		$this->Client->Program->recursive = -1;
		$programs= $this->Client->Program->find("list");

		$this->Client->recursive = -1;
		$clients= $this->Client->find("list",array('order'=>'Client.name','conditions'=>array('Client.name!=""')));

		//	create the programs_clients array
		$programs_clients = array();
		foreach ($programs as $program_id => $value) {
			$programs_clients[$program_id] = $this->Client->find("list", array('conditions' => "Client.program_id = '". $program_id. "'"));
		}

		$countries = $this->Country->find('list', array('fields' => array('Country.name','Country.name')));
		$states = array();
		$this->set('programs_clients', $programs_clients);

		$this->set(compact('categories','programs','clients','countries','states'));
	}

	/*
	 * First time logon statistics Reports
	 *
	 */
	function admin_user_statistics() {
		$first_day_of_last_month = date("Y-m-d",mktime(0,0,1,date("m") -1,1,date("Y"))) ;
		$start_year = date("Y",mktime(0,0,1,date("m") -1,1,date("Y"))) ;
		$last_day_of_last_month  = date("Y-m-d",mktime(0,0,1,date("m"),1,date("Y")) - 3600 * 24);
		$end_year  =  date("Y",mktime(0,0,1,date("m"),1,date("Y")) - 3600 * 24);

		$this->set('start_default',$first_day_of_last_month);
		$this->set('end_default',$last_day_of_last_month);
		$this->set('start_year',$start_year);
		$this->set('end_year',$end_year);

		$this->Client->Category->recursive = -1;
		$categories = $this->Client->Category->find('list', array('order'=>'description, name', 'fields'=>'id,description'));

		$this->Client->Program->recursive = -1;
		$programs= $this->Client->Program->find("list");

		$this->Client->recursive = -1;
		$clients= $this->Client->find("list",array('order'=>'Client.name','conditions'=>array('Client.name!=""')));

		//	create the programs_clients array
		$programs_clients = array();
		foreach ($programs as $program_id => $value) {
			$programs_clients[$program_id] = $this->Client->find("list", array('conditions' => "Client.program_id = '". $program_id. "'"));
		}
		$this->set('programs_clients', $programs_clients);

		$this->set(compact('categories','programs','clients'));
	}




	/*
	 * Export User actions per client Report
	 *
	 */
	function admin_user_actions_export(){
		$this->layout = "blank";
		ini_set('max_execution_time', 0);
		//create reports
		if(!empty($this->request->data)){
			$start_date = $this->request->data['start_year']['year'].'-'.$this->request->data['Report']['start_date']['month'].'-'.$this->request->data['Report']['start_date']['day'];
			$end_date = $this->request->data['end_year']['year'].'-'.$this->request->data['Report']['end_date']['month'].'-'.$this->request->data['Report']['end_date']['day'];
			$all_sql = "SELECT  DISTINCT ct.name, ua.created, ua.url, users.first_name, users.username, Category.id, Category.name, Category.description  ";
			$count_sql = "SELECT count(DISTINCT ua.id) as total ";

			//	modified to get the first category that a product is in if there is a product id (see sub query)
			$sql	="FROM user_actions as ua
								INNER JOIN users ON users.id = ua.user_id
								LEFT JOIN clients as ct ON ct.id = ua.client_id
								LEFT JOIN categories_products AS CategoriesProduct ON ua.product_id = CategoriesProduct.product_id
								LEFT JOIN categories AS Category ON CategoriesProduct.category_id = Category.id
								WHERE ua.created >= '{$start_date}' AND DATE(ua.created) <='{$end_date}'
								AND (ua.product_id =  0
									OR CategoriesProduct.category_id = (
										SELECT min(CategoriesProduct.category_id)
										FROM categories_products AS CategoriesProduct
										WHERE CategoriesProduct.product_id = ua.product_id
									)
								)
								";

			$start = 0;

			//	limit to clients
			if(empty($this->request->data['Report']['client'])){
				$this->request->data['Report']['client'] = array();
			}
			$sql .= " AND ua.client_id IN ('". implode("','", $this->request->data['Report']['client']). "') ";
			if(!empty($this->request->data['Report']['member'])){
				$sql.= " and users.username ='".$this->request->data['Report']['member']."' ";
			}
			if(!empty($this->request->data['Report']['country'])){
				$sql.= " and users.country = '".$this->request->data['Report']['country']."' ";
			}
			if($this->request->data['Report']['country'] !='Hong Kong')
			{
				if(!empty($this->request->data['Report']['state'])){
					$sql.= " and users.state = '".$this->request->data['Report']['state']."' ";
				}
			}

			$all_sql .= $sql;
			$count_sql .= $sql;

			$counts = $this->UserAction->query($count_sql);

			$this->set('row_num', $counts[0][0]['total']);
			$this->set('name','user_actions');

			if($counts[0][0]['total'] > MAX_REPORT_ROW_NUM){
				//
				$this->layout = "admin";
				$this->Session->write('Report.all_sql',$all_sql);

			}else{

				$reports  = $this->UserAction->query($all_sql);

				$reports = $this->_create_ua_report_array($reports);

				$this->set('reports',$reports);

			}
		}
	}
	/*
	 * Export User statistics per client Report
	 *
	 */
	function admin_user_statistics_export() {
		$this->layout = "blank";
		//create reports
		if(!empty($this->request->data)){
			$start_date = $this->request->data['start_year']['year'].'-'.$this->request->data['Report']['start_date']['month'].'-'.$this->request->data['Report']['start_date']['day'];
			$end_date = $this->request->data['end_year']['year'].'-'.$this->request->data['Report']['end_date']['month'].'-'.$this->request->data['Report']['end_date']['day'];
			$all_sql = "SELECT  DISTINCT ct.name, ua.created, ua.url  ";
			$count_sql = "SELECT count(DISTINCT ua.id) as total ";

			//	modified to get the first category that a product is in if there is a product id (see sub query)
			$sql	="FROM user_actions as ua
								LEFT JOIN clients as ct ON ct.id = ua.client_id
								WHERE ua.created >= '{$start_date}' AND DATE(ua.created) <='{$end_date}'
								AND (ua.url like '%login%' or ua.url like '%first_time_login%'  or ua.url ='/' )
								";

			$start = 0;

			//	limit to clients
			if(empty($this->request->data['Report']['client'])){
				$this->request->data['Report']['client'] = array();
			}
			$sql .= " AND ua.client_id IN ('". implode("','", $this->request->data['Report']['client']). "') GROUP BY ct.name, ua.created ";
			//$sql .= " GROUP BY ct.name, ua.created "

			$all_sql .= $sql;
			$count_sql .= $sql;

			$counts = $this->UserAction->query($count_sql);

			$this->set('row_num', $counts[0][0]['total']);
			$this->set('name','user_statistics');

			if($counts[0][0]['total'] > MAX_REPORT_ROW_NUM){
				//
				$this->layout = "admin";
				$this->Session->write('Report.all_sql',$all_sql);

			}else{

				$reports  = $this->UserAction->query($all_sql);

				$reports = $this->_create_uf_report_array($reports);

				$this->set('reports',$reports);

			}
		}
	}


	/**
	 * Create User actions
	 */
	function _create_ua_report_array($reports) {

		foreach ($reports as $key => $report) {

			$reports[$key]['Export'] = array(
				'User Name'			=>	$report['users']['username'],
				'Client Name' 		=>	$report['ct']['name'],
				'ActionTime'		=>	date('Y-m-d h:i:s', strtotime($report['ua']['created'])),
				'URL'		=>	$report['ua']['url'],
				'Category.id' => $report['Category']['id'],
				'Category.name' => $report['Category']['name'],
				'Category.description' => $report['Category']['description'],
			);
		}

		return $reports;
	}

	/**
	 * Create Client Referrer
	 */
	function _create_uf_report_array($reports) {

		foreach ($reports as $key => $report) {

			$reports[$key]['Export'] = array(
				'Client Name' 		=>	$report['ct']['name'],
				'ActionTime'		=>	date('Y-m-d h:i:s', strtotime($report['ua']['created'])),
				'URL'		=>	$report['ua']['url'],
			);
		}

		return $reports;
	}


	/**Show the logged on users
	 *
	 */
	function admin_logged_on_users() {

		$this->Client->Program->recursive = -1;
		$programs= $this->Client->Program->find("list");

		$this->Client->recursive = -1;
		$clients= $this->Client->find("list",array('order'=>'Client.name','conditions'=>array('Client.name!=""')));

		//	create the programs_clients array
		$programs_clients = array();
		foreach ($programs as $program_id => $value) {
			$programs_clients[$program_id] = $this->Client->find("list", array('conditions' => "Client.program_id = '". $program_id. "'"));
		}

		$countries = $this->Country->find('list', array('fields' => array('Country.name','Country.name')));
		$states = array();
		$this->set('programs_clients', $programs_clients);


		$this->set(compact('programs','clients','countries','states'));
	}

	/*
	 * Export Registered users report per client
	 *
	 */
	function admin_logged_on_users_export(){

		$this->layout = "blank";
		ini_set('max_execution_time', 0);
		$logged_on_time = time() - REPORT_SESSION_EXPIRY;

		//create reports
		$count_sql = "SELECT count(DISTINCT ua.id) as total  ";
	    $all_sql = "SELECT DISTINCT ct.name, MAX(ua.created) as log_time, users.first_name, users.username, users.country,users.state";
		$sql = " FROM user_actions as ua
				LEFT JOIN programs_products as pp ON pp.product_id = ua.product_id
				INNER JOIN users ON users.id = ua.user_id
				LEFT JOIN clients as ct ON ct.id = ua.client_id
				WHERE  UNIX_TIMESTAMP(ua.created) > $logged_on_time ";

		//	limit to clients
		if(!empty($this->request->data['Report']['client'])){
			$sql .= " AND ua.client_id IN ('". implode("','", $this->request->data['Report']['client']). "') ";
		}

		if(!empty($this->request->data['Report']['country'])){
			$sql.= " and users.country = '".$this->request->data['Report']['country']."' ";
		}
		if($this->request->data['Report']['country'] !='Hong Kong')
		{
			if(!empty($this->request->data['Report']['state'])){
				$sql.= " and users.state = '".$this->request->data['Report']['state']."' ";
			}
		}
		$sql.=" GROUP BY ua.user_id";

	 	$all_sql .= $sql;
		$count_sql .= $sql;

        $counts = $this->User->query($count_sql);

		$this->set('row_num', $counts[0][0]['total']);
		$this->set('name','logged_on_users');

		if($counts[0][0]['total'] >MAX_REPORT_ROW_NUM){
			//
			$this->layout = "admin";
			$this->Session->write('Report.all_sql',$all_sql);

		}else{

			$reports  = $this->User->query($all_sql);

			$reports = $this->_create_logged_on_users_report_array($reports);

			$this->set('reports',$reports);

		}
		//$reports  = $this->User->query($sql);

		//$this->set('reports',$reports);
	}


	function _create_logged_on_users_report_array($reports) {
		foreach ($reports as $key => $report) {

				$reports[$key]['Export'] = array(
					'Client Name' 	=>	$report['ct']['name'],
					'User Name'               =>   $report['users']['username'],
				);
			}
			return $reports;
	}

	/**Create total Registered users per clinet report
	 *
	 */
	function _create_lggu_report_array($reports) {

		foreach ($reports as $key => $report) {

			$reports[$key]['Export'] = array(
				'Client Name' 	=>	$report['ct']['name'],
				'User Name'               =>   $report['users']['username'],
			);
		}
		return $reports;
	}

	/*
	 * Populate states using ajax for country in search
	 */
	function admin_update_state()
	{

		$selected_country= $this->request->data['Report']['country'];
		$defaultstatesql ="SELECT State.state_cd, State.state_name FROM states AS State, countries as Country where State.country_cd = Country.id
	   	and Country.name='".$selected_country."'";

		$this->loadModel("State");
		$cstates = $this->State->query($defaultstatesql);

		$ctrystate = array();
		foreach ($cstates as $ctrystates) {
			$ctrystate[$ctrystates['State']['state_cd']] = $ctrystates['State']['state_name'];
		}

		$this->set('cstates',$ctrystate);
		$this->layout = 'ajax';
	}

	/*
	 * Export users with prefix reports
	 *
	 */
	function admin_export_users_with_prefix() {

		$this->Client->recursive = -1;
		$clients= $this->Client->find("list",array('order'=>'Client.name','conditions'=>array('Client.name!=""')));

		$this->set(compact('clients'));
	}

	/*
	 * Export users with prefix report
	 *
	 */
	function admin_users_with_prefix_export(){

		$this->layout = "blank";
		ini_set('max_execution_time', 0);
		$registered = "";
		//create reports
		if(!empty($this->request->data)){
			$registered = $this->request->data['Report']['check'];
			$all_sql = "SELECT DISTINCT ct.id, ct.name, us.* ";
			$count_sql = "SELECT count(DISTINCT us.id) as total ";

			$sql ="FROM users as us
						LEFT JOIN clients as ct ON ct.id = us.client_id ";

			//	limit to clients
			if(empty($this->request->data['Report']['client'])){
				$this->request->data['Report']['client'] = array();
			}
			$sql .= " WHERE us.client_id =". $this->request->data['Report']['client'] ;

			switch ($registered ) {
				case "R":
					$sql .= " AND registered = 1 ";
					$this->set('name','registered_users_with_prefix');
					break;
				case "U":
					$sql .= " AND registered = 0 ";
					$this->set('name','unregistered_users_with_prefix');
					break;
				case "A":
					$this->set('name','all_users_with_prefix');
					break;
				default :
					$this->set('name','all_users_with_prefix');
			}

			if(!empty($this->request->data['Report']['prefix'])){
				$sql.= " and us.username like '".$this->request->data['Report']['prefix']."%' ";
			}

			$all_sql .= $sql;
			$count_sql .= $sql;

			$counts = $this->UserAction->query($count_sql);

			$this->set('row_num', $counts[0][0]['total']);

			if($counts[0][0]['total'] > MAX_REPORT_ROW_NUM) {
				$this->layout = "admin";
				$this->Session->write('Report.all_sql',$all_sql);

			}else{

				$reports  = $this->UserAction->query($all_sql);

				$reports = $this->_create_ru_with_prefix_report_array($reports);

				$this->set('reports',$reports);

			}
		}
	}


	/**Create Registered users report
	 *
	 */
	function _create_ru_with_prefix_report_array($reports) {

		$tmp_reports = array();
		foreach ($reports as $key => $report) {
			$tmp_reports[$key]['Export']['client_id'] = $report['ct']['id'];
			$tmp_reports[$key]['Export']['client'] = $report['ct']['name'];
			foreach ($report['us'] as $col => $val) {
				$tmp_reports[$key]['Export'][$col] = $val;
			}
		}
		return $tmp_reports;
	}

	/*
	 * Offer report
	 */

	function admin_offer_report()
	{
		$first_day_of_last_month = date("Y-m-d",mktime(0,0,1,date("m") -1,1,date("Y"))) ;
		$start_year = date("Y",mktime(0,0,1,date("m") -1,1,date("Y"))) ;
		$last_day_of_last_month  = date("Y-m-d",mktime(0,0,1,date("m"),1,date("Y")) - 3600 * 24);
		$end_year  =  date("Y",mktime(0,0,1,date("m"),1,date("Y")) - 3600 * 24);

		$this->set('start_default',$first_day_of_last_month);
		$this->set('end_default',$last_day_of_last_month);
		$this->set('start_year',$start_year);
		$this->set('end_year',$end_year);

		$this->loadModel("Product");
		$conditions = array();

		if (isset($this->params['url']['data'])) {
			$this->Session->write('Report.search', $this->params['url']['data']);
		}

		if ($this->Session->check('Report.search')) {
			$this->request->data = $this->Session->read('Report.search');
			if  (!empty($this->request->data['Report']['search'])) {
				$conditions[] = "
					(`Product`.`name` LIKE  '%". $this->request->data['Report']['search']. "%'
					OR `Product`.`id` LIKE  '%". $this->request->data['Report']['search']. "%')
				";
			}
		}
		$this->Product->recursive = 0;
		$this->set('products', $this->paginate('Product',$conditions));
	}

	function admin_offer_export_report($start,$end,$list)
	{
		$this->layout = "blank";
		ini_set('max_execution_time', 0);
		if(!empty($list)){

			$all_sql = "SELECT DISTINCT products.id, products.name , merchants.name, categories.name, ProductsTracker.clicks, ProductsTracker.prints ";
			$count_sql = "SELECT count(DISTINCT products.id) as total ";

			$sql = " FROM products_trackers as ProductsTracker, user_actions as UserAction, products ";
			$sql .=" LEFT JOIN merchants ON merchants.id = products.merchant_id  ";
			//$sql.=" LEFT JOIN merchant_addresses ON merchant_addresses.merchant_id = products.merchant_id  ";
			$sql.=" LEFT JOIN categories_products ON categories_products.product_id = products.id ";
			$sql.=" LEFT JOIN categories ON categories.id = categories_products.category_id ";
			//$sql.=" LEFT JOIN clients_products  ON clients_products.product_id = products.id ";
			//$sql.=" LEFT JOIN clients ON clients.id = clients_products.client_id ";
			$sql.=" WHERE 1=1 ";

			//	limit to clients
			if(!empty($list)){
				$sql .= " AND products.id IN (". $list. ") ";
			}
			$sql.= " AND ProductsTracker.product_id=products.id AND products.id=UserAction.product_id";
			if(!empty($start)){$sql.= " AND UserAction.created >= '".$start."'";}
			if(!empty($end)){$sql.= " AND UserAction.created <= '".$end."'";}

			$all_sql .= $sql;
			$count_sql .= $sql;

			$counts = $this->UserAction->query($count_sql);

			$this->set('row_num', $counts[0][0]['total']);

			$this->set('name','offer_export');

			if($counts[0][0]['total'] > MAX_REPORT_ROW_NUM){

				$this->layout = "admin";
				$this->Session->write('Report.all_sql',$all_sql);

			}else{
				$reports  = $this->UserAction->query($all_sql);
				$reports = $this->_create_prod_selected_report_array($reports);
				$this->set('reports',$reports);
			}
		}
	}

	/**Create products Report
	 *
	 */
	function _create_prod_selected_report_array($reports) {

		foreach ($reports as $key => $report) {

			foreach($report['products'] as $field => $product){
				$reports[$key]['Export']['Product.'.$field] = $product;
			}
			foreach($report['merchants'] as $field => $product){
				$reports[$key]['Export']['Merchant.'.$field] = $product;
			}
			foreach($report['merchant_addresses'] as $field => $product){
				$reports[$key]['Export']['MerchantAddress.'.$field] = $product;
			}
			foreach($report['categories'] as $field => $product){
					$reports[$key]['Export']['Category.'.$field] = $product;
			}
			foreach($report['ProductsTracker'] as $field => $product){
					$reports[$key]['Export']['ProductsTracker.'.$field] = $product;
			}
		}

		return $reports;
	}

	/*
	 *  Products by clients
	 */

	function admin_client_products()
	{
		$conditions = array();
		if (isset($this->params['url']['data'])) {
			$this->Session->write('Report.search', $this->params['url']['data']);
		}
		if ($this->Session->check('Report.search')) {
			$this->request->data = $this->Session->read('Report.search');

			if  (!empty($this->request->data['Report']['search'])) {
				$conditions[] = "
					(`Client`.`name` LIKE  '%". $this->request->data['Report']['search']. "%'
					OR `Client`.`id` =  '". $this->request->data['Report']['search']. "')
				";
			}
		}

		$conditions[] = " `Client`.`name` !='' ";

		$this->Client->recursive = -1;
		$this->set('clients', $this->paginate('Client', $conditions));


	}

	function admin_client_products_list($list)
	{
		if(!empty($list))
		{
			$sql1 = "where Client.id = ClientsProduct.client_id and Product.id = ClientsProduct.product_id and (Client.id IN (". $list. ") OR ClientsProduct.client_id = 0)";

			$this->paginate = array('ClientProductList' => array('limit' => 10));
			$client_product_list = $this->paginate('ClientProductList', array('search' =>array('sql' => $sql1)));
		}
		$this->autoRender=false;
	}
	function admin_product_search($pname,$clist)
	{
		$this->layout = 'ajax';
		$this->loadModel("Product");
		$this->loadModel('ClientProductList');
		$conditions = array();

		if(!empty($pname))
			$this->Session->write('Report.psearch', $pname);
		if(!empty($clist))
			$this->Session->write('Report.csearch', $clist);

		$sql = "select Product.id from Products as Product where Product.name like '%". $this->Session->read('Report.psearch'). "%'
		or Product.id like '%". $this->Session->read('Report.psearch'). "%'";

		$list = "";
		$query_id = mysql_query($sql);
		while ($row = mysql_fetch_array($query_id))
		{
			$list .= $row['id'].",";
		}
		$plist = substr($list, 0, -1);
		$sql1 = " where ClientsProduct.product_id=Product.id and (ClientsProduct.client_id = Client.id OR ClientsProduct.client_id = 0)
		and Client.id in(".$clist.") and Product.id in (".$plist.") and Merchant.id = Product.merchant_id ";

		$this->paginate = array('ClientProductList' => array('limit' => 25));
		$products_list = $this->paginate('ClientProductList', array('search' =>array('sql' => $sql1)));

		$this->set('products_list',$products_list);
	}

	function admin_product_export_search($list)
	{
		$this->layout = "blank";
		ini_set('max_execution_time', 0);
		$products = "";
		$clients = "0";
		if(!empty($list))
		{
			$lists = explode(',',$list);
			foreach($lists as $lists1)
			{
				if(strlen($clients) > 0)
					$clients = $clients.','.substr($lists1,strrpos($lists1, "|")+1);
				else
					$clients = substr($lists1,strrpos($lists1, "|")+1);

				if(strlen($products) > 0)
					$products = $products.','.substr($lists1,0,strrpos($lists1, "|"));
				else
					$products = substr($lists1,0,strrpos($lists1, "|"));

			}

			$all_sql = "SELECT products.id, products.name , merchants.name, categories.name, ProductsTracker.clicks, ProductsTracker.prints, Client.name ";
			$count_sql = "SELECT count(DISTINCT products.id) as total ";

			$sql = " FROM clients as Client, products_trackers as ProductsTracker, clients_products as ClientsProduct, products  ";
			$sql .=" LEFT JOIN merchants ON merchants.id = products.merchant_id  ";
			$sql.=" LEFT JOIN categories_products ON categories_products.product_id = products.id ";
			$sql.=" LEFT JOIN categories ON categories.id = categories_products.category_id ";
			$sql.=" WHERE 1=1 ";

			//	limit to clients
			//if(!empty($list)){
				$sql .= " AND products.id IN (". $products. ") AND Client.id IN (".$clients.") ";
			//}
			$sql.= " AND ClientsProduct.product_id=products.id and (ClientsProduct.client_id=Client.id OR ClientsProduct.client_id = 0)";
			$sql.= " AND ProductsTracker.product_id=products.id";

			$all_sql .= $sql;
			$count_sql .= $sql;

			$counts = $this->UserAction->query($count_sql);

			$this->set('row_num', $counts[0][0]['total']);

			$this->set('name','client_product_export');

			if($counts[0][0]['total'] > MAX_REPORT_ROW_NUM){

				$this->layout = "admin";
				$this->Session->write('Report.all_sql',$all_sql);

			}else{
				$reports  = $this->UserAction->query($all_sql);
				$reports = $this->_create_client_prod_selected_report_array($reports,$client);
				$this->set('reports',$reports);
			}
		}
	}

	/**Create products Report
	 *
	 */
	function _create_client_prod_selected_report_array($reports) {

		foreach ($reports as $key => $report) {

			foreach($report['products'] as $field => $product){
				$reports[$key]['Export']['Product.'.$field] = $product;
			}
			foreach($report['merchants'] as $field => $product){
				$reports[$key]['Export']['Merchant.'.$field] = $product;
			}
			foreach($report['Client'] as $field => $product){
					$reports[$key]['Export']['Client.'.$field] = $product;
			}
			foreach($report['categories'] as $field => $product){
					$reports[$key]['Export']['Category.'.$field] = $product;
			}
			foreach($report['ProductsTracker'] as $field => $product){
					$reports[$key]['Export']['ProductsTracker.'.$field] = $product;
			}

		}

		return $reports;
	}
	/*
	 *  Client users not logged
	 */
	function admin_clients_users_not_logged() {

	    $this->Client->Program->recursive = -1;
		$programs= $this->Client->Program->find("list");

		$this->Client->recursive = -1;
		$clients= $this->Client->find("list",array('order'=>'Client.name','conditions'=>array('Client.name!=""')));

		//	create the programs_clients array
		$programs_clients = array();
		foreach ($programs as $program_id => $value) {
			$programs_clients[$program_id] = $this->Client->find("list", array('conditions' => "Client.program_id = '". $program_id. "'"));
		}
		$this->set('programs_clients', $programs_clients);

		$this->set(compact('programs','clients'));
	}

	function admin_clients_users_not_logged_export()
	{
    	$this->layout = "blank";
		ini_set('max_execution_time', 0);
		//create reports
	   	$count_sql = "select sum(total) from (SELECT COUNT(a.id) as total ";
	   	$all_sql = "select a.*,c.name";
		$sql = " from users as a ,clients as c where a.client_id=c.id and (a.client_id not in (select ua.client_id from user_actions as ua)
                or a.id not in (select ua.user_id from user_actions as ua)) ";
		//	limit to clients
		if(!empty($this->request->data['Report']['client'])){
			$sql .= " AND a.client_id IN ('". implode("','", $this->request->data['Report']['client']). "') ";
		}
		if(!empty($this->request->data['Report']['Prefix'])){
			$sql .= " AND a.username like ('". $this->request->data['Report']['Prefix']. "%') ";
		}
		$sql.=" AND a.registered=1 ";
		$sql.=" GROUP BY a.id ";
        $all_sql .= $sql;

		$count_sql .= $sql;
		$count_sql .=" ) as t";

		$counts = $this->User->query($count_sql);
		$this->set('row_num', $counts[0][0]['sum(total)']);
		$this->set('name','clients_users_not_logged');

		if($counts[0][0]['sum(total)'] >MAX_REPORT_ROW_NUM){
			$this->layout = "admin";
			$this->Session->write('Report.all_sql',$all_sql);
		}else{
			$reports  = $this->User->query($all_sql);
			$reports = $this->_create_not_logged_report_array($reports);
			$this->set('reports',$reports);
		}

	}

	/**Create user not logged Report
	 *
	 */
	function _create_not_logged_report_array($reports)
	{
		foreach ($reports as $key => $report) {
                 $user_id= $report['a']['id'];
                 $client_id=$report['a']['client_id'];
                 $client_name=$report['c']['name'];
                 $user_name=$report['a']['username'];
                 $registered=$report['a']['registered'];
			   //  $prefix=$this->request->data['Report']['Prefix'];

			$reports[$key]['Export'] = array(
				'User id' 		=>	$user_id,
				'Client_id'		=>	$client_id,
			    'Client_name'   =>  $client_name,
			    'User_name'     =>  $user_name,
			    'Registered'    =>  $registered
			   );
		}
		return $reports;
	}

	/*
	 *  Multiple logins by member
	 */
	function admin_multiple_logins_by_member() {
		$first_day_of_last_month = date("Y-m-d",mktime(0,0,1,date("m") -1,1,date("Y"))) ;
		$start_year = date("Y",mktime(0,0,1,date("m") -1,1,date("Y"))) ;
		$last_day_of_last_month  = date("Y-m-d",mktime(0,0,1,date("m"),1,date("Y")) - 3600 * 24);
		$end_year  =  date("Y",mktime(0,0,1,date("m"),1,date("Y")) - 3600 * 24);

		$this->set('start_default',$first_day_of_last_month);
		$this->set('end_default',$last_day_of_last_month);
		$this->set('start_year',$start_year);
		$this->set('end_year',$end_year);

	    $this->Client->Program->recursive = -1;
		$programs= $this->Client->Program->find("list");

		$this->Client->recursive = -1;
		$clients= $this->Client->find("list",array('order'=>'Client.name','conditions'=>array('Client.name!=""')));

		//	create the programs_clients array
		$programs_clients = array();
		foreach ($programs as $program_id => $value) {
			$programs_clients[$program_id] = $this->Client->find("list", array('conditions' => "Client.program_id = '". $program_id. "'"));
		}
		$this->set('programs_clients', $programs_clients);

		$this->set(compact('programs','clients'));

	}

	function admin_multiple_logins_by_member_export()
	{
		$this->layout = "blank";
		ini_set('max_execution_time', 0);
		//create reports
	   	$count_sql = "SELECT count(DISTINCT ua.user_id) as total ";
	    $all_sql = "select distinct User.username, User.first_name, User.last_name, ua.user_id, ct.name";
		$sql = " FROM user_actions as ua, clients as ct, users AS User ";

		if($this->request->data['Report']['first_time_login'] == '1'){
			$sql .=" ,first_time_logins as fl ";
		}
		$sql.=" WHERE ct.id = ua.client_id and ua.user_id = User.id and User.client_id = ua.client_id ";
		//	limit to clients
		if(!empty($this->request->data['Report']['client'])){
			$sql .= " AND ct.id IN ('". implode("','", $this->request->data['Report']['client']). "') ";
		}
		$sql .="and (ua.url !='/' and ua.url not like '%/users/login%' and ua.url not like '%/users/first_time_login/%'
		and ua.url like '%/') AND
		ua.created >= '". $this->request->data['start_year']['year'].'-'.$this->request->data['Report']['start_date']['month'].'-'.$this->request->data['Report']['start_date']['day']. "'
		AND ua.created <= '". $this->request->data['end_year']['year'].'-'.$this->request->data['Report']['end_date']['month'].'-'.$this->request->data['Report']['end_date']['day']. "'
		AND User.created >= '". $this->request->data['create_from_year']['year'].'-'.$this->request->data['Report']['create_from']['month'].'-'.$this->request->data['Report']['create_from']['day']. "'
		AND User.created <= '". $this->request->data['create_to_year']['year'].'-'.$this->request->data['Report']['create_to']['month'].'-'.$this->request->data['Report']['create_to']['day']. "'";

		if($this->request->data['Report']['first_time_login'] == '1'){
			//$sql .=" and ua.url like '%/users/first_time_login_done/%'";
			$sql .=" and fl.client_id = User.client_id AND fl.user_id=User.id and
			fl.created >= '".  $this->request->data['start_year']['year'].'-'.$this->request->data['Report']['start_date']['month'].'-'.$this->request->data['Report']['start_date']['day']. "'
			AND fl.created <= '". $this->request->data['end_year']['year'].'-'.$this->request->data['Report']['end_date']['month'].'-'.$this->request->data['Report']['end_date']['day']. "'";
		}

		$sql.=" group by ua.user_id HAVING COUNT(ua.url) >1";

		$all_sql .= $sql;
		$count_sql .= $sql;

	    $counts = $this->User->query($count_sql);

		$this->set('row_num',$counts[0][0]['total']);
		$this->set('name','multiple_logins_by_member');

		if($counts[0][0]['total'] >MAX_REPORT_ROW_NUM){
			//
			$this->layout = "admin";
			$this->Session->write('Report.all_sql',$all_sql);

		}else{

			$reports  = $this->User->query($all_sql);

			$reports = $this->_create_multiple_logins_by_member_array($reports);

			$this->set('reports',$reports);

		}

	}

     function _create_multiple_logins_by_member_array($reports) {


		foreach ($reports as $key => $report) {

			$reports[$key]['Export'] = array(
				'User id' 		=>	$report['ua']['user_id'],
			    'User_name'     =>  $report['User']['username'],
			    'First Name'     =>  $report['User']['first_name'],
			    'Last Name'     =>  $report['User']['last_name'],
			    'Client'     =>  $report['ct']['name'] );
		}

		return $reports;
	}


	function admin_actions_by_membership_number() {

	    $this->Client->Program->recursive = -1;
		$programs= $this->Client->Program->find("list");

		$this->Client->recursive = -1;
		$clients= $this->Client->find("list",array('order'=>'Client.name','conditions'=>array('Client.name!=""')));

		//	create the programs_clients array
		$programs_clients = array();
		foreach ($programs as $program_id => $value) {
			$programs_clients[$program_id] = $this->Client->find("list", array('conditions' => "Client.program_id = '". $program_id. "'"));
		}
		$this->set('programs_clients', $programs_clients);


		$this->set(compact('programs','clients'));

	}

	function admin_actions_by_membership_number_export()
	{
	 	$this->layout = "blank";
		ini_set('max_execution_time', 0);
       	if(!empty($this->request->data['Report']['MembershipNumber']))
       	{
	      	//create reports
		   	$count_sql = "SELECT count(DISTINCT upv.id) as total ";
		    $all_sql = "select upv.*, u.registered, count(ua.product_id) as count";
			$sql = " FROM users as u, user_product_views as upv, user_actions as ua ";
			$sql.= " INNER JOIN pages AS Page ON ua.page_id = Page.id ";
			$sql.= " where upv.username = '". $this->request->data['Report']['MembershipNumber']. "' ";
			$sql.= " and u.id=upv.user_id and Page.name = '/products/view' ";
			$sql.=" and upv.product_id= ua.product_id group by upv.id";

	        $all_sql .= $sql;
			$count_sql .= $sql;

		    $counts = $this->UserProductView->query($count_sql);

			$this->set('row_num', $counts[0][0]['sum(total)']);
			$this->set('name','actions_by_membership_number');

			if($counts[0][0]['sum(total)'] >MAX_REPORT_ROW_NUM)
			{
				$this->layout = "admin";
				$this->Session->write('Report.all_sql',$all_sql);
			}
			else
			{
				$reports  = $this->User->query($all_sql);
				$reports = $this->_create_actions_by_membership_number_array($reports);
				$this->set('reports',$reports);
			}
      	}
	}

 	function _create_actions_by_membership_number_array($reports)
 	{
		foreach ($reports as $key => $report) {
                 $user_id= $report['upv']['user_id'];
                 $user_name=$report['upv']['username'];
                 $product_id= $report['upv']['product_id'];
                 $clicks= $report['upv']['clicks'];
                 $prints= $report['upv']['prints'];
                 $views = $report[0]['count'];
                 $registered = $report['u']['registered'];
                 //$last_login= $report[0]['last_login'];
                 //$first_login= $report[0]['first_login'];

		$reports[$key]['Export'] = array(
			'User id' 		=>	$user_id,
		    'User_name'     =>  $user_name,
		    'Product_id'    =>  $product_id,
			'Views' 		=>	$views,
		    'Clicks'        =>  $clicks,
		    'Prints'        =>  $prints,
			'Registered'    =>  $registered);
		}

		return $reports;
	}

	function admin_login_info_by_membership_number()
	{
	    $this->Client->recursive = -1;
		$clients= $this->Client->find("list");
		$clientslist = array();
		$this->set('clientslist',$clientslist);
		$this->set(compact('clients'));
	}


	function admin_user_search($uname, $cname)
	{
		$this->layout = 'ajax';
		$this->loadModel("User");

		$conditions = array();
		$i = 0;
		$this->set('i',$i);
		$j = 1;
		$this->set('j',$j);
		$sql1 = " where (user.username like '%". $uname. "%' or user.id like '%". $uname. "%') and user.client_id=".$cname;

		$this->paginate = array('ClientUserList' => array('limit' => 25));
		$users_list = $this->paginate('ClientUserList', array('search' =>array('sql' => $sql1)));

		$this->set('users_list',$users_list);

	}

	function admin_user_export_search($list)
	{
		$this->layout = "blank";
		ini_set('max_execution_time', 0);
		$user_id = "";
		$clients = "";

		if(!empty($list))
		{
			$lists = explode(',',$list);
			foreach($lists as $lists1)
			{
				if(strlen($clients) > 0) {
					$clients = $clients.','.substr($lists1,strrpos($lists1, "|")+1);

				}	else
					$clients = substr($lists1,strrpos($lists1, "|")+1);

				if(strlen($user_id) > 0) {
					$user_id = $user_id.','.substr($lists1,0,strrpos($lists1, "|"));

				}	else
					$user_id = substr($lists1,0,strrpos($lists1, "|"));

			}

        $count_sql = "select sum(total) from (SELECT count(DISTINCT ua.user_id) as total ";
	    $all_sql = "select ua.user_id,ua.client_id,user.username,min(ua.created) as first_login,max(ua.created) as last_login ";
		$sql = " FROM user_actions as ua ,users as user where ua.user_id in (". $user_id. ") and ua.client_id in (". $clients. ")
		         and ua.user_id=user.id group by ua.user_id ";

                    $all_sql .= $sql;
					$count_sql .= $sql;
					 $count_sql .=" ) as t";

			$counts = $this->UserAction->query($count_sql);

			$this->set('row_num',  $counts[0][0]['sum(total)']);

			$this->set('name','client_user_export');

			if( $counts[0][0]['sum(total)'] > MAX_REPORT_ROW_NUM){

				$this->layout = "admin";
				$this->Session->write('Report.all_sql',$all_sql);

			}else{
				$reports  = $this->UserAction->query($all_sql);
				$reports = $this->_create_client_user_selected_report_array($reports);
				$this->set('reports',$reports);
			}
		}
	}

function _create_client_user_selected_report_array($reports) {

		foreach ($reports as $key => $report) {

		         $client_id=$report['ua']['client_id'];
                 $user_id= $report['ua']['user_id'];
                 $user_name=$report['user']['username'];
                 $last_login= $report[0]['last_login'];
                 $first_login= $report[0]['first_login'];
			$reports[$key]['Export'] = array(
				'User id' 		=>	$user_id,
			    'User_name'     =>  $user_name,
			    'Client_id'     =>  $client_id,
			    'Last_Login'    =>  $last_login,
			    'First_Login'   =>  $first_login);
		}

		return $reports;


	}

function admin_user_graph() {
		$first_day_of_last_month = date("Y-m-d",mktime(0,0,1,date("m") -1,1,date("Y"))) ;
		$start_year = date("Y",mktime(0,0,1,date("m") -1,1,date("Y"))) ;
		$last_day_of_last_month  = date("Y-m-d",mktime(0,0,1,date("m"),1,date("Y")) - 3600 * 24);
		$end_year  =  date("Y",mktime(0,0,1,date("m"),1,date("Y")) - 3600 * 24);

		$this->set('start_default',$first_day_of_last_month);
		$this->set('end_default',$last_day_of_last_month);
		$this->set('start_year',$start_year);
		$this->set('end_year',$end_year);

		 $this->Client->recursive = -1;
		$clients= $this->Client->find("list");
		$clientslist = array();
		$this->set('clientslist',$clientslist);
		$this->set(compact('clients'));
	}

	function admin_user_graph_report() {
	$start_date = $this->request->data['start_year']['year'].'-'.$this->request->data['Report']['start_date']['month'].'-'.$this->request->data['Report']['start_date']['day'];
	$end_date = $this->request->data['end_year']['year'].'-'.$this->request->data['Report']['end_date']['month'].'-'.$this->request->data['Report']['end_date']['day'];

  $sql1="(select 1 as num,client.name, count(user.client_id) as month from users as user, clients as client
  	where user.client_id in('". implode("','", $this->request->data['Report']['client_id']). "')
  	and registered = '1' and user.client_id=client.id and
  	user.created >= '{$start_date}' AND DATE(user.created) <='{$end_date}' group by user.client_id)";

    $sql= "(select 2 as num,client.name, count(user.client_id) as tot from users as user, clients as client
         where user.client_id in('". implode("','", $this->request->data['Report']['client_id']). "') and
         registered = '1' and user.client_id=client.id group by user.client_id)";

    $sql3 =  $sql1.' union '. $sql .' order by name, num desc';

   	  	$cc = $this->User->query($sql3);

     	$clients=array();$totUsers=array();$monUsers=array();$checkmon='';

foreach($cc as $tCnt)
{
	$ttCnt = $tCnt[0];
	if($cName != $ttCnt['name'])
	{
		if(count($monUsers)<count($totUsers)){array_push($monUsers,0);}
		array_push($totUsers,$ttCnt['month']);
		array_push($clients,$ttCnt['name']);
	}
	else if($cName == $ttCnt['name'])
	{
		array_push($monUsers,$ttCnt['month']);
	}
	$cName = $ttCnt['name'];
}
if(count($monUsers)<count($totUsers)){array_push($monUsers,0);}

	 // content="text/plain; charset=utf-8"
		require_once ('jpgraph/src/jpgraph.php');
		require_once ('jpgraph/src/jpgraph_bar.php');

		$data1y=$totUsers;
		$data2y=$monUsers;
        $count_clients=count($clients);

		// Create the graph. These two calls are always required
		$graph = new Graph(1200,600,'auto');

		$graph->SetScale("textlin");
        $theme_class=new UniversalTheme;
		$graph->SetTheme($theme_class);

		$graph->SetBox(false);

		$graph->ygrid->SetFill(false);
		$graph->xaxis->SetTickLabels($clients);
		$graph->yaxis->HideLine(false);
		$graph->yaxis->HideTicks(false,false);

		// Create the bar plots
		$b1plot = new BarPlot($data2y);
		$b2plot = new BarPlot($data1y);
		$b1plot->SetAbsWidth(40);
		$b2plot->SetAbsWidth(40);
		//$b1plot->SetFillGradient("navy","lightsteelblue",GRAD_CENTER);
		$b1plot->SetColor("#4f81bd");
        $b1plot->SetLegend("Total Users Registered Between ".$start_date." And ".$end_date);
		//$b2plot->SetFillGradient("#cc1111","#eda6a6",GRAD_CENTER);
		$b2plot->SetColor("#c1504c");
        $b2plot->SetLegend("Total Registered Users");

		// Create the grouped bar plot
		$gbplot = new GroupBarPlot(array($b1plot,$b2plot));
		$gbplot->SetWidth($count_clients*0.075);
		// ...and add it to the graPH
		$graph->Add($gbplot);


		$b1plot->SetColor("white");
		$b1plot->SetFillColor("#4f81bd");
		$b1plot->value->Show();
		$b1plot->value->SetFormat('%d');
		$b1plot->SetShadow("#3c628f",1,1);

		$b2plot->SetColor("white");
		$b2plot->SetFillColor("#c1504c");

		$b2plot->value->Show();
		$b2plot->value->SetFormat('%d');
		$b2plot->SetShadow("#903e3c",1,1);
		$graph->title->Set("Registered Users Per Client");

		header('Content-Description: File Transfer');
		header('Content-Type: application/octet-image');
		header("Content-disposition: attachment; filename=UsersGraphPerClient.jpg");
		// Display the graph
		$graph->Stroke();

		$this->autoRender = false;
	}


	/*
	 * Total product views by client
	 *
	 */
		function admin_product_views_clients() {

			$first_day_of_last_month = date("Y-m-d",mktime(0,0,1,date("m") -1,1,date("Y"))) ;
			$start_year = date("Y",mktime(0,0,1,date("m") -1,1,date("Y"))) ;
			$last_day_of_last_month  = date("Y-m-d",mktime(0,0,1,date("m"),1,date("Y")) - 3600 * 24);
			$end_year  =  date("Y",mktime(0,0,1,date("m"),1,date("Y")) - 3600 * 24);

			$this->set('start_default',$first_day_of_last_month);
			$this->set('end_default',$last_day_of_last_month);
			$this->set('start_year',$start_year);
			$this->set('end_year',$end_year);


			$this->Client->recursive = -1;
			$clients= $this->Client->find("list",array('order'=>'Client.name','conditions'=>array('Client.name!=""')));

			$this->set(compact('clients'));
		}


	/*
	 * Total product views by clients export
	 */
	function admin_product_views_clients_export()
	{
		$this->layout = "blank";
		ini_set('max_execution_time', 0);
		$sql1 = "SELECT * from users as User, user_product_views as upv where User.registered = 1 and User.id=upv.user_id and User.client_id=".$this->request->data['Report']['client']." group by User.id";

		$users = $this->User->query($sql1);

		$list = array();
		foreach($users as $user)
		{
			array_push($list,$user['User']['id']);
		}

		$count_sql = "SELECT count(*) as total ";

		$all_sql = "select UserProductView.product_id, Product.name, UserProductView.clicks, UserProductView.prints,
		count(UserAction.product_id) as views, Category.name as catname ";

		$sql = " FROM pages AS Page, user_actions as UserAction, products as Product,user_product_views as UserProductView
		LEFT JOIN categories_products AS CategoriesProduct ON UserProductView.product_id = CategoriesProduct.product_id
		LEFT JOIN categories AS Category ON CategoriesProduct.category_id = Category.id
		WHERE UserAction.page_id = Page.id and UserProductView.user_id in (". implode(",", $list). ")
		and UserProductView.product_id=UserAction.product_id
		and UserProductView.user_id=UserAction.user_id and Page.name = '/products/view'
		AND UserAction.created >= '". $this->request->data['start_year']['year'].'-'.$this->request->data['Report']['start_date']['month'].'-'.$this->request->data['Report']['start_date']['day']. "'
		AND UserAction.created <= '". $this->request->data['end_year']['year'].'-'.$this->request->data['Report']['end_date']['month'].'-'.$this->request->data['Report']['end_date']['day']. "'
		AND Product.id= UserProductView.product_id group by UserProductView.product_id, Category.name order by UserProductView.product_id ";

		$all_sql .= $sql;
		$count_sql .= $sql;

		$counts = $this->UserProductView->query($count_sql);

		$this->set('row_num', $counts[0][0]['total']);
		$this->set('name','product_views_clients');
		$this->Client->recursive = -1;
		$client = $this->Client->findById($this->request->data['Report']['client']);

		if($counts[0][0]['total'] >MAX_REPORT_ROW_NUM)
		{
			$this->layout = "admin";
			$this->Session->write('Report.all_sql',$all_sql);
			$this->Session->write('Report.client_name',$client['Client']['name']);
		}
		else
		{
			$reports  = $this->User->query($all_sql);
			//ini_set( 'memory_limit', '-1' );
			$reports = $this->_create_actions_by_product_views_clients_array($reports, $client['Client']['name']);
			$this->set('reports',$reports);

		}
	}

	function _create_actions_by_product_views_clients_array($reports, $client_name)
	{
		$tKey = $prdtId = $prdtNm = $cat = '';
		$clcks = $prnts = $vws = 0;
		$val = 0;$check = false;
		foreach ($reports as $key => $report)
		{
			if($tKey == $report['UserProductView']['product_id'])
			{
				$vws = $vws + $report['0']['views'];
				$cat = $cat.", ".$report['Category']['catname'];
			}

			if($tKey != $report['UserProductView']['product_id'])
			{
				if($prdtId != '' || !empty($prdtId))
				{
					$reports[$val]['Export'] = array(
					'Product_id'    =>  $prdtId,
					'Product_name'  => 	$prdtNm,
					'Clicks'        =>  $clcks,
					'Prints'        =>  $prnts,
					'Views' 		=>	$vws,
					'Client'        =>  $client_name,
					'Category'      =>  $cat);
					$val++;
				}

				$prdtId= $report['UserProductView']['product_id'];
				$prdtNm = $report['Product']['name'];
				$clcks= $report['UserProductView']['clicks'];
				$prnts= $report['UserProductView']['prints'];
				$vws = $report['0']['views'];
				$cat = $report['Category']['catname'];
			}
			$tKey = $report['UserProductView']['product_id'];
		}

		return $reports;
	}

	/*
		 * Export registered users reports
		*
		*/
		function admin_export_first_time_login_members($registered='all') {
			$this->Client->Program->recursive = -1;
			$programs= $this->Client->Program->find("list");

			$this->Client->recursive = -1;
			$clients= $this->Client->find("list",array('order'=>'Client.name','conditions'=>array('Client.name!=""')));

			//	create the programs_clients array
			$programs_clients = array();
			foreach ($programs as $program_id => $value) {
				$programs_clients[$program_id] = $this->Client->find("list", array('conditions' => "Client.program_id = '". $program_id. "'"));
			}

			$countries = $this->Country->find('list', array('fields' => array('Country.name','Country.name')));

			$states = array();

			$start_default = date("Y-m-d",mktime(0,0,1,date("m") -1,1,date("Y"))) ;
			$start_year = date("Y",mktime(0,0,1,date("m") -1,1,date("Y"))) ;
			$end_default  = date("Y-m-d",mktime(0,0,1,date("m"),1,date("Y")) - 3600 * 24);
			$end_year  =  date("Y",mktime(0,0,1,date("m"),1,date("Y")) - 3600 * 24);
			$this->set('programs_clients', $programs_clients);

			$this->set(compact('programs','clients','countries','states','start_default','end_default','start_year','end_year'));
			$this->set('registered', $registered);
		}

		/*
		 * Export Registered users report
		*
		*/
		function admin_users_first_time_login_export($registered = 'all'){

			$this->layout = "blank";
			ini_set('max_execution_time', 0);
			//create reports
			if(!empty($this->request->data)){
				$all_sql = "SELECT DISTINCT ct.id, ct.name, us.* ";
				$count_sql = "SELECT count(DISTINCT us.id) as total ";

				$sql ="FROM users as us, first_time_logins as fl, clients as ct  ";

				//	limit to clients
				if(empty($this->request->data['Report']['client'])){
					$this->request->data['Report']['client'] = array();
				}
				$sql .= " WHERE fl.client_id = ct.id and fl.client_id IN ('". implode("','", $this->request->data['Report']['client']). "')  AND us.id = fl.user_id
				AND fl.created >= '". $this->request->data['start_year']['year'].'-'.$this->request->data['Report']['start_date']['month'].'-'.$this->request->data['Report']['start_date']['day']. "'
				AND fl.created <= '". $this->request->data['end_year']['year'].'-'.$this->request->data['Report']['end_date']['month'].'-'.$this->request->data['Report']['end_date']['day']. "'";

				$this->set('name','first_time_login_users');

				if(!empty($this->request->data['Report']['country'])){
					$sql.= " and us.country = '".$this->request->data['Report']['country']."' ";
				}
				if($this->request->data['Report']['country'] !='Hong Kong')
				{
					if(!empty($this->request->data['Report']['state'])){
						$sql.= " and us.state = '".$this->request->data['Report']['state']."' ";
					}
				}

				$all_sql .= $sql;

				$count_sql .= $sql;

				$counts = $this->UserAction->query($count_sql);

				$this->set('row_num', $counts[0][0]['total']);

				if($counts[0][0]['total'] > MAX_REPORT_ROW_NUM) {
					$this->layout = "admin";
					$this->Session->write('Report.all_sql',$all_sql);

				}else{

					$reports  = $this->UserAction->query($all_sql);

					$reports = $this->_create_ru_report_array($reports);

					$this->set('reports',$reports);

				}
			}
	}

	function admin_cart_products()
	{
		$first_day_of_last_month = date("Y-m-d",mktime(0,0,1,date("m") -1,1,date("Y"))) ;
		$start_year = date("Y",mktime(0,0,1,date("m") -1,1,date("Y"))) ;
		$last_day_of_last_month  = date("Y-m-d",mktime(0,0,1,date("m"),1,date("Y")) - 3600 * 24);
		$end_year  =  date("Y",mktime(0,0,1,date("m"),1,date("Y")) - 3600 * 24);

		$this->set('start_default',$first_day_of_last_month);
		$this->set('end_default',$last_day_of_last_month);
		$this->set('start_year',$start_year);
		$this->set('end_year',$end_year);
	}

	function admin_cart_products_export()
	{
		$this->layout = "blank";
		ini_set('max_execution_time', 0);
		if(!empty($this->request->data)){

			$all_sql = "SELECT Client.name as 'Site Name',
    CartItem.order_date as 'Order Date',
    CartItem.order_id as 'Order Number',
    '' as 'Supplier Product Code',
    Product.name as 'Product Name',
	CartItem.product_qty as 'Quantity',
    CartAddress.member_name as Recipient,
    '' as 'Business Name',
    CartAddress.address1 as 'Address1',
    CartAddress.address2 as 'Address2',
    CartAddress.city as Suburb,
    CartAddress.state as State,
    CartAddress.zipcode as Postcode,
    CartAddress.country as Country,
    CartAddress.mobile as 'Contact Number',
    CartProduct.product_exgst_cost as 'Cost Ex GST',
    CartProduct.product_incgst_cost as 'Cost Inc GST',
    Product.id as 'My Rewards Product Code',
    Product.id as 'My Rewards Unique ID',
    CartProduct.product_shipping_cost as 'Supplier Freight Ex GST'";
			/*
			CartItem.product_id ProductId, CartItem.cart_id CartId, CartItem.user_id UserId,
			CartItem.product_qty Quantity, CartItem.cart_status Status, CartItem.product_type Type, CartAddress.username UserName, CartAddress.member_name MemberName,
			CartAddress.email Email, ";

			if($this->request->data['Report']['type']=='cart')
			{
				$all_sql.= " , ShippingCategory.name ShippingType, CartProduct.product_price ProductPrice, CartItem.ship_cost ShippingCost, CartItem.total TotalAmount ";
			}

			if($this->request->data['Report']['shipped']==1)
			{
				$all_sql.= " ,CartItem.shipping_date ShippingDate, CartItem.courier_name ShipmentName, CartItem.courier_number ConsignmentNumber,
				CartItem.tracking_link TrackingUrl";
			}
			if($this->request->data['Report']['completed']==1)
			{
				$all_sql.=" ,CartItem.completed_date CompletedDate ";
			}*/

			$count_sql = "SELECT count(CartItem.id) as total ";

			$sql =" FROM cart_items as CartItem, carts as Cart, cart_products as CartProduct, cart_addresses as CartAddress, products as Product,
			users as User, clients as Client ";
			if($this->request->data['Report']['type']=='cart')
			{
				$sql.=", shipping_categories as ShippingCategory ";
			}

			$sql.=" WHERE Cart.id=CartItem.cart_id and CartItem.product_id=CartProduct.product_id and CartItem.product_type=CartProduct.product_type
			AND Cart.address_id = CartAddress.id AND CartProduct.product_id = Product.id AND Cart.user_id=User.id AND User.client_id=Client.id ";

			if($this->request->data['Report']['type']=='cart')
			{
				$sql.=" AND ShippingCategory.id=CartProduct.product_shipping_type ";
			}

			if($this->request->data['Report']['type']=='cart')
			{
				$sql.= " AND CartItem.product_type= 'Pay' ";
			}
			elseif($this->request->data['Report']['type']=='points')
			{
				$sql.= " AND CartItem.product_type= 'Points' ";
			}

			if($this->request->data['Report']['pending']==1 && $this->request->data['Report']['shipped']==0 && $this->request->data['Report']['completed']==0)
			{
				$sql.= " AND (CartItem.cart_status= 'Pending' OR CartItem.cart_status= 'Paid')";
			}
			if($this->request->data['Report']['pending']==0 && $this->request->data['Report']['shipped']==1 && $this->request->data['Report']['completed']==0)
			{
				$sql.= " AND CartItem.cart_status= 'Shipped' ";
			}
			if($this->request->data['Report']['pending']==0 && $this->request->data['Report']['shipped']==0 && $this->request->data['Report']['completed']==1)
			{
				$sql.= " AND CartItem.cart_status= 'Completed' ";
			}
			if($this->request->data['Report']['pending']==1 && $this->request->data['Report']['shipped']==1 && $this->request->data['Report']['completed']==1)
			{
				$sql.= " AND (CartItem.cart_status= 'Pending' OR CartItem.cart_status= 'Paid' OR CartItem.cart_status= 'Shipped' OR CartItem.cart_status= 'Completed') ";
			}

			//created from
			if(isset($this->request->data['created_from_year'])){
				$created_from = $this->request->data['created_from_year']['year'].'-'.$this->request->data['Report']['created_from']['month'].'-'.$this->request->data['Report']['created_from']['day'];
				$sql.= " AND CartItem.order_date >= '$created_from' ";

			}

			//created to
			if(isset($this->request->data['created_to_year'])){
				$created_to = $this->request->data['created_to_year']['year'].'-'.$this->request->data['Report']['created_to']['month'].'-'.$this->request->data['Report']['created_to']['day'];
				$sql.= " AND CartItem.order_date <= '$created_to' ";
			}

			$sql.= " ORDER BY CartItem.order_id desc ";
			if($this->request->data['Report']['type']=='cart')
			{
				$sql.= " , CartProduct.product_shipping_type, CartProduct.product_shipping_cost desc ";
			}

			$all_sql .= $sql;

			$count_sql .= $sql;
			CakeLog::debug("Repo-$all_sql");


			$counts = $this->Cart->query($count_sql);

			$this->set('row_num', $counts[0][0]['total']);

			if($this->request->data['Report']['type']=='cart')
				$this->set('name','shopping_cart_products');
			elseif($this->request->data['Report']['type']=='points')
				$this->set('name','points_products');

			if($counts[0][0]['total'] > MAX_REPORT_ROW_NUM){
				//
				$this->layout = "admin";
				$this->Session->write('Report.all_sql',$all_sql);

			}else{

				$reports  = $this->Cart->query($all_sql);

				$reports = $this->_create_cart_products_report_array($reports);

				$this->set('reports',$reports);

			}

		}

	}

	/**Create Cart Product Report
	 *
	*/
	function _create_cart_products_report_array($reports) {
		foreach ($reports as $key => $report) {

			foreach($report['Product'] as $field => $value){
				$reports[$key]['Export'][$field] = $value;
			}
			foreach($report['CartItem'] as $field => $value){
				$reports[$key]['Export'][$field] = $value;
			}

			foreach($report['CartProduct'] as $field => $value){
				$reports[$key]['Export'][$field] = $value;
			}
			foreach($report['ShippingCategory'] as $field => $value){
				$reports[$key]['Export'][$field] = $value;
			}

			foreach($report['CartAddress'] as $field => $value){
				$reports[$key]['Export'][$field] = $value;
			}
		}
		return $reports;
	}

	//Report for advt4free merchants
	function admin_advt4free_merchant()
	{

	}

	function admin_advt4free_merchant_export()
	{
		$this->layout = "blank";
		ini_set('max_execution_time', 0);
		if(!empty($this->request->data)){

			$all_sql = "SELECT * FROM advt4_frees as Advt4Free WHERE (1=1) ";
			$count_sql = "SELECT count(Advt4Free.id) as total FROM advt4_frees as Advt4Free WHERE (1=1) ";

			$sql = "";
			if($this->request->data['Report']['merchant_approve']=='yes' && $this->request->data['Report']['admin_approve']==0 && $this->request->data['Report']['admin_not_approve']==0)
			{
				$sql.= " AND Advt4Free.merchant_approved='YES' ";
				$this->set('name','advt4free_merchant_approved');
			}
			if($this->request->data['Report']['merchant_approve']=='yes' && ($this->request->data['Report']['admin_approve']==1 && $this->request->data['Report']['admin_not_approve']==0))
			{
				$sql.= " AND Advt4Free.merchant_approved='YES' AND Advt4Free.approved='1' ";
				$this->set('name','advt4free_merchant_admin_approved');
			}
			if($this->request->data['Report']['merchant_approve']=='yes' && ($this->request->data['Report']['admin_approve']==0 && $this->request->data['Report']['admin_not_approve']==1))
			{
				$sql.= " AND Advt4Free.merchant_approved='YES' AND Advt4Free.approved='0' ";
				$this->set('name','advt4free_merchant_app_admin_notapp');
			}
			if($this->request->data['Report']['merchant_approve']=='yes' && $this->request->data['Report']['admin_approve']==1 && $this->request->data['Report']['admin_not_approve']==1)
			{
				$sql.= " AND Advt4Free.merchant_approved='YES' AND (Advt4Free.approved='1' OR Advt4Free.approved='0') ";
				$this->set('name','advt4free_merchant_app_admin_app_and_napp');
			}

			if($this->request->data['Report']['merchant_approve']=='no')
			{
				$sql.= " AND Advt4Free.merchant_approved='NO' ";
				$this->set('name','advt4free_merchant_not_approved');
			}

			$all_sql .= $sql;
			$count_sql .= $sql;
			$counts = $this->Advt4Free->query($count_sql);

			$this->set('row_num', $counts[0][0]['total']);

			//$this->set('name','advt4_frees');

			if($counts[0][0]['total'] > MAX_REPORT_ROW_NUM){
				//
				$this->layout = "admin";
				$this->Session->write('Report.all_sql',$all_sql);

			}else{

				$reports  = $this->Advt4Free->query($all_sql);

				$reports = $this->_create_advt4_frees_report_array($reports);

				$this->set('reports',$reports);

			}

		}

	}

	/**Create Advt4Free Merchant Report
	 *
	*/
	function _create_advt4_frees_report_array($reports)
	{
		foreach ($reports as $key => $report) {
		foreach($report['Advt4Free'] as $field => $product){
				$reports[$key]['Export']['Advt4Free.'.$field] = $product;
			}
		}
		return $reports;
	}
}
?>