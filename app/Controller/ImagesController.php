<?php

/**

 * Serve images up to users

*/
class ImagesController extends AppController{

	var $uses = array('Product');

	var $layout = 'file';
	var $autoRender = false;

	/**
	 * Displays a product image
	 *
	 * Keep trying to find a suitable logo from product, merchant, client and program
	 *
	 * @param str $filename Product image file name
	 * @param str $logo if set to no_logo, will not try to display the merchant logo
	 * @access public
	 */
	function product($filename, $logo = '') {

		Configure::write('debug', 1);	//	do not show sql debug stuff here, this is an image.

		$this->_cacheHeaders();

		$id = substr($filename, 0, strrpos($filename, '.'));
		$this->Product->recursive = 0;
		$product = $this->Product->read(null, $id);

		//	select the correct image, depending on what is set in the admin area
		if ($product['Product']['display_image'] == 'Product Image') {
			$filepath = PRODUCT_IMAGE_PATH;
		} else {
			$filepath = MERCHANT_LOGO_PATH;
			$filename = $product['Merchant']['id']. '.'. $product['Merchant']['logo_extension'];
		}

		if (!file_exists($filepath. $filename)) {

			//	try for a merchant image
			$filepath = MERCHANT_IMAGE_PATH;
			$filename = $product['Merchant']['id']. '.'. $product['Merchant']['image_extension'];
			if (!file_exists($filepath. $filename)) {
				//	try for a merchant logo
				$filepath = MERCHANT_LOGO_PATH;
				if ($logo != 'no_logo') {	//	if logo is not meant to appear, set a dummy name so it will not be found
					$filename = 'no_file';
				} else {

					$filename = $product['Merchant']['id']. '.'. $product['Merchant']['logo_extension'];
				}
				if (!file_exists($filepath. $filename)) {
					//	try for a client image
					$filepath = FILES_PATH. 'default_client_product_image/';
					$filename = $this->Session->read('client.Client.id'). '.'. $this->Session->read('client.Client.default_client_product_image_extension');
					if (!file_exists($filepath. $filename)) {
						//	try for a program image
						$filepath = FILES_PATH. 'default_product_image/';
						$filename = $this->Session->read('client.Program.id'). '.'. $this->Session->read('client.Program.default_product_image_extension');
						if (!file_exists($filepath. $filename)) {
							//	use the default no image image
							$filepath = WWW_ROOT. 'img/';
							$filename = 'no_image.gif';
						}
					}
				}
			}
		}

		$this->set('filename', $filename);
		$this->set('filepath', $filepath);

		$this->render('image');

	}

	/**
	 * Displays a style image for the style of the current client
	 *
	 * @param str $image image to display
	 * @access public
	 */
	function style($image) {

		Configure::write('debug', 1);	//	do not show sql debug stuff here, this is an image.

		$this->_cacheHeaders();

		$filepath = STYLE_PATH. $image. '/';

		if ($this->Session->check('client.Style.'. $image. '_extension')) {
			$extension = $this->Session->read('client.Style.'. $image. '_extension');
		} else {
			$extension = 'gif';
		}

		$filename =  $this->Session->read('client.Style.id'). '.'. $extension;

		$this->set('filename', $filename);
		$this->set('filepath', $filepath);

		$this->render('image');

	}

	/**
	 * Display a banner for a client
	 *
	 * @param	int	$id	client id
	*/
	function banner($id) {

		Configure::write('debug', 1);	//	do not show sql debug stuff here, this is an image.

		$this->_cacheHeaders();

		$filepath = BANNER_PATH;

		if ($this->Session->check('client.Client.banner_extension')) {
			$filename = $id. '.'. $this->Session->read('client.Client.banner_extension');
		} else {
			$filename = '';
		}

		$this->set('filename', $filename);
		$this->set('filepath', $filepath);

		$this->render('image');
	}

	/**
	* Display an image from the files folder
	*
	* @param	str	$filepath folder name containing the images
	* @param	str	$filename	name of the image
	*/
	function image($filepath, $filename) {

		Configure::write('debug', 1);	//	do not show sql debug stuff here, this is an image.

		$this->_cacheHeaders();

		//	if the image does not exist, display a 1px transparant image
		if (!file_exists(FILES_PATH. $filepath. '/'. $filename)) {
			$filename = 'transparent.gif';
			$filepath = '../img';
		}

		$this->set('filename', $filename);
		$this->set('filepath', FILES_PATH.$filepath. '/');

		$this->render('image');

	}

	/**
	 * create headers to cache the image
	*/
	function _cacheHeaders() {

		$expires = 60 * 60 * 24 * 3;
		$exp_gmt = gmdate("D, d M Y H:i:s", time() + $expires )." GMT";
		$mod_gmt = gmdate("D, d M Y H:i:s", time() + (3600 * -5 * 24 * 365) )." GMT";

		@header("Expires: $exp_gmt");
		@header("Last-Modified: $mod_gmt");
		@header("Cache-Control: public, max-age=$expires");
		@header("Pragma: !invalid");
		@header("Content-Length: $size");
		@header("ETag: " . md5( ob_get_contents() ) );

	}

}
?>