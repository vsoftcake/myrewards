<?php
class SkyscrapersController extends AppController {

	var $components = array('File');

	function admin_index() {

		$conditions = array();
		if (isset($this->params['url']['data'])) {
			$this->Session->write($this->name.'.search', $this->params['url']['data']);
		}

		if ($this->Session->check($this->name.'.search')) {
			$this->request->data = $this->Session->read($this->name.'.search');
			if  (!empty($this->request->data['Skyscraper']['search'])) {
				$conditions[] = "
					(`Skyscraper`.`name` LIKE  '%". $this->request->data['Skyscraper']['search']. "%'
					OR `Skyscraper`.`id` LIKE  '%". $this->request->data['Skyscraper']['search']. "%')
				";
			}
		}
		
		if($this->Session->read('user.User.type')=='Client Admin')
		{
			$this->loadModel('SkyscraperAdmin');
			$sql1 = " Where ClientsSkyscraper.client_id = Client.id and ClientsSkyscraper.skyscraper_id = Skyscraper.id
			and ClientsSkyscraper.client_id = ".$this->Session->read('user.User.client_id')." group by Skyscraper.id";
		
			$this->set('skyscrapers', $this->paginate('SkyscraperAdmin',array('search' =>array('sql' => $sql1))));
		}
		elseif($this->Session->read('user.User.type')=='Country Admin')
		{
			$this->loadModel('SkyscraperAdmin');
			$sql1 = " Where ClientsSkyscraper.client_id = Client.id and ClientsSkyscraper.skyscraper_id = Skyscraper.id
			and Client.country = '".$this->Session->read('client.Client.country')."' group by Skyscraper.id";
		
			$this->set('skyscrapers', $this->paginate('SkyscraperAdmin',array('search' =>array('sql' => $sql1))));
		}
		else
		{
			$this->Skyscraper->recursive = 0;
			$this->set('skyscrapers', $this->paginate('Skyscraper',$conditions));
		}
	}

	function admin_edit($id=null)
	{
		if (!empty($this->request->data)) {
				//	get the image extensions

			if ($this->request->data['Image']['file']['name'] != '') {
				$extension = substr($this->request->data['Image']['file']['name'], strrpos($this->request->data['Image']['file']['name'], '.') + 1);
				$this->request->data['Skyscraper']['scraper_extension'] = $extension;
			}
			if ($this->Skyscraper->save($this->request->data)) {

				// delete unwanted images of merchant logo
				if (isset($this->request->data['ImageDelete'])) {
					$delete_images = array();
					foreach ($this->request->data['ImageDelete'] as $key => $value) {
						if ($value == '1') {
							$this->File->deleteByPrefix('skyscraper_image/'.$this->Skyscraper->id);
							$this->request->data['Skyscraper']['scraper_extension']='';
							$this->Skyscraper->save($this->request->data);
						}
					}
				}
				if (isset($this->request->data['Image'])) {
					//	save the uploaded files
					if ($this->request->data['Image']['file']['name']!= '') {
						move_uploaded_file($this->request->data['Image']['file']['tmp_name'], SKYSCRAPER_IMAGE_PATH. $this->Skyscraper->id. '.'. $this->request->data['Skyscraper']['scraper_extension']);
					}
				}

				$this->Session->setFlash('The Skyscraper has been saved');
				$this->redirect_session(array('action'=>'index'), null, true);
			} else {
				$this->Session->setFlash('Please correct the errors below.');
			}
		} else {
			$this->request->data = $this->Skyscraper->read(null, $id);
			$this->_setRedirect();
		}

		if($this->Session->read('user.User.type') == 'Client Admin' && empty($id))
		{
			$default = $this->Skyscraper->Client->find('first',array('conditions' =>array('Client.id' =>$this->Session->read('user.User.client_id')),'fields'=>array('Client.program_id'),'recursive'=>-1));
		
			$this->loadModel("Program");
			$program_list = $this->Program->find('list',array('conditions'=>array('id'=>$default['Client']['program_id'])));
			$this->set('programlist',$program_list);
		
			$clients = $this->Skyscraper->Client->find('list', array('conditions' =>array('id' =>$this->Session->read('user.User.client_id') )));
			$this->set(compact('clients'));
		}
		elseif($this->Session->read('user.User.type') == 'Country Admin' && empty($id))
		{
			$default = $this->Skyscraper->Client->find('list',array('conditions' =>array('Client.country'=>$this->Session->read('client.Client.country')),'fields'=>'Client.program_id, Client.program_id','recursive'=>-1));
			
			$this->loadModel("Program");
			$program_list = $this->Program->find('list',array('conditions'=>array('id'=>$default)));
			$this->set('programlist',$program_list);
		
			$clients = $this->Skyscraper->Client->find('list', array('conditions'=>array('country'=>$this->Session->read('client.Client.country'))));
			$this->set(compact('clients'));
		}
		else
		{
			$this->loadModel("Client");
			$programs = $this->Skyscraper->ProgramsSkyscraper->find('all',array('conditions'=>array('skyscraper_id'=> $id),'fields'=>array('program_id')));

			$program_ids = array();
			foreach ($programs as $list) {
				$program_ids[] = $list['ProgramsSkyscraper']['program_id'];
			}
			$clients = $this->Skyscraper->Client->find('list', array('conditions' =>array('program_id' =>$program_ids )));
			$this->set(compact('clients'));

			$this->loadModel("Program");
			$program_list = $this->Program->find('list');
			$this->set('programlist',$program_list);
		}
	}

	function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash('Invalid id for Skyscraper');
			$this->redirect(array('action'=>'index'), null, true);
		}
		if ($this->Skyscraper->delete($id)) {
			$this->Session->setFlash('Skyscraper #'.$id.' deleted');
			$this->redirect(array('action'=>'index'), null, true);
		}
	}

	function admin_update_program_clients()
	{
		$pid = $this->request->data['Program']['Program'];
		$this->loadModel("Client");
		$programs = $this->Client->find('list', array('conditions' =>array('program_id' => $pid)));
		$this->set('program_clients',$programs);
        $this->layout = 'ajax';
	}
}
?>