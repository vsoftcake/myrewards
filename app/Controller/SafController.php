<?php
class SafController extends AppController {

	var $components = array('Cookie','File','SwiftMailer','RequestHandler','Email');
 	var $uses = array('User','Client','ProductsSaf','ClientsProductsSaf','Product','SafsInvitation','ClientsSafsInvitation','SafsLog','MobileSaf');

	function admin_index() {

		// Search Results
		if(isset($this->params['url']['data']))
		{
			$product= $this->params['url']['data'];
			$sql1 = " and ProductsSaf.product_id=".$product['Product']['search'];
			$sql = "SELECT * FROM products as Product, products_safs as ProductsSaf where ProductsSaf.product_id=Product.id and 
			(ProductsSaf.product_id='".$product['Product']['search']."' OR Product.name LIKE '%".$product['Product']['search']."%')";
			$searchproducts=$this->ProductsSaf->query($sql);
			$this->set("searchproducts",$searchproducts);
		}else{
			$this->paginate = array('ProductsSaf' => array('limit' => 20));
			$products = $this->paginate('ProductsSaf', array('search' =>array('sql' => $sql1)));
		}

		$this->set("products",$products);
	}


	function admin_edit($id=null) {

		if (!empty($this->request->data)) {

			$this->request->data['ProductsSaf']['product_id']=current(explode(" ", $this->request->data['ProductsSaf']['product_id']));
			$prdt = $this->ProductsSaf->find('first', array('conditions' => array('product_id'=>$this->request->data['ProductsSaf']['product_id'])));
			if(empty($prdt))
			{
				$msg = "";
				if (isset($this->request->data['ProductsSaf']['id']) && $this->request->data['ProductsSaf']['id']>0)
					$msg = "updated";
				elseif (isset($this->request->data['ProductsSaf']['id']))

				$this->request->data['ProductsSaf']['id'] = 0;
				if ($this->request->data['ProductsSaf']['file']['name'] != '') {
					$logo_extension = substr($this->request->data['ProductsSaf']['file']['name'], strrpos($this->request->data['ProductsSaf']['file']['name'], '.') + 1);
					$this->request->data['ProductsSaf']['logo_extension'] = $logo_extension;
				}

				if ($this->request->data['ProductsSaf']['file']['name'] != '') {
					move_uploaded_file($this->request->data['ProductsSaf']['file']['tmp_name'], PRODUCT_HOT_OFFER_IMAGE_SAF_PATH. $this->request->data['ProductsSaf']['product_id']. '.'. $logo_extension);
				}

				$this->ProductsSaf->save($this->request->data);
				
				$prId = $this->request->data['ProductsSaf']['product_id'];
				$this->ClientsProductsSaf->deleteAll(array('product_id'=>$prId));
				if($this->request->data['ProductsSaf']['product_type'] == '-1'){
					$this->ClientsProductsSaf->save(array('product_id'=>$prId, 'client_id'=>0));
				}else{
					//Save clients products points
					$prClientData = array();
					foreach($this->request->data['Client']['Client'] as $prClient){
						if(!empty($prClient)){
							$prClientData[] = array('product_id'=>$prId, 'client_id'=>$prClient);
						}
					}
					if(!empty($prClientData)){$this->ClientsProductsSaf->saveMany($prClientData);}
				}

				if ($msg == 'updated') {
					$this->Session->setFlash('The product is successfully updated!!');
				} else {
					$this->Session->setFlash('The product is successfully added!!');
				}
				$this->redirect('/admin/saf');
			}
			else{
				$data=$this->request->data;
				if ($this->request->data['ProductsSaf']['file']['name'] != '') {
					$logo_extension = substr($this->request->data['ProductsSaf']['file']['name'], strrpos($this->request->data['ProductsSaf']['file']['name'], '.') + 1);
					$this->request->data['ProductsSaf']['logo_extension'] = $logo_extension;
					$sql = "UPDATE products_safs set logo_extension='$logo_extension' where product_id=".$data['ProductsSaf']['product_id'];
					$this->ProductsSaf->query($sql);
				}

				if ($this->request->data['ProductsSaf']['file']['name'] != '') {
					move_uploaded_file($this->request->data['ProductsSaf']['file']['tmp_name'], PRODUCT_HOT_OFFER_IMAGE_SAF_PATH. $this->request->data['ProductsSaf']['product_id']. '.'. $logo_extension);
				}
				
				$prId = $this->request->data['ProductsSaf']['product_id'];
				$this->ClientsProductsSaf->deleteAll(array('product_id'=>$prId));
				if($this->request->data['ProductsSaf']['product_type'] == '-1'){
					$this->ClientsProductsSaf->save(array('product_id'=>$prId, 'client_id'=>0));
				}else{
					//Save clients products points
					$prClientData = array();
					foreach($this->request->data['Client']['Client'] as $prClient){
						if(!empty($prClient)){
							$prClientData[] = array('product_id'=>$prId, 'client_id'=>$prClient);
						}
					}
					if(!empty($prClientData)){$this->ClientsProductsSaf->saveMany($prClientData);}
				}

				// delete hotoffer image
				if (isset($this->request->data['ProductsSafImage1Delete'])) {
					foreach ($this->request->data['ProductsSafImage1Delete'] as $key => $value) {
						if ($value == '1') {
							$this->File->deleteByPrefix('hot_offer_img_saf/'.$data['ProductsSaf']['product_id']);
							$delExtsql = "UPDATE products_safs set logo_extension='' where product_id=".$data['ProductsSaf']['product_id'];
							$this->ProductsSaf->query($delExtsql);
						}
					}
				}

				$this->Session->setFlash('The product is successfully updated!!');
				$this->redirect('/admin/saf');

			}
		}
		else {

			$this->request->data = $this->ProductsSaf->read(null, $id);
		}

		$clientsList = $this->Client->find('list',array('conditions'=> 'Client.name!=""'));
		$clientsSaf = $this->ClientsProductsSaf->find('list',array('conditions' =>array('ClientsProductsSaf.product_id' => $this->request->data['ProductsSaf']['product_id']),'fields'=>array('ClientsProductsSaf.id','ClientsProductsSaf.client_id')));
		$clients= array();
			foreach ($clientsSaf as $client) {
			   array_push($clients,$client);
			}
		if(!empty($clients))
		{
			$programsList = $this->Product->Program->find('list');

			$sql = "SELECT Client.program_id from clients as Client where Client.id in (".implode(',',$clients).") group by Client.program_id";
			$programsSaf = $this->Client->query($sql);

			$programs= array();$program_ids = array();
			foreach ($programsSaf as $program) {
				array_push($programs,$program['Client']['program_id']);
				$program_ids[] = $program['Client']['program_id'];
			}
			$this->set(compact('programsList','programs'));

			$allclients = $this->Client->find('all', array('conditions' =>array('program_id' =>$program_ids ),'order'=>'Client.program_id, Client.name','fields'=>array('Client.id','Client.name','Client.program_id','Program.name'),'recursive'=>0));

			$this->set('allclients',$allclients);
		}
		$this->set(compact('clients','clientsList'));

	}

	function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash('Invalid id for Product');
			$this->redirect(array('action'=>'index'), null, true);
		}

		$sql = "SELECT product_id FROM products_safs as ProductsSaf where ProductsSaf.id=".$id;
		$product_id=$this->ProductsSaf->query($sql);
		$product_id=$product_id[0]['ProductsSaf']['product_id'];
		$condition = array('ClientsProductsSaf.product_id' => $product_id);
		$this->ClientsProductsSaf->deleteAll($condition,false);

		if ($this->ProductsSaf->delete($id)) {

			$this->Session->setFlash('Product #'.$id.' deleted');
			$this->redirect(array('action'=>'index'), null, true);
		}
	}

	function saf_request($id = null) {
		if (!empty($this->request->data)) {

			$fname= $this->Session->read('user.User.first_name');
			$lname= $this->Session->read('user.User.last_name');
			$mnumber=$this->Session->read('user.User.username');
			$youremail= $this->Session->read('user.User.email');;
			$dname=  $this->request->data['ProductsSaf']['dealership'];
			$clientname=$this->Session->read('client.Client.name');

			foreach($this->request->data['ProductsSaf'] as $key => $value)
			{
				$success = "";
				if(!empty($value['friendemail']))
				{
					$invitation_id = $this->ClientsSafsInvitation->find('first',array('conditions'=>array('ClientsSafsInvitation.client_id'=>$this->Session->read('client.Client.id'))));

					if(!empty($invitation_id))
					{
						//$unique_id = md5(uniqid(rand(), true));
						$unique_id = substr(number_format(time() * rand(),0,'',''),0,8);

						$invitation_message = $this->SafsInvitation->find('first',array('conditions'=>array('SafsInvitation.id'=>$invitation_id['ClientsSafsInvitation']['safs_invitation_id']),'recursive'=>-1));

						$invitee_message = str_replace('[[client_name]]',$clientname,str_replace('[[friend_name]]',$value['friendname'],str_replace('[[user_first_name]]',$fname,str_replace('[[user_last_name]]',$lname,str_replace('[[user_membership_number]]',$mnumber,$invitation_message['SafsInvitation']['message_body'])))));
						$sender_message = str_replace('[[client_name]]',$clientname,str_replace('[[friend_name]]',$value['friendname'],str_replace('[[user_first_name]]',$fname,str_replace('[[user_last_name]]',$lname,str_replace('[[user_membership_number]]',$mnumber,$invitation_message['SafsInvitation']['sender_message_body'])))));

						$to = $youremail;
						$from = "support@therewardsteam.com";
						$subject = $invitation_message['SafsInvitation']['sender_subject'];

						$message ="<html><body>$sender_message <br> <span style='font-style:italic;font-weight:bold;'>Unique Key : </span>$unique_id</body></html>";

						$headers  = "From: $from\r\n";
						$headers .= "Content-type: text/html\r\n";
						$mail_sender = mail($to, $subject, $message, $headers);


						$to = $value['friendemail'];
						$from = $youremail;
						$subject_to_friend = $invitation_message['SafsInvitation']['subject'];

						$message = "<html><body>$invitee_message <br> <span style='font-style:italic;font-weight:bold;'>Unique Key : </span>$unique_id</body></html>";

						$headers  = "From: $from\r\n";
						$headers .= "Content-type: text/html\r\n";
						$mail_invitee = mail($to, $subject_to_friend, $message, $headers);

						if($mail_invitee && $mail_sender)
						{
							$this->SafsLog->create();
							$this->request->data['SafsLog']['user_id'] = $this->Session->read('user.User.id');
							$this->request->data['SafsLog']['user_name'] = $this->Session->read('user.User.username');
							$this->request->data['SafsLog']['client_id'] = $this->Session->read('client.Client.id');
							$this->request->data['SafsLog']['client_name'] = $this->Session->read('client.Client.name');
							$this->request->data['SafsLog']['invitation_id'] = $invitation_message['SafsInvitation']['id'];
							$this->request->data['SafsLog']['invitee_name'] = $value['friendname'];
							$this->request->data['SafsLog']['invitee_email'] = $value['friendemail'];
							$this->request->data['SafsLog']['unique_id'] = $unique_id;
							$this->request->data['SafsLog']['created'] = date('Y-m-d h:i:s');

							$this->SafsLog->save($this->request->data);
							$success = "true";
						}
					}

				}

			}
			if($success == 'true')
			{
				echo '<p style="padding:10px"><strong> Mail has been Sent.</strong></p>';
			}
		}

		$sql = "SELECT count(*)As count FROM safs_logs As SafsLog where user_id =".$this->Session->read('user.User.id');
		$count = $this->SafsLog->query($sql);
		$this->set('count', $count);
	}

	function admin_xml_products($limit = 10) {

		$this->layout = 'ajax';

		$conditions = "Product.active = 1 AND (Product.expires = '0000-00-00' OR Product.expires >= NOW()) AND ClientsProductsSaf.product_id LIKE '". $this->params['url']['s']. "%'";
		if (isset($this->params['url']['s'])) {
			$conditions = "Product.id LIKE '". $this->params['url']['s']. "%'";
		}

		$data = $this->Product->find('all', array('conditions'=>$conditions, 'fields'=>'Product.id,Product.name', 'order'=>'Product.name', 'limit'=>$limit));

		$this->set('products', $data);

	}

	function admin_products() {
		$pid = $_GET['term'];
		if (isset($pid)) {
			$conditions = array("Product.active = 1 AND (Product.expires = '0000-00-00' OR Product.expires >= NOW())  AND Product.id LIKE '%$pid%'");
		}
		else
		{
			$conditions = array("Product.active = 1 AND (Product.expires = '0000-00-00' OR Product.expires >= NOW())");
		}

		$data = $this->Product->find('all',array('conditions' => $conditions,'fields'=>array('Product.id','Product.name','Product.product_type'),'recursive'=>-1));

		$this->set('products', $data);

	}

	function admin_get_product_clients($productId)
	{
		$this->layout = 'ajax';
		$programsList = $this->Product->Program->find('list');
		$programs_ids = $this->Product->Program->ProgramsProduct->find('all',array('conditions' =>array('ProgramsProduct.product_id' => $productId),
																				'fields'=>array('ProgramsProduct.program_id'),
																				'recursive'=>-1));
		$programs = array();
		foreach($programs_ids as $program)
		{
			$programs[] = $program['ProgramsProduct']['program_id'];
		}

		$clientsSaf = $this->Product->Client->ClientsProduct->find('list', array('conditions' =>array('ClientsProduct.product_id' =>$productId),
																			  'fields'=>array('ClientsProduct.id','ClientsProduct.client_id'),
																			  'recursive'=>-1));
		$clients= array();
			foreach ($clientsSaf as $client) {
			   array_push($clients,$client);
			}

		$allclients = $this->Client->find('all', array('conditions' =>array('program_id' =>$programs ),'order'=>'Client.program_id, Client.name','fields'=>array('Client.id','Client.name','Client.program_id','Program.name'),'recursive'=>0));

		$this->set('allclients',$allclients);
		$this->set(compact('clients','programs','programsList'));

	}

	function admin_update_program_clients()
	{
		$pid = $this->request->data['Program']['Program'];
		$programs = $this->Product->Program->Client->find('list', array('conditions' =>array('program_id' => $pid)));

		$this->set('program_clients',$programs);
		$this->layout = 'ajax';
	}

	function admin_load_clients($list)
	{
		$this->layout = 'blank';
		$this->loadModel('Program');
		if(!empty($list))
		{
			$clients = $this->Client->find('list',array('conditions'=>array('Client.program_id'=>$list),'order'=>'Client.name'));
			$this->set('clientsList',$clients);

			$programname = $this->Program->find('first',array('conditions'=>array('Program.id'=>$list), 'recursive'=>-1, 'fields'=>array('Program.name')));
			$this->set('program_name',$programname['Program']['name']);
			$this->set('pid',$list);
			$this->set('value',$value);

		}
	}

	function mobile_saf()
	{
		$getUsersList = $this->MobileSaf->find('all',array('conditions'=>array('MobileSaf.sent_status'=>0)));

		foreach($getUsersList as $usersList)
		{
			if(!empty($usersList['MobileSaf']['email']))
			{
				$this->User->unBindModel(array('hasMany' => array('UserAction')));
				$userDetails = $this->User->find('first',array('conditions'=>array('User.id'=>$usersList['MobileSaf']['user_id']),
															 'fields'=>array('User.id','User.client_id','User.first_name', 'User.last_name', 'User.username','User.email','Client.name')));

				$fname= $userDetails['User']['first_name'];
				$lname= $userDetails['User']['last_name'];
				$mnumber= $userDetails['User']['username'];
				$youremail= $userDetails['User']['email'];
				$clientname= $userDetails['Client']['name'];
				$clientid = $userDetails['User']['client_id'];

				$invitation_id = $this->ClientsSafsInvitation->find('first',array('conditions'=>array('ClientsSafsInvitation.client_id'=>$clientid)));

				if(!empty($invitation_id))
				{
					$unique_id = substr(number_format(time() * rand(),0,'',''),0,8);

					$invitation_message = $this->SafsInvitation->find('first',array('conditions'=>array('SafsInvitation.id'=>$invitation_id['ClientsSafsInvitation']['safs_invitation_id']),'recursive'=>-1));

					$invitee_message = str_replace('[[client_name]]',$clientname,str_replace('[[friend_name]]',$usersList['MobileSaf']['fname'],str_replace('[[user_first_name]]',$fname,str_replace('[[user_last_name]]',$lname,str_replace('[[user_membership_number]]',$mnumber,$invitation_message['SafsInvitation']['message_body'])))));
					$sender_message = str_replace('[[client_name]]',$clientname,str_replace('[[friend_name]]',$usersList['MobileSaf']['fname'],str_replace('[[user_first_name]]',$fname,str_replace('[[user_last_name]]',$lname,str_replace('[[user_membership_number]]',$mnumber,$invitation_message['SafsInvitation']['sender_message_body'])))));

					$to = $youremail;
					$from = "support@therewardsteam.com";
					$subject = $invitation_message['SafsInvitation']['sender_subject'];

					$message ="<html><body>$sender_message <br> <span style='font-style:italic;font-weight:bold;'>Unique Key : </span>$unique_id</body></html>";

					$headers  = "From: $from\r\n";
					$headers .= "Content-type: text/html\r\n";
					$mail_sender = mail($to, $subject, $message, $headers);


					$to = $usersList['MobileSaf']['email'];
					$from = $youremail;
					$subject_to_friend = $invitation_message['SafsInvitation']['subject'];

					$message = "<html><body>$invitee_message <br> <span style='font-style:italic;font-weight:bold;'>Unique Key : </span>$unique_id</body></html>";
					$headers  = "From: $from\r\n";
					$headers .= "Content-type: text/html\r\n";
					$mail_invitee = mail($to, $subject_to_friend, $message, $headers);

					if($mail_invitee && $mail_sender)
					{
						$this->SafsLog->create();
						$this->request->data['SafsLog']['user_id'] = $userDetails['User']['id'];
						$this->request->data['SafsLog']['user_name'] = $mnumber;
						$this->request->data['SafsLog']['client_id'] = $clientid;
						$this->request->data['SafsLog']['client_name'] = $clientname;
						$this->request->data['SafsLog']['invitation_id'] = $invitation_message['SafsInvitation']['id'];
						$this->request->data['SafsLog']['invitee_name'] = $usersList['MobileSaf']['fname'];
						$this->request->data['SafsLog']['invitee_email'] = $usersList['MobileSaf']['email'];
						$this->request->data['SafsLog']['unique_id'] = $unique_id;
						$this->request->data['SafsLog']['created'] = date('Y-m-d h:i:s');

						$this->SafsLog->save($this->request->data);

						$updateQuery = array();
						$updateQuery['sent_status'] = 1;
						$this->MobileSaf->updateAll($updateQuery,
								array('MobileSaf.user_id' => $usersList['MobileSaf']['user_id']));
					}
				}
			}
		}
		$this->autoRender = false;
	}
}
?>