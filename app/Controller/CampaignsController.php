<?php
class CampaignsController extends AppController {

	var $helpers = array('Csv');
	var $components = array('Cookie', 'SwiftMailer', 'MailQueue','RequestHandler','File');
	var $uses = array('User', 'Client','Campaign','Program','CampaignEntry','Participate','ClientsCampaign','FirstTimeLogin', 'ProgramsCampaign', 'CampaignDetail','CampaignWinner','CfContents','CustomFields');

	function admin_index() {

		$conditions = array();
		if (isset($this->params['url']['data'])) {
			$this->Session->write($this->name.'.search', $this->params['url']['data']);
		}

		if ($this->Session->check($this->name.'.search')) {
			$this->request->data = $this->Session->read($this->name.'.search');
			if  (!empty($this->request->data['Campaign']['search'])) {
				$conditions[] = "
					(`Campaign`.`name` LIKE  '%". $this->request->data['Campaign']['search']. "%'
					OR `Campaign`.`id` LIKE  '%". $this->request->data['Campaign']['search']. "%')
				";
			}
		}

		$this->Campaign->recursive = 0;
		$this->set('campaigns', $this->paginate('Campaign',$conditions));

		$this->loadModel("Program");
		$plist = $this->Program->find('list');
		$this->set('programs_list',$plist);

		$clist = $this->Client->find('list');
		$this->set('clients_list',$clist);
	}


	function admin_edit($id=null)
	{
		if (!empty($this->request->data))
		{
				if ($this->request->data['Image']['file']['name'] != '')
				{
					$extension = substr($this->request->data['Image']['file']['name'], strrpos($this->request->data['Image']['file']['name'], '.') + 1);
					$this->request->data['Campaign']['campaign_logo_extension'] = $extension;
				}
				if ($this->request->data['ParticipateImage']['file']['name'] != '')
				{
					$extension = substr($this->request->data['ParticipateImage']['file']['name'], strrpos($this->request->data['ParticipateImage']['file']['name'], '.') + 1);
					$this->request->data['Campaign']['participate_button_image_ext'] = $extension;
				}
				if ($this->request->data['FlyerNotificationImage']['file']['name'] != '') {
					$extension = substr($this->request->data['FlyerNotificationImage']['file']['name'], strrpos($this->request->data['FlyerNotificationImage']['file']['name'], '.') + 1);
					$this->request->data['Campaign']['notification_image_ext'] = $extension;
				}

				$weekdays = "";
				foreach($this->request->data['Campaign']['week_days'] as $days)
				{
					$weekdays .= strlen($weekdays)?",":"";
					$weekdays .= $days;
				}

				$this->request->data['Campaign']['week_days'] = $weekdays;

				$draw_start = $this->request->data['Campaign']['draw_start_date']['year']."-".$this->request->data['Campaign']['draw_start_date']['month']."-".$this->request->data['Campaign']['draw_start_date']['day'];
				$draw_end = $this->request->data['Campaign']['draw_end_date']['year']."-".$this->request->data['Campaign']['draw_end_date']['month']."-".$this->request->data['Campaign']['draw_end_date']['day'];

				$entry_start = $this->request->data['Campaign']['entry_start_date']['year']."-".$this->request->data['Campaign']['entry_start_date']['month']."-".$this->request->data['Campaign']['entry_start_date']['day'];
           		$entry_end = $this->request->data['Campaign']['entry_end_date']['year']."-".$this->request->data['Campaign']['entry_end_date']['month']."-".$this->request->data['Campaign']['entry_end_date']['day'];

           		$start = $this->request->data['Campaign']['start_date']['year']."-".$this->request->data['Campaign']['start_date']['month']."-".$this->request->data['Campaign']['start_date']['day'];
          		$end = $this->request->data['Campaign']['end_date']['year']."-".$this->request->data['Campaign']['end_date']['month']."-".$this->request->data['Campaign']['end_date']['day'];

				$login_start = $this->request->data['Campaign']['login_start_date']['year']."-".$this->request->data['Campaign']['login_start_date']['month']."-".$this->request->data['Campaign']['login_start_date']['day'];
				$login_end = $this->request->data['Campaign']['login_end_date']['year']."-".$this->request->data['Campaign']['login_end_date']['month']."-".$this->request->data['Campaign']['login_end_date']['day'];

          		$check = true;
	            if($start == date('Y-m-d'))
				{
					$this->request->data['Campaign']['active'] = 1;
				}
			   	if ($this->request->data['Campaign']['name']=='')
			   	{
					$check = false;
					$this->Campaign->invalidate('name', 'required');

				}

			   	if ($this->request->data['Client']['Client']=='')
				{
					$check = false;
					$this->Client->invalidate('Client', 'required');

				}

				if ($this->request->data['Campaign']['description']=='')
				{
					$check = false;
					$this->Campaign->invalidate('description', 'required');

				}

			   	if($this->request->data['Campaign']['max_winners']=='' && ($this->request->data['Campaign']['campaign_type']=='A' || ($this->request->data['Campaign']['campaign_type']=='M' && ($this->request->data['Campaign']['manual_winner_type'] != 'choose'))))
				{
				    $check = false;
					$this->Campaign->invalidate('max_winners', 'required');

				}
				if($this->request->data['Campaign']['winners_per_draw']=='' && ($this->request->data['Campaign']['campaign_type']=='A' || ($this->request->data['Campaign']['campaign_type']=='M' && ($this->request->data['Campaign']['manual_winner_type'] != 'choose'))))
				{
				    $check = false;
					$this->Campaign->invalidate('winners_per_draw', 'required');

				}
				if($this->request->data['Campaign']['campaign_type']=='')
				{
				    $check = false;
					$this->Campaign->invalidate('campaign_type', 'required');

				}
				if($this->request->data['Campaign']['draw_type']=='' && ($this->request->data['Campaign']['campaign_type']=='A' || ($this->request->data['Campaign']['campaign_type']=='M' && ($this->request->data['Campaign']['manual_winner_type'] != 'choose'))))
				{
				    $check = false;
					$this->Campaign->invalidate('draw_type', 'required');

				}

				if(($this->request->data['Campaign']['campaign_type'] == 'A') && ($this->request->data['Campaign']['user_type'] == '' || empty($this->request->data['Campaign']['user_type'])))
				{
					$check = false;
					$this->Campaign->invalidate('user_type', 'required');
					$this->Session->setFlash('Please select user participant type.');
				}

				if(($this->request->data['Campaign']['campaign_type'] == 'M') && ($this->request->data['Campaign']['manual_winner_type'] == '' || empty($this->request->data['Campaign']['manual_winner_type'])))
				{
					$check = false;
					$this->Campaign->invalidate('manual_winner_type', 'required');
				}

				if(empty($id))
				{
					if($start < date("Y-m-d"))
					{  $check = false;
						$this->Campaign->invalidate('start_date', 'todaystartdate');
						$this->Session->setFlash('Please correct the errors below');
					}
				}

		 	if ($check == true && $this->Campaign->save($this->request->data))
			{
				if ($this->request->data['Image']['file']['name'] != '') {
					move_uploaded_file($this->request->data['Image']['file']['tmp_name'], CAMPAIGN_IMAGE_PATH.'image/'. $this->Campaign->id. '.'. $this->request->data['Campaign']['campaign_logo_extension']);
				}
				if ($this->request->data['ParticipateImage']['file']['name'] != '') {
					move_uploaded_file($this->request->data['ParticipateImage']['file']['tmp_name'], CAMPAIGN_IMAGE_PATH.'participate_image/'. $this->Campaign->id. '.'. $this->request->data['Campaign']['participate_button_image_ext']);
				}
				if ($this->request->data['FlyerNotificationImage']['file']['name'] != '') {
					move_uploaded_file($this->request->data['FlyerNotificationImage']['file']['tmp_name'], CAMPAIGN_IMAGE_PATH.'notification_image/'. $this->Campaign->id. '.'. $this->request->data['Campaign']['notification_image_ext']);
				}

				// delete unwanted images
				if (isset($this->request->data['ImageDelete'])) {
					$delete_images = array();
					foreach ($this->request->data['ImageDelete'] as $key => $value) {
						if ($value == '1') {
							$this->File->deleteByPrefix('campaign/image/'.$this->Campaign->id);
							$this->request->data['Campaign']['campaign_logo_extension']='';
							$this->Campaign->save($this->request->data);
						}
					}
				}

				if (isset($this->request->data['ParticipateImageDelete']))
				{
					$delete_images = array();
					foreach ($this->request->data['ParticipateImageDelete'] as $key => $value) {
						if ($value == '1') {
							$this->File->deleteByPrefix('campaign/participate_image/'.$this->Campaign->id);
							$this->request->data['Campaign']['participate_button_image_ext']='';
							$this->Campaign->save($this->request->data);
						}
					}
				}

				foreach ($this->request->data['CustomFields'] as $key => $value)
				{
					if(!empty($value['name']))
					{
						$this->CustomFields->create();
						$value['campaign_id'] = $id;
						$this->CustomFields->save(array('CustomFields' => $value));
					}
				}

				$this->Session->setFlash('The Competition has been saved');
				$this->redirect_session(array('action'=>'index'), null, true);
			}
			else
			{
				$this->Session->setFlash('Please correct the errors below.');
			}
		}
		else
		{
			$this->request->data = $this->Campaign->read(null, $id);
			$this->_setRedirect();
		}

		$this->loadModel("Client");
		$programs = $this->Campaign->ProgramsCampaign->find('all',array('conditions'=>array('campaign_id'=> $id),'fields'=>array('program_id')));

		$program_ids = array();
		foreach ($programs as $list) {
			$program_ids[] = $list['ProgramsCampaign']['program_id'];
		}
		$clients = $this->Campaign->Client->find('list', array('conditions' =>array('program_id' =>$program_ids )));
		$this->set(compact('clients'));

		$this->loadModel("Program");
		$program_list = $this->Program->find('list');
		$this->set('programlist',$program_list);
		$this->set('days',$this->request->data['Campaign']['week_days']);

		if(!empty($id)) {
			$sql = "SELECT count(*)As count FROM custom_fields As CustomFields where campaign_id =$id";
			$count = $this->CustomFields->query($sql);
			$this->set('count', $count);
	
			$sql1 = "SELECT * FROM custom_fields As CustomFields where campaign_id =$id";
			$list = $this->CustomFields->query($sql1);
			$this->set('lists', $list);
		}
	}

	function admin_update_program_clients()
	{
		$pid = $this->request->data['Program']['Program'];
		$this->loadModel("Client");
		$programs = $this->Client->find('list', array('conditions' =>array('program_id' => $pid)));
		$this->set('program_clients',$programs);
        $this->layout = 'ajax';
	}

	function admin_update_clients()
	{
		$pid = $this->request->data['Campaign']['program'];

		if(!empty($pid))
			$programs = $this->Client->find('list', array('conditions' =>array('program_id' => $pid)));
		else
			$programs = $this->Client->find('list');

		$this->set('program_clients',$programs);
        $this->layout = 'ajax';
	}

	function details($step=1,$id=0)
	{
		$query  = "	where Campaign.id=ClientsCampaign.campaign_id and Campaign.active=1 and
		ClientsCampaign.client_id=".$this->Session->read('client.Client.id')." and ClientsCampaign.client_id=Client.id
		and Campaign.start_date<= current_date and end_date >= current_date";

		$this->paginate = array('CampaignDetail' => array('limit' => 6));
		$details = $this->paginate('CampaignDetail', array('search' =>array('sql' => $query)));


		if ($step == 1)
		{
			if (empty($this->request->data['Campaign']['name1']))
			{
				$this->set('details',$details);
			}
			else
			{
				$step = 2;
			}
		}
		else if ($step == 2)
		{
			$query  = "SELECT * FROM campaigns as Campaign, clients_campaigns as ClientsCampaign where Campaign.id=ClientsCampaign.campaign_id
			and Campaign.active=1 and Campaign.id=".$id." and ClientsCampaign.client_id=".$this->Session->read('client.Client.id')."
			and Campaign.start_date<= current_date and Campaign.end_date >= current_date";

			$details = $this->Campaign->query($query);

			$sql = "SELECT count(CampaignEntry.campaign_id) as count from campaigns as Campaign, campaign_entries as CampaignEntry
			WHERE Campaign.id=CampaignEntry.campaign_id and Campaign.id=".$id;
			$count = $this->CampaignEntry->query($sql);

			$existing_users_query = "SELECT * from campaigns as Campaign, campaign_entries as CampaignEntry
			where CampaignEntry.campaign_id=Campaign.id and Campaign.id=".$id." and CampaignEntry.user_id=".$this->Session->read('user.User.id')."
			and CampaignEntry.campaign_type='".$details[0]['Campaign']['campaign_type']."'";
			$existing_users = $this->CampaignEntry->query($existing_users_query);

			$this->set('existing_users',$existing_users);

			$this->set('details',$details);
			$this->set('count',$count);
		}
		else if ($step == 3)
		{
			$query  = "SELECT * FROM campaigns as Campaign, clients_campaigns as ClientsCampaign where Campaign.id=ClientsCampaign.campaign_id
			and Campaign.active=1 and Campaign.id=".$id." and ClientsCampaign.client_id=".$this->Session->read('client.Client.id')."
			and Campaign.start_date<= current_date and end_date >= current_date";

			$details = $this->Campaign->query($query);
			$this->set('dlist',$details);

			$sql1 = "SELECT * FROM custom_fields As CustomFields where campaign_id =$id";
			$list = $this->CustomFields->query($sql1);
			$this->set('lists', $list);
		}
		else if($step==4)
		{
			$query  = "SELECT * FROM campaigns as Campaign where id=".$id;

			$details = $this->Campaign->query($query);

			if($details[0]['Campaign']['campaign_type'] == 'M')
			{
				$this->request->data['CampaignEntry']['campaign_id'] = $id;
				$this->request->data['CampaignEntry']['campaign_type'] = $details[0]['Campaign']['campaign_type'];
				$this->request->data['CampaignEntry']['user_id'] = $this->Session->read('user.User.id');
				$this->request->data['CampaignEntry']['user_name'] = $this->Session->read('user.User.username');
				$this->request->data['CampaignEntry']['first_name'] = $this->Session->read('user.User.first_name');
				$this->request->data['CampaignEntry']['last_name'] = $this->Session->read('user.User.last_name');
				$this->request->data['CampaignEntry']['client_id'] = $this->Session->read('client.Client.id');
				$this->request->data['CampaignEntry']['join_date'] =  date('Y-m-d h:i:s');

				if($this->CampaignEntry->save($this->request->data))
				{
					$entryid = $this->CampaignEntry->id;
					foreach ($this->request->data['CfContents'] as $key => $value)
					{
						$this->CfContents->create();
						$fields['campaign_id'] = $id;
						$fields['campaign_entry_id'] = $entryid;
						$fields['cf_id'] = $key;
						$fields['value'] = $value;
						$this->CfContents->save(array('CfContents' => $fields));
					}

					$step = 5;
					$this->set('type',$details[0]['Campaign']['campaign_type']);
					$this->set('message',$details[0]['Campaign']['participation_message']);
				}
				else
				{
					$this->Session->setFlash('Please correct the errors below.');
				}
			}
			else if($details[0]['Campaign']['campaign_type'] == 'A')
			{
				$query  = "SELECT * FROM campaign_entries as CampaignEntry where CampaignEntry.campaign_id=".$id."
				and CampaignEntry.user_id=".$this->Session->read('user.User.id');
				$check = $this->CampaignEntry->query($query);
				if(empty($check))
				{
					$this->request->data['CampaignEntry']['campaign_id'] = $id;
					$this->request->data['CampaignEntry']['campaign_type'] = $details[0]['Campaign']['campaign_type'];
					$this->request->data['CampaignEntry']['user_id'] = $this->Session->read('user.User.id');
					$this->request->data['CampaignEntry']['user_name'] = $this->Session->read('user.User.username');
					$this->request->data['CampaignEntry']['first_name'] = $this->Session->read('user.User.first_name');
					$this->request->data['CampaignEntry']['last_name'] = $this->Session->read('user.User.last_name');
					$this->request->data['CampaignEntry']['client_id'] = $this->Session->read('client.Client.id');
					$this->request->data['CampaignEntry']['join_date'] =  date('Y-m-d h:i:s');

					if($this->CampaignEntry->save($this->request->data))
					{
						$entryid = $this->CampaignEntry->id;
						foreach ($this->request->data['CfContents'] as $key => $value)
						{
							$this->CfContents->create();
							$fields['campaign_id'] = $id;
							$fields['campaign_entry_id'] = $entryid;
							$fields['cf_id'] = $key;
							$fields['value'] = $value;
							$this->CfContents->save(array('CfContents' => $fields));
						}

						$updateSql = "UPDATE campaign_winners SET flag=1 WHERE campaign_id=".$id." AND winner_id=".$this->Session->read('user.User.id');
						$update = $this->CampaignWinner->query($updateSql);

						$step = 5;
						$this->set('type',$details[0]['Campaign']['campaign_type']);
						$this->set('message',$details[0]['Campaign']['participation_message']);
					}
					else
					{
						$this->Session->setFlash('Please correct the errors below.');
					}
				}
				else
				{
					$joindate = date('Y-m-d h:i:s');
					$sql = "UPDATE campaign_entries as CampaignEntry set CampaignEntry.email='".$this->request->data['CampaignEntry']['email']."',
					CampaignEntry.address1='".$this->request->data['CampaignEntry']['address1']."', CampaignEntry.address2='".$this->request->data['CampaignEntry']['address2']."',
					CampaignEntry.country='".$this->request->data['CampaignEntry']['country']."', CampaignEntry.state='".$this->request->data['CampaignEntry']['state']."',
					CampaignEntry.region='".$this->request->data['CampaignEntry']['region']."', CampaignEntry.suburb='".$this->request->data['CampaignEntry']['suburb']."',
					CampaignEntry.city='".$this->request->data['CampaignEntry']['city']."', CampaignEntry.phone='".$this->request->data['CampaignEntry']['phone']."',
					CampaignEntry.join_date='".$joindate."' WHERE CampaignEntry.campaign_id=".$id." and user_id=".$this->Session->read('user.User.id');
					$updateUser = $this->CampaignEntry->query($sql);

					$entryid = $check[0]['CampaignEntry']['id'];
					foreach ($this->request->data['CfContents'] as $key => $value)
					{
						$this->CfContents->create();
						$fields['campaign_id'] = $id;
						$fields['campaign_entry_id'] = $entryid;
						$fields['cf_id'] = $key;
						$fields['value'] = $value;
						$this->CfContents->save(array('CfContents' => $fields));
					}
					$updateSql = "UPDATE campaign_winners SET flag=1 WHERE campaign_id=".$id." AND winner_id=".$this->Session->read('user.User.id');
					$update = $this->CampaignWinner->query($updateSql);

					$step = 5;
					$this->set('type',$details[0]['Campaign']['campaign_type']);
					$this->set('message',$details[0]['Campaign']['participation_message']);
				}
			}
		}
		else if($step==6)
		{
			$updateSql = "UPDATE campaign_winners SET flag=1 WHERE campaign_id=".$id." AND winner_id=".$this->Session->read('user.User.id');
			$update = $this->CampaignWinner->query($updateSql);
			$this->redirect('/');
		}
		$this->set('step', $step);

	}

	function campaign_rules($id)
	{
		$sql = "SELECT * from campaigns as Campaign WHERE Campaign.id=".$id;
		$rules = $this->Campaign->query($sql);

		$this->set('rules',$rules);

		$sql1 = "SELECT count(CampaignEntry.campaign_id) as count from campaigns as Campaign, campaign_entries as CampaignEntry
		WHERE Campaign.id=CampaignEntry.campaign_id and Campaign.id=".$id;
		$count = $this->CampaignEntry->query($sql1);

		$existing_users_query = "SELECT * from campaigns as Campaign, campaign_entries as CampaignEntry
					where CampaignEntry.campaign_id=Campaign.id and Campaign.id=".$id." and CampaignEntry.user_id=".$this->Session->read('user.User.id')."
					and CampaignEntry.campaign_type='".$rules[0]['Campaign']['campaign_type']."'";
					$existing_users = $this->CampaignEntry->query($existing_users_query);

			$this->set('existing_users',$existing_users);


		$this->set('count',$count);
	}

	function admin_users_participated($id)
	{
		$sql1 = " WHERE Campaign.id=CampaignEntry.campaign_id and Client.id=CampaignEntry.client_id and CampaignEntry.campaign_id=".$id;

		$this->paginate = array('Participate' => array('limit' => 30));
		$users = $this->paginate('Participate', array('search' =>array('sql' => $sql1)));

		$this->set('users_list',$users);

		$this->set('campaignid',$id);

	}

	function update_state()
    {
	   	$selected_country= $this->request->data['CampaignEntry']['country'];
    	$defaultstatesql ="SELECT State.state_cd, State.state_name FROM states AS State, countries as Country where State.country_cd = Country.id
	   	and Country.name='".$selected_country."'";

    	$this->loadModel("State");
		$cstates = $this->State->query($defaultstatesql);

    	$ctrystate = array();
		foreach ($cstates as $ctrystates) {
			$ctrystate[$ctrystates['State']['state_cd']] = $ctrystates['State']['state_name'];
		}

        $this->set('cstates',$ctrystate);
        $this->layout = 'ajax';
    }

    function admin_status($id, $status)
	{
		if($status == 1)
		{
			$sql = "update campaigns set active=0, manual_flag=1 where id=".$id;
			$this->Campaign->query($sql);
			$this->Session->setFlash('The Competition status has been deactivated');

			$this->redirect(array('action'=>'list_campaign'), null, true);
		}
		else if($status ==0)
		{
			$sql = "update campaigns set active=1, manual_flag=0 where id=".$id." and end_date>=current_date and start_date<=current_date";
			$this->Campaign->query($sql);

			if( mysql_affected_rows() == 0 )
				$this->Session->setFlash('The Competition status is not activated');
			else
				$this->Session->setFlash('The Competition status has been activated');
			$this->redirect(array('action'=>'list_campaign'), null, true);
		}
		$this->autoRender=false;
    }
	function admin_update_campaigns($cid,$sort,$pid,$type)
	{
		$this->loadModel("CampaignList");
		$sql1 = " where 1=1 ";
		if($cid<>'c=')
		{
			$cid = substr($cid,2);
			$sql1.= " and ClientsCampaign.campaign_id=Campaign.id and ClientsCampaign.client_id=".$cid." ";
		}

		if($pid<>'p=')
		{
			$pid = substr($pid,2);
			$sql1.= " and ProgramCampaign.campaign_id=Campaign.id and ProgramCampaign.program_id=".$pid." ";
		}

		if($type<>'t=')
		{
			$type = substr($type,2);
			$sql1.= " and Campaign.campaign_type='".$type."' ";
		}

		if($sort<>'s=')
		{
			$sort = substr($sort,2);
			$sql1.=" and Campaign.active=".$sort." ";
		}
		else
			$sql1.=" and (Campaign.active='0' or Campaign.active=1) ";

		$sql1.="  group by Campaign.id ";
		$this->paginate = array('CampaignList' => array('limit' => 10));
		$campaigns = $this->paginate('CampaignList', array('search' =>array('sql' => $sql1)));

		if($sort == 's=')
			$sort = '';
		if($pid == 'p=')
			$pid = '';
		if($cid == 'c=')
			$cid = '';
		if($type == 't=')
			$type = '';

		$this->loadModel("Program");
		$plist = $this->Program->find('list');

		$clist= '';

		if(!empty($pid))
		{
			$clist = $this->Client->find('list', array('conditions' =>array('program_id' =>$pid )));
		}
		else
		{
			$clist = $this->Client->find('list');
		}

		$this->set('clients_list',$clist);
		$this->set('client',$cid);
		$this->set('program',$pid);
		$this->set('campaigns',$campaigns);
		$this->set('sortby',$sort);
		$this->set('oftype',$type);
		$this->set('programs_list',$plist);

		$check_winners="SELECT campaign_id, COUNT(*) as total FROM campaign_winners group by campaign_id";
		$winner_counts = $this->CampaignWinner->query($check_winners);
		$winners = array();
		foreach($winner_counts as $winner)
			$winners[$winner['campaign_winners']['campaign_id']] = $winner[0]['total'];
		$this->set('winner_counts',$winners);

		$check_participants = "SELECT CampaignEntry.campaign_id, COUNT(*) as total FROM campaign_entries as CampaignEntry
		group by CampaignEntry.campaign_id";
		$pcounts = $this->CampaignEntry->query($check_participants);
		$participants = array();

		foreach($pcounts as $count)
		{
			$participants[$count['CampaignEntry']['campaign_id']] = $count[0]['total'];
		}
		$this->set('participant_counts',$participants);

	}

	function admin_delete($id)
	{
		if (!$id) {
			$this->Session->setFlash('Invalid id for Competition');
			$this->redirect(array('action'=>'index'), null, true);
		}
		else
		{
			$sql1 = "delete from campaign_entries where campaign_id=".$id;
			$this->CampaignEntry->query($sql1);

			$sql2 = "delete from clients_campaigns where campaign_id=".$id;
			$this->ClientsCampaign->query($sql2);

			$sql3 = "delete from programs_campaigns where campaign_id=".$id;
			$this->ProgramsCampaign->query($sql3);

			$sql4 = "delete from custom_fields where campaign_id=".$id;
			$this->ProgramsCampaign->query($sql4);

			$sql5 = "delete from cf_contents where campaign_id=".$id;
			$this->ProgramsCampaign->query($sql5);

			$sql6 = "delete from campaign_winners where campaign_id=".$id;
			$this->CampaignEntry->query($sql6);

			$sql7 = "delete from campaigns where id=".$id;
			$this->Campaign->query($sql7);

			$this->Session->setFlash('Competition #'.$id.' deleted');
			$this->redirect(array('action'=>'list_campaign'), null, true);
		}
	}

	function admin_list_campaign()
	{
		$sql = "SELECT * from campaigns as Campaign";
		$campaigns = $this->Campaign->query($sql);
		$this->paginate = array('Campaign' => array('limit' => 10));
		$this->set('campaigns', $this->paginate('Campaign'));

		$this->loadModel("Program");
		$plist = $this->Program->find('list');
		$this->set('programs_list',$plist);

		$check_winners="SELECT campaign_id, COUNT(*) as total FROM campaign_winners group by campaign_id";
	    $winner_counts = $this->CampaignWinner->query($check_winners);
	    $winners = array();
	    foreach($winner_counts as $winner)
	    	$winners[$winner['campaign_winners']['campaign_id']] = $winner[0]['total'];
		$this->set('winner_counts',$winners);

		$check_participants = "SELECT CampaignEntry.campaign_id, COUNT(*) as total FROM campaign_entries as CampaignEntry
		group by CampaignEntry.campaign_id";
		$pcounts = $this->CampaignEntry->query($check_participants);
		$participants = array();

		foreach($pcounts as $count)
		{
			$participants[$count['CampaignEntry']['campaign_id']] = $count[0]['total'];
		}
		$this->set('participant_counts',$participants);


		$clist = $this->Client->find('list');
		$this->set('clients_list',$clist);
	}

	function admin_participants()
	{
		$campaignlist = $this->Campaign->find('list');
		$this->set('campaignlist',$campaignlist);
	}

	function admin_view($id)
	{
		$query  = "SELECT * from campaigns as Campaign where Campaign.id=".$id;
		$details = $this->Campaign->query($query);
		$this->set('details',$details);

		$countQuery = "SELECT Client.name
		FROM campaigns as Campaign, clients_campaigns as ClientsCampaign, clients as Client
		WHERE ClientsCampaign.client_id=Client.id and Campaign.id=ClientsCampaign.campaign_id and Campaign.id=".$id;

		$client_list = $this->Campaign->query($countQuery);

		$clientsList = "";
		foreach($client_list as $cl)
		{
			$clientsList .= strlen($clientsList)?", ":"";
			$clientsList .=$cl['Client']['name'];
		}

		$this->set('clientsList',$clientsList);

		$programQuery = "SELECT Program.name
		FROM campaigns as Campaign, programs_campaigns as ProgramsCampaign, programs as Program
		WHERE ProgramsCampaign.program_id=Program.id and Campaign.id=ProgramsCampaign.campaign_id and Campaign.id=".$id;

		$program_list = $this->Campaign->query($programQuery);

		$programsList = "";
		foreach($program_list as $pl)
		{
			$programsList .= strlen($programsList)?", ":"";
			$programsList .=$pl['Program']['name'];
		}

		$this->set('programsList',$programsList);
		$this->set('campaignid',$id);

		$winner_counts = $this->CampaignWinner->find('count',array('conditions'=>array('CampaignWinner.campaign_id'=>$id)));
		$this->set('wcount',$winner_counts);

		$pcounts = $this->CampaignEntry->find('count',array('conditions'=>array('CampaignEntry.campaign_id'=>$id)));
		$this->set('pcount',$pcounts);
	}

	//Draw functionality

	function admin_manual_draw($id)
	{
		$sql = "select * from campaigns as Campaign where Campaign.id=".$id;
		$list = $this->Campaign->query($sql);
		$max_winners=$list[0]['Campaign']['max_winners'];
		$campaign_start_date=$list[0]['Campaign']['start_date'];
		$campaign_end_date=$list[0]['Campaign']['end_date'];
		$login_start=$list[0]['Campaign']['login_start_date'];
		$login_end=$list[0]['Campaign']['login_end_date'];


		$check_winners="SELECT COUNT(CampaignWinner.winner_id) FROM campaign_winners as CampaignWinner WHERE CampaignWinner.campaign_id=$id";
		$winner_counts = $this->CampaignWinner->query($check_winners);

		foreach ($winner_counts as $winner_count)
		{
			$count_winner=$winner_count[0]['COUNT(CampaignWinner.winner_id)'];

			$difference=$max_winners-$count_winner;

			$draw_count = 0;
			if($difference > $list[0]['Campaign']['winners_per_draw'])
			{
				$draw_count = $list[0]['Campaign']['winners_per_draw'];
			}
			else if(($difference <= $list[0]['Campaign']['winners_per_draw'] ) && ($difference > 0))
			{
				$draw_count = $difference;
			}

			if($list[0]['Campaign']['campaign_type'] == 'M')
			{
				$draw="SELECT * FROM campaigns as Campaign,campaign_entries as CampaignEntry
				WHERE Campaign.id=CampaignEntry.campaign_id and Campaign.id =".$id." and CampaignEntry.user_id not in
				(select winner_id from campaign_winners where campaign_id=".$id.") ORDER by RAND() LIMIT ".$draw_count;
				$draw_winners = $this->CampaignEntry->query($draw);

				if(!empty($draw_winners))
				{
					foreach ($draw_winners as $winner)
					{
						$insert="INSERT INTO campaign_winners (campaign_id,campaign_type,winner_id,winner_name,winner_email,winner_client_id,win_date)
						VALUES (".$winner['Campaign']['id'].",'".$winner['Campaign']['campaign_type']."',".$winner['CampaignEntry']['user_id'].",
						'".addslashes($winner['CampaignEntry']['user_name'])."','".$winner['CampaignEntry']['email']."',
						".$winner['CampaignEntry']['client_id'].",'".date('Y-m-d h:i:s')."')";

						$this->CampaignWinner->query($insert);

						$sql ="SELECT * from campaigns as Campaign, clients_campaigns as ClientsCampaign, clients as Client
						where Campaign.id=ClientsCampaign.campaign_id and ClientsCampaign.client_id=Client.id and
						Campaign.id=".$winner['Campaign']['id'];

						$winnerMail = $this->Campaign->query($sql);

						if($winner['Campaign']['notification_email'] == 1 && ($winner['CampaignEntry']['email']!='' || !empty($winner['CampaignEntry']['email'])))
						{
							$subject = $winner['Campaign']['subject'];
							$name = $winner['CampaignEntry']['first_name']." ".$winner['CampaignEntry']['last_name'];
							$member_name = str_replace('[[member_name]]',$name,$winnerMail[0]['Campaign']['notification_message']);
							$body = str_replace('[[client_name]]',$winnerMail[0]['Client']['name'],$member_name);

							$message = '';

							if($winnerMail[0]['Campaign']['winner_message'] == 'D')
							{
								$message.=$body;
							}
							else if($winnerMail[0]['Campaign']['winner_message'] == 'F')
							{
								$path = "http://www.myrewards.com.au/files/campaign/notification_image/".$winnerMail[0]['Campaign']['id'].".".$winnerMail[0]['Campaign']['notification_image_ext'];

								$message.="Hello ".$name."!<br>";
								$message.="<p><img src=".$path."/></p>";
								$message.="<p>Regards,</p><p>Team ".$winnerMail[0]['Client']['name']."</p>";
							}

							$headers  = "From: support@therewardsteam.com\r\n";
							$headers .= "Content-type: text/html\r\n";
							$mail = mail($winner['CampaignEntry']['email'], $subject, $message, $headers);
						}
					}
					$this->Session->setFlash('The Competition is drawn successfully ');
					$this->redirect(array('action'=>'list_campaign'), null, true);
				}
				else
				{
					$this->Session->setFlash('There are no new participants to draw ');
					$this->redirect(array('action'=>'list_campaign'), null, true);
				}
			}
			else if($list[0]['Campaign']['campaign_type'] == 'A')
			{
				$draw="SELECT * FROM campaigns as Campaign,campaign_entries as CampaignEntry
				WHERE Campaign.id=CampaignEntry.campaign_id and Campaign.id =".$id." and CampaignEntry.user_id not in
				(select winner_id from campaign_winners where campaign_id=".$id.") ORDER by RAND() LIMIT ".$draw_count;

				$draw_winners = $this->CampaignEntry->query($draw);

				if(!empty($draw_winners))
				{
					foreach ($draw_winners as $winner)
					{
						$insert="INSERT INTO campaign_winners (campaign_id,campaign_type,winner_id,winner_name,winner_email,winner_client_id,
						win_date) VALUES (".$winner['Campaign']['id'].",'".$winner['Campaign']['campaign_type']."',".$winner['CampaignEntry']['user_id'].",
						'".addslashes($winner['CampaignEntry']['user_name'])."','".$winner['CampaignEntry']['email']."',".$winner['CampaignEntry']['client_id'].",
						'".date('Y-m-d h:i:s')."')";

						$this->CampaignWinner->query($insert);

						$sql ="SELECT * from campaigns as Campaign, clients_campaigns as ClientsCampaign, clients as Client
						where Campaign.id=ClientsCampaign.campaign_id and ClientsCampaign.client_id=Client.id and
						Campaign.id=".$winner['Campaign']['id'];

						$winnerMail = $this->Campaign->query($sql);

						if($winner['Campaign']['notification_email'] == 1 && ($winner['CampaignEntry']['email']!='' || !empty($winner['CampaignEntry']['email'])))
						{
							$subject = $winner['Campaign']['subject'];
							$name = $winner['CampaignEntry']['first_name']." ".$winner['CampaignEntry']['last_name'];
							$member_name = str_replace('[[member_name]]',$name,$winnerMail[0]['Campaign']['notification_message']);
							$body = str_replace('[[client_name]]',$winnerMail[0]['Client']['name'],$member_name);

							$message = '';
							if($winnerMail[0]['Campaign']['winner_message'] == 'D')
							{
								$message.=$body;
							}
							else if($winnerMail[0]['Campaign']['winner_message'] == 'F')
							{
								$path = "http://www.myrewards.com.au/files/campaign/notification_image/".$winnerMail[0]['Campaign']['id'].".".$winnerMail[0]['Campaign']['notification_image_ext'];

								$message.="Hello ".$name."!<br>";
								$message.="<p><img src=".$path."/></p>";
								$message.="<p>Regards,</p><p>Team ".$winnerMail[0]['Client']['name']."</p>";
							}

							$headers  = "From: support@therewardsteam.com\r\n";
							$headers .= "Content-type: text/html\r\n";
							$mail = mail($winner['CampaignEntry']['email'], $subject, $message, $headers);
					   }
					}
					$this->Session->setFlash('The Competition is drawn successfully ');
					$this->redirect(array('action'=>'list_campaign'), null, true);
				}
				else
				{
					$this->Session->setFlash('There are no new users to draw ');
					$this->redirect(array('action'=>'list_campaign'), null, true);
				}
			}
		}
		$this->autoRender=false;
		}

		function auto_draw()
		{
			$listQuery = "select * from campaigns as Campaign where Campaign.active=1 and Campaign.draw_start_date<=current_date
			and Campaign.draw_end_date>=current_date and (Campaign.week_days='' or locate(weekday(current_date),Campaign.week_days)>0)
			and Campaign.draw_type='AD'";
			$lists = $this->Campaign->query($listQuery);

			foreach ($lists as $list)
			{
				$campaign_type=$list['Campaign']['campaign_type'];
				$campaign_id=$list['Campaign']['id'];
				$max_winners=$list['Campaign']['max_winners'];
				$winners_per_draw=$list['Campaign']['winners_per_draw'];

				$campaign_start_date=$list['Campaign']['start_date'];
				$campaign_end_date=$list['Campaign']['end_date'];
				$login_start=$list['Campaign']['login_start_date'];
				$login_end=$list['Campaign']['login_end_date'];

				$check_winners="SELECT COUNT(CampaignWinner.winner_id) FROM campaign_winners as CampaignWinner
				WHERE CampaignWinner.campaign_id=$campaign_id";
				$winner_counts = $this->CampaignWinner->query($check_winners);

				foreach ($winner_counts as $winner_count)
				{
					$count_winner=$winner_count[0]['COUNT(CampaignWinner.winner_id)'];
					$difference=$max_winners-$count_winner;
					$draw_count = 0;
					if($difference > $winners_per_draw)
					{
						$draw_count = $winners_per_draw;
					}
					else if(($difference <= $winners_per_draw ) && ($difference > 0))
					{
						$draw_count = $difference;
					}

					$draw_winners= array();

					if($campaign_type== 'A')
					{
						$sql="SELECT * FROM campaigns as Campaign,campaign_entries as CampaignEntry
						WHERE Campaign.id=CampaignEntry.campaign_id and Campaign.id =".$campaign_id."
						and CampaignEntry.user_id not in (select winner_id from campaign_winners where campaign_id=".$campaign_id.")
						ORDER by RAND() LIMIT ".$draw_count;

						$draw_winners = $this->Campaign->query($sql);
						foreach ($draw_winners as $winner)
						{
							$insert="INSERT INTO campaign_winners (campaign_id,campaign_type,winner_id,winner_name,winner_email,winner_client_id,
							win_date) VALUES (".$winner['Campaign']['id'].",'".$winner['Campaign']['campaign_type']."',".$winner['CampaignEntry']['user_id'].",
							'".addslashes($winner['CampaignEntry']['user_name'])."','".$winner['CampaignEntry']['email']."', ".$winner['CampaignEntry']['client_id'].",
							'".date('Y-m-d h:i:s')."')";

							$this->CampaignWinner->query($insert);

							$sql ="SELECT * from campaigns as Campaign, clients_campaigns as ClientsCampaign, clients as Client
							where Campaign.id=ClientsCampaign.campaign_id and ClientsCampaign.client_id=Client.id and Campaign.id=".$winner['Campaign']['id'];
							$winnerMail = $this->Campaign->query($sql);

							if($winner['Campaign']['notification_email'] == 1 && ($winner['CampaignEntry']['email']!='' || !empty($winner['CampaignEntry']['email'])))
							{
								$subject = $winner['Campaign']['subject'];
								$name = $winner['CampaignEntry']['first_name']." ".$winner['CampaignEntry']['last_name'];
								$member_name = str_replace('[[member_name]]',$name,$winnerMail[0]['Campaign']['notification_message']);
								$body = str_replace('[[client_name]]',$winnerMail[0]['Client']['name'],$member_name);

								$message = '';
								if($winnerMail[0]['Campaign']['winner_message'] == 'D')
								{
									$message.=$body;
								}
								else if($winnerMail[0]['Campaign']['winner_message'] == 'F')
								{
									$path = "http://www.myrewards.com.au/files/campaign/notification_image/".$winnerMail[0]['Campaign']['id'].".".$winnerMail[0]['Campaign']['notification_image_ext'];

									$message.="Hello ".$name."!<br>";
									$message.="<p><img src=".$path."/></p>";
									$message.="<p>Regards,</p><p>Team ".$winnerMail[0]['Client']['name']."</p>";
								}

								$headers  = "From: support@therewardsteam.com\r\n";
								$headers .= "Content-type: text/html\r\n";
								$mail = mail($winner['CampaignEntry']['email'], $subject, $message, $headers);
							}
						}
					}
					else if($campaign_type== 'M' && $list['Campaign']['manual_winner_type'] == 'default')
					{
						$sql="SELECT * FROM campaigns as Campaign,campaign_entries as CampaignEntry
						WHERE Campaign.id=CampaignEntry.campaign_id and Campaign.id =".$campaign_id."
						and CampaignEntry.user_id not in (select winner_id from campaign_winners where campaign_id=".$campaign_id.")
						ORDER by RAND() LIMIT ".$draw_count;

						$draw_winners = $this->Campaign->query($sql);
						foreach ($draw_winners as $winner)
						{
							$insert="INSERT INTO campaign_winners (campaign_id,campaign_type,winner_id,winner_name,winner_email,winner_client_id,
							win_date) VALUES (".$winner['Campaign']['id'].",'".$winner['Campaign']['campaign_type']."',
							".$winner['CampaignEntry']['user_id'].",'".addslashes($winner['CampaignEntry']['user_name'])."',
							'".$winner['CampaignEntry']['email']."',".$winner['CampaignEntry']['client_id'].",'".date('Y-m-d h:i:s')."')";

							$this->CampaignWinner->query($insert);

							$sql ="SELECT * from campaigns as Campaign, clients_campaigns as ClientsCampaign, clients as Client
							where Campaign.id=ClientsCampaign.campaign_id and ClientsCampaign.client_id=Client.id and Campaign.id=".$winner['Campaign']['id'];
							$winnerMail = $this->Campaign->query($sql);

							if($winner['Campaign']['notification_email'] == 1 && ($winner['CampaignEntry']['email']!='' || !empty($winner['CampaignEntry']['email'])))
							{
								$subject = $winner['Campaign']['subject'];
								$name = $winner['CampaignEntry']['first_name']." ".$winner['CampaignEntry']['last_name'];
								$member_name = str_replace('[[member_name]]',$name,$winnerMail[0]['Campaign']['notification_message']);
								$body = str_replace('[[client_name]]',$winnerMail[0]['Client']['name'],$member_name);

								$message = '';

								if($winnerMail[0]['Campaign']['winner_message'] == 'D')
								{
									$message.=$body;
								}
								else if($winnerMail[0]['Campaign']['winner_message'] == 'F')
								{
									$path = "http://www.myrewards.com.au/files/campaign/notification_image/".$winnerMail[0]['Campaign']['id'].".".$winnerMail[0]['Campaign']['notification_image_ext'];

									$message.="Hello ".$name."!<br>";
									$message.="<p><img src=".$path."/></p>";
									$message.="<p>Regards,</p><p>Team ".$winnerMail[0]['Client']['name']."</p>";
								}

								$headers  = "From: support@therewardsteam.com\r\n";
								$headers .= "Content-type: text/html\r\n";
								$mail = mail($winner['CampaignEntry']['email'], $subject, $message, $headers);
							}
						}
					}
				}
			}
			$this->autoRender=false;
	}

	function admin_view_winners($id)
	{
		$query = " WHERE Campaign.id=CampaignWinner.campaign_id and CampaignWinner.campaign_id= CampaignEntry.campaign_id
		and CampaignWinner.winner_id= CampaignEntry.user_id and CampaignWinner.campaign_id=$id";

		$this->paginate = array('CampaignWinner' => array('limit' => 20));
		$winners = $this->paginate('CampaignWinner', array('search' =>array('sql' => $query)));
		$this->set('winners',$winners);
		$this->set('campaignid',$id);
	}


	function admin_view_pages()
	{
	$campaigns = $this->Campaign->find('all');
	$this->set('campaigns', $this->paginate('Campaign'));
	}

	function campaign_active()
	{
		$updateSql = "update campaigns as Campaign set Campaign.active=1 where Campaign.active=0 and Campaign.manual_flag=0
		and Campaign.start_date<=current_date and Campaign.end_date>=current_date";
		$update = $this->Campaign->query($updateSql);
		$this->autoRender=false;
	}

	function campaign_pause()
	{
		$updateSql = "update campaigns as Campaign set Campaign.active=0 where Campaign.active=1 and Campaign.end_date<current_date";
		$update = $this->Campaign->query($updateSql);
		$this->autoRender=false;
	}

	/*function checkwinner($id)
    {
	   	$check_winners="SELECT campaign_id FROM campaign_winners WHERE flag = 0 AND winner_id=".$id;
	    $winner_counts = $this->CampaignWinner->query($check_winners);
	    foreach($winner_counts as $winner)
	    {
	    	$sql = "SELECT * FROM campaigns WHERE id=".$winner['campaign_winners']['campaign_id'];
			$campaigns = $this->Campaign->query($sql);
			if($campaigns[0]['campaigns']['notification_popup']==1)
			{
				echo "<table><tr><td style='color:#01B7FF;font-size:14px'>".$campaigns[0]['campaigns']['name']."</td></tr>";
				if($campaigns[0]['campaigns']['winner_message']=='F')
					echo "<tr><td><img src='".CAMPAIGN_IMAGE_PATH."notification_image\\".$campaigns[0]['campaigns']['id'].".".$campaigns[0]['campaigns']['notification_image_ext']."'></td></tr>";
				else
					echo "<tr><td>".$campaigns[0]['campaigns']['notification_message']."</td></tr>";

				if($campaigns[0]['campaigns']['campaign_type']=='A')
					echo "<tr><td><a href='#more' onClick='campaignMoreDetails()'>Enter More Details</a></td></tr>";
				echo "</table>";
			}
			$updateSql = "UPDATE campaign_winners SET flag=1 WHERE campaign_id=".$winner['campaign_winners']['campaign_id']." AND winner_id=".$id;
			$update = $this->CampaignWinner->query($updateSql);
	    }
	    $this->autoRender=false;
    }*/

    function checkwinner($id)
	{
		$check_winners="SELECT campaign_id FROM campaign_winners WHERE flag = 0 AND winner_id=".$id." order by rand() limit 1";
		$winner_counts = $this->CampaignWinner->query($check_winners);

		$sql = "SELECT * FROM campaigns as Campaign WHERE Campaign.id=".$winner_counts[0]['campaign_winners']['campaign_id'];
		$campaigns = $this->Campaign->query($sql);
		$this->set('campaigns',$campaigns);

		$sql1 = "SELECT CampaignsDashboard.dashboard_id from campaigns Campaign, campaigns_dashboards as CampaignsDashboard
				WHERE  Campaign.active = 1 and Campaign.id = CampaignsDashboard.campaign_id and
				CampaignsDashboard.campaign_id=".$winner_counts[0]['campaign_winners']['campaign_id'];

		$dashboards = $this->Campaign->query($sql1);
		$dashArray = array();
		foreach($dashboards as $dash)
		{
			array_push($dashArray, $dash['CampaignsDashboard']['dashboard_id']);
		}
		$this->set('dashArray',$dashArray);

		$this->layout = 'ajax';
    }

    function admin_download_participants($id)
    {
    	$this->layout = "blank";
    	$sql = "SELECT * from campaigns as Campaign, campaign_entries as CampaignEntry, clients as Client
    	WHERE Campaign.id=CampaignEntry.campaign_id and Client.id=CampaignEntry.client_id and Campaign.id=".$id;
    	$reports = $this->Campaign->query($sql);

    	foreach ($reports as $key => $report)
	    {
        	$campaign_id= $report['Campaign']['id'];
            $campaign_name=$report['Campaign']['name'];
            $client_name= $report['Client']['name'];
            $user_id= $report['CampaignEntry']['user_id'];
            $user_name= $report['CampaignEntry']['user_name'];
            $user_email= $report['CampaignEntry']['email'];

            $this->Client->recursive = -1;
			$reports[$key]['Export'] = array(
                           	'Campaign Id'   =>  $campaign_id,
                           	'Campaign Name' =>  $campaign_name,
                           	'Client Name'    =>  $client_name,
                           	'User Id'  =>  $user_id,
							'User Name'  =>  $user_name,
							'User Email'  =>  $user_email
							);
		}
    	$this->set('reports',$reports);
    	$this->set('name','participants_list');
    }

    function participants_all_users()
    {
   		$sql = "SELECT * from campaigns as Campaign	WHERE Campaign.start_date<= current_date and Campaign.end_date>=current_date
   		and Campaign.active=1 ";
		$list = $this->Campaign->query($sql);

		if(!empty($list))
		{
			foreach($list as $lists)
			{
				if($lists['Campaign']['campaign_type'] == 'A' && $lists['Campaign']['user_type'] =='AU')
				{
					$sql = "SELECT User.* from users as User, clients_campaigns as ClientsCampaign WHERE ClientsCampaign.client_id = User.client_id
					and ClientsCampaign.campaign_id=".$lists['Campaign']['id']." and User.registered=1";
					$checkUser = $this->User->query($sql);

					if(!empty($checkUser))
					{
						foreach($checkUser as $user)
						{
							$sql1 = "SELECT CampaignEntry.id from campaign_entries as CampaignEntry
							where CampaignEntry.campaign_id=".$lists['Campaign']['id']." and CampaignEntry.user_id =".$user['User']['id'];
							$user_exist = $this->CampaignEntry->query($sql1);

							if(empty($user_exist))
							{
								$joindate = date('Y-m-d h:i:s');
								$sql = "INSERT into campaign_entries (user_id, user_name, client_id, campaign_id, campaign_type, first_name,
								last_name,join_date,email) VALUES (".$user['User']['id'].", '".$user['User']['username']."', ".$user['User']['client_id'].",
								".$lists['Campaign']['id'].", '".$lists['Campaign']['campaign_type']."', '".$user['User']['first_name']."',
								'".$user['User']['last_name']."', '".$joindate."','".$user['User']['email']."') ";
								$this->CampaignEntry->query($sql);
							}
						}
					}
				}
			}
		}
		$this->autoRender=false;
    }

    function admin_choose_winners_list($list1,$id)
	{
		if(!empty($list1))
		{
			$sql = "SELECT * from campaign_entries as CampaignEntry, campaigns as Campaign, clients as Client
			where CampaignEntry.campaign_id = Campaign.id and Campaign.id=".$id." and CampaignEntry.client_id = Client.id
			and CampaignEntry.user_id IN (".$list1.") and CampaignEntry.user_id not IN
			(select winner_id from campaign_winners where winner_id in (".$list1.") and campaign_id=".$id.")";

			$winners_list = $this->Campaign->query($sql);
			foreach($winners_list as $list)
			{
				$sql = "INSERT into campaign_winners (campaign_id, winner_id, winner_name, winner_email, winner_client_id, campaign_type,
				win_date) values(".$id.", ".$list['CampaignEntry']['user_id'].", '".$list['CampaignEntry']['user_name']."',
				'".$list['CampaignEntry']['email']."', ".$list['CampaignEntry']['client_id'].", '".$list['CampaignEntry']['campaign_type']."',
				'".date('Y-m-d h:i:s')."')";

				$this->CampaignWinner->query($sql);

				if($list['Campaign']['notification_email'] == 1 && ($list['CampaignEntry']['email']!='' || !empty($list['CampaignEntry']['email'])))
				{
					$subject = $list['Campaign']['subject'];
					$name = $list['CampaignEntry']['first_name']." ".$list['CampaignEntry']['last_name'];
					$member_name = str_replace('[[member_name]]',$name,$list['Campaign']['notification_message']);
					$body = str_replace('[[client_name]]',$list['Client']['name'],$member_name);

					$message = '';

					if($list['Campaign']['winner_message'] == 'D')
					{
						$message.=$body."<br>";
					}
					else if($list['Campaign']['winner_message'] == 'F')
					{
						$path = "http://www.myrewards.com.au/files/campaign/notification_image/".$list['Campaign']['id'].".".$list['Campaign']['notification_image_ext'];

						$message.="Hello ".$name."!<br>";
						$message.="<p><img src=".$path."/></p>";
						$message.="<p>Regards,</p><p>Team ".$list['Client']['name']."</p>";
					}

					$headers  = "From: support@therewardsteam.com\r\n";
					$headers .= "Content-type: text/html\r\n";
					$mail = mail($list['CampaignEntry']['email'], $subject, $message, $headers);
				}

			}
			$this->redirect('view_winners/'.$id);
		}
    }

	function admin_download_winners($id)
    {
    	$this->layout = "blank";
    	$sql = "SELECT * from campaigns as Campaign, campaign_winners as CampaignWinner, campaign_entries as CampaignEntry, clients as Client
    	WHERE Campaign.id=CampaignWinner.campaign_id and Client.id=CampaignWinner.winner_client_id
    	and CampaignWinner.campaign_id= CampaignEntry.campaign_id and CampaignWinner.winner_id= CampaignEntry.user_id and Campaign.id=".$id;
    	$reports = $this->Campaign->query($sql);

    	foreach ($reports as $key => $report)
	    {
        	$campaign_id= $report['Campaign']['id'];
            $campaign_name=$report['Campaign']['name'];
            $client_name= $report['Client']['name'];
            $user_name= $report['CampaignWinner']['winner_name'];
            $first_name= $report['CampaignEntry']['first_name'];
            $last_name= $report['CampaignEntry']['last_name'];
            $user_email= $report['CampaignWinner']['winner_email'];
            $win_date= $report['CampaignWinner']['win_date'];

            $this->Client->recursive = -1;
			$reports[$key]['Export'] = array(
                           	'Campaign Id'   =>  $campaign_id,
                           	'Campaign Name' =>  $campaign_name,
                           	'Client Name'    =>  $client_name,
							'User Name'  =>  $user_name,
							'First Name'  =>  $first_name,
							'Last Name'  =>  $last_name,
							'User Email'  =>  $user_email,
							'Win date'  =>  $win_date
							);
		}
    	$this->set('reports',$reports);
    	$this->set('name','winners_list');
    }
}