<?php
class MerchantsController extends AppController {

	var $components = array('Csv','File');
	var $uses = array('Merchant','Merchantlist', 'MerchantAddressesImport', 'MerchantProductsImport','Category','MerchantsImport','MerchantAddressesDelete','ProductsAddress');

	function admin_index() {

		$conditions = array();
		if (isset($this->params['url']['data'])) {
			$this->Session->write($this->name.'.search_index', $this->params['url']['data']);
		}

		if ($this->Session->check($this->name.'.search_index')) {
			$this->request->data = $this->Session->read($this->name.'.search_index');
			if  (!empty($this->request->data['Merchant']['search'])) {
				$conditions[] = "
					(`Merchant`.`name` LIKE  '%". $this->request->data['Merchant']['search']. "%'
					OR `Merchant`.`id` LIKE  '%". $this->request->data['Merchant']['search']. "%')
				";
			}
		}

		$this->Merchant->recursive = 0;
		$this->set('merchants', $this->paginate('Merchant',$conditions));

	}

	function admin_edit($id = null) {

		//	cleare saved searches
		$this->Session->delete($this->name.'.search');
		$this->Session->delete($this->name.'.search_url');

		if (!empty($this->request->data)) {

			//	get the image extensions
			if ($this->request->data['MerchantImage']['file']['name'] != '') {
				$extension = substr($this->request->data['MerchantImage']['file']['name'], strrpos($this->request->data['MerchantImage']['file']['name'], '.') + 1);
				$this->request->data['Merchant']['image_extension'] = $extension;
			}
			if ($this->request->data['MerchantLogo']['file']['name'] != '') {
				$extension = substr($this->request->data['MerchantLogo']['file']['name'], strrpos($this->request->data['MerchantLogo']['file']['name'], '.') + 1);
				$this->request->data['Merchant']['logo_extension'] = $extension;
			}

			//	save the product if submitted
			if (isset($this->request->data['Product'])) {
				$this->admin_ajax_product($this->request->data['Product']['id']);
			}

			if ($this->Merchant->save($this->request->data)) {

				$mid = $this->Merchant->MerchantAddress->findByMerchantId($id);
				if(empty($mid))
				{
					$this->request->data['MerchantAddress']['merchant_id'] = $this->Merchant->id;
					$this->request->data['MerchantAddress']['name'] = $this->request->data['Merchant']['name'];
					$this->request->data['MerchantAddress']['country'] =$this->request->data['Merchant']['mail_country'];
					$this->request->data['MerchantAddress']['address1'] = $this->request->data['Merchant']['mail_address1'];
					$this->request->data['MerchantAddress']['address2'] = $this->request->data['Merchant']['mail_address2'];
					$this->request->data['MerchantAddress']['state'] = $this->request->data['Merchant']['mail_state'];
					$this->request->data['MerchantAddress']['suburb'] = $this->request->data['Merchant']['mail_suburb'];
					$this->request->data['MerchantAddress']['postcode'] = $this->request->data['Merchant']['mail_postcode'];
					$this->request->data['MerchantAddress']['phone'] = $this->request->data['Merchant']['phone'];
					$this->Merchant->MerchantAddress->save($this->request->data);
				}
				// delete unwanted images of merchant image
				if (isset($this->request->data['MerchantImageDelete'])) {
					$delete_images = array();
					foreach ($this->request->data['MerchantImageDelete'] as $key => $value) {
						if ($value == '1') {
							$this->File->deleteByPrefix('merchant_image/'.$this->Merchant->id);
						}
					}
				}

				// delete unwanted images of merchant logo
				if (isset($this->request->data['MerchantLogoImageDelete'])) {
					$delete_images = array();
					foreach ($this->request->data['MerchantLogoImageDelete'] as $key => $value) {
						if ($value == '1') {
							$this->File->deleteByPrefix('merchant_logo/'.$this->Merchant->id);
						}
					}
				}

				//	save the uploaded files
				if ($this->request->data['MerchantImage']['file']['name'] != '') {
					move_uploaded_file($this->request->data['MerchantImage']['file']['tmp_name'], MERCHANT_IMAGE_PATH. $this->Merchant->id. '.'. $this->request->data['Merchant']['image_extension']);
				}

				if ($this->request->data['MerchantLogo']['file']['name'] != '') {
					move_uploaded_file($this->request->data['MerchantLogo']['file']['tmp_name'], MERCHANT_LOGO_PATH. $this->Merchant->id. '.'. $this->request->data['Merchant']['logo_extension']);
				}

				//	update addresses
				if (isset($this->request->data['MerchantAddress'])) {
					foreach($this->request->data['MerchantAddress'] as $key => $address) {
						if (isset($this->request->data['PrimaryMerchantAddress']['id']) && $this->request->data['PrimaryMerchantAddress']['id'] == $address['id']) {
							$address['primary'] = 1;
							$this->request->data['MerchantAddress'][$key]['primary'] = 1;
						} else {
							$address['primary'] = 0;
							$this->request->data['MerchantAddress'][$key]['primary'] = 0;
						}

						if (substr($address['id'], 0, 3) == 'new') {
							$address['id'] = '';
						}

						if(!empty($this->request->data['MerchantAddress'][$key]['id']))
						{

							$addressnew=$this->Merchant->MerchantAddress->find('all',array('conditions'=>array('MerchantAddress.id' => $this->request->data['MerchantAddress'][$key]['id'])));

							$country= $addressnew[0]['MerchantAddress']['country'];
							$state1=$addressnew[0]['MerchantAddress']['state'];
							$suburb=$addressnew[0]['MerchantAddress']['suburb'];

							$address2=strip_tags($addressnew[0]['MerchantAddress']['address2']);
							$address22=preg_replace("/[^a-zA-Z0-9s]/", " ", $address2);
							$address1=strip_tags($addressnew[0]['MerchantAddress']['address1']);

							$address11=preg_replace("/[^a-zA-Z0-9s]/", " ", $address1);
							$lat = $addressnew[0]['MerchantAddress']['latitude'];
							$long = $addressnew[0]['MerchantAddress']['longitude'];

							if (substr($this->request->data['MerchantAddress'][$key]['id'], 0, 3) == 'new') {
								$country= $this->request->data['MerchantAddress'][$key]['country'];
								$state1=$this->request->data['MerchantAddress'][$key]['state'];
								$suburb=$this->request->data['MerchantAddress'][$key]['suburb'];

								$address2=strip_tags($this->request->data['MerchantAddress'][$key]['address2']);
								$address22=preg_replace("/[^a-zA-Z0-9s]/", " ", $address2);
								$address1=strip_tags($this->request->data['MerchantAddress'][$key]['address1']);

								$address11=preg_replace("/[^a-zA-Z0-9s]/", " ", $address1);
							}

							$mlat = $this->request->data['MerchantAddress'][$key]['latitude'];
							$mlong = $this->request->data['MerchantAddress'][$key]['longitude'];

							if(($lat=='' || $lat==null) || ($long=='' || $long==null))
							{
								if((($mlat !='' || $mlat!=null || !empty($mlat) && $mlat !='0.0000000')) || ( ($mlong !='' || $mlong !=null || !empty($mlong) && $mlong !='0.0000000') ))
								{
									$address['latitude']= $mlat;
									$address['longitude']= $mlong;
								}
								else
								{

									$state_name_cd = mysql_query("select `state_name` from `states` where `state_cd`='".$state1."'");
									$state_name = mysql_fetch_array($state_name_cd);
									$state = $state_name['state_name'];

									$addressfinal = $address11.'+'.$address22.'+'.$suburb.'+'.$state.'+'.$country;
									 $finalURL = 'http://maps.google.com/maps/api/geocode/json?address='.str_replace(" ","+",$addressfinal).'&sensor=false';

									$geocode=file_get_contents($finalURL);

									$output= json_decode($geocode);

									$latitude = $output->results[0]->geometry->location->lat;
									$longitude = $output->results[0]->geometry->location->lng;

									if ($latitude==''||$longitude=='')
									{
										$addressfinal =$suburb.'+'.$state.'+'.$country;

										$finalURL = 'http://maps.google.com/maps/api/geocode/json?address='.str_replace(" ","+",$addressfinal).'&sensor=false';

										$geocode=file_get_contents($finalURL);

										$output= json_decode($geocode);

										$latitude = $output->results[0]->geometry->location->lat;
										$longitude = $output->results[0]->geometry->location->lng;
									}
								   	$address['latitude']= $latitude;
									$address['longitude']= $longitude;
								}
							}
							if(isset($this->request->data[MerchantAddress][$key]['edit']))
							{
								if((($mlat =='' || empty($mlat)) && ($latitude=='' || empty($latitude))) || (($mlong =='' || empty($mlong)) && ($longitude=='' || empty($longitude))))
								{
									$address['latitude']= null;
									$address['longitude']= null;
								}
							}
							$address['merchant_id'] = $this->Merchant->id;
							$this->Merchant->MerchantAddress->save(array('MerchantAddress' => $address));
						}
					}
				}

				//	remove deleted addresses
				if (isset($this->request->data['RemoveAddress'])) {
					foreach($this->request->data['RemoveAddress'] as $address_id) {
						$this->Merchant->MerchantAddress->delete($address_id);
					}
				}

				//	log the change
				$log = array(
					'id' => null,
					'foreign_id' => $this->{Inflector::singularize($this->name)}->id,
					'foreign_model' => Inflector::singularize($this->name),
					'user_id' => $this->Session->read('user.User.id'),
					'old_data' => $old_data,
					'new_data' => $this->request->data,
					'notes' => ''
				);
				$log['description'] = Inflector::humanize(Inflector::underscore($this->name)). ' modified';

				//	add the related changes
				$hasMany = array('MerchantAddress' => 'name');
				foreach ($hasMany as $model => $display)  {

					$old_has_many = array();
					//	reindex old data
					if (isset($old_data[$model])) {
						foreach ($old_data[$model] as $model_data) {
							$old_has_many[$model_data['id']] = $model_data;
						}
					}
					$new_has_many = array();
					//	reindex new data
					if (isset($this->request->data[$model])) {
						foreach ($this->request->data[$model] as $model_data) {
							$new_has_many[$model_data['id']] = $model_data;
						}
					}

					//	deleted
					foreach ($old_has_many as $model_id => $model_data) {
						if (!in_array($model_id, array_keys($new_has_many))) {
							$log['old_data'][$model][$model. ' '. $model_data['id']. ' '. $model_data['name']] = $model_data['name']. ' '. $model_data['address1']. ' '. $model_data['address2'].
									' '. $model_data['suburb']. ' '. $model_data['state']. ' '.
									$model_data['postcode']. ' '. $model_data['phone'];
							$log['new_data'][$model][$model. ' '. $model_data['id']. ' '. $model_data['name']] = 'deleted';
						}
					}

					//	added
					foreach ($new_has_many as $model_id => $model_data) {
						if (!in_array($model_id, array_keys($old_has_many))) {
							$log['old_data'][$model][Inflector::humanize(Inflector::underscore($model)). ' '. $model_data['name']] = 'added';
							$log['new_data'][$model][Inflector::humanize(Inflector::underscore($model)). ' '. $model_data['name']] = $model_data['name']. ' '. $model_data['address1']. ' '. $model_data['address2'].
									' '. $model_data['suburb']. ' '. $model_data['state']. ' '.
									$model_data['postcode']. ' '. $model_data['phone'];
						}
					}

					//	changed
					foreach ($new_has_many as $model_id => $model_data) {
						if (isset($old_has_many[$model_id])) {
							foreach ($model_data as $model_key => $model_value) {
								if ($model_key != 'primary') {
									if (isset($old_has_many[$model_id][$model_key]) && trim($old_has_many[$model_id][$model_key]) != trim($model_value)) {
										$log['new_data'][$model][Inflector::humanize(Inflector::underscore($model)). ' '. $model_data['name']] = $model_data['name']. ' '. $model_data['address1']. ' '. $model_data['address2'].
											' '. $model_data['suburb']. ' '. $model_data['state']. ' '.
											$model_data['postcode']. ' '. $model_data['phone'];
										$log['old_data'][$model][Inflector::humanize(Inflector::underscore($model)). ' '. $model_data['name']] = $old_has_many[$model_id]['name']. ' '.
											$old_has_many[$model_id]['address1']. ' '. $old_has_many[$model_id]['address2'].
											' '. $old_has_many[$model_id]['suburb']. ' '. $old_has_many[$model_id]['state']. ' '.
											$old_has_many[$model_id]['postcode']. ' '. $old_has_many[$model_id]['phone'];
											break;
									}
								}
							}
							//	primary address changed
							if (isset($old_has_many[$model_id]['primary']) && $old_has_many[$model_id]['primary'] != $model_data['primary'] && $model_data['primary'] == '0') {
								$log['new_data'][$model]['Primary address changed'] = $this->request->data['PrimaryMerchantAddress']['id'];
								$log['old_data'][$model]['Primary address changed'] = $model_id;
							}
						}
					}
				}

				$this->save_log($log);

				$this->Session->setFlash('The Merchant has been saved');
				if ($this->request->data['Merchant']['id'] == null) {
					$this->redirect('/admin/merchants/edit/'. $this->Merchant->id);
				} else {
					$this->redirect(array('action'=>'index'), null, true);
				}
			} else {
				$this->Session->setFlash('Please correct the errors below.');
			}
		}
		if (empty($this->request->data)) {
			$this->request->data = $this->Merchant->read(null, $id);
		}

		$ctry= $this->request->data['Merchant']['mail_country'];

		if($ctry=='' || empty($ctry))
		{
			$ctry = 'Australia';
		}

        $statesql ="SELECT State.state_cd, State.state_name FROM states AS State, countries as Country, clients as Client where State.country_cd = Country.id
	   	and Country.name='".$ctry."'";

    	$this->loadModel("State");
		$state = $this->State->query($statesql);

    	$statectry = array();
		foreach ($state as $state1) {
			$statectry[$state1['State']['state_cd']] = $state1['State']['state_name'];
		}

		$this->set('statelist',$statectry);

		$this->loadModel('Country');
		$countries = $this->Country->find('list', array('fields' => array('Country.name','Country.name')));

		$this->set('mail_countries',$countries);
		$this->set('search_weights', $this->Merchant->getEnumValues('search_weight'));
	}

	/*
	 * Populate states using ajax for country in admin
	 */
	function admin_update_state()
    {
    	$selected_country= $this->request->data['Merchant']['mail_country'];

    	$defaultstatesql ="SELECT State.state_cd, State.state_name FROM states AS State, countries as Country, clients as Client where State.country_cd = Country.id
		and Country.name='".$selected_country."'";

    	$this->loadModel("State");
		$cstates = $this->State->query($defaultstatesql);

    	$ctrystate = array();
		foreach ($cstates as $ctrystates) {
			$ctrystate[$ctrystates['State']['state_cd']] = $ctrystates['State']['state_name'];
		}

        $this->set('cstates',$ctrystate);
        $this->layout = 'blank';
    }

    /*
	 * generate merchant addresses states in merchants admin
	 */
	function admin_ajax_states($adid,$country,$state)
    {
    	$selected_country = $country;

    	$defaultstatesql ="SELECT State.state_cd, State.state_name FROM states AS State, countries as Country, clients as Client where State.country_cd = Country.id
		and Country.name='".$selected_country."'";

    	$this->loadModel("State");
		$cstates = $this->State->query($defaultstatesql);

    	$ctrystate = array();
		foreach ($cstates as $ctrystates) {
			$ctrystate[$ctrystates['State']['state_cd']] = $ctrystates['State']['state_name'];
		}
		$this->set('stateid',$state);
		$this->set('adId',$adid);
        $this->set('cstates',$ctrystate);
        $this->layout = 'ajax';
    }


	/**
	 * Return a html list of products for an ajax call
	 *
	 * @param	int	$id	merchant id
	 */
	function admin_ajax_products($id = null) {

		$this->layout = 'ajax';

		$name = 'Product';
		$conditions = array();

		if (isset($this->params['url']['data'])) {
			$this->Session->write($this->name.'.search.'.$name, $this->params['url']['data']);
		}

		$this->Session->write($this->name.'.search_url.'.$name, $_SERVER['REQUEST_URI']);

		if ($this->Session->check($this->name.'.search.'.$name)) {
			$this->request->data = $this->Session->read($this->name.'.search.'.$name);
			if  (!empty($this->request->data[$name]['search'])) {
				$conditions[] = "
					(`". $name. "`.`name` LIKE  '%". $this->request->data[$name]['search']. "%'
					OR `". $name. "`.`id` =  '". $this->request->data[$name]['search']. "')
				";
			}
		}
		$conditions[] = "`". 'Merchant'. "`.`id` = '". $id. "'";

		$this->Merchant->Product->recursive = 0;
		$this->set('products', $this->paginate('Product', $conditions));

		$this->set('id', $id);

	}

	/**
	 * Edit a merchants product
	 *
	 * @param	int	$product_id	id of the product to edit
	*/
	function admin_ajax_product($product_id = null) {

		if (!empty($this->request->data)) {
//			debug($this->request->data); die();
			$this->request->data['Product']['id'] = $product_id;
			if ($this->request->data['Product']['expires']['day'] == '' || $this->request->data['Product']['expires']['month'] == '' || $this->request->data['Product']['expires']['year'] == '') {
				$this->request->data['Product']['expires']['day'] = '00';
				$this->request->data['Product']['expires']['month'] = '00';
				$this->request->data['Product']['expires']['year'] = '0000';
			}
		/*	if (
						(isset($this->request->data['Product']['yearuntil']['day'])   && $this->request->data['Product']['yearuntil']['day'] == '') ||
				 		(isset($this->request->data['Product']['yearuntil']['month']) && $this->request->data['Product']['yearuntil']['month'] == '')
				 )
			{
		*/
				$this->request->data['Product']['yearuntil']['day'] = '31';
				$this->request->data['Product']['yearuntil']['month'] = '12';

			//	get the product image extension
			if ($this->request->data['ProductImage']['file']['name'] != '') {
				$extension = substr($this->request->data['ProductImage']['file']['name'], strrpos($this->request->data['ProductImage']['file']['name'], '.') + 1);
				$this->request->data['Product']['image_extension'] = $extension;
			}

			if ($this->Merchant->Product->save($this->request->data)) {


				// delete unwanted images of product image
				if (isset($this->request->data['ProductImageDelete'])) {
					$delete_images = array();
					foreach ($this->request->data['ProductImageDelete'] as $key => $value) {
						if ($value == '1') {
							$this->File->deleteByPrefix('product_image/'.$product_id);
						}
					}
				}

				//	save the uploaded files
				if ($this->request->data['ProductImage']['file']['name'] != '') {
					move_uploaded_file($this->request->data['ProductImage']['file']['tmp_name'], PRODUCT_IMAGE_PATH. $this->Merchant->Product->id. '.'. $this->request->data['Product']['image_extension']);
				}

				//	save the programs and clients
				if (isset($this->request->data['ClientsProduct'])) {
					$this->Merchant->Product->ClientsProduct->deleteAll("product_id = '". $this->Merchant->Product->id. "' AND client_id IN ('". implode("','", array_keys($this->request->data['ClientsProduct']['id'])). "')");
					foreach ($this->request->data['ClientsProduct']['id'] as $client_id => $enabled) {
						if ($enabled == '1') {
							$this->Merchant->Product->ClientsProduct->save(array('ClientsProduct' => array('id' => null, 'product_id' => $this->Merchant->Product->id, 'client_id' =>  $client_id)));
						}
					}
				}
				if (isset($this->request->data['ProgramsProduct'])) {
					$this->Merchant->Product->ProgramsProduct->deleteAll("product_id = '". $this->Merchant->Product->id. "' AND program_id IN ('". implode("','", array_keys($this->request->data['ProgramsProduct']['id'])). "')");
					foreach ($this->request->data['ProgramsProduct']['id'] as $program_id => $enabled) {
						if ($enabled == '1') {
							$this->Merchant->Product->ProgramsProduct->save(array('ProgramsProduct' => array('id' => null, 'product_id' => $this->Merchant->Product->id, 'program_id' =>  $program_id)));
						}
					}
				}


				$this->set('status', 'update_complete');

				//	if this is a new product
				if ($this->request->data['Product']['id'] == '') {
					$this->set('new', true);
				}

				return;
			} else {
				$this->set('status', 'update_error');
			}
		}

		$this->layout = 'ajax';

		$product = $this->Merchant->Product->findById($product_id);
		$category_id = '';
		if ($product_id == 'new') {
			$product['Product']['id'] = '';
			if (isset($this->passedArgs['merchant_id'])) {
				$product['Product']['merchant_id'] = $this->passedArgs['merchant_id'];
			}

			//	select the category, programs and clients for the category if set

			if (isset($this->params['url']['data']['Product']['category_id'] )) {
				$category_id = $this->params['url']['data']['Product']['category_id'];

				$this->Merchant->Product->Category->recursive = -1;
				$category = $this->Merchant->Product->Category->findById($category_id);

				if ($category) {
					$product['Category'][] = $category['Category'];
					$programs = $this->Merchant->Product->Program->ProgramsCategory->findAllByCategoryId($category_id);
					foreach ($programs as $program) {
						$product['Program'][] = $program['Program'];
					}
					$clients = $this->Merchant->Product->Client->ClientsCategory->findAllByCategoryId($category_id);
					foreach ($clients as $client) {
						$product['Client'][] = $client['Client'];
					}
				}
			}
		}

		$this->set('category_id', $category_id);


		$this->Merchant->Product->Program->recursive = -1;
		$programs = $this->Merchant->Product->Program->find('all');

		$this->Merchant->Product->Category->recursive = -1;
		$categories = $this->Merchant->Product->Category->find('list', array('order'=>'description, name', 'fields'=>'id,description'));

		$this->set('search_weights', $this->Merchant->Product->getEnumValues('search_weight'));

		$this->Merchant->Product->bindModel(array('belongsTo' => array('User')));
		$this->Merchant->Product->User->recursive = -1;
		$old_debug = configure::read('debug');
		$users = $this->Merchant->Product->User->generateList(array('type' => 'Administrator'), 'User.username', null, null, '{n}.User.username');

		$this->set(compact('categories','programs','clients','users'));
		$this->request->data = $product;

		$this->set('id', $product_id);

		$this->set('product', true);

		$this->set('display_images', $this->Merchant->Product->getEnumValues('display_image'));

	}

	/**
	 * delete a product
	 */
	function admin_ajax_product_delete($product_id) {

		$this->Merchant->Product->delete($product_id);
	}

	/**
	 * get the clients for a products program
	 *
	 * @param	int	$product_id	id of the product
	 * @param	int	$program_id	id of the program
	 */
	function admin_ajax_product_clients($product_id, $program_id) {

		$this->layout = 'ajax';

		$this->Merchant->Product->Client->recursive = -1;
		$clients = $this->Merchant->Product->Client->findAllByProgramId($program_id);
		$this->set('clients', $clients);

		$client_ids = array();
		foreach ($clients as $client) {
			$client_ids[] = $client['Client']['id'];
		}

		// if this is a new product, try to get the selected clients for the products category
		if ($product_id == 'new') {

			if (isset($this->params['url']['data']['Product']['category_id'])) {
				$category_id = $this->params['url']['data']['Product']['category_id'];
			} else {
				$category_id = '';
			}
			$product_clients = $this->Merchant->Product->Client->ClientsCategory->findAllByCategoryId($category_id);

		}  else {
			$this->Merchant->Product->ClientsProduct->recursive = 0;
			$product_clients = $this->Merchant->Product->ClientsProduct->find('all', array('conditions'=>array("ClientsProduct.product_id = '". $product_id. "' AND ClientsProduct.client_id IN ('". implode("','", $client_ids). "')")));
		}



		$this->set('product_clients', $product_clients);

		$this->Merchant->Product->Program->recursive = -1;
		$program = $this->Merchant->Product->Program->findById($program_id);
		$this->set('program', $program);
	}

	function admin_ajax_product_preview_voucher($voucher_id=false) {
		$this->layout = 'ajax';

		if (!$voucher_id && $this->Session->read($this->name. '.preview_voucher_id')) {
			$voucher_id = $this->Session->read($this->name. '.preview_voucher_id');
		}

		$this->Merchant->Product->Client->Voucher->recursive = 0;
		$voucher = false;
		if ($voucher_id) {
			$voucher = $this->Merchant->Product->Client->Voucher->findById($voucher_id);
		}

		//	try for my rewards voucher
		if (!$voucher) {
			$voucher = $this->Merchant->Product->Client->Voucher->findByName('My Rewards');
		}

		//	default to first voucher
		if (!$voucher) {
			$voucher = $this->Merchant->Product->Client->Voucher->find('first');
		}


		$this->Session->write($this->name. '.preview_voucher_id', $voucher['Voucher']['id']);

		$this->Merchant->MerchantAddress->recursive = 0;
		$merchant_address = $this->Merchant->MerchantAddress->findByMerchantId($this->request->data['Product']['merchant_id']);

		$this->Merchant->recursive = 0;
		$merchant = $this->Merchant->findById($this->request->data['Product']['merchant_id']);

		$this->request->data['Merchant'] = $merchant['Merchant'];
		$this->request->data['MerchantAddress'] = $merchant_address['MerchantAddress'];

		$this->set('vouchers', $this->Merchant->Product->Client->Voucher->find('list'));
		$this->set('product', $this->request->data);
		$this->set('voucher', $voucher['Voucher']);
	}

	/**
	 * Return a html snipet of addresses for a merchant
	*/
	function admin_ajax_addresses($id = false) {

		$this->layout = 'ajax';

		$this->Merchant->MerchantAddress->recursive = 0;
		$this->set('merchant_addresses', $this->Merchant->MerchantAddress->findAllByMerchantId($id));

		$this->set('id', $id);

	}

	/**
	 * Return an HTML snippet of merchant comments
	*/
	function admin_ajax_comments($id = null) {

		$this->layout = 'ajax';

		$comments = $this->Merchant->MerchantComment->findAllByMerchantId($id);
		
		$this->set('comments', $comments);
		$this->set('id', $id);
	}

	/**
	 * Save a new comment
	*/
	function admin_ajax_comment_save($id,$desc,$notes) {

		$this->layout = 'ajax';

		$this->request->data['MerchantComment']['user_id'] = $this->Session->read('user.User.id');
		$this->request->data['MerchantComment']['username'] = $this->Session->read('user.User.username');
		$this->request->data['MerchantComment']['merchant_id'] = $id;
	
		$this->request->data['MerchantComment']['description'] = $desc;	
		$this->request->data['MerchantComment']['notes'] = $notes;
		
		
		
		if ($this->Merchant->MerchantComment->save($this->request->data)) {

		} else {
			$this->set('error', true);
		}

		$this->set('id', $id);
	}

	/**
	 * Delete a  comment
	*/
	function admin_ajax_comment_delete($id, $comment_id) {

		$this->layout = 'ajax';

		$this->Merchant->MerchantComment->delete($comment_id);

		$this->set('id', $id);
	}

	function admin_ajax_search() {

	 	$this->paginate = array('limit' => 10);

		$this->layout = 'blank';

		if  (isset($this->request->data['Merchant']['search'])) {
			$this->Session->write('Merchant.search', $this->request->data['Merchant']['search']);
		}

		if  (isset($this->request->data['Product']['id'])) {
			$this->Session->write('Product.id', $this->request->data['Product']['id']);
		}

		$merchant_search = $this->Session->read('Merchant.search');

		$conditions = array();

		if  (!empty($merchant_search)) {

			$conditions[] = "

				(`Merchant`.`name` LIKE  '%". $merchant_search. "%'

				OR `Merchant`.`id` LIKE  '%". $merchant_search. "%')

			";

		}

		$this->Merchant->recursive = 0;

		$this->set('merchants', $this->paginate('Merchant',$conditions));

	}



	/**
	 * Return a form table of merchant details
	 *
	 * @param	int	i$d		Merchant ID

	*/

	function admin_ajax_update($id) {

		$this->layout = 'blank';

		$this->request->data = $this->Merchant->read(null, $id);

		$this->set('states', $this->Merchant->getEnumValues('state'));

		$this->set('related_products', $this->paginate('Product', "Product.merchant_id = '". $id. "' AND Product.id != '". $this->Session->read('Product.id'). "'"));

	}

	/**
	 * Upload a csv file of addresses for a merchant
	 */
	 function admin_upload_addresses($id, $step = 1) {
	 	$this->set(compact('id','step'));
		//debug($this->request->data);

		ini_set('auto_detect_line_endings', '1');
		set_time_limit(360);
		ini_set('memory_limit', '128M');

		if (!empty($this->request->data)) {

			if ($step = 2) {

				$uploadErrors = array(
					UPLOAD_ERR_INI_SIZE => 'The uploaded file exceeds the upload_max_filesize directive in php.ini.',
					UPLOAD_ERR_FORM_SIZE => 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form.',
					UPLOAD_ERR_PARTIAL => 'The uploaded file was only partially uploaded.',
					UPLOAD_ERR_NO_FILE => 'No file was uploaded.',
					UPLOAD_ERR_NO_TMP_DIR => 'Missing a temporary folder.',
					UPLOAD_ERR_CANT_WRITE => 'Failed to write file to disk.',
					//UPLOAD_ERR_EXTENSION => 'File upload stopped by extension.',
				);

				$error_code = $this->request->data['Merchant']['addresses']['error'];

				// if($errorCode !== UPLOAD_ERR_OK && isset($uploadErrors[$errorCode]))
				if($error_code !== UPLOAD_ERR_OK) {
					if(isset($uploadErrors[$error_code])) {
						$upload_error = $uploadErrors[$error_code];
					} else {
						$upload_error = "Unknown error uploading file.";
					}
					$this->set('upload_error', $upload_error);
					return;
				}

				$addresses= $this->Csv->file_to_array($this->request->data['Merchant']['addresses']['tmp_name']);

				// make sure some rows have been uploaded
				if (empty($addresses)) {
					$this->Session->setFlash('Csv file contains no rows');
					$this->redirect(array('action'=>'upload'), null, true);
				}

				//check that the correct columns have been sent
				$required_cols = array('name','address1', 'address2');

				foreach($required_cols as $key => $col) {
					if (in_array($col, array_keys($addresses[0]), true)) {
						unset($required_cols[$key]);
					}
				}

				if (!empty($required_cols)) {
					$this->Session->setFlash('Missing required columns: '. implode(', ', $required_cols));
					$this->redirect('/admin/merchants/upload_addresses/'.$id);
				}

				//	import into tempory table
				$this->MerchantAddressesImport->query('TRUNCATE TABLE `merchant_addresses_imports`');
				$errors = array();
				foreach ($addresses as $row => $address) {
					//	remove any blank info to use database defaults
					foreach($address as $key => $value) {
						if ($value == '') {
							unset($address[$key]);
						} else {
							$address[$key] = trim($address[$key]);
						}
					}
					//	fix up states
					if (isset($address['state'])) {
						$address['state'] = strtoupper($address['state']);
					}

					$address['id'] = $row + 2;
					$address['row_number'] = $row + 2;
					$address['merchant_id'] = $id;
					$address['address2'] = !empty($address['address2'])?$address['address2']:"";
					$address['city'] = !empty($address['city'])?$address['city']:"";
					$address['region'] = !empty($address['region'])?$address['region']:"";
					$address['country'] = !empty($address['country'])?$address['country']:"";
					$this->MerchantAddressesImport->id = $address['id'];
					$this->MerchantAddressesImport->save(array('MerchantAddressesImport' => $address));
				}

				//check for existing primary addresses
				$sql = "SELECT MerchantAddressesImport.* FROM merchant_addresses_imports AS MerchantAddressesImport WHERE MerchantAddressesImport.primary = 1";

				$primary_addresses = $this->MerchantAddressesImport->query($sql);

				if(!empty($primary_addresses)){
						$sql = "SELECT MerchantAddresses.id FROM merchant_addresses AS MerchantAddresses WHERE MerchantAddresses.primary = 1 AND MerchantAddresses.merchant_id = $id";
						$existing_primary = $this->Merchant->MerchantAddress->query($sql);

						if(!empty($existing_primary)){
							foreach($primary_addresses as $key=> $primary){
								$errors[] = array('row' => $primary['MerchantAddressesImport']['id'], 'error' => 'A primary address is already set, the uploaded address will not be set as primary');
								$address_error = true;
								//write it back to the database and changes the primary
 								$sql = "UPDATE `merchant_addresses_imports` SET `primary` = '0' WHERE `merchant_addresses_imports`.`id` =  {$primary['MerchantAddressesImport']['id']}";
								 $this->MerchantAddressesImport->query($sql);

							}
						}else{
							$start =0;
							foreach($primary_addresses as $key=> $primary){
								if($start >0){
										$errors[] = array('row' => $primary['MerchantAddressesImport']['id'], 'error' => 'Multiple primary addresses uploaded, only the first will be set as primary');
										$address_error = true;
										//write it back to the database and changes the primary
										$sql = "UPDATE `merchant_addresses_imports` SET `primary` = '0' WHERE `merchant_addresses_imports`.`id` =  {$primary['MerchantAddressesImport']['id']}";
								 		$this->MerchantAddressesImport->query($sql);
								}
								$start++;
							}
						}
				}

				$this->set('upload_errors', $errors);
				$this->set('uploaded_addresses', count($addresses));
				$this->set('good_addresses', count($addresses));
			}
		}else{
				if($step ==3){
						$sql = "INSERT INTO merchant_addresses (`merchant_id` ,`primary` ,`address1` ,`name` ,`address2` ,`suburb` ,`city` ,`state` ,`region` ,`country` ,`postcode` ,`phone` )
							SELECT  merchant_addresses_imports.merchant_id, merchant_addresses_imports.primary, merchant_addresses_imports.address1, merchant_addresses_imports.name , merchant_addresses_imports.address2,
							merchant_addresses_imports.suburb, merchant_addresses_imports.city, merchant_addresses_imports.state, merchant_addresses_imports.region, merchant_addresses_imports.country, merchant_addresses_imports.postcode, merchant_addresses_imports.phone
							FROM merchant_addresses_imports ";
						$this->Merchant->MerchantAddress->query($sql);

						$addresses = $this->MerchantAddressesImport->find('count');
						$this->set('address_count', $addresses);

						$this->MerchantAddressesImport->query('TRUNCATE TABLE `merchant_addresses_imports`');
				}
		}
	 }

	function admin_example_merchant_addresses_import() {
	 	$this->layout = 'csv';
	}
	function admin_example_products_import() {
	 	$this->layout = 'csv';
	}

/**
	 * Upload a csv file of merchants
	 */
	 function admin_upload_merchants($step = 1) {
	 	$this->set(compact('step'));
		//debug($this->request->data);

		ini_set('auto_detect_line_endings', '1');
		set_time_limit(360);
		ini_set('memory_limit', '128M');

		if (!empty($this->request->data)) {

			if ($step = 2) {

				$uploadErrors = array(
					UPLOAD_ERR_INI_SIZE => 'The uploaded file exceeds the upload_max_filesize directive in php.ini.',
					UPLOAD_ERR_FORM_SIZE => 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form.',
					UPLOAD_ERR_PARTIAL => 'The uploaded file was only partially uploaded.',
					UPLOAD_ERR_NO_FILE => 'No file was uploaded.',
					UPLOAD_ERR_NO_TMP_DIR => 'Missing a temporary folder.',
					UPLOAD_ERR_CANT_WRITE => 'Failed to write file to disk.',
					//UPLOAD_ERR_EXTENSION => 'File upload stopped by extension.',
				);

				$error_code = $this->request->data['Merchant']['merchants']['error'];

				// if($errorCode !== UPLOAD_ERR_OK && isset($uploadErrors[$errorCode]))
				if($error_code !== UPLOAD_ERR_OK) {
					if(isset($uploadErrors[$error_code])) {
						$upload_error = $uploadErrors[$error_code];
					} else {
						$upload_error = "Unknown error uploading file.";
					}
					$this->set('upload_error', $upload_error);
					return;
				}

				$merchants= $this->Csv->file_to_array($this->request->data['Merchant']['merchants']['tmp_name']);

				// make sure some rows have been uploaded
				if (empty($merchants)) {
					$this->Session->setFlash('Csv file contains no rows');
					$this->redirect(array('action'=>'upload'), null, true);
				}

				//check that the correct columns have been sent
				$required_cols = array('name');

				foreach($required_cols as $key => $col) {
					if (in_array($col, array_keys($merchants[0]), true)) {
						unset($required_cols[$key]);
					}
				}

				if (!empty($required_cols)) {
					$this->Session->setFlash('Missing required columns: '. implode(', ', $required_cols));
					$this->redirect('/admin/merchants/upload_merchants/');
				}

				//	import into tempory table
				$this->MerchantsImport->query('TRUNCATE TABLE `merchants_imports`');
				$errors = array();
				foreach ($merchants as $row => $product) {
					//	remove any blank info to use database defaults
					foreach($product as $key => $value) {
						if ($value == '') {
							unset($product[$key]);
						} else {
							$product[$key] = trim($product[$key]);
						}
					}
					//	fix up states
					if (isset($product['state'])) {
						$product['state'] = strtoupper($product['state']);
					}

					if(isset($product['address_state'])){
						$product['address_state'] = strtoupper($product['address_state']);
					}

					$product['id'] = $row + 2;
					$this->MerchantsImport->id = $product['id'];
					$this->MerchantsImport->save(array('MerchantsImport' => $product));
				}

				//check at least one row has been uploaded
				if(count($merchants) ==0)
					$errors[] = array('row' => 0, 'error' => 'No row in the uploaded csv file');

				//check for existing categories
				foreach($merchants as $row=>$merchant){

					if(!empty($merchant['product_name'])){
						if(!empty($merchant['product_category']) ){
							$sql = "SELECT id FROM categories WHERE description = '".trim($merchant['product_category'])."' LIMIT 1";
							$category = $this->Category->query($sql);
							if(empty($category)){
								$errors[] = array('row' => $row +2, 'error' => 'Category specified does not exist, product will not be assigned to a category or any clients');
							}else{
								//write category id into the system
								$sql = "UPDATE `merchants_imports` SET `product_category_id` = '{$category[0]['categories']['id']}' WHERE `merchants_imports`.`id` =  ".($row+2)."";
								$this->MerchantsImport->query($sql);
							}
						}else{
							$errors[] = array('row' => $row +2, 'error' => 'No category specified, product will not be assigned to a category or any clients');

						}
					}
				}

				$this->set('upload_errors', $errors);
				$this->set('uploaded_merchants', count($merchants));
				$this->set('good_merchants', count($merchants));
			}
		}else{
				if($step ==3){
						//needs to be changed

						$merchants = $this->MerchantsImport->find('all');

						foreach($merchants as $merchant){

							unset($merchant['MerchantsImport']['id']);

							//split the product and merchant address out
							foreach($merchant['MerchantsImport'] as $key=> $value){
								if(strpos($key,'product_') === 0){
									$key_temp= substr($key, 8,strlen($key));
									$product [$key_temp] = $value;
									unset($merchant['MerchantsImport'][$key]);
								}elseif(strpos($key,'address_') === 0){
									$key_temp= substr($key, 8,strlen($key));
									$address [$key_temp] = $value;
									unset($merchant['MerchantsImport'][$key]);
								}
							}

							//save them into the merchant table
							$this->Merchant->create();
							$this->Merchant->save(array('Merchant'=>$merchant['MerchantsImport']));
							$merchant_id = $this->Merchant->getInsertID();
							$prod_id = '';
							if(!empty($product['name'])){
								//save product into product table
								$product['merchant_id'] = $merchant_id;
								$product['user_id'] = $this->Session->read('user.User.id');
								$this->Category->Product->create();
								$this->Category->Product->save(array('Product' => $product));
								$prod_id = $this->Category->Product->getInsertID();
							}

							//save merchant as merchant address in merchantaddress table


							if(!empty($address['name'])){
								//save address into address table
								$address['merchant_id'] = $merchant_id;
								//$address['primary'] = 1;
								$this->Merchant->MerchantAddress->create();
								$this->Merchant->MerchantAddress->save(array('MerchantAddress'=>$address));
							}
							else
							{
								$madd = array();
								$this->Merchant->MerchantAddress->create();
								$madd['MerchantAddress']['merchant_id'] = $merchant_id;
								$madd['MerchantAddress']['name'] =  $merchant['MerchantsImport']['name'];
								$madd['MerchantAddress']['address1'] = $merchant['MerchantsImport']['mail_address1'];
								$madd['MerchantAddress']['address2'] = $merchant['MerchantsImport']['mail_address2'];
								$madd['MerchantAddress']['suburb'] = $merchant['MerchantsImport']['mail_suburb'];
								$madd['MerchantAddress']['state'] = $merchant['MerchantsImport']['mail_state'];
								$madd['MerchantAddress']['postcode'] = $merchant['MerchantsImport']['mail_postcode'];
								$madd['MerchantAddress']['country'] = $merchant['MerchantsImport']['mail_country'];
								$this->Merchant->MerchantAddress->save($madd);

							}

							$madd_id = $this->Merchant->MerchantAddress->find('all',array('conditions'=>array('MerchantAddress.merchant_id' => $merchant_id),'fields'=>array('MerchantAddress.id')));

							foreach($madd_id as $mid)
							{
								$mmid = $mid['MerchantAddress']['id'] ;

								$sql = "INSERT INTO `products_addresses` (`product_id`,`merchant_address_id`) VALUES (".$prod_id.",".$mmid.")";
								$this->Merchant->MerchantAddress->Product->ProductsAddress->query($sql);
							}

							//products trackers
							$this->loadModel("ProductsTracker");
							$sql = "INSERT INTO products_trackers(`product_id`,`clicks`,`prints`) VALUES ($prod_id,0,0 )";
							$this->ProductsTracker->query($sql);

							if(!empty($product['name']) && $product['category_id'] >0){
									//product_id
									$product_id = $this->Category->Product->getInsertID();

									//categories_products
									$sql = "INSERT INTO categories_products(`category_id`, `product_id`) VALUES ({$product['category_id']} , $product_id )";

									$this->Category->query($sql);

									//clients_products
									$sql = "INSERT INTO clients_products(`product_id`, `client_id`) SELECT $product_id, clients_categories.client_id FROM clients_categories WHERE clients_categories. category_id = {$product['category_id']}";

									$this->Category->query($sql);

									//programs_products
									$sql = "INSERT INTO programs_products(`product_id`, `program_id`) SELECT $product_id, programs_categories.program_id FROM programs_categories WHERE programs_categories. category_id = {$product['category_id']}";

									$this->Category->query($sql);
							}
						}


						$count = $this->MerchantsImport->find('count');
						$this->set('product_count', $count);

						$this->MerchantsImport->query('TRUNCATE TABLE `merchant_products_imports`');
				}
		}
	 }

	function admin_example_merchants_import() {
	 	$this->layout = 'csv';
	 }

	function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash('Invalid id for Merchant');
			$this->redirect(array('action'=>'index'), null, true);
		}

		$products = $this->Merchant->Product->find('all', array('conditions'=>array('merchant_id' => $id)));
		if (!empty($products)) {
			$this->Session->setFlash('Please delete all products before removing the Merchant');
			$this->redirect('/admin/merchants/edit/'. $id);
		}

		if ($this->Merchant->delete($id)) {
			$this->Session->setFlash('Merchant #'.$id.' deleted');
			$this->redirect(array('action'=>'index'), null, true);
		}
	}

	function admin_no_geo_codes()
	{
		$sql1 = " where Merchant.id=MerchantAddress.merchant_id and MerchantAddress.latitude is null and MerchantAddress.longitude is null group by Merchant.id";

		$this->paginate = array('Merchantlist' => array('limit' => 20));
		$list = $this->paginate('Merchantlist', array('search' =>array('sql' => $sql1)));

		$this->set('list',$list);
	}

	function admin_delete_addresses($id, $step = 1)
	 	{
		 	$this->set(compact('id','step'));

			ini_set('auto_detect_line_endings', '1');
			set_time_limit(360);
			ini_set('memory_limit', '128M');

			if (!empty($this->request->data))
			{
				if ($step = 2)
				{

					$uploadErrors = array(
						UPLOAD_ERR_INI_SIZE => 'The uploaded file exceeds the upload_max_filesize directive in php.ini.',
						UPLOAD_ERR_FORM_SIZE => 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form.',
						UPLOAD_ERR_PARTIAL => 'The uploaded file was only partially uploaded.',
						UPLOAD_ERR_NO_FILE => 'No file was uploaded.',
						UPLOAD_ERR_NO_TMP_DIR => 'Missing a temporary folder.',
						UPLOAD_ERR_CANT_WRITE => 'Failed to write file to disk.',
						//UPLOAD_ERR_EXTENSION => 'File upload stopped by extension.',
					);

					$error_code = $this->request->data['Merchant']['addresses']['error'];

					if($error_code !== UPLOAD_ERR_OK) {
						if(isset($uploadErrors[$error_code])) {
							$upload_error = $uploadErrors[$error_code];
						} else {
							$upload_error = "Unknown error uploading file.";
						}
						$this->set('upload_error', $upload_error);
						return;
					}

					$addresses= $this->Csv->file_to_array($this->request->data['Merchant']['addresses']['tmp_name']);

					// make sure some rows have been uploaded
					if (empty($addresses)) {
						$this->Session->setFlash('Csv file contains no rows');
						$this->redirect(array('action'=>'delete_addresses'), null, true);
					}

					//check that the correct columns have been sent
					$required_cols = array('name');

					foreach($required_cols as $key => $col)
					{
						if (in_array($col, array_keys($addresses[0]), true)) {
							unset($required_cols[$key]);
						}
					}

					if (!empty($required_cols))
					{
						$this->Session->setFlash('Missing required columns: '. implode(', ', $required_cols));
						$this->redirect('/admin/merchants/delete_addresses/'.$id);
					}

					//	import into tempory table
					$this->MerchantAddressesImport->query('TRUNCATE TABLE `merchant_addresses_deletes`');
					$errors = array();
					foreach ($addresses as $row => $address)
					{
						$address['id'] = null;
						$address['merchant_id'] = $id;
						$this->MerchantAddressesDelete->save(array('MerchantAddressesDelete' => $address));
					}

					$this->set('upload_errors', $errors);
					$this->set('uploaded_addresses', count($addresses));

					$delete_merchants = $this->MerchantAddressesDelete->query("SELECT count(MerchantAddressesDelete.id) AS count
					FROM merchant_addresses_deletes AS MerchantAddressesDelete
					INNER JOIN merchant_addresses AS MerchantAddress ON MerchantAddressesDelete.merchant_id = MerchantAddress.merchant_id
					and MerchantAddressesDelete.name = MerchantAddress.name");

					$this->set('delete_merchants', $delete_merchants[0][0]['count']);
				}
			}
	 		else
	 		{
				if ($step == '3')
				{
					$addresses = $this->MerchantAddressesDelete->query("SELECT ProductsAddress.* FROM products_addresses as ProductsAddress,
					merchant_addresses_deletes AS MerchantAddressesDelete , merchant_addresses AS MerchantAddress
					WHERE MerchantAddressesDelete.merchant_id = MerchantAddress.merchant_id and MerchantAddressesDelete.name = MerchantAddress.name
					and ProductsAddress.merchant_address_id=MerchantAddress.id");

					$this->loadModel('ProductsAddress');

					$this->ProductsAddress->query("DELETE ProductsAddress FROM products_addresses as ProductsAddress,
					merchant_addresses_deletes AS MerchantAddressesDelete , merchant_addresses AS MerchantAddress
					WHERE MerchantAddressesDelete.merchant_id = MerchantAddress.merchant_id
					and MerchantAddressesDelete.name = MerchantAddress.name and ProductsAddress.merchant_address_id=MerchantAddress.id");

					$this->Merchant->MerchantAddress->query("
						DELETE MerchantAddress FROM merchant_addresses_deletes AS MerchantAddressesDelete
						INNER JOIN merchant_addresses AS MerchantAddress
						ON MerchantAddressesDelete.merchant_id = MerchantAddress.merchant_id
						and MerchantAddressesDelete.name = MerchantAddress.name
						");

					$this->set('address_count', $this->Merchant->MerchantAddress->getAffectedRows());
					$this->Merchant->MerchantAddress->query('TRUNCATE TABLE `merchant_addresses_deletes`');
				}
			}

		 }

	function admin_example_address_delete() {
		 	$this->layout = 'csv';
	 }

}
?>
