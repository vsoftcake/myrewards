<?php
class AutoLoginsController extends AppController {
	var $uses = array('User','Domain','Client','AutoLogin');

	function admin_index()
	{
		$conditions = "";
		$conditions = "where AutoLogin.domain_id=Domain.id and AutoLogin.client_id = Client.id ";
		if (isset($this->params['url']['data'])) {
			$this->Session->write($this->name.'.search', $this->params['url']['data']);
		}

		if ($this->Session->check($this->name.'.search')) {
			$this->request->data = $this->Session->read($this->name.'.search');
			if  (!empty($this->request->data['AutoLogin']['search'])) {
				$conditions.= " AND
				(`AutoLogin`.`domain_url` LIKE  '%". $this->request->data['AutoLogin']['search']. "%'
				OR `AutoLogin`.`id` LIKE  '%". $this->request->data['AutoLogin']['search']. "%'
				OR `Client`.`name` LIKE  '%". $this->request->data['AutoLogin']['search']. "%'
				OR `Domain`.`name` LIKE  '%". $this->request->data['AutoLogin']['search']. "%'
				OR `AutoLogin`.`username` LIKE  '%". $this->request->data['AutoLogin']['search']. "%')
				";
			}
		}

		$this->AutoLogin->recursive = 0;
		$this->paginate = array('AutoLogin' => array('limit' => 20));
		$details = $this->paginate('AutoLogin', array('search' =>array('sql' => $conditions)));

		$this->set('details', $details);
	}
	function admin_edit($id)
	{
		if(!empty($this->request->data))
		{
			$url = $this->request->data['AutoLogin']['domain_url'];
			if($this->AutoLogin->save($this->request->data))
			{
				$this->redirect('/admin/auto_logins/');
			}
		}
		else
		{
			$this->AutoLogin->id = $id;
			$this->request->data = $this->AutoLogin->read(null, $id);
		}

		$clients = $this->Client->find('list',array('conditions'=>'Client.name!=""'));
		$domains = $this->Domain->find('list');

		$this->set('clients',$clients);
		$this->set('domains',$domains);
	}

	function admin_update_clients()
	{
		$did = $this->request->data['AutoLogin']['domain_id'];
		$clients = $this->Client->find('list', array('conditions' =>array('Client.domain_id' => $did)));

		$this->set('domain_clients',$clients);
		$this->layout = 'ajax';
	}

	function admin_get_users($client)
	{
		$username = $this->request->data['AutoLogin']['username'];
		$conditions = array("User.client_id=".$client."  AND User.registered=1 AND User.username LIKE '%".$username."%'");
		$data = $this->User->find('all',array('conditions' => $conditions, 'recursive'=>-1));
		$this->set('users', $data);
	}

	function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash('Invalid id for Auto Login Detail');
			$this->redirect(array('action'=>'index'), null, true);
		}
		if ($this->AutoLogin->delete($id)) {
			$this->Session->setFlash('Auto Login Detail#'.$id.' deleted');
			$this->redirect(array('action'=>'index'), null, true);
		}
	}
}