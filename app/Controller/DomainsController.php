<?php
class DomainsController extends AppController {

	function admin_index() {
	
		$name = Inflector::singularize($this->name);
		$conditions = array();
		
		if (isset($this->params['url']['data'])) {
			$this->Session->write($name.'.search', $this->params['url']['data']);
		}
		
		if ($this->Session->check($name.'.search')) {
			$this->request->data = $this->Session->read($name.'.search');
			if  (!empty($this->request->data[$name]['search'])) {
				$conditions[] = "
					(`". $name. "`.`name` LIKE  '%". $this->request->data[$name]['search']. "%'
					OR `". $name. "`.`id` LIKE  '%". $this->request->data[$name]['search']. "%')
				";
			}
		}
		
		$this->$name->recursive = 0;
		$this->set(Inflector::underscore($this->name), $this->paginate($name, $conditions));
	}

	function admin_edit($id = null) {
		
		if (!empty($this->request->data)) {
			$this->Domain->recursive = -1;
			$old_data = $this->Domain->findById($id);
			if ($this->Domain->save($this->request->data)) {
			
				//	log the change
				$log = array(
					'id' => null, 
					'foreign_id' => $this->{Inflector::singularize($this->name)}->id,
					'foreign_model' => Inflector::singularize($this->name),
					'user_id' => $this->Session->read('user.User.id'),
					'old_data' => $old_data,
					'new_data' => $this->request->data,
					'notes' => ''
				);
				$log['description'] = Inflector::humanize(Inflector::underscore($this->name)). ' modified';
				$this->save_log($log);
				
				$this->Session->setFlash('The Domain has been saved');
				$this->redirect_session(array('action'=>'index'), null, true);
			} else {
				$this->Session->setFlash('Please correct the errors below.');
			}
		} else {
			$this->request->data = $this->Domain->read(null, $id);
			$this->_setRedirect();
		}
		$clients = $this->Domain->Client->find('list');
		$this->set(compact('clients'));
	}

	function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash('Invalid id for Domain');
			$this->redirect(array('action'=>'index'), null, true);
		}
		if ($this->Domain->delete($id)) {
			$this->Session->setFlash('Domain #'.$id.' deleted');
			$this->redirect(array('action'=>'index'), null, true);
		}
	}

}
?>