<?php

/**
 * Functions that are accessible to external clietns to manage their users
*/

class ExternalController extends AppController{

	var $uses = array('Domain');
	var $layout = 'blank';

	/**
	 * Instructions
	*/
	function index() {

	}

	/**
	 * Add/Update a members details
	*/
	function add_member() {

		Configure::write('debug', 0);

		if (isset($this->data['access_username'])) {

			// check for required
			if  (!isset($this->data['access_password']) || empty($this->data['access_password'])) {
				$this->set('return', 'error: missing access_password');
				return;
			} else if  (!isset($this->data['username']) || empty($this->data['username'])) {
				$this->set('return', 'error: missing username');
				return;
			} else if  (!isset($this->data['firstname']) || empty($this->data['firstname'])) {
				$this->set('return', 'error: missing firstname');
				return;
			} else if  (!isset($this->data['lastname']) || empty($this->data['lastname'])) {
				$this->set('return', 'error: missing lastname');
				return;
			}
			/*		password validation no longer required
			else if  (isset($this->data['password']) && strlen($this->data['password']) > 0 && strlen($this->data['password']) < 6) {
				$this->set('return', 'error: password must be 6 characters');
				return;
			}
			*/

			$this->Domain->Client->recursive = 0;
			$client  = $this->Domain->Client->find('first', array('conditions'=>array('username' => $this->data['access_username'], 'Client.password' => $this->data['access_password'])));

			if (empty($client)) {
				$this->set('return', 'error: incorrect access_username or access_password');
				return;
			} else {

				// check for existing admin users with the same name
				$this->Domain->Client->User->recursive = 0;
				$existing_user = $this->Domain->Client->User->find('first', array('conditions'=>array("User.username = '". $this->data['username']. "' AND User.type = 'Administrator'")));
				if  (!empty($existing_user)) {
					$this->set('return', 'error: username already exists');
					return;
				}

				//check for existing users of other clients of this domain
				if ($client['Domain']['all_client'] == '0') {

					$this->Domain->Client->User->recursive = 0;
					$users = $this->Domain->Client->User->find('all',array('conditions'=>array("User.username = '". $this->data['username']. "' AND Client.domain_id = '". $client['Client']['domain_id']. "' AND User.client_id != '". $client['Client']['id']. "'")));

					if  (!empty($users)) {
						$this->set('return', 'error: username already exists');
						return;
					}
				}

				$this->Domain->Client->User->recursive = 0;
				$user = $this->Domain->Client->User->find('first', array('conditions'=>array('User.username' => $this->data['username'], 'Client.domain_id' => $client['Client']['domain_id'])));
				if ($user) {
					$data['User']['id'] = $user['User']['id'];
				}
				$data['User']['client_id'] = $client['Client']['id'];
				$data['User']['domain_id'] = $client['Client']['domain_id'];
				$data['User']['login_string'] = md5(uniqid(rand(), true));
				$data['User']['username'] = $this->data['username'];
				$data['User']['first_name'] = $this->data['firstname'];
				$data['User']['last_name'] = $this->data['lastname'];
				if (isset($this->data['state'])) {
					$data['User']['state'] = $this->data['state'];
				}
				if (isset($this->data['password']) && !empty($this->data['password'])) {
					$data['User']['password'] = $this->data['password'];
				}

				if (isset($this->data['newsletter']) && $this->data['newsletter'] == '1') {
					$data['User']['newsletter'] = 1;
				} else {
					$data['User']['newsletter'] = 0;
				}

				//	auto register the user
				$data['User']['registered'] = 1;

				// ^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$

				if (isset($this->data['email']) && eregi("^[A-Za-z0-9._%+-]+@([A-Za-z0-9-]+\.)+([A-Za-z0-9]{2,4}$", $this->data['email'])) {
					$data['User']['email'] = $this->data['email'];
				}

				$user = $this->Domain->Client->User->save($data);
				if (!empty($user)) {
					$this->set('return', 'http://'. $client['Domain']['name']. $this->base. '/users/auto_login/'. $data['User']['login_string']);
				} else {
					$this->set('return', 'error: user not added');
				}


			}
		}
	}

	/**
	 * Auto login a member
	*/
	function login_member() {

		Configure::write('debug', 0);	//	do not show sql debug stuff here.

		if (isset($this->data['access_username'])) {

			// check for required
			if  (!isset($this->data['access_password']) || empty($this->data['access_password'])) {
				$this->set('return', 'error: missing access_password');
				return;
			} else if  (!isset($this->data['username']) || empty($this->data['username'])) {
				$this->set('return', 'error: missing username');
				return;
			}

			$this->Domain->Client->recursive = 1;
			$client  = $this->Domain->Client->find('first', array('conditions'=>array('Client.username' => $this->data['access_username'], 'Client.password' => $this->data['access_password'])));

			if (empty($client)) {
				$this->set('return', 'error: incorrect access_username or access_password');
				return;
			} else {

				$this->Domain->Client->User->recursive = -1;
				$user = $this->Domain->Client->User->find('first', array('conditions'=>array('username' => $this->data['username'], 'client_id' => $client['Client']['id'])));
				if (!$user) {
					$this->set('return', 'error: username not found');
					return;
				}
				$data['User']['id'] = $user['User']['id'];
				$data['User']['login_string'] = md5(uniqid(rand(), true));
				$user = $this->Domain->Client->User->save($data);

				$this->set('return', 'http://'. $client['Domain']['name']. $this->base. '/users/auto_login/'. $data['User']['login_string']);
			}
		}
	}

	/**
	 * delete members
	*/
	function delete_members() {

		Configure::write('debug', 0);	//	do not show sql debug stuff here, this is an image.

		if (isset($this->data['access_username'])) {

			// check for required
			if  (!isset($this->data['access_password']) || empty($this->data['access_password'])) {
				$this->set('return', 'error: missing access_password');
				return;
			} else if  (!isset($this->data['username']) || empty($this->data['username'])) {
				$this->set('return', 'error: missing username');
				return;
			}

			$this->Domain->Client->recursive = 0;
			$client  = $this->Domain->Client->find('first', array('conditions'=>array('username' => $this->data['access_username'], 'Client.password' => $this->data['access_password'])));

			if (empty($client)) {
				$this->set('return', 'error: incorrect access_username or access_password');
				return;
			} else {
				$this->Domain->Client->User->deleteAll("User.client_id = '". $client['Client']['id']. "' AND User.username IN ('". implode("','", $this->data['username']). "')");
				$this->set('return', 'success');
			}
		}
	}

	//	temp initial offer import
	function import() {

		Configure::write('debug', 2);

		$this->Domain->Client->recursive = -1;
		//$clients = $this->Domain->Client->findAll();

		$clients = $this->Domain->Client->findAll('Client.id <= 689');

		foreach ($clients as $client) {


			/* categories */
			/*
			$sql = "INSERT INTO clients_categories (client_id, category_id) ";
			$sql .= "SELECT '". $client['Client']['id']. "', category_id FROM programs_categories WHERE programs_categories.program_id = '". $client['Client']['program_id']. "'";
			echo $sql. '<br/>';
			//$this->Domain->query($sql);


			/* products */
			//	insert all products
			$sql = "INSERT INTO tmp_clients_products (client_id, product_id) ";
			$sql .= "SELECT '". $client['Client']['id']. "', offerid FROM myr_offer";
			echo $sql. '<br/>';
			$this->Domain->query($sql);


			// delete all not for offers
			$sql = "DELETE FROM tmp_clients_products WHERE client_id = '". $client['Client']['id']. "' AND product_id IN (
			SELECT offerid FROM myr_offerexclude WHERE clientid = '". $client['Client']['id']. "')";
			echo $sql. '<br/>';
			$this->Domain->query($sql);

			// delete only for offers where not only for this client
			$sql = "DELETE FROM tmp_clients_products WHERE client_id = '". $client['Client']['id']. "' AND product_id IN (
			SELECT offerid FROM myr_offerinclude WHERE clientid != '". $client['Client']['id']. "')";
			echo $sql. '<br/>';
			$this->Domain->query($sql);

		}

		// delete non existing offers
		$sql = "DELETE FROM tmp_clients_products WHERE product_id NOT IN (SELECT id FROM products)";
		echo $sql. '<br/>';
		$this->Domain->query($sql);

		// delete duplicates
		$sql = "CREATE  TABLE bad_tmp_clients_products AS SELECT DISTINCT client_id, product_id FROM tmp_clients_products";
		echo $sql. '<br/>';
		$this->Domain->query($sql);

		$sql = "DELETE FROM tmp_clients_products;";
		echo $sql. '<br/>';
		$this->Domain->query($sql);

		$sql = "INSERT INTO tmp_clients_products (client_id, product_id) SELECT client_id, product_id FROM bad_tmp_clients_products;";
		echo $sql. '<br/>';
		$this->Domain->query($sql);


		$sql = "DROP TABLE bad_tmp_clients_products";
		echo $sql. '<br/>';
		$this->Domain->query($sql);
		die();

	}

}
?>