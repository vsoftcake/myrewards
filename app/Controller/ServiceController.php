<?php
class ServiceController extends AppController
{
    public $uses = array('UserService');
    public $components = array('Soap');
    /**
     * Handle SOAP calls
     */
    function call($model)
    {
		Configure::write('debug', 2);
		header('Content-type: text/xml');
		define ('BASE', $this->base);
		$this->autoRender = FALSE;
		$this->Soap->handle($model, 'wsdl');
    }
    /**
     * Provide WSDL for a model
     */
    function wsdl($model=null)
    {
		Configure::write('debug', 2);
		header('Content-type: text/xml');
		define ('BASE', $this->base);
		$this->autoRender = FALSE;
		echo $this->Soap->getWSDL($model, 'call');
    }
}
?>