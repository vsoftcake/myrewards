<?php
class Skyscraper extends AppModel
{
	var $hasAndBelongsToMany = array(
	'Client' => array('className' => 'Client',
						'joinTable' => 'clients_skyscrapers',
						'foreignKey' => 'skyscraper_id',
						'associationForeignKey' => 'client_id',
						'conditions' => '',
						'fields' => '',
						'order' => '',
						'limit' => '',
						'offset' => '',
						'unique' => '',
						'finderQuery' => '',
						'deleteQuery' => '',
						'insertQuery' => ''),
	'Program' => array('className' => 'Program',
						'joinTable' => 'programs_skyscrapers',
						'foreignKey' => 'skyscraper_id',
						'associationForeignKey' => 'program_id',
						'conditions' => '',
						'fields' => '',
						'order' => '',
						'limit' => '',
						'offset' => '',
						'unique' => '',
						'finderQuery' => '',
						'deleteQuery' => '',
						'insertQuery' => ''),
	);
}
?>