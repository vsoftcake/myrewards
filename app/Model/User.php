<?php
class User extends AppModel {

	var $name = 'User';
	
	public $validate = array(
			'username' => array(
				'mustNotEmpty'=>array('rule' => 'notEmpty'),
				//'unique'=>array('rule' => 'isUnique')
			),
			'email' => array('rule'=>'email'),
			'first_name' => array('rule'=>'notEmpty'),
			'last_name' => array('rule'=>'notEmpty'),
			'client_id' => array('rule'=>'notEmpty')
			);
	
	//The Associations below have been created with all possible keys, those that are not needed can be removed
	public $belongsTo = array(
			'Client' => array('className' => 'Client',
								'foreignKey' => 'client_id',
								'conditions' => '',
								'fields' => '',
								'order' => '',
								'counterCache' => ''),
	);
	
	var $hasMany = array('UserAction');
	
	function beforeSave() {
		
		//	add the unique id if we are creating a new record
		if ($this->id == null ) {
			$this->data['User']['unique_id'] = md5(uniqid(rand(), true));
		}
		
		return true;
	}
	
}
?>