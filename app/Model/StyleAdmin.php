<?php
class StyleAdmin extends AppModel {

	var $useTable = false;

	function paginateCount($conditions = null, $recursive = null) {
			$sql = "SELECT count(DISTINCT Style.id) AS count FROM styles as Style, clients as Client, clients_dashboards as ClientsDashboard ";
			$sql .= str_replace('group by Style.id', '', $conditions['search']['sql']);

		$count = $this->query($sql);
    	return $count[0][0]['count'];
	}

	function paginate($conditions = null, $fields = null, $order = null, $limit = null, $page = 1, $recursive = null) {

		if ($page <= 0) {
			$page = 1;
		}

		$sql = "SELECT Style.* FROM styles as Style, clients as Client, clients_dashboards as ClientsDashboard ";

		$sql .= $conditions['search']['sql'];

		$sql .= " LIMIT ". (($page -1) * $limit). ", ". $limit;

		return $this->query($sql);
	}
}
?>