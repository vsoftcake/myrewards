<?php
class Newsletter extends AppModel {
	var $order = 'Newsletter.created';

	public $validate = array(
			'name' => array(
				'mustNotEmpty'=>array('rule' => 'notEmpty')
			));

		//The Associations below have been created with all possible keys, those that are not needed can be removed
	var $belongsTo = array(
			'Program' => array('className' => 'Program',
								'foreignKey' => 'program_id',
								'conditions' => '',
								'fields' => '',
								'order' => '',
								'counterCache' => ''),
			'User' => array('className' => 'User',
								'foreignKey' => 'user_id',
								'conditions' => '',
								'fields' => '',
								'order' => '',
								'counterCache' => ''),
	);

	var $hasAndBelongsToMany = array(
			'Client' => array('className' => 'Client',
						'joinTable' => 'newsletters_clients',
						'foreignKey' => 'newsletter_id',
						'associationForeignKey' => 'client_id',
						'conditions' => '',
						'fields' => '',
						'order' => '',
						'limit' => '',
						'offset' => '',
						'unique' => '',
						'finderQuery' => '',
						'deleteQuery' => '',
						'insertQuery' => ''),
	);

	var $hasMany = array(
				'NewslettersProduct' => array('className' => 'NewslettersProduct',
								'foreignKey' => 'newsletter_id',
								'conditions' => '',
								'fields' => '',
								'order' => 'NewslettersProduct.id',
								'limit' => '',
								'offset' => '',
								'dependent' => '',
								'exclusive' => '',
								'finderQuery' => '',
								'counterQuery' => ''),
				'NewslettersShortProduct' => array('className' => 'NewslettersShortProduct',
								'foreignKey' => 'newsletter_id',
								'conditions' => '',
								'fields' => '',
								'order' => 'NewslettersShortProduct.id',
								'limit' => '',
								'offset' => '',
								'dependent' => '',
								'exclusive' => '',
								'finderQuery' => '',
								'counterQuery' => ''),
				'NewslettersEmail' => array('className' => 'NewslettersEmail',
								'foreignKey' => 'newsletter_id',
								'conditions' => '',
								'fields' => '',
								'order' => '',
								'limit' => '',
								'offset' => '',
								'dependent' => '',
								'exclusive' => '',
								'finderQuery' => '',
								'counterQuery' => ''),
				'NewsletterState',
				'NewslettersTempClient',
				'NewslettersTempState'
	);
}
?>