<?php
class Cart extends AppModel {
	function paginateCount($conditions = null, $recursive = null) {
		$sql = "SELECT count(*) as count FROM products as Product , cart_products as CartProduct where CartProduct.product_id=Product.id
		and CartProduct.product_type='Pay' ";
		$product_count = $this->query($sql);
		return $product_count[0][0]['count'];
	}

	function paginate($conditions = null, $fields = null, $order = null, $limit = null, $page = 1, $recursive = null) {

		if ($page <= 0) {
			$page = 1;
		}

		$sql = "SELECT * FROM products as Product , cart_products as CartProduct where CartProduct.product_id=Product.id
		and CartProduct.product_type='Pay'";

		$sql .= $conditions['search']['sql'];

		$sql .= " LIMIT ". (($page -1) * $limit). ", ". $limit;

		return $this->query($sql);
	}

	var $hasMany = array(
			'CartItem' => array('className' => 'CartItem',
					'foreignKey' => 'cart_id',
					'conditions' => '',
					'fields' => '',
					'order' => '',
					'limit' => '',
					'offset' => '',
					'dependent' => '',
					'exclusive' => '',
					'finderQuery' => '',
					'counterQuery' => ''),
	);
}
?>