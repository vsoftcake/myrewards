<?php
class Merchant extends AppModel {

	public $validate = array(
		'name' => array('rule'=>'notEmpty'),
		'contact' => array('rule'=>'notEmpty'),
		'address1' => array('rule'=>'notEmpty'),
		'address2' => array('rule'=>'notEmpty'),
		'suburb' => array('rule'=>'notEmpty'),
		'state' => array('rule'=>'notEmpty'),
		'postcode' => array('rule'=>'notEmpty')
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed
	var $hasMany = array(
			'Product' => array('className' => 'Product',
								'foreignKey' => 'merchant_id',
								'conditions' => '',
								'fields' => '',
								'order' => '',
								'limit' => '',
								'offset' => '',
								'dependent' => '',
								'exclusive' => '',
								'finderQuery' => '',
								'counterQuery' => ''),
			'MerchantAddress' => array('className' => 'MerchantAddress',
								'foreignKey' => 'merchant_id',
								'conditions' => '',
								'fields' => '',
								'order' => '',
								'limit' => '',
								'offset' => '',
								'dependent' => '',
								'exclusive' => '',
								'finderQuery' => '',
								'counterQuery' => ''),
			'MerchantComment' => array(
								'order' => 'MerchantComment.created DESC',
								),
	);

}
?>