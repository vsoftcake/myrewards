<?php
class Wishlist extends AppModel {
	function paginateCount($conditions = null, $recursive = null) {
			$sql = "SELECT count(DISTINCT Product.id) AS count from products as Product, wishlists as Wishlist, merchants as Merchant, 
			merchant_addresses as MerchantAddress, clients_products as ClientsProduct, products_addresses as ProductsAddress ";
			$sql .= str_replace('group by Product.id , MerchantAddress.merchant_id', '', $conditions['search']['sql']);

		$product_count = $this->query($sql);
    	return $product_count[0][0]['count'];
	}
	
	function paginate($conditions = null, $fields = null, $order = null, $limit = null, $page = 1, $recursive = null) {

		if ($page <= 0) {
			$page = 1;
		}
        
		$sql = " select count(Product.id) as count, Product.*, Merchant.*, MerchantAddress.*, Wishlist.* from products as Product, wishlists as Wishlist, merchants as Merchant, 
		merchant_addresses as MerchantAddress, clients_products as ClientsProduct, products_addresses as ProductsAddress ";

		$sql .= $conditions['search']['sql'];

		$sql .= " LIMIT ". (($page -1) * $limit). ", ". $limit;

		return $this->query($sql);
	}
}
?>