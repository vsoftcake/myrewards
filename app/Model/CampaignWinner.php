<?php
class CampaignWinner extends AppModel {

	function paginateCount($conditions = null, $recursive = null) {

				$sql = "SELECT count(Campaign.id) AS count FROM campaigns as Campaign, campaign_winners as CampaignWinner, campaign_entries as CampaignEntry  ";

				$sql .=  $conditions['search']['sql'];
				$count = $this->query($sql);
		    	return $count[0][0]['count'];
		}

		function paginate($conditions = null, $fields = null, $order = null, $limit = null, $page = 1, $recursive = null) {

			if ($page <= 0) {
				$page = 1;
			}

			$sql = "SELECT * FROM campaign_winners as CampaignWinner, campaigns as Campaign, campaign_entries as CampaignEntry  ";

			$sql .= $conditions['search']['sql'];

			$sql .= " LIMIT ". (($page -1) * $limit). ", ". $limit;

			return $this->query($sql);
	}

}
