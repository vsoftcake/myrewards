<?php
class Merchantlist extends AppModel {
	var $useTable = 'merchants';
	function paginateCount($conditions = null) {
			$sql = "SELECT count(DISTINCT Merchant.id) AS count from merchants as Merchant, merchant_addresses as MerchantAddress ";
			$sql .= str_replace('group by Merchant.id', '', $conditions['search']['sql']);
			
		$merchant_count = $this->query($sql);
    	return $merchant_count[0][0]['count'];
	}
	
	function paginate($conditions = null, $fields = null, $order = null, $limit = null, $page = 1, $recursive = null) {

		if ($page <= 0) {
			$page = 1;
		}
        
		$sql = " select MerchantAddress.*, Merchant.* from merchants as Merchant, merchant_addresses as MerchantAddress ";

		$sql .= $conditions['search']['sql'];

		$sql .= " LIMIT ". (($page -1) * $limit). ", ". $limit;

		return $this->query($sql);
	}
}
?>