<?php
/**
 * Application model for CakePHP.
 *
 * This file is application-wide model file. You can put all
 * application-wide model-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Model
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Model', 'Model');

/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       app.Model
 */
class AppModel extends Model {
	/**
	 * Get Enum Values
	 * Snippet v0.1.3
	 * http://cakeforge.org/snippet/detail.php?type=snippet&id=112
	 *
	 * Gets the enum values for MySQL 4 and 5 to use in selectTag()
	 */
	function getEnumValues($columnName=null) {
		if ($columnName==null) {
			return array();
		} //no field specified
	
		//Get the name of the table
		$db =& ConnectionManager::getDataSource($this->useDbConfig);
		$tableName = $db->fullTableName($this, false);
	
		//Get the values for the specified column (database and version specific, needs testing)
		$result = $this->query("SHOW COLUMNS FROM {$tableName} LIKE '{$columnName}'");
	
		//figure out where in the result our Types are (this varies between mysql versions)
		$types = null;
		if     ( isset( $result[0]['COLUMNS']['Type'] ) ) {
			$types = $result[0]['COLUMNS']['Type'];
		} //MySQL 5
		elseif ( isset( $result[0][0]['Type'] ) )         {
			$types = $result[0][0]['Type'];
		} //MySQL 4
		else   { return array();
		} //types return not accounted for
	
		//Get the values
		$values = explode("','", preg_replace("/(enum)\('(.+?)'\)/","\\2", $types) );
	
		//explode doesn't do assoc arrays, but cake needs an assoc to assign values
		$assoc_values = array();
		foreach ( $values as $value ) {
			//leave the call to humanize if you want it to look pretty
			$assoc_values[$value] = Inflector::humanize($value);
		}
	
		return $assoc_values;
	
	} //end getEnumValues
	
	/*
	 * from http://othy.wordpress.com/2006/06/03/generatenestedlist/
	*/
	function generateNestedList($conditions = null, $indent = '--', $sort = null, $key_path = null, $value_path = null) {
	
		if ($key_path == null) {
			$key_path = 'id';
		}
	
		if ($value_path == null) {
			$value_path = 'name';
		}
	
		$cats = $this->findAllThreaded($conditions, array($this->name . '.'. $key_path, $this->name . '.'. $value_path, $this->name . '.parent_id'), $sort);
		$glist = $this->_generateNestedList($cats, $indent, 0, $key_path, $value_path);
		return $glist;
	}
	/*
	 * from http://othy.wordpress.com/2006/06/03/generatenestedlist/
	*/
	function _generateNestedList($cats,$indent,$level = 0, $key_path, $value_path) {
	
		static $list = array();
		for($i = 0, $c = count($cats); $i < $c; $i++) {
			$list[$cats[$i][$this->name][$key_path]] = str_repeat($indent, $level) . $cats[$i][$this->name][$value_path];
			if(isset($cats[$i]['children']) && !empty($cats[$i]['children'])) {
				$this->_generateNestedList($cats[$i]['children'], $indent, $level + 1, $key_path, $value_path);
			}
		}
		return $list;
	}
	
	// If you need to disable validation for particular columns, you may populate this variable like so:
	// $this->User->disabledValidate = array('email', 'password'); // disables validation on email and password columns
	// $this->User->disabledValidate = array(
	//   'email',
	//   'password' => array('confirmed', 'required')
	// ); // disables validation on email column, and password['confirmed'] && password['required']
	var $disabledValidate;
	
	function loadValidation() {
		// placeholder for overloading
	
	}
	
	function invalidFields($data = array()) {
	
		$this->loadValidation();
	
		if (!$this->beforeValidate()) {
			return false;
		}
	
		if (is_array($this->disabledValidate)) {
			foreach($this->disabledValidate as $field => $params) {
				if (is_string($field) && is_array($params)) {
					foreach($params as $param) {
						if (is_string($param)) {
							$this->validate[$field][$param] = false;
						}
					}
				} else if (is_int($field) && is_string($params)) {
					$this->validate[$params] = false;
				}
			}
		}
	
		if (!isset($this->validate) || !empty($this->validationErrors)) {
			if (!isset($this->validate)) {
				return true;
			} else {
				return $this->validationErrors;
			}
		}
	
		if (isset($this->data)) {
			$data = array_merge($data, $this->data);
		}
	
		$errors = array();
		$this->set($data);
	
		foreach ($data as $table => $field) {
			foreach ($this->validate as $field_name => $validators) {
				if ($validators && is_array($validators)) {
					foreach($validators as $type => $validator) {
						if (isset($validator['method'])) {
							if (method_exists($this, $validator['method'])) {
								$parameters = (isset($validator['parameters'])) ? $validator['parameters'] : array();
								$parameters['var'] = $field_name;
								if (isset($data[$table][$field_name]) &&
										!call_user_func_array(array(&$this, $validator['method']),array($parameters))) {
									if (!isset($errors[$field_name])) {
										$errors[$field_name][$type] = isset($validator['message']) ? $validator['message'] : 1;
									}
								}
							} else {
								if (isset($data[$table][$field_name]) &&
										!preg_match($validator['method'], $data[$table][$field_name])) {
									if (!isset($errors[$field_name])) {
										$errors[$field_name][$type] = isset($validator['message']) ? $validator['message'] : 1;
									}
								}
							}
						}
					}
				}
			}
		}
		$this->validationErrors = $errors;
		return $errors;
	}
	
	// validation methods
	function isUnique($params) {
		$val = $this->data[$this->name][$params['var']];
		$db = $this->name . '.' . $params['var'];
		$id = $this->name . '.id';
		if($this->id == null ) {
			return(!$this->hasAny(array($db => $val ) ));
		} else {
			return(!$this->hasAny(array($db => $val, $id => '!='.$this->data[$this->name]['id'] ) ) );
		}
	}
	
	function isLengthWithin($params) {
		$val = $this->data[$this->name][$params['var']];
		$length = strlen($val);
		if (array_key_exists('min', $params) && array_key_exists('max', $params)) {
			return $length >= $params['min'] && $length <= $params['max'];
		} else if (array_key_exists('min', $params)) {
			return $length >= $params['min'];
		} else if (array_key_exists('max', $params)) {
			return $length <= $params['max'];
			echo $length;
		}
	}
	
	function isConfirmed($params) {
		$val = $this->data[$this->name][$params['var']];
		$val_confirmation = array_key_exists('confirm_var', $params) ?
		$this->data[$this->name][$params['confirm_var']] :
		$this->data[$this->name][$params['var'].'_confirmation'];
		return $val == $val_confirmation;
	}
	
	function isTrue($params) {
		if ($this->data[$this->name][$params['var']] == '1') {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Check that there is only one row in the table with the given fiels set to 1
	 */
	function isSingle($params) {
		$val = $this->data[$this->name][$params['var']];
		if ($val == '1') {
	
			$db = $this->name . '.' . $params['var'];
			$id = $this->name . '.id';
			if($this->id == null ) {
				return(!$this->hasAny(array($db => $val ) ));
			} else {
				return(!$this->hasAny(array($db => $val, $id => '!='.$this->data[$this->name]['id'] ) ) );
			}
		} else {
			return true;
		}
	}
	
	/**
	 * If a field is not empty, check it is a valid email address
	 */
	function isEmptyOrEmail($params) {
	
		if ($this->data[$this->name][$params['var']] == '') {
			return true;
		} else {
			return preg_match(VALID_EMAIL, $this->data[$this->name][$params['var']]);;
		}
	
	}
	
	/**
	 * Always return true for a validateion Use as a dummy validateion to validate in the controller
	 */
	function  valid($params) {
		return true;
	}
	
	function find($type, $options = array())
	{
		switch ($type)
		{
			case 'superlist':
				$total_fields = count($options['fields']);
	
				if(!isset($options['fields']) || $total_fields < 3)
				{
					return parent::find('list', $options);
				}
				if(!isset($options['separator']))
				{
					$options['separator'] = ' ';
				}
	
				if(!isset($options['format']))
				{
					$options['format'] = '%s';
					for($i = 2; $i<$total_fields;$i++)
					{
						$options['format'] .= "{$options['separator']}%s";
					}
				}
	
				$options['recursive'] = -1;
				$list = parent::find('all', $options);

				$formatVals = array();
				$formatVals[0] = $options['format'];
				for($i = 1; $i < $total_fields; $i++)
				{
					$formatVals[$i] = "{n}.{$this->alias}.".str_replace("{$this->alias}.", '', $options['fields'][$i]);
				}

				return Set::combine($list,"{n}.{$this->alias}.{$this->primaryKey}",$formatVals);
				break;
	
			default:
				return parent::find($type, $options);
				break;
		}
	}
}
