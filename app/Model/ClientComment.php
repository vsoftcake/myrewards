<?php
class ClientComment extends AppModel {
	var $order = 'ClientComment.created DESC';
	
	var $belongsTo = array(
			'Client',
			'User'
	);

	public $validate = array(
			'description' => array('rule' => 'notEmpty'));
}
?>