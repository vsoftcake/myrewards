<?php
class ProductsSaf extends AppModel {
	function paginateCount($conditions = null, $recursive = null) {
		$sql = "SELECT count(*) as count FROM products as Product , products_safs as ProductsSaf where ProductsSaf.product_id=Product.id";
		$product_count = $this->query($sql);
		return $product_count[0][0]['count'];
	}
	
	function paginate($conditions = null, $fields = null, $order = null, $limit = null, $page = 1, $recursive = null) {
	
		if ($page <= 0) {
			$page = 1;
		}
	
		$sql = "SELECT * FROM products as Product , products_safs as ProductsSaf where ProductsSaf.product_id=Product.id";
	
		$sql .= $conditions['search']['sql'];
	
		$sql .= " LIMIT ". (($page -1) * $limit). ", ". $limit;
	
		return $this->query($sql);
	}

}
?>