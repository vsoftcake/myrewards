<?php
class Style extends AppModel {
	var $order = 'Style.name';

	public $validate = array(
			'name' => array(
				'mustNotEmpty'=>array('rule' => 'notEmpty'),
				//'unique'=>array('rule' => 'isUnique')
			));
	//The Associations below have been created with all possible keys, those that are not needed can be removed
	var $hasMany = array(
			'ClientDashboard' => array('className' => 'Client',
								'foreignKey' => 'style_id',
								'conditions' => '',
								'fields' => '',
								'order' => '',
								'limit' => '',
								'offset' => '',
								'dependent' => '',
								'exclusive' => '',
								'finderQuery' => '',
								'counterQuery' => ''),
			'Program' => array('className' => 'Program',
								'foreignKey' => 'style_id',
								'conditions' => '',
								'fields' => '',
								'order' => '',
								'limit' => '',
								'offset' => '',
								'dependent' => '',
								'exclusive' => '',
								'finderQuery' => '',
								'counterQuery' => ''),
	);

}
?>