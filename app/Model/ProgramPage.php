<?php
class ProgramPage extends AppModel {

	//The Associations below have been created with all possible keys, those that are not needed can be removed
	public $belongsTo = array(
		'Program' => array('className' => 'Program',
						'foreignKey' => 'program_id',
						'conditions' => '',
						'fields' => '',
						'order' => 'ProgramPage.sort, ProgramPage.title',
						'counterCache' => ''),
		'Page' => array('className' => 'Page',
						'foreignKey' => 'page_id',
						'conditions' => '',
						'fields' => '',
						'order' => 'ProgramPage.sort, ProgramPage.title',
						'counterCache' => ''));
}
?>