<?php
class UserNominate extends AppModel {
	var $belongsTo = array(
		'User' => array('className' => 'User', 'foreignKey' => 'user_id', 'fields' => 'country'),
		'Client' => array('className' => 'Client', 'foreignKey' => 'clientid', 'fields' => 'name')
	);
}
?>