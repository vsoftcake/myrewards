<?php
class Advt4FreeSearch extends AppModel {
	var $useTable = 'advt4_frees';
	function paginateCount($conditions = null, $recursive = null) {
		$sql = "SELECT count(Advt4Free.id) AS count from advt4_frees as Advt4Free ";
		$sql.= $conditions['search']['sql'];
		$count = $this->query($sql);
		return $count[0][0]['count'];
	}
	
	function paginate($conditions = null, $fields = null, $order = null, $limit = null, $page = 1, $recursive = null) {
		if ($page <= 0) {
			$page = 1;
		}
		$sql = "SELECT Advt4Free.* from advt4_frees as Advt4Free  ";
		$sql .= $conditions['search']['sql'];
		$sql .= " LIMIT ". (($page -1) * $limit). ", ". $limit;
		
		return $this->query($sql);
	}
}?>