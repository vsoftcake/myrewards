<?php
class CommentList extends AppModel {
	var $useTable = 'comments';

	function paginateCount($conditions = null, $recursive = null) {
			
			$sql = $conditions['search']['count'];
			
			$sql .= str_replace('group by Comment.id', '', $conditions['search']['sql']);
			
			$product_count = $this->query($sql);
	    	return $product_count[0][0]['count'];
	}

	function paginate($conditions = null, $fields = null, $order = null, $limit = null, $page = 1, $recursive = null) {

		if ($page <= 0) {
			$page = 1;
		}

		$sql = $conditions['search']['select'];

		$sql .= $conditions['search']['sql'];

		$sql .= " LIMIT ". (($page -1) * $limit). ", ". $limit;

		return $this->query($sql);
	}
}
?>