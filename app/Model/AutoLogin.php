<?php
class AutoLogin extends AppModel {

	public $validate = array(
			'domain_url' => array(
				'mustNotEmpty'=>array('rule' => 'notEmpty'),
				//'unique'=>array('rule' => 'isUnique')
			),
			'domain_id' => array('rule'=>'notEmpty'),
			'client_id' => array('rule'=>'notEmpty'));

	function paginateCount($conditions = null, $recursive = null) {
		$sql = "SELECT count(*) as count FROM clients as Client , domains as Domain, auto_logins as AutoLogin  ";
		$sql .= $conditions['search']['sql'];
		$count = $this->query($sql);
		return $count[0][0]['count'];
	}

	function paginate($conditions = null, $fields = null, $order = null, $limit = null, $page = 1, $recursive = null) {

		if ($page <= 0) {
			$page = 1;
		}

		$sql = "SELECT * FROM clients as Client , domains as Domain, auto_logins as AutoLogin ";

		$sql .= $conditions['search']['sql'];

		$sql .= " LIMIT ". (($page -1) * $limit). ", ". $limit;
		return $this->query($sql);
	}

}
?>