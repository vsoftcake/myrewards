<?php
class ClientProductList extends AppModel {
	var $useTable = false;
	
	function paginateCount($conditions = null, $recursive = null) {
			$sql = "SELECT count(DISTINCT Product.id) AS count from products as Product, merchants as Merchant, 
			clients_products as ClientsProduct, clients as Client ";
			$sql .= $conditions['search']['sql'];
			
		$product_count = $this->query($sql);
    	return $product_count[0][0]['count'];
	}
	
	function paginate($conditions = null, $fields = null, $order = null, $limit = null, $page = 1, $recursive = null) {

		if ($page <= 0) {
			$page = 1;
		}
        
		$sql = " select Product.name, Product.id, Merchant.name, Client.name, Client.id from products as Product, merchants as Merchant, 
			clients_products as ClientsProduct, clients as Client ";

		$sql .= $conditions['search']['sql'];

		$sql .= " LIMIT ". (($page -1) * $limit). ", ". $limit;

		return $this->query($sql);
	}
}
?>