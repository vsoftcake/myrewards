<?php
class CategoryProductList extends AppModel {
	var $useTable = 'categories';
	function paginateCount($conditions = null, $recursive = null) {
   		
		//	get the products count
		$sql = "SELECT count(DISTINCT Product.id, Merchant.id, MerchantAddress.id) as count FROM products AS Product
					INNER JOIN categories_products AS CategoriesProduct ON Product.id = CategoriesProduct.product_id 
					INNER JOIN clients_categories AS ClientsCategory ON CategoriesProduct.category_id = ClientsCategory.category_id AND ClientsCategory.client_id = '". $conditions['Client']['id']. "' 
					INNER JOIN clients_products ON Product.id = clients_products.product_id AND clients_products.client_id = '". $conditions['Client']['id']. "'
					LEFT JOIN merchant_addresses AS MerchantAddress ON Product.merchant_id = MerchantAddress.merchant_id AND MerchantAddress.primary =1
					LEFT JOIN merchants AS Merchant ON Product.merchant_id = Merchant.id
					WHERE Product.special_offer =  '". $conditions['Product']['special_offer']. "'
					AND Product.active = 1
					AND (Product.expires = '0000-00-00' OR Product.expires >= NOW())
					AND CategoriesProduct.category_id = '". $conditions['Category']['id']. "'";
		$product_count = $this->query($sql);
    	return $product_count[0][0]['count'];
	}

	function paginate($conditions = null, $fields = null, $order = null, $limit = null, $page = 1, $recursive = null) {
		
		if ($page <= 0) {
			$page = 1;
		}
		
		//	get the products
		$sql = "SELECT DISTINCT Product.*, Merchant.*, MerchantAddress.* FROM products AS Product
					INNER JOIN categories_products AS CategoriesProduct ON Product.id = CategoriesProduct.product_id 
					INNER JOIN clients_categories AS ClientsCategory ON CategoriesProduct.category_id = ClientsCategory.category_id AND ClientsCategory.client_id = '". $conditions['Client']['id']. "' 
					INNER JOIN clients_products ON Product.id = clients_products.product_id AND clients_products.client_id = '". $conditions['Client']['id']. "'
					LEFT JOIN merchant_addresses AS MerchantAddress ON Product.merchant_id = MerchantAddress.merchant_id AND MerchantAddress.primary =1
					LEFT JOIN merchants AS Merchant ON Product.merchant_id = Merchant.id
					WHERE Product.special_offer =  '". $conditions['Product']['special_offer']. "'
					AND Product.active = 1
					AND (Product.expires = '0000-00-00' OR Product.expires >= NOW())
					AND CategoriesProduct.category_id = '". $conditions['Category']['id']. "' 
					ORDER BY Product.search_weight DESC, MerchantAddress.name
					LIMIT ". (($page -1) * $limit). ", ". $limit;
					
		return $this->query($sql);
	} 

}
?>