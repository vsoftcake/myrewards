<?php
class UserService extends AppModel
{
    public $useTable = 'users';

	public $belongsTo = array(
			'Client' => array('className' => 'Client',
								'foreignKey' => 'client_id',
								'conditions' => '',
								'fields' => '',
								'order' => '',
								'counterCache' => ''),
	);
	/**
	 * Auto login a member
	 *
	  * @param string $access_username Client access username
	  * @param string $access_password Client access password
	  * @param string $username Member username
	  * @return string
	*/
	function login_member($access_username, $access_password, $username) {

		if ($access_username == '') {
			return 'error: access_username required';
		}

		if ($access_password == '') {
			return 'error: access_password required';
		}

		$this->Client->recursive = 0;
		$client  = $this->Client->find('first', array('conditions'=>array('Client.username' => $access_username, 'Client.password' => $access_password)));

		if (empty($client)) {
			return 'error: incorrect access_username or access_password';

		} else {

			$this->recursive = -1;
			$user = $this->find('first', array('conditions'=>array('username' => $username, 'client_id' => $client['Client']['id'])));

			if (!$user) {
				return 'error: username not found';
			}
			$data['UserService']['id'] = $user['UserService']['id'];
			$data['UserService']['login_string'] = md5(uniqid(rand(), true));
			$user = $this->save($data);

			return  'http://'. $client['Domain']['name']. BASE. '/users/auto_login/'. $data['UserService']['login_string'];
		}
	}

	/**
	 * Add a member and log them in
	 *
	  * @param string $access_username Client access username
	  * @param string $access_password Client access password
	  * @param string $username Member username
	  * @param string $password Member password [OPTIONAL]
	  * @param string $firstname Member firstname
	  * @param string $lastname Member lastname
	  * @param string $email Member email [OPTIONAL]
	  * @param string $state Member state [OPTIONAL]
	  * @param int $newsletter Member receive newsletter  [OPTIONAL]
	  * @return LoginStatus
	*/
	function add_member($access_username, $access_password,$username, $password='', $firstname, $lastname, $email=false, $state=false, $newsletter=false) {

		//* @return object(status=>string,loginurl=>string)

		// check for required

		/* password no longer required
		if (strlen($password) > 0 && strlen($password) < 1) {
			return array('status' => 'error: password must be 1 character long', 'login_url');
		}*/

		if (strlen($username) < 1) {
			return array('status' => 'error: username must be 1 character long', 'login_url' => '');
		}

		$this->Client->recursive = 0;
		$client  = $this->Client->find('first', array('conditions'=>array('username' => $access_username, 'Client.password' => $access_password)));

		if (empty($client)) {
			return array('status' => 'error: incorrect access_username or access_password', 'login_url' => '');
		} else {

			// check for existing admin users with the same name
			$this->recursive = 0;
			$existing_user = $this->find('first', array('conditions'=>array("UserService.username = '". $username. "' AND UserService.type = 'Administrator'")));
			if  (!empty($existing_user)) {
				return array('status' => 'error: username already exists', 'login_url' => '');
				return;
			}

			//check for existing users of other clients of this domain
			if ($client['Domain']['all_client'] == '0') {
				$this->recursive = 0;
				$users = $this->find('all', array('conditions'=>array("UserService.username = '". $username. "' AND Client.domain_id = '". $client['Client']['domain_id']. "' AND UserService.client_id != '". $client['Client']['id']. "'")));
				if  (!empty($users)) {
					return array('status' => 'error: username already exists', 'login_url' => '');
				}
			}

			$this->recursive = 0;
			$user = $this->find('first', array('conditions'=>array('UserService.username' => $username, 'Client.domain_id' => $client['Client']['domain_id'])));
			if ($user) {
				$data['UserService']['id'] = $user['UserService']['id'];

			//	add the unique key to a new user
			} else {
				$data['UserService']['unique_id'] = md5(uniqid(rand(), true));
			}

			$data['UserService']['client_id'] = $client['Client']['id'];
			$data['UserService']['domain_id'] = $client['Client']['domain_id'];
			$data['UserService']['login_string'] = md5(uniqid(rand(), true));
			$data['UserService']['username'] = $username;
			$data['UserService']['first_name'] = $firstname;
			$data['UserService']['last_name'] = $lastname;
			if ($state) {
				$data['UserService']['state'] = $state;
			}

			if (!empty($password)) {
				$data['UserService']['password'] = $password;
			}

			if ($email && eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $email)) {
				$data['UserService']['email'] = $email;
			}

			//	auto register the user
			$data['UserService']['registered'] = 1;

			//	subscribe the user ?
			if ($newsletter) {
				$data['UserService']['newsletter'] = 1;
			} else {
				$data['UserService']['newsletter'] = 0;
			}

			$user = $this->save($data);
			if (!empty($user)) {
				return array('status' => 'success', 'login_url' => 'http://'. $client['Domain']['name']. BASE. '/users/auto_login/'. $data['UserService']['login_string']);
			} else {
				return array('status' => 'error: user not added', 'login_url' => '');
			}

		}
	}

	/**
	 * Delete members
	 *
	  * @param string $access_username Client access username
	  * @param string $access_password Client access password
	  * @param string[] $usernames Array of usernames to delete
	  * @param string $username Member username
	  * @return string
	*/
	function delete_members($access_username, $access_password, $usernames) {

		// check for required
		if  (empty($usernames)) {
			return 'error: missing usernames';
		}

		$this->Client->recursive = 0;
		$client  = $this->Client->find('first', array('conditions'=>array('username' => $access_username, 'Client.password' => $access_password)));

		if (empty($client)) {
			return 'error: incorrect access_username or access_password';
		} else {

			//if ($client['Domain']['all_client'] == '0') {
				//$this->deleteAll("User.client_id = '". $client['Client']['id']. "' AND User.username IN ('". implode("','", $this->params['form']['username']). "')");
			//} else {
				$this->deleteAll("User.client_id = '". $client['Client']['id']. "' AND User.username IN ('". implode("','", $usernames). "')");
			//}
			return 'success';
		}
	}
}
?>