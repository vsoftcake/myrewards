<?php
class Voucher extends AppModel {
	var $order = 'Voucher.name';

	public $validate = array(
			'name' => array(
				'mustNotEmpty'=>array('rule' => 'notEmpty'),
				//'unique'=>array('rule' => 'isUnique')
			));

	//The Associations below have been created with all possible keys, those that are not needed can be removed
	var $hasMany = array(
			'Client' => array('className' => 'Client',
								'foreignKey' => 'voucher_id',
								'conditions' => '',
								'fields' => '',
								'order' => '',
								'limit' => '',
								'offset' => '',
								'dependent' => '',
								'exclusive' => '',
								'finderQuery' => '',
								'counterQuery' => ''),
			'Program' => array('className' => 'Program',
								'foreignKey' => 'voucher_id',
								'conditions' => '',
								'fields' => '',
								'order' => '',
								'limit' => '',
								'offset' => '',
								'dependent' => '',
								'exclusive' => '',
								'finderQuery' => '',
								'counterQuery' => ''),
	);

}
?>