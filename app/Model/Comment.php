<?php
class Comment extends AppModel {

	public $validate = array(
			'name' => array('rule' => 'notEmpty'),
			'comments' => array('rule' => 'notEmpty'));

	function paginateCount($conditions = null, $recursive = null) {
			$sql = "SELECT count(Comment.id) as count FROM comments as Comment, products as Product, merchants as Merchant, users as User ";
			$sql .= str_replace('group by Comment.id', '', $conditions['search']['sql']);
			$product_count = $this->query($sql);
	    	return $product_count[0][0]['count'];
	}

	function paginate($conditions = null, $fields = null, $order = null, $limit = null, $page = 1, $recursive = null) {

		if ($page <= 0) {
			$page = 1;
		}
        $sql = "SELECT Comment.*, Merchant.name, Product.name, User.username, User.first_name, User.last_name
        FROM comments as Comment, products as Product, merchants as Merchant, users as User ";

		$sql .= $conditions['search']['sql'];

		$sql .= " LIMIT ". (($page -1) * $limit). ", ". $limit;

		return $this->query($sql);
	}
}
?>