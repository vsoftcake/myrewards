<?php
class Category extends AppModel {

	public $validate = array(
			'name' => array('rule' => 'notEmpty'),
			'description' => array('rule' => 'notEmpty')
			);
	
	var $order = 'Category.sort, Category.name';

	var $hasAndBelongsToMany = array(
			'Client' => array('className' => 'Client',
						'joinTable' => 'clients_categories',
						'foreignKey' => 'category_id',
						'associationForeignKey' => 'client_id',
						'conditions' => '',
						'fields' => '',
						'order' => '',
						'limit' => '',
						'offset' => '',
						'unique' => '',
						'finderQuery' => '',
						'deleteQuery' => '',
						'insertQuery' => ''),
			'Program' => array('className' => 'Program',
						'joinTable' => 'programs_categories',
						'foreignKey' => 'category_id',
						'associationForeignKey' => 'program_id',
						'conditions' => '',
						'fields' => '',
						'order' => '',
						'limit' => '',
						'offset' => '',
						'unique' => '',
						'finderQuery' => '',
						'deleteQuery' => '',
						'insertQuery' => ''),
			'Product' => array('className' => 'Product',
						'joinTable' => 'categories_products',
						'foreignKey' => 'category_id',
						'associationForeignKey' => 'product_id',
						'conditions' => '',
						'fields' => '',
						'order' => '',
						'limit' => '',
						'offset' => '',
						'unique' => '',
						'finderQuery' => '',
						'deleteQuery' => '',
						'insertQuery' => ''),
	);

}
?>