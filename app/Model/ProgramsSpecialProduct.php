<?php
class ProgramsSpecialProduct extends AppModel {
	//The Associations below have been created with all possible keys, those that are not needed can be removed
	var $belongsTo = array(
			'Program' => array('className' => 'Program',
								'foreignKey' => 'program_id',
								'conditions' => '',
								'fields' => '',
								'order' => '',
								'counterCache' => ''),
			'Product' => array('className' => 'Product',
								'foreignKey' => 'product_id',
								'conditions' => '',
								'fields' => '',
								'order' => '',
								'counterCache' => ''),
	);

}
?>