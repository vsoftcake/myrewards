<?php
class Product extends AppModel {

	public $validate = array(
			'merchant_id' => array(
				'mustNotEmpty'=>array('rule' => 'notEmpty')
			));

	//The Associations below have been created with all possible keys, those that are not needed can be removed
	var $hasAndBelongsToMany = array(
			'Client' => array('className' => 'Client',
						'joinTable' => 'clients_products',
						'foreignKey' => 'product_id',
						'associationForeignKey' => 'client_id',
						'conditions' => '',
						'fields' => '',
						'order' => '',
						'limit' => '',
						'offset' => '',
						'unique' => '',
						'finderQuery' => '',
						'deleteQuery' => '',
						'insertQuery' => ''),
			'Program' => array('className' => 'Program',
						'joinTable' => 'programs_products',
						'foreignKey' => 'product_id',
						'associationForeignKey' => 'program_id',
						'conditions' => '',
						'fields' => '',
						'order' => '',
						'limit' => '',
						'offset' => '',
						'unique' => '',
						'finderQuery' => '',
						'deleteQuery' => '',
						'insertQuery' => ''),
			'Category' => array('className' => 'Category',
						'joinTable' => 'categories_products',
						'foreignKey' => 'product_id',
						'associationForeignKey' => 'category_id',
						'conditions' => '',
						'fields' => '',
						'order' => '',
						'limit' => '',
						'offset' => '',
						'unique' => '',
						'finderQuery' => '',
						'deleteQuery' => '',
						'insertQuery' => ''),
			'MerchantAddress' => array('className' => 'MerchantAddress',
						'joinTable' => 'products_addresses',
						'foreignKey' => 'product_id',
						'associationForeignKey' => 'merchant_address_id',
						'conditions' => '',
						'fields' => '',
						'order' => '',
						'limit' => '',
						'offset' => '',
						'unique' => '',
						'finderQuery' => '',
						'deleteQuery' => '',
						'insertQuery' => ''),
	);

	var $belongsTo = array(
			'Merchant' => array('className' => 'Merchant',
								'foreignKey' => 'merchant_id',
								'conditions' => '',
								'fields' => '',
								'order' => '',
								'limit' => '',
								'offset' => '',
								'dependent' => '',
								'exclusive' => '',
								'finderQuery' => '',
								'counterQuery' => ''),
	);
	var $hasMany = array(
				'ProductDeal' => array('className' => 'ProductDeal',
								'foreignKey' => 'product_id',
								'conditions' => '',
								'fields' => '',
								'order' => '',
								'limit' => '',
								'offset' => '',
								'dependent' => '',
								'exclusive' => '',
								'finderQuery' => '',
								'counterQuery' => ''),
	);
}
?>