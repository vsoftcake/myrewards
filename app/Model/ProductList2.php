<?php
class ProductList2 extends AppModel {

	public $useTable = 'products';

	function paginateCount($conditions = null, $recursive = null) {
		$dashboarid=$conditions['search']['dashboardid'];
		//	get the products count, if sorting by suburb, get multiple products per merchant address
		if ($conditions['sort'] == 'suburb')
		{
			if($dashboarid==1)
			{
			$sql = "SELECT count(DISTINCT Product.id) AS count FROM merchant_addresses as MerchantAddress, categories as Category, merchants as Merchant, products_addresses as ProductsAddress, clients_products AS ClientsProduct, products AS Product ";
			$sql .= $conditions['search']['sql'];
			}
			if($dashboarid==2)
			{
			$sql = "SELECT count(DISTINCT Product.id) AS count FROM merchant_addresses as MerchantAddress, categories as Category, merchants as Merchant, products_addresses as ProductsAddress, clients_products_safs AS ClientsProductsSaf, products AS Product ";
			$sql .= $conditions['search']['sql'];
			}
			if($dashboarid==3)
			{
			$sql = "SELECT count(DISTINCT Product.id) AS count FROM merchant_addresses as MerchantAddress, categories as Category, merchants as Merchant,
			products_points AS ProductsPoint, products_addresses as ProductsAddress, clients_products_points AS ClientsProductsPoint, products AS Product ";
			$sql .= $conditions['search']['sql'];
			}
		//	otherwise ony get the primary address
		}
		else
		{
			if($dashboarid==1)
			{
			$sql = "SELECT count(DISTINCT Product.id) AS count FROM merchant_addresses as MerchantAddress, categories as Category, merchants as Merchant, products_addresses as ProductsAddress, clients_products AS ClientsProduct, products AS Product ";
			$sql .= $conditions['search']['sql'];
			}
			if($dashboarid==2)
			{
			$sql = "SELECT count(DISTINCT Product.id) AS count FROM merchant_addresses as MerchantAddress, categories as Category, merchants as Merchant, products_addresses as ProductsAddress, clients_products_safs AS ClientsProductsSaf, products AS Product ";
			$sql .= $conditions['search']['sql'];
			}
			if($dashboarid==3)
			{
			$sql = "SELECT count(DISTINCT Product.id) AS count FROM merchant_addresses as MerchantAddress, categories as Category, merchants as Merchant,
			products_points AS ProductsPoint, products_addresses as ProductsAddress, clients_products_points AS ClientsProductsPoint, products AS Product ";
			$sql .= $conditions['search']['sql'];
			}
		}

		$product_count = $this->query($sql);
    	return $product_count[0][0]['count'];
	}

	function paginate($conditions = null, $fields = null, $order = null, $limit = null, $page = 1, $recursive = null) {
        $dashboarid=$conditions['search']['dashboardid'];
		if ($page <= 0) {
			$page = 1;
		}

		if ($conditions['sort'] == 'suburb')
		 {
		 	if($dashboarid==1)
			{
           	 $sql = "SELECT Product. * , MerchantAddress.*,Merchant.*, Category.id, Category.logo_extension FROM merchant_addresses as MerchantAddress, categories as Category, merchants as Merchant, products_addresses as ProductsAddress, clients_products AS ClientsProduct, products AS Product ";
        	}
			if($dashboarid==2)
			{
           	 $sql = "SELECT Product. * , MerchantAddress.*,Merchant.*, Category.id, Category.logo_extension FROM merchant_addresses as MerchantAddress, categories as Category, merchants as Merchant, products_addresses as ProductsAddress, clients_products_safs AS ClientsProductsSaf, products AS Product ";
        	}
			if($dashboarid==3)
			{
           	 $sql = "SELECT Product. * , MerchantAddress.*,Merchant.*, Category.id, Category.logo_extension FROM merchant_addresses as MerchantAddress,
           	 products_points AS ProductsPoint, categories as Category, merchants as Merchant, products_addresses as ProductsAddress,
           	 clients_products_points AS ClientsProductsPoint, products AS Product ";
        	}
		 }
        else
          {
          	if($dashboarid==1)
			{
          	  $sql = "SELECT DISTINCT Product. * ,Merchant.*, Category.id, Category.logo_extension, MATCH (Product.keyword,
                            Product.name,
                            Product.details,
                            Product.highlight,
                            Product.`text`,
                            Product.special_offer_headline,
                            Product.special_offer_body) AGAINST ('". $conditions['search']['keyword']. "') AS Relevance
                            FROM merchant_addresses as MerchantAddress, categories as Category, merchants as Merchant, products_addresses as ProductsAddress, clients_products AS ClientsProduct, products AS Product ";
        	}
         	if($dashboarid==2)
			{
          	  $sql = "SELECT DISTINCT Product. * ,Merchant.*, Category.id, Category.logo_extension, MATCH (Product.keyword,
                            Product.name,
                            Product.details,
                            Product.highlight,
                            Product.`text`,
                            Product.special_offer_headline,
                            Product.special_offer_body) AGAINST ('". $conditions['search']['keyword']. "') AS Relevance
                            FROM merchant_addresses as MerchantAddress, categories as Category, merchants as Merchant, products_addresses as ProductsAddress, clients_products_safs AS ClientsProductsSaf, products AS Product ";
        	}
        	if($dashboarid==3)
			{
          	  $sql = "SELECT DISTINCT Product. * ,Merchant.*, Category.id, Category.logo_extension, MATCH (Product.keyword,
                            Product.name,
                            Product.details,
                            Product.highlight,
                            Product.`text`,
                            Product.special_offer_headline,
                            Product.special_offer_body) AGAINST ('". $conditions['search']['keyword']. "') AS Relevance
                            FROM merchant_addresses as MerchantAddress, categories as Category, merchants as Merchant, products_addresses as ProductsAddress,
                            products_points AS ProductsPoint, clients_products_points AS ClientsProductsPoint, products AS Product ";
        	}
          }
        $sql .= $conditions['search']['sql'];

        //,if(merchantAddress.suburb is null or MerchantAddress.suburb='',1,0) as isnull
		//	custom ordering
		switch ($conditions['sort']) {
			case 'name' :
				$sort = array('Product.name');
				break;
			case 'suburb' :
				$sort = array('-MerchantAddress.suburb', 'Product.name');
				break;
			case 'latest' :
				$sort = array('Product.created DESC');
				break;
			case 'popular' :
				$sort = array('Product.views DESC, Product.name');
				break;
			case 'state' :
				$sort = array('MerchantAddress.state');
				break;
			default:
				$sort = array('Product.search_weight DESC', 'Product.special_offer DESC', 'Product.name');
				//$sort = array('isnull','Product.special_offer DESC', 'Product.search_weight DESC', 'Product.name');
		}
		$sql .= " GROUP BY Product.id ";
		if ($sort) {
			$sql .= " ORDER BY ". implode(',', $sort);
		}

		//$sql .= " ORDER BY rand()";
		$sql .= " LIMIT ". (($page -1) * $limit). ", ". $limit;

		return $this->query($sql);
	}

}
?>