<?php
class CampaignList extends AppModel {
	var $useTable = 'campaigns';

	function paginateCount($conditions = null, $recursive = null) {

			$sql = "SELECT count(DISTINCT Campaign.id) AS count from campaigns as Campaign, clients_campaigns as ClientsCampaign,
			programs_campaigns as ProgramCampaign ";

			$sql .= str_replace('group by Campaign.id', '', $conditions['search']['sql']);
			$count = $this->query($sql);
	    	return $count[0][0]['count'];
	}

	function paginate($conditions = null, $fields = null, $order = null, $limit = null, $page = 1, $recursive = null) {

		if ($page <= 0) {
			$page = 1;
		}

		$sql = "SELECT Campaign.* from campaigns as Campaign, clients_campaigns as ClientsCampaign, programs_campaigns as ProgramCampaign  ";

		$sql .= $conditions['search']['sql'];

		$sql .= " LIMIT ". (($page -1) * $limit). ", ". $limit;

		return $this->query($sql);
	}
}
?>