<?php
class SkyscraperAdmin extends AppModel {

	var $useTable = false;

	function paginateCount($conditions = null, $recursive = null) {
			$sql = "SELECT count(DISTINCT Skyscraper.id) AS count from skyscrapers as Skyscraper, clients as Client, clients_skyscrapers as ClientsSkyscraper ";
			$sql .= str_replace('group by Skyscraper.id', '', $conditions['search']['sql']);

		$count = $this->query($sql);
    	return $count[0][0]['count'];
	}

	function paginate($conditions = null, $fields = null, $order = null, $limit = null, $page = 1, $recursive = null) {

		if ($page <= 0) {
			$page = 1;
		}

		$sql = " select Skyscraper.* from skyscrapers as Skyscraper, clients as Client, clients_skyscrapers as ClientsSkyscraper ";

		$sql .= $conditions['search']['sql'];

		$sql .= " LIMIT ". (($page -1) * $limit). ", ". $limit;

		return $this->query($sql);
	}
}
?>