<?php
class ClientPage extends AppModel {

	//The Associations below have been created with all possible keys, those that are not needed can be removed
	var $belongsTo = array(
			'Client' => array('className' => 'Client',
								'foreignKey' => 'client_id',
								'conditions' => '',
								'fields' => '',
								'order' => 'ClientPage.sort, ClientPage.title',
								'counterCache' => ''),
			'Page' => array('className' => 'Page',
								'foreignKey' => 'page_id',
								'conditions' => '',
								'fields' => '',
								'order' => 'ClientPage.sort, ClientPage.title',
								'counterCache' => ''),
	);

}
?>