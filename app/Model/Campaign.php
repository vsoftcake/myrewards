<?php
class Campaign extends AppModel
{
	/* public $validate = array(
			'name' => array('rule' => 'notEmpty'),
			'description' => array('rule' => 'notEmpty'),
			'start_date' => array('rule' => 'notEmpty'),
			'end_date' => array('rule' => 'notEmpty'),
			'max_winners' => array('rule' => 'notEmpty'),
			'winners_per_draw' => array('rule' => 'notEmpty'),
			'campaign_type' => array('rule' => 'notEmpty'),
			'entry_start_date' => array('rule' => 'notEmpty'),
			'entry_end_date' => array('rule' => 'notEmpty'),
			'draw_type' => array('rule' => 'notEmpty'),
			'draw_start_date' => array('rule' => 'notEmpty'),
			'draw_end_date' => array('rule' => 'notEmpty'),
			'week_days' => array('rule' => 'notEmpty'),
			'notification_popup' => array('rule' => 'notEmpty'),
			'winner_message' => array('rule' => 'notEmpty'),
			'login_start_date' => array('rule' => 'notEmpty'),
			'login_end_date' => array('rule' => 'notEmpty')
			);  */

	var $hasAndBelongsToMany = array(
		'Client' => array('className' => 'Client',
						'joinTable' => 'clients_campaigns',
						'foreignKey' => 'campaign_id',
						'associationForeignKey' => 'client_id',
						'conditions' => '',
						'fields' => '',
						'order' => '',
						'limit' => '',
						'offset' => '',
						'unique' => '',
						'finderQuery' => '',
						'deleteQuery' => '',
						'insertQuery' => ''),
		'Program' => array('className' => 'Program',
						'joinTable' => 'programs_campaigns',
						'foreignKey' => 'campaign_id',
						'associationForeignKey' => 'program_id',
						'conditions' => '',
						'fields' => '',
						'order' => '',
						'limit' => '',
						'offset' => '',
						'unique' => '',
						'finderQuery' => '',
						'deleteQuery' => '',
						'insertQuery' => ''),
		'Dashboard' => array('className' => 'Dashboard',
						'joinTable' => 'campaigns_dashboards',
						'foreignKey' => 'campaign_id',
						'associationForeignKey' => 'dashboard_id',
						'conditions' => '',
						'fields' => '',
						'order' => '',
						'limit' => '',
						'offset' => '',
						'unique' => '',
						'finderQuery' => '',
						'deleteQuery' => '',
						'insertQuery' => ''),
		);
}
?>