<?php
class ProductList extends AppModel {
	var $useTable = 'products';
	
	function paginateCount($conditions = null, $recursive = null) {
   		
		//	get the products count
		$sql = "SELECT count(DISTINCT Product.id, Merchant.id, MerchantAddress.id) AS count FROM products AS Product ";
		$sql .= $conditions['Search']['sql'];
		if ($conditions['Product']['special_offer'] == 1) {
			$sql .= " AND Product.special_offer = 1 ";
		} else {
			$sql .= " AND Product.special_offer = 0 ";
		}
					
		$product_count = $this->query($sql);
    	return $product_count[0][0]['count'];
	}

	function paginate($conditions = null, $fields = null, $order = null, $limit = null, $page = 1, $recursive = null) {
		
		if ($page <= 0) {
			$page = 1;
		}
		
		$sql = "SELECT DISTINCT Product. * , Merchant.*, MerchantAddress. * FROM products AS Product ";
		$sql .= $conditions['Search']['sql'];
		
		//	always order special offers random, and limit
		if ($conditions['Product']['special_offer'] == 1) {
			$sql .= " AND Product.special_offer = 1  ";
			$sql .= " ORDER BY RAND() LIMIT ". $limit;
		} else {
			$sql .= " AND Product.special_offer = 0 ";
			$sql .= " ORDER BY Product.search_weight DESC, MerchantAddress.name ";
			$sql .= " LIMIT ". (($page -1) * $limit). ", ". $limit;
			
		}
		
		return $this->query($sql);
	} 

}
?>