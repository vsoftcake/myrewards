<?php
class Domain extends AppModel {

	public $validate = array(
			'name' => array(
				'mustNotEmpty'=>array('rule' => 'notEmpty'),
				//'unique'=>array('rule' => 'isUnique')
			));

	//The Associations below have been created with all possible keys, those that are not needed can be removed
	public $hasMany = array(
		'Client' => array('className' => 'Client',
						'foreignKey' => 'domain_id',
						'conditions' => '',
						'fields' => '',
						'order' => '',
						'limit' => '',
						'offset' => '',
						'dependent' => '',
						'exclusive' => '',
						'finderQuery' => '',
						'counterQuery' => ''),
	);

}
?>