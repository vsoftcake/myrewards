<?php
class MerchantComment extends AppModel {
	var $order = 'MerchantComment.created DESC';
	
	var $belongsTo = array(
			'Merchant',
			'User'
	);

	public $validate = array(
			'description' => array('rule' => 'notEmpty')
			);
}
?>