<?php
class Log extends AppModel {

	var $order = 'Log.created DESC';

	var $hasMany = array('LogChange');
	var $belongsTo = array('User');
}
?>