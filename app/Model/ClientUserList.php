<?php
class ClientUserList extends AppModel {
	var $useTable = false;
	
	function paginateCount($conditions = null, $recursive = null) {
			$sql = "SELECT count(DISTINCT user.id) AS count from users as user ";
			$sql .= $conditions['search']['sql'];
			
		$user_count = $this->query($sql);
    	return $user_count[0][0]['count'];
	}
	
	function paginate($conditions = null, $fields = null, $order = null, $limit = null, $page = 1, $recursive = null) {

		if ($page <= 0) {
			$page = 1;
		}
        
		$sql = " select user.id,user.username,user.client_id from users as user";

		$sql .= $conditions['search']['sql'];

		$sql .= " LIMIT ". (($page -1) * $limit). ", ". $limit;

		return $this->query($sql);
	}
}
?>