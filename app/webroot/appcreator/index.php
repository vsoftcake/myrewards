<?PHP
		/** Page_id ref = 9 **/
		/** Only one field value is required - output f1 **/
		/** $ID should match the database field client_code  **/

        $cfgProgDir = 'phpSecurePages/';
        include($cfgProgDir . "secure.php");
        require_once('config.php');
 		$fpath ='phpSecurePages/apps/'.$CLIENT_CODE.'/'; // default path
       	$upath = "/".$fpath."web/";
       	$vpath = $fpath."web/";
  		$UserLevel;
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title><? echo $rows['page_title']; ?></title>
<link rel="stylesheet" href="filemanager.css" type="text/css" />
</head>

<body>
<div id="content">
	<div id="header">
	        <img id="rotator" name="rotator" src="phpSecurePages/images/topheader.png"></img>
	</div>
<?PHP if ( $UserLevel == 1) { ?>
    <div id="maincontent">
   <?php include_once('admin_links.php');?>
        <div class="page">
		<div class="data">
        <br><br><br><br>
		  <table>
			<tr>
              <td width="200" align="center"><a href="viewdesign.php"><img src="phpSecurePages/images/reports.jpg" border="0" width="75" height="50"></a>
                        <p><a class="link_icons" href="viewdesign.php">Prospects</a></p>
             </td>
			<td width="200" align="center"><a href="app_users.php"><img src="phpSecurePages/images/user.jpg" border="0" width="75" height="50"></a>
            <p><a class="link_icons" href="app_users.php">App Users</a></p>
            </td>
               <td width="200" align="center"><a href="admin_notice.php"><img src="phpSecurePages/images/notice.jpg" border="0" width="75" height="50"></a>
                        <p><a class="link_icons" href="admin_notice.php">Notice Board</a></p>
             </td>
              <td width="200" align="center"><a href="app_pages.php"><img src="phpSecurePages/images/app_pages.jpg" border="0" width="75" height="50"></a>
                        <p><a class="link_icons" href="app_pages.php">App Pages</a></p>
             </td>
            <td width="200" align="center"><a href="mobile_redeem.php"><img src="phpSecurePages/images/mobile_redeem.jpg" border="0" width="75" height="50"></a>
                        <p><a class="link_icons" href="mobile_redeem.php">Mobile Redeem</a></p>
             </td>
			</tr>
            <tr></tr>
            </table>
            <br><br><br><br><br>
            <table height="200">
            <tr>

				</tr>
          </table>

      </div>
    </div>
   <?PHP } else if ( $UserLevel == 5) { ?>
    <div id="maincontent">
   <?php include_once('client_links.php');?>
        <div class="page">
		<div class="data">
        <br><br><br><br>
		  <table>
			    <td width="200" align="center"><a href="c_admin_notice.php"><img src="phpSecurePages/images/notice.jpg" border="0" width="75" height="50"></a>
                        <p><a class="link_icons" href="c_admin_notice.php">Notice Boards</a></p>
             </td>
              <td width="200" align="center"><a href="c_app_pages.php"><img src="phpSecurePages/images/app_pages.jpg" border="0" width="75" height="50"></a>
                        <p><a class="link_icons" href="c_app_pages.php">Content Pages</a></p>
             </td>
            </tr>
            <tr></tr>
            </table>
            <br><br><br><br><br>
            <table height="200">
            <tr>

				</tr>
          </table>

      </div>
    </div>
   <?PHP } else { ?>
     <div id="maincontent">
        <div class="page">
		<div class="data">
			<table>
			<tr>
			<td align="top"><b>Welcome to your My Rewards App Creator</b>
			<p>
			Welcome to your personal app creator. You can use this program to build your own app for your business or organisation.
			<p>
			The program allows you to customize your text, images and other relevant information that you will need to supply to create the app.
			<p>
			You will find that you cannot edit some buttons as these are set and only full administrator access will do this.
			<p>
			To get started all you need to do is follow the prompts. If you get stuck you will find information buttons and bubbles that will explain what is required or you can mail us on <a href="mailto:appmin@therewardsteam.com">appmin@therewardsteam.com</a>
			<p>
			You will also note that we have included sample app pages so you can refer to this to see how others have laid out their page.
			<p>
			Enjoy the experience and we look forward to delivering  a sample app to you within 10 days.
			<p>
			Happy Apping!
			<p>
			My Rewards Easy Apps
			</td>
            <td>      <img src="phpSecurePages/images/contact.png" width="400">
             </td>
			</tr>
			<tr><td> <a href="admin.php"><img src="phpSecurePages/images/start.png" border="0"></a></td>
				<td align=right><a href="logout.php"><img src="phpSecurePages/images/back.png" border="0"></a>
				</td></tr>
			</table>
      </div>
    </div>
    <?php } ?>
</div>
</body>
</html>
