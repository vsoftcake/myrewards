<?

/**************************************************************/
/*              phpSecurePages version 0.4 beta              */
/*              Copyright 2013 Circlex.com, Inc.              */
/*       Versions .30 and earlier coded by Paul Kruyt         */
/*                http://www.phpSecurePages.com               */
/*                                                            */
/*              Free for non-commercial use only.             */
/*               If you are using commercially,               */
/*         or using to secure your clients' web sites,        */
/*   please purchase a license at http://phpsecurepages.com   */
/*                                                            */
/**************************************************************/
/*    This is the login page that is shown when your users    */
/*      try to access protected pages. Change logo to your    */
/*                  logo on line 88 below.                    */
/**************************************************************/

// ------ create document-location variable ------
if ( ereg("php\.exe", $_SERVER['PHP_SELF']) || ereg("php3\.cgi", $_SERVER['PHP_SELF']) || ereg("phpts\.exe", $_SERVER['PHP_SELF']) || ereg("php-cgi\.exe", $_SERVER['PHP_SELF']) ) {
        $documentLocation = $_ENV['PATH_INFO'];
        }
else {
        $documentLocation = $_SERVER['PHP_SELF'];
        }
if ( $_ENV['QUERY_STRING'] ) {
        $documentLocation .= "?" . $_ENV['QUERY_STRING'];
        }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><? echo $strLoginInterface; ?></title>

<SCRIPT LANGUAGE="JavaScript">
<!--
//  ------ check form ------
function checkData() {
        var f1 = document.forms[0];
        var wm = "<? echo $strJSHello; ?>\n\r\n";
        var noerror = 1;

        // --- entered_login ---
        var t1 = f1.entered_login;
        if (t1.value == "" || t1.value == " ") {
                wm += "<? echo $strLogin; ?>\r\n";
                noerror = 0;
        }

        // --- entered_password ---
        var t1 = f1.entered_password;
        if (t1.value == "" || t1.value == " ") {
                wm += "<? echo $strPassword; ?>\r\n";
                noerror = 0;
        }

        // --- check if errors occurred ---
        if (noerror == 0) {
                alert(wm);
                return false;
        }
        else return true;
}
//-->
</SCRIPT>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><? echo $strLoginInterface; ?></title>
<style>
body{font-family:arial;background:#FFFFFF;text-align:center;}
#error{margin:1em auto;background:#FA4956;color:#FFFFFF;border:8px solid #FA4956;font-weight:bold;width:500px;text-align:center;position:relative;}
#entry{margin:2em auto;background:#fff;border:8px solid #eee;width:500px;text-align:left;position:relative;}
#entry a, #entry a:visited{color:#0283b2;}
#entry a:hover{color:#111;}
#entry h1{text-align:center;background:#3B8CCA;color:#fff;font-size:16px;padding:16px 25px;margin:0 0 1.5em 0;border-bottom:1px solid #007dab;}
#entry p{text-align:center;}
#entry div{margin:.5em 25px;background:#eee;padding:4px;text-align:right;position:relative;}
#entry label{float:left;line-height:30px;padding-left:10px;}
#entry .field{border:1px solid #ccc;width:280px;font-size:12px;line-height:1em;padding:4px;}
#entry div.submit{background:none;margin:1em 25px;text-align:center;}
#entry div.submit label{float:none;display:inline;font-size:11px;}
#entry button{border:0;padding:0 30px;height:30px;line-height:30px;text-align:center;font-size:16px;font-weight:bold;color:#fff;background:#3B8CCA;cursor:pointer;}
</style>
</head>
<body>

<!-- Place your logo here -->
        <P><IMG SRC="phpSecurePages/images/topheader.png"  ALT="app creator"></P>
<!-- Place your logo here -->

<?php
// check for error messages
if ($phpSP_message) {
        echo '<div id="error">'.$phpSP_message.'</div>';
        }
?>


<form id="entry" action="<? echo $documentLocation; ?>" METHOD="post" onSubmit="return checkData()">
    <h1><? echo $strLoginInterface; ?></h1>
    <div>
            <label for="login_username">Username</label>
            <input type="text" name="entered_login" class="field required" />
    </div>
    <div>
            <label for="login_password">Password</label>
            <input type="password" name="entered_password" class="field required" />
    </div>
    <div class="submit">
        <button type="submit">Log in</button>
    </form>
    </div>

<SCRIPT LANGUAGE="JavaScript">
<!--
document.forms[0].entered_login.select();
document.forms[0].entered_login.focus();
//-->
</SCRIPT>
</body>
</html>
