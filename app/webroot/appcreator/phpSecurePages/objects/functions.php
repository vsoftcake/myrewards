<?

/**************************************************************/
/*              phpSecurePages version 0.4 beta              */
/*              Copyright 2013 Circlex.com, Inc.              */
/*       Versions .30 and earlier coded by Paul Kruyt         */
/*                http://www.phpSecurePages.com               */
/*                                                            */
/*              Free for non-commercial use only.             */
/*               If you are using commercially,               */
/*         or using to secure your clients' web sites,        */
/*   please purchase a license at http://phpsecurepages.com   */
/*                                                            */
/**************************************************************/
/*      There are no user-configurable items on this page     */
/**************************************************************/

// functions and libraries
define('FUNCTIONS_LOADED', true);

function phpSP_random($max) {
        // create random number between 0 and $max
        srand( (double)microtime() * 1000000 );
        $r = round(rand(0, $max));
        if ($r != 0) $r = $r - 1;
        return $r;
        }


function phpSP_rotateBg() {
        // rotate background login interface
        global $backgrounds, $bgImage, $i;
        $c = count($backgrounds);
        if ($c == 0) return;
        $r =  phpSP_random($c);
        // if the background list contains an empty spot,
        // give it 10 tries to come up with a filled spot
        if ($backgrounds[$r] == '' && $i < 10) {
                $i++;
                phpSP_rotateBg();
                }
        elseif ($i >= 10) {
                if (!$bgImage || $bgImage == '') {
                        $bgImage = 'bg_lock.gif';
                        }
                }
        else {
                $bgImage = $backgrounds[$r];
                }
        return $bgImage;
        }

function admEmail() {
        // create administrators email link
        global $admEmail;
        return("<A HREF='mailto:$admEmail'>$admEmail</A>");
        }

function is_in_array($needle, $haystack) {
        // check if the value of $needle exist in array $haystack
        if ($needle && $haystack) {
                return(in_array($needle, $haystack));
                }
        else return(false);
        }
?>
