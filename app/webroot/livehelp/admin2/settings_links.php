<?php
/*
stardevelop.com Live Help
International Copyright stardevelop.com

You may not distribute this program in any manner,
modified or otherwise, without the express, written
consent from stardevelop.com

You may make modifications, but only for your own 
use and within the confines of the License Agreement.
All rights reserved.

Selling the code for this program without prior 
written consent is expressly forbidden. Obtain 
permission before redistributing this program over 
the Internet or in any other medium.  In all cases 
copyright and header must remain intact.  
*/
include('../include/database.php');
include('../include/class.mysql.php');
include('../include/class.cookie.php');
include('../include/class.aes.php');
include('../include/config.php');
include('../include/functions.php');
include('../include/auth.php');

header('Content-type: text/html; charset=utf-8');

if (file_exists('../locale/' . LANGUAGE . '/admin.php')) {
	include('../locale/' . LANGUAGE . '/admin.php');
}
else {
	include('../locale/en/admin.php');
}

include('settings_include.php');

if (isset($_REQUEST['ONLINELOGO'])){ $_SETTINGS['ONLINELOGO'] = $_REQUEST['ONLINELOGO']; }
if (isset($_REQUEST['OFFLINELOGO'])){ $_SETTINGS['OFFLINELOGO'] = $_REQUEST['OFFLINELOGO']; }
if (isset($_REQUEST['BERIGHTBACKLOGO'])){ $_SETTINGS['BERIGHTBACKLOGO'] = $_REQUEST['BERIGHTBACKLOGO']; }
if (isset($_REQUEST['AWAYLOGO'])){ $_SETTINGS['AWAYLOGO'] = $_REQUEST['AWAYLOGO']; }
if (isset($_REQUEST['LOGINDETAILS'])){ $_SETTINGS['LOGINDETAILS'] = $_REQUEST['LOGINDETAILS']; }
if (isset($_REQUEST['OFFLINEEMAIL'])){ $_SETTINGS['OFFLINEEMAIL'] = $_REQUEST['OFFLINEEMAIL']; }
if (isset($_REQUEST['OFFLINEEMAILLOGO'])){ $_SETTINGS['OFFLINEEMAILLOGO'] = $_REQUEST['OFFLINEEMAILLOGO']; }
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"> 
<html>
<head>
<title><?php echo($_SETTINGS['NAME']); ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="../styles/styles.php" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
.background {
	background-image: url(../images/background_settings.gif);
	background-repeat: no-repeat;
	background-position: right bottom;
	margin-left: 0px;
	margin-top: 0px;
}
-->
</style>
</head>
<body class="background"> 
<div align="center"> 
  <form name="UPDATE_SETTINGS" method="post" action="settings_links.php"> 
    <table width="400" border="0" align="center"> 
      <tr> 
        <td width="22"><img src="../images/configure_small.gif" alt="<?php echo($_LOCALE['managesettings']); ?> - <?php echo($_LOCALE['links']); ?>" width="22" height="22"></td> 
        <td colspan="2"><em class="heading"><?php echo($_LOCALE['managesettings']); ?> - <?php echo($_LOCALE['links']); ?></em> </td> 
      </tr> 
      <tr> 
        <td>&nbsp;</td> 
        <td colspan="2"><?php include('./settings_toolbar.php'); ?> </td> 
      </tr> 
      <tr> 
        <td>&nbsp;</td> 
        <td colspan="2"></td> 
      </tr> 
      <tr> 
        <td>&nbsp;</td> 
        <td><div align="right"><?php echo($_LOCALE['onlinelogo']); ?>: </div></td> 
        <td><input name="ONLINELOGO" type="text" id="ONLINELOGO" value="<?php echo($_SETTINGS['ONLINELOGO']); ?>" size="25"> <a href="#" class="tooltip"><img src="../images/help_dialog.gif" alt="Help" width="9" height="11" border="0"><span style="left: -150px"><?php echo($_LOCALE['onlinelogo']); ?>: <?php echo($_LOCALE['onlinelogotooltip']); ?>.</span></a></td> 
      </tr> 
      <tr> 
        <td>&nbsp;</td> 
        <td><div align="right"><?php echo($_LOCALE['offlinelogo']); ?>:</div></td> 
        <td><input name="OFFLINELOGO" type="text" id="OFFLINELOGO" value="<?php echo($_SETTINGS['OFFLINELOGO']); ?>" size="25"> <a href="#" class="tooltip"><img src="../images/help_dialog.gif" alt="Help" width="9" height="11" border="0"><span style="left: -150px"><?php echo($_LOCALE['offlinelogo']); ?>: <?php echo($_LOCALE['offlinelogotooltip']); ?>.</span></a></td> 
      </tr> 
      <tr> 
        <td>&nbsp;</td> 
        <td><div align="right"><?php echo($_LOCALE['brblogo']); ?>:</div></td> 
        <td><input name="BERIGHTBACKLOGO" type="text" id="BERIGHTBACKLOGO" value="<?php echo($_SETTINGS['BERIGHTBACKLOGO']); ?>" size="25"> <a href="#" class="tooltip"><img src="../images/help_dialog.gif" alt="Help" width="9" height="11" border="0"><span style="left: -150px"><?php echo($_LOCALE['brblogo']); ?>: <?php echo($_LOCALE['brblogotooltip']); ?>.</span></a></td> 
      </tr> 
      <tr> 
        <td>&nbsp;</td> 
        <td><div align="right"><?php echo($_LOCALE['awaylogo']); ?>:</div></td> 
        <td><input name="AWAYLOGO" type="text" id="AWAYLOGO" value="<?php echo($_SETTINGS['AWAYLOGO']); ?>" size="25"> <a href="#" class="tooltip"><img src="../images/help_dialog.gif" alt="Help" width="9" height="11" border="0"><span style="left: -150px"><?php echo($_LOCALE['awaylogo']); ?>: <?php echo($_LOCALE['awaylogotooltip']); ?>.</span></a></td> 
      </tr> 
      <tr> 
        <td>&nbsp;</td> 
        <td><div align="right"><?php echo($_LOCALE['guestlogindetails']); ?>:</div></td> 
        <td> <input name="LOGINDETAILS" type="radio" value="-1" <?php if ($_SETTINGS['LOGINDETAILS'] == true) { echo("checked"); }?>> 
          <?php echo($_LOCALE['on']); ?> 
          <input name="LOGINDETAILS" type="radio"  value="0" <?php if ($_SETTINGS['LOGINDETAILS'] == false) { echo("checked"); }?>> 
          <?php echo($_LOCALE['off']); ?> <a href="#" class="tooltip"><img src="../images/help_dialog.gif" alt="Help" width="9" height="11" border="0"><span><?php echo($_LOCALE['guestlogindetails']); ?>: <?php echo($_LOCALE['disablelogindetailstooltip']); ?>.</span></a></td> 
      </tr> 
      <tr> 
        <td>&nbsp;</td> 
        <td><div align="right"><?php echo($_LOCALE['offlineemailsetting']); ?>:</div></td> 
        <td> <input name="OFFLINEEMAIL" type="radio" value="-1" <?php if ($_SETTINGS['OFFLINEEMAIL'] == true) { echo("checked"); }?>> 
          <?php echo($_LOCALE['on']); ?> 
          <input type="radio" name="OFFLINEEMAIL" value="0" <?php if ($_SETTINGS['OFFLINEEMAIL'] == false) { echo("checked"); }?>> 
          <?php echo($_LOCALE['off']); ?> <a href="#" class="tooltip"><img src="../images/help_dialog.gif" alt="Help" width="9" height="11" border="0"><span><?php echo($_LOCALE['offlineemailsetting']); ?>: <?php echo($_LOCALE['disableofflineemailtooltip']); ?>.</span></a></td> 
      </tr> 
      <tr> 
        <td>&nbsp;</td> 
        <td><div align="right"><?php echo($_LOCALE['offlinelogowithoutemail']); ?>:</div></td> 
        <td><input name="OFFLINEEMAILLOGO" type="text" id="OFFLINEEMAILLOGO" value="<?php echo($_SETTINGS['OFFLINEEMAILLOGO']); ?>" size="25"> <a href="#" class="tooltip"><img src="../images/help_dialog.gif" alt="Help" width="9" height="11" border="0"><span style="left: -200px;width: 225px;"><?php echo($_SETTINGS['OFFLINEEMAILLOGO']); ?>: <?php echo($_LOCALE['offlinelogowithoutemailtooltip']); ?>.</span></a></td> 
      </tr> 
      <tr>
        <td>&nbsp;</td>
        <td colspan="2">&nbsp;</td>
      </tr>
      <tr> 
        <td>&nbsp;</td> 
        <td colspan="2"><div align="center"> 
            <input name="SAVE" type="hidden" id="SAVE" value="1"> 
            <input name="Submit" type="submit" id="Submit" value="<?php echo($_LOCALE['save']); ?>" <?php if ($current_privilege > 2 || $_REQUEST['SAVE'] == true) { echo('disabled="true"'); } ?>>
          </div></td> 
      </tr> 
    </table> 
  </form> 
</div> 
</body>
</html>
