<?php
/*
stardevelop.com Live Help
International Copyright stardevelop.com

You may not distribute this program in any manner,
modified or otherwise, without the express, written
consent from stardevelop.com

You may make modifications, but only for your own 
use and within the confines of the License Agreement.
All rights reserved.

Selling the code for this program without prior 
written consent is expressly forbidden. Obtain 
permission before redistributing this program over 
the Internet or in any other medium.  In all cases 
copyright and header must remain intact.  
*/

$installed = false;
$database = include('../include/database.php');
if ($database) {
	include('../include/spiders.php');
	include('../include/class.mysql.php');
	include('../include/class.cookie.php');
	include('../include/class.aes.php');
	$installed = include('../include/config.php');
	include('../include/version.php');
} else {
	include('../include/default.php');
}

if ($installed == false && file_exists('../install')) {
	header('Location: ../install/index.php');
	exit();
}


if (!isset($_REQUEST['STATUS'])){ $_REQUEST['STATUS'] = ''; }
$status = $_REQUEST['STATUS'];

header('Content-type: text/html; charset=utf-8');

if (file_exists('../locale/' . LANGUAGE . '/admin.php')) {
	include('../locale/' . LANGUAGE . '/admin.php');
}
else {
	include('../locale/en/admin.php');
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title><?php echo($_SETTINGS['NAME']) ?> - Administration</title>
<?php
if ($installed == true) {
?>
<script language="JavaScript" type="text/JavaScript">
<!--
var windowWidth = 800;
var windowHeight = 585;
var windowLeft = (screen.width - windowWidth) / 2;
var windowTop = (screen.height - windowHeight) / 2;
var size = 'height=' + windowHeight + ', width=' + windowWidth + ', top=' + windowTop + ', left=' + windowLeft + ', resizable=yes';

function loadAdmin() {
	var status = window.open('index_popup.php', '', size);
	if (!status) {
		alert('Please disable your popup blocker or click the Open New Administration Window link');
	}
}

//-->
</script>
<?php
}
?>
<link href="../styles/styles.php" rel="stylesheet" type="text/css">
</head>
<body link="#000000" vlink="#000000" alink="#000000">
<br>
<div align="center">
  <p><img src="../images/help_logo_admin.gif" alt="stardevelop.com Live Help" width="289" height="105" border="0" /></p>
  <p><?php echo($_LOCALE['adminwelcomemessage']); ?></p>
  <p>
<?php
if($status == 'authentication') {
?>
  <p><strong><?php echo($_LOCALE['authenticationsessionerror']); ?></strong>
    <?php
}
if ($installed == true) {
?>
  <p><a href="#" onClick="javascript:loadAdmin();" class="normlink"><?php echo($_LOCALE['newwindow']); ?></a>
    <?php
  }
?>
  </p>
  <p class="small"><em><?php echo($_LOCALE['adminsupportslineone']); ?><br>
    <?php echo($_LOCALE['adminsupportslinetwo']); ?></em><br>
    <em><?php echo($_LOCALE['adminsupportslinethree']); ?></em></p>
  <?php
if ($installed == true && file_exists('../install')) {
?>
  <table width="300" border="0">
    <tr>
      <td width="32"><img src="../images/error.gif" alt="<?php echo($_LOCALE['warning']); ?>" width="32" height="32"></td>
      <td><div align="center">
          <p><span class="heading"><em><?php echo($_LOCALE['securitywarning']); ?><strong></strong></em></span><em><strong><br>
            </strong><?php echo($_LOCALE['securityinstructions']); ?></em></p>
        </div></td>
    </tr>
  </table>
  <?php
}
?>
  <p class="small"><?php echo($_LOCALE['stardevelopcopyright']); ?></p>
</div>
</body>
</html>
