<?php
/*
stardevelop.com Live Help
International Copyright stardevelop.com

You may not distribute this program in any manner,
modified or otherwise, without the express, written
consent from stardevelop.com

You may make modifications, but only for your own 
use and within the confines of the License Agreement.
All rights reserved.

Selling the code for this program without prior 
written consent is expressly forbidden. Obtain 
permission before redistributing this program over 
the Internet or in any other medium.  In all cases 
copyright and header must remain intact.  
*/
include('../include/database.php');
include('../include/class.mysql.php');
include('../include/class.cookie.php');
include('../include/class.aes.php');
include('../include/config.php');
include('../include/functions.php');
include('../include/auth.php');

if ($current_privilege > 2){
	header('Location: ./denied.php');
	exit();
}

if (!isset($_REQUEST['DATE'])){ $_REQUEST['DATE'] = ''; }
if (!isset($_REQUEST['SKIP'])){ $_REQUEST['SKIP'] = 0; }
if (!isset($_REQUEST['LIMIT'])){ $_REQUEST['LIMIT'] = 0; }

$date = $_REQUEST['DATE'];
$skip = $_REQUEST['SKIP'];
$limit = $_REQUEST['LIMIT'];

list($day, $month, $year) = explode('-', $date);
$txt_date = date('d F Y', mktime(date('H') + $timezonehours, date('i') + $timezoneminutes, 0, $month, $day, $year));

header('Content-type: text/html; charset=utf-8');

if (file_exists('../locale/' . LANGUAGE . '/admin.php')) {
	include('../locale/' . LANGUAGE . '/admin.php');
}
else {
	include('../locale/en/admin.php');
}

if (LANGUAGE != 'en') {	  
	switch ($month) { 
		case 'January':
			$month = $_LOCALE['january']; 
			break;
		case 'February':
			$month = $_LOCALE['february']; 
			break;
		case 'March':
			$month = $_LOCALE['march']; 
			break;
		case 'April':
			$month = $_LOCALE['april']; 
			break;
		case 'May':
			$month = $_LOCALE['may']; 
			break;
		case 'June':
			$month = $_LOCALE['june']; 
			break;
		case 'July':
			$month = $_LOCALE['july']; 
			break;
		case 'August':
			$month = $_LOCALE['august']; 
			break;
		case 'September':
			$month = $_LOCALE['september']; 
			break;
		case 'October':
			$month = $_LOCALE['october']; 
			break;
		case 'November':
			$month = $_LOCALE['november']; 
			break;
		case 'December':
			$month = $_LOCALE['december']; 
			break;
	}
}		

if ($limit == 0) $limit = 6;

function show_reports($limit, $total, $skip) {
	if ($total_products > $limit) {
		echo('Page: ');   
		$pages = ceil(($total/$limit));
	
		for ($i = 0; $i < $pages; $i++) { 
			$page = $i+1;
			if ($skip == ($i * $limit)){
			  echo(' ' . $page . ' |'); 
			}
			else {
				echo('<a href="#" onClick="submitForm(' . $i * $limit . ');" class="normlink">' . $page . '</a> |'); 
			}
		} 
	}  
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"> 
<html>
<head>
<title><?php echo($_SETTINGS['NAME']); ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="../styles/styles.php" rel="stylesheet" type="text/css">
<script language="JavaScript" type="text/JavaScript">
<!--
function submitForm(skipped) {
	document.reports.SKIP.value = skipped;
	void(document.reports.submit());
}
/-->
</script>
<style type="text/css">
<!--
.background {
	background-image: url(../images/background_reports.gif);
	background-repeat: no-repeat;
	background-position: right bottom;
}
-->
</style>
</head>
<body class="background"> 
<div align="center"> 
  <form name="reports" method="post" action="./reports_daily_summary.php?DATE=<?php echo($date); ?>"> 
    <table border="0" cellspacing="2" cellpadding="2"> 
      <tr> 
        <td width="22"><strong><img src="../images/reports_small.gif" alt="<?php echo($_LOCALE['dailyreports']); ?>" width="22" height="22" border="0"></strong></td> 
        <td><em class="heading"><?php echo($_LOCALE['dailyreports']); ?> - <?php echo($txt_date); ?></em></td> 
      </tr> 
      <tr> 
        <td colspan="2"> <?php
	  // Count the total number of chats for the passed date
	  $query = "SELECT count(`id`) FROM " . $table_prefix . "chats WHERE DATE_FORMAT(DATE_ADD(`datetime`, INTERVAL '$timezonehours:$timezoneminutes' HOUR_MINUTE), '%d-%m-%Y') = '$date' AND (`active` > '0' OR `active` = '-3')";
	  $row = $SQL->selectquery($query); 
	  if (is_array($row)) {
			$total_chats = $row['count(`id`)'];
	  }
	  else {
			$total_chats = 0;
	  }
	  ?> 
          <table width="400" height="25" border="0" align="center" cellpadding="4" cellspacing="0"> 
            <tr height="5"> 
              <td></td> 
              <td></td> 
              <td></td> 
              <td></td> 
              <td></td> 
            </tr> 
            <tr> 
              <td></td> 
              <td><strong><?php echo($_LOCALE['username']); ?></strong></td> 
              <td><strong><?php echo($_LOCALE['department']); ?></strong></td> 
              <td><strong><?php echo($_LOCALE['rating']); ?></strong></td> 
              <td><strong><?php echo($_LOCALE['email']); ?></strong></td> 
            </tr> 
            <?php
	  $query = "SELECT `id`, `request`, `username`, `email`, `department`, `rating`, (UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(`refresh`)) AS `ttl_refresh` FROM " . $table_prefix . "chats WHERE DATE_FORMAT(DATE_ADD(`datetime`, INTERVAL '$timezonehours:$timezoneminutes' HOUR_MINUTE), '%d-%m-%Y') = '$date' AND active != '0' LIMIT $skip, $limit";
	  $rows = $SQL->selectall($query);
	  
	  $colour = false;
	  if (is_array($rows)) {
	  		foreach ($rows as $key => $row) {
				if (is_array($row)) {
				
					if ($colour == true) {
						$colour = false;
					}
					elseif ($colour == false) {
						$rgb = '#E4F2FB';
						$colour = true;
					}
					
					$login_id = $row['id'];
					$request = $row['request'];
					$username = $row['username'];
					$department = $row['department'];
					$rating = $row['rating'];
					$email = $row['email'];
					$ttl_refresh = $row['ttl_refresh'];
					
					switch ($rating) { 
						case '-1':
							$rating = $_LOCALE['unavailable']; 
							break;
						case '1':
							$rating = $_LOCALE['poor']; 
							break;
						case '2':
							$rating = $_LOCALE['fair']; 
							break;
						case '3':
							$rating = $_LOCALE['good']; 
							break;
						case '4':
							$rating = $_LOCALE['verygood']; 
							break;
						case '5':
							$rating = $_LOCALE['excellent']; 
							break;
					}

	  ?> 
            <tr<?php if($colour == false) { echo(' bgcolor="E4F2FB"'); } ?>  onMouseOver="this.style.background='#CAE6F7';" onMouseOut="this.style.background='<?php if($colour == false) { echo('E4F2FB'); } else { echo('#FFFFFF'); } ?>';" onClick="location.href = './visitors_index.php?<?php echo('REQUEST=' . $request); if($ttl_refresh < $connection_timeout) { echo('&PREVIOUS=false'); } else { echo('&PREVIOUS=true'); }  ?>';" > 
              <td><img src="../images/<?php if($ttl_refresh < $connection_timeout) { echo('mini_chatting.gif'); } else { echo('mini_chat_ended.gif'); } ?>" alt="<?php if($ttl_refresh < $connection_timeout) { echo($_LOCALE['currentlychatting']); } else { echo($_LOCALE['chatended']); } ?>"></td> 
              <td><?php echo($username); ?></td> 
              <td><?php echo($department); ?></td> 
              <td><?php echo($rating); ?></td> 
              <td><?php if ($email != '') { ?> 
                <a href="mailto:<?php echo($email); ?>" class="normlink"><?php echo($_LOCALE['sendemail']); ?></a> 
                <?php } else { echo($_LOCALE['unavailable']); } ?></td> 
            </tr> 
            <?php
						}
			}
	  }
	  ?> 
            <tr> 
              <td colspan="5"><div align="right"> 
                  <input name="SKIP" type="hidden" id="SKIP" value="<?php echo($skip); ?>"> 
                  <input name="LIMIT" type="hidden" id="LIMIT" value="<?php echo($limit); ?>"> 
                  <?php show_product_pages($limit, $total_chats, $skip); ?> 
                </div></td> 
            </tr> 
          </table></td> 
      </tr> 
    </table> 
  </form> 
</div> 
<div align="right"><a href="reports_index.php" class="normlink"><?php echo($_LOCALE['backtodailyreports']); ?></a></div>
</body>
</html>
