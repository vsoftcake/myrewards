<?php
/*
stardevelop.com Live Help
International Copyright stardevelop.com

You may not distribute this program in any manner,
modified or otherwise, without the express, written
consent from stardevelop.com

You may make modifications, but only for your own 
use and within the confines of the License Agreement.
All rights reserved.

Selling the code for this program without prior 
written consent is expressly forbidden. Obtain 
permission before redistributing this program over 
the Internet or in any other medium.  In all cases 
copyright and header must remain intact.  
*/
?>
 <table border="0" cellspacing="1" cellpadding="1"> 
  <tr> 
     <td width="50" height="22"><div align="center"><a href="./settings_index.php"><img src="../images/configure_small.gif" alt="<?php echo($_LOCALE['general']); ?>" width="22" height="22" border="0"></a></div></td> 
     <td width="50" height="22"><div align="center"><a href="./settings_display.php"><img src="../images/display.gif" alt="<?php echo($_LOCALE['display']); ?>" width="22" height="22" border="0"></a></div></td> 
     <td width="50"><div align="center"><a href="./settings_fonts.php"><img src="../images/font.gif" alt="<?php echo($_LOCALE['fonts']); ?>" width="22" height="22" border="0"></a></div></td>
     <td width="50"><div align="center"><a href="./settings_chat.php"><img src="../images/chat.gif" alt="<?php echo($_LOCALE['chat']); ?>" width="22" height="22" border="0"></a></div></td> 
     <td width="50" height="22"><div align="center"><a href="./settings_links.php"><img src="../images/network.gif" alt="<?php echo($_LOCALE['links']); ?>" width="22" height="22" border="0"></a></div></td> 
     <td width="50" height="22"><div align="center"><a href="./settings_email.php"><img src="../images/mail_send.gif" alt="<?php echo($_LOCALE['email']); ?>" width="22" height="22" border="0"></a></div></td> 
     <td width="50" height="22"><!--<div align="center"><a href="./settings_code.php"><img src="../images/code.gif" alt="<?php //echo($_LOCALE['code']); ?>" width="22" height="22" border="0"></a></div>--></td> 
   </tr> 
  <tr> 
     <td width="50"><div align="center"><span class="small"><a href="./settings_index.php" class="normlink"><?php echo($_LOCALE['general']); ?></a></span></div></td> 
     <td width="50"><div align="center"><span class="small"><a href="./settings_display.php" class="normlink"><?php echo($_LOCALE['display']); ?></a></span></div></td> 
     <td width="50"><div align="center"><span class="small"><a href="./settings_fonts.php" class="normlink"><?php echo($_LOCALE['fonts']); ?></a></span></div></td>
     <td width="50"><div align="center"><span class="small"><a href="./settings_chat.php" class="normlink"><?php echo($_LOCALE['chat']); ?></a></span></div></td> 
     <td width="50"><div align="center"><span class="small"><a href="./settings_links.php" class="normlink"><?php echo($_LOCALE['links']); ?></a></span></div></td> 
     <td width="50"><div align="center"><span class="small"><a href="./settings_email.php" class="normlink"><?php echo($_LOCALE['email']); ?></a></span></div></td> 
     <td width="50"><!--<div align="center"><span class="small"><a href="./settings_code.php" class="normlink"><?php //echo($_LOCALE['code']); ?></a></span></div>--></td> 
   </tr> 
</table> 
