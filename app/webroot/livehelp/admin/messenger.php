<?php
/*
stardevelop.com Live Help
International Copyright stardevelop.com

You may not distribute this program in any manner,
modified or otherwise, without the express, written
consent from stardevelop.com

You may make modifications, but only for your own
use and within the confines of the License Agreement.
All rights reserved.

Selling the code for this program without prior
written consent is expressly forbidden. Obtain
permission before redistributing this program over
the Internet or in any other medium.  In all cases
copyright and header must remain intact.
*/
include('../include/database.php');
include('../include/class.mysql.php');
include('../include/class.cookie.php');
include('../include/class.aes.php');
include('../include/config.php');
include('../include/functions.php');
include('../include/auth.php');

ignore_user_abort(true);

if (!isset($_REQUEST['ID'])){ $_REQUEST['ID'] = ''; }
if (!isset($_REQUEST['USER'])){ $_REQUEST['USER'] = ''; }
if (!isset($_REQUEST['STAFF'])){ $_REQUEST['STAFF'] = 0; }

$guest_login_id = $_REQUEST['ID'];
$guest_username = stripslashes($_REQUEST['USER']);
$staff = $_REQUEST['STAFF'];

header('Content-type: text/html; charset=utf-8');

if (file_exists('../locale/' . LANGUAGE . '/admin.php')) {
	include('../locale/' . LANGUAGE . '/admin.php');
}
else {
	include('../locale/en/admin.php');
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title><?php echo($_SETTINGS['NAME']); ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<script language="JavaScript" type="text/JavaScript">
<!--

function toggle(object) {
  if (document.getElementById) {
    if (document.getElementById(object).style.visibility == 'visible')
      document.getElementById(object).style.visibility = 'hidden';
    else
      document.getElementById(object).style.visibility = 'visible';
  }

  else if (document.layers && document.layers[object] != null) {
    if (document.layers[object].visibility == 'visible' ||
        document.layers[object].visibility == 'show' )
      document.layers[object].visibility = 'hidden';
    else
      document.layers[object].visibility = 'visible';
  }

  else if (document.all) {
    if (document.all[object].style.visibility == 'visible')
      document.all[object].style.visibility = 'hidden';
    else
      document.all[object].style.visibility = 'visible';
  }

  return false;
}

function currentTime() {
	var date = new Date();
	return date.getTime();
}

function isTyping() {
	var updateIsTypingStatus = new Image;
	var time = currentTime();

	var message = document.MessageForm.MESSAGE.value;
	var intLength = message.length;
	if (intLength == 0) {
		notTyping();
	}
	else {
		updateIsTypingStatus.src = '../typing.php?ID=<?php echo($guest_login_id); ?>&STATUS=1&TIME=' + time;
	}
}

function notTyping() {
	var updateNotTypingStatus = new Image;
	var time = currentTime();

	updateNotTypingStatus.src = '../typing.php?ID=<?php echo($guest_login_id); ?>&STATUS=0&TIME=' + time;
}

function swapImgRestore() { //v3.0
  var i,x,a=document.sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.p) d.p=new Array();
    var i,j=d.p.length,a=preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.p[j]=new Image; d.p[j++].src=a[i];}}
}

function findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function swapImage() { //v3.0
  var i,j=0,x,a=swapImage.arguments; document.sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=findObj(a[i]))!=null){document.sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
<link href="../styles/styles.php" rel="stylesheet" type="text/css">
<script language="JavaScript" type="text/JavaScript" src="../scripts/jquery-latest.js"></script>
<script language="JavaScript" type="text/JavaScript" src="../scripts/jquery.bubbletip.js"></script>
<script language="JavaScript" type="text/JavaScript" src="../scripts/admin.js.php"></script>
</head>
<body onFocus="parent.document.title = 'Admin <?php echo($_SETTINGS['NAME']); ?>'"; onLoad="preloadImages('../locale/<?php echo(LANGUAGE); ?>/images/send_hover.gif')">
<div align="center">
  <form action="../send.php" method="POST" name="MessageForm" target="sendMessageFrame">
    <table width="470" border="0" cellspacing="2" cellpadding="2">
      <tr>
        <td colspan="2"><span class="small"><?php echo($_LOCALE['chattingwith']); ?>&nbsp;
          <?php
		  if ($guest_login_id == '') {
		  	echo($_LOCALE['clickchatuser']);
		  }
		  else {
		  	$query = "SELECT `server`,`department` FROM `" . $table_prefix . "chats` WHERE `id` = '$guest_login_id'";
		  	$row = $SQL->selectquery($query);
		  	if (is_array($row)) {
		  	  $server = $row['server'];
		  	  $dept = $row['department'];
			  if ($server != '') {
			  	if (substr($server, 0, 7) == 'http://') {
			  		$server = substr($server, 7);
			  	}
			  	elseif (substr($server, 0, 8) == 'https://') {
			  		$server = substr($server, 8);
			  	}
				echo(' ' . $guest_username . '@' . $server . ', ' . $dept);
			  }
			  else {
			    echo(' ' . $guest_username);
			  }
		  	}
		  }

		  if ($staff == '') {
		  ?>
          </span> <img src="../locale/<?php echo(LANGUAGE); ?>/images/waiting.gif" alt="Typing Status" name="messengerStatus" width="125" height="20" id="messengerStatus"><?php } ?> </td>
      </tr>
      <tr>
        <td colspan="2"><table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
              <td width="20" height="20">&nbsp;</td>
              <td rowspan="3">
				<div align="center">
                  <textarea id="MESSAGE" name="MESSAGE" cols="38" rows="3" onKeyDown="return checkEnter(event)" onBlur="notTyping()" style="width: 320px"></textarea>
                  <a href="#" onMouseOut="swapImgRestore()" onMouseOver="swapImage('Send','','../locale/<?php echo(LANGUAGE); ?>/images/send_hover.gif',1)" onClick="processForm();"><img src="../locale/<?php echo(LANGUAGE); ?>/images/send.gif" alt="<?php echo($_LOCALE['sendmsg']); ?>" name="Send" width="58" height="50" border="0"></a>&nbsp;
<?php
if ($_SETTINGS['SMILIES'] == true) {
?>
					<div id="LiveHelpSmiliesButton" style="width:24px; height:24px; position:absolute; bottom:130px; right:5px; top:auto; left:auto">
						<img class="trigger" src="../images/Smile.png" id="download" title="Smilies" alt="Smilies"/>
						<div id="SmiliesTooltip" style="display:none;"><div><span title="Laugh" class="sprite Laugh"></span><span title="Smile" class="sprite Smile"></span><span title="Sad" class="sprite Sad"></span><span title="Money" class="sprite Money"></span><span title="Impish" class="sprite Impish"></span><span title="Sweat" class="sprite Sweat"></span><span title="Cool" class="sprite Cool"></span><br/><span title="Frown" class="sprite Frown"></span><span title="Wink" class="sprite Wink"></span><span title="Surprise" class="sprite Surprise"></span><span title="Woo" class="sprite Woo"></span><span title="Tired" class="sprite Tired"></span><span title="Shock" class="sprite Shock"></span><span title="Hysterical" class="sprite Hysterical"></span><br/><span title="Kissed" class="sprite Kissed"></span><span title="Dizzy" class="sprite Dizzy"></span><span title="Celebrate" class="sprite Celebrate"></span><span title="Angry" class="sprite Angry"></span><span title="Adore" class="sprite Adore"></span><span title="Sleep" class="sprite Sleep"></span><span title="Quiet" class="sprite Stop"></span></div></div>
					</div>
<?php
}
?>
				 </div></td>
                  <input name="ID" type="hidden" id="ID" value="<?php echo($guest_login_id); ?>"><?php
				  if ($staff != '') {
?>
				  <input name="STAFF" type="hidden" id="STAFF" value="<?php echo($staff); ?>"><?php
				  }
?>
              <td width="20" height="20">&nbsp;</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td width="20" height="20">&nbsp;</td>
              <td width="20" height="20">&nbsp;</td>
            </tr>
          </table></td>
      </tr>
      <tr>
        <td><div align="right"><?php echo($_LOCALE['responses']); ?>:</div></td>
        <td><select name="RESPONSE" id="RESPONSE" width="300" style="width:300px;"<?php if ($staff != '') { echo(' disabled="true"'); } ?>>
            <?php
		$query = "SELECT `id`, `content` FROM " . $table_prefix . "responses WHERE `type` = 1";
		$rows = $SQL->selectall($query);
		if (is_array($rows)) {
		?>
            <option value=''><?php echo($_LOCALE['selectresponse']); ?></option>
            <?php
			foreach ($rows as $key => $row) {
				if (is_array($row)) {
					$id = $row['id'];
					$content = $row['content'];
		?>
            <option value="<?php echo($content); ?>"><?php echo($content); ?></option>
            <?php
				}
			}
		?>
          </select>
          <a href="#" onClick="appendResponse()"><img src="../images/mail_send.gif" alt="<?php echo($_LOCALE['appendresponse']); ?>" width="22" height="22" border="0"></a>&nbsp;<a href="responses.php" target="displayFrame"><img src="../images/mail_edit.gif" alt="<?php echo($_LOCALE['editresponses']); ?>" width="22" height="22" border="0"></a>
          <?php
		}
		else {
		?>
          <option value=''><?php echo($_LOCALE['clickaddresponse']); ?></option>
          </select>
          <a href="responses.php" target="displayFrame"><img src="../images/mail_edit.gif" alt="<?php echo($_LOCALE['editresponses']); ?>" width="22" height="22" border="0"></a>
          <?php
		}
		?>
        </td>
      </tr>
      <tr>
        <td><div align="right"><?php echo($_LOCALE['commands']); ?>:</div></td>
        <td><select name="COMMAND" id="COMMAND" width="300" style="width:300px;"<?php if ($staff != '') { echo(' disabled="true"'); } ?>>
            <?php
		$query = "SELECT `id`, `name`, `type`, `content` FROM " . $table_prefix . "responses WHERE `type` > 1";
		$rows = $SQL->selectall($query);
		if (is_array($rows)) {
		?>
            <option value=''><?php echo($_LOCALE['selectcommand']); ?></option>
            <?php
			foreach ($rows as $key => $row) {
				if (is_array($row)) {
					$id = $row['id'];
					$name = $row['name'];
					$type = $row['type'];
					$content = $row['content'];

					switch ($type) {
						case 2:
							$type = 'Hyperlink';
							break;
						case 3:
							$type = 'Image';
							break;
						case 4:
							$type = 'PUSH';
							break;
						case 5;
							$type = 'JavaScript';
							break;
					}
					?>
            <option value="<?php echo($id); ?>"><?php echo($type . ' - ' . $name); ?></option>
            <?php
				}
			}
		?>
          </select>
          <a href="commands.php" target="displayFrame"><img src="../images/mail_edit.gif" alt="<?php echo($_LOCALE['editcommands']); ?>" width="22" height="22" border="0"></a>
          <?php
		}
		else {
		?>
          <option value=''><?php echo($_LOCALE['clickaddcommand']); ?></option>
          </select>
          <a href="commands.php" target="displayFrame"><img src="../images/mail_edit.gif" alt="<?php echo($_LOCALE['editcommands']); ?>" width="22" height="22" border="0"></a>
          <?php
		}
		?>
        </td>
      </tr>
      <tr>
        <td colspan="2"><div align="center">
        <?php
			if ($guest_login_id != '') {
		?>
            <table border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="30"><div align="center"><a href="transfer_user.php?LOGIN_ID=<?php echo($guest_login_id); ?>&USER=<?php echo($guest_username); ?>" target="displayFrame"><img src="../images/reload.gif" alt="<?php echo($_LOCALE['transferuser']); ?>" width="22" height="22" border="0"></a></div></td>
                <td class="small"><a href="transfer_user.php?LOGIN_ID=<?php echo($guest_login_id); ?>&USER=<?php echo($guest_username); ?>" target="displayFrame" class="normlink"><?php echo($_LOCALE['transferuser']); ?></a></td>
                <td width="20"><div align="center" class="small">-</div></td>
                <td width="30"><div align="center"><a href="print.php?LOGIN_ID=<?php echo($guest_login_id); ?>&USER=<?php echo($guest_username); ?>" target="displayFrame"><img src="../images/fileprint.gif" alt="<?php echo($_LOCALE['printchat']); ?>" width="22" height="22" border="0"></a></div></td>
                <td class="small"><a href="print.php?LOGIN_ID=<?php echo($guest_login_id); ?>&USER=<?php echo($guest_username); ?>" target="displayFrame" class="normlink"><?php echo($_LOCALE['printchat']); ?></a></td>
                <td width="20"><div align="center" class="small">-</div></td>
                <td width="30"><div align="center"><a href="displayer_frame.php?ID=<?php echo($guest_login_id); ?>&USER=<?php echo($guest_username); ?>" target="displayFrame"><img src="../images/chat.gif" alt="<?php echo($_LOCALE['displaychat']); ?>" width="22" height="22" border="0"></a></div></td>
                <td class="small"><a href="displayer.php?ID=<?php echo($guest_login_id); ?>&USER=<?php echo($guest_username); ?>" target="displayFrame" class="normlink"><?php echo($_LOCALE['displaychat']); ?></a></td>
              </tr>
            </table>
        <?php
			}
		?>
          </div></td>
      </tr>
    </table>
    <span class="small"><?php echo($_LOCALE['stardevelopcopyright']); ?><br>
    <?php echo($_LOCALE['stardeveloplivehelpversion']); ?></span>
  </form>
  <script language="JavaScript">
<!--
document.MessageForm.MESSAGE.focus();

function processForm() {
  notTyping();
  void(document.MessageForm.submit());
  document.MessageForm.MESSAGE.value='';
  document.MessageForm.RESPONSE.value = '';
  document.MessageForm.COMMAND.value = '';
  document.MessageForm.MESSAGE.focus();
}

function appendResponse() {
  var current = document.MessageForm.MESSAGE.value;
  var text = document.MessageForm.RESPONSE.value;
  document.MessageForm.RESPONSE.value = '';
  document.MessageForm.MESSAGE.value = current + text;
  document.MessageForm.MESSAGE.focus();
}

function appendText(text) {
  var current = document.MessageForm.MESSAGE.value;
  document.MessageForm.MESSAGE.value = current + text;
  document.MessageForm.MESSAGE.focus();
}

function checkEnter(e) {
  isTyping();
  var characterCode

  if(e && e.which){
    e = e
	characterCode = e.which
  }
  else{
    e = event
	characterCode = e.keyCode
  }

  if(characterCode == 13){
    processForm()
    return false
  }
  else{
    return true
  }
}
//-->
</script>
</div>
</body>
</html>
