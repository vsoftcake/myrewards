<?php

require_once('config.php');
require_once('XMLSerializer.php');
header('Content-Type:text/xml; charset=UTF-8');
try {
			$dbh = new PDO($conn_string, $db_user, $db_pwd);
			$stmt = $dbh->prepare("select count(*) count from mobile_redeem where user_id = :user_id and product_id=:pid
			");

					$stmt->bindParam(':user_id', $_GET['user_id']);
					$stmt->bindParam(':pid', $_GET['pid'], PDO::PARAM_INT);

					if($stmt->execute()) {
						if ($row = $stmt->fetchAll(PDO::FETCH_ASSOC)) {
							print XMLSerializer::generateValidXmlFromArray($row, 'root', 'redeem');
						}
					}
					$dbh = null;
				} catch (PDOException $e) {
					print '<?xml version="1.0" encoding="UTF-8"?>';
					print '<status>'.$e->getMessage().'</status>';
					die();
	}

?>
