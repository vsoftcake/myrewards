<?php
/*
 * get category for a selected country
 * input: String country
 * author:gilbert@easyapps.com.hk
 */
require_once('config.php');
require_once('XMLSerializer.php');
header('Content-Type:text/xml; charset=UTF-8');
//session_start();
//if (isset($_SESSION['log']) && $_SESSION['log']) {
	try {

		$dbh = new PDO($conn_string, $db_user, $db_pwd);

		//select
		$stmt = $dbh->prepare("
		SELECT DISTINCT Category.*
		FROM categories AS Category
			LEFT JOIN clients_categories AS ClientsCategory
				ON Category.id = ClientsCategory.category_id
		WHERE ClientsCategory.client_id = :cid and Category.countries = :country
			AND Category.is_mobile = 1 ORDER BY Category.name
		");

		$stmt->bindParam(':cid', $cid);
		$stmt->bindParam(':country', $country);

		$cid = $_GET['cid'];
		$country = $_GET['country'];

		if($stmt->execute()) {
			if ($row = $stmt->fetchAll(PDO::FETCH_ASSOC)) {
				print XMLSerializer::generateValidXmlFromArray($row, 'root', 'category');
			}
		}
		$dbh = null;
	} catch (PDOException $e) {
		print '<?xml version="1.0" encoding="UTF-8"?>';
		print '<status>'.$e->getMessage().'</status>';
		die();
	}
/*} else {
//	print '<?xml version="1.0" encoding="UTF-8"?>';
//	print '<status>NOT_LOGIN</status>';
//}*/
?>
