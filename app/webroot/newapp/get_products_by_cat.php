<?php
/* Return a list of countries of the client
 * int cid client_id, String q, String country
 * author: gilbert@easyapps.com.hk
 */
require_once('config.php');
require_once('XMLSerializer.php');
header('Content-Type:text/xml; charset=UTF-8');
session_start();
//if (isset($_SESSION['log']) && $_SESSION['log']) {
	try {

		$dbh = new PDO($conn_string, $db_user, $db_pwd);

		$start = intval(trim(isset($_GET['start']) ? $_GET['start'] : 0));
		$cid = $_GET['cid'];
		$cat_id = $_GET['cat_id'];
		$limit = intval(trim(isset($_GET['limit']) ? $_GET['limit'] : 10));

		//select
		$stmt = $dbh->prepare("
		SELECT
			DISTINCT Product.id,
			MATCH (Product.keyword, Product.name, Product.details, Product.highlight, Product.`text`, Product.special_offer_headline, Product.special_offer_body)
				AGAINST ('') AS Relevance
		FROM
			merchant_addresses as MerchantAddress,
			merchants as Merchant,
			products_addresses as ProductAddress,
			clients_products AS ClientsProduct,
			products AS Product INNER JOIN categories_products AS CategoriesProduct ON Product.id = CategoriesProduct.product_id
				INNER JOIN clients_categories AS ClientsCategory ON CategoriesProduct.category_id = ClientsCategory.category_id AND ClientsCategory.client_id = :cid
		WHERE
			Product.merchant_id = MerchantAddress.merchant_id
			and ProductAddress.merchant_address_id=MerchantAddress.id
			and ProductAddress.product_id=Product.id
			AND Product.active = 1
			and Product.cancelled = 0
			and Product.merchant_id = Merchant.id
			AND (Product.offer_start = '0000-00-00' OR Product.offer_start <= NOW())
			AND (Product.expires = '0000-00-00' OR Product.expires >= NOW())
			and Product.id = ClientsProduct.product_id
			AND (ClientsProduct.client_id = :cid OR ClientsProduct.client_id = 0)
			AND CategoriesProduct.category_id = :cat_id
		ORDER BY
			Product.search_weight DESC,
			Product.special_offer DESC,
			Product.name
		LIMIT :start, :limit
		");

		$stmt->bindParam(':cid', $cid);
		$stmt->bindParam(':cat_id', $cat_id);
		$stmt->bindParam(':start', $start, PDO::PARAM_INT);
		$stmt->bindParam(':limit', $limit, PDO::PARAM_INT);

		if($stmt->execute()) {
			if ($row = $stmt->fetchAll(PDO::FETCH_ASSOC)) {
				$count = count($row);
				$row['count'] = $count;
				$row['link_next'] = ($count == $limit)
										? 'get_products_by_cat.php?cid='.$cid.'&cat_id='.$cat_id.'&country='.$country.'&start='.($start + $limit).'&limit='.$limit
										: '';
				$row['link_prev'] = ($start > 0)
										? 'search.php?cid='.$cid.'&cat_id='.$cat_id.'&country='.$country.'&start='.($start - $limit).'&limit='.$limit
										: '';
				print XMLSerializer::generateValidXmlFromArray($row, 'root', 'product');
			}
		}
		$dbh = null;
	} catch (PDOException $e) {
		print '<?xml version="1.0" encoding="UTF-8"?>';
		print '<status>'.$e->getMessage().'</status>';
		die();
	}
/*} else {
//	print '<?xml version="1.0" encoding="UTF-8"?>';
//	print '<status>NOT_LOGIN</status>';
//}*/
?>
