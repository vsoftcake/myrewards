<?php
/*
 * get state for a selected country
 * input: String country, int cid client_id
 * author:gilbert@easyapps.com.hk
 */
require_once('config.php');
require_once('XMLSerializer.php');
header('Content-Type:text/xml; charset=UTF-8');
//session_start();
//if (isset($_SESSION['log']) && $_SESSION['log']) {
	try {
		
		$dbh = new PDO($conn_string, $db_user, $db_pwd);

		//select
		$stmt = $dbh->prepare("
		SELECT DISTINCT State.state_cd, State.state_name 
		FROM states AS State, countries as Country, clients as Client, 
			merchant_addresses as MerchantAddress, products as Product 
		WHERE State.country_cd = Country.id and Country.name = ?
				and MerchantAddress.merchant_id = Product.merchant_id
				and MerchantAddress.state = State.state_cd 
				and Client.id = ?
		");
		
		if($stmt->execute(array($_GET['country'], $_GET['cid']))) {
			if ($row = $stmt->fetchAll(PDO::FETCH_ASSOC)) {
				print XMLSerializer::generateValidXmlFromArray($row, 'root', 'state');
			}
		}
		$dbh = null;
	} catch (PDOException $e) {
		print '<?xml version="1.0" encoding="UTF-8"?>';
		print '<status>'.$e->getMessage().'</status>';
		die();
	}
/*} else {
//	print '<?xml version="1.0" encoding="UTF-8"?>';
//	print '<status>NOT_LOGIN</status>';
//}*/
?>