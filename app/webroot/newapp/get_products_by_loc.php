<?php
/* Return a list of countries of the client
 * int cid client_id, String q, String country
 * author: gilbert@easyapps.com.hk
 */
require_once('config.php');
require_once('XMLSerializer.php');
header('Content-Type:text/xml; charset=UTF-8');
session_start();
//if (isset($_SESSION['log']) && $_SESSION['log']) {
	try {

		$dbh = new PDO($conn_string, $db_user, $db_pwd);
		$dbh->exec('set names utf8');

		$lat = isset($_GET['lat']) ? $_GET['lat'] : 0;
		$lng = isset($_GET['lng']) ? $_GET['lng'] : 0;
		$cid = $_GET['cid'];
		$bound = $_GET['b'];
		//select
		$stmt = $dbh->prepare("
		SELECT
			DISTINCT Product.id, Product.name, MerchantAddress.latitude, MerchantAddress.longitude, cat.pin_type
		FROM
			merchant_addresses as MerchantAddress,
			merchants as Merchant,
			products_addresses as ProductAddress,
			clients_products AS ClientsProduct,
			products AS Product,
            categories_products AS CategoriesProduct,
            clients_categories AS ClientsCategories,
            categories AS cat
		WHERE
			Product.merchant_id = MerchantAddress.merchant_id
			AND ProductAddress.merchant_address_id=MerchantAddress.id
			AND ProductAddress.product_id=Product.id
			AND CategoriesProduct.product_id = Product.id
			AND cat.id = CategoriesProduct.category_id
			AND cat.id = ClientsCategories.category_id
			AND ClientsCategories.client_id = :cid
			AND Product.active = 1
			AND Product.cancelled = 0
			AND Product.merchant_id = Merchant.id
			AND (Product.offer_start = '0000-00-00' OR Product.offer_start <= NOW())
			AND (Product.expires = '0000-00-00' OR Product.expires >= NOW())
			AND Product.id = ClientsProduct.product_id
			AND (ClientsProduct.client_id = :cid OR ClientsProduct.client_id = 0)
            AND (MerchantAddress.latitude IS NOT NULL)
            AND (MerchantAddress.longitude IS NOT NULL)
            AND	(MerchantAddress.latitude > :lat - :bound)
            AND (MerchantAddress.latitude < :lat + :bound)
			AND (MerchantAddress.longitude > :lng - :bound)
            AND (MerchantAddress.longitude < :lng + :bound)
		ORDER BY
			Product.search_weight DESC,
			Product.special_offer DESC,
			Product.name
		");

		$stmt->bindParam(':cid', $cid);
		$stmt->bindParam(':lat', $lat);
		$stmt->bindParam(':lng', $lng);
		$stmt->bindParam(':bound', $bound);
		if($stmt->execute()) {
			if ($row = $stmt->fetchAll(PDO::FETCH_ASSOC)) {
				print XMLSerializer::generateValidXmlFromArray($row, 'root', 'product');
			}
		}
		$dbh = null;
	} catch (PDOException $e) {
		print '<?xml version="1.0" encoding="UTF-8"?>';
		print '<status>'.$e->getMessage().'</status>';
		die();
	}
/*} else {
//	print '<?xml version="1.0" encoding="UTF-8"?>';
//	print '<status>NOT_LOGIN</status>';
//}*/
?>
