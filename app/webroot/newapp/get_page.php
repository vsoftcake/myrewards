<?php
/*
 * get page detail
 * input: int cid
 * input: int pid
 * author:sb
 */
require_once('config.php');
require_once('XMLSerializer.php');
header('Content-Type:text/xml; charset=UTF-8');
try {

		$dbh = new PDO($conn_string, $db_user, $db_pwd);

		//select
		$stmt = $dbh->prepare("
		SELECT ap.`details` from app_pages ap
		where ap.`client_id` = :cid and ap.`page_id` = :pid
		");

		$stmt->bindParam(':cid', $cid);
		$stmt->bindParam(':pid', $pid);

		$cid = $_GET['cid'];
		$pid = $_GET['pid'];

		if($stmt->execute()) {
			if ($row = $stmt->fetchAll(PDO::FETCH_ASSOC)) {
				print XMLSerializer::generateValidXmlFromArray($row, 'root', 'page');
			}
		}
		$dbh = null;
	} catch (PDOException $e) {
		print '<?xml version="1.0" encoding="UTF-8"?>';
		print '<status>'.$e->getMessage().'</status>';
		die();
	}
?>