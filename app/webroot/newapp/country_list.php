<?php
/* Return a list of countries of the client
 * int cid client_id
 * author: gilbert@easyapps.com.hk
 */
require_once('config.php');
require_once('XMLSerializer.php');
header('Content-Type:text/xml; charset=UTF-8');
//session_start();
//if (isset($_SESSION['log']) && $_SESSION['log']) {
	try {
		
		$dbh = new PDO($conn_string, $db_user, $db_pwd);

		//select
		$stmt = $dbh->prepare('
		SELECT Country.id, Country.name  
		FROM countries AS Country 
		
		');
		
		if($stmt->execute()) {
			if ($row = $stmt->fetchAll(PDO::FETCH_ASSOC)) {
				print XMLSerializer::generateValidXmlFromArray($row, 'root', 'countries');
			}
		}
		$dbh = null;
	} catch (PDOException $e) {
		print '<?xml version="1.0" encoding="UTF-8"?>';
		print '<status>'.$e->getMessage().'</status>';
		die();
	}
/*} else {
//	print '<?xml version="1.0" encoding="UTF-8"?>';
//	print '<status>NOT_LOGIN</status>';
//}*/
?>
