<?php
/*
 * update user detail
 * input: int id
 * author:gilbert@easyapps.com.hk
 */
require_once('config.php');
require_once('XMLSerializer.php');
header('Content-Type:text/xml; charset=UTF-8');
//session_start();
//if (isset($_SESSION['uname'])) {
	try {

		$query = "UPDATE users AS User 
							SET 
							User.modified = NOW(),";
if (isset($_POST['email'])) {
			$query .= 'User.email = :email,';
		}
	
		if (isset($_POST['fname'])) {
			$query .= 'User.first_name = :fname,';
		}
		if (isset($_POST['lname'])) {
			$query .= 'User.last_name = :lname,';
		}
		if (isset($_POST['country'])) {
			$query .= 'User.country = :country,';
		}
		if (isset($_POST['state'])) {
			$query .= 'User.state = :state,';
		}
		if (isset($_POST['newsletter'])) {
			$query .= 'User.newsletter = :newsletter,';
		}

		$query = substr($query, 0, strlen($query) - 1);


		$query .= ' WHERE User.id = :name';

		$dbh = new PDO($conn_string, $db_user, $db_pwd);

		$stmt = $dbh->prepare($query);

	//	if (isset($_POST['pwd'])) {
	//		$stmt->bindParam(':pwd', $_POST['pwd']);
	//	}
		if (isset($_POST['email'])) {
			$stmt->bindParam(':email', $_POST['email']);
		}
		if (isset($_POST['fname'])) {
			$stmt->bindParam(':fname', $_POST['fname']);
		}
		if (isset($_POST['lname'])) {
			$stmt->bindParam(':lname', $_POST['lname']);
		}
		if (isset($_POST['country'])) {
			$stmt->bindParam(':country', $_POST['country']);
		}
		if (isset($_POST['state'])) {
			$stmt->bindParam(':state', $_POST['state']);
		}
		if (isset($_POST['newsletter'])) {
			$stmt->bindParam(':newsletter', $_POST['newsletter']);
		}

		$stmt->bindParam(':name', $_POST['uname']);
				
		if($stmt->execute()) {
			$count = $stmt->rowCount();
			echo $count ? 'success' : 'not_found'; 
		} else {
			echo 'query_error';
		}
		$dbh = null;
	} catch (PDOException $e) {
		print '<?xml version="1.0" encoding="UTF-8"?>';
		print '<status>'.$e->getMessage().'</status>';
		die();
	}
	
//} else {


?>
