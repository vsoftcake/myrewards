<?php
/*
 * get user detail
 * input: int id
 * author:gilbert@easyapps.com.hk
 */
require_once('config.php');
require_once('XMLSerializer.php');
header('Content-Type:text/xml; charset=UTF-8');
session_start();
if (isset($_SESSION['uname'])) {
	try {

		$dbh = new PDO($conn_string, $db_user, $db_pwd);

		//select
		$stmt = $dbh->prepare("
		SELECT u.`id`, `client_id`, u.`domain_id`, `type`, u.`username`,
		u.`email`, `first_name`, `last_name`, `state`, u.`country`,
		`mobile`, c.`mobile_membership_card_extension` as `card_ext`, c.name AS client_name,c.facebook_link, c.twitter_link, u.newsletter
		FROM users as u
		LEFT JOIN clients as c ON (u.`client_id` = c.`id`)
		WHERE u.`id` = ?
		");

		if($stmt->execute(array($_SESSION['uname']))) {
			if ($row = $stmt->fetchAll(PDO::FETCH_ASSOC)) {
				print XMLSerializer::generateValidXmlFromArray($row, 'root', 'user');
			}
		}
		$dbh = null;
	} catch (PDOException $e) {
		print '<?xml version="1.0" encoding="UTF-8"?>';
		print '<status>'.$e->getMessage().'</status>';
		die();
	}
} else {
	print '<?xml version="1.0" encoding="UTF-8"?>';
	print '<status>NOT_LOGIN</status>';
}
?>
