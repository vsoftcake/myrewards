<?php
/*
 * get product
 * input: int id product_id
 * author:gilbert@easyapps.com.hk
 */
require_once('config.php');
require_once('XMLSerializer.php');
header('Content-Type:text/xml; charset=UTF-8');
//session_start();
//if (isset($_SESSION['log']) && $_SESSION['log']) {
	try {

		$dbh = new PDO($conn_string, $db_user, $db_pwd);
		$dbh->exec('set names utf8');

		//select
		$stmt = $dbh->prepare("
SELECT `Product`.`id`,
			`Product`.`merchant_id`,
			`Product`.`name`, `Product`.`details`, `Product`.`highlight`, `Product`.`text`,`Product`.`quantity`,
			`Product`.`terms_and_conditions`, `Product`.`link1`,
			`Product`.`link1_text`, `Product`.`link2`, `Product`.`link2_text`,
			`Product`.`image_extension`, `Product`.`display_image`,`Product`.`app_coupon`,`Product`.`mobile_reward`,
			`Product`.`redeem_password`, `Product`.`repet_offer`,`Product`.`target`,`Product`.`punch_on_img`,`Product`.`punch_off_img`,`Product`.`redeem_button_img`,`Product`.`target_message`,`Product`.`bg_color`,
			`Merchant`.`name` as `mname`, `Merchant`.`contact_first_name`,
			`Merchant`.`contact_last_name`, `Merchant`.`contact_title`, `Merchant`.`contact_position`,
			`MA`.`address1` as `mail_address1`, `MA`.`address2` as `mail_address2`, `MA`.`suburb` as `mail_suburb`,
			`MA`.`state` as `mail_state`, `MA`.`postcode` as `mail_postcode`, `MA`.`country` as `mail_country`,
			 NULL AS `email`, `MA`.`phone`, NULL AS `mobile`,  NULL AS `fax`,
			`MA`.`latitude`, `MA`.`longitude`,
			`Merchant`.`logo_extension`, `Merchant`.`image_extension` AS `m_image_extension`, `Merchant`.`text` as `mtext`,
			`Merchant`.`created`, `Merchant`.`modified`
		FROM `products` AS `Product`
		LEFT JOIN `merchants` AS `Merchant` ON (`Product`.`merchant_id` = `Merchant`.`id`)
		LEFT JOIN `merchant_addresses` AS `MA` ON (`Merchant`.`id` = `MA`.`merchant_id` AND `MA`.`primary` = TRUE)
		WHERE `Product`.`id` = ?
		LIMIT 1
		");

		if($stmt->execute(array($_GET['id']))) {
			if ($row = $stmt->fetchAll(PDO::FETCH_ASSOC)) {
				print XMLSerializer::generateValidXmlFromArray($row, 'root', 'product');
				//print_r($row);
			}
		}
		$dbh = null;
	} catch (PDOException $e) {
		print '<?xml version="1.0" encoding="UTF-8"?>';
		print '<status>'.$e->getMessage().'</status>';
		die();
	}
/*} else {
//	print '<?xml version="1.0" encoding="UTF-8"?>';
//	print '<status>NOT_LOGIN</status>';
//}*/
?>
