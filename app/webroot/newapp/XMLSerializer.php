<?php
class XMLSerializer {

    // functions adopted from http://www.sean-barton.co.uk/2009/03/turning-an-array-or-object-into-xml-using-php/

    public static function generateValidXmlFromObj(stdClass $obj, $node_block='nodes', $node_name='node') {
        $arr = get_object_vars($obj);
        return self::generateValidXmlFromArray($arr, $node_block, $node_name);
    }

    public static function generateValidXmlFromArray($array, $node_block='nodes', $node_name='node') {
        $xml = '<?xml version="1.0" encoding="UTF-8" ?>'."\n";

        $xml .= '<' . $node_block . '>'."\n";
        $xml .= self::generateXmlFromArray($array, $node_name);
        $xml .= '</' . $node_block . '>'."\n";

        return $xml;
    }

    private static function generateXmlFromArray($array, $node_name) {
        $xml = '';

        if (is_array($array) || is_object($array)) {
            foreach ($array as $key=>$value) {
                if (is_numeric($key)) {
                    $key = $node_name;
                }

                $xml .= '<' . $key . '>' . self::generateXmlFromArray($value, $node_name) . '</' . $key . '>'."\n";
            }
        } else {
            //$xml = iconv('BIG5','UTF-8',$array);// utf8_encode($array);
           //$xml = mb_convert_encoding($array,'UTF-8','BIG5');
            $xml = $array;
           
            $xml = str_replace("&nbsp;", " ", $xml);
            $xml = str_replace("&amp;", "&", $xml);
			$xml = str_replace("'", "&apos;", $xml);
           /* 
            $xml = str_replace("&", "&amp;", $xml);
			$xml = str_replace("'", "&apos;", $xml);
			
			$xml = str_replace('"', "&quot;", $xml);
			$xml = str_replace("<", "&lt;", $xml);
			$xml = str_replace(">", "&gt;", $xml);
			*/
			$xml = htmlspecialchars($xml);
			/*if( !empty($xml) )
              $xml = "<![CDATA[".($xml)."]]>";*/
        }

        return $xml;
    }

}
?>
