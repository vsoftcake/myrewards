<?php
/*
 * get a hot offer product_id
 * input: int cid client id
 * author:gilbert@easyapps.com.hk
 */
require_once('config.php');
require_once('XMLSerializer.php');
header('Content-Type:text/xml; charset=UTF-8');
//session_start();
//if (isset($_SESSION['log']) && $_SESSION['log']) {
	try {

		$dbh = new PDO($conn_string, $db_user, $db_pwd);

		//select
		$stmt = $dbh->prepare("

SELECT 		Product.id , Product.name, Product.highlight
 FROM products AS Product
LEFT JOIN merchants AS Merchant ON (Product.merchant_id = Merchant.id) LEFT JOIN clients_special_products AS ClientsSpecialProduct ON (ClientsSpecialProduct.product_id = Product.id)
LEFT JOIN merchant_addresses AS MerchantAddress ON (MerchantAddress.primary = 1 AND MerchantAddress.merchant_id = Product.merchant_id)
LEFT JOIN clients_products AS ClientsProduct ON (ClientsProduct.client_id = :cid AND ClientsProduct.product_id = Product.id)
LEFT JOIN categories_products AS CategoriesProduct ON (CategoriesProduct.category_id IN (261,262,263) AND CategoriesProduct.product_id = Product.id)
WHERE ClientsSpecialProduct.client_id = :cid AND Product.active = 1 AND (Product.expires = 0000-00-00 OR Product.expires >= NOW()) group by Product.id LIMIT 1
		");

		$stmt->bindParam(':cid', $_GET['cid'], PDO::PARAM_INT);

		if($stmt->execute()) {
			if ($row = $stmt->fetchAll(PDO::FETCH_ASSOC)) {
				print XMLSerializer::generateValidXmlFromArray($row, 'root', 'product');
			}
		}
		$dbh = null;
	} catch (PDOException $e) {
		print '<?xml version="1.0" encoding="UTF-8"?>';
		print '<status>'.$e->getMessage().'</status>';
		die();
	}
/*} else {
//	print '<?xml version="1.0" encoding="UTF-8"?>';
//	print '<status>NOT_LOGIN</status>';
//}*/
?>
