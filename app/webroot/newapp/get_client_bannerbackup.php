<?php
/**
 * get the newsletter banner of the
 */
require_once('config.php');
try {

		$dbh = new PDO($conn_string, $db_user, $db_pwd);

		//select
		$stmt = $dbh->prepare("
		SELECT
		`newsletter_banner_image` AS `nbi`
		FROM
		`clients`
		WHERE
		`id` = ?
		");

		if($stmt->execute(array($_GET['cid']))) {
			if ($row = $stmt->fetchAll(PDO::FETCH_ASSOC)) {
				 header ("Location: http://184.107.135.82/files/clients/newsletter_banner_image/".$row[0]['nbi']);
			}
		}
		$dbh = null;
	} catch (PDOException $e) {
		echo "FAIL";
		die();
	}

?>
