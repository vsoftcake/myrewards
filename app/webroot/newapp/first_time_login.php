<?php
/* login page for Smartphone API
 * input: String username, String password
 * author: gilbert@easyapps.com.hk
 */
require_once('config.php');
require_once('XMLSerializer.php');
header('Content-Type:text/xml; charset=UTF-8');
try {

	$dbh = new PDO($conn_string, $db_user, $db_pwd);
	$uname = $_POST['uname'];
	$subdomain = $_POST['sub'];

	if (! (isset($uname) && isset($subdomain))) {
		print '<?xml version="1.0" encoding="UTF-8"?>';
		print '<status>uname / subdomain must not be empty.</status>';
		return;
	}
	$stmt = $dbh->prepare('
		SELECT u.id
		FROM users AS u
			LEFT JOIN domains AS d
			ON (u.domain_id = d.id)
		WHERE registered=0 and u.username = ?
			AND d.name = ?
		');

	if($stmt->execute(array($uname, $subdomain))) {
		if ($row = $stmt->fetchAll(PDO::FETCH_ASSOC)) {
			if ($row[0]['id'] !="") {
				print '<first_status>'.$row[0]['id'].'</first_status>';
				session_start();
				$_SESSION['uname'] = $row[0]['id'];
				session_write_close();
			} else {
				print '<first_status>FAILURE</first_status>';
			}

		} else {
			print '<first_status>FAILURE</first_status>';
		}
	} else {
		print '<first_status>PDO ERROR</first_status>';
	}
	$dbh = null;
} catch (PDOException $e) {
	print '<?xml version="1.0" encoding="UTF-8"?>';
	print '<first_status>'.$e->getMessage().'</first_status>';
	die();
}
?>

