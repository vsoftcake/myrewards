<?php
/*
 * get a hot offer product_id
 * input: int cid client id
 * author:gilbert@easyapps.com.hk
 */
require_once('config.php');
require_once('XMLSerializer.php');
header('Content-Type:text/xml; charset=UTF-8');
//session_start();
//if (isset($_SESSION['log']) && $_SESSION['log']) {
	try {

		$dbh = new PDO($conn_string, $db_user, $db_pwd);

		//select
		$stmt = $dbh->prepare("
			select * from mobile_events where client_id = :cid and active = 1
		");

		$stmt->bindParam(':cid', $_GET['cid'], PDO::PARAM_INT);

		if($stmt->execute()) {
			if ($row = $stmt->fetchAll(PDO::FETCH_ASSOC)) {
				print XMLSerializer::generateValidXmlFromArray($row, 'root', 'events');
			}
		}
		$dbh = null;
	} catch (PDOException $e) {
		print '<?xml version="1.0" encoding="UTF-8"?>';
		print '<status>'.$e->getMessage().'</status>';
		die();
	}
/*} else {
//	print '<?xml version="1.0" encoding="UTF-8"?>';
//	print '<status>NOT_LOGIN</status>';
//}*/
?>
