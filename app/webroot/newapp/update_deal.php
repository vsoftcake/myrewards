<?php
/*
 * update user detail
 * input: int id
 * author:gilbert@easyapps.com.hk
 */
require_once('config.php');
require_once('XMLSerializer.php');
header('Content-Type:text/xml; charset=UTF-8');
//session_start();
//if (isset($_SESSION['uname'])) {
	try {

		$query = "UPDATE deals AS Deal
							SET
							Deal.merchant_id = :merchant_id,";

		if (isset($_GET['logo'])) { 		$query .= 'Deal.logo = :logo,';		}
		if (isset($_GET['name'])) {			$query .= 'Deal.name = :name,';		}
		if (isset($_GET['hg'])) {			$query .= 'Deal.highlights = :highlights,';		}
		if (isset($_GET['count'])) {		$query .= 'Deal.count = :count,';		}
		if (isset($_GET['cat_id'])) {		$query .= 'Deal.category_id = :category_id,';		}
		if (isset($_GET['hd'])) {			$query .= 'Deal.hours_countdown = :hours_countdown,';		}
		if (isset($_GET['sd'])) {			$query .= 'Deal.start_date = :start_date,';		}
		if (isset($_GET['ed'])) {			$query .= 'Deal.end_date = :end_date,';		}
		if (isset($_GET['od'])) {			$query .= 'Deal.offer_details = :offer_details,';		}
		if (isset($_GET['tc'])) {			$query .= 'Deal.terms_conditions = :terms_conditions,';		}

		if (isset($_GET['stores'])) {		$query .= 'Deal.stores = :stores,';		}
		if (isset($_GET['fbs'])) {			$query .= 'Deal.facebook_status = :facebook_status,';		}
		if (isset($_GET['fbu'])) {			$query .= 'Deal.facebook_url = :facebook_url,';		}
		if (isset($_GET['tws'])) {			$query .= 'Deal.twitter_status = :twitter_status,';		}
		if (isset($_GET['twu'])) {			$query .= 'Deal.twitter_url = :twitter_url,';		}
		if (isset($_GET['es'])) {			$query .= 'Deal.email_status = :email_status,';		}
		if (isset($_GET['ea'])) {			$query .= 'Deal.email_address = :email_address,';		}
		if (isset($_GET['pns'])) {			$query .= 'Deal.phone_num_status = :phone_num_status,';		}
		if (isset($_GET['pn1'])) {			$query .= 'Deal.phone_num1 = :phone_num1,';		}
		if (isset($_GET['pn2'])) {			$query .= 'Deal.phone_num2 = :phone_num2,';		}

	if (isset($_GET['bs'])) {			$query .= 'Deal.booknow_status = :booknow_status,';		}
		if (isset($_GET['bt'])) {			$query .= 'Deal.booknow_text = :booknow_text,';		}
		if (isset($_GET['ws'])) {			$query .= 'Deal.website_status = :website_status,';		}
		if (isset($_GET['wu'])) {			$query .= 'Deal.website_url = :website_url,';		}
		if (isset($_GET['cs'])) {			$query .= 'Deal.comment_status = :comment_status,';		}
		if (isset($_GET['ct'])) {			$query .= 'Deal.comments_text = :comments_text,';		}
		if (isset($_GET['scs'])) {			$query .= 'Deal.score_status = :score_status,';		}
		if (isset($_GET['sct'])) {			$query .= 'Deal.score_text = :score_text,';		}
		if (isset($_GET['sts'])) {			$query .= 'Deal.scan_status = :scan_status,';		}
		if (isset($_GET['stt'])) {			$query .= 'Deal.scan_text = :scan_text,';		}
		if (isset($_GET['rs'])) {			$query .= 'Deal.redeem_status = :redeem_status,';		}
		if (isset($_GET['rt'])) {			$query .= 'Deal.redeem_text = :redeem_text,';		}
		if (isset($_GET['active'])) {		$query .= 'Deal.active = :active,';		}


		$query = substr($query, 0, strlen($query) - 1);

		$query .= ' WHERE Deal.id = :id';



		$dbh = new PDO($conn_string, $db_user, $db_pwd);
		$stmt = $dbh->prepare($query);
$q='';
		if (isset($_GET['m_id'])) 	{ $stmt->bindParam(':merchant_id', $_GET['m_id']); 	echo 'mid - ' .$_GET['m_id'];
		} else 		{	$q .= '2'; }

		if (isset($_GET['logo'])) {  		$stmt->bindParam(':logo', $_GET['logo']);		echo 'mid - ' .$_GET['name'];	} else 		{	$q .= '2'; }
		if (isset($_GET['name'])) {	 	$stmt->bindParam(':name', $_GET['name']);		echo 'mid - ' .$_GET['logo'];	} else 		{	$q .= '2'; }
		if (isset($_GET['hg'])) {	 		$stmt->bindParam(':highlights', $_GET['hg']);		echo 'mid - ' .$_GET['hg'];	} else 		{	$q .= '2'; }
		if (isset($_GET['count'])) { 		$stmt->bindParam(':count', $_GET['count']);		echo 'mid - ' .$_GET['count'];	} else 		{	$q .= '2'; }
		if (isset($_GET['cat_id'])) { 		$stmt->bindParam(':category_id', $_GET['cat_id']);		echo 'mid - ' .$_GET['cat_id'];	} else 		{	$q .= '2'; }
		if (isset($_GET['hd'])) {	 		$stmt->bindParam(':hours_countdown', $_GET['hd']);		echo 'mid - ' .$_GET['hd'];	} else 		{	$q .= '2'; }
		if (isset($_GET['sd'])) { 			$stmt->bindParam(':start_date', $_GET['sd']);		echo 'mid - ' .$_GET['sd'];	} else 		{	$q .= '3'; }
		if (isset($_GET['ed'])) {	 		$stmt->bindParam(':end_date', $_GET['ed']);		echo 'mid - ' .$_GET['ed'];	} else 		{	$q .= '4'; }
		if (isset($_GET['od'])) {	 		$stmt->bindParam(':offer_details', $_GET['od']);		echo 'od - ' .$_GET['od'];	} else 		{	$q .= '2'; }
		if (isset($_GET['tc'])) {	 		$stmt->bindParam(':terms_conditions', $_GET['tc']);			echo 'od - ' .$_GET['tc'];	} else 		{	$q .= '2'; }
		if (isset($_GET['stores'])) {	 	$stmt->bindParam(':stores', $_GET['stores']);			echo 'od - ' .$_GET['stores'];	} else 		{	$q .= '2'; }
		if (isset($_GET['fbs'])) {		 	$stmt->bindParam(':facebook_status', $_GET['fbs']);			echo 'od - ' .$_GET['fbs'];	} else 		{	$q .= '2'; }
		if (isset($_GET['fbu'])) {		 	$stmt->bindParam(':facebook_url', $_GET['fbu']);			echo 'od - ' .$_GET['fbu'];	} else 		{	$q .= '2'; }
		if (isset($_GET['tws'])) {		 	$stmt->bindParam(':twitter_status', $_GET['tws']);			echo 'od - ' .$_GET['tws'];	} else 		{	$q .= '2'; }
		if (isset($_GET['twu'])) {		 	$stmt->bindParam(':twitter_url', $_GET['twu']);			echo 'od - ' .$_GET['twu'];	} else 		{	$q .= '2'; }
		if (isset($_GET['es'])) {		 	$stmt->bindParam(':email_status', $_GET['es']);			echo 'od - ' .$_GET['es'];	} else 		{	$q .= '2'; }
		if (isset($_GET['ea'])) {		 	$stmt->bindParam(':email_address', $_GET['ea']);			echo 'od - ' .$_GET['ea'];	} else 		{	$q .= '2'; }
		if (isset($_GET['pns'])) {		 	$stmt->bindParam(':phone_num_status', $_GET['pns']);			echo 'od - ' .$_GET['pns'];	} else 		{	$q .= '2'; }
		if (isset($_GET['pn1'])) {		 	$stmt->bindParam(':phone_num1', $_GET['pn1']);			echo 'od - ' .$_GET['pn1'];	} else 		{	$q .= '2'; }
		if (isset($_GET['pn2'])) {		 	$stmt->bindParam(':phone_num2', $_GET['pn2']);			echo 'od - ' .$_GET['pn2'];	} else 		{	$q .= '2'; }
		if (isset($_GET['bs'])) {		 	$stmt->bindParam(':booknow_status', $_GET['bs']);			echo 'od - ' .$_GET['bs'];	} else 		{	$q .= '2'; }
		if (isset($_GET['bt'])) {		 	$stmt->bindParam(':booknow_text', $_GET['bt']);			echo 'od - ' .$_GET['bt'];	} else 		{	$q .= '2'; }
		if (isset($_GET['ws'])) {		 	$stmt->bindParam(':website_status', $_GET['ws']);			echo 'od - ' .$_GET['ws'];	} else 		{	$q .= '2'; }
		if (isset($_GET['wu'])) {		 	$stmt->bindParam(':website_url', $_GET['wu']);			echo 'od - ' .$_GET['wu'];	} else 		{	$q .= '2'; }
		if (isset($_GET['cs'])) {		 	$stmt->bindParam(':comment_status', $_GET['cs']);			echo 'od - ' .$_GET['cs'];	} else 		{	$q .= '2'; }
		if (isset($_GET['ct'])) {		 	$stmt->bindParam(':comments_text', $_GET['ct']);			echo 'od - ' .$_GET['ct'];	} else 		{	$q .= '2'; }
		if (isset($_GET['scs'])) {		 	$stmt->bindParam(':score_status', $_GET['scs']);			echo 'od - ' .$_GET['scs'];	} else 		{	$q .= '2'; }
		if (isset($_GET['sct'])) {		 	$stmt->bindParam(':score_text', $_GET['sct']);			echo 'od - ' .$_GET['sct'];	} else 		{	$q .= '2'; }
		if (isset($_GET['sts'])) {		 	$stmt->bindParam(':scan_status', $_GET['sts']);			echo 'od - ' .$_GET['sts'];	} else 		{	$q .= '2'; }
		if (isset($_GET['stt'])) {		 	$stmt->bindParam(':scan_text', $_GET['stt']);			echo 'od - ' .$_GET['stt'];	} else 		{	$q .= '2'; }
		if (isset($_GET['rs'])) {		 	$stmt->bindParam(':redeem_status', $_GET['rs']);			echo 'od - ' .$_GET['rs'];	} else 		{	$q .= '2'; }
		if (isset($_GET['rt'])) {		 	$stmt->bindParam(':redeem_text', $_GET['rt']);			echo 'od - ' .$_GET['rt'];	} else 		{	$q .= '2'; }
		if (isset($_GET['active'])) {	 	$stmt->bindParam(':active', $_GET['active']);			echo 'od - ' .$_GET['active'];	} else 		{	$q .= '2'; }
		if (isset($_GET['id'])) {			$stmt->bindParam(':id', $_GET['id']);				echo 'od - ' .$_GET['active'];	} else 		{	$q .= '2'; }

echo 'printing -'. $q;


		if($stmt->execute()) {
			$count = $stmt->rowCount();
			echo $count ? 'success' : 'not_found';
		} else {
			echo 'query_error';
		}

	$dbh = null;
	} catch (PDOException $e) {
		print '<?xml version="1.0" encoding="UTF-8"?>';
		print '<status>'.$e->getMessage().'</status>';
		die();
	}

//} else {


?>
