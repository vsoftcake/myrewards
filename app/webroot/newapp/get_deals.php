<?php
/*
 * get deal
 * input: int id product_id
 * author:gilbert@easyapps.com.hk
 */
require_once('config.php');
require_once('XMLSerializer.php');
header('Content-Type:text/xml; charset=UTF-8');
//session_start();
//if (isset($_SESSION['log']) && $_SESSION['log']) {
	try {

		$dbh = new PDO($conn_string, $db_user, $db_pwd);
		//select
		$stmt = $dbh->prepare("
SELECT `Deals`.id,`Deals`.name, `Deals`.highlights, `Deals`.count,`Deals`.start_date,`Deals`.end_date,`Deals`.active
			FROM `deals` AS `Deals`
		WHERE `Deals`.`merchant_id` = ?
		");

		if($stmt->execute(array($_GET['m_id']))) {
			if ($row = $stmt->fetchAll(PDO::FETCH_ASSOC)) {
				print XMLSerializer::generateValidXmlFromArray($row, 'root', 'deals');
				//print_r($row);
			}
		}
		$dbh = null;
	} catch (PDOException $e) {
		print '<?xml version="1.0" encoding="UTF-8"?>';
		print '<status>'.$e->getMessage().'</status>';
		die();
	}
/*} else {
//	print '<?xml version="1.0" encoding="UTF-8"?>';
//	print '<status>NOT_LOGIN</status>';
//}*/
?>
