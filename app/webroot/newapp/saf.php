<?php
require_once('config.php');
require_once('XMLSerializer.php');
header('Content-Type:text/xml; charset=UTF-8');
	try {

		$query = "Insert into mobile_saf (user_id,fname, email)
							values(:uid,:fname,:email)";

		$dbh = new PDO($conn_string, $db_user, $db_pwd);

		$stmt = $dbh->prepare($query);

		if (isset($_POST['uid'])) {
			$stmt->bindParam(':uid', $_POST['uid']);
		}
		if (isset($_POST['fname'])) {
			$stmt->bindParam(':fname', $_POST['fname']);
		}
		if (isset($_POST['email'])) {
			$stmt->bindParam(':email', $_POST['email']);
		}

		if($stmt->execute()) {
			$count = $stmt->rowCount();
			echo $count ? 'success' : 'not_found';
		} else {
			echo 'query_error';
		}
		$dbh = null;
	} catch (PDOException $e) {
		print '<?xml version="1.0" encoding="UTF-8"?>';
		print '<status>'.$e->getMessage().'</status>';
		die();
	}
?>
