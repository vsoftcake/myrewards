<?php
/*
 * get all offers of a merchant/client
 * input: int cid client id
 * author:sb
 */
require_once('config.php');
require_once('XMLSerializer.php');
header('Content-Type:text/xml; charset=UTF-8');
//session_start();
//if (isset($_SESSION['log']) && $_SESSION['log']) {
	try {

		$dbh = new PDO($conn_string, $db_user, $db_pwd);

		//select
		$stmt = $dbh->prepare("
SELECT DISTINCT
				Product.id , Product.name, Product.highlight,Product.app_image_extension
				FROM
				merchant_addresses as MerchantAddress,
				products_addresses as ProductAddress,
				clients_products AS ClientsProduct,
				products AS Product
				INNER JOIN categories_products AS CategoriesProduct
				ON Product.id = CategoriesProduct.product_id
				INNER JOIN clients_categories AS ClientsCategory
				ON CategoriesProduct.category_id = ClientsCategory.category_id AND ClientsCategory.client_id = :cid
				WHERE
				Product.merchant_id = MerchantAddress.merchant_id
				and ProductAddress.merchant_address_id=MerchantAddress.id
				and ProductAddress.product_id=Product.id
				AND Product.active = 1
				and Product.cancelled = 0
				AND Product.loyality = 1
				AND (Product.offer_start = '0000-00-00' OR Product.offer_start <= NOW())
				AND (Product.expires = '0000-00-00' OR Product.expires >= NOW())
				and Product.id = ClientsProduct.product_id
				AND (ClientsProduct.client_id = :cid OR ClientsProduct.client_id=0)

		");






     // $stmt->bindParam(':loyality', $_GET['loyality'], PDO::PARAM_INT);
	  $stmt->bindParam(':cid', $_GET['cid'], PDO::PARAM_INT);

		if($stmt->execute()) {
			if ($row = $stmt->fetchAll(PDO::FETCH_ASSOC)) {
				print XMLSerializer::generateValidXmlFromArray($row, 'root', 'product');
			}
		}
		$dbh = null;
	} catch (PDOException $e) {
		print '<?xml version="1.0" encoding="UTF-8"?>';
		print '<status>'.$e->getMessage().'</status>';
		die();
	}
/*} else {
//	print '<?xml version="1.0" encoding="UTF-8"?>';
//	print '<status>NOT_LOGIN</status>';
//}*/
?>
