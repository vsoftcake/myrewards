<?php
require_once('config.php');
require_once('XMLSerializer.php');
header('Content-Type:text/xml; charset=UTF-8');
//session_start();
//if (isset($_SESSION['uname'])) {
	try {

		$query = "Insert into deals (merchant_id, logo,name,highlights, count, category_id, hours_countdown,start_date,end_date,offer_details,terms_conditions,stores, facebook_status, facebook_url,twitter_status, twitter_url, email_status, email_address, phone_num_status,phone_num1, phone_num2, booknow_status,booknow_text, website_status, website_url, comment_status, comments_text,score_status,score_text, scan_status, scan_text, redeem_status,redeem_text,active)
						values(:merchant_id, :logo,:name,:highlights, :count, :category_id, :hours_countdown,:start_date,:end_date,:offer_details,:terms_conditions,:stores, :facebook_status, :facebook_url,:twitter_status, :twitter_url, :email_status, :email_address, :phone_num_status,:phone_num1, :phone_num2, :booknow_status,:booknow_text, :website_status, :website_url, :comment_status, :comments_text,:score_status,:score_text, :scan_status, :scan_text, :redeem_status,:redeem_text, :active)";

		$dbh = new PDO($conn_string, $db_user, $db_pwd);
// echo 'query ' .  $query;
		$stmt = $dbh->prepare($query);
$q='';
		if (isset($_GET['m_id'])) 	{ $stmt->bindParam(':merchant_id', $_GET['m_id']); 				} else 		{	$q .= '1'; }
		if (isset($_GET['logo'])) {  		$stmt->bindParam(':logo', $_GET['logo']);				} else 		{	$q .= '2'; }
		if (isset($_GET['name'])) {	 	$stmt->bindParam(':name', $_GET['name']);					} else 		{	$q .= '3'; }
		if (isset($_GET['hg'])) {	 		$stmt->bindParam(':highlights', $_GET['hg']);			} else 		{	$q .= '4'; }
		if (isset($_GET['count'])) { 		$stmt->bindParam(':count', $_GET['count']);				} else 		{	$q .= '5'; }
		if (isset($_GET['cat_id'])) { 		$stmt->bindParam(':category_id', $_GET['cat_id']);		} else 		{	$q .= '6'; }
		if (isset($_GET['hd'])) {	 		$stmt->bindParam(':hours_countdown', $_GET['hd']);		} else 		{	$q .= '7'; }
		if (isset($_GET['sd'])) { 			$stmt->bindParam(':start_date', $_GET['sd']);			} else 		{	$q .= '8'; }
		if (isset($_GET['ed'])) {	 		$stmt->bindParam(':end_date', $_GET['ed']);				} else 		{	$q .= '9'; }
		if (isset($_GET['od'])) {	 		$stmt->bindParam(':offer_details', $_GET['od']);		} else 		{	$q .= '10'; }
		if (isset($_GET['tc'])) {	 		$stmt->bindParam(':terms_conditions', $_GET['tc']);		} else 		{	$q .= '11'; }
		if (isset($_GET['stores'])) {	 	$stmt->bindParam(':stores', $_GET['stores']);			} else 		{	$q .= '12'; }
		if (isset($_GET['fbs'])) {		 	$stmt->bindParam(':facebook_status', $_GET['fbs']);		} else 		{	$q .= '13'; }
		if (isset($_GET['fbu'])) {		 	$stmt->bindParam(':facebook_url', $_GET['fbu']);		} else 		{	$q .= '14'; }
		if (isset($_GET['tws'])) {		 	$stmt->bindParam(':twitter_status', $_GET['tws']);		} else 		{	$q .= '15'; }
		if (isset($_GET['twu'])) {		 	$stmt->bindParam(':twitter_url', $_GET['twu']);			} else 		{	$q .= '16'; }
		if (isset($_GET['es'])) {		 	$stmt->bindParam(':email_status', $_GET['es']);			} else 		{	$q .= '17'; }
		if (isset($_GET['ea'])) {		 	$stmt->bindParam(':email_address', $_GET['ea']);		} else 		{	$q .= '18'; }
		if (isset($_GET['pns'])) {		 	$stmt->bindParam(':phone_num_status', $_GET['pns']);	} else 		{	$q .= '19'; }
		if (isset($_GET['pn1'])) {		 	$stmt->bindParam(':phone_num1', $_GET['pn1']);			} else 		{	$q .= '20'; }
		if (isset($_GET['pn2'])) {		 	$stmt->bindParam(':phone_num2', $_GET['pn2']);			} else 		{	$q .= '21'; }
		if (isset($_GET['bs'])) {		 	$stmt->bindParam(':booknow_status', $_GET['bs']);		} else 		{	$q .= '22'; }
		if (isset($_GET['bt'])) {		 	$stmt->bindParam(':booknow_text', $_GET['bt']);			} else 		{	$q .= '23'; }
		if (isset($_GET['ws'])) {		 	$stmt->bindParam(':website_status', $_GET['ws']);		} else 		{	$q .= '24'; }
		if (isset($_GET['wu'])) {		 	$stmt->bindParam(':website_url', $_GET['wu']);			} else 		{	$q .= '25'; }
		if (isset($_GET['cs'])) {		 	$stmt->bindParam(':comment_status', $_GET['cs']);		} else 		{	$q .= '26'; }
		if (isset($_GET['ct'])) {		 	$stmt->bindParam(':comments_text', $_GET['ct']);		} else 		{	$q .= '27'; }
		if (isset($_GET['scs'])) {		 	$stmt->bindParam(':score_status', $_GET['scs']);		} else 		{	$q .= '28'; }
		if (isset($_GET['sct'])) {		 	$stmt->bindParam(':score_text', $_GET['sct']);			} else 		{	$q .= '29'; }
		if (isset($_GET['sts'])) {		 	$stmt->bindParam(':scan_status', $_GET['sts']);			} else 		{	$q .= '30'; }
		if (isset($_GET['stt'])) {		 	$stmt->bindParam(':scan_text', $_GET['stt']);			} else 		{	$q .= '31'; }
		if (isset($_GET['rs'])) {		 	$stmt->bindParam(':redeem_status', $_GET['rs']);		} else 		{	$q .= '32'; }
		if (isset($_GET['rt'])) {		 	$stmt->bindParam(':redeem_text', $_GET['rt']);			} else 		{	$q .= '33'; }
		if (isset($_GET['active'])) {	 	$stmt->bindParam(':active', $_GET['active']);			} else 		{	$q .= '34'; }
//echo 'printing -'. $q;
		if($stmt->execute()) {
			$count = $stmt->rowCount();
			echo $count ? 'success' : 'not_found';
		} else {
			echo 'query_errors'. trigger_error($stmt->error);
		}

	$dbh = null;
	} catch (PDOException $e) {
		print '<?xml version="1.0" encoding="UTF-8"?>';
		print '<status>'.$e->getMessage().'</status>';
		die();
	}
?>