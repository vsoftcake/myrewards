<?php
/*
 * get product
 * input: int id product_id
 * author:gilbert@easyapps.com.hk
 */
require_once('config.php');
require_once('XMLSerializer.php');
header('Content-Type:text/xml; charset=UTF-8');
	try {
		
		$dbh = new PDO($conn_string, $db_user, $db_pwd);

		//select
		$stmt = $dbh->prepare("
			SELECT 
		    MA.id, MA.address1, MA.name, MA.suburb, MA.city, MA.state, MA.region, MA.country, MA.postcode, MA.latitude, MA.longitude, MA.phone, MA.primary
			from
	    merchant_addresses as MA,
	    products_addresses as PA
			WHERE
		    MA.id = PA.merchant_address_id and PA.product_id = ?
		");
		
		if($stmt->execute(array($_GET['pid']))) {
			if ($row = $stmt->fetchAll(PDO::FETCH_ASSOC)) {
				print XMLSerializer::generateValidXmlFromArray($row, 'merchants', 'merchant');
				//print_r($row);
			}
		}
		$dbh = null;
	} catch (PDOException $e) {
		print '<?xml version="1.0" encoding="UTF-8"?>';
		print '<status>'.$e->getMessage().'</status>';
		die();
	}
/*} else {
//	print '<?xml version="1.0" encoding="UTF-8"?>';
//	print '<status>NOT_LOGIN</status>';
//}*/
?>
