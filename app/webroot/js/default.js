function changeBgImg(val)
{
	document.getElementById(val+"quick").style.display="block";
}
function changeOldBg(valu)
{
	document.getElementById(valu+"quick").style.display="none";
}
function changeBgImg1(val1)
{ 
	document.getElementById(val1+"quick1").style.display="block";
}
function changeOldBg1(valu1)
{   
	document.getElementById(valu1+"quick1").style.display="none";
}
function SUDEffect(element){
	jQuery( "#"+element ).toggle( "slow" );
}
function editDetails() {
	jQuery("#modalboxpopup").load("/users/edit").dialog({width: 730, modal: true, "position": "top"});
}
function showLogin() {
	jQuery("#modalboxpopup").load("/users/login").dialog({width: 400, modal: true, "position": "top"});
}
function showFirstTimeLogin() {
	jQuery("#modalboxpopup").load("/users/first_time_login").dialog({width: 850, modal: true, "position": "top"});
}
function productpopup(pid) {
	jQuery("#modalboxpopup").html("").load("/products/quick_view/"+pid).dialog({width: 800, modal: true, "position": "top"});
}
function contactUs() {
	jQuery("#modalboxpopup").load("/contacts/contact_us").dialog({width: 700, modal: true, "position": "top"});
}
function feedback() {
	jQuery("#modalboxpopup").load("/contacts/feedback").dialog({width: 700, modal: true, "position": "top"});
}
function addcartpopup(pid) {
	jQuery("#modalboxpopup").html("").load("/carts/add_cart/"+pid).dialog({width: 730, modal: true, "position": "top"});
}
function redeempoints(pid) {
	jQuery("#modalboxpopup").html("").load("/carts/redeem_cart_points/"+pid).dialog({width: 730, modal: true, "position": "top"});
}
function saf_popup() {
	jQuery("#modalboxpopup").html("").load("/saf/saf_request").dialog({width: 730, modal: true, "position": "top"});
}