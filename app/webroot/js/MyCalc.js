function MyCalc(){

var valFuel = document.savingForm.inputFuel.value;

valFuel = parseFloat(valFuel);

var amtFuel = 0.95 * parseFloat(valFuel);

var savFuel =  0.05 * parseFloat(valFuel);

var valEntertain = document.savingForm.inputEntertain.value;

valEntertain = parseFloat(valEntertain);

var amtEntertain = 0.9 * parseFloat(valEntertain);

var savEntertain = 0.1 * parseFloat(valEntertain);

var valMovie = document.savingForm.inputMovie.value;

valMovie = parseFloat(valMovie);

var amtMovie = 0.8 * parseFloat(valMovie);

var savMovie = 0.2 * parseFloat(valMovie);

var valGroceries = document.savingForm.inputGroceries.value;

valGroceries = parseFloat(valGroceries);

var amtGroceries = 0.95 * parseFloat(valGroceries);

var savGroceries = 0.05 * parseFloat(valGroceries);

var valAuto = document.savingForm.inputAuto.value;

valAuto = parseFloat(valAuto);

var amtAuto = 0.9 * parseFloat(valAuto);

var savAuto = 0.1 * parseFloat(valAuto);

var valClothing = document.savingForm.inputClothing.value;

valClothing = parseFloat(valClothing);

var amtClothing = 0.9 * parseFloat(valClothing);

var savClothing = 0.1 * parseFloat(valClothing);

var valRest = document.savingForm.inputRest.value;

valRest = parseFloat(valRest);

var amtRest = 0.9 * parseFloat(valRest);

var savRest = 0.1 * parseFloat(valRest);

var valTravel = document.savingForm.inputTravel.value;

valTravel = parseFloat(valTravel);

var amtTravel = 0.95 * parseFloat(valTravel);

var savTravel = 0.05 * parseFloat(valTravel);

var valHealth = document.savingForm.inputHealth.value;

valHealth = parseFloat(valHealth);

var amtHealth = 0.9 * parseFloat(valHealth);

var savHealth = 0.1 * parseFloat(valHealth);

var valElectric = document.savingForm.inputElectric.value;

valElectric = parseFloat(valElectric);

var amtElectric = 0.95 * parseFloat(valElectric);

var savElectric = 0.05 * parseFloat(valElectric);

var valTime = document.savingForm.selectTime.value;

var amtTime = parseFloat(valTime);

var spendTotal = (valFuel + valEntertain + valMovie + valGroceries + valAuto + valClothing + valRest + valTravel  + valHealth + valElectric);

var amtTotal = (amtFuel + amtEntertain + amtMovie + amtGroceries + amtAuto + amtClothing + amtRest + amtTravel  + amtHealth + amtElectric);

var savTotal = (savFuel + savEntertain + savMovie + savGroceries + savAuto + savClothing + savRest + savTravel  + savHealth + savElectric);

var weekSave = ((savFuel + savEntertain + savMovie + savGroceries + savAuto + savClothing + savRest + savTravel  + savHealth + savElectric) * amtTime)/52;

var monthSave = ((savFuel + savEntertain + savMovie + savGroceries + savAuto + savClothing + savRest + savTravel  + savHealth + savElectric) * amtTime)/12;

var yearSave = (savFuel + savEntertain + savMovie + savGroceries + savAuto + savClothing + savRest + savTravel  + savHealth + savElectric) * amtTime;

  
var outputFuelAmt = document.getElementById("outputFuelAmt");

outputFuelAmt.firstChild.nodeValue=amtFuel.toFixed(2);

var outputFuelSav = document.getElementById("outputFuelSav");

outputFuelSav.firstChild.nodeValue=savFuel.toFixed(2);

var outputEntertainAmt = document.getElementById("outputEntertainAmt");

outputEntertainAmt.firstChild.nodeValue=amtEntertain.toFixed(2);

var outputEntertainSav = document.getElementById("outputEntertainSav");

outputEntertainSav.firstChild.nodeValue=savEntertain.toFixed(2);

var outputMovieAmt = document.getElementById("outputMovieAmt");

outputMovieAmt.firstChild.nodeValue=amtMovie.toFixed(2);

var outputMovieSav = document.getElementById("outputMovieSav");

outputMovieSav.firstChild.nodeValue=savMovie.toFixed(2);

var outputGroceriesAmt = document.getElementById("outputGroceriesAmt");

outputGroceriesAmt.firstChild.nodeValue=amtGroceries.toFixed(2);

var outputGroceriesSav = document.getElementById("outputGroceriesSav");

outputGroceriesSav.firstChild.nodeValue=savGroceries.toFixed(2);

var outputAutoAmt = document.getElementById("outputAutoAmt");

outputAutoAmt.firstChild.nodeValue=amtAuto.toFixed(2);

var outputAutoSav = document.getElementById("outputAutoSav");

outputAutoSav.firstChild.nodeValue=savAuto.toFixed(2);

var outputClothingAmt = document.getElementById("outputClothingAmt");

outputClothingAmt.firstChild.nodeValue=amtClothing.toFixed(2);

var outputClothingSav = document.getElementById("outputClothingSav");

outputClothingSav.firstChild.nodeValue=savClothing.toFixed(2);

var outputRestAmt = document.getElementById("outputRestAmt");

outputRestAmt.firstChild.nodeValue=amtRest.toFixed(2);

var outputRestSav = document.getElementById("outputRestSav");

outputRestSav.firstChild.nodeValue=savRest.toFixed(2);

var outputTravelAmt = document.getElementById("outputTravelAmt");

outputTravelAmt.firstChild.nodeValue=amtTravel.toFixed(2);

var outputTravelSav = document.getElementById("outputTravelSav");

outputTravelSav.firstChild.nodeValue=savTravel.toFixed(2);

var outputHealthAmt = document.getElementById("outputHealthAmt");

outputHealthAmt.firstChild.nodeValue=amtHealth.toFixed(2);

var outputHealthSav = document.getElementById("outputHealthSav");

outputHealthSav.firstChild.nodeValue=savHealth.toFixed(2);

var outputElectricAmt = document.getElementById("outputElectricAmt");

outputElectricAmt.firstChild.nodeValue=amtElectric.toFixed(2);

var outputElectricSav = document.getElementById("outputElectricSav");

outputElectricSav.firstChild.nodeValue=savElectric.toFixed(2);

var outputSpendAll = document.getElementById("outputSpendAll");

outputSpendAll.firstChild.nodeValue=spendTotal.toFixed(2);

var outputAmtAll = document.getElementById("outputAmtAll");

outputAmtAll.firstChild.nodeValue=amtTotal.toFixed(2);

var outputSavAll = document.getElementById("outputSavAll");

outputSavAll.firstChild.nodeValue=savTotal.toFixed(2);

var outputWeekSave = document.getElementById("outputWeekSave");

outputWeekSave.firstChild.nodeValue=weekSave.toFixed(2);

var outputMonthSave = document.getElementById("outputMonthSave");

outputMonthSave.firstChild.nodeValue=monthSave.toFixed(2);

var outputYearSave = document.getElementById("outputYearSave");

outputYearSave.firstChild.nodeValue=yearSave.toFixed(2);


}
