//	functions used in reporting forms

function selectClients(program_id) {

	if (selected_programs[program_id] == 0) {
		selected_programs[program_id] = 1;
	} else {
		selected_programs[program_id] = 0;
	}

	$("#ReportClient > option").each(function() {var clientId = $(this).val();
		$.each(programs[program_id], function(client_id, val) {
			if (val == clientId) {
				$("#ReportClient option[value='"+clientId+"']").attr("selected", "selected");
				throw $break;
			}
		});
	});
}

function selectClientsProducts(program_id) {

	if (selected_programs[program_id] == 0) {
		selected_programs[program_id] = 1;
	} else {
		selected_programs[program_id] = 0;
	}

	if (selected_programs_cat[program_id] == 0) {
		selected_programs_cat[program_id] = 1;
	} else {
		selected_programs_cat[program_id] = 0;
	}
	
	$('#ReportClient').children().each(function(element) {
		programs[program_id].each(function(client_id) {
			if (client_id == element.value) {
				element.selected = selected_programs[program_id];
				throw $break;
			}
		});
	});
	
	$('ReportCategory').immediateDescendants().each(function(element1) {
		programs_cat[program_id].each(function(cat_id) {
			if (cat_id == element1.value) {
				element1.selected = selected_programs_cat[program_id];
				throw $break;
			}
		});
	});
}


function selectAllClients() {
	$("#ReportClient > option").each(function() {
		if (all_clients == 0) {
			$("#ReportClient option[value='"+$(this).val()+"']").attr("selected", "selected");
		}else{
			$("#ReportClient option[value='"+$(this).val()+"']").removeAttr("selected");
		}
	});
	if (all_clients == 0) {
		all_clients = 1;
	} else {
		all_clients = 0;
	}
}

function checkForm() {
	if($('ReportClient').value == '') {
		alert('Please select some clients');
		return false;
	}
}
