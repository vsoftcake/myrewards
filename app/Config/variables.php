<?php
/**
 *
 * This file is loaded automatically by the app/webroot/index.php file after the core bootstrap.php is loaded
 * This is an application wide file to load any function that is not used within a class define.
 * You can also use this to include or require any files in your application.
 *
 */
/**
 * The settings below can be used to set additional paths to models, views and controllers.
 * This is related to Ticket #470 (https://trac.cakephp.org/ticket/470)
 *
 * $modelPaths = array('full path to models', 'second full path to models', 'etc...');
 * $viewPaths = array('this path to views', 'second full path to views', 'etc...');
 * $controllerPaths = array('this path to controllers', 'second full path to controllers', 'etc...');
 *
 */

// file paths
define('FILES_PATH', WWW_ROOT. 'files/');
define('BANNER_PATH',  FILES_PATH. 'banner/');
define('MERCHANT_LOGO_PATH',  FILES_PATH. 'merchant_logo/');
define('MERCHANT_IMAGE_PATH',  FILES_PATH. 'merchant_image/');
define('PRODUCT_IMAGE_PATH',  FILES_PATH. 'product_image/');
define('PRODUCT_HOT_OFFER_IMAGE_PATH',  FILES_PATH. 'hot_offer_image/');
define('PRODUCT_HOT_OFFER_IMAGE_SAF_PATH',  FILES_PATH. 'hot_offer_img_saf/');
define('PRODUCT_HOT_OFFER_IMAGE_POINTS_PATH',  FILES_PATH. 'hot_offer_img_points/');
define('SKYSCRAPER_IMAGE_PATH',  FILES_PATH. 'skyscraper_image/');
define('STYLE_PATH',  FILES_PATH. 'style/');
define('CATEGORY_LOGO_PATH',  FILES_PATH. 'category_logo/');
define('CLIENT_MEMBERSHIP_CARD_IMAGE_PATH',  FILES_PATH. 'mobile_membership_card/');
define('APP_IMAGE_PATH',  FILES_PATH. 'app_image/');
define('CAMPAIGN_IMAGE_PATH',  FILES_PATH. 'campaign/');
define('NEWSLETTER_IMAGE1_PATH',  FILES_PATH. 'newsletters/image1/');
define('NEWSLETTER_IMAGE2_PATH',  FILES_PATH. 'newsletters/image2/');
define('NEWSLETTER_IMAGE3_PATH',  FILES_PATH. 'newsletters/image3/');
define('NEWSLETTER_IMAGE4_PATH',  FILES_PATH. 'newsletters/image4/');
define('BANNER_BACKGROUND_IMAGE_LEFT_PATH',  FILES_PATH. 'banner_background_image_left/');
define('BANNER_BACKGROUND_IMAGE_RIGHT_PATH',  FILES_PATH. 'banner_background_image_right/');
define('BANNER_BACKGROUND_IMAGE_LEFT_SAF_PATH',  FILES_PATH. 'banner_background_image_left_saf/');
define('BANNER_BACKGROUND_IMAGE_RIGHT_SAF_PATH',  FILES_PATH. 'banner_background_image_right_saf/');
define('BANNER_BACKGROUND_IMAGE_LEFT_POINTS_PATH',  FILES_PATH. 'banner_background_image_left_points/');
define('BANNER_BACKGROUND_IMAGE_RIGHT_POINTS_PATH',  FILES_PATH. 'banner_background_image_right_points/');

define('CLIENT_BACKGROUND_IMAGE_LEFT_PATH',  FILES_PATH. 'client_background_images_left/');
define('CLIENT_BACKGROUND_IMAGE_RIGHT_PATH',  FILES_PATH. 'client_background_images_right/');
define('CLIENT_BACKGROUND_IMAGE_LEFT_2_PATH',  FILES_PATH. 'client_background_images_left_2/');
define('CLIENT_BACKGROUND_IMAGE_RIGHT_2_PATH',  FILES_PATH. 'client_background_images_right_2/');
define('CLIENT_BACKGROUND_IMAGE_LEFT_3_PATH',  FILES_PATH. 'client_background_images_left_3/');
define('CLIENT_BACKGROUND_IMAGE_RIGHT_3_PATH',  FILES_PATH. 'client_background_images_right_3/');

define('PROGRAM_BACKGROUND_IMAGE_LEFT_PATH',  FILES_PATH. 'program_background_images_left/');
define('PROGRAM_BACKGROUND_IMAGE_RIGHT_PATH',  FILES_PATH. 'program_background_images_right/');
define('PROGRAM_BACKGROUND_IMAGE_LEFT_2_PATH',  FILES_PATH. 'program_background_images_left_2/');
define('PROGRAM_BACKGROUND_IMAGE_RIGHT_2_PATH',  FILES_PATH. 'program_background_images_right_2/');
define('PROGRAM_BACKGROUND_IMAGE_LEFT_3_PATH',  FILES_PATH. 'program_background_images_left_3/');
define('PROGRAM_BACKGROUND_IMAGE_RIGHT_3_PATH',  FILES_PATH. 'program_background_images_right_3/');


//	www paths
define('FILES_WWW_PATH', '/files/');
define('BANNER_WWW_PATH',  FILES_WWW_PATH. '/banner/');
define('MERCHANT_LOGO_WWW_PATH',  FILES_WWW_PATH. '/merchant_logo/');
define('MERCHANT_IMAGE_WWW_PATH',  FILES_WWW_PATH. '/merchant_image/');
define('PRODUCT_IMAGE_WWW_PATH',  FILES_WWW_PATH. '/product_image/');
define('HOT_OFFER_IMAGE_WWW_PATH',  FILES_WWW_PATH. '/hot_offer_image/');
define('HOT_OFFER_IMAGE_POINTS_WWW_PATH',  FILES_PATH. '/hot_offer_img_points/');
define('HOT_OFFER_IMAGE_SAF_WWW_PATH',  FILES_PATH. '/hot_offer_img_saf/');
define('SKYSCRAPER_IMAGE_WWW_PATH',  FILES_WWW_PATH. '/skyscraper_image/');
define('STYLE_WWW_PATH',  FILES_WWW_PATH. '/style/');
define('CATEGORY_LOGO_WWW_PATH',  FILES_WWW_PATH. '/category_logo/');
define('CLIENT_MEMBERSHIP_CARD_IMAGE_WWW_PATH',  FILES_WWW_PATH. '/mobile_membership_card/');
define('APP_IMAGE_WWW_PATH',  FILES_WWW_PATH. '/app_image/');
define('CAMPAIGN_IMAGE_WWW_PATH',  FILES_WWW_PATH. '/campaign/');
define('NEWSLETTER_IMAGE1_WWW_PATH',  FILES_WWW_PATH. '/newsletters/image1/');
define('NEWSLETTER_IMAGE2_WWW_PATH',  FILES_WWW_PATH. '/newsletters/image2/');
define('NEWSLETTER_IMAGE3_WWW_PATH',  FILES_WWW_PATH. '/newsletters/image3/');
define('NEWSLETTER_IMAGE4_WWW_PATH',  FILES_WWW_PATH. '/newsletters/image4/');
define('BANNER_BACKGROUND_IMAGE_LEFT_WWW_PATH',  FILES_WWW_PATH. '/banner_background_image_left/');
define('BANNER_BACKGROUND_IMAGE_RIGHT_WWW_PATH',  FILES_WWW_PATH. '/banner_background_image_right/');
define('BANNER_BACKGROUND_IMAGE_LEFT_SAF_WWW_PATH',  FILES_WWW_PATH. '/banner_background_image_left_saf/');
define('BANNER_BACKGROUND_IMAGE_RIGHT_SAF_WWW_PATH',  FILES_WWW_PATH. '/banner_background_image_right_saf/');
define('BANNER_BACKGROUND_IMAGE_LEFT_POINTS_WWW_PATH',  FILES_WWW_PATH. '/banner_background_image_left_points/');
define('BANNER_BACKGROUND_IMAGE_RIGHT_POINTS_WWW_PATH',  FILES_WWW_PATH. '/banner_background_image_right_points/');

define('CLIENT_BACKGROUND_IMAGE_LEFT_WWW_PATH',  FILES_WWW_PATH. '/client_background_images_left/');
define('CLIENT_BACKGROUND_IMAGE_RIGHT_WWW_PATH',  FILES_WWW_PATH. '/client_background_images_right/');
define('CLIENT_BACKGROUND_IMAGE_LEFT_2_WWW_PATH',  FILES_WWW_PATH. '/client_background_images_left_2/');
define('CLIENT_BACKGROUND_IMAGE_RIGHT_2_WWW_PATH',  FILES_WWW_PATH. '/client_background_images_right_2/');
define('CLIENT_BACKGROUND_IMAGE_LEFT_3_WWW_PATH',  FILES_WWW_PATH. '/client_background_images_left_3/');
define('CLIENT_BACKGROUND_IMAGE_RIGHT_3_WWW_PATH',  FILES_WWW_PATH. '/client_background_images_right_3/');

define('PROGRAM_BACKGROUND_IMAGE_LEFT_WWW_PATH',  FILES_WWW_PATH. '/program_background_images_left/');
define('PROGRAM_BACKGROUND_IMAGE_RIGHT_WWW_PATH',  FILES_WWW_PATH. '/program_background_images_right/');
define('PROGRAM_BACKGROUND_IMAGE_LEFT_2_WWW_PATH',  FILES_WWW_PATH. '/program_background_images_left_2/');
define('PROGRAM_BACKGROUND_IMAGE_RIGHT_2_WWW_PATH',  FILES_WWW_PATH. '/program_background_images_right_2/');
define('PROGRAM_BACKGROUND_IMAGE_LEFT_3_WWW_PATH',  FILES_WWW_PATH. '/program_background_images_left_3/');
define('PROGRAM_BACKGROUND_IMAGE_RIGHT_3_WWW_PATH',  FILES_WWW_PATH. '/program_background_images_right_3/');

define('MAIL_CONNECTION', 'smtp');
define('SMTP_SERVER', 'localhost.localdomain');
define('SMTP_USER', '');
define('SMTP_PASS', '');
define('WEB_MAIL_FROM','info@myrewards.com.au');

//turn off soap wsdl cahing whiel in development
ini_set('soap.wsdl_cache_enabled', 0);

//	maximum rows returned in one report
define('MAX_REPORT_ROW_NUM', 30000);

//	time before a user is considered logged out (used for reporting of logged on users (seconds)
define('REPORT_SESSION_EXPIRY', 600);

//	number of rows to return per page in a search
define('SEARCH_RESULTS_PER_PAGE', 15);

//	number of special offers to  return per page in a search
define('SPECIAL_OFFERS_PER_PAGE', 20);
?>